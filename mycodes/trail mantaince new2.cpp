#include<bits/stdc++.h>

using namespace std;


struct node
{
    int n,w;
};
bool operator < (node x, node y)
{
    if(x.w!=y.n)
    {
        return x.w >y.w;
    }
    return false;
}

int find_rep(int r,int *ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    ar[r]=find_rep(ar[r],ar);
    return ar[r];
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,w,i,j,mstchk=0,mx,sum;
        scanf("%d %d",&n,&w);
        int parent[n+1];
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }

        vector<int>edges[n+1];
        vector<int>cost[n+1];
        vector<int>rank_(n+1,0);
        vector<int>trail;
        for(i=1;i<=w;i++)
        {
            int u ,v,c;
            scanf("%d %d %d",&u,&v,&c);
            if(mstchk==1)
            {
                if(mx<c)
                {
                    cout<<sum<<endl;
                    trail.push_back(sum);
                    continue;
                }
            }
            edges[u].push_back(v);
            edges[v].push_back(u);
            cost[u].push_back(c);
            cost[v].push_back(c);

            int x,y;
            x=find_rep(u,parent);
            y=find_rep(v,parent);
            if(x!=y)
            {
                if(rank_[x]==rank_[y])
                {
                    parent[y]=x;
                    rank_[x]++;
                }
                else if(rank_[x]>rank_[y])
                {
                    parent[y]=x;
                }
                else
                    parent[x]=y;
            }
            int chk=0;
            for(j=1;j<n;j++)
            {
                if(mstchk==1)
                {
                    break;
                }
                if(parent[parent[j]]!=parent[parent[j+1]])
                {
                    chk=1;
                    break;

                }
            }
            if(chk==0)
            {
                mstchk=1;
            }
            if(mstchk==0)
            {
                trail.push_back(-1);
            }
            else
            {
                vector<int>vis(n+1,0);
                vector<int>mst(n+1,2e9);
                node start;
                start.n=1;
                start.w=0;
                mst[1]=0;
                priority_queue<node>q;
                q.push(start);

                while(!q.empty())
                {
                    node cur_top=q.top();
                    q.pop();

                    int cur_node=cur_top.n;

                    if(vis[cur_node])
                    {
                        continue;
                    }
                    vis[cur_node]=1;
                    //cout<<"current node ="<<cur_node<<endl;
                    for(j=0;j<edges[cur_node].size();j++)
                    {
                        //cout<<"here";
                        int nxt_node=edges[cur_node][j];
                        int nxt_cost=cost[cur_node][j];
                      //  cout<<"nxt node="<<nxt_node<<" next cost="<<nxt_cost<<" currnt cost="<<mst[nxt_node]<<endl;
                        if(nxt_cost<mst[nxt_node]&& vis[nxt_node]==0)
                        {
                            mst[nxt_node]=nxt_cost;
                            node nwnode;
                            nwnode.n=nxt_node;
                            nwnode.w=nxt_cost;
                            q.push(nwnode);
                        }
                    }
                }
                sum=0,mx=-1;
                for(j=1;j<=n;j++)
                {
                    mx=max(mst[j],mx);
                    sum+=mst[j];
                }
                //cout<<"mx="<<mx<<endl;
                trail.push_back(sum);
            }
        }
        printf("Case %d:\n",t);
        for(j=0;j<trail.size();j++)
        {
            printf("%d\n",trail[j]);
        }
        t++;
    }
}
