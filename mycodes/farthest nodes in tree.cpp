#include<bits/stdc++.h>
using namespace std;
int main()
{
    int test,t=1;
    cin>>test;
    while(t<=test)
    {
        unsigned long long nodes,i;
        cin>>nodes;

        vector<int>v2[nodes+10],ve[nodes+10];


      unsigned long long  dis[nodes+10];
        vector<int>vis(nodes+10);
       int v,u,w;
        for(i=1;i<nodes;i++)
        {

            cin>>u>>v>>w;
            v2[u].push_back(v);
            ve[u].push_back(w);
            v2[v].push_back(u);
            ve[v].push_back(w);

        }

        unsigned long long source=0;
        queue<unsigned long long>q;
        q.push(u);
        vis[u]=1;
        dis[u]=0;
        while(!q.empty())
        {
            unsigned long long current_node=q.front();
            q.pop();
            for(i=0;i<v2[current_node].size();i++)
            {
                if(!vis[v2[current_node].at(i)])
                {
                    vis[v2[current_node].at(i)]=1;
                    dis[v2[current_node].at(i)]=dis[current_node]+ve[current_node].at(i);
                    q.push(v2[current_node].at(i));
                }
            }

        }
       unsigned long long  maxdis=0,maxnode1;
        for(i=1;i<nodes;i++)
        {
            if(dis[i]>maxdis)
            {
                maxnode1=i;
            }
        }

        queue<unsigned long long>q2;
        vector<int>vis2(nodes+10);
       unsigned long long dis2[nodes+10];
        q2.push(maxnode1);
        vis2[maxnode1]=1;
        dis2[maxnode1]=0;
        while(!q2.empty())
        {
            int currentnode=q2.front();
            q2.pop();
            for(i=0;i<v2[currentnode].size();i++)
            {
                if(!vis2[v2[currentnode].at(i)])
                {
                    vis2[v2[currentnode].at(i)]=1;
                    dis2[v2[currentnode].at(i)]=dis2[currentnode]+ve[currentnode].at(i);
                    q2.push(v2[currentnode].at(i));
                }

            }
        }

         maxdis=0;
        for(i=0;i<nodes;i++)
        {
            if(maxdis<dis2[i])
            {
                maxdis=dis2[i];
            }
        }
        printf("Case %d: %llu\n",t,maxdis);
        t++;
    }
}
