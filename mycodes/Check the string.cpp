#include<bits/stdc++.h>
using namespace std;

bool rightorder(string s)
{
    int i,j,a=0,b=0,c=0;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='a')
        {
            if(b==1 || c==1)
            {
                return false;
            }
            a=1;
        }
        else if(s[i]=='b')
        {
            if(a==0 || c==1)
            {
                return false;
            }
            b=1;
        }
        else if(s[i]=='c')
        {
            if(a==0 || b==0)
            {
                return false;
            }
            c=1;
        }
    }
    return true;
}

bool correct(string s)
{
    int a=0,b=0,c=0,i;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='a')
        {
            a++;
        }
        else if(s[i]=='b')
        {
            b++;
        }
        else c++;
    }
    if(a>0&& b>0&& (a==c || b==c))
    {
        return true;
    }
    else
        return false;
}
int main()
{
    string s;
    cin>>s;
    //cout<<"order="<<rightorder(s)<<" correct="<<correct(s)<<endl;
    if(rightorder(s)&&correct(s))
    {
        cout<<"YES"<<endl;
    }
    else
        cout<<"NO"<<endl;
}
