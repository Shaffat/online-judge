
#include<bits/stdc++.h>
using namespace std;

double expectedRounds(int a,int b,vector<vector<double> >&memory,vector<vector<int> >&vis)
{
    //cout<<"a="<<a<<" b="<<b<<endl;
    if(a==b) return 1;
    if(vis[a][b]) return 2;
    if(memory[a][b]!=-1 || memory[b][a]!=-1)
        return memory[a][b];
    int i,j,c;
    double res=0;
    c=min(a,b);

    if(a>c){
        //cout<<"for greater a a="<<a<<" b="<<b<<" calling a="<<a-c<<" b="<<b-c<<" c="<<c<<endl;
        vis[a-c][b+c]=1;
        res=(0.5*(expectedRounds(a-c,b+c,memory,vis)))+ 1;
    }
    else if (b>c){
        //cout<<"for greater b a"<<a<<" b="<<b<<" calling a="<<a+c<<" b="<<b+c<<endl;
        vis[a+c][b-c]=1;
        res=(0.5*(expectedRounds(a+c,b-c,memory,vis)))+ 1;
    }
    return memory[a][b]=res;
}


int main()
{
    int test,t;
    double a,b,win,rounds;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lf %lf",&a,&b);
        win=a/(a+b);
        vector<double>col(400,-1);
        vector<vector<double> >memory(400,col);
        vector<int>col1(400,0);
        vector<vector<int> >vis(400,col1);
        vis[int(a)][int(b)]=1;
        rounds=expectedRounds(int(a),int(b),memory,vis);

        printf("Case %d: %.8lf %.8lf\n",t,rounds,win);
    }
}
