#include<bits/stdc++.h>

using namespace std;

int t;
vector<int>testcase(1000,0);
vector<int>memory(1000,0);

vector<int>realName(1000);

int solve(int n,int id,vector<vector<int> >&tree,vector<int>&age,vector<int>&memory)
{
    //cout<<"id "<<id<<endl;
    int i,j,res=2e9;
    if(testcase[id]==t)
    {
        return memory[id];
    }
    for(i=1;i<=n;i++)
    {
        if(tree[id][i]==1)
        {
            //cout<<id<<" is connected to "<<i<<endl;
            int real=realName[i];
            //cout<<"calling "<<i<<" cur_min="<<res<<endl;
            int cur=solve(n,i,tree,age,memory);
            //cout<<i<<" gives "<<cur<<endl;

            res=min(res,min(cur,age[real]));
            //cout<<"at id "<<id<<" res="<<res<<endl;
        }
    }
    //cout<<"res="<<res<<" at "<<id<<endl;
    testcase[id]=t;
    return memory[id]=res;
}

int getGraphNode(int x,int n)
{
    for(int i=1;i<=n;i++)
    {
        if(realName[i]==x)
            return i;
    }
}

int countBoss(int x,int n,vector<vector<int> >&tree)
{
    int i,j=0;
    for(i=1;i<=n;i++)
    {
        if(tree[x][i])
            j++;
    }
    return j;
}


int main()
{
    int n,m,I,u,v,i,j,q;
    while(scanf("%d %d %d",&n,&m,&I)!=EOF)
    {
        vector<int>age(n+1);
        for(i=1;i<=n;i++)
        {
            realName[i]=i;
        }
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            age[i]=j;
        }

        vector<int>col(501,0);
        vector<vector<int> >tree(501,col);
        vector<vector<int> >myEmploye(501);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            tree[v][u]=1;
            myEmploye[u].push_back(v);
        }
        for(i=1;i<=I;i++)
        {
            char c;
            scanf(" %c",&c);
            if(c=='P')
            {
                scanf("%d",&u);
                t++;
                //cout<<u<<" has "<<tree[u].size()<<" boss"<<endl;
                int x=getGraphNode(u,n);
                //cout<<"real pos of "<<u<<" is "<<x<<endl;
                if(countBoss(x,n,tree)==0)
                {
                    printf("*\n");
                }
                else{
                int res=solve(n,x,tree,age,memory);
                //cout<<"solve has finished"<<endl;
                printf("%d\n",res);
                }

            }
            else
            {
                scanf("%d %d",&u,&v);
                //update(n,u,v,tree,myEmploye);
                int real_u,real_v;
                real_u=getGraphNode(u,n);
                real_v=getGraphNode(v,n);
                realName[real_u]=v;
                realName[real_v]=u;

            }
        }

    }
}
