#include<bits/stdc++.h>
using namespace std;

void LongestProperSuffix(string &pattern,vector<int>&lps,vector<int>&suffix_exist)
{
    int i,j;
    lps[0]=-1;
    suffix_exist[0]=1;
    for(i=1;i<pattern.size();i++)
    {
        lps[i]=-1;
        int cur=i-1;
        if(pattern[lps[cur]+1]==pattern[i])
        {
            lps[i]=lps[cur]+1;
        }
        else
        {
            while(cur!=-1)
            {
                    if(pattern[lps[cur]+1]==pattern[i])
                    {
                        lps[i]=lps[cur]+1;
                        break;
                    }
                    else
                    {
                        cur=lps[cur];
                    }
            }
        }
        cout<<"i="<<i<<" lps="<<lps[i]<<endl;
        suffix_exist[lps[i]+1]+=1;
        cout<<"len="<<lps[i]+1<<" ocuured="<<suffix_exist[lps[i]+1]<<endl;
    }
    return;
}


int solve(int n,vector<int>&lps,vector<int>&suffix_counter)
{
    int cur=n-1,mx;
    //cout<<"Cur="<<cur<<endl;
    mx=suffix_counter[lps[cur]+1];
    if(mx>=2)
    {
        return lps[cur]+1;
    }
    cur=lps[cur];
    while(cur!=-1)
    {
        int len=lps[cur]+1;
        //cout<<"len="<<len<<" counter="<<suffix_counter[len]<<endl;
        if(suffix_counter[len]+mx>=2)
            return len;
        cur=lps[cur];
    }
    return 0;
}


int main()
{
    ios_base::sync_with_stdio(0);
    string s;
    cin>>s;
    vector<int>lps(s.size());
    vector<int>counter(s.size(),0);
    LongestProperSuffix(s,lps,counter);
    int res,i,j;
    res=solve(s.size(),lps,counter);
    if(res)
    {
        for(i=0;i<res;i++)
        {
            cout<<s[i];
        }
        cout<<endl;
    }
    else
        cout<<"Just a legend"<<endl;
}
