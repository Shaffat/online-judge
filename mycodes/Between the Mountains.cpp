
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

ll solve(vl &v1,vl &v2)
{
    ll i,j,res=1e11;
    for(i=0;i<v1.size();i++)
    {
        for(j=0;j<v2.size();j++)
        {
            res=min(res,abs(v1[i]-v2[j]));
        }
    }
    return res;
}

int main()
{
    ll test,t,n,i,j;
    scanf("%lld",&test);
    while(test--)
    {
        vl v;
        vl v1;
        scanf("%lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            v.push_back(j);
        }
        scanf("%lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            v1.push_back(j);
        }
        ll res=solve(v,v1);
        printf("%lld\n",res);
    }
}
