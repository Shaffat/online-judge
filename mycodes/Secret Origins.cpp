#include<bits/stdc++.h>
using namespace std;

int converter(string s)
{
    int res=0,j=0,i;
    for(i=s.size()-1;i>=0;i--)
    {
        if(s[i]=='1')
        {
            res+=(1<<j);
        }
        j++;
    }
    return res;
}


string binary_represent(int n)
{
    int bits=ceil(log2(n)),i,j;
    string s;
    for(i=0;i<=bits;i++)
    {
        if((n&(1<<i))==(1<<i))
        {
            s.push_back('1');
        }
        else
            s.push_back('0');
    }
    if(s[s.size()-1]=='0')
    {
        s.erase(s.begin()+s.size()-1);
    }
    reverse(s.begin(),s.end());
    return s;
}
int solve(string s)
{
    int i,j,res=0,ones=0,pos=-1;
    string rstr="";
    for(i=s.size()-1;i>=0;i--)
    {
        //cout<<"i="<<i<<" s="<<s[i]<<" ones="<<ones<<endl;
        if(ones)
        {
            if(s[i]=='0')
            {
                pos=i;
                break;
            }
        }
        if(s[i]=='1')
        {
            ones++;
        }
    }
    //cout<<"pos="<<pos<<" ones="<<ones<<endl;
    if(pos==-1)
    {
        rstr.push_back('1');
        int zero=s.size()-ones+1;
        for(i=1;i<=zero;i++)
        {
            rstr.push_back('0');
        }
        for(i=1;i<ones;i++)
        {
            rstr.push_back('1');
        }
    }
    else
    {
        for(i=0;i<pos;i++)
        {
            rstr.push_back(s[i]);
        }
        rstr.push_back('1');
        rstr.push_back('0');
        while(rstr.size()!=s.size())
        {
            rstr.push_back('0');
        }
        //cout<<"b adjust="<<rstr<<endl;
        i=rstr.size()-1;
        ones--;
        while(ones)
        {
            rstr[i]='1';
            ones--;
            i--;
        }
    }
    //cout<<"rstr="<<rstr<<endl;
    res=converter(rstr);
    return res;

}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,i,j,m,first_zero;
        scanf("%d",&n);
        string s;
        s=binary_represent(n);
        //cout<<"s="<<s<<endl;
        printf("Case %d: %d\n",t,solve(s));
    }

}
