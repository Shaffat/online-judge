#include<bits/stdc++.h>

using namespace std;

int m,n;

struct gridview
{
    int r,c;
};

int solve(int curgold,int bitmask,vector<gridview>&golds,vector< vector<int> >&memory)
{
    //cout<<"curgold="<<curgold<<" bitmask="<<bitmask<<endl;
    if(bitmask==(1<<golds.size())-1)
    {
        int curdis=min(abs(golds[curgold].r-golds[0].r),abs(golds[curgold].c-golds[0].c));
        curdis+=max(abs(golds[curgold].r-golds[0].r),abs(golds[curgold].c-golds[0].c))-curdis;
        return curdis;
    }
    if(memory[curgold][bitmask]!=-1)
    {
        return memory[curgold][bitmask];
    }

    int dis=2e9,i,j;
    for(i=1;i<golds.size();i++)
    {
        if((bitmask&(1<<i))==0)
        {
            int curdis=min(abs(golds[curgold].r-golds[i].r),abs(golds[curgold].c-golds[i].c));
            curdis+=max(abs(golds[curgold].r-golds[i].r),abs(golds[curgold].c-golds[i].c))-curdis;
            dis=min(solve(i,bitmask|(1<<i),golds,memory)+curdis,dis);
        }
    }
    return memory[curgold][bitmask]=dis;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j;
        scanf("%d %d",&m,&n);
        getchar();
        vector<gridview>golds;
        for(i=1;i<=m;i++)
        {
            for(j=1;j<=n;j++)
            {
                char c;
                scanf("%c",&c);
                if(c=='g')
                {
                    gridview tmp;
                    tmp.r=i;
                    tmp.c=j;
                    golds.push_back(tmp);
                }
                if(c=='x')
                {
                    gridview tmp;
                    tmp.r=i;
                    tmp.c=j;
                    golds.insert(golds.begin()+0,tmp);
                }

            }
            getchar();
        }
        vector<int>value(1<<16,-1);
        vector<vector<int> >memory(17,value);
        int result=solve(0,1,golds,memory);
        printf("Case %d: %d\n",t,result);
        t++;


    }
}
