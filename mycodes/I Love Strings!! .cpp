#include<bits/stdc++.h>

using namespace std;

int LongestProperSuffix(int n,string &pattern,vector<int>&lps)
{
    int i,j;
    lps[0]=-1;
    //cout<<"i=0 lps="<<lps[0]<<endl;
    for(i=1;i<pattern.size();i++)
    {
        lps[i]=-1;
        int cur=i-1;
        //cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
        if(pattern[lps[cur]+1]==pattern[i])
        {
            lps[i]=lps[cur]+1;
        }
        else
        {
            while(cur!=-1)
            {
//                cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
//                cout<<"trying to match "<<lps[cur]+1<<" and "<<i<<endl;
                    if(pattern[lps[cur]+1]==pattern[i])
                    {
                        lps[i]=lps[cur]+1;
                        break;
                    }
                    else
                    {
                        cur=lps[cur];
                    }
            }
        }
        if(lps[i]+1==n) return 1;
    }
    return 0;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j,q;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s,p,s1;
        cin>>s>>q;
        for(i=1;i<=q;i++)
        {
            cin>>p;
            s1=p+"#"+s;
            vector<int>lps(s1.size());
            if(LongestProperSuffix(p.size(),s1,lps))
                cout<<"y"<<endl;
            else
                cout<<"n"<<endl;
        }

    }

}
