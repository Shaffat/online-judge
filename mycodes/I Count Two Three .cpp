#include<stdio.h>
#include<vector>
#include<algorithm>
using namespace std;

#define ll long long int
vector<ll>numbers;
vector<ll>coins;

void preprocess(ll index,ll cur)
{
    //cout<<"cur="<<cur<<" index="<<index<<endl;
    if(cur>10000000000 || cur<0)
        return;
    if(index>=coins.size())
    {
        //cout<<"cur="<<cur<<endl;
        numbers.push_back(cur);
        return;
    }
    //cout<<"here"<<endl;
    ll i,tmp=1,res;
    //cout<<"here1"<<endl;
    for(i=0;i<32;i++)
    {
        //cout<<"inside index="<<index<<endl;
        res=cur;
        //cout<<"res="<<res<<endl;
        res*=tmp;
        tmp*=coins[index];
//        cout<<"res="<<res<<" tmp="<<tmp<<endl;
//        cout<<"tmp="<<tmp<<endl;
        preprocess(index+1,res);
    }
    return;
}

ll not_less(ll n)
{
    ll first=0,last=numbers.size()-1,mid,res,f=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        //cout<<"n="<<n<<" first="<<first<<" last="<<last<<" mid="<<mid<<" ="<<numbers[mid]<<endl;
        if(numbers[mid]>=n)
        {
            res=numbers[mid];
            last=mid-1;
        }
        else
            first=mid+1;
    }
    //cout<<"res="<<res<<endl;
    return res;
}


int main()
{
    coins.push_back(2);
    coins.push_back(3);
    coins.push_back(5);
    coins.push_back(7);
    preprocess(0,1);
    //cout<<"least="<<numbers[0]<<" size="<<numbers.size()<<endl;
    sort(numbers.begin(),numbers.end());
    ll test,t,i,j,res;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld",&j);
        res=not_less(j);
        printf("%lld\n",res);
    }
}
