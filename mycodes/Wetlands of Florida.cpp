#include<bits/stdc++.h>

using namespace std;

struct node
{
    int r,c;
};


bool is_valid(int r,int c,node n)
{
    if(n.r<0 || n.r>r ||n.c<0 ||n.c>c)
    {
        return false;
    }
    return true;
}

node convertion(string s)
{
    node res;
    int i,j=0,total=0,k=1;
    for(i=s.size()-1;i>=0;i--)
    {
        if(s[i]==' ')
        {
            j=1;
            res.c=total;
            total=0;
            k=1;
        }
        else
        {
            total+=k*(s[i]-'0');
            k*=10;
        }
    }
    res.r=total;
    return res;
}
void bfs(node root,vector<vector<bool> >&vis,vector<vector<int> >&dis,vector<vector<node> >&parent,vector<string>&land)
{
    char ch=land[root.r][root.c];
    int i,j,r,c,total=1;
    r=land.size()-1;
    c=land[0].size()-1;
    vis[root.r][root.c]=1;
    parent[root.r][root.c]=root;
    queue<node>q;
    q.push(root);
    node tmp;
    while(!q.empty())
    {
        node cur=q.front();
        q.pop();
        tmp.r=cur.r-1;
        tmp.c=cur.c-1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r;
        tmp.c=cur.c-1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r+1;
        tmp.c=cur.c-1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r-1;
        tmp.c=cur.c;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r+1;
        tmp.c=cur.c;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r-1;
        tmp.c=cur.c+1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r;
        tmp.c=cur.c+1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
        tmp.r=cur.r+1;
        tmp.c=cur.c+1;
        if(is_valid(r,c,tmp))
        {
            if(vis[tmp.r][tmp.c]==0 && land[tmp.r][tmp.c]==ch)
            {
                vis[tmp.r][tmp.c]=1;
                parent[tmp.r][tmp.c]=root;
                total++;
                q.push(tmp);
            }
        }
    }
    dis[root.r][root.c]=total;
    return;
}
void solve(vector<string>&land,vector<vector<node> >&parent,vector<vector<int> >&dis)
{
    int i,j;
    vector<bool>col1(land[0].size(),0);
    vector<vector<bool> >vis(land.size(),col1);

    for(i=0;i<land.size();i++)
    {
        for(j=0;j<land[i].size();j++)
        {
            node tmp;
            if(!vis[i][j])
            {
                tmp.r=i;
                tmp.c=j;
                bfs(tmp,vis,dis,parent,land);
            }
        }
    }

    return;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,j,test,t=1;
    string s;
    cin>>test;
    while(t<=test)
    {
        if(t!=1)
        {
            cout<<"\n";
        }
        vector<string>land;
        while(getline(cin,s))
        {
            if(s=="")
            {
                continue;
            }
            if(s[0]!='W'&&s[0]!='L')
            {
                break;
            }
            land.push_back(s);
        }
        vector<node>col2(land[0].size());
        vector<vector<node> >parent(land.size(),col2);
        vector<int>col3(land[0].size());
        vector<vector<int> >dis(land.size(),col3);
        solve(land,parent,dis);
        int r,c,pr,pc;
        node tmp=convertion(s);
        r=tmp.r;
        c=tmp.c;
        r--;
        c--;
        pr=parent[r][c].r;
        pc=parent[r][c].c;
//        cout<<"r="<<r<<" c="<<c<<endl;
//        cout<<"pr="<<pr<<" pc="<<pc<<endl;
        cout<<dis[pr][pc]<<"\n";
        while(getline(cin,s))
        {
//            cout<<"q s="<<s<<endl;
            if(s=="" || s[0]=='\0')
            {
                break;
            }
            else
            {
                tmp=convertion(s);
                r=tmp.r;
                c=tmp.c;
                r--;
                c--;
                pr=parent[r][c].r;
                pc=parent[r][c].c;
//                cout<<"r="<<r<<" c="<<c<<endl;
//                cout<<"pr="<<pr<<" pc="<<pc<<endl;
                cout<<dis[pr][pc]<<"\n";
            }

        }
        t++;
    }

}
