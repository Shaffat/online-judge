#include<bits/stdc++.h>
using namespace std;

bool dfs_cycle(int node,vector<vector<int> >&connection,vector<int>&vis,vector<int>&dis,vector<int>&dis_root)
{
    int i,j,nxt;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        if(!vis)
        {
            vis[nxt]=1;
            if(dfs_cycle(nxt,connection,vis,dis,dis_root))
            {
                dis_root[node]=dis_root[nxt];
                dis[node]=dis[nxt]+1;
                return true;
            }
            else
            {
                dis_root[node]=node;
                dis[node]=dis[nxt]+1;
                return false;
            }

        }
    }
}
