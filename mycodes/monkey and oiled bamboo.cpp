#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j,n,d,last,mx;
        scanf("%d",&n);
        vector<int>dis;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&d);
            if(i==1)
            {
                dis.push_back(d);
                last=d;
                mx=d;

            }
            else
            {
                dis.push_back(d-last);
                mx=max(mx,(d-last));
                last=d;
            }
            //cout<<mx<<endl;
        }
       // cout<<"cur_s "<<mx<<endl;
        int cur_s=mx;
        for(i=0;i<dis.size();i++)
        {
            if(cur_s>dis[i])
            {
                continue;
            }
            else if(cur_s==dis[i])
            {
                cur_s--;
            }
            else if(cur_s<dis[i])
            {
                mx++;
                cur_s=mx;
                while(cur_s<dis[i])
                {
                    //cout<<"inner max="<<mx<<endl;
                    mx++;
                    cur_s=mx;
                }
                if(cur_s>dis[i])
                {
                    continue;
                }
                else cur_s--;
            }
        }
        printf("Case %d: %d\n",t,mx);
        t++;

    }
}
