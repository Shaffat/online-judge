#include<bits/stdc++.h>

using namespace std;

vector< vector<int> >column(11);
vector< vector<int> >row(11);
vector< vector<int> >bucket(11);
vector< vector<int> >result(11);
vector< vector<int> >sudoku(11);

bool solve(int r,int c)
{

     int i,j;

    if(sudoku[r][c]!=0)
    {

        while(sudoku[r][c]!=0)
        {

            result[r][c]=sudoku[r][c];
            c++;
            if(c>8)
            {

                r++;
                c=0;
            }

        }

    }

    if(r>8)
    {
        return true;
    }

    for(j=1; j<=10; j++)
    {
        if(j==10)
        {
            return false;
        }
        int b=(r/3)*3+(c/3);
        if(row[r][j]==1||column[c][j]==1||bucket[b][j]==1)
        {
            continue;
        }

        result[r][c]=j;

        row[r][j]=1;
        column[c][j]=1;
        bucket[b][j]=1;
        c++;
        if(c>8)
        {
            r++;
            c=0;
        }

        bool res=solve(r,c);
        if(res==true)
        {
            return true;
        }
        else
        {
            c--;
            if(c<0)
            {
                r--;
                c=8;
            }

            result[r][c]=0;
            row[r][j]=0;
            column[c][j]=0;
            bucket[b][j]=0;
        }

    }
}



int  main()
{
    int i,j,k;
    for(i=0;i<=10;i++)
    {
        for(j=0;j<=10;j++)
        {
            row[i].push_back(0);
            column[i].push_back(0);
            bucket[i].push_back(0);
            result[i].push_back(0);
            sudoku[i].push_back(0);
        }
    }
    for(i=0;i<9;i++)
    {
        for(j=0;j<9;j++)
        {
            scanf("%d",&k);
            sudoku[i][j]=k;
            row[i][k]=1;
            column[j][k]=1;
            int b=((i/3)*3)+(j/3);
            bucket[b][k]=1;
        }
    }
    cout<<endl;
     solve(0,0);

     cout<<endl;
    for(i=0;i<9;i++)
    {
        for(j=0;j<9;j++)
        {
            cout<<result[i][j]<<" ";
        }
        cout<<endl;
    }

}
