#include<bits/stdc++.h>
using namespace std;

struct product
{
    int price,weight;
};

vector<int>col(10001,-1);
vector<vector<int> >testcase(101,col);
vector<vector<int> >memory(101,col);
int n,t;
int knapsack(int item,int space,vector<product>&shop)
{
    if(item>=n)
    {
        return 0;
    }
    if(testcase[item][space]==t)
    {
        return memory[item][space];
    }
    int case1=0,case2;
    if(shop[item].weight<=space)
    {
        case1=knapsack(item,space-shop[item].weight,shop)+shop[item].price;
    }
    case2=knapsack(item+1,space,shop);
    testcase[item][space]=t;
    return memory[item][space]=max(case1,case2);
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int bag,p,c,w,i,j,total=0,res;
        scanf("%d %d",&n,&bag);
        //cout<<"n="<<n<<" bag="<<bag<<endl;
        vector<product>shop;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d %d",&p,&c,&w);
            //cout<<"p="<<p<<" c="<<c<<" w="<<w<<endl;
            product tmp;
            tmp.weight=w;
            tmp.price=p;
            total+=w*c;
            shop.push_back(tmp);
        }
        //cout<<"total="<<total<<endl;
        if(total<=bag)
        {
            res=knapsack(0,bag-total,shop);
            //cout<<t<<" res="<<res<<endl;
            printf("Case %d: %d\n",t,res);
        }
        else
            printf("Case %d: Impossible\n",t);
    }
}
