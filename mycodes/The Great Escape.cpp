#include<bits/stdc++.h>
using namespace std;

int n,m;

struct grid{
    int r,c;
};
struct flowNode{
    int res_id,res_pos,w;
};

struct queNode{
    int id,flow;
};

struct parentNode{
    int id,pos;
};

bool valid(int r,int c){
    if(r>=1 && r<=n && c>=1 && c<=m) return true;
    return false;
}

void buildGraph(int totalNode,vector<vector<int> >&decode,vector<grid>&nodes,vector<vector<flowNode> >&connection,

    vector<int>&sinks,vector<int>&sources){
    int i,j;
    ///Node splitting
    for(i=1;i<=totalNode;i++)
    {
        flowNode tmp1,tmp2;
        tmp1.res_id=i+1+totalNode;
        tmp1.res_pos=connection[i+1+totalNode].size();
        tmp1.w=1;

        tmp2.res_id=i;
        tmp2.res_pos=connection[i].size();
        tmp2.w=0;
        connection[i].push_back(tmp1);
        connection[i+1+totalNode].push_back(tmp2);
    }
    ///grid connection graph
    for(i=1;i<=n;i++){
        for(j=1;j<=m;j++){
            int nxt_r,nxt_c,nxtNode,curNode;
            curNode=decode[i][j];
            nxt_r=i-1;
            nxt_c=j;
            if(valid(nxt_r,nxt_c)){
                nxtNode=decode[nxt_r][nxt_c];
                flowNode tmp1,tmp2;
                tmp1.res_id=nxtNode;
                tmp1.res_pos=connection[nxtNode].size();
                tmp1.w=1;

                tmp2.res_id=curNode+totalNode+1;
                tmp2.res_pos=connection[tmp2.res_id].size();
                tmp2.w=0;
                connection[curNode+totalNode+1].push_back(tmp1);
                connection[nxtNode].push_back(tmp2);
            }
            nxt_r=i+1;
            nxt_c=j;
            if(valid(nxt_r,nxt_c)){
                nxtNode=decode[nxt_r][nxt_c];
                flowNode tmp1,tmp2;
                tmp1.res_id=nxtNode;
                tmp1.res_pos=connection[nxtNode].size();
                tmp1.w=1;

                tmp2.res_id=curNode+totalNode+1;
                tmp2.res_pos=connection[tmp2.res_id].size();
                tmp2.w=0;
                connection[curNode+totalNode+1].push_back(tmp1);
                connection[nxtNode].push_back(tmp2);
            }
            nxt_r=i;
            nxt_c=j+1;
            if(valid(nxt_r,nxt_c)){
                nxtNode=decode[nxt_r][nxt_c];
                flowNode tmp1,tmp2;
                tmp1.res_id=nxtNode;
                tmp1.res_pos=connection[nxtNode].size();
                tmp1.w=1;

                tmp2.res_id=curNode+totalNode+1;
                tmp2.res_pos=connection[tmp2.res_id].size();
                tmp2.w=0;
                connection[curNode+totalNode+1].push_back(tmp1);
                connection[nxtNode].push_back(tmp2);
            }
            nxt_r=i;
            nxt_c=j-1;
            if(valid(nxt_r,nxt_c)){
                nxtNode=decode[nxt_r][nxt_c];
                flowNode tmp1,tmp2;
                tmp1.res_id=nxtNode;
                tmp1.res_pos=connection[nxtNode].size();
                tmp1.w=1;

                tmp2.res_id=curNode+totalNode+1;
                tmp2.res_pos=connection[tmp2.res_id].size();
                tmp2.w=0;
                connection[curNode+totalNode+1].push_back(tmp1);
                connection[nxtNode].push_back(tmp2);
            }
        }
    }

    ///add source
    for(i=0;i<sources.size();i++){
       flowNode tmp1,tmp2;
        tmp1.res_id=0;
        tmp1.res_pos=connection[0].size();
        tmp1.w=0;

        tmp2.res_id=sources[i];
        tmp2.res_pos=connection[sources[i]].size();
        tmp2.w=1;
        connection[sources[i]].push_back(tmp1);
        connection[0].push_back(tmp2);
    }
    ///add sink
    for(i=0;i<sinks.size();i++){
        flowNode tmp1,tmp2;
        tmp1.res_id=totalNode+1;
        tmp1.res_pos=connection[totalNode+1].size();
        tmp1.w=1;

        tmp2.res_id=sinks[i]+totalNode+1;
        tmp2.res_pos=connection[sinks[i]+totalNode+1].size();
        tmp2.w=0;

        connection[sinks[i]+totalNode+1].push_back(tmp1);
        connection[totalNode+1].push_back(tmp2);
    }
    return;
}
void show_Edges(int totalNode,vector<vector<flowNode> >&connection){
    int i,j;
    for(i=0;i<=2*totalNode+1;i++){
        cout<<"for node "<<i<<"---"<<endl;
        for(j=0;j<connection[i].size();j++)
        {
            cout<<"id="<<connection[i][j].res_id<<" pos="<<connection[i][j].res_pos<<" w="
            <<connection[i][j].w<<endl;
        }
    }
    return;
}

int send_flow(int source,int sink,vector<vector<flowNode> >&connection){
    //cout<<"trying send flow from "<<source<<" to "<<sink<<endl;
    int i,j,res=0;
    parentNode pnt;
    pnt.id=-1;
    vector<parentNode>parent(40000,pnt);
    queue<queNode>q;
    queNode cur;
    cur.id=source;
    cur.flow=1e9;
    q.push(cur);
    while(!q.empty()){
        cur=q.front();
        q.pop();
        if(cur.id==sink){
            res=cur.flow;
            break;
        }
        int cur_node,cur_flow,nxt_node,nxt_flow;
        cur_node=cur.id;
        cur_flow=cur.flow;
        //cout<<"cur node="<<cur_node<<" flow="<<cur_flow<<endl;
        for(i=0;i<connection[cur_node].size();i++){
            nxt_node=connection[cur_node][i].res_id;
            nxt_flow=connection[cur_node][i].w;
            //cout<<"nxt_node="<<nxt_node<<" nxt_flow="<<nxt_flow<<endl;
            if(parent[nxt_node].id==-1 && nxt_flow>0){
                //cout<<"send to this node"<<endl;
                parent[nxt_node].id=cur_node;
                parent[nxt_node].pos=i;
                nxt_flow=min(nxt_flow,cur_flow);
                queNode tmp;
                tmp.id=nxt_node;
                tmp.flow=nxt_flow;
                q.push(tmp);
            }
        }

    }
    //cout<<"res="<<res<<endl;
    //cout<<"parent of sink="<<parent[sink].id<<endl;
    if(res>0){
        int p,c,pos,r,r_pos;
        c=sink;
        //cout<<"path"<<endl;
        while(c!=source){
                //cout<<"c="<<c<<endl;
            //cout<<c<<"<=";
            p=parent[c].id;
            pos=parent[c].pos;
            //cout<<"p="<<p<<" pos="<<pos<<endl;
            r=connection[p][pos].res_id;
            r_pos=connection[p][pos].res_pos;
            //cout<<"r="<<r<<" r_pos="<<r_pos<<endl;
            connection[p][pos].w-=res;
            connection[r][r_pos].w+=res;
            c=p;
        }
        //cout<<c<<endl;
    }
    //cout << "Currently sent " << res << endl ;
    //cout<<"current situation in graph"<<endl;
    //show_Edges(9,connection);
    return res;
}




int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j;
    cin>>test;
    for(t=1;t<=test;t++){
        cin>>n>>m;
        vector<int>col1(m+1);
        vector<vector<int> >decode(n+1,col1);
        vector<int>sources;
        vector<int>sinks;
        vector<grid>nodes(2*n*m +10);
        vector<vector<flowNode> >connection(40000);
        int id=0;
        for(i=1;i<=n;i++)
        {
            string s;
            cin>>s;
            for(j=0;j<s.size();j++)
            {
                id++;
                grid tmp;
                tmp.r=i;
                tmp.c=j+1;
                decode[i][j+1]=id;
                nodes[id]=tmp;
                if(s[j]=='*'){
                    sources.push_back(id);
                }
                if(i==1 || i==n || j==0 || j==s.size()-1){
                    //cout<<"sink="<<id<<endl;
                    sinks.push_back(id);
                }
            }
        }
        //cout<<"trying to build graph"<<endl;
        buildGraph(id,decode,nodes,connection,sinks,sources);
        //cout<<"graph built"<<endl;
        //cout<<"Current Graph"<<endl;
        //show_Edges(9,connection);
        int mxflow=0,f;
        while(f=send_flow(0,id+1,connection)) mxflow+=f;
        //cout<<"mxflow="<<mxflow<<endl;
        if(mxflow==sources.size()){
            cout<<"Case "<<t<<": yes"<<endl;
        }
        else
            cout<<"Case "<<t<<": no"<<endl;
    }
}
