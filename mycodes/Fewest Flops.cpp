#include<bits/stdc++.h>
using namespace std;


int solve(int idx,int last,int n,vector<vector<int> >&container,vector<vector<int> >&memory)
{
    if(idx>n)
        return 0;
    if(memory[idx][last]!=-1)
    {
        return memory[idx][last];
    }
    int ans=2e9,cur=2e9,cur1=2e9,sz=container[idx].size(),i,j;
    if(sz==1)
    {
        if(container[idx][0]==last)
        {
            cur=solve(idx+1,last,n,container,memory);
        }
        else
        {
            cur=solve(idx+1,container[idx][0],n,container,memory)+1;
        }
        ans=min(ans,cur);
    }
    else
    {
        int lastHere=0;
        for(i=0;i<sz;i++)
        {
            if(container[idx][i]==last)
                lastHere=1;
        }
        if(lastHere)
        {
            for(i=0;i<sz;i++)
            {
                if(container[idx][i]==last)
                {
                    cur=solve(idx+1,last,n,container,memory)+sz;
                }
                else
                    cur=solve(idx+1,container[idx][i],n,container,memory)+sz-1;
                ans=min(ans,cur);
            }
        }
        else
        {
            for(i=0;i<sz;i++)
            {
                cur=solve(idx+1,container[idx][i],n,container,memory)+sz;
                ans=min(ans,cur);
            }
        }
    }
    return memory[idx][last]=ans;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j,n,k;
    string s;
    cin>>test;
    while(test--){
        cin>>k>>s;
        n=s.size()/k;
        vector<vector<int> >container(n+1);
        for(i=0;i<n;i++)
        {
            vector<int>counter(136,0);
            for(j=i*k;j<(i*k)+k;j++)
            {
                if(counter[s[j]]==0)
                {
                    counter[s[j]]=1;
                    container[i+1].push_back(s[j]-'a');
                }
            }
            //cout<<i+1<<"container size="<<container[i+1].size()<<endl;
        }
        vector<int>col(28,-1);
        vector<vector<int> >memory(n+1,col);
        int res=solve(1,27,n,container,memory);
        cout<<res<<endl;
    }
}
