#include<stdio.h>
#include<iostream>
#include<string>
#include<algorithm>
#include<vector>
#include<string.h>

using namespace std;

struct teams
{
    std::string name;
    int solved;
    int penalty;
};

bool operator <(teams a, teams b)
{
    if(a.solved!=b.solved)
    {
        return a.solved < b.solved;
    }
    return a.penalty > b.penalty;
}

int main()
{
    vector<teams>judge;
    int n,i,j;
    string s;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        teams tmp;
        int solve=0,penalty=0;
        cin>>s;
        tmp.name=s;
        for(j=1;j<=4;j++)
        {
            int sub,pe;
            scanf("%d %d",&sub,&pe);
            if(pe>0)
            {
                solve++;
                penalty+=pe+(sub-1)*20;
            }
        }
        //cout<<"penalty="<<penalty<<" solve"<<solve<<endl;
        tmp.penalty=penalty;
        tmp.solved=solve;
        judge.push_back(tmp);
    }
    sort(judge.begin(),judge.end());
    cout<<judge[n-1].name<<" "<<judge[n-1].solved<<" "<<judge[n-1].penalty<<endl;
}
