#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>memory(65,-1);


ll log_2(ll n)
{
    ll counter=0;
    while(n>1)
    {
        n/=2;
        counter++;
    }
    return counter;
}

ll count_fi_binary(ll number)
{

    //cout<<"number="<<number<<endl;
    if(number==1) return 1;
    if(number<=0) return 0;
    ll i,j,bits,total=0,nxt,nxt_bit;
    bits=log_2(number);

   //cout<<"bits="<<bits<<endl;
    if(number==((1ll<<(bits+1))-1))
    {
        //cout<<"checking if i have"<<endl;
        if(memory[bits]!=-1){
        //cout<<"memorize  "<<number<<" = "<<memory[bits]<<endl;
        return memory[bits];
        }
    }
    if(bits>1){
    ll tmp=(1ll<<(bits-1))-1;
    if((number&(1ll<<(bits-1)))==(1ll<<(bits-1)))
    {
        nxt=(1ll<<(bits-1))-1;
    }
    else
    nxt=(number&tmp);
    }
    else
        nxt=0;
    //cout<<"diving "<<number<<" into "<<(1ll<<bits)-1<<" and "<<nxt<<endl;
    total=count_fi_binary(((1ll<<bits)-1))+1;
    total+=count_fi_binary(nxt);

    if(number==((1ll<<(bits+1))-1))
    {
        memory[bits]=total;
    }
    //cout<<"number ="<<number<<" ans="<<total<<endl;
    return total;
}

ll solve(ll pos)
{
    ll first,last,mid,res,f=0,cur;
    first=1;
    last=2e15;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;

        mid=(first+last)/2;
        cur=count_fi_binary(mid);
        //cout<<"-------------------------total in "<<mid<<" is "<<cur<<" first="<<first<<" last="<<last<<endl;
        if(cur>=pos)
        {
            if(cur==pos)
            {
                res=mid;
            }
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}



int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll test,t,n,i,j,res;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld",&n);
        res=solve(n);
        //cout<<"res="<<res<<endl;
        printf("Case %lld: ",t);
        if(res<=0ll)
        {
            printf("0\n");
            continue;
        }
        for(i=log2(res);i>=0;i--)
        {
            if((res&(1ll<<i))==0)
                printf("0");
            else
                printf("1");
        }
        printf("\n");
    }
}
