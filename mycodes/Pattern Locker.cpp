#include<bits/stdc++.h>
using namespace std;
#define ll unsigned long long int
vector<ll>col(10001);
vector<vector<ll> >multiple(10001,col);

void preprocess()
{
    ll i,j,cur;
    for(i=1;i<=10000;i++)
    {
        cur=1;
        for(j=i;j<=10000;j++)
        {
            cur*=j;
            cur%=10000000000007;
            multiple[i][j]=cur;

        }
    }
}


int main()
{
    preprocess();
    ll t,l,m,n,i,j,test;
    scanf("%llu",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%llu %llu %llu",&l,&m,&n);
        ll last=l*l;
        ll res=0;
        for(i=m;i<=n;i++)
        {
            ll first=last-i+1;
            //cout<<"length="<<i<<" "<<first<<" "<<last<<endl;
            res+=multiple[first][last];
            res%=10000000000007;
        }
        printf("Case %llu: %llu\n",t,res);

    }

}
