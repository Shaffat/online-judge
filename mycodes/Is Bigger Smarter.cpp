#include<bits/stdc++.h>

using namespace std;

struct animal
{
    int w,iq,id;
};
bool operator <(animal a,animal b)
{
    if(a.w!=b.w)
    {
        return a.w<b.w;
    }
    return false;
}
vector<int>memory(1010,-1);
int solve(int l,int r,vector<animal>&elephants,vector<int>&path)
{
//    cout<<"l="<<l<<" r="<<r<<endl;
    if(l>r)
    {
        return 0;
    }
    if(memory[l]!=-1)
    {
        return memory[l];
    }
    int i,j,res=0,cur;
    for(i=l+1;i<=r;i++)
    {
        if(elephants[l].w<elephants[i].w && elephants[l].iq>elephants[i].iq)
        {
            //cout<<i<<" can be takem after "<<l<<endl;
            cur=solve(i,r,elephants,path);
//            cout<<"result from "<<i<<"="<<cur<<endl;
//            cout<<"current best for "<<l<<"="<<max(cur,res)<<endl;
            if(cur>res)
            {
                res=cur;
                path[l]=i;
            }
        }
    }
    return memory[l]=res+1;
}

int main()
{
    int i,j,k=0,res;
    animal tmp;
    vector<animal>v;
    tmp.w=0;
    tmp.iq=1e6;
    tmp.id=k;
    v.push_back(tmp);
    k++;
    while(scanf("%d %d",&i,&j)==2)
    {
        tmp.w=i;
        tmp.iq=j;
        tmp.id=k;
        v.push_back(tmp);
        k++;
    }
    sort(v.begin(),v.end());
    vector<int>path(v.size(),-1);
//    cout<<"-----sorted---"<<endl;
//    for(i=0;i<v.size();i++)
//    {
//        cout<<v[i].w<<" "<<v[i].iq<<endl;
//    }
    res=solve(0,v.size()-1,v,path)-1;
    printf("%d\n",res);
    int cur=path[0],nxt;
    while(cur!=-1)
    {
        printf("%d\n",v[cur].id);
        cur=path[cur];
    }

}
