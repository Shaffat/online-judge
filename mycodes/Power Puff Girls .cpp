#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int r,c;
};

bool is_valid(int n,int m,int i,int j)
{
    if(i<1 || i>n)
    {
        return false;
    }
    if(j<1 || j>m)
    {
        return false;
    }
    return true;
}

int solve(pos des,pos start,int n,int m,vector<vector<char> >&city)
{
    vector<int>col(m+100,0);
    vector<vector<int> >vis(n+100,col);
    vector<vector<int> >dis(n+100,col);
    queue<pos>q;
    vis[start.r][start.c]=1;
    q.push(start);
    while(!q.empty())
    {
        pos cur;
        cur=q.front();
        q.pop();
        if(cur.r==des.r && cur.c==des.c)
        {
            return dis[cur.r][cur.c];
        }
        pos nxt;
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(is_valid(n,m,nxt.r,nxt.c))
        {
            if(vis[nxt.r][nxt.c]==0 && city[nxt.r][nxt.c]=='.')
            {
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(is_valid(n,m,nxt.r,nxt.c))
        {
            if(vis[nxt.r][nxt.c]==0 && city[nxt.r][nxt.c]=='.')
            {
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(is_valid(n,m,nxt.r,nxt.c))
        {
            if(vis[nxt.r][nxt.c]==0 && city[nxt.r][nxt.c]=='.')
            {
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(is_valid(n,m,nxt.r,nxt.c))
        {
            if(vis[nxt.r][nxt.c]==0 &&city[nxt.r][nxt.c]=='.')
            {

                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
    }

}

int main()
{
   int test,t=1;
   scanf("%d",&test);
   while(t<=test)
   {
       int n,m,i,j,res1,res2,res3;
       pos a,b,c,h;
       scanf("%d %d",&n,&m);
       vector<char>col(n+100);
       vector<vector<char> >city(m+100,col);
       for(i=1;i<=n;i++)
       {
           for(j=1;j<=m;j++)
           {
               char ch;
               scanf(" %c",&ch);
               if(ch=='a')
               {
                   a.r=i;
                   a.c=j;
                   city[i][j]='.';
               }
               else if(ch=='b')
               {
                   b.r=i;
                   b.c=j;
                   city[i][j]='.';
               }
               else if(ch=='c')
               {
                   c.r=i;
                   c.c=j;
                   city[i][j]='.';
               }
               else if(ch=='h')
               {
                   h.r=i;
                   h.c=j;
                   city[i][j]='.';
               }
               else
               city[i][j]=ch;
           }
       }
       res1=solve(h,a,n,m,city);
       res2=solve(h,b,n,m,city);
       res3=solve(h,c,n,m,city);
       int f_res=max(res1,max(res2,res3));
       printf("Case %d: %d\n",t,f_res);
       t++;
   }
}
