#include<bits/stdc++.h>
using namespace std;

vector<int>parent(1e4+1);
vector<int>start(1e4+1);
vector<int>low(1e4+1);
vector<int>vis(1e4+1,0);

int tim;
int root,t=0;
void find_AP(int node,vector<int>&AP,vector<vector<int> >&connection)
{
    int i,j,children=0,next;
    tim++;
    start[node]=low[node]=tim;
    for(i=0;i<connection[node].size();i++)
    {
        next=connection[node][i];
        if(next==parent[node])
        {
            continue;
        }
        if(vis[next]!=t)
        {
            ///forward edge
            vis[next]=t;
            parent[next]=node;
            find_AP(next,AP,connection);
            if((start[node]<=low[next]) && node!=root)
            {
                AP[node]=1;
            }
            low[node]=min(low[node],low[next]);
            children++;
        }
        else
        {
            ///back edge
            low[node]=min(low[node],start[next]);
        }

    }
    if(children>1 && node==root)
    {
        AP[node]=1;
    }
    return;
}
int solve(int n,int m)
{
    t++;
    int i,j,u,v,res=0;
    vector<vector<int> >connection(n+1);
    vector<int>AP(n+1,0);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }
    tim=0;
    root=1;
    parent[1]=1;
    vis[1]=t;
    find_AP(1,AP,connection);
    for(i=1;i<=n;i++)
    {
        if(AP[i]==1)
        {
            res++;
        }
    }

    return res;
}
int main()
{
    int n,m,t=1;
    while(scanf("%d %d",&n,&m))
    {
        if(n==0 && m==0)
        {
            return 0;
        }
        int res=solve(n,m);
        printf("%d\n",res);
    }
}
