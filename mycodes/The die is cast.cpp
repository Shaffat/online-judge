#include<bits/stdc++.h>
using namespace std;
struct node
{
    int r,c;
};

int row,col;
bool is_valid(node n)
{
    if(n.r<0 || n.r>=row || n.c<0 || n.c>=col)
    {
        return false;
    }
    return true;
}
void dice_grid(vector<node>&v,node root,vector<vector<bool> >&vis,vector<string>&grid)
{
    queue<node>q;
    q.push(root);
    vis[root.r][root.c]=1;
    while(!q.empty())
    {
        node cur=q.front(),nxt;
        q.pop();
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && (grid[nxt.r][nxt.c]=='*'||grid[nxt.r][nxt.c]=='X'))
            {
                vis[nxt.r][nxt.c]=1;
                if(grid[nxt.r][nxt.c]=='X')
                {
                    v.push_back(nxt);
                }
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && (grid[nxt.r][nxt.c]=='*'||grid[nxt.r][nxt.c]=='X'))
            {
                vis[nxt.r][nxt.c]=1;
                if(grid[nxt.r][nxt.c]=='X')
                {
                    v.push_back(nxt);
                }
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && (grid[nxt.r][nxt.c]=='*'||grid[nxt.r][nxt.c]=='X'))
            {
                vis[nxt.r][nxt.c]=1;
                if(grid[nxt.r][nxt.c]=='X')
                {
                    v.push_back(nxt);
                }
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && (grid[nxt.r][nxt.c]=='*'||grid[nxt.r][nxt.c]=='X'))
            {
                vis[nxt.r][nxt.c]=1;
                if(grid[nxt.r][nxt.c]=='X')
                {
                    v.push_back(nxt);
                }
                q.push(nxt);
            }
        }
    }
    return;
}
void traverse(node root,vector<string>&grid,vector<vector<bool> >&vis)
{
    queue<node>q;
    q.push(root);
    vis[root.r][root.c]=1;
    while(!q.empty())
    {
        node cur,nxt;
        cur=q.front();
        q.pop();
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && grid[nxt.r][nxt.c]=='X')
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && grid[nxt.r][nxt.c]=='X')
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && grid[nxt.r][nxt.c]=='X')
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(is_valid(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && grid[nxt.r][nxt.c]=='X')
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
    }
}
int value_count(vector<node>&v,vector<vector<bool> >&vis,vector<string>&grid)
{
    int i,j,counter=0;
    for(i=0;i<v.size();i++)
    {
        node cur=v[i];
        if(vis[cur.r][cur.c]==0)
        {
            counter++;
            traverse(cur,grid,vis);
        }
    }
    return counter;
}

void solve(vector<string>&image,vector<int>&v)
{
    int i,j,res;
    vector<bool>col1(col,0);
    vector<vector<bool> >vis1(row,col1);
    vector<vector<bool> >vis2(row,col1);
    for(i=0;i<row;i++)
    {
        for(j=0;j<col;j++)
        {
            if((image[i][j]=='X' || image[i][j]=='*' )&& vis1[i][j]==0)
            {
                    node tmp;
                    tmp.r=i;
                    tmp.c=j;
                    vector<node>all_X;
                    if(image[i][j]=='X')
                    {
                        all_X.push_back(tmp);
                    }
                    dice_grid(all_X,tmp,vis1,image);
                    v.push_back(value_count(all_X,vis2,image));
            }
        }
    }
    return;
}

int main()
{
    int t=1;
    ios_base::sync_with_stdio(0);
    while(cin>>col>>row)
    {
        if(t>1)
        {
            cout<<endl;
        }
        if(row==0 && col==0)
        {
            break;
        }
        int i,j;
        vector<string>img;
        vector<int>res;
        for(i=1;i<=row;i++)
        {
            string s;
            cin>>s;
            img.push_back(s);
        }
        solve(img,res);
        sort(res.begin(),res.end());
        cout<<"Throw "<<t<<endl;
        for(i=0;i<res.size();i++)
        {
            if(i<res.size()-1)
            cout<<res[i]<<" ";
            else
                cout<<res[i]<<endl;
        }
        t++;
    }
}
