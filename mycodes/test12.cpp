#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int r,c;
};
int R,C,casecounter=1;
vector<int>col(31,0);
vector<vector<int> >row(31,col);
vector<vector<vector<int> > >testcase(20,row);

vector<bool>col1(31,0);
vector<vector<bool> >row1(31,col1);
vector<vector<vector<bool> > >memory(20,row1);

bool valid(int r,int c)
{
    cout<<"R="<<R<<" C="<<C<<endl;
    if(r>=0 && r<R && c>=0 && c<C)
    {
        return true;
    }
    cout<<"invalid access"<<endl;
    return false;
}

bool solve(int index,int r,int c,string &name,vector<string>&maze,vector<vector<pos> >&path)
{
    cout<<"index="<<index<<" r="<<r<<" c="<<c<<endl;
    if(index>=name.size() || !valid(r,c))
    {
        cout<<"fuck off"<<endl;
        return false;
    }
    if(maze[r][c]!=name[index])
    {
        return false;
    }
    cout<<"matched "<<name[index]<<" with "<<maze[r][c]<<endl;
    if(index==name.size()-1)
    {
        return true;
    }
    if(testcase[index][r][c]==casecounter)
    {
        return memory[index][r][c];
    }
    int i,j,chk=0,nxt_r,nxt_c;
    nxt_r=r+1;
    nxt_c=c+1;
    if(solve(index+1,nxt_r,nxt_c,name,maze,path))
    {
        pos tmp;
        tmp.r=nxt_r;
        tmp.c=nxt_c;
        path[r][c]=tmp;
        chk=1;
        return memory[index][r][c]=1;
    }
    if(!chk)
    {
        nxt_r=r+1;
        nxt_c=c;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r+1;
        nxt_c=c-1;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r;
        nxt_c=c+1;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r;
        nxt_c=c;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r;
        nxt_c=c-1;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r-1;
        nxt_c=c+1;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r-1;
        nxt_c=c;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        nxt_r=r-1;
        nxt_c=c-1;
        if(solve(index+1,nxt_r,nxt_c,name,maze,path))
        {
            pos tmp;
            tmp.r=nxt_r;
            tmp.c=nxt_c;
            path[r][c]=tmp;
            chk=1;
            return memory[index][r][c]=1;
        }
    }
    if(!chk)
    {
        return memory[index][r][c]=0;
    }
}

string get_direction(pos a, pos b)
{
    if(b.r==a.r && b.c==a.c)
    {
        return "*";
    }
    if(b.r==a.r && b.c==a.c+1)
    {
        return "R";
    }
    if(b.r==a.r && b.c==a.c-1)
    {
        return "L";
    }


    if(b.r==a.r+1 && b.c==a.c)
    {
        return "D";
    }
    if(b.r==a.r+1 && b.c==a.c+1)
    {
        return "DR";
    }
    if(b.r==a.r+1 && b.c==a.c-1)
    {
        return "DL";
    }


    if(b.r==a.r-1 && b.c==a.c)
    {
        return "U";
    }
    if(b.r==a.r-1 && b.c==a.c+1)
    {
        return "UR";
    }
    if(b.r==a.r-1 && b.c==a.c-1)
    {
        return "UL";
    }
}


int main()
{
    int test,t,i,j,q;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>R>>C;
        string s;
        vector<string>maze;
        for(i=1;i<=R;i++)
        {
            cin>>s;
            maze.push_back(s);
        }
        cin>>q;

        for(int l=1;l<=q;i++)
        {
            casecounter++;
            cin>>s;
            pos cur;
            cur.r=-1;
            cur.c=-1;
            vector<pos>col(C+1,cur);
            vector<vector<pos> >path(R+1,col);
            int chk=0;
            for(i=0;i<R;i++)
            {
                for(j=0;j<C;j++)
                {
                    if(solve(0,i,j,s,maze,path))
                    {
                        chk=1;
                        cout<<s<<" found: ("<<i+1<<","<<j+1<<")";
                        cur=path[i][j];
                        pos pre;
                        pre.r=i;
                        pre.c=j;
                        while(cur.c!=-1)
                        {
                            cout<<", "<<get_direction(pre,cur);
                            pre.r=cur.r;
                            pre.c=cur.c;
                            cur=path[pre.r][pre.c];
                        }
                        cout<<endl;
                        break;
                    }
                }
                if(chk)
                {
                    break;
                }
            }
            if(!chk)
            {
                cout<<s<<" not found"<<endl;
            }
        }
    }
}
