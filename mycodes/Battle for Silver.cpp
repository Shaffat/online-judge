#include<bits/stdc++.h>
using namespace std;

int t;

int solve(int n,int node,int silver,int p1,int p2,int p3,vector<vector<int> >&connection
          ,vector<int>&total,vector<int>&load,vector<int>&start,vector<int>&vis)
{
    t++;
    start[node]=t;
    int i,j,child,cyclelen,res=load[node];
    total[node]=load[node]+silver;
    cout<<"node="<<node<<" silver="<<silver<<" curent silver="<<total[node]<<" p1="<<p1<<" p2="<<p2<<" p3="<<p3<<endl;


    for(i=1;i<=n;i++)
    {
        if(connection[node][i]==0) continue;

        child=i;
        if(vis[child]==1){
            cyclelen=start[node]-start[child]+1;
            cout<<"start of "<<child<<" is "<<start[child]<<" and "<<node<<" is "<<start[node]<<endl;
            if(cyclelen<4){
                cout<<cyclelen<<" length cycle from "<<child<<" to "<<node<<" cost "<<total[node]-total[child]<<endl;
                res=max(res,total[node]-total[child]);
            }
            else if(cyclelen==4){
                cout<<"four length cycle from "<<child<<" to "<<node<<" cost "<<total[node]-total[child]<<endl;
                if(connection[p1][p2]==1 && connection[p1][p3]==1 && connection[p1][node]==1 &&
                   connection[p2][p3]==1 && connection[p2][node]==1 && connection[p3][node]==1){
                    res=max(res,total[node]-total[child]);
                   }
            }

        }
        else
        {
            vis[child]=1;
            int res1=solve(n,child,total[node],p2,p3,node,connection,total,load,start,vis);
            res=max(res1,res);

        }
    }
    vis[node]=2;
    return res;
}

int main()
{
    int v,e,i,j,w,x,y;
    while(scanf("%d %d",&v,&e)!=EOF)
    {
        vector<int>start(v+10,0);
        vector<int>vis(v+10,0);
        vector<int>load(v+10,0);
        vector<int>total(v+10,0);
        vector<int>col(v+10);
        vector<vector<int> >connection(v+10,col);
        for(i=1;i<=v;i++)
        {
            scanf("%d",&j);
            load[i]=j;
        }
        for(i=1;i<=e;i++)
        {
            scanf("%d %d",&x,&y);
            connection[x][y]=1;
            connection[y][x]=1;
        }
        t=0;
        vis[1]=1;
        int ans=solve(v,1,0,-1,-1,-1,connection,total,load,start,vis);
        printf("%d\n",ans);
    }
}
