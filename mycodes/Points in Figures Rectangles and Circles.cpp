#include<bits/stdc++.h>
using namespace std;
struct fig
{
    int id;
    double x1,x2,y1,y2,r;
};
bool inside(double px,double py, fig figure)
{
   if(figure.id)
   {
         if(px>figure.x1 && px<figure.x2 && py<figure.y1 && py>figure.y2)
        {
            return true;
        }
   }
   else
   {
       double dis=(px-figure.x1)*(px-figure.x1)+(py-figure.y1)*(py-figure.y1);
       if(dis<figure.r*figure.r)
       {
           return true;
       }
   }
    return false;
}
int main()
{
   vector<fig>figure;
   char c,c1;
   while(scanf("%c",&c))
   {
       if(c=='*')
       {
           break;
       }
       fig tmp;
       if(c=='r')
       {
           double x1,y1,x2,y2;
           scanf("%lf %lf %lf %lf",&x1,&y1,&x2,&y2);
           //cout<<"r x1="<<x1<<" "<<y1<<" "<<x2<<" "<<y2<<endl;
           tmp.id=1;
           tmp.x1=x1;
           tmp.x2=x2;
           tmp.y1=y1;
           tmp.y2=y2;
       }
       else if(c=='c')
       {
           double x1,y1,r;
           scanf("%lf %lf %lf",&x1,&y1,&r);
           //cout<<"c x1="<<x1<<" y1="<<y1<<" r="<<r<<endl;
           tmp.id=0;
           tmp.r=r;
           tmp.x1=x1;
           tmp.y1=y1;
       }
       figure.push_back(tmp);
       //cout<<"size="<<figure.size()<<endl;
       getchar();
   }
   int i;
//   for(i=0;i<figure.size();i++)
//   {
//       if(figure[i].id==1)
//       {
//           cout<<"x1="<<figure[i].x1<<" "<<figure[i].x2<<" "<<figure[i].y1<<" "<<figure[i].y2<<endl;
//       }
//       else
//       {
//           cout<<"x1="<<figure[i].x1<<" "<<figure[i].y1<<" "<<figure[i].r<<endl;
//       }
//   }
   double x1,y1;
   int counter=0;
   while(scanf("%lf %lf",&x1,&y1))
   {
       if(x1==9999.9 && y1==9999.9)
       {
           break;
       }
       counter++;
       int chk=0,i;
       for(i=0;i<figure.size();i++)
       {
           if(inside(x1,y1,figure[i]))
           {
               chk=1;
               printf("Point %d is contained in figure %d\n",counter,i+1);
           }
       }
       if(!chk)
       {
           printf("Point %d is not contained in any figure\n",counter);
       }
   }

}
