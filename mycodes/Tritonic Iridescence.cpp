#include<bits/stdc++.h>
using namespace std;

bool is_valid(string s)
{
    int i,j;
    char pre=s[0];
    for(i=1;i<s.size();i++)
    {
        if(pre==s[i] && pre!='?')
        {
            return false;
        }
        pre=s[i];
    }
    return true;
}

bool is_possible(string s)
{
    int i,j;
    if(s[0]=='?'||s[s.size()-1]=='?')
    {
        return true;
    }
    for(i=1;i<s.size()-1;i++)
    {
        if(s[i]=='?')
        {
            //cout<<"i="<<i<<" next="<<s[i]<<endl;
            if(s[i+1]=='?')
            {
                return true;
            }
            if(s[i-1]==s[i+1])
            {
                return true;
            }
        }
    }
    return false;
}
int main()
{
    string s;
    int n;
    cin>>n>>s;
    if(is_valid(s))
    {
        if(is_possible(s))
        {
            cout<<"Yes"<<endl;
        }
        else
            cout<<"No"<<endl;
    }
    else
        cout<<"No"<<endl;
}
