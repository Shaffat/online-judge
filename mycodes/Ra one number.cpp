#include<bits/stdc++.h>

using namespace std;

string ConvertAndAdjust(int k)
{
    if(k==0)
    {
        return "0";
    }
    string nw="";
    while(k!=0)
    {
        int i=k%10;
        k/=10;
        nw.push_back('0'+i);
    }
    return nw;
}
int solve(string s,int index,int diff,int sign,bool restriction,vector<vector<vector<int> > >&memory)
{
    if(index<0)
    {
        if(diff==1 && sign==1)
        {
            return 1;
        }
        return 0;
    }
    if(memory[index][diff][sign]!=-1 && restriction==0)
    {
        return memory[index][diff][sign];
    }
    int i,j,realdiff,result=0,odd;
    odd=(index+1)%2;
    if(sign)
    {
        realdiff=diff;
    }
    else
        realdiff=diff*-1;

    if(restriction)
    {
        int limit=s[index]-'0';
        for(i=0; i<=limit; i++)
        {
            int curdif;
            if(i<limit)
            {
                if(odd)
                {
                    curdif=realdiff-i;
                }
                else
                    curdif=realdiff+i;
                if(curdif>0)
                {
                    result+=solve(s,index-1,abs(curdif),1,0,memory);
                }
                else
                    result+=solve(s,index-1,abs(curdif),0,0,memory);
            }
            else
            {
                if(odd)
                {
                    curdif=realdiff-i;
                }
                else
                    curdif=realdiff+i;
                if(curdif>0)
                {
                    result+=solve(s,index-1,abs(curdif),1,1,memory);
                }
                else
                    result+=solve(s,index-1,abs(curdif),0,1,memory);
            }
        }
    }
    else
    {
        for(i=0; i<=9; i++)
        {
            int curdif;
            if(odd)
            {
                curdif=realdiff-i;
            }
            else
                curdif=realdiff+i;
            if(curdif>0)
            {
                result+=solve(s,index-1,abs(curdif),1,0,memory);
            }
            else
                result+=solve(s,index-1,abs(curdif),0,0,memory);
        }
    }
    if(restriction)
    {
        return result;
    }
    else
    {
        return memory[index][diff][sign]=result;
    }

}
int main()
{
    int test;
    vector<int>sign(2,-1);
    vector< vector<int> >diff(50,sign);
    vector< vector <vector<int> > >memory(10,diff);
    scanf("%d",&test);
    while(test--)
    {
        int a,b;
        scanf("%d %d",&a,&b);
        a--;
        string s1,s2;
        s1=ConvertAndAdjust(a);
        s2=ConvertAndAdjust(b);
        printf("%d\n",solve(s2,s2.size()-1,0,1,1,memory)-solve(s1,s1.size()-1,0,1,1,memory));


    }
}
