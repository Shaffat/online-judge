
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())using namespace std;

ll findPos(ll t,ll h)
{
    ll dir,p;
    dir=t/h;
    if(dir%2)
    {
        p=h-(t%h);
    }
    else
        p=t%h;
    return p;
}


int main()
{
    ll n,i,j,h,test,t;
    sc1(test);
    FOR(t,1,test)
    {
        sc1(n);
        vl heights;
        FOR(i,1,n)
        {
            sc1(j);
            heights.push_back(j);
        }
        ll time=0;
        FOR(i,0,n-2)
        {
          ll curPOs=findPos(time,heights[i]),nxtPos=findPos(time,heights[i+1]);
          ll curDir=(time/heights[i])%2;
          ll nxtDir=(time/heights[i])%2;
          //cout<<"at time="<<time<<" "<<i<<"th elevtor is at "<<curPOs<<" "<<i+1<<"th elevator at "<<nxtPos<<endl;
          if(curPOs>=nxtPos)
          {
              //cout<<i<<"th lift is high, jump"<<endl;
              time++;
          }
          else
          {
              if()
              time+=heights[i+1]-nxtPos+(heights[i+1]-heights[i]);
              curPOs=findPos(time,heights[i]);
              nxtPos=heights[i];
              //cout<<"after adjusting, at time"<<time<<" curpos="<<curPOs<<" nxtpos="<<nxtPos<<endl;
              ll meetPos=ceil(double(curPOs+nxtPos)/double(2));
              //cout<<"meetpoint="<<meetPos<<endl;
              time+=abs(curPOs-meetPos)+1;
              //cout<<"jmp at time"<<time<<endl;
          }
        }
        printf("Case %lld: %lld\n",t,time);
    }
}
