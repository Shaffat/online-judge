#include<bits/stdc++.h>
using namespace std;


int main()
{
    double r,R,PI=3.141592653589793238462;
    int test,t=1,n,i,j;
    scanf("%d",&test);
    while(t<=test)
    {
        double first ,last,mid,res,required;
        scanf("%lf %d",&R,&n);
        ///get the maximum angle for maximum radius
        double angle=360/(n*2.0);
        //cout<<angle<<endl;
        double sineofmax=sin(angle*PI/180.0);
        //cout<<sineofmax<<endl;
        first=0;
        last=R;
        int d=0;
        while(first<=last)
        {
            if(d==1)
            {
                break;
            }
            if(first==last)
            {
                d=1;
            }
            mid=(first+last)/2.0;
            ///get the sine of current radius
            double currentangle=mid/((R-mid));
            if(abs(currentangle-sineofmax)<0.00000001)
            {
                res=mid;
                break;
            }
            else if(currentangle<sineofmax)
            {
                first=mid;
            }
            else
                last=mid;
        }

        printf("Case %d: %.7lf\n",t,res);
        t++;

    }
}
