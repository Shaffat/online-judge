#include<vector>
#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
using namespace std;

int solve(int T,int m,int tree,int w,vector<vector<int> >&apple,vector<vector<vector<int> > >&memory){

    if(m>T) return 0;

    if(memory[tree][m][w]!=-1){
        return memory[tree][m][w];
    }
    int res1=0,res2=0;
    if(w>0){
        res1=solve(T,m+1,(tree+1)%2,w-1,apple,memory)+apple[m][tree];
    }
    res2=solve(T,m+1,tree,w,apple,memory)+apple[m][tree];

    return memory[tree][m][w]=max(res1,res2);

}


int main(){
    int T,w,i,j,res;
    scanf("%d %d",&T,&w);
    vector<int>col(2,0);
    vector<vector<int> >apple(T+1,col);
    for(i=1;i<=T;i++){
        scanf("%d",&j);
        apple[i][j%2]=1;
    }
    vector<int>col1(w+1,-1);
    vector<vector<int> >col2(T+1,col1);
    vector<vector<vector<int> > >memory(2,col2);
    res=solve(T,0,1,w,apple,memory);
    printf("%d\n",res);


}
