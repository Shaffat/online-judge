#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<int>v;
    for(i=1;i<=n;i++){
        scanf("%d",&j);
        v.push_back(j);
    }
    vector<int>total(n+1,0);
    for(i=0;i<v.size();i++){
        int ok=1;
        if(i+1<v.size() && i+2<v.size()){
            if(v[i]>=v[i+1] && v[i+1]>=v[i+2]){
                ok=0;

            }
             if(v[i]<=v[i+1] && v[i+1]<=v[i+2]){
                ok=0;

            }
        }
        if(ok)
            total[i]=1;
    }
    int mx=0,cur=0;
    for(i=0;i<v.size();i++){
        if(total[i]==0){
            mx=max(mx,cur);
            cur=0;
        }
        else{
            cur++;
        }
    }
    cout<<cur<<endl;
}
