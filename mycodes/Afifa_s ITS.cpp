#include<bits/stdc++.h>
using namespace std;
#define ll long long int

struct node
{
    ll val;
};

bool operator < (node a, node b)
{
    if(a.val!=b.val)
        return a.val>b.val;
    return false;
}

ll solve(ll n,vector<vector<ll> >&connection,vector<ll>&edges)
{
    ll i,j,totalEdge=n-1,res=0,mn=2e9;
    priority_queue<node>q;
    for(i=1;i<=n;i++)
    {
        if(edges[i]==1)
        {
            node tmp;
            tmp.val=i;
            q.push(tmp);
        }
    }

    while(!q.empty())
    {
        ll cur=q.top().val;
        q.pop();
        res=max(res,cur*totalEdge);
        edges[cur]=0;
        for(i=0;i<connection[cur].size();i++)
        {
            ll nxt=connection[cur][i];
            if(edges[nxt]>0)
            {
                edges[nxt]--;
                totalEdge--;
            }
            if(edges[nxt]==1)
            {
                node tmp;
                tmp.val=nxt;
                q.push(tmp);
            }
        }

    }
    return res;
}

int main()
{
    ll n,i,j,m,u,v,res;
    scanf("%lld",&n);
    vector<vector<ll> >connection(n+1);
    vector<ll>edges(n+1,0);
    for(i=1;i<n;i++)
    {
        scanf("%lld %lld",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
        edges[u]++;
        edges[v]++;
    }
    res=solve(n,connection,edges);
    printf("%lld\n",res);

}
