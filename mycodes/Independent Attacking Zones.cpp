#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll solve(ll start,ll ending,string &s,vector<vector<ll> >&memory)
{
    if(ending<start)
    {
        return 1;
    }
    if(memory[start][ending]!=-1)
    {
        return memory[start][ending];
    }
    ll res=0,i,j,red=0;
    for(i=start+1;i<ending;i+=3)
    {
        for(j=i+1;j<=ending;j+=3)
        {
            if(s[start]=='R')
               red=1;
            else
              red=0;
            if(s[i]=='R')
               red++;
            if(s[j]=='R')
               red++;
            if(red<2)
            {
                ll cur=solve(start+1,i-1,s,memory)*solve(i+1,j-1,s,memory)
                *solve(j+1,ending,s,memory);
                res+=cur;
            }
        }
    }
    return memory[start][ending]=res;
}

int main()
{
    ll test,t,i,j;
    cin>>test;
    string s;
    for(t=1;t<=test;t++)
    {
       cin>>s;
       vector<ll>col(71,-1);
       vector<vector<ll> >memory(71,col);
       ll res=solve(0,s.size()-1,s,memory);
       printf("Case %lld: %lld\n",t,res);
    }
}
