
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())
#define mod 1000000007
ll nine(200000,1);

ll solve(ll d,ll m,ll n)
{
    if(d+m>=n) return 0;
    ll p=ceil(double(n-(2*(d+m)))/2);
    if(d<=1 && m==1)
    {
        return (((9*9)%mod)*nine[p])%mod;
    }
//    if(d==2 && m==1)
//    {
//
//    }
    if(d==3 && m==1)
    {
        if(n==5 || n==6)
        {
            return 2*9*9;
        }
        else if(n>=7)
        {
            return (2*9*9*9+1)*nine[p];
        }
    }
    if(d==1 && m==2)
    {
        return ((9*(2*6))%mod*nine[p])%mod;
    }
    if(d==2 && m==2)
    {
        if(n==5)
        {
            return 9*6*2;
        }
        else if(n==6)
        {
            return 9*9*2;
        }
        else if (n==7)
        {
            return 9*9*2*6;
        }
        else
            return (((9*9*2*6)%mod)*nine[p])%mod;
    }
    if(d==3 && m==2)
    {
        if(n==6)
        {
            return 2*6*2;
        }
        else if(n==7)
    }
}

int main()
{

}
