
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct pos
{
    ll x,y;
};
bool operator <(pos a,pos b)
{
    if(a.x!=b.x)
        return a.x<b.x;
    return false;
}

void buildTree(ll node,ll s,ll e,vl &v,vl &tree )
{
    if(s==e)
    {
        tree[node]=v[s];
        return;
    }
    ll mid=(s+e)/2,left,right;
    left=2*node;
    right=2*node+1;
    buildTree(left,s,mid,v,tree);
    buildTree(right,mid+1,e,v,tree);
    tree[node]=max(tree[left],tree[right]);
    return;
}

ll query(ll node,ll s,ll e,ll q_s,ll q_e,vl &tree)
{
    if(q_s>e || q_e<s)
        return 0;
    if(s>=q_s && e<=q_e)
        return tree[node];
    ll mid=(s+e)/2,left,right,l,r;
    left=2*node;
    right=2*node+1;
    l=query(left,s,mid,q_s,q_e,tree);
    r=query(right,mid+1,e,q_s,q_e,tree);
    return max(l,r);
}

int main()
{
    ll n,i,j,x,y;
    double res=0;
    vector<pos>v;
    sc1(n);
    vl height;
    vl tree(4*n);
    vl maximum(n);
    FOR(i,1,n)
    {
        sc2(x,y);
        if(x<y) swap(x,y);
        pos tmp;
        tmp.x=x;
        tmp.y=y;
        double xt,yt;
        xt=x;
        yt=y;
        double tmp1=(xt*yt)/2;
        //cout<<"tmp1="<<tmp1<<endl;
        res=max(res,tmp1);
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    //cout<<"sorted"<<endl;
    maximum[n-1]=v[n-1].y;
    for(i=n-2;i>=0;i--)
    {
        maximum[i]=max(maximum[i+1],v[i].y);
    }
    ROF(i,n-2,0)
    {
        ll w=v[i].x;
        //ll h=query(1,0,n-1,i+1,n-1,tree);
        //cout<<"w="<<w<<" h="<<v[i].y<<" query="<<maximum[i+1]<<endl;
        ll h=min(maximum[i+1],v[i].y);
        double xt,yt;
        xt=w;
        yt=h;
        res=max(xt*yt,res);
        //cout<<"res="<<res<<endl;
    }
    printf("%.1lf\n",res);

}
