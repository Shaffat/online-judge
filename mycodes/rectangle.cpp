#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,r,c,n;
    scanf("%d %d",&r,&c);
    vector<int>col(c+1);
    vector<vector<int> >box(r+1,col);
    vector<int>rowzero(r+1,0);
    vector<int>columnzero(c+1,0);
    vector<int>rowone(r+1,0);
    vector<int>columnone(c+1,0);
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            int k;
            scanf("%d",&k);
            if(k==0)
            {
                rowzero[i]+=1;
                columnzero[j]+=1;
            }
            else
            {
                rowone[i]+=1;
                columnone[j]+=1;
            }
        }
    }

    unsigned long long int total=0;
    for(i=1;i<=r;i++)
    {
        total+=(1LL<<rowzero[i])-1+(1LL<<rowone[i])-1;
    }
    for(i=1;i<=c;i++)
    {
        total+=(1LL<<columnone[i])-1+(1LL<<columnzero[i])-1;
    }
    cout<<total-(r*c)<<endl;
}
