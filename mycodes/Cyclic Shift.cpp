
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,i,j,n;
    cin>>test;
    string a,b;
    while(test--)
    {
        cin>>n>>a>>b;
        vector<ll>pos;
        for(i=0;i<a.size();i++)
        {
            if(a[i]!=b[i]){
                pos.push_back(i);
                //cout<<"mismatch at "<<i<<endl;
            }
        }
        string res=b;
        for(i=0;i<pos.size();i++)
        {
            //cout<<"i="<<i;
            j=i+1;
            ll p=pos[i];
            if(j>=pos.size())
                j=0;
            //cout<<" j="<<j;
            ll c=pos[j];
            //cout<<" p="<<p<<" c="<<c<<endl;
            res[c]=b[p];
            //cout<<"res="<<res<<endl;
        }
        //cout<<"res="<<res<<endl;
        if(a==res)
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
}

