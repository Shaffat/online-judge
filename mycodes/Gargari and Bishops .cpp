#include<bits/stdc++.h>
using namespace std;

struct detail
{
    int left,right;
};

void left_cut(int n,vector<vector<long long int> >&chess,vector<vector<detail> >&matrix,vector<long long int>&left_diagonal)
{
    int counter=0,i,j;
    long long int sum;
    for(i=1;i<=n;i++)
    {
        counter++;
        sum=0;
        int cur_row=1,cur_col=i;
        while(cur_col>0 && cur_row<=n)
        {
            sum+=chess[cur_row][cur_col];
            matrix[cur_row][cur_col].left=counter;
            cur_row++;
            cur_col--;
        }
        left_diagonal[counter]=sum;
    }
    for(i=2;i<=n;i++)
    {
        counter++;
        sum=0;
        int cur_row=i,cur_col=n;
        while(cur_col>0 && cur_row<=n)
        {
            sum+=chess[cur_row][cur_col];
            matrix[cur_row][cur_col].left=counter;
            cur_row++;
            cur_col--;
        }
        left_diagonal[counter]=sum;
    }
    return;
}

void right_cut(int n,vector<vector<long long int> >&chess,vector<vector<detail> >&matrix,vector<long long int>&right_diagonal)
{
    int counter=0,i,j,x1,y1,x2,y2;
    long long int sum;
    for(i=1;i<=n;i++)
    {
        counter++;
        sum=0;
        int cur_row=n,cur_col=i;
        while(cur_col>0 && cur_row>0)
        {
            sum+=chess[cur_row][cur_col];
            matrix[cur_row][cur_col].right=counter;
            cur_row--;
            cur_col--;
        }
        right_diagonal[counter]=sum;
    }
    for(i=1;i<n;i++)
    {
        counter++;
        sum=0;
        int cur_row=i,cur_col=n;
        while(cur_col>0 && cur_row>0)
        {
            sum+=chess[cur_row][cur_col];
            matrix[cur_row][cur_col].right=counter;
            cur_row--;
            cur_col--;
        }
        right_diagonal[counter]=sum;
    }
    return;
}

int main()
{
    long long int n,i,j,k,odd=-1,even=-1,cur,pre,x1,x2,y1,y2;
    cin>>n;
    vector<long long int>col(n+1);
    vector<vector<long long int> >chess(n+1,col);
    vector<detail>col1(n+1);
    vector<vector<detail> >matrix(n+1,col1);
    vector<long long int>left_diagonal(1e5+1);
    vector<long long int>right_diagonal(1e5+1);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            scanf("%lld",&k);
            chess[i][j]=k;
        }
    }
    right_cut(n,chess,matrix,right_diagonal);
    left_cut(n,chess,matrix,left_diagonal);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            if(matrix[i][j].left%2==0)
            {
                int l=matrix[i][j].left,r=matrix[i][j].right;
                pre=even;
                even=max(even,left_diagonal[l]+right_diagonal[r]-chess[i][j]);
                if(pre!=even)
                {
                    x1=i;
                    y1=j;
                }
            }
            else
            {
                int l=matrix[i][j].left,r=matrix[i][j].right;
                pre=odd;
                odd=max(odd,left_diagonal[l]+right_diagonal[r]-chess[i][j]);
                if(pre!=odd)
                {
                    x2=i;
                    y2=j;
                }
            }
        }
    }
    cout<<odd+even<<endl<<x1<<" "<<y1<<" "<<x2<<" "<<y2<<endl;
}
