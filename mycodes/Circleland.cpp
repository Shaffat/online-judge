
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())




int main()
{
    ll test,t,n,i,j,tot;
    sc1(test);
    while(test--)
    {
        sc1(n);
        ll mx=-1,res=1e15;
        tot=0;
        vl v;
        vl fw(n+10,0);
        vl rv(n+10,0);
        for(i=1;i<=n;i++)
        {
            sc1(j);
            v.push_back(j);
        }
        //cout<<"tot="<<tot<<" mx="<<mx<<endl;
        for(i=1;i<n;i++)
        {
            fw[i]=fw[i-1]+v[i-1];
            //cout<<"fw["<<i<<"]="<<fw[i]<<endl;
        }
        for(i=n-1;i>0;i--)
        {
            rv[i]=rv[i+1]+v[i];
            //cout<<"rv["<<i<<"]="<<rv[i]<<endl;
        }
        for(i=0;i<n;i++)
        {
            if(i==n-1)
                j=0;
            else
                j=i+1;
            ll res1,res2;
            res1=2*fw[i]+rv[j];
            res2=2*rv[j]+fw[i];
            //cout<<"("<<i<<","<<j<<")="<<res1<<" , "<<res2<<endl;
            res=min3(res,res1,res2);
        }

        printf("%lld\n",res);
    }

}
