#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,m,c,t=1;
    while(scanf("%d %d %d",&n,&m,&c))
    {
        if(n==0 && m==0 && c==0)
        {
            return 0;
        }
        printf("Sequence %d\n",t);
        t++;
        int mx=0,i,j,total=0;
        vector<int>on_off(n+1,0);
        vector<int>usage(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            usage[i]=j;
        }
        for(i=1;i<=m;i++)
        {
            scanf("%d",&j);
            if(on_off[j])
            {
                on_off[j]=0;
                total-=usage[j];
            }
            else
            {
                on_off[j]=1;
                total+=usage[j];
                mx=max(mx,total);
            }
        }
        if(mx<=c)
        {
            printf("Fuse was not blown.\nMaximal power consumption was %d amperes.\n\n",mx);
        }
        else
            printf("Fuse was blown.\n\n");
    }
}
