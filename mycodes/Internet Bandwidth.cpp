#include<bits/stdc++.h>

using namespace std;

struct node
{
    int residual_index,pos_in_index,capacity;
};

struct flow_node
{
    int id,flow;
};

struct parent_node
{
    int parent_id,pos;
};




int send_flow(int s,int t,vector<vector<node> >&graph,vector<parent_node>&path)
{
    //cout<<"sending flow"<<endl;
    int i,j;
    queue<flow_node>q;
    parent_node p;
    p.parent_id=s;
    path[s]=p;
    flow_node cur,start;
    cur.flow=2e9;
    cur.id=s;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        //cout<<"cur_node="<<cur.id<<" flow="<<cur.flow<<endl;
        if(cur.id==t)
        {
            return cur.flow;
        }
        q.pop();

        for(i=0;i<graph[cur.id].size();i++)
        {
            int nxt_node=graph[cur.id][i].residual_index;
            //cout<<"nxt_node="<<nxt_node<<" capacity="<<graph[cur.id][i].capacity<<" parent="<<path[nxt_node].parent_id<<endl;
            if(path[nxt_node].parent_id==-1 && graph[cur.id][i].capacity>0)
            {
                flow_node nxt;
                nxt.flow=min(cur.flow,graph[cur.id][i].capacity);
                nxt.id=nxt_node;
                p.parent_id=cur.id;
                p.pos=i;
                q.push(nxt);
                path[nxt_node]=p;
//                cout<<"pushed id="<<nxt.id<<" and flow="<<nxt.flow<<endl;
//                cout<<"parent of "<<nxt_node<<" id="<<path[nxt_node].parent_id<<" pos="<<path[nxt_node].pos<<endl;
            }
        }
    }
    return 0;
}



int mx_flow(int n,int s,int t,vector<vector<node> >&graph)
{
    parent_node p;
    p.parent_id=-1;
    int res=0,flow;
    while(1)
    {
         vector<parent_node>path(n+1,p);
         flow=send_flow(s,t,graph,path);
         if(!flow)
         {
             break;
         }
         //cout<<"sent flow "<<flow<<endl;
         res+=flow;
         int node=t;
         while(node!=s)
         {
             //cout<<"node="<<node<<endl;
             int index,pos,resudial_index,resudial_pos;
             index=path[node].parent_id;
             pos=path[node].pos;
             //cout<<"index="<<index<<" pos="<<pos<<endl;
             resudial_index=graph[index][pos].residual_index;
             //cout<<"resu index="<<resudial_index<<endl;
             resudial_pos=graph[index][pos].pos_in_index;
             //cout<<"resu pos"<<resudial_pos<<endl;
             graph[index][pos].capacity-=flow;
             graph[resudial_index][resudial_pos].capacity+=flow;
             node=index;

         }
    }
    return res;
}


int main()
{
    int test,te,n,i,j;
    scanf("%d",&test);
    for(te=1;te<=test;te++)
    {
        int u,v,w,s,t,c,i,j,res;
        scanf("%d",&n);
        vector<node>col;
        vector<vector<node> >total_graph(n+1,col);
        scanf("%d %d %d",&s,&t,&c);
        for(i=1;i<=c;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            node tmp1,tmp2;
            tmp1.residual_index=v;
            tmp2.residual_index=u;
            tmp1.capacity=w;
            tmp2.capacity=0;
            tmp1.pos_in_index=total_graph[v].size();
            tmp2.pos_in_index=total_graph[u].size();
            total_graph[u].push_back(tmp1);
            total_graph[v].push_back(tmp2);

            tmp1.residual_index=u;
            tmp2.residual_index=v;
            tmp1.capacity=w;
            tmp2.capacity=0;
            tmp1.pos_in_index=total_graph[u].size();
            tmp2.pos_in_index=total_graph[v].size();
            total_graph[v].push_back(tmp1);
            total_graph[u].push_back(tmp2);
        }
        res=mx_flow(n,s,t,total_graph);
        printf("Case %d: %d\n",te,res);
    }
}


