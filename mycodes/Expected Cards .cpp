#include<bits/stdc++.h>
using namespace std;
int c,d,h,s;
double solve(int h_c,int h_d,int h_h,int h_s,int h_j,int t_c,int t_d,int t_h,int t_s,vector<vector<vector<vector<vector<double> > > > >&memory)
{
    //cout << "Recure : " << c << " " << d << " " << h << " " << s << endl;
    //cout << h_c << " " << h_d << " " << h_h << " " << h_s << " " << h_j << " table " << t_c << " " << t_d << " " << t_h << " " << t_s << endl;
    if(t_c>=c&&t_d>=d&&t_h>=h&&t_s>=s)
    {
        //cout<<"0 execute\n";
        return 0;
    }
    if(h_c==0&&h_d==0&&h_h==0&&h_s==0&&h_j==0)
    {
        return 2e9;
    }
    //cout<<"here"<<endl;
    if(memory[h_c][h_d][h_h][h_s][h_j]!=-1)
    {
        //cout<<"mem"<<endl;
        return memory[h_c][h_d][h_h][h_s][h_j];
    }
    double total=h_c+h_d+h_h+h_s+h_j;
    double res=0;
    //cout << "h_c is " << h_c << endl;
    if(h_c>0)
    {   //cout<<"i should execute"<<endl;
        res+=h_c*(solve(h_c-1,h_d,h_h,h_s,h_j,t_c+1,t_d,t_h,t_s,memory)+1);
    }
    if(h_d>0)
    {
        res+=h_d*(solve(h_c,h_d-1,h_h,h_s,h_j,t_c,t_d+1,t_h,t_s,memory)+1);
    }
    if(h_h>0)
    {
        res+=h_h*(solve(h_c,h_d,h_h-1,h_s,h_j,t_c,t_d,t_h+1,t_s,memory)+1);
    }
    if(h_s>0)
    {
        res+=h_s*(solve(h_c,h_d,h_h,h_s-1,h_j,t_c,t_d,t_h,t_s+1,memory)+1);
    }
    if(h_j>0)
    {
        double tmp=2e9;
        tmp=min(tmp,solve(h_c,h_d,h_h,h_s,h_j-1,t_c+1,t_d,t_h,t_s,memory)+1);
        tmp=min(tmp,solve(h_c,h_d,h_h,h_s,h_j-1,t_c,t_d+1,t_h,t_s,memory)+1);
        tmp=min(tmp,solve(h_c,h_d,h_h,h_s,h_j-1,t_c,t_d,t_h+1,t_s,memory)+1);
        tmp=min(tmp,solve(h_c,h_d,h_h,h_s,h_j-1,t_c,t_d,t_h,t_s+1,memory)+1);
        res+=h_j*tmp;
    }
    return memory[h_c][h_d][h_h][h_s][h_j]=res/total;
}
int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d %d",&c,&d,&h,&s);
        vector<int>num(16,0);
        vector<double>col1(3,-1);
        vector<vector<double> >col2(17,col1);
        vector<vector<vector<double> > >col3(17,col2);
        vector<vector<vector<vector<double> > > >col4(17,col3);
        vector<vector<vector<vector<vector<double> > > > >memory(17,col4);
        num[c]++;
        num[d]++;
        num[h]++;
        num[s]++;
        int invalid=0;
        double res;
        if(num[15]>1||(num[15]==1&&num[14]>1)||num[14]>2)
        {
            invalid=1;
        }
        if(invalid)
        {
            res=-1;
        }
        else
        res=solve(13,13,13,13,2,0,0,0,0,memory);
        printf("Case %d: %.9lf\n",t,res);
    }
}
