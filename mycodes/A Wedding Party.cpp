#include<bits/stdc++.h>


using namespace std;

struct node
{
    int x,dis;
};

bool operator <(node a, node b)
{
    if(a.dis!=b.dis)
    {
        return a.dis>b.dis;
    }
    return false;
}


struct node1
{
    int x,dis;
};

bool operator <(node1 a, node1 b)
{
    if(a.x!=b.x)
    {
        return a.x<b.x;
    }
    if(a.dis!=b.dis)
    {
        return a.dis>b.dis;
    }
    return false;
}

node1 findMax(node1 res1,node1 res2)
{
    node1 ans;
    ans.x=-1;
    ans.dis=1e9;

    if(ans.x<res1.x)
    {
        ans.x=res1.x;
        ans.dis=res1.dis;
    }
    else if(ans.x==res1.x && ans.dis>res1.dis)
    {
        ans.x=res1.x;
        ans.dis=res1.dis;
    }

    if(ans.x<res2.x)
    {
        ans.x=res2.x;
        ans.dis=res2.dis;
    }
    else if(ans.x==res2.x && ans.dis>res2.dis)
    {
        ans.x=res2.x;
        ans.dis=res2.dis;
    }

    return ans;
}

void djsktra(int source,vector<vector<int> >&connection,vector<vector<int> >&cost,vector<vector<int> >&dis)
{
    vector<int>vis(1000,0);

    dis[source][source]=0;
    int i,j;
    node cur;
    cur.x=source;
    cur.dis=0;
    priority_queue<node>pq;
    pq.push(cur);
    while(!pq.empty())
    {
        cur=pq.top();
        pq.pop();
        if(vis[cur.x]){
            continue;
        }

        vis[cur.x]=1;

        int curNode=cur.x,curDis=cur.dis;

        for(i=0;i<connection[curNode].size();i++)
        {
            int child=connection[curNode][i];
            if(dis[source][child]>curDis+cost[curNode][i])
            {
                dis[source][child]=curDis+cost[curNode][i];
                node tmp;
                tmp.x=child;
                tmp.dis=dis[source][child];
                pq.push(tmp);
            }
        }
    }
    return;

}


node1 solve(int bitmask,int last,int n,vector<int>&shops,vector<vector<int> >&dis,vector<vector<node1> >&memory)
{
    int i,j,cost=0;
    if(memory[bitmask][last].x!=-1)
    {
        return memory[bitmask][last];
    }
    node1 ans,res3;
    ans.x=-100;
    ans.dis=1e9;
    for(i=0;i<shops.size();i++)
    {
        int x,y;
        x=shops[last];
        y=shops[i];
        if((bitmask&(1<<i))==0  &&  dis[x][y]!=1e9)
        {

            node1 res1,res2,tmp;

            res1.dis=1e9;
            res1.x=-100;
            tmp=solve(bitmask|(1<<i),i,n,shops,dis,memory) ;
            //cout<<"from "<<x<<" to "<<y<<" bitmask="<<bitmask<<" ans is "<<tmp.x<<"  "<<tmp.dis<<endl;
            res1.dis=tmp.dis+dis[x][y];
            res1.x=tmp.x+1;

            ans=findMax(res1,ans);

        }
    }
     res3.x=-1;
     res3.dis=1e9;

     if(dis[shops[last]][n-1]!=1e9)
     {
         //cout<<"from "<<shops[last]<<" to "<<n-1<<" bitmask="<<bitmask<<" ans is "<<res3.x<<"  "<<res3.dis<<endl;
         res3.x=0;
         res3.dis=dis[shops[last]][n-1];
     }
     ans=findMax(res3,ans);
     //cout<<"memory["<<bitmask<<"]["<<last<<"]="<<ans.x<<" "<<ans.dis<<endl;
     return memory[bitmask][last]=ans;
}

int main()
{
    int t,n,m,i,j,u,v,w,test,s;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d",&n,&m,&s);
        vector<int>col(n+1,1e9);
        vector<vector<int> >dis(n+1,col);

        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);

        vector<int>shops;
        for(i=1;i<=s;i++)
        {
            scanf("%d",&j);
            shops.push_back(j);
        }
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost[u].push_back(w);
        }
        djsktra(0,connection,cost,dis);
        for(i=0;i<shops.size();i++)
        {
            u=shops[i];
            djsktra(u,connection,cost,dis);
        }

        if(dis[0][n-1]>=1e8)
        {
            printf("Case %d: Impossible\n",t);
            continue;
        }
        node1 ans,tmp;
        ans.x=0;
        ans.dis=dis[0][n-1];

        tmp.x=-1;
        tmp.dis=1e9;
        vector<node1>col1(16,tmp);
        vector<vector<node1> >memory(1<<16,col1);

        for(i=0;i<s;i++)
        {
            u=shops[i];
            if(dis[0][u]>=1e8) continue;
            tmp=solve(1<<i,i,n,shops,dis,memory);
            int f=(1<<i);
            //cout<<"out ["<<(1<<i)<<"]["<<i<<"]="<<memory[f][i].x<<" "<<memory[f][i].dis<<endl;
            tmp.x+=1;
            tmp.dis+=dis[0][shops[i]];
            ans=findMax(tmp,ans);

        }

        printf("Case %d: %d %d\n",t,ans.x,ans.dis);

    }
}
