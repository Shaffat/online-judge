#include<bits/stdc++.h>

using namespace std;

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int r1,r2,c1,c2,res=-1,neg,c3;
        scanf("%d %d %d %d",&r1,&c1,&r2,&c2);
        neg=r2-r1;
        r2-=neg;
        c3=c2+neg;
        c2-=neg;
        //cout<<"c3="<<c3<<" c2="<<c2<<" c1="<<c1<<endl;
        if(c1==c2||c1==c3)
        {
            res=1;
        }
        else if((c1-c2)%2==0)
        {
            res=2;
        }
        if(res!=-1)
        {
            printf("Case %d: %d\n",t,res);
        }
        else
        {
            printf("Case %d: impossible\n",t);
        }
        t++;
    }
}
