#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,u,v,n,m;
    while(scanf("%d %d",&n,&m)){
    if(n==0 && m==0)
    {
        return 0;
    }
    vector<int>s;
    vector<vector<int> >connection(n+1,s);
    vector<int>indegree(n+1,0);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        indegree[v]+=1;
        //cout<<"indegree of "<<v<<" = "<<indegree[v]<<endl;
    }

    int task=0;

    while(task<n)
    {
        for(i=1;i<=n;i++)
        {
            if(indegree[i]==0)
            {
                task++;
                indegree[i]=-1;
                printf("%d",i);
                if(task<n)
                {
                    printf(" ");
                }
                else
                    printf("\n");
                for(j=0;j<connection[i].size();j++)
                {
                    indegree[connection[i][j]]-=1;
                }
            }
        }

    }
    }
}
