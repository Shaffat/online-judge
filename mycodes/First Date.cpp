#include<bits/stdc++.h>
//19999-2-29
using namespace std;

int main()
{
    int year,month,day,i,j,res;
    while(scanf("%d-%d-%d",&year,&month,&day)!=EOF){
            int tmpyear=year;
        if(month<2){
            tmpyear--;
        }
        if(month==2 && day<29){
            tmpyear--;
        }
        //cout<<"year ="<<year<<" month="<<month<<" day="<<day<<endl;
        int skip=11+((tmpyear/100)-(tmpyear/400)-((1582/100)-(1582/400)));

        cout<<"skip="<<skip<<endl;
        if(month==2 && day==29){
            if(year%100==0 && year%400!=0){
                day=28;

            }
        }
        int monthday[15];

        monthday[1]=31;
        monthday[2]=28;
        monthday[3]=31;
        monthday[4]=30;
        monthday[5]=31;
        monthday[6]=30;
        monthday[7]=31;
        monthday[8]=31;
        monthday[9]=30;
        monthday[10]=31;
        monthday[11]=30;
        monthday[12]=31;
        if(year%100==0){
            if(year%400==0){
                monthday[2]=29;
            }
        }
        else if(year%4==0){
            monthday[2]=29;
        }

        while(skip>0){
            if(day>=monthday[month]){
                day=1;
                month++;
                if(month>12){
                    year++;
                    month=1;
                }
            }
            else
                day++;
            skip--;
        }
        printf("%d-",year);
        if(month/10>0){
            printf("%d-",month);
        }else{
            printf("0%d-",month);
        }
        if(day/10>0){
            printf("%d\n",day);
        }else{
            printf("0%d\n",day);
        }

    }
}
