#include<bits/stdc++.h>
using namespace std;

struct node
{
    long long int vertex,cost;
};

bool operator <(node a,node b)
{
    if(a.cost!=b.cost)
    {
        return a.cost>b.cost;
    }
    return false;
}

void solve(int &n,vector<int>&path,vector<vector<int> >&connection,vector<vector<int> >&cost)
{
    long long int i,nxt_node,nxt_cost;
    priority_queue<node>q;
    vector<int>vis(n+1,0);
    vector<long long int>dis(n+1,1e12);
    node start;
    start.vertex=1;
    start.cost=0;
    q.push(start);
    path[1]=1;
    while(!q.empty())
    {
        node cur=q.top();
        q.pop();
        int cur_node=cur.vertex,cur_cost=cur.cost;
        if(vis[cur_node])
        {
            continue;
        }
        vis[cur_node]=1;
        for(i=0;i<connection[cur_node].size();i++)
        {
            nxt_node=connection[cur_node][i];
            nxt_cost=cost[cur_node][i];
            if(dis[nxt_node]>cur_cost+nxt_cost && vis[nxt_node]==0)
            {
                node tmp;
                tmp.vertex=nxt_node;
                tmp.cost=cur_cost+nxt_cost;
                dis[nxt_node]=tmp.cost;
                q.push(tmp);
                path[nxt_node]=cur_node;
            }
        }
    }

}

void path_printer(int node,vector<int>&path)
{
    if(path[node]==1)
    {
        printf("%d %d ",1,node);
        return;
    }
    path_printer(path[node],path);
    printf("%d ",node);
    return;
}

int main()
{
    int i,j,u,v,w,n,m;
    scanf("%d %d",&n,&m);
    vector<vector<int> >connection(n+1);
    vector<vector<int> >cost(n+1);
    vector<int>path(n+1);
    path[n]=-1;
    for(i=1;i<=m;i++)
    {
        scanf("%d %d %d",&u,&v,&w);
        assert(1 <= w && w <= 1000000 ) ;
        connection[u].push_back(v);
        connection[v].push_back(u);
        cost[u].push_back(w);
        cost[v].push_back(w);
    }
    solve(n,path,connection,cost);
    if(path[n]!=-1)
    path_printer(n,path);
    else
        printf("-1\n");

}
