#include<bits/stdc++.h>

using namespace std;

#define ll long long int

void multiplication(vector<vector<int> >&a,vector<vector<int> >&b,vector<vector<int> >&res,int m)
{
    int i,j,k;
    for(i=0;i<a.size();i++)
    {
        for(j=0;j<b[0].size();j++)
        {
            int sum=0;
            for(k=0;k<b.size();k++)
            {
                sum+=(a[i][k]*b[k][j])%m;
                sum%=m;
            }
            res[i][j]=sum;
        }
    }
    return ;
}
void power(int p,vector<vector<int> >&base,vector<vector<int> >&res,int m)
{
    int i,j;

    if(p==1)
    {
        for(i=0;i<base.size();i++)
        {
            for(j=0;j<base[0].size();j++)
            {
                res[i][j]=base[i][j];
            }
        }
        return;
    }
    else if(p%2==0)
    {
        power(p/2,base,res,m);
        vector<int>col(4);
        vector<vector<int> >tmp(4,col);
        for(i=0;i<4;i++)
        {
            for(j=0;j<4;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,res,m);
    }
    else
    {
        power(p/2,base,res,m);
        vector<int>col(4);
        vector<vector<int> >tmp(4,col);
        vector<vector<int> >tmp2(4,col);
        for(i=0;i<4;i++)
        {
            for(j=0;j<4;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,tmp2,m);
        multiplication(tmp2,base,res,m);
    }
    return;
}

int solve(int n,int a,int b,int c)
{
    if(n<=2)
    {
        return 0;
    }
    else
    {
        vector<int>col(1);
        vector<vector<int> >matrix(4,col);
        vector<vector<int> >final_res(4,col);
        matrix[0][0]= 0;
        matrix[1][0]= 0;
        matrix[2][0]= 0;
        matrix[3][0]= 1;
        vector<int>col1(4);
        vector<vector<int> >base(4,col1);
        vector<vector<int> >res(4,col1);
        base[0][0]=a;
        base[0][1]=0;
        base[0][2]=b;
        base[0][3]=c;
        base[1][0]=1;
        base[1][1]=0;
        base[1][2]=0;
        base[1][3]=0;
        base[2][0]=0;
        base[2][1]=1;
        base[2][2]=0;
        base[2][3]=0;
        base[3][0]=0;
        base[3][1]=0;
        base[3][2]=0;
        base[3][3]=1;
        power(n-2,base,res,10007);
        multiplication(res,matrix,final_res,10007);
        return final_res[0][0];
    }

}

int main()
{
    int test,t,n,a,b,c,res;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d %d",&n,&a,&b,&c);
        res=solve(n,a,b,c);
        printf("Case %d: %d\n",t,res);
    }
}
