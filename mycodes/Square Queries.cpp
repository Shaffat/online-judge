#include<bits/stdc++.h>
using namespace std;

bool valid(int i,int j,int n)
{
    if(i<=0 || i>n ||j<=0 || j>n)
    {
        return false;
    }
    return true;
}

void print(int n,vector<vector<vector<int> > >&data)
{
    int i,j,k;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            cout<<"for ["<<i<<"]["<<j<<"]=";
            for(k=0;k<=20;k++)
            {
                cout<<data[i][j][k]<<" ";
            }
            cout<<endl;
        }
    }
}

void preproocess(int n,vector<vector<vector<int> > >&data,vector<vector<int> >&table)
{
    //cout<<"preprocess"<<endl;
    int i,j;
    for(i=1;i<=n;i++)
    {
        for(j=1 ;j<=n;j++)
        {
            data[i][j][0]=table[i][j];
            //cout<<"data["<<i<<"]["<<j<<"][0]="<<data[i][j][0]<<endl;
        }
    }
    int len,ans,res;
    for(len=1;len<=log2(n);len++)
    {
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                //cout<<"finding re for ["<<i<<"]["<<j<<"]["<<len<<"]"<<endl;
                int nxtI=i+(1<<(len-1)),nxtJ=j;
                int ans=data[i][j][len-1];
                if(valid(nxtI,nxtJ,n))
                {
                    ans=max(ans,data[nxtI][nxtJ][len-1]);
                }
                else
                    continue;
                //cout<<"1"<<endl;
                nxtI=i,nxtJ=j+(1<<(len-1));
                if(valid(nxtI,nxtJ,n))
                {
                    ans=max(ans,data[nxtI][nxtJ][len-1]);
                }
                else
                    continue;
                //cout<<"2"<<endl;
                nxtI=i+(1<<(len-1)),nxtJ=j+(1<<(len-1));
                if(valid(nxtI,nxtJ,n))
                {
                    ans=max(ans,data[nxtI][nxtJ][len-1]);
                }
                else
                    continue;
                    //cout<<"3"<<endl;
                data[i][j][len]=ans;
                //cout<<"res="<<ans<<endl;
            }
        }
    }

    //print(n,data);
    return;
}




int query(int n,int ss,int se,int ds,int de,vector<vector<vector<int> > >&data,vector<vector<int> >&vis,int q)
{
    //cout<<"ss="<<ss<<" se="<<se<<" ds="<<ds<<" de="<<de <<" 0="<<data[1][1][1]<<endl;
    if(ss>ds || se>de) return 0;
    vis[ss][se]=q;
    int i,j,len,nxtI,nxtJ;
    for(len=22;len>=0;len--)
    {
        int x= (1<<len);
        nxtI=ss+(1<<(len))-1;
        nxtJ=se+(1<<(len))-1;
        //cout<<"nxtI="<<nxtI<<" "<<nxtJ<<endl;
        if(nxtI<=ds && nxtJ<=de)
        {
            //cout<<"is valid"<<endl;
            int ans1=0,ans2=0,ans3=0,ans4;
            ans4=data[ss][se][len];
            if(valid(ss+x,se,n))
            {
                ans1=query(n,ss+x,se,ds,de,data,vis,q);
            }
            if(valid(ss,se+x,n))
            {
                ans2=query(n,ss,se+x,ds,de,data,vis,q);
            }
            if(valid(ss+x,se+x,n))
            {
                ans3=query(n,ss+x,se+x,ds,de,data,vis,q);
            }

            return max(ans1,max(ans2,max(ans3,ans4)));
        }
    }
}

int main()
{
    int n,q,i,j,test,t,I,J,S,k;
    scanf("%d",&test);
    //freopen("out.txt","w",stdout);
    for(t=1;t<=test;t++)
    {
        printf("Case %d:\n",t);
        scanf("%d %d",&n,&q);
        vector<int>col(n+1);
        vector<vector<int> >table(n+1,col);
        vector<int>col1(33,-1);
        vector<vector<int> >col2(n+1,col1);
        vector<vector<vector<int> > >data(n+1,col2);
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                scanf("%d",&k);
                table[i][j]=k;

            }
        }

        preproocess(n,data,table);
//        cout<<"from main"<<endl;
//        print(n,data);
        vector<int>col5(n+1,0);
        vector<vector<int> >vis(n+1,col5);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d %d",&I,&J,&S);
            int res=query(n,I,J,I+S-1,J+S-1,data,vis,q);
            printf("%d\n",res);
        }
    }
}
