#include<bits/stdc++.h>
using namespace std;

bool solve(string &s,int i,int j)
{
    if(i>j)
    {
        return true;
    }
    if(s[i]!=s[j])
    {
        return false;
    }
    else
        return solve(s,i+1,j-1);
}

int main()
{
    ios_base::sync_with_stdio(false);
    int test,t=1;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s;
        cin>>s;
        //cout<<"s="<<s<<endl;
        if(solve(s,0,s.size()-1))
        {
            printf("Case %d: Yes\n",t);
        }
        else
            printf("Case %d: No\n",t);
    }
}
