#include<bits/stdc++.h>
typedef long long int ll;
using namespace std;

ll hashvalue(string s,ll m,ll mod,ll base)
{
    ll value,power=1,i;
    for(i=m-1;i>=0;i++)
    {
        ll add=power*s[i];
        add%=mod;
        value+=add;
        value%=mod;
        power*=base;
    }
    return value;
}

ll getpower(ll base,ll mod,ll power)
{
    ll value=1,i;
    for(i=1;i<=power;i++)
    {
        value*=base;
        value%=mod;
    }
    return value;
}

int main()
{
    int test,i,j,l,m,n,q,f,g;
    scanf("%d",&test);
    while(test--)
    {
        scanf("%d %d",&f,&g);
        string first,second,tmp;
        int last;
        cin>>first>>second;
        scanf("%d",&q);
        for(i=1;i<=q;i++)
        {
            int counter=0;
            scanf("%d",&l);
            if(l>first.size()||l>second.size())
            {
                printf("0\n");
                continue;
            }
            tmp="";
            last=l-1;
            for(n=0;n<last;n++)
            {
                tmp.push_back(first[n]);
            }
            while(last<first.size())
            {
                tmp.push_back(first[last]);
                cout<<"tmp="<<tmp<<endl;
                ll base=347,mod=1e9+7,m=tmp.size(),power1,texthash1,patternhash1,power2,mod2,base2,texthash2,patternhash2;
                texthash1=hashvalue(second,m,mod,base);
                patternhash1=hashvalue(tmp,m,mod,base);
                base2=349,mod2=1e10+19;
                texthash2=hashvalue(second,m,mod2,base2);
                patternhash2=hashvalue(tmp,m,mod2,base2);
                power1=getpower(base,m,mod);
                power2=getpower(base2,m,mod2);
        //        cout<<"power1="<<power1<<" power2="<<power2<<" hashvalue1="<<texthash1<<" patternhash1="<<patternhash1<<endl;
        //        cout<<"hashvalue2="<<texthash2<<" patternhash2="<<patternhash2<<endl;
                if(texthash1==patternhash1 && texthash2==patternhash2)
                {
                    counter++;
                    last++;
                    tmp.erase(tmp.begin());
                    continue;
                }
                for(i=m;i<second.size();i++)
                {
                    texthash1=(texthash1-(power1*second[i-m])%mod)%mod;
                    texthash1=(texthash1+ mod)%mod;
                    texthash1=(texthash1*base)%mod;
                    texthash1=(texthash1+second[i])%mod;
                    texthash2=(texthash2-(power2*second[i-m])%mod2)%mod2;
                    texthash2=(texthash2+mod2)%mod2;
                    texthash2=(texthash2*base2)%mod2;
                    texthash2=(texthash2+second[i])%mod2;
                    //cout<<"texthash1="<<texthash1<<" texthash2="<<texthash2<<endl;
                    if(texthash1==patternhash1 && texthash2==patternhash2)
                    {
                        counter++;
                        last++;
                        tmp.erase(tmp.begin());
                        break;
                    }

                }
                last++;
                tmp.erase(tmp.begin());
            }
            printf("%d\n",counter);
        }
    }
}
