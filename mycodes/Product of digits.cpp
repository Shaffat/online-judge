#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>primes;
vector<bool>ar(100000,0);

bool ok(ll n)
{
    ll res=1,tmp=n;
    while(n>0)
    {
        res*=n%10;
        n/=10;
    }
    if(res==tmp)
    {
        return true;
    }
    return false;
}

void sieve()
{
    ll i,j;
    ar[1]=1;
    for(i=4;i<1e5;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e5);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e5;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e5;i+=2)
    {
        if(ar[i]==0)
        {
            if(ok(i))
            primes.push_back(i);
        }
    }
}


bool solve(ll n,vector<int>&counter)
{
    ll i,j;
    string res="";
    for(i=0;i<primes.size();i++)
    {
        ll cur=1;
        if(n%primes[i]==0){
            while(n%primes[i]==0){
                counter[primes[i]]++;
                n/=primes[i];
            }
        }
    }

    if(n>1){
       return false;
    }
    else
        return true;
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ios_base::sync_with_stdio(0);
    primes.push_back(2);
    primes.push_back(3);
    primes.push_back(5);
    primes.push_back(7);
    ll n,i,j,test;
    cin>>test;
    while(test--){
        cin>>n;
        vector<int>counters(11,0);
        bool ok;
        if(n==0){
            cout<<"0"<<endl;
        }
        else if(n==1){
            cout<<"1"<<endl;
        }
        else{
            string s="";
            ok=solve(n,counters);
            if(ok){
                ll mn=min(counters[2]%3,counters[3]%2);
                for(i=1;i<=mn;i++){
                    s.push_back('6');
                }
                counters[2]-=mn;
                counters[3]-=mn;
                while(counters[2]>0){
                    mn=min(counters[2],3);
                    counters[2]-=mn;
                    s.push_back((1<<mn)+'0');
                }
                while(counters[3]>0){
                    mn=min(counters[3],2);
                    counters[3]-=mn;
                    if(mn==2){
                        s.push_back('9');
                    }
                    else
                        s.push_back('3');
                }
                for(i=1;i<=counters[5];i++){
                    s.push_back('5');
                }
                for(i=1;i<=counters[7];i++){
                    s.push_back('7');
                }
                sort(s.begin(),s.end());
                cout<<s<<endl;
            }
            else
                cout<<"-1"<<endl;
        }

    }
}

