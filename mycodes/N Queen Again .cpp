#include<bits/stdc++.h>
using namespace std;

struct place
{
    int row,col;
};

vector<int>col(9,0);
vector<vector<int> >board(9,col);
vector<int>combination(9);
vector<vector<int> >all_combinations;

void add_in_row(int r,int c)
{
    int i,j;
    for(i=1;i<=8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]+=1;
    }
    return;
}
void add_in_col(int r,int c)
{
    int i,j;
    for(i=1;i<=8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]+=1;
    }
    return;
}
void add_in_up_diagonal(int r,int c)
{
    int last_row=r+min(8-r,c-1);
    int last_col=c-min(8-r,c-1);
    while(last_row>=1 && last_col<=8)
    {
        if(last_row==r && last_col==c)
        {
            last_row--;
            last_col++;
            continue;
        }
        board[last_row][last_col]++;
        last_row--;
        last_col++;
    }
    return;
}
void add_in_down_diagonal(int r,int c)
{
    int first_row=r-min(r-1,c-1);
    int first_col=c-min(r-1,c-1);
    while(first_row<=8 && first_col<=8)
    {
        if(first_row!=r && first_col!=c)
        {
            board[first_row][first_col]++;
        }
        first_row++;
        first_col++;
    }
    return;
}
void remove_in_row(int r,int c)
{
    int i,j;
    for(i=1;i<=8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]-=1;
    }
    return;
}
void remove_in_col(int r,int c)
{
    int i,j;
    for(i=1;i<=8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]-=1;
    }
    return;
}
void remove_in_up_diagonal(int r,int c)
{
    int last_row=r+min(8-r,c-1);
    int last_col=c-min(8-r,c-1);
    while(last_row>=1 && last_col<=8)
    {
        if(last_row==r && last_col==c)
        {
            last_row--;
            last_col++;
            continue;
        }
        board[last_row][last_col]--;
        last_row--;
        last_col++;
    }
    return;
}
void remove_in_down_diagonal(int r,int c)
{
    int first_row=r-min(r-1,c-1);
    int first_col=c-min(r-1,c-1);
    while(first_row<=8 && first_col<=8)
    {
        if(first_row!=r && first_col!=c)
        {
            board[first_row][first_col]--;
        }
        first_row++;
        first_col++;
    }
    return;
}

void all_combinations_for_8_queen(int r)
{
    int i,j;
    if(r==9)
    {
        int index=all_combinations.size();
        all_combinations.push_back(col);
        for(i=1;i<=8;i++)
        {
            all_combinations[index][i]=combination[i];
        }
        return;
    }
    for(i=1;i<=8;i++)
    {
        if(board[r][i]==0)
        {
            combination[r]=i;
            board[r][i]+=1;
            add_in_row(r,i);
            add_in_col(r,i);
            add_in_down_diagonal(r,i);
            add_in_up_diagonal(r,i);
            all_combinations_for_8_queen(r+1);
            board[r][i]-=1;
            remove_in_row(r,i);
            remove_in_col(r,i);
            remove_in_down_diagonal(r,i);
            remove_in_up_diagonal(r,i);
        }
    }
    return ;
}

int moves_need(place a, place b)
{
    if(a.row==b.row && a.col==b.col)
    {
        return 0;
    }
    int row_dif=abs(a.row-b.row),col_dif=abs(a.col-b.col);
    if(row_dif==col_dif ||a.row==b.row||a.col==b.col)
    {
        return 1;
    }
    else
        return 2;
}

int minimum_moves(int index,int bitmask ,vector<place>&correct,vector<place>&available,vector<vector<int> >&memory)
{
    //cout<<"index="<<index<<" bitmask="<<bitmask<<endl;
    if(index>7)
    {
        return 0;
    }
    if(memory[index][bitmask]!=-1)
    {
        return memory[index][bitmask];
    }
    int i,j,moves=2e9;
    for(i=0;i<available.size();i++)
    {
        if((bitmask&(1<<i))==0)
        {
            //cout<<"placing "<<available[i].row<<" "<<available[i].col<<" to "<<correct[i].row<<" "<<correct[i].col<<" move added="<<moves_need(correct[index],available[i])<<endl;
            moves=min(moves,(minimum_moves(index+1,(bitmask|(1<<i)),correct,available,memory)
            +moves_need(correct[index],available[i])));
        }
    }
    //cout<<"memory["<<index<<"]["<<bitmask<<"]="<<moves<<endl;
    return memory[index][bitmask]=moves;
}

int solve(vector<place>&v)
{
    int res=2e9,i,j,k;
    for(i=0;i<92;i++)
    {
        vector<place>correct;
        for(j=1;j<=8;j++)
        {
            place tmp;
            tmp.row=j;
            tmp.col=all_combinations[i][j];
            correct.push_back(tmp);
        }
        vector<int>c1(1000,-1);
        vector<vector<int> >memory(9,c1);
//        cout<<"currect positions"<<endl;
//        for(j=0;j<correct.size();j++)
//        {
//            cout<<correct[j].row<<" "<<correct[j].col<<endl;
//        }
//        //cout<<"given positions"<<endl;
//        for(j=0;j<v.size();j++)
//        {
//            cout<<v[j].row<<" "<<v[j].col<<endl;
//        }
//        //cout<<"movement count="<<endl;
        int cur=minimum_moves(0,0,correct,v,memory);
        //cout<<"cur="<<cur<<endl;
        res=min(res,cur);
    }
    return res;
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    all_combinations_for_8_queen(1);
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int i,j;
        vector<place>queens;
        for(i=1;i<=8;i++)
        {
            int chk=1;
            for(j=1;j<=8;j++)
            {
                char c;
                scanf(" %c",&c);
                if(c=='q')
                {
                    chk=0;
                    place tmp;
                    tmp.row=i;
                    tmp.col=j;
                    queens.push_back(tmp);
                }

            }
        }
        int res=solve(queens);
        printf("Case %d: %d\n",t,res);
    }
}
