#include<bits/stdc++.h>
using namespace std;

int total(int k,vector<int>votes)
{
    int total=0,i,j;
    for(i=0;i<votes.size();i++)
    {
        total+=k-votes[i];
    }
    return total;
}

int solve(int endrion,vector<int>&votes,int mx)
{
    int i,j,res;
    for(i=mx;i<=401;i++)
    {
        j=total(i,votes);
        if(j>endrion)
            return i;
    }
}

int main()
{
    int n,i,j,mx=-1,edrion=0,res;
    scanf("%d",&n);
    vector<int>votes;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        votes.push_back(j);
        edrion+=j;
        mx=max(mx,j);
    }
    res=solve(edrion,votes,mx);
    cout<<res<<endl;

}
