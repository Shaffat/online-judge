#include<bits/stdc++.h>
using namespace std;

#define ll long long int

ll GCD(ll a,ll b)
{
    while(a%b!=0)
    {
        ll temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}
int main()
{
    ios_base::sync_with_stdio(0);
    ll i,j,n,sum;
    cin>>n;
    vector<ll>v;
    for(i=1;i<=n;i++)
    {
        cin>>j;
        v.push_back(j);
    }
    set<ll>s;
    ll total=v[0],gcd=v[0];
    ll last=total;
    s.insert(last);
    cout<<total<<endl;
    for(i=1;i<v.size();i++)
    {
        ll before,nxt;
        gcd=GCD(gcd,v[i]);
        if(gcd==1){
            s.clear();
            gcd=v[i];
        }
        cout<<"last="<<last<<" current="<<v[i]<<" gcd="<<gcd<<" total="<<total<<endl;
        before=s.size();
        s.insert(v[i]);
        nxt=s.size();
        if(before!=nxt)
        {
            total+=v[i];
        }
        last=v[i];
        cout<<total<<endl;
    }
}
