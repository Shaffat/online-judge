#include<bits/stdc++.h>
using namespace std;

bool valid(int x,int y,int n)
{
    if(x<0 || x>=n || y<0 || y>=n) return false;
    return true;
}

bool is_cross(int x,int y,int n,vector<string>&mp)
{
    int nxt_x=x,nxt_y=y;
    if(valid(nxt_x,nxt_y,n))
    {
        if(mp[nxt_x][nxt_y]!='X')
            return false;
    }
    else return false;
    nxt_x=x-1,nxt_y=y-1;
    if(valid(nxt_x,nxt_y,n))
    {
        if(mp[nxt_x][nxt_y]!='X')
            return false;
    }
    else return false;
    nxt_x=x-1,nxt_y=y+1;
    if(valid(nxt_x,nxt_y,n))
    {
        if(mp[nxt_x][nxt_y]!='X')
            return false;
    }
    else return false;
    nxt_x=x+1,nxt_y=y-1;
    if(valid(nxt_x,nxt_y,n))
    {
        if(mp[nxt_x][nxt_y]!='X')
            return false;
    }
    else return false;
    nxt_x=x+1,nxt_y=y+1;
    if(valid(nxt_x,nxt_y,n))
    {
        if(mp[nxt_x][nxt_y]!='X')
            return false;
    }
    else return false;
    //cout<<"is cross x="<<x<<" y="<<y<<endl;
    return true;
}
int main()
{
    int n,i,j,res=0;
    string s;
    cin>>n;
    vector<string>v;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        v.push_back(s);
    }
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            if(is_cross(i,j,n,v))
                res++;
        }
    }
    cout<<res<<endl;
}
