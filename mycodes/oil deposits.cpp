#include<bits/stdc++.h>

using namespace std;

struct oilmine
{
    char value;
    int vis;
};

struct oilishere
{
    int row,column;
};

int main()
{
    int r,c;
    while(cin>>r>>c)
    {
        if(r==0 && c==0)
        {
            break;
        }

        int i,j;
        oilmine grid[r+10][c+10];
        vector<oilishere>track;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                cin>>grid[i][j].value;
                grid[i][j].vis=0;
                if(grid[i][j].value=='@')
                {
                    oilishere temp;
                    temp.row=i;
                    temp.column=j;
                    track.push_back(temp);
                }
            }
        }
        int mine=0;

        for(i=0;i<track.size();i++)
        {
            if(!grid[track.at(i).row][track.at(i).column].vis)
            {
                mine++;
               oilishere s;
               s.row=track.at(i).row;
               s.column=track.at(i).column;
               queue<oilishere>q;
               q.push(s);
               grid[s.row][s.column].vis=1;
               while(!q.empty())
               {
                   oilishere temp,c;
                   c.row=q.front().row;
                   c.column=q.front().column;
                   q.pop();
                   temp.row=c.row-1;
                   temp.column=c.column;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row-1;
                   temp.column=c.column+1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row-1;
                   temp.column=c.column-1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row;
                   temp.column=c.column+1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row;
                   temp.column=c.column-1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row+1;
                   temp.column=c.column;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row+1;
                   temp.column=c.column+1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
                    temp.row=c.row+1;
                   temp.column=c.column-1;
                   if(temp.row<=100 && temp.row>=1 && temp.column<=100 && temp.column>=1 && grid[temp.row][temp.column].value=='@'
                      && !grid[temp.row][temp.column].vis)
                   {
                       grid[temp.row][temp.column].vis=1;
                       q.push(temp);
                   }
               }
            }
        }
        cout<<mine<<endl;
    }
}
