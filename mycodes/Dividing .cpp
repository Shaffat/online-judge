#include<bits/stdc++.h>
using namespace std;

vector<int>col(1e5,-1);
vector<vector<int> >memory(6,col);
vector<vector<int> >test(6,col);
int t=0;
int solve(int index,int need,vector<int>&values,vector<vector<int> >&memory)
{
    cout<<"index="<<index<<" need="<<need<<" t="<<t<<endl;
    if(need==0)
    {
        return 1;
    }
    if(need<0 || index>=values.size())
    {
        return 0;
    }
    if(test[index][need]==t)
    {
        cout<<"memory activate"<<endl;
        return memory[index][need];
    }
    int i,j,chk=0;
    for(i=1;i<=values[index];i++)
    {
        if(!chk)
        {
            if(solve(index+1,need-(i*(index+1)),values,memory))
            {
                chk=1;
            }
        }
    }
    if(!chk)
    {
        if(solve(index+1,need,values,memory))
        {
            chk=1;
        }
    }
    test[index][need]=t;
    return memory[index][need]=chk;
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    while(1)
    {
        int i,j,n,k,res,chk=0,total=0;
        vector<int>marvels;
        for(i=1;i<=6;i++)
        {
            scanf("%d",&j);
            marvels.push_back(j);
            if(j!=0)
            {
                chk=1;
            }
            total+=j*i;
        }
        if(!chk)
        {
            return 0;
        }
        t++;
        printf("Collection #%d:\n",t);
        if(total%2==0)
        {
            if(solve(0,total/2,marvels,memory))
            {
                printf("Can be divided.\n\n");
            }
            else
            {
                printf("Can't be divided.\n\n");
            }
        }
        else
            printf("Can't be divided.\n\n");

    }
}
