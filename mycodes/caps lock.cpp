#include<bits/stdc++.h>
using namespace std;
//

string invert(string s)
{
    int i,j;
    for(i=0;i<s.size();i++)
    {
        if(s[i]>='A' && s[i]<='Z')
        {
            s[i]+=32;
        }
        else
        s[i]-=32;
    }
    return s;
}

bool is_mistake(string s)
{
    bool allcaps=1;
    for(int i=1;i<s.size();i++)
    {
        if(s[i]>='a'&&s[i]<='z')
        {

            allcaps=0;
            break;
        }
    }
    return allcaps;
}

int main()
{
    string s;
    cin>>s;
    bool chk=is_mistake(s);
    if(chk)
    {
        s=invert(s);
    }
    cout<<s<<endl;
}
