#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j,mz=-2e9;
    vector<int>v;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        if(j>=0)
        v.push_back(j);
        else
        {
            mz=max(mz,j);
        }
    }

    int res=-1;
    for(i=0;i<v.size();i++)
    {
        double sq=sqrt(v[i]);
        //cout<<ceil(sq)<<" "<<floor(sq)<<endl;
        if(ceil(sq)!=floor(sq))
        {
            res=max(res,v[i]);
        }
    }
    if(res!=-1)
    cout<<res<<endl;
    else
        cout<<mz<<endl;
}
