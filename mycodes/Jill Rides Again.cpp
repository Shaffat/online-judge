#include<bits/stdc++.h>
using namespace std;


struct nice
{
    int val,s,e;
};


nice solve(vector<int>&v)
{
    nice mx,cur;
    mx.val=-1e9;
    mx.s=-1;
    mx.e=-1;
    cur.val=-1e9;
    int i,j;
    for(i=0;i<v.size();i++)
    {
        //cout<<"at pos "<<i<<" cur="<<cur.val<<" th nice="<<v[i]<<" mx is "<<mx.val<<endl;
        if(cur.val+v[i]<v[i]){
            cur.val=v[i];
            cur.s=i;
            cur.e=i+1;
        }
        else
        {
            cur.val+=v[i];
            cur.e=i+1;
        }
        if(cur.val>mx.val){
            mx.val=cur.val;
            mx.s=cur.s;
            mx.e=cur.e;
        }else if(cur.val==mx.val){

            int len1=abs(mx.e-mx.s),len2=abs(cur.e-cur.s);
            if(len1<len2){
                mx.val=cur.val;
                mx.s=cur.s;
                mx.e=cur.e;
            }
        }
    }
    return mx;
}

int main()
{
    int test,t,i,j,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>v;
        for(i=1;i<n;i++)
        {
            scanf("%d",&j);
            v.push_back(j);
        }
        nice res;
        res=solve(v);
        if(res.val<=0){
            printf("Route %d has no nice parts\n",t);
        }
        else
        {
            printf("The nicest part of route %d is between stops %d and %d\n",t,res.s+1,res.e+1);
        }
    }

}
