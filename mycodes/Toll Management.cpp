#include<bits/stdc++.h>
using namespace std;

struct node1
{
    int node,cost;
};

struct node2
{
    int node,cost;
};

bool operator <(node1 a,node1 b)
{
    if(a.cost!=b.cost)
    {
        return a.cost>b.cost;
    }
    return false;
}

bool operator <(node2 a,node2 b)
{
    if(a.cost!=b.cost)
    {
        return a.cost<b.cost;
    }
    return false;
}


void djsktra(int source,vector<vector<int> >&connection,vector<vector<int> >&cost,vector<int>&dis)
{
    node1 cur;
    cur.node=source;
    cur.cost=0;
    vector<int>vis(10001,0);

    priority_queue<node1>pq;
    pq.push(cur);

    dis[source]=0;

    while(!pq.empty())
    {
        cur=pq.top();
        pq.pop();

        int curNode,curCost,i;
        curNode=cur.node;
        curCost=cur.cost;


        if(!vis[curNode])
        {
            vis[curNode]=1;
        }
        else
        {
            continue;
        }

        for(i=0;i<connection[curNode].size();i++)
        {
            int nxtNode,nxtCost;
            nxtNode=connection[curNode][i];
            nxtCost=cost[curNode][i];
            if(dis[nxtNode]>curCost+nxtCost)
            {
                dis[nxtNode]=curCost+nxtCost;
                node1 tmp;
                tmp.node=nxtNode;
                tmp.cost=curCost+nxtCost;
                pq.push(tmp);
            }
        }
    }
    return;
}

int solve(int source,int sink,int p,vector<vector<int> >&connection,vector<vector<int> >&cost,vector<int>&sinkdis)
{
    //cout<<"solve"<<endl;
    int res=-1,counter=0;
    node2 cur;
    cur.node=source;
    cur.cost=0;
    vector<int>vis(10001,0);
    vector<int>dis(10001,1e9);
    priority_queue<node2>pq;
    pq.push(cur);
    dis[cur.node]=0;
    while(!pq.empty())
    {
        counter++;
        if(counter>100000) break;
        cur=pq.top();
        pq.pop();
        //cout<<"curNode="<<cur.node<<endl;

        int curNode,curCost,i;
        curNode=cur.node;
        curCost=cur.cost;

        if(curNode==sink)
        {
            res=max(res,curCost);
        }
        if(pq.size()>100000) continue;
        for(i=0;i<connection[curNode].size();i++)
        {
            int nxtNode,nxtCost;
            nxtNode=connection[curNode][i];
            nxtCost=cost[curNode][i];
            if(sinkdis[nxtNode]+dis[curNode]+nxtCost<=p)
            {
                dis[nxtNode]=min(dis[curNode]+nxtCost,dis[nxtNode]);
                node2 tmp;
                tmp.node=nxtNode;
                tmp.cost=max(curCost,nxtCost);
                pq.push(tmp);
            }
        }
    }
    return res;
}

int main()
{
    int n,m,i,j,p,s,t,test,t1,u,v,w;
    scanf("%d",&test);
    for(t1=1;t1<=test;t1++)
    {
        scanf("%d %d %d %d %d",&n,&m,&s,&t,&p);
        vector<vector<int> >sinkConnection(n+1);
        vector<vector<int> >sinkCost(n+1);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        vector<int>sinkDis(10001,1e9);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost[u].push_back(w);

            sinkConnection[v].push_back(u);
            sinkCost[v].push_back(w);
        }
        djsktra(t,sinkConnection,sinkCost,sinkDis);
        int res=solve(s,t,p,connection,cost,sinkDis);
        printf("Case %d: %d\n",t1,res);
    }

}
