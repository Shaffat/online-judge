#include <bits/stdc++.h>

using namespace std;

int main() {

    long long int n, a, b, p, q, i, sum = 0;
    bool arr[1000000000 + 5] = {0};
    cin >> n >> a >> b >> p >> q;
    if (p > q) {
        sum = p;
        for (i = a; i <= n; i += a) {
            sum += p;
            arr[i] = 1;
        }

        for (i = b; i <= n; i += b) {
            if (arr[i] == 0) {
                sum += q;
            }
        }
    }
    else {
        sum = q;
        for (i = b; i <= n; i += b) {
            sum += q;
            arr[i] = 1;
        }
        for (i = a; i <= n; i += a) {
            sum += p;
        }
    }

    return (0);
}
