#include<bits/stdc++.h>
using namespace std;
string s;
using namespace std;
bool palindrome(int i,int j)
{
    if(i==j ||j<i)
    {
        return true;
    }
    if(s[i]!=s[j])
    {
        return false;
    }
    return palindrome(i+1,j-1);
}
int main()
{
    int i,j;
    map<char,char>mymap;
    mymap['A']='A';
    mymap['E']='3';
    mymap['H']='H';
    mymap['I']='I';
    mymap['J']='L';
    mymap['L']='J';
    mymap['M']='M';
    mymap['O']='O';
    mymap['S']='2';
    mymap['T']='T';
    mymap['U']='U';
    mymap['V']='V';
    mymap['W']='W';
    mymap['X']='X';
    mymap['Y']='Y';
    mymap['Z']='5';
    mymap['1']='1';
    mymap['2']='S';
    mymap['3']='E';
    mymap['5']='Z';
    mymap['8']='8';
    while(getline(cin,s))
    {
        if(s=="")
        {
            return 0;
        }
        bool palin,mirror=0,reverseable=1;
        palin=palindrome(0,s.size()-1);

        string reverstring="";
        for(i=0;i<s.size();i++)
        {
            if(mymap.find(s[i])==mymap.end())
            {
                reverseable=0;
                break;
            }
            else
                reverstring.push_back(mymap[s[i]]);

        }
        reverse(reverstring.begin(),reverstring.end());
        if(palin)
        {
            if(s==reverstring)
            {
                cout<<s<<" -- is a mirrored palindrome.\n";
            }
            else
                cout<<s<<" -- is a regular palindrome.\n";
        }
        else
        {
            if(s==reverstring)
            {
                cout<<s<<" -- is a mirrored string.\n";
            }
            else
                cout<<s<<" -- is not a palindrome.\n";
        }
       printf("\n");
    }
}
