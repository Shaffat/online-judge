#include<bits/stdc++.h>
using namespace std;

#define ll long long int
#define mod 1000000007
#define mx 400000
struct info
{
    ll propagate,val;
};
vector<info>tree(mx);

void build_tree(ll node,ll s,ll e,vector<ll>&v)
{
    info tmp;
    if(s==e)
    {
        tmp.propagate=0;
        tmp.val=v[i];
        return;
    }
    ll left,right,mid;
    left=node*2;
    right=node*2+1;
    mid=(s+e)/2;
    build_tree(left,s,mid,v);
    build_tree(right,mid+1,e,v);
    tmp.propagate=0;
    tmp.val=(tree[left].val+tree[right].val)%mod;
    return;
}

void update(ll node,ll p,ll s,ll e,ll q_s,ll q_e)
{
    if(s>q_e|| e<q_s) return;
    if(s>=q_s && e<=q_e)
    {
        tree[node].propagate+=p;
        tree[node].propagate%=mod;
        tree[node].val+=((e-s+1)*tree[node].propagate)%mod;
        return;
    }
    ll left,right,mid;
    left=node*2;
    right=node*2+1;
    mid=(s+e)/2;
    update(left,p,s,mid,q_s,q_e);
    update(right,p,mid+1,e,q_s,q_e);
    tree[node].val=tree[left].val+tree[right].val+(e-s)*tree[node].propagate;
    return;
}
ll query(ll node,ll p,ll s,ll e,ll q_s,ll q_e)
{
     if(s>q_e|| e<q_s) return 0;
     if(s>q_s && e<=q_e)
     {
         return (tree[node].val+(p*(e-s+1))%mod)%mod;
     }
    ll left,right,mid,ll ans;
    left=node*2;
    right=node*2+1;
    mid=(s+e)/2;
    ans=query(left,p+tree[node].propagate,s,mid,q_s,q_e);
    ans%=mod;
    ans+=query(right,p+tree[node].propagate,mid+1,e,q_s,q_e);
    ans%=mod;
    return ans;
}


ll bigmod(ll n,ll p)
{
    if(p==0) return 1;
    if(p==1) return n;
    ll ans,ans1;
    if(p%2==0)
    {
        ans=bigmod(n,p/2);
        return (ans*ans)%mod;
    }
    else
    {
        ans=bigmod(n,p/2);
        return ((ans*ans)%mod * n)%mod;
    }
}

int main()
{
    ll n,i,j,q,u,t,k,s,total,tmp;
    scanf("%lld",&n);
    vector<ll>v(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        v[i]=j;
    }
    tree_building(1,1,n,v);
    scanf("%lld",&q);
    for(i=1;i<=q;i++)
    {
        scanf("%lld %lld %lld",&u,&t,&k);
        total=0;
        s=query(1,0,1,n,u,t);
        cout<<"s="<<s<<endl;
        total=s;
        tmp=0;
        if(k>1)
        {
           tmp=(bigmod(t-u+1,k-1)*s)%mod;
           cout<<"power="<<k<<" n="<<t-u+1<<" tmp="<<tmp<<endl;
        }
        total+=tmp;
        total%=mod;
        tmp=0;
        if(k>2)
        {
            tmp=(bigmod(t-u+1,k-2)*s)%mod;
        }
        total+=tmp;
        total%=mod;
        cout<<"total="<<total<<endl;
        update(1,total,1,n,u,t);
        cout<<"after update\n";
        for(ll l=1;l<=n;l++)
        {
            cout<<query(1,0,1,n,l,l)<<" ";
        }
        cout<<endl;

    }
    for(i=1;i<=n;i++)
    {
        cout<<"adjust"<<endl;
        v[i]=query(1,0,1,n,i,i);
    }
    for(i=1;i<=n;i++)
    {
        printf("%lld ",v[i]);
    }
}
