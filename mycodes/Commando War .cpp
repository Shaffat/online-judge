#include<bits/stdc++.h>
using namespace std;

struct workdone
{
    int64_t b,t;
};
bool operator <(workdone p, workdone q)
{
    if(p.t>q.t)
    {
        return true;
    }
    return false;
}
int main()
{
    int64_t n,c=1;
    while(scanf("%lld",&n))
    {
        if(n==0)
        {
            break;
        }
         int64_t i,j,b,task;
        vector<workdone> soldiers;
        for(i=1;i<=n;i++)
        {
            workdone tmp;
            scanf("%lld %lld",&tmp.b,&tmp.t);
            soldiers.push_back(tmp);
        }
        sort(soldiers.begin(),soldiers.end());
        int64_t commado=0,taskdone=0;
        //cout<<endl;
        for(i=0;i<soldiers.size();i++)
        {
           //cout<<soldiers[i].b<<" "<<soldiers[i].t<<endl;
            commado+=soldiers[i].b;
            taskdone=max(taskdone,commado+soldiers[i].t);
        }
        //cout<<endl;
        printf("Case %lld: %lld\n",c,taskdone);
        c++;

    }
}
