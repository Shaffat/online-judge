#include<bits/stdc++.h>
using namespace std;

int solve(int node,int equalize,vector<int>&money,vector<bool>&vis,vector<vector<int> >&connection)
{
    int need=equalize-money[node],nxt,i,j;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        if(!vis[nxt]){
        vis[nxt]=1;
        need+=solve(nxt,equalize,money,vis,connection);
        }
    }
    //cout<<"node="<<node<<" need="<<need<<endl;
    return need;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,j,n,m,t,test,u,v,res,total;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        res=1;total=0;
        scanf("%d %d",&n,&m);
        vector<bool>vis(n+1,0);
        vector<vector<int> >connection(n+1);
        vector<int>money(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            money[i]=j;
            total+=j;
        }
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        if(total%n!=0)
        {
            printf("Case %d: No\n",t);
            continue;
        }
        for(i=1;i<=n;i++)
        {
            if(!vis[i])
            {
                vis[i]=1;
                if(solve(i,total/n,money,vis,connection))
                {
                    res=0;
                }
            }
        }
        if(res)
        {
            printf("Case %d: Yes\n",t);
        }
        else
            printf("Case %d: No\n",t);

    }
}
