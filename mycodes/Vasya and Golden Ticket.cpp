#include<bits/stdc++.h>
using namespace std;

bool valid(string &s, long long int value,long long int segment)
{
    //cout<<"val="<<value<<" segment="<<segment<<endl;
    long long int sum=0,i;
    for(i=0;i<s.size();i++)
    {
        if(sum+s[i]-'0'>value)
        {
            return 0;
        }
        else if(sum+s[i]-'0'==value)
        {
            sum=0;
            segment--;
        }
        else
            sum+=s[i]-'0';
    }
    if(segment)
    return 0;
    return 1;
}

int main()
{
    long long int n,i,j,total=0,f=0;

    string s;

    cin>>n>>s;

    for(i=0;i<s.size();i++)
    {
        total+=s[i]-'0';
    }
    if(total==0)
    {
        if(n>1)
        {
            cout<<"YES"<<endl;
        }
        else
            cout<<"NO"<<endl;
        return 0;

    }
    //cout<<"total="<<total<<endl;
    for(i=1;i<=sqrt(total);i++)
    {
        if(f)
            break;
        if(total%i==0)
        {
            if(total/i>1){
            if(valid(s,i,total/i))
            {
                f=1;
            }
            }
            if(i>1){
            if(valid(s,total/i,i))
            {
                f=1;
            }
            }
        }
    }
    if(f)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}
