#include<bits/stdc++.h>

using namespace std;



struct q_ry{
    int start,ending,id,need;
};

bool operator <(q_ry a, q_ry b)
{
    if(a.ending!=b.ending)
        return a.ending<b.ending;
    return a.start>b.start;
}

void update(int val,int idx,int n,vector<int>&tree)
{
    while(idx<=n)
    {
        tree[idx]+=val;
        idx+=(idx&(-idx));
    }
    return;
}

int query(int idx,vector<int>&tree)
{
    int sum=0;
    while(idx>0)
    {
        sum+=tree[idx];
        idx-=(idx &(-idx));
    }
    return sum;
}

int main()
{
    int a,b,c,d,e,n,i,j,m,q;
    map<char,int>Need;
    scanf("%d %d %d %d %d",&a,&b,&c,&d,&e);
    Need['A']=a;
    Need['B']=b;
    Need['C']=c;
    Need['D']=d;
    Need['E']=e;
    scanf("%d %d",&m,&q);
    vector<int>v(m+1);
    vector<int>tree(m+1,0);
    for(i=1;i<=m;i++)
    {
        update(1,i,m,tree);
        scanf("%d",&v[i]);
    }
    vector<q_ry>queries;
    for(i=1;i<=q;i++)
    {
        q_ry tmp;
        char ch;
        scanf(" %c %d %d",&ch,&tmp.start,&tmp.ending);
        tmp.id=i;
        tmp.need=Need[ch];
        //cout<<"Qury is "<<tmp.start<<" "<<tmp.ending<<" "<<tmp.need<<" "<<tmp.id<<endl;
        queries.push_back(tmp);
    }
    sort(queries.begin(),queries.end());
    set<int>s;
    map<int,int>last_pos;
    vector<int>ans(q+1,0);
    int last=0;
    for(i=0;i<queries.size();i++)
    {
        int start=queries[i].start;

//        cout<<"query start="<<queries[i].start<<" ending="<<queries[i].ending<<endl;
//        cout<<"loop starts from "<<last+1<<" to "<<queries[i].ending<<endl;

        for(j=last+1;j<=queries[i].ending;j++)
        {
            //cout<<"does "<<v[j]<<" exist ?"<<endl;
            int f,l;
            f=s.size();
            s.insert(v[j]);
            if(s.size()==f){
                //cout<<"yes last pos was"<<last_pos[v[j]]<<endl;
                update(-1,last_pos[v[j]],m,tree);
                //cout<<"updating with -1"<<endl;
                last_pos[v[j]]=j;
            }
            else
            {
                //cout<<"No. updating last pos"<<endl;
                last_pos[v[j]]=j;
            }
        }

        int cur=query(queries[i].ending,tree)-query(queries[i].start-1,tree);
        //cout<<"ans is "<<cur<<endl;
        if(cur>=queries[i].need){
            ans[queries[i].id]=1;
        }
        last=max(last,queries[i].ending);
    }
    for(i=1;i<=q;i++)
    {
        if(ans[i])
        {
            printf("Yes\n");
        }
        else
            printf("No\n");
    }
}


