#include<bits/stdc++.h>
using namespace std;

bool ispalindrome(string s)
{
    int i,j;
    i=0;
    j=s.size()-1;
    while(i<=j)
    {
        if(s[i]!=s[j])
        {
            return false;
        }
        i++;
        j--;
    }
    return true;
}
int main()
{
    string s;
    cin>>s;
    int chk=0,i;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='A'||s[i]=='H'||s[i]=='I'||s[i]=='M'||s[i]=='O'||s[i]=='T'||s[i]=='U'||s[i]=='V'||s[i]=='W'||s[i]=='X'||s[i]=='Y')
        {
            continue;
        }
        else
        {
            chk=1;
            break;
        }
    }
    if(ispalindrome(s) && chk==0)
    {
        cout<<"YES"<<endl;
    }
    else
        cout<<"NO"<<endl;
}
