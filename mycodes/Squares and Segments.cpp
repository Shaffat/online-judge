#include<bits/stdc++.h>
using namespace std;

long long int solve(long long int n)
{
    long long int first,last,mid,res=0,f=0;
    first=0;
    last=100000;
    while(first<=last)
    {
        //cout<<"first="<<first<<" last="<<last<<endl;
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if((mid*mid)<=n)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}



int main()
{
    long long int n,m,sz,ans;
    cin>>n;
    if(n==1)
    {
        cout<<"2"<<endl;
    }
    else if(n==2)
        cout<<"3"<<endl;
    else if(n==3)
        cout<<"4"<<endl;
    else if(n==4)
        cout<<"4"<<endl;
    else
    {
        sz=solve(n);
        //cout<<"sz="<<sz<<endl;
        ans=4+(sz-2)*2;
        m=n-(sz*sz);
        if(m>0)
        {
            ans++;
            if(m>sz)
                ans++;
        }
        cout<<ans<<endl;
    }
}
