#include<bits/stdc++.h>
using namespace std;


int solve(vector<int>&v)
{
    int i,j,cur=-1e9,mx=-1e9;
    for(i=0;i<v.size();i++){
        cur=max(cur+v[i],v[i]);
        mx=max(cur,mx);
    }
    return mx;
}


int main()
{
    int n,i,j,res;
    while(scanf("%d",&n))
    {
        if(n==0) break;
        vector<int>v;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            v.push_back(j);
        }
        int res=solve(v);
        if(res>0){
            printf("The maximum winning streak is %d.\n",res);
        }
        else
            printf("Losing streak.\n");
    }
}
