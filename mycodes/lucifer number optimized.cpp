#include<bits/stdc++.h>

using namespace std;

vector<bool>primes(100,0);
void sieve()
{
    primes[0]=1;
    primes[1]=1;
    int i,j;
    for(i=4; i<=100; i+=2)
    {
        primes[i]=1;
    }
    for(i=3; i<=sqrt(100); i+=2)
    {
        if(primes[i]==0)
        {
            for(j=i*i; j<=100; j+=i)
            {
                primes[j]=1;
            }
        }
    }
}
string ConvertAndAdjust(int k)
{
    if(k==0)
    {
        return "0";
    }
    string nw="";
    while(k!=0)
    {
        int i=k%10;
        k/=10;
        nw.push_back('0'+i);
    }
    return nw;
}


int solve(string s,int index,int sum, int sign,bool restriction,vector<vector<vector<int> > >&memory)
{

    if(index<0)
    {
        if(primes[sum]==0 && sign==1)
        {
            return 1;
        }
        return 0;
    }
    if(memory[index][sum][sign]!=-1 && restriction==0)
    {
        return memory[index][sum][sign];
    }
    int i,j,realdiff,odd,result=0;
    if(sign)
    {
        realdiff=sum;
    }
    else
        realdiff=sum*-1;
    odd=(index+1)%2;
    if(restriction)
    {
        int range=s[index]-'0';
        int curdiff;
        for(i=0; i<=range; i++)
        {
            if(i<range)
            {
                if(odd)
                {
                    curdiff=realdiff-i;
                }
                else
                    curdiff=realdiff+i;
                if(curdiff>=0)
                {
                    result+=solve(s,index-1,abs(curdiff),1,0,memory);
                }
                else
                    result+=solve(s,index-1,abs(curdiff),0,0,memory);
            }
            else
            {
                if(odd)
                {
                    curdiff=realdiff-i;
                }
                else
                    curdiff=realdiff+i;
                if(curdiff>=0)
                {
                    result+=solve(s,index-1,abs(curdiff),1,1,memory);
                }
                else
                    result+=solve(s,index-1,abs(curdiff),0,1,memory);
            }
        }
    }
    else
    {
        for(i=0; i<=9; i++)
        {
            int curdiff;
            if(odd)
            {
                curdiff=realdiff-i;
            }
            else
                curdiff=realdiff+i;
            if(curdiff>=0)
            {
                result+=solve(s,index-1,abs(curdiff),1,0,memory);
            }
            else
                result+=solve(s,index-1,abs(curdiff),0,0,memory);
        }
    }
    if(restriction)
    {
        return result;
    }
    else
    {
        return memory[index][sum][sign]=result;
    }
}

int main()
{
    sieve();
    vector<int>sign(2,-1);
    vector<vector<int> >sum(90,sign);
    vector<vector<vector<int> > >memory(12,sum);
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int a,b;
        scanf("%d %d",&a,&b);
        string s1,s2;
        a--;
        s1=ConvertAndAdjust(a);
        s2=ConvertAndAdjust(b);
        if(s1.size()!=s2.size())
        {
            if(s1.size()>s2.size())
            {
                s2.push_back('0');
            }
            else
                s1.push_back('0');
        }
        printf("%d\n",solve(s2,s2.size()-1,0,1,1,memory)-solve(s1,s1.size()-1,0,1,1,memory));
    }
}
