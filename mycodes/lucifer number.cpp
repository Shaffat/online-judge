#include<bits/stdc++.h>

using namespace std;

vector<bool>primes(99,0);
void sieve()

{
    int i;
    for(i=1; i<=95; i++)
    {
        primes[i]=0;
    }
    primes[1]=1;
    for(i=4; i<=95; i+=2)
    {
        primes[i]=1;
    }
    int sq=sqrt(95);
    for(i=3; i<=sq; i+=2)
    {
        if (primes[i]==0)
        {
            for(int j=i*i; j<=95; j+=i)
            {
                primes[j]=1;
            }
        }
    }
}

string stringminus(string s)
{
    string nw="";
    int chk=0,i,j;
    for(i=s.size()-1; i>=0; i--)
    {
        if(!chk)
        {
            if(s[i]>'0')
            {
                chk=1;
                nw.insert(nw.begin()+0,s[i]-1);
            }
            else
            {
                nw.insert(nw.begin()+0,'9');
            }
        }
        else
        {
            nw.insert(nw.begin()+0,s[i]);
        }
    }
    return nw;
}

 string adjust(string s)
 {
     while(s[0]=='0')
     {
         s.erase(s.begin());
     }
     reverse(s.begin(),s.end());
     return s;
 }
long long int solve(string s,int index,int differences,int restrict,vector<vector<vector<long long int> > >&memory)
{
    bool sumbit=restrict&(1<<0);
    if(index<0)
    {
        if(sumbit==1 && primes[differences]==0)
        {
            return 1;
        }
        return 0;
    }
    if(memory[index][differences][restrict]!=-1)
    {
        return memory[index][differences][restrict];
    }
    int  restrictbit=restrict&(1<<1);
    int realdiff;
    if(sumbit)
    {
        realdiff=differences;
    }
    else
    {
        realdiff=differences*-1;
    }
    bool odd=(index+1)%2;
    int i,j, range,curdif,result=0;
    if(restrictbit)
    {
        range=s[index]-'0';
        for(i=0;i<=range;i++)
        {
            if(i<range)
            {
                if(odd)
                {
                    curdif=realdiff-i;
                }
                else
                    curdif=realdiff+i;
                if(curdif>=0)
                {
                    result+=solve(s,index-1,abs(curdif),1,memory);
                }
                else
                {
                    result+=solve(s,index-1,abs(curdif),0,memory);
                }
            }
            else
            {
                if(odd)
                {
                    curdif=realdiff-i;
                }
                else
                    curdif=realdiff+i;
                if(curdif>=0)
                {
                    result+=solve(s,index-1,abs(curdif),3,memory);
                }
                else
                {
                    result+=solve(s,index-1,abs(curdif),2,memory);
                }
            }
        }
    }
    else
    {
        for(i=0;i<=9;i++)
        {
            if(odd)
                {
                    curdif=realdiff-i;
                }
                else
                    curdif=realdiff+i;
                if(curdif>=0)
                {
                    result+=solve(s,index-1,abs(curdif),1,memory);
                }
                else
                {
                    result+=solve(s,index-1,abs(curdif),0,memory);
                }
        }
    }
    return memory[index][differences][restrict]=result;

}

int main()
{
    sieve();
    primes[0]=1;
    int test;
    scanf("%d",&test);
    while(test--)
    {
        string a,b;
        cin>>a>>b;
        getchar();
        a=stringminus(a);
        a=adjust(a);
        b=adjust(b);
        vector<long long int>mask(4,-1);
        vector<vector<long long int> >diff(95,mask);
        vector< vector< vector<long long int> > >memory(12,diff);
        vector<long long int>mask1(4,-1);
        vector<vector<long long int> >diff1(95,mask);
        vector< vector< vector<long long int> > >memory1(12,diff);
        printf("%lld\n",solve(b,(b.size()-1),0,3,memory)-solve(a,(a.size()-1),0,3,memory1));



    }

}
