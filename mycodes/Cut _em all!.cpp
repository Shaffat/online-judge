#include<bits/stdc++.h>
using namespace std;

int cmp=0,n;

int dfs(int node,vector<int>&vis,vector<vector<int> >&connection)
{
    //cout<<"node="<<node<<endl;
    int i,j,child,total=1;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(!vis[child])
        {
            vis[child]=1;
            int cur=dfs(child,vis,connection);
            //cout<<"for child="<<child<<" total="<<cur<<endl;
            if(cur%2==0 && (n-cur)%2==0)
            {
                //cout<<"got a component"<<endl;
                cmp++;
//                n-=cur;
            }
            else
                total+=cur;
        }
    }
    //cout<<"node="<<node<<" final total="<<total<<endl;
    return total;
}

int main()
{
    int i,j,u,v;
    scanf("%d",&n);
    vector<vector<int> >connection(n+1);
    for(i=1;i<n;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }
    if(n%2==1)
    {
        printf("-1\n");
        return 0;
    }
    vector<int>vis(n+1,0);
    vis[1]=1;
    dfs(1,vis,connection);
    printf("%d\n",cmp);

    return 0;
}
