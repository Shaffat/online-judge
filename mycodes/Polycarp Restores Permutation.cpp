#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,m,i,j,mn=1e9,mx=-1e9,cur=0,ok=1;
    vector<int>v;
    scanf("%d",&n);
    for(i=1;i<n;i++)
    {
        scanf("%d",&j);
        cur+=j;
        if(j==0) ok=0;
        if(cur<0)
        mn=min(cur,mn);
        else
        mx=max(cur,mx);
        v.push_back(j);
        //cout<<"mn="<<mn<<" mx="<<mx<<endl;
    }
    if(ok==0){
        printf("-1\n");
        return 0;
    }
    if(mx==-1e9) mx=0;
    if(mn==1e9) mn=0;
    if(abs(mx)+abs(mn)==n-1){
        vector<bool>res(n+1,0);
        vector<int>ans;
        int x=1-(mn);
        ans.push_back(x);
        res[x]=1;
        for(i=0;i<v.size();i++)
        {
            x+=v[i];
            if(res[x]==0){
                ans.push_back(x);
            }
            else
                ok=0;
        }
        if(ok){
            for(i=0;i<ans.size();i++)
            {
                printf("%d ",ans[i]);
            }
        }else{
            printf("-1\n");
        }

    }else{
        printf("-1\n");
    }
}
