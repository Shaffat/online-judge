#include<bits/stdc++.h>
using namespace std;
#define ll long long int

void leftmax(ll n,vector<ll>&v,vector<ll>&left)
{
    ll i,j,sum=0,mx=0;
    for(i=1;i<=n;i++)
    {
        sum+=v[i];
        mx=max(mx,sum);
        left[i]=max(sum,mx);
        //cout<<"left["<<i<<"]="<<left[i]<<endl;
    }
    return;
}
void rightmax(ll n,vector<ll>&v,vector<ll>&right)
{
    ll i,j,sum=0,mx=0;
    for(i=n;i>0;i--)
    {
        sum+=v[i];
        mx=max(mx,sum);
        right[i]=max(sum,mx);
        //cout<<"right["<<i<<"]="<<right[i]<<endl;
    }
    return;
}
int main()
{
    //freopen("in.txt","r",stdin);
    ll n,i,j,sum=0,mx=0;
    scanf("%lld",&n);
    vector<ll>v(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        v[i]=j;
    }
    vector<ll>left(n+2,0);
    vector<ll>right(n+2,0);
    right[n+1]=0;
    left[n+1]=0;
    leftmax(n,v,left);
    rightmax(n,v,right);
    for(i=1;i<=n;i++)
    {
        ll cur1=left[i-1]+right[i];
        ll cur2=left[i]+right[i+1];
        //cout<<"at "<<i<<" cur1="<<cur1<<" cur2="<<cur2<<endl;
        mx=max(mx,max(cur1,cur2));
    }

    printf("%lld\n",mx);
}
