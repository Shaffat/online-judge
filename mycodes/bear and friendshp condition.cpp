#include<bits/stdc++.h>

using namespace std;

int main()
{
    int n,m,i,j,start;
    scanf("%d %d",&n,&m);
    vector<int>edges[n+1];
    vector<int> vis(n+1,0);
    for(i=1;i<=m;i++)
    {
        int k,l;
        scanf("%d %d",&k,&l);
        edges[k].push_back(l);
        edges[l].push_back(k);
        start=k;
    }
    queue<int>q;
    q.push(start);
    while(!q.empty())
    {
        int cur_top=q.front();
        q.pop();
        if(!vis[cur_top])
        {
            vis[cur_top]=1;
            for(i=0;i<edges[cur_top].size();i++)
            {
                if(!vis[edges[cur_top][i]])
                {
                    cout<<"entering in queue = "<<edges[cur_top][i]<<endl;
                    q.push(edges[cur_top][i]);
                }
            }
        }
    }
    int chk=0;
    for(i=1;i<=n;i++)
    {
        if(vis[i]==0)
        {
            chk=1;
            break;
        }
    }
    if(chk)
    {
        printf("NO\n");
    }
    else
    {
        printf("YES\n");
    }
}
