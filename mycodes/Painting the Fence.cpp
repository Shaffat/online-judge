#include<bits/stdc++.h>
using namespace std;

struct job
{
    int l,r;
};

struct propagate
{
    int p,sum;
};


int query(int node,int l,int r,int q_l,int q_r,vector<propagate>&tree)
{
    int total=0;
    if(q_l>r || q_r<l)
    {
        return 0;
    }
    if(l>=q_l && r<=q_r)
    {
        total=tree[node].sum+(tree[node].p*(r-l+1));
        return total;
    }
    int mid=(l+r)/2;
    total+=query(2*node,l,mid,q_l,q_r,tree);
    total+=query(2*node + 1,mid+1,r,q_l,q_r,tree);
    return total;
}

void update(int node,int val,int l,int r,int q_l,int q_r,vector<propagate>&tree)
{
    if(q_l>r || q_r<l)
    {
        return ;
    }
    if(l>=q_l && r<=q_r)
    {
        tree[node].sum+=(r-l+1)*val;
        tree[node].p+=val;
        return;
    }
    int mid=(l+r)/2;
    update(2*node,val,l,mid,q_l,q_r,tree);
    update(2*node+1,val,mid+1,r,q_l,q_r,tree);
    tree[node].sum=tree[2*node].sum+tree[2*node+1].sum+(r-l+1)*tree[node].p;
    return;
}

void build_tree(int node,int l,int r,vector<propagate>&tree)
{
    if(l==r)
    {
        propagate tmp;
        tmp.p=0;
        tmp.sum=0;
        tree[node]=tmp;
        return;
    }
    int mid=(l+r)/2;
    build_tree(2*node,l,mid,tree);
    build_tree(2*node + 1,mid+1,r,tree);
    return;
}

int main()
{
    int n,i,j,k,l,q,overlap=0,total=0;
    scanf("%d %d",&n,&q);
    vector<int>ovrlp(q+1,0);
    vector<job>worker(q+1);
    vector<int>own_lap(q+1,0);
    vector<propagate>tree(4*q);
    for(i=1;i<=q;i++)
    {
        job tmp;
        scanf("%d %d",&tmp.l, &tmp.r);
        update(1,1,1,q,tmp.l,tmp.r,tree);
        worker.push_back(tmp);
    }
    for(i=1;i<=q;i++)
    {
        for(j=worker[i].l;j<=worker[i].r;j++)
        {
            total++;
            ovrlp[j]++;
            if(ovrlp[j]>1)
            {
                overlap++;
            }
        }
    }
    for(i=1;i<=q;i++)
    {
        int tmp=0;
        for(j=worker[i].l;j<=worker[i].r;j++)
        {
            if(ovrlp[j]>1)
            {
                tmp++;
            }
        }
        own_lap[i]=tmp;
    }
    int mx=0;
    cout<<"total="<<total<<" overlap="<<overlap<<" "<<query(1,1,q,1,n,tree)<<endl;
    for(i=1;i<=q;i++)
    {
        update(1,-1,1,q,worker[i].l,worker[i].r,tree);
        for(j=1;j<=q;j++)
        {
            update(1,-1,1,q,worker[j].l,worker[j].r,tree);
            int ans=query(1,1,q,1,n,tree);

            ans-=(overlap-own_lap[i]-own_lap[j]);
            mx=max(mx,ans);
            update(1,1,1,q,worker[j].l,worker[j].r,tree);
        }
    }
    printf("%d\n",mx);

}
