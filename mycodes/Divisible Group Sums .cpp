#include<bits/stdc++.h>
using namespace std;
#define ll long long int
ll d;
ll solve(ll pos,ll need,ll reminder,ll sign,vector<ll>&numbers,vector<vector<vector<vector<ll> > > >&memory)
{
//    cout<<"d="<<d<<endl;
//    cout<<"pos="<<pos<<" need="<<need<<" reminder="<<reminder<<" sign="<<sign<<endl;
    if(need==0)
    {
        if(reminder==0)
        {
            return 1;
        }
        else
            return 0;
    }
    if(pos>=numbers.size())
    {
        return 0;
    }
    if(memory[pos][need][reminder][sign]!=-1)
    {
        return memory[pos][need][reminder][sign];
    }
    ll res=0;
    ll cur_reminder,cur_num,next_reminder,next_sign;
    if(sign==1)
    {
        cur_reminder=-1*reminder;
    }
    else
        cur_reminder=reminder;
    cur_num=numbers[pos];
    //cout<<" cur_num="<<cur_num;
    cur_num%=d;
    //cout<<" after mod cur_num="<<cur_num;
    next_reminder=cur_num+cur_reminder;
    next_reminder%=d;
    //cout<<" next reminder="<<next_reminder<<endl;
    if(next_reminder<0)
    {
        next_reminder*=-1;
        next_sign=1;
    }
    else
        next_sign=0;

    res+=solve(pos+1,need-1,next_reminder,next_sign,numbers,memory);
    res+=solve(pos+1,need,reminder,sign,numbers,memory);
    //cout<<"memory["<<pos<<"]["<<need<<"]["<<reminder<<"]["<<sign<<"]="<<res<<endl;
    return memory[pos][need][reminder][sign]=res;
}
int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll test,t;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        ll n,q,i,j,m,res;
        scanf("%lld %lld",&n,&q);
        vector<ll>numbers;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            numbers.push_back(j);
        }
        printf("Case %lld:\n",t);
        for(i=1;i<=q;i++)
        {
            vector<ll>sign(2,-1);
            vector<vector<ll> >col1(20,sign);
            vector<vector<vector<ll> > >col3(11,col1);
            vector<vector<vector<vector<ll> > > >memory(n,col3);
            scanf("%lld %lld",&d,&m);
            res=solve(0,m,0,0,numbers,memory);
            printf("%lld\n",res);
        }
    }
}
