#include<bits/stdc++.h>
using namespace std;

int solve(int index,int m,vector<int>&enemy,vector<int>&memory)
{
    if(index>=enemy.size())
    {
        return 0;
    }
    if(memory[index]!=-1)
    {
        return memory[index];
    }
    int res1,res2;
    res1=solve(index+m,m,enemy,memory)+enemy[index];
    res2=solve(index+1,m,enemy,memory);
    return memory[index]=max(res1,res2);
}

int main()
{
    int n,m,i,j,res;
    scanf("%d %d",&n,&m);
    vector<int>enemy;
    vector<int>memory(n,-1);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        enemy.push_back(j);
    }
    res=solve(m,m,enemy,memory);
    printf("%d\n",res);
}
