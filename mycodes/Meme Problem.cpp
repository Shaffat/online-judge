#include<bits/stdc++.h>

using namespace std;



double solve(double d)
{
    double first=1,last=2,mid,a=0;
    int t=1;
    while(t<=300)
    {
       if(first>last) break;
       mid=(first+last)/2;
       //cout<<"mid="<<mid<<" and "<<d-mid<<endl;
       if(d-mid<0) break;
       double mul=mid*(d-mid);
       //cout<<"mul="<<mul<<endl;
       if(mul<= d)
       {
           a=mid;
           first=mid;
       }
       else
       {
          last=mid;
       }
        t++;
    }
    return a;
}


int main()
{
    int test;
    double d,a;
    scanf("%d",&test);
    while(test--)
    {
        scanf("%lf",&d);
        a=solve(d);
        double multi=a*(d-a);
        if(abs(multi-d)<=1e-7)
        {
            //cout<<a<<" mul="<<multi<<" add="<<d-a+a<<endl;
            printf("Y %.10lf %.10lf\n",d-a,a);
        }
        else
            printf("N\n");
    }
}
