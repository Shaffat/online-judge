#include<bits/stdc++.h>
using namespace std;

int sum(int n)
{
    int s=0;
    while(n>0)
    {
        s+=n%10;
        n/=10;
    }
    return s;
}

int main()
{
    int n,i,j,a;
    cin>>a;
    for(i=a;i<=100000000;i++)
    {
        if(sum(i)%4==0)
        {
            n=i;
            break;
        }
    }
    cout<<n;
}
