#include<bits/stdc++.h>

using namespace std;

bool ar[10000001];

void sieve(int N)

{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[0]=1;
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}

int main()
{
    sieve(10000000);
    vector<int>primes;
    primes.push_back(2);
    for(int i=3;i<10000000;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
    int test;

        while(scanf("%d",&test)!=EOF)
        {

        int k=1;
        while(k<=test)
        {

            int gold,counter=0,i=0;
            scanf("%d",&gold);
            while(primes.at(i)<=gold/2)
            {
                if(ar[gold-primes.at(i)]==0)
                {
                    counter++;
                }
                i++;
            }
            printf("Case %d: %d\n",k,counter);
            k++;

        }
    }
}


