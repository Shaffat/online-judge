#include<bits/stdc++.h>
using namespace std;

#define ll long long int
#define mod 1000000007
void multiplication(vector<vector<ll> >&a,vector<vector<ll> >&b,vector<vector<ll> >&res,ll m)
{
    ll i,j,k;
    for(i=0;i<a.size();i++)
    {
        for(j=0;j<b[0].size();j++)
        {
            ll sum=0;
            for(k=0;k<b.size();k++)
            {
                sum+=(a[i][k]*b[k][j])%m;
                sum%=m;
            }
            res[i][j]=sum;
        }
    }
    return ;
}
void power(ll p,vector<vector<ll> >&base,vector<vector<ll> >&res,ll m)
{
    ll i,j;

    if(p==1)
    {
        for(i=0;i<base.size();i++)
        {
            for(j=0;j<base[0].size();j++)
            {
                res[i][j]=base[i][j];
            }
        }
        return;
    }
    else if(p%2==0)
    {
        power(p/2,base,res,m);
        vector<ll>col(2);
        vector<vector<ll> >tmp(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,res,m);
    }
    else
    {
        power(p/2,base,res,m);
        vector<ll>col(2);
        vector<vector<ll> >tmp(2,col);
        vector<vector<ll> >tmp2(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,tmp2,m);
        multiplication(tmp2,base,res,m);
    }
    return;
}

ll solve(ll n)
{
    if(n==0) return 0;
    if(n==1) return 1;
    vector<ll>col(1);
    vector<vector<ll> >matrix(2,col);
    vector<vector<ll> >final_res(2,col);
    matrix[0][0]= 1;
    matrix[1][0]= 0;

    vector<ll>col1(2);
    vector<vector<ll> >base(2,col1);
    vector<vector<ll> >res(2,col1);
    base[0][0]=1;
    base[0][1]=1;
    base[1][0]=1;
    base[1][1]=0;

    power(n-1,base,res,mod);
    multiplication(res,matrix,final_res,mod);
    return final_res[0][0];
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll n,i,j;
    cin>>n;
    cout<<solve(n)<<endl;
}
