#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>col(1001,-1);
vector<vector<ll> >memory(51,col);
vector<vector<ll> >testcase(51,col);
ll t;
ll solve(ll index,ll need,vector<ll>&coins,vector<ll>&amount)
{
    if(need==0)
    {
        return 1;
    }
    if(index>=coins.size()||need<0)
    {
        return 0;
    }
    if(testcase[index][need]==t)
    {
        return memory[index][need];
    }
    ll case1=0,case2=0,i;
    for(i=1;i<=amount[index];i++)
    {
        case1+=solve(index+1,need-(coins[index]*i),coins,amount);
        case1%=100000007;
    }
    case2=solve(index+1,need,coins,amount);
    case2%=100000007;
    testcase[index][need]=t;
    return memory[index][need]=(case1+case2)%100000007;
}

int main()
{
    ll i,j,test;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        vector<ll>coins;
        vector<ll>amount;
        ll n,k,res;
        scanf("%lld %lld",&n,&k);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            coins.push_back(j);
        }
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            amount.push_back(j);
        }
        res=solve(0,k,coins,amount);
        printf("Case %lld: %lld\n",t,res);
    }
}
