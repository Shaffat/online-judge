#include<bits/stdc++.h>

using namespace std;

void bfs(int node,vector<vector<int> >&connection,vector<int>&distance,vector<int>&vis)
{
    distance[node]=1;
    vis[node]=1;
    queue<int>q;
    q.push(node);
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        for(int i=0;i<connection[cur].size();i++)
        {
            int nxt=connection[cur][i];
            if(vis[nxt]==0)
            {
                vis[nxt]=1;
                distance[nxt]=distance[cur]+1;
                q.push(nxt);
            }
        }
    }
    return;
}

int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<vector<int> >connection(n+1);
    vector<int>distance(n+1,0);
    vector<int>vis(n+1,0);
    vector<int>roots;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        if(j!=-1)
        {
            connection[j].push_back(i);
        }
        else
            roots.push_back(i);

    }
    for(i=0;i<roots.size();i++)
    {

            bfs(roots[i],connection,distance,vis);
    }
    int mx=0;
    for(i=1;i<=n;i++)
    {
        //cout<<"i="<<distance[i]<<endl;
        mx=max(mx,distance[i]);
    }
    cout<<mx<<endl;
}
