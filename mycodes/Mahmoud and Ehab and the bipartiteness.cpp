#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j;
    cin>>n;
    vector<int>ed;
    vector<vector<int> >graph(n+1,ed);
    vector<int>colour1(n+1,-1);
    vector<int>coloured1(n+1,0);
    vector<int>vis(n+1,0);
    for(i=1;i<n;i++)
    {
        int u,v;
        scanf("%d %d",&u,&v);
        graph[u].push_back(v);
        graph[v].push_back(u);
    }

    int bi=1;
    for(int k=1;k<=n;k++)
    {
        if(bi==0)
        {
            break;
        }
        if(vis[k]==0)
        {
            //cout<<"checking"<<endl;
            colour1[k]=1;
            coloured1[k]=1;
            queue<int>bfs;
            bfs.push(k);
            while(!bfs.empty())
            {
                int current=bfs.front();
                vis[current]=1;
                bfs.pop();
                for(i=0;i<graph[current].size();i++)
                {
                    int node1=graph[current][i];
                    if(coloured1[node1]==1)
                    {
                        if(colour1[node1]==colour1[current])
                        {
                            bi=0;
                            break;
                        }

                    }
                    else
                    {
                        coloured1[node1]=1;
                        colour1[node1]=(colour1[current]+1)%2;
                        bfs.push(node1);
                    }
                }
            }
        }
    }
    int uncoloured=0;
    int ar[2]={0,0};
    for(i=1;i<=n;i++)
    {
        if(colour1[i]!=-1)
        {
            ar[colour1[i]]++;
        }
        if(coloured1[i]==0)
        {
            uncoloured++;
        }
    }
//    cout<<uncoloured<<endl;
//    cout<<set1.size()<<" "<<set2.size()<<endl;
    if(bi==0)
    {
        cout<<"0"<<endl;
        return 0;
    }
    long long int total,g,h;
    g=ar[0];
    h=ar[1];
    for(i=1;i<=uncoloured;i++)
    {
        if(g==h)
        {
            g++;
        }
        else if(g>h)
        {
            h++;
        }
        else
        {
            g++;
        }

    }
    //cout<<set1.size()<<" "<<set2.size()<<endl;

    cout<<(g*h)-n+1<<endl;

}
