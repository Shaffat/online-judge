#include<bits/stdc++.h>
using namespace std;

string lcsString(int l1,int l2,string &s1,string &s2,vector<vector<string> >&memory)
{
    if(l1>=s1.size()||l2>=s2.size())
    {
        return "";
    }
    string ans1="",ans2="",ans3="";
    if(s1[l1]==s2[l2])
    {
        ans1=lcsString(l1+1,l2+1,s1,s2,memory);
        ans1.push_back(s1[l1]);
    }
    ans2=lcsString(l1+1,l2,s1,s2,memory);
    ans3=lcsString(l1,l2+1,s1,s2,memory);
    if(ans1)
}

void counting(string &s,vector<int>&v)
{
    int i,j;
    for(i=0;i<s.size();i++)
    {
        j=s[i];
        v[j]++;
    }
    return;
}

bool impossible(vector<int>&v1,vector<int>&v2,vector<int>&need)
{
    int i,j;
    for(i=1;i<150;i++)
    {
        if(v1[i]>v2[i])
            return true;
        else
            need[i]=v2[i]-v1[i];
    }
    return false;
}

bool ok(vector<int>&need,vector<int>&have)
{
    int i,j;
    for(i=1;i<150;i++)
    {
        if(need[i]>have[i])
            return false;
    }
    return true;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s,t,p;
        cin>>s>>t>>p;
        vector<int>sCount(200,0);
        vector<int>tCount(200,0);
        vector<int>pCount(200,0);
        vector<int>need(200,0);
        counting(s,sCount);
        counting(t,tCount);
        counting(p,pCount);
        if(impossible(sCount,tCount,need))
        {
            cout<<" 1 NO\n";
        }
        else{
            if(ok(need,pCount))
            {
                cout<<"YES\n";
            }
            else
                cout<<"NO\n";
        }
    }
}
