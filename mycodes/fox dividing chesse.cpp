#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()
{
    int a,b,caneat;
    scanf("%d %d",&a,&b);
    caneat=GCD(a,b);
    int m1=a/caneat,m2=b/caneat;
    int canteat=0;
    int mov=0;
    while(m1!=1&&canteat==0)
    {
        //cout<<"m1="<<m1<<" mov="<<mov<<endl;
      if(m1%2!=0&&m1%3!=0&&m1%5!=0)
      {
          canteat=1;
      }
      if(m1%5==0)
      {
          m1/=5;
          mov++;
      }
      else if(m1%3==0)
      {
          m1/=3;
          mov++;
      }
      else if(m1%2==0)
      {
          m1/=2;
          mov++;
      }
    }
    while(m2!=1&&canteat==0)
    {
      //cout<<"m2= "<<m2<<endl;
      if(m2%2!=0&&m2%3!=0&&m2%5!=0)
      {
          canteat=1;
      }
      if(m2%5==0)
      {
          m2/=5;
          mov++;
      }
      else if(m2%3==0)
      {
          m2/=3;
          mov++;
      }
      else if(m2%2==0)
      {
          m2/=2;
          mov++;
      }
    }
    if(canteat)
    {
        cout<<"-1"<<endl;
    }
    else
        cout<<mov<<endl;
}
