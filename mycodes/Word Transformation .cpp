#include<bits/stdc++.h>

using namespace std;

int compare(string s1,string s2)
{
    if(s1.size()!=s2.size())
    {
        return 2e9;
    }
    int i,j,chng=0;
    for(i=0;i<s1.size();i++)
    {
        if(s1[i]!=s2[i])
            chng++;
    }
    return chng;
}

int solve(int source_index,int final_index,vector<string>words)
{
    int i,j;
    vector<int>vis(words.size(),0);
    vector<int>dis(words.size(),0);
    queue<int>q;
    vis[source_index]=1;
    q.push(source_index);
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        if(words[cur]==words[final_index])
        {
            return dis[final_index];
        }
        for(i=0;i<words.size();i++)
        {
            if(!vis[i])
            {
                if(compare(words[i],words[cur])==1)
                {
                    q.push(i);
                    vis[i]=1;
                    dis[i]=dis[cur]+1;
                }
            }
        }
    }
    return -1;
}

int main()
{
//  freopen("in.txt","r",stdin);
//  freopen("out.txt","w",stdout);
  int i,j,test,t=1;
  scanf("%d",&test);
  while(t<=test)
  {
      if(t>1)
      {
          printf("\n");
      }
      //cout<<"i am here\n";
      string s,query;
      vector<string>words;
      while(cin>>s)
      {
          if(s=="*")
          {
              break;
          }
          //cout<<s<<endl;
          words.push_back(s);
      }
      getchar();
      while(getline(cin,query))
      {
          string start="",ending="";
//          cout<<"query="<<query<<endl;
          if(query=="")
          {
              break;
          }
          bool chk=0;
          for(i=0;i<query.size();i++)
          {
              if(query[i]==' ')
              {
                  chk=1;
                  continue;
              }
              if(!chk)
              {
                  start.push_back(query[i]);
              }
              else
                  ending.push_back(query[i]);
          }
          int startindex,endingindex;
          for(i=0;i<words.size();i++)
          {
              if(start==words[i])
              {
                  startindex=i;
              }
              if(ending==words[i])
              {
                  endingindex=i;
              }
          }
          int res=solve(startindex,endingindex,words);
          //cout<<"res="<<res<<endl;
          cout<<start<<" "<<ending<<" "<<res<<"\n";
      }
      t++;
  }

}
