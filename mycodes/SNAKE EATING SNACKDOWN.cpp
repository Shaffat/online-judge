#include<bits/stdc++.h>
using namespace std;

int can_eat(vector<int>&v,int key,int startpoint,int endpoint)
{
    int first=startpoint,last=endpoint,mid,result=0,f=0;
    while(first<=last)
    {
        if(first==last)
        {
            f++;
        }
        mid=(first+last)/2;
        if(v[mid]<key)
        {
            result=mid;
            first=mid+1;
        }
        else
        {
            last=mid-1;
        }
        if(f)
        {
            break;
        }
    }
    return result;
}
int main()
{
     int test;
     scanf("%d",&test);
     while(test--)
     {
         int n,q,i,j;
         vector<int>snakes;

         snakes.push_back(-1);
         scanf("%d %d",&n,&q);
         for(i=1;i<=n;i++)
         {
             int k;
             scanf("%d",&k);
             snakes.push_back(k);
         }
         sort(snakes.begin(),snakes.end());
         for(i=1;i<=q;i++)
         {
             int k;
             scanf("%d",&k);
             int result=0,available=0;
             int lower_index=can_eat(snakes,k,1,n);

             result=snakes.size()-1-lower_index;
//             cout<<"initial result="<<result<<endl;

             int f=0;
//             cout<<snakes[0]<<endl;
//             cout<<"f="<<f<<endl;
             for(j=lower_index;j>=f;j--)
             {
                f+=k-snakes[j];
                // cout<<"f="<<f<<" j="<<j<<endl;
                 if(j>f)
                 {
                     result++;
                 }

             }
             printf("%d\n",result);

         }

     }
}
