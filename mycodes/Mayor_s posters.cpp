#include<bits/stdc++.h>
using namespace std;

#include<stdio.h>
#include<vector>
#include<iostream>
#include<math.h>
#include<algorithm>
using namespace std;
#define ll long long int

struct info
{
    ll p,val;
};
info tree[40000000];



void update(ll node,ll val,ll s,ll e,ll q_s,ll q_e)
{
    if(s>q_e||e<q_s)
    {
        return;
    }
    if(s>=q_s && e<=q_e)
    {
        tree[node].p+=val;
        //cout<<"before ="<<tree[node].val<<endl;
        tree[node].val+=val*(e-s+1);
         //cout<<"add propagtion at node "<<node<<" s="<<s<<" e="<<e<<" val="<<tree[node].val<<" p="<<tree[node].p<<endl;
        return;
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    update(left,val,s,mid,q_s,q_e);
    update(right,val,mid+1,e,q_s,q_e);
    tree[node].val=tree[left].val+tree[right].val+(e-s+1)*tree[node].p;
    //cout<<"updated value at node "<<node<<" s="<<s<<" e="<<e<<" total="<<tree[node].val<<endl;
}


ll query(ll node,ll s,ll e,ll q_s,ll q_e,ll carry)
{
   // cout<<"node="<<node<<" s="<<s<<" e="<<e<<" carry="<<carry<<endl;
    if(s>q_e||e<q_s)
    {
        return 0;
    }
    if(s>=q_s && e<=q_e)
    {
        return tree[node].val+carry*(e-s+1);
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    return query(left,s,mid,q_s,q_e,carry+tree[node].p)+query(right,mid+1,e,q_s,q_e,carry+tree[node].p);
}


void build_tree(ll node,ll start,ll ending,vector<ll>&values)
{

    ll left,right,mid;
    if(start==ending)
    {
        tree[node].p=0;
        tree[node].val=values[start];
        //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
        return;
    }
    left=(node<<1);
    right=(node<<1)+1;
    mid=(start+ending)/2;
    build_tree(left,start,mid,values);
    build_tree(right,mid+1,ending,values);
    tree[node].p=0;
    tree[node].val=tree[left].val+tree[right].val;
     //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
    return;
}


int main()
{

}
