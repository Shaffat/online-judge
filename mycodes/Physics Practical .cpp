#include<bits/stdc++.h>
using namespace std;

int main()
{
    freopen("input.txt","r",stdin);
    freopen("output.txt","w",stdin);
    int n,i,j;
    scanf("%d",&n);
    vector<int>v;
    for(i=0;i<n;i++)
    {
        scanf("%d",&j);
        v.push_back(j);
    }
    sort(v.begin(),v.end());
    int mx=-1;
    for(i=0;i<v.size();i++)
    {
        vector<int>::iterator low;
        int target=2*v[i]+1;
        low=lower_bound(v.begin()+i,v.end(),target);
        int index=low-v.begin();
        mx=max(mx,index-i);
    }
    cout<<n-mx<<endl;
}
