#include<bits/stdc++.h>
using namespace std;

int main()
{
    int t,n,sum;
    scanf("%d",&t);
    while(t--)
    {
        scanf("%d",&n);
        sum=0;
        while(n>0)
        {
            sum+=n%10;
            n/=10;
        }
        printf("%d\n",sum);
    }
}
