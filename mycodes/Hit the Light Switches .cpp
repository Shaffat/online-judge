#include<bits/stdc++.h>
using namespace std;


void bfs(int source,vector<vector<int> >&connection,vector<int>&lights)
{

   lights[source]=1;
   queue<int>q;
   q.push(source);
   while(!q.empty())
   {
       int cur=q.front();
       q.pop();
       int i,j;
       for(i=0;i<connection[cur].size();i++)
       {
           int nxt=connection[cur][i];
           if(lights[nxt]==0)
           {
               lights[nxt]=1;
               q.push(nxt);
           }
       }
   }
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int t=1,test;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,m,i,j,u,v;
        scanf("%d %d",&n,&m);
        vector<int>lights(n+1,0);
        vector<int>manual(n+1,0);
        vector<vector<int> >connection(n+1);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
        }
        vector<int>nxtbfs;
        int switc=0,res;
        for(i=1;i<=n;i++)
        {
            if(lights[i]==0)
            {
                nxtbfs.push_back(i);
                bfs(i,connection,lights);
            }
        }
        vector<int>vis(n+1,0);
        for(i=nxtbfs.size()-1;i>=0;i--)
        {
            if(vis[nxtbfs[i]]==0)
            {
                switc++;
                bfs(nxtbfs[i],connection,vis);
            }
        }
        printf("Case %d: %d\n",t,switc);
        t++;
    }

}
