#include<bits/stdc++.h>
using namespace std;


int how_much(int n,vector<int>&rider)
{
    int first=0,last=rider.size()-1,mid,f=0,res=-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        //cout<<"mid="<<mid<<" val="<<rider[mid]<<" need="<<n<<endl;
        if(rider[mid]<=n)
        {
            res=mid;
            //cout<<"res="<<mid<<endl;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
    int n,m,i,j;
    scanf("%d %d",&n,&m);
    vector<int>driver;
    vector<int>rider;
    vector<int>all;
    for(i=1;i<=n+m;i++)
    {
        scanf("%d",&j);
        all.push_back(j);
    }
    for(i=0;i<n+m;i++)
    {
        scanf("%d",&j);
        if(j==1)
            driver.push_back(all[i]);
        else
            rider.push_back(all[i]);
    }
    vector<int>caller(m+1,0);
    int before=0,after;
    for(i=0;i<driver.size();i++)
    {
       if(i==m-1)
       {
           after=1e9;
       }
       else
       {
           int total=driver[i+1]-driver[i]-1;
           if(total%2==0)
                after=driver[i]+total/2;
           else
                after=driver[i]+(total/2)+1;
       }
       int x,y;
       //cout<<"before="<<before<<" after="<<after<<endl;
       x=how_much(before,rider)+1;
       y=how_much(after,rider)+1;
       //cout<<"x="<<x<<" y="<<y<<endl;
       caller[i]=y-x;
       before=after;

    }
    for(i=0;i<m;i++)
    {
        printf("%d ",caller[i]);
    }

}
