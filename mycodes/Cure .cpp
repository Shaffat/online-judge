#include<stdio.h>
#include<math.h>
#include<vector>
#include<iostream>
#include<algorithm>

using namespace std;

int main()
{
    unsigned long long int test,t,i,j,n;

    vector<double>ans(1000001);
    ans[1]=1;
    for(i=2;i<=1000000;i++)
    {
        double m=i;
        ans[i]=ans[i-1]+(1/(m*m));
        //cout<<"i="<<i<<" ans="<<ans[i]<<endl;
    }
    while(scanf("%llu",&n)==1)
    {
        n=min(1000000llu,n);
        printf("%.5lf\n",ans[n]);
    }

}
