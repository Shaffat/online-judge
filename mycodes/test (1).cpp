#include<bits/stdc++.h>
using namespace std;



double solve(int n,string &x,string &y,vector<vector<int> >&transaction)
{
    int i,j;
    int haveX=0,haveY=0;

    for(i=0;i<n;i++)
    {
        int ok=1,cnt=0,ok2=1;
        for(j=0;j<x.size();j++)
        {
            int cur=x[j];
            if(!transaction[i][cur])
            {
                ok=0;
                break;
            }
        }
        if(ok)
        {
            //cout<<i<<" have "<<x<<endl;
            haveX++;
                for(j=0;j<y.size();j++)
                {
                    int cur=y[j];
                    //cout<<"check for "<<cur<<" in "<<i<<" ="<<transaction[i][cur]<<endl;
                    if(!transaction[i][cur])
                    {
                        ok2=0;
                        break;
                    }
                }
            if(ok2)
            {
                //cout<<i<<" have "<<y<<endl;
                haveY++;
            }
        }
    }
    //cout<<haveY<<"/"<<haveX<<endl;
    return (double) haveY/haveX;
}


int main()
{
    //ios_base::sync_with_stdio(0);
    //cin.tie(0);
    int n,m,i,j,test,t;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cout<<"Case "<<t<<":"<<endl;
        cin>>n>>m;
        string s,x,y;
        vector<int>col(130,0);
        vector<vector<int> >transactions(n+1,col);
        for(i=0;i<n;i++)
        {
            cin>>s;
            for(j=0;j<s.size();j++)
            {
                int c=s[j];
                //cout<<"c="<<c<<endl;
                transactions[i][c]=1;
            }
        }
//        for(i=0;i<3;i++)
//        {
//            for(j=65;j<=69;j++)
//            {
//                cout<<transactions[i][j]<<" ";
//            }
//            cout<<endl;
//        }
//        cout<<"cj "<<transactions[2][66]<<endl;
        for(i=1;i<=m;i++)
        {
            cin>>x>>y;
            double res=solve(n,x,y,transactions);

            cout <<setprecision(2) << fixed <<res<<endl;
        }
    }
}
