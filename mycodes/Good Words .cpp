#include<bits/stdc++.h>
using namespace std;

bool ok(string &text,string &patter){
    int i,j;
    if(patter.size()>text.size()) return false;
    int done=0;
    for(i=0;i<text.size();i++){
        for(j=0;j<patter.size();j++){
                //cout<<"i="<<i<<" j="<<j<<endl;
            if(text[i+j]==patter[j]){
                if(j==patter.size()-1){
                    return true;
                }
            }
            else
                break;
        }
    }
    return false;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j;
    cin>>test;
    for(t=1;t<=test;t++){
        string s1,s2;
        cin>>s1>>s2;
        vector<string>v;
        for(i=0;i<s2.size();i++){
            string tmp;
            for(j=0;j<s2.size();j++){
                if(i==j)continue;
                tmp.push_back(s2[j]);
            }
            v.push_back(tmp);
        }
        if(ok(s1,s2)){
            cout<<"good"<<endl;
        }
        else{
            int done=0;
            for(i=0;i<v.size();i++){
                if(ok(s1,v[i])){
                    done=1;
                    break;
                }
            }
            if(done){
                cout<<"almost good"<<endl;
            }else{
                cout<<"none"<<endl;
            }
        }
    }

}
