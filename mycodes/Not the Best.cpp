#include<bits/stdc++.h>

using namespace std;

struct node
{
    int vertex,first,second;
};

bool operator <(node a, node b)
{
    if(a.first<b.first)
    {
        return a.first>a.first;
    }
    else if(a.second<b.second)
    {
        return a.second > b.second;
    }
    return false;
}
int djsktra(int n,vector<vector<int> >&connection,vector<vector<int> >&cost)
{
    node cur;
    int i,j;
    cur.first=2e9;
    cur.second=2e9;
    vector<node>dis(n+1,cur);
    priority_queue<node>q;
    dis[1].first=0;
    cur.vertex=1;
    cur.first=0;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.top();
        q.pop();
        int cur_node=cur.vertex,nxt_node,nxt_dis;
        for(i=0;i<connection[cur_node].size();i++)
        {
            nxt_node=connection[cur_node][i];
            nxt_dis=dis[cur_node].first+cost[cur_node][i];
            int chk=0;
            if(nxt_dis<dis[nxt_node].first || nxt_dis<dis[nxt_node].second)
            {
                if(nxt_dis<dis[nxt_node].first)
                {
                    chk=1;
                    dis[nxt_node].second=dis[nxt_node].first;
                    dis[nxt_node].first=nxt_dis;
                }
                else
                {
                    if(nxt_dis>dis[nxt_node].first)
                    {
                        chk=1;
                        dis[nxt_node].second=nxt_dis;
                    }
                }
            }
            nxt_dis=dis[cur_node].second+cost[cur_node][i];
            if(nxt_dis<dis[nxt_node].first || nxt_dis<dis[nxt_node].second)
            {
                if(nxt_dis<dis[nxt_node].first)
                {
                    chk=1;
                    dis[nxt_node].second=dis[nxt_node].first;
                    dis[nxt_node].first=nxt_dis;
                }
                else
                {
                    if(nxt_dis>dis[nxt_node].first)
                    {
                        chk=1;
                        dis[nxt_node].second=nxt_dis;
                    }
                }
            }
            if(chk)
            {
                node tmp;
                tmp.vertex=nxt_node;
                tmp.first=dis[nxt_node].first;
                tmp.second=dis[nxt_node].second;
                q.push(tmp);
            }
        }

    }
    return dis[n].second;
}

int main()
{
    int n,e,i,j,u,v,w,test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&e);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        for(i=1;i<=e;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost[u].push_back(w);
            connection[v].push_back(u);
            cost[v].push_back(w);
        }
        int res=djsktra(n,connection,cost);
        printf("Case %d: %d\n",t,res);
    }
}
