#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1,n,i,j;
    scanf("%d",&test);
    while(t<=test)
    {
       scanf("%d",&n);
       vector<int>numbers(n+1);
       for(i=1;i<=n;i++)
       {
           scanf("%d",&j);
           numbers[i]=j;
       }
       int moves=0;
       for(i=1;i<=n;i++)
       {
           while(numbers[i]!=i)
           {
               moves++;
               int tmp;
               tmp=numbers[numbers[i]];
               numbers[numbers[i]]=numbers[i];
               numbers[i]=tmp;
           }
       }
       printf("Case %d: %d\n",t,moves);
       t++;
    }
}
