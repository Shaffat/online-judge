#include<bits/stdc++.h>

using namespace std;

struct node
{
    int id,oil,cost;
};

bool operator <(node a, node b)
{
    if(a.cost!=b.cost)
    {
        return a.cost>b.cost;
    }
    return false;
}

int solve(int &start, int &last,int &capacity,vector<int>&fuel,vector<vector<int> >&roads,vector<vector<int> >&length)
{
    int i,j;
    node cur;
    priority_queue<node>q;
    cur.id=start;
    cur.oil=0;
    cur.cost=0;
    q.push(cur);
    vector<int>col(101,0);
    vector<vector<int> >vis(101,col);
    vector<int>col1(101,2e9);
    vector<vector<int> >dis(101,col1);
    dis[start][0]=0;
    while(!q.empty())
    {
        //cout<<"node="<<q.top().id<<" gas="<<q.top().oil<<" cost="<<q.top().cost<<" inside="<<chker<<endl;
        cur=q.top();
        q.pop();
        if(cur.id==last && cur.oil==0)
        {
            return cur.cost;
        }
        if(vis[cur.id][cur.oil]==1)
        {
            //cout<<"skipped"<<endl;
            continue;
        }
        vis[cur.id][cur.oil]=1;
        int cur_fuel=cur.oil;
        for(i=0;i<roads[cur.id].size();i++)
        {
            int need_fuel=length[cur.id][i];
            int nxt_id=roads[cur.id][i];
            //cout<<"need fuel="<<need_fuel<<" for city "<<nxt_id<<" capacity="<<capacity<<endl;
            for(j=max(need_fuel,cur_fuel);j<=capacity;j++)
            {
                int oil_bought=(j-cur_fuel)*fuel[cur.id];
                int tmp_fuel=j;
                node tmp_node;
                tmp_node.id=nxt_id;
                tmp_node.oil=tmp_fuel-need_fuel;
                tmp_node.cost=cur.cost+oil_bought;
                //cout<<"pushed situation="<<vis[tmp_node.id][tmp_node.oil]<<endl;
                if(vis[tmp_node.id][tmp_node.oil]==0 && dis[tmp_node.id][tmp_node.oil]>tmp_node.cost)
                {
                    dis[tmp_node.id][tmp_node.oil]=tmp_node.cost;
                    q.push(tmp_node);
                }
            }
        }

    }
    return 2e9;

}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,m,u,v,w,c,i,Q;
        scanf("%d %d",&n,&m);
        vector<int>fuel;
        vector<int>col;
        vector<vector<int> >roads(n,col);
        vector<vector<int> >length(n,col);
        for(i=0;i<n;i++)
        {
            scanf("%d",&w);
            fuel.push_back(w);
        }
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            roads[u].push_back(v);
            length[u].push_back(w);
            roads[v].push_back(u);
            length[v].push_back(w);

        }
        scanf("%d",&Q);
        printf("Case %d:\n",t);
        printf("Case %d:\n",t);
        t++;
        for(i=1;i<=Q;i++)
        {
            scanf("%d %d %d",&c,&u,&v);
            int res=solve(u,v,c,fuel,roads,length);
            if(res==2e9)
            {
                printf("impossible\n");
            }
            else
            {
                printf("%d\n",res);
            }
        }
    }
}
