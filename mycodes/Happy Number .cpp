#include<bits/stdc++.h>
using namespace std;

bool happy(int number,int counter)
{
    if(number==1)
    {
        return 1;
    }
    if(counter>1000)
    {
        return 0;
    }
    int sum=0,i;
    while(number>0)
    {
        i=number%10;
        number/=10;
        sum+=i*i;
    }
    return happy(sum,counter+1);

}


int main()
{
    int i,j;
    while(scanf("%d",&i)!=EOF)
    {
        if(happy(i,1))
        {
            printf("HAPPY\n");
        }
        else
            printf("UNHAPPY\n");
    }
}

