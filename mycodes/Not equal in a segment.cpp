#include<bits/stdc++.h>

using namespace std;
vector<vector<int> >numbers(10000001);

int binary(int x,int l)
{
    int first=0,last=numbers[x].size(),mid,result,f=0;
    while(first<=last)
    {
        mid=(first+last)/2;
        if(numbers[x][mid]==l)
        {
            return mid;
        }
        if(numbers[x][mid]>l)
        {
            last=mid-1;
        }
        else
        {
            first=mid+1;
        }

    }

}

int solve(int l,int r,int x)
{
    if(!binary_search(numbers[x].begin(),numbers[x].end(),l))
    {
        return l;
    }
    if(!binary_search(numbers[x].begin(),numbers[x].end(),r))
    {
        return r;
    }
    else
    {
        int first,last,mid;
        first=binary(x,l);
        last=binary(x,r);
        //cout<<"first="<<first<<" last="<<last<<endl;
        if((first-last)==(numbers[x][first]-numbers[x][last]))
           {
               //cout<<"-1"<<endl;
               return -1;
           }
        else
            {
                int f=0;
                while(first<=last)
                {
                    //cout<<"first="<<first<<" last="<<last<<"and value in first="<<numbers[x][first]<<" and in last="<<numbers[x][last];

                    if(first==last)
                    {
                        f++;
                    }
                    mid=(first+last)/2;
                    //cout<<" mid="<<mid<<" value in mid="<<numbers[x][mid]<<endl;
                    if(last-first==1)
                    {
                        return numbers[x][first]+1;
                    }

                    if((mid-first)<(numbers[x][mid]-numbers[x][first]))
                    {
                        last=mid;
                    }
                    else
                    {
                        first=mid;
                    }
                }
            }
    }
}


int main()
{
    int n,m,i,j,x,l,r;
    //vector<int>a;


    scanf("%d %d",&n,&m);
    for(i=1;i<=n;i++)
    {
        int k;
        scanf("%d",&k);
        //a.push_back(k);
        numbers[k].push_back(i);
        //valid[k]=1;
    }
    for(i=1;i<=m;i++)
    {
        int l,r,x;
        scanf("%d %d %d",&l,&r,&x);
        printf("%d\n",solve(l,r,x));

    }

}
