#include<bits/stdc++.h>
using namespace std;
int main()
{
    long long n,m,k;
    while(cin>>n>>m>>k)
    {
        long long extra=m-n,result=0,first=1,last=extra;
        long long left=k-1,right=n-k,mid;
        while(first<=last)
        {
            mid=(first+last)/2;
            long long k,l;
            k=min(left,mid-1);
            l=min(right,mid-1);
            long long left_last_value=mid-k;
            long long right_last_value=mid-l;
            long long left_sum=((k*((mid-1)+left_last_value))/2);
            long long right_sum=((l*((mid-1)+right_last_value))/2);
            long long total_sum=mid+left_sum+right_sum;
            if(total_sum==extra)
            {
                result=mid;
                break;
            }
            else if(total_sum<extra)
            {
                result=mid;
                first=mid+1;
            }
            else
            {
                last=mid-1;
            }

        }
        cout<<result+1<<endl;
    }
}
