#include<bits/stdc++.h>
using namespace std;
struct info
{
    int val;
};
bool operator <(info a,info b)
{
    if(a.val!=b.val)
        return a.val>b.val;
    return false;
}
void bfs(vector<vector<int> >&connection)
{
    int i,j,cur;
    info nxt;
    vector<int>vis(200000,0);
    priority_queue<info>q;
    nxt.val=1;
    q.push(nxt);
    vis[1]=1;
    while(!q.empty())
    {
        cur=q.top().val;
        printf("%d ",cur);
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            nxt.val=connection[cur][i];
            if(!vis[nxt.val])
            {
                vis[nxt.val]=1;
                q.push(nxt);
            }
        }
    }
    return;
}


int main()
{
    int n,m,i,j,u,v;
    scanf("%d %d",&n,&m);
    vector<vector<int> >conn(n+1);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        conn[u].push_back(v);
        conn[v].push_back(u);
    }

    bfs(conn);

}
