#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>primes;
vector<bool>ar(1000000,0);



void sieve()
{
    ll i,j;
    ar[1]=1;
    for(i=4;i<1e6;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e6);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e6;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e6;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}


ll solve(ll n)
{
    ll i,j,counter=0;
    for(i=0;i<primes.size();i++)
    {
        ll cur=primes[i];
        while(cur<=n)
        {
            counter+=n/cur;
            cur*=primes[i];
        }
    }
    return counter;

}

int main()
{
    sieve();
    ll i,j,n;
    while(scanf("%lld",&n)!=EOF)
    {
        ll res= solve(n);
        printf("%lld\n",res);
    }
}
