#include<bits/stdc++.h>

using namespace std;

vector< vector<int> >board;
vector< vector<int> >result;
vector< int >combination(8,0);
int s=1;
void addingdowndiagonal(int r,int c)
{
    int a,b;
    a=r-min(r,c);
    b=c-min(r,c);
    while(a!=8 && b!=8)
    {
        if(a==r && b==c)
        {
            a++;
            b++;
            continue;
        }
        board[a][b]+=1;
        a++;
        b++;
    }
}
void removingdowndiagonal(int r,int c)
{
    int a,b;
    a=r-min(r,c);
    b=c-min(r,c);
    while(a!=8 && b!=8)
    {
        if(a==r && b==c)
        {
            a++;
            b++;
            continue;
        }
        board[a][b]-=1;
        a++;
        b++;
    }
}

void addingupdiagoanl(int r,int c)
{
   int d=r+c,a,b;
   if(d>7)
   {
       a=7;
       b=d-7;
   }
   else
   {
       a=d;
       b=0;
   }

   while(a!=-1 && b!=8)
   {
       if(a==r && b==c)
       {
           a--;
           b++;
           continue;
       }
       board[a][b]+=1;
       a--;
       b++;
   }
}

void removingupdiagoanl(int r,int c)
{
   int d=r+c,a,b;
   if(d>7)
   {

       a=7;
       b=d-7;
   }
   else
   {
       a=d;
       b=0;
   }
   while(a!=-1 && b!=8)
   {
       if(a==r && b==c)
       {
           a--;
           b++;
           continue;
       }
       board[a][b]-=1;
       a--;
       b++;
   }
}

void addingrow(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]+=1;
    }
}
void removingrow(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]-=1;
    }
}

void addingcolumn(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]+=1;
    }
}

void removingcolumn(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]-=1;
    }
}

bool nqueen(int c,int fixedcolumn)
{
    int i,j;
    if(c==8)
    {
        if(s<10)
        {
            printf(" %d      ",s);
        }
        else
        {
            printf("%d      ",s);
        }
        for(i=0;i<8;i++)
        {
            printf("%d",combination[i]+1);
            if(i==7)
            {
                printf("\n");
            }
            else
                printf(" ");
        }
        s++;
        return true;
    }

    if(c==fixedcolumn)
    {
        nqueen(c+1,fixedcolumn);
    }
    else
    {

        for(i=0;i<=8;i++)
        {
            if(i==8)
            {
                return false;
            }
            if(board[i][c]==0)
            {
                board[i][c]+=1;
                addingcolumn(i,c);
                addingrow(i,c);
                addingdowndiagonal(i,c);
                addingupdiagoanl(i,c);
                combination[c]=i;
                nqueen(c+1,fixedcolumn);
                board[i][c]-=1;
                removingcolumn(i,c);
                removingrow(i,c);
                removingdowndiagonal(i,c);
                removingupdiagoanl(i,c);
            }
        }
    }
}

int main()
{
    int i,j,r,c,test,t=1;
    vector<int>v;
    for(i=0;i<8;i++)
    {
        board.push_back(v);
        for(j=0;j<8;j++)
        {
            board[i].push_back(0);
        }
    }
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&r,&c);
        r--;c--;
        addingcolumn(r,c);
        addingrow(r,c);
        addingdowndiagonal(r,c);
        addingupdiagoanl(r,c);
        board[r][c]=1;
        combination[c]=r;
        printf("SOLN       COLUMN\n");
        printf(" #      1 2 3 4 5 6 7 8\n\n");
        nqueen(0,c);
        board[r][c]-=1;
        removingcolumn(r,c);
        removingrow(r,c);
        removingdowndiagonal(r,c);
        removingupdiagoanl(r,c);
        s=1;

        t++;
        if(t<=test)
        {
            printf("\n");
        }
    }

}
