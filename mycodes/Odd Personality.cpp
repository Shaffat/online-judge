#include<bits/stdc++.h>
using namespace std;
int timer,t;
vector<int>start(10000);
vector<int>low(10000);
vector<int>vis(10000);

void no_bridge(int node,int parent,vector<vector<int> >&connection,vector<vector<int> >&newconnection)
{
    int i,j,child;
    start[node]=low[node]=timer;
    //cout<<"start of "<<node<<" ="<<start[node]<<endl;
    timer++;
    for(i=0;i<connection[node].size();i++)
    {
       child=connection[node][i];
       if(child==parent)
       {
           continue;
       }
       if(vis[child]!=t)
       {
           vis[child]=t;
           no_bridge(child,node,connection,newconnection);
           if(low[child]<=start[node])
           {
               newconnection[node].push_back(child);
           }
           low[node]=min(low[node],low[child]);
       }
       else
       {
           newconnection[node].push_back(child);
           low[node]=min(low[node],start[child]);
       }
    }
    if(node!=parent && start[parent]>=low[node])
    {
        newconnection[node].push_back(parent);
    }
}
vector<int>start_cmp(10000);
int counter;
int odd_cycle(int node,int parent,vector<vector<int> >&connection,vector<int>&vis_cmp)
{
    counter++;
    int i,j,child,odd=0;
    start_cmp[node]=timer;
    //cout<<"start of"<<node<<"="<<start_cmp[node]<<endl;
    timer++;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(child==parent)
        {
            continue;
        }
        if(vis_cmp[child]==0)
        {
            vis_cmp[child]=1;
            odd+=odd_cycle(child,node,connection,vis_cmp);
        }
        else if(vis_cmp[child]==1)
        {
            int cycle_len=start_cmp[node]-start_cmp[child]+1;
            //cout<<"len="<<cycle_len<<endl;
            if(cycle_len%2==1)
            {
                //cout<<"odd cycle"<<endl;
                odd++;
            }
        }
    }
    timer--;
    vis_cmp[node]=2;
    //cout<<"odd="<<odd<<endl;
    return odd;

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,n,m,i,j;
    t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int u,v;
        scanf("%d %d",&n,&m);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >no_bridge_graph(n+1);
        vector<int>vis_cmp(n+1,0);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        for(i=0;i<n;i++)
        {
            if(vis[i]!=t)
            {
                vis[i]=t;
                no_bridge(i,i,connection,no_bridge_graph);
            }
        }
        int res=0,odd;
        for(i=0;i<n;i++)
        {
           if(vis_cmp[i]==0)
           {
               counter=0;
               timer=0;
               vis_cmp[i]=1;
               odd=odd_cycle(i,i,no_bridge_graph,vis_cmp);
               //cout<<"odd="<<odd<<endl;
               if(odd>0)
               {
                   //cout<<"counter="<<counter<<endl;
                   res+=counter;
               }
           }
        }
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
