
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())using namespace std;

ll solve(ll id,ll start, ll k, ll mx,ll mn,vl &v, vll &memory)
{
    //cout<<"id="<<id<<" start="<<start<<" k="<<k<<" mx="<<mx<<" mn="<<mn<<endl;
    if(id>=v.size())
    {
        if(k==0)
            return 0;
        return 1e12;
    }
    //cout<<"here"<<endl;
    if(k<=0) return 1e12;
    //cout<<"here"<<endl;
    if(memory[start][k]!=-1)
        return memory[start][k];

    ll cost1,cost2,nwmx=mx,nwmn=mn;

    if(v[id]>v[mx])
        nwmx=id;

    if(v[id]<v[mn])
        nwmn=id;

    cost1=solve(id+1,start,k,nwmx,nwmn,v,memory);
    //cout<<"close a partition at "<<id<<" started at"<<start<<" mx="<<nwmx<<" mn="<<nwmn<<" add value "<<v[nwmx]-v[nwmn]<<endl;
    cost2=solve(id+1,id+1,k-1,id+1,id+1,v,memory)+v[nwmx]-v[nwmn];
    //cout<<"memory["<<start<<"]["<<k<<"]="<<min(cost1,cost2)<<endl;
    return memory[start][k]=min(cost1,cost2);
}


int main()
{
    ll test,t,n,m,i,j,k;
    sc1(test);
    FOR(t,1,test)
    {
        sc2(n,k);
        vl v(n);
        vl col(k+1,-1);
        vll memory(n+1,col);
        FOR(i,0,n-1)
        {
            sc1(j);
            v[i]=j;
        }
        ll res=solve(0,0,k,0,0,v,memory);
        printf("%lld\n",res);
    }
}
