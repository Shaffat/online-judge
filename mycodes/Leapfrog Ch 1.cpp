#include<bits/stdc++.h>
using namespace std;

int main(){
    freopen("leapfrog_ch_ (1).txt","r",stdin);
    freopen("out.txt","w",stdout);
    int i,j,test,t;
    string s;
    cin>>test;
    for(t=1;t<=test;t++){
        cin>>s;
        int need,have=0;
        //cout<<"sz="<<s.size()<<endl;
        if((s.size()-1)%2==1){
            need=(s.size()-1)/2 + 1;
        }else{
            need=(s.size()-1)/2;
        }
        for(i=0;i<s.size();i++){
            if(s[i]=='B'){
                have++;
            }
        }
        //cout<<"need="<<need<<" have="<<have<<endl;
        if(need>have || have>=(s.size()-1)){
            cout<<"Case #"<<t<<": N"<<endl;
        }
        else{
             cout<<"Case #"<<t<<": Y"<<endl;
        }
    }
}
