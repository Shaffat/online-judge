#include<bits/stdc++.h>
using namespace std;

vector<int>col(501,-1);
vector<vector<int> >testcase(501,col);
vector<double>col1(501);
vector<vector<double> >memory(501,col1);
int t;
double solve(int red,int blue)
{
    if(red<=0)
    {
        return 1;
    }
    if(blue<=0)
    {
        return 0;
    }
    if(testcase[red][blue]!=-1)
    {
        cout<<"memory"<<endl;
        return memory[red][blue];
    }
    double case1,case2,res;
    case1=red*solve(red-1,blue-1);
    case2=blue*solve(red,blue-2);
    res=(case1+case2)/(red+blue);
    testcase[red][blue]=t;
    return memory[red][blue]=res;
}
int main()
{
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int red,blue;
        double res;
        scanf("%d %d",&red,&blue);
        res=solve(red,blue);
        printf("Case %d: %.10lf\n",t,res);
    }
}
