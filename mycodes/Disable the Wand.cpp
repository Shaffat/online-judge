#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll memory[33][33][33][7][3][2];
ll testcase[33][33][33][7][3][2];
ll t,ideal;

ll solve(ll index,ll three_mod,ll seven_mod,ll k_remain,ll maxone_remain,ll restriction)
{
    if(k_remain<0 || maxone_remain<0)
        return 0;
    if(index<0)
    {
        if(three_mod==0 && seven_mod!=0 && maxone_remain>=0 && k_remain>=0)
        return total;
    }
    if(testcase[index][maxone_remain][k_remain][seven_mod][three_mod][restriction]==t)
    {
        memory[index][maxone_remain][k_remain][seven_mod][three_mod];
    }
    ll res=0;
    int one=0;
    if((ideal&(1<<index))!=0)
        one=1;
    if(restriction)
    {
        if(one)
        {
            res+=solve(index-1,three_mod,seven_mod,k_remain-1,maxone_remain,0,total);
            ll cur=total+(1<<index);
            res+=solve(index-1,cur%3,cur%7,k_remain-1,maxone_remain,1,total);
        }
        else
        {
            res+=solve(index-1,three_mod,seven_mod,k_remain,maxone_remain,1,total);
        }
    }
    else
    {
        res+=solve(index-1,three_mod,seven_mod,k_remain-1,maxone_remain,0,total);
        ll cur=total+(1<<index);
        res+=solve(index-1,cur%3,cur%7,k_remain-1,maxone_remain,0,total);
    }
}
