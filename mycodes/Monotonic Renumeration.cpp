#include<bits/stdc++.h>
using namespace std;

struct val
{
    int c,pos;
};

bool operator <(val a, val b)
{
    if(a.c!=b.c)
        return a.c<b.c;
    return a.pos<b.pos;
}


int lowest(int n,vector<val>&v)
{
    int i,j,first,last,mid,f=0,ans;
    first=0;
    last=v.size()-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if(v[mid].c>=n)
        {
            last=mid-1;
            ans=v[mid].pos;
        }
        else
            first=mid+1;
    }
    return ans;
}

int highest(int n,vector<val>&v)
{
    int i,j,first,last,mid,f=0,ans;
    first=0;
    last=v.size()-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if(v[mid].c<=n)
        {
            first=mid+1;
            ans=v[mid].pos;
        }
        else
            last=mid-1;
    }
    return ans;
}


int main()
{
    int n,i,j,k,ans=1,last,h=-1,l,nxt,cur,b,a;
    scanf("%d",&n);
    set<int>s;
    vector<val>v;
    vector<int>values;
    for(i=1;i<=n;i++)
    {
        b=s.size();
        scanf("%d",&j);
        val tmp;
        tmp.c=j;
        tmp.pos=i;
        s.insert(j);
        if(s.size()!=b)
        {
            if(b==0) cur=j;
            else
                values.push_back(j);
        }
        v.push_back(tmp);
    }
    sort(values.begin(),values.end());
    sort(v.begin(),v.end());
    h=highest(cur,v);
    for(i=0;i<v.size();i++)
    {
        l=lowest(values[i],v);

        if(l>h)
            ans*=2;
        h=max(h,highest(values[i],v));
        ans%=998244353 ;
    }
    printf("%d\n",ans);
}
