#include<bits/stdc++.h>

using namespace std;

struct mxnode
{
    int u,w;
};

struct mnnode
{
    int u,w;
};
bool operator < (mxnode x,mxnode y)
{
    if(x.w!=y.w)
    {
        return x.w>y.w;
    }
    return false;
}
bool operator < (mnnode x,mnnode y)
{
    return x.w< y.w;
}


int main()
{
    int test, t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int x,y,z,i,j,n;
        scanf("%d",&n);
        vector<int>edges[n+1];
        vector<int>cost[n+1];
        vector<int>vis(n+1,0);
        vector<int>weight(n+1,1<<29);

        while(scanf("%d %d %d",&x,&y,&z)==3)
        {
            if(x==0 && y==0 && z==0)
            {
                break;
            }
            edges[x].push_back(y);
            edges[y].push_back(x);
            cost[x].push_back(z);
            cost[y].push_back(z);
        }
        mxnode start;
        start.u=0;
        start.w=0;
        weight[0]=0;
        priority_queue<mxnode>q;
        q.push(start);
        while(!q.empty())
        {
            mxnode cur_top=q.top();
            q.pop();
            int cur_node=cur_top.u;
            if(vis[cur_node])
            {
                continue;
            }
            vis[cur_node]=1;
            for(i=0;i<edges[cur_node].size();i++)
            {
                int nxt_node=edges[cur_node][i];
                int nxt_cost=cost[cur_node][i];
                if(nxt_cost<weight[nxt_node] && vis[nxt_node]==0)
                {
                    weight[nxt_node]=nxt_cost;
                    mxnode nwnode;
                    nwnode.u =nxt_node;
                    nwnode.w=nxt_cost;
                    q.push(nwnode);
                }
            }
        }
       int mx=0;
       for(i=0;i<=n;i++)
       {
           mx+=weight[i];
       }

       vector<int>mxweight(n+1,-1);
       mnnode mstart;
       mstart.u=0;
       mstart.w=0;
       mxweight[0]=0;
       vector<int>vis1(n+1,0);
       priority_queue<mnnode>q1;
       q1.push(mstart);
       while(!q1.empty())
       {
           mnnode cur_top=q1.top();
           q1.pop();
           int cur_node=cur_top.u;
            if(vis1[cur_node])
            {
                continue;
            }
            vis1[cur_node]=1;
            for(i=0;i<edges[cur_node].size();i++)
            {
                int nxt_node=edges[cur_node][i];
                int nxt_cost=cost[cur_node][i];
                if(nxt_cost>mxweight[nxt_node] && vis1[nxt_node]==0)
                {
                    mxweight[nxt_node]=nxt_cost;
                    mnnode nwnode;
                    nwnode.u =nxt_node;
                    nwnode.w=nxt_cost;
                    q1.push(nwnode);

                }
            }

       }

       int mn=0;
       for(i=0;i<=n;i++)
       {
           mn+=mxweight[i];
       }
      int total=mn+mx;
      if(total%2==0)
      {
          printf("Case %d: %d\n",t,total/2);
      }
      else
      {
          printf("Case %d: %d/%d\n",t,total,2);
      }
      t++;
    }
}
