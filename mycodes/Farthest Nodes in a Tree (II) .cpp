#include<bits/stdc++.h>
using namespace std;

void bfs(int root,vector<vector<int> >&connection,vector<vector<int> >&weight,vector<int>&distance)
{
    int i,j;
    vector<int>vis(distance.size(),0);
    vis[root]=1;
    queue<int>q;
    q.push(root);
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            int child=connection[cur][i];
            if(vis[child]==0)
            {
                vis[child]=1;
                q.push(child);
                distance[child]=distance[cur]+weight[cur][i];
            }
        }
    }
    return;
}

int farthest_node(vector<int>&distance)
{
    int i,j,mx=-1,res;
    for(i=0;i<distance.size();i++)
    {
        if(mx<distance[i])
        {
            mx=distance[i];
            res=i;
        }
    }
    return res;
}

void final_distance(vector<int>&node1,vector<int>&node2,vector<int>&res)
{
    for(int i=0;i<node1.size();i++)
    {
        res[i]=max(node1[i],node2[i]);
    }
    return;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,i,j,u,v,w,node1,node2;
        scanf("%d",&n);
        vector<int>vec;
        vector<vector<int> >connection(n,vec);
        vector<vector<int> >weight(n,vec);
        for(i=1;i<n;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            weight[u].push_back(w);
            connection[v].push_back(u);
            weight[v].push_back(w);
        }
        printf("Case %d:\n",t);
        vector<int>dis1(n+1);
        bfs(0,connection,weight,dis1);
        node1=farthest_node(dis1);
        vector<int>dis2(n+1);
        bfs(node1,connection,weight,dis2);
        node2=farthest_node(dis2);
        vector<int>dis3(n+1);
        vector<int>res(n+1);
        bfs(node2,connection,weight,dis3);
        final_distance(dis2,dis3,res);
        for(i=0;i<n;i++)
        {
            printf("%d\n",res[i]);
        }
    }
}
