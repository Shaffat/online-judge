#include<bits/stdc++.h>

using namespace std;

vector<int>col(1001,-1);
vector<vector<int> >memory(1001,col);

int solve(int horse,int pos)
{
    if(horse<=0 || pos<=0)
        return 1;
    if(memory[horse][pos]!=-1)
        return memory[horse][pos];
    int i,j,way=0;
    for(i=1;i<=horse;i++)
    {
        way+=solve(horse-i,pos-1);
    }
    return memory[horse][pos]=way;
}

int main()
{

    int n,t,test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        int res=solve(n,n);
        printf("Case %d: %d\n",t,res);
    }

}
