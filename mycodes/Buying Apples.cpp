#include<bits/stdc++.h>

using namespace std;
int n;
int solve(int index,int need,int packet,vector<int>&price,vector<vector<vector<int> > >&memory)
{
    //cout<<"index="<<index<<" need="<<need<<" packet="<<packet<<endl;
    if(packet>n)
    {
        return 1e9;
    }
    if(index>=price.size()||need<0)
    {
        if(need==0)
        {
            return 0;
        }
        else
            return 1e9;
    }
    if(memory[index][need][packet]!=-1)
    {
        return memory[index][need][packet];
    }
    int res,i,j,case1,case2;
    if(price[index]==-1)
    {
        res=solve(index+1,need,packet,price,memory);
    }
    else
    {
        case1=solve(index,need-index,packet+1,price,memory)+price[index];
        case2=solve(index+1,need,packet,price,memory);
        res=min(case1,case2);
    }
    return memory[index][need][packet]=res;
}

int main()
{
    int i,j,k,test;
    scanf("%d",&test);
    while(test--)
    {
        vector<int>price;
        price.push_back(-1);
        scanf("%d %d",&n,&k);
        for(i=1;i<=k;i++)
        {
            scanf("%d",&j);
            price.push_back(j);
        }
        vector<int>col1(n+1,-1);
        vector<vector<int> >col2(k+1,col1);
        vector<vector<vector<int> > >memory(k+1,col2);
        int res=solve(1,k,0,price,memory);
        if(res>1e5)
        {
            printf("-1\n");
        }
        else
        printf("%d\n",res);
    }
}
