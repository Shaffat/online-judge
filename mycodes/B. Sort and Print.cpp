#include <bits/stdc++.h>

using namespace std;

struct student {
    int rolls, marks;
    string names;

    bool operator < (student s) {
        if (this->marks > s.marks)
            return true;
        else if (this->marks < s.marks)
            return false;

        if (this->rolls < s.rolls)
            return true;
        else
            return false;
    }
};

int main() {

    //freopen("in.txt", "r", stdin);

    int n, i;
    student aStudent;

    vector < student > vt;

    cin >> n;
    for (i = 0; i < n; ++ i) {
        cin >> aStudent.rolls;
        cin.ignore();
        cin >> aStudent.names;
        cin >> aStudent.marks;

        vt.push_back(aStudent);
    }

    sort(vt.begin(), vt.end());

    cout << "Roll | Name       | Marks" << endl;
    cout << "-------------------------" << endl;

    for (i = 0; i < n; ++ i) {
//        printf("%4d | %s | %d \n", vt[i].rolls, vt[i].names.c_str(), vt[i].marks);
        cout.width(4);cout<<right<<vt[i].rolls<<" | ";
        cout.width(10);cout<<left<<vt[i].names<<" | ";
        cout<<left<<vt[i].marks<<endl;
    }

    return (0);
}
