#include<bits/stdc++.h>
using namespace std;

#define ll long long int
ll GCD(ll a,ll b)
{
    while(a%b!=0)
    {
        ll temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}


int main()
{
    ll test,t,n,d,r,i,j,f,s;
    scanf("%lld",&test);
    for(t=1;t<=test;t++){
        scanf("%lld %lld %lld",&n,&d,&r);
         f=1;
         s=1;
         ll timer=0,res;
         vector<int>lake_d(n+1,0);
         vector<int>lake_r(n+1,0);
         while(1){
            timer++;
            f+=d;
            f%=n;
            s+=r;
            s%=n;
            lake_d[f]=1;
            lake_r[s]=1;
            if((lake_d[f]==1 && lake_r[f]==1 ) || (lake_d[s]==1 && lake_r[s]==1)){
                res=timer;
                break;
               }
         }
        printf("%lld\n",res);

    }
}
