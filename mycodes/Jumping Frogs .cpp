#include<bits/stdc++.h>
using namespace std;

struct rock
{
    int type,dis;
};

int solve(int s1,int s2,int index,vector<rock>&stones,vector<vector<int> >&memory)
{
    //cout<<"s1="<<s1<<" s2="<<s2<<" index="<<index<<" type="<<stones[index].type<<endl;
    if(index>=stones.size())
    {
        return 0;
    }
    if(memory[s1][s2]!=-1)
    {
        return memory[s1][s2];
    }
    int res1,res2,res3,res;
    if(stones[index].type==1)
    {
        //cout<<"its a big stone dis="<<stones[index].dis<<endl;
        res1=max(abs(stones[s1].dis-stones[index].dis),abs(stones[index].dis-stones[s2].dis));
        //cout<<"before jumping "<<res1<<endl;
        res=max(res1,solve(index,index,index+1,stones,memory));
        //cout<<"after jumping from "<<index<<" res="<<res<<endl;
    }
    else
    {
        res1=max(abs(stones[index].dis-stones[s1].dis),solve(index,s2,index+1,stones,memory));
        //cout<<"giving to s1 res="<<res1<<endl;
        res2=max(abs(stones[index].dis-stones[s2].dis),solve(s1,index,index+1,stones,memory));
        //cout<<"giving to s2 res="<<res2<<endl;
        res=min(res1,res2);
    }
    //cout<<"s1="<<s1<<" s2="<<s2<<" memory="<<res<<endl;
    return memory[s1][s2]=res;
}

int getdis(string &s)
{
    int cur=0,i;
    for(i=2;i<s.size();i++)
    {
        cur*=10;
        cur+=s[i]-'0';
    }
    return  cur;
}

int main()
{
    int test,t=1,n,d,i,j;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s;
        cin>>n>>d;
        vector<rock>stones;
         rock tmp;
         tmp.type=1;
         tmp.dis=0;
         stones.push_back(tmp);
        for(i=1;i<=n;i++)
        {
            cin>>s;
            //cout<<"s="<<s<<endl;
            if(s[0]=='B')
            {
                tmp.type=1;
            }
            else
                tmp.type=0;
            //cout<<"dis="<<getdis(s)<<endl;
            //cout<<"index="<<stones.size()<<" type="<<tmp.type<<endl;
            tmp.dis=getdis(s);
            stones.push_back(tmp);
        }
        tmp.type=1;
        tmp.dis=d;
        stones.push_back(tmp);
        vector<int>col(110,-1);
        vector<vector<int> >memory(110,col);
        int res=solve(0,0,1,stones,memory);
        printf("Case %d: %d\n",t,res);

    }
}
