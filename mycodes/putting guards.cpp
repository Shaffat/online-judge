#include<bits/stdc++.h>

using namespace std;

struct point
{
    int row,column;
};
int main()
{
     int test,t=1;
     scanf("%d",&test);
     while(t<=test)
     {
         int r,c,i,j,n;
         scanf("%d %d",&r,&c);

         string s;
         vector<string>warehouse;
         vector<int>col(c+2,0);
         vector<vector<int> >onemanarmy(r+2,col);
          vector<int>col1(c+2,0);
         vector<vector<int> >vis(r+2,col1);
         for(i=1;i<=r;i++)
         {
             cin>>s;
             warehouse.push_back(s);
         }
         int guards=0;
         for(i=0;i<r;i++)
         {
             for(j=0;j<c;j++)
             {
                 if(warehouse[i][j]=='1'&& vis[i][j]==0)
                 {
                     cout<<"warehouse["<<i<<"]["<<j<<"]="<<warehouse[i][j]<<endl;
                     point tmp;
                     tmp.row=i;
                     tmp.column=j;
                     vis[i][j]=1;
                     queue<point>q;
                     q.push(tmp);

                     while(!q.empty())
                     {
                         point cur=q.front();
                         q.pop();
                         int left=0,right=0,up=0,down=0,diagonalleftup=0,diagonalleftdown=0,diagonalrightup=0,diagonalrightdown=0;
                         if(cur.column-1>=0)
                         {
                             if(warehouse[cur.row][cur.column-1]=='1'&& vis[cur.row][cur.column-1]==0)
                             {
                                 point tmp;
                                 tmp.row=cur.row;
                                 tmp.column=cur.column-1;
                                 left=1;
                                 cout<<"pushing"<<tmp.row<<" 1 "<<tmp.column<<endl;
                                 vis[tmp.row][tmp.column]=1;
                                 q.push(tmp);

                             }
                         }
                         if(cur.column+1<c)
                         {
                             if(warehouse[cur.row][cur.column+1]=='1'&& vis[cur.row][cur.column+1]==0)
                             {
                                 cout<<"checking warehouse["<<cur.row<<"]["<<cur.column+1<<"]="<<warehouse[cur.row][cur.column+1]<<endl;
                                 point tmp;
                                 tmp.row=cur.row;
                                 tmp.column=cur.column+1;
                                 right=1;
                                 cout<<"pushing"<<tmp.row<<" 2 "<<tmp.column<<endl;
                                 vis[tmp.row][tmp.column+1]=1;
                                 q.push(tmp);

                             }
                         }
                         if(cur.row-1>=0)
                         {
                             if(warehouse[cur.row-1][cur.column]=='1'&& vis[cur.row-1][cur.column]==0)
                             {
                                 point tmp;
                                 tmp.row=cur.row-1;
                                 tmp.column=cur.column;
                                 up=1;
                                 cout<<"pushing"<<tmp.row<<" 3 "<<tmp.column<<endl;
                                 vis[tmp.row-1][tmp.column]=1;
                                 q.push(tmp);

                             }
                         }
                         if(cur.row+1<r)
                         {
                             if(warehouse[cur.row+1][cur.column]=='1' && vis[cur.row+1][cur.column]==0)
                             {
                                 point tmp;
                                 tmp.row=cur.row+1;
                                 tmp.column=cur.column;
                                 down=1;
                                 cout<<"pushing"<<tmp.row<<" 4 "<<tmp.column<<endl;
                                 vis[tmp.row+1][tmp.column]=1;
                                 q.push(tmp);
                             }
                         }
                         if(cur.column-1>=0 && cur.row-1>=0)
                         {
                             if(warehouse[cur.row-1][cur.column-1]=='1'&& vis[cur.row-1][cur.column-1]==0)
                             {
                                 if(onemanarmy[cur.row-1][cur.column-1]==0)
                                 {
                                     onemanarmy[cur.row-1][cur.column-1]=1;
                                     guards++;
                                 }
                                 diagonalleftup=1;
                                 vis[cur.row-1][cur.column-1]=1;
                                 point tmp;
                                 tmp.row=cur.row-1;
                                 tmp.column=cur.column-1;
                                 q.push(tmp);
                             }

                         }
                         if(cur.column+1<c && cur.row+1<r)
                         {
                             if(warehouse[cur.row+1][cur.column+1]=='1'&&vis[cur.row+1][cur.column+1]==0)
                             {
                                 if(onemanarmy[cur.row+1][cur.column+1]==0)
                                 {
                                     onemanarmy[cur.row+1][cur.column+1]=1;
                                     guards++;
                                 }
                                 diagonalleftdown=1;
                                 vis[cur.row+1][cur.column+1]=1;
                                 point tmp;
                                 tmp.row=cur.row+1;
                                 tmp.column=cur.column+1;
                                 q.push(tmp);
                             }

                         }
                          if(cur.column-1>=0 && cur.row+1<r)
                         {
                             if(warehouse[cur.row+1][cur.column-1]=='1' && vis[cur.row+1][cur.column-1]==0)
                             {
                                 if(onemanarmy[cur.row+1][cur.column-1]==0)
                                 {
                                     onemanarmy[cur.row+1][cur.column-1]=1;
                                     guards++;
                                     cout<<cur.row<<" "<<cur.column<<endl;

                                 }
                                 diagonalrightdown=1;
                                 vis[cur.row+1][cur.column-1]=1;
                                 point tmp;
                                 tmp.row=cur.row+1;
                                 tmp.column=cur.column-1;
                                 q.push(tmp);
                             }

                         }
                         if(cur.column+1<c && cur.row-1>=0)
                         {
                             if(warehouse[cur.row-1][cur.column+1]=='1'&& vis[cur.row-1][cur.column+1]==0)
                             {
                                 if(onemanarmy[cur.row-1][cur.column+1]==0)
                                 {
                                     onemanarmy[cur.row-1][cur.column+1]=1;
                                     guards++;
                                     cout<<cur.row<<" multiguard "<<cur.column<<endl;
                                 }
                                 diagonalrightup=1;
                                 vis[cur.row-1][cur.column+1]=1;
                                 point tmp;
                                 tmp.row=cur.row-1;
                                 tmp.column=cur.column+1;
                                 q.push(tmp);
                             }

                         }
                         if(left==0 &&up==0 && diagonalleftup==0)
                         {
                             guards++;

                         }
                         if(left==0 && down==0 && diagonalrightdown==0)
                         {
                             guards++;
                              //cout<<cur.row<<" leftdowncorner "<<cur.column<<endl;
                         }
                         if(right==0 && up==0 && diagonalrightup==0)
                         {
                             guards++;
                              //cout<<cur.row<<" rightupcorner "<<cur.column<<endl;
                         }
                         if(right==0 && down==0 && diagonalleftdown==0)
                         {
                             guards++;
                             // cout<<cur.row<<" righdown "<<cur.column<<endl;
                         }
                     }
                 }
             }
         }
        printf("Case %d: %d\n",t,guards);
        t++;
         }
     }

