#include<bits/stdc++.h>
using namespace std;
int prime[101];
int result[101];
void sieve()
{
    int i,j;
    prime[1]=1;
    for(i=4;i<=100;i+=2)
    {
        prime[i]=1;
    }
    for(i=3;i<=sqrt(100);i+=2)
    {
        if(prime[i]==0)
        {
            for(j=i*i;j<=100;j+=i)
            {
                prime[j]=1;
            }
        }
    }
}
void solve()
{
    int i,j,pre=-1;
    for(i=1;i<=60;i++)
    {
        if(prime[i]==0)
        {
            result[i]=pre;
            pre=i;
        }
    }
}
int main()
{
    sieve();
    solve();
    int x,y;
    cin>>x>>y;
    if(result[y]==x)
    {
        cout<<"YES"<<endl;
    }
    else
        cout<<"NO"<<endl;
}
