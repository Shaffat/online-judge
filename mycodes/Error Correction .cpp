#include<stdio.h>
#include<iostream>
#include<string>
#include<math.h>
#include<algorithm>
#include<vector>
using namespace std;
int main()
{

    int n,i,j;
    while(scanf("%d",&n))
    {
        if(n==0)
        {
            break;
        }
        vector<int>rowsum(n+1);
        vector<int>colsum(n+1);
        vector<bool>corruptrow(n+1,0);
        vector<bool>corruptcol(n+1,0);
        int errorcol=0,errorrow=0;
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                int k;
                scanf("%d",&k);
                rowsum[i]+=k;
                colsum[j]+=k;
                corruptrow[i]=rowsum[i]%2;
                corruptcol[j]=colsum[j]%2;

            }
        }
        int currectrow=0,currectcol=0;
        for(i=1;i<=n;i++)
        {
//            cout<<"row="<<i<<" "<<corruptrow[i]<<endl;
//            cout<<"col="<<i<<" "<<corruptcol[i]<<endl;
            if(corruptrow[i])
            {
                currectrow=i;
                errorrow++;
            }
            if(corruptcol[i])
            {
                currectcol=i;
                errorcol++;
            }
        }
        if(errorcol>1 ||errorrow>1)
        {
            printf("Corrupt\n");
        }
        else if(errorcol==1&&errorrow==1)
        {

            printf("Change bit (%d,%d)\n",currectrow,currectcol);
        }
        else
            printf("OK\n");

    }
}
