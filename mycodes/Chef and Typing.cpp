#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t,i,j,n;
    string s;
    cin>>test;
    map<char,int>group;
    group['d']=1;
    group['f']=1;
    group['j']=2;
    group['k']=2;
    for(t=1;t<=test;t++)
    {
        map<string,int>counter;
        cin>>n;
        int total=0;
        while(n--){
            cin>>s;
            //cout<<"s="<<s<<endl;
            int before=0,cur=0;
            if(counter[s]>0) before=1;
            counter[s]=1;
            cur=2;
            for(i=1;i<s.size();i++)
            {
                //cout<<"checking "<<group[s[i-1]]<<" "<<group[s[i]]<<endl;
                if(group[s[i-1]]!=group[s[i]])
                {
                    cur+=2;
                }
                else
                    cur+=4;
            }
            if(before)
            {
                cur/=2;
            }
            total+=cur;
            //cout<<"total="<<total<<endl;
        }
        cout<<total<<endl;
    }

}
