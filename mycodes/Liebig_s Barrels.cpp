#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int i,j,n,k,l,mn=2e9;
    vector<long long int>v;
    vector<long long int>valid;
    scanf("%lld %lld %lld",&n,&k,&l);
    for(i=1;i<=n*k;i++)
    {
        scanf("%lld",&j);
        mn=min(mn,j);
        v.push_back(j);
    }
    //cout<<"mn="<<mn<<endl;
    for(i=0;i<v.size();i++)
    {
        if(abs(v[i]-mn)<=l)
        {
            //cout<<"val="<<v[i]<<" mn="<<mn<<endl;
            valid.push_back(v[i]);
        }
    }
    if(valid.size()<n)
    {
        printf("0\n");
        return 0;
    }
    sort(valid.begin(),valid.end());
//    for(i=0;i<valid.size();i++)
//    {
//        cout<<valid[i]<<" ";
//    }
    //cout<<endl<<"valid list"<<endl;
    long long int need=n,total=0;
    for(i=0;i<valid.size();i++)
    {
        //cout<<"taken at "<<i<<" need="<<need<<" sz="<<valid.size()<<endl;
        total+=valid[i];
        //cout<<"available="<<valid.size()-i-need+1<<endl;
        i+=min(k,valid.size()-i-need+1);
        i--;
        need--;
    }
    printf("%lld\n",total);
    return 0;
}
