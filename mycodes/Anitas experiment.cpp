#include<bits/stdc++.h>

using namespace std;

int main()
{
    int n,i,j,t;
    scanf("%d",&t);
    while(t--)
    {
        long long int great=0,lesse=0,equall=0,previous,k;
        scanf("%d",&n);
        vector<long long int>arr(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&k);
            arr[i]=k;
            if(i==1)
            {
                previous=k;
            }
            else
            {

                if(k>previous)
                {
                    great++;
                }
                if(k<previous)
                {
                    lesse++;
                }
                if(k==previous)
                {
                    equall++;
                    great++;
                    lesse++;
                }
                previous=k;
            }
        }
       // cout<<"great="<<great<<" less="<<lesse<<" equal="<<equall<<endl;
       bool chk=0;
        if(great==n-1||lesse==n-1||equall==n-1)
        {
            if(equall==n-1)
            {
                printf("neutral\n");
            }
            else if(great==n-1)
            {
                printf("allGoodDays\n");
            }
            else if(lesse==n-1)
            {
                printf("allBadDays\n");
            }
            chk=1;
        }
        if(chk)
        {
            continue;
        }
        long long int specialday=0;
        long long int currentday;
        long long int mxday=0;
        for(i=2;i<=n-1;i++)
        {
            if(arr[i-1]<arr[i] && arr[i]>arr[i+1])
            {
                if(specialday==0)
                {
                    specialday=1;
                    currentday=i;
                    //cout<<"in start curday="<<currentday<<endl;
                }
                else
                {
                    //cout<<"i="<<i<<" curday="<<currentday<<" and mxday"<<mxday<<endl;
                    mxday=max(mxday,abs(i-currentday-1));
                    currentday=i;
                    //cout<<mxday<<endl;
                }

            }
        }
        if(mxday==0)
        {
            printf("none\n");
        }
        else
        {
            printf("%d\n",mxday);
        }
    }

}
