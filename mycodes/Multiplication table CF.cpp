#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()
{
    int n,i,j,m,k;
    scanf("%d",&n);
    vector<int>col(n+1);
    vector<vector<int> >matrix(n+1,col);

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            scanf("%d",&k);
            matrix[i][j]=k;
        }
    }
    int a1=matrix[1][2];
    for(i=2;i<=n;i++)
    {
        a1=GCD(a1,matrix[1][i]);
    }
    printf("%d ",a1);
    for(i=2;i<=n;i++)
    {
        printf("%d ",matrix[1][i]/a1);
    }
}
