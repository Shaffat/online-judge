#include<bits/stdc++.h>

using namespace std;

struct info
{
    int nxt;
    int counter;
};
void build_tree(string &s,vector<vector<info> >&tree)
{
    //cout<<"trying to build "<<s<<endl;
    int i,j,pos=0;
    for(i=0;i<s.size();i++)
    {
        if(i==s.size()-1){
            tree[pos][s[i]].counter++;
            break;
        }
        if(tree[pos][s[i]].nxt!=-1)
        {
            tree[pos][s[i]].counter++;
        }
        else
        {
            info tmp;
            tmp.nxt=-1;
            tmp.counter=0;
            vector<info>v(130,tmp);
            tree.push_back(v);
            tree[pos][s[i]].nxt=tree.size()-1;
            tree[pos][s[i]].counter++;
            //cout<<"after "<<pos<<" comes "<<tree.size()-1<<endl;
        }
        pos=tree[pos][s[i]].nxt;
    }
    //cout<<"finished"<<endl;
    return;
}

int ans_me(string &s,vector<vector<info> >&tree)
{
    int i,j,pos=0,res=0;
    for(i=0;i<s.size();i++)
    {
        //cout<<"finding pos="<<pos<<endl;
        if(pos==-1)
        {
            //cout<<"terminate"<<endl;
            res=0;
            break;
        }
//        cout<<"res="<<res<<endl;
//        cout<<"coming down"<<endl;
        res=tree[pos][s[i]].counter;
        pos=tree[pos][s[i]].nxt;
    }
    //cout<<"done"<<endl;
    return res;
}

int main()
{
    //cout << "Start from main" << endl;

//    freopen("input03.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ios_base::sync_with_stdio(0);
    int i,j,m,n,t=0;
    string s;
    info tmp;
    tmp.nxt=-1;
    tmp.counter=0;

    vector<vector<info> >tree;
    vector<info>v(130,tmp);
    tree.push_back(v);
    cin>>n;
    //cin.ignore();
    //cout << "N is : " << n << endl;
    for(i=1;i<=n;i++)
    {
        string cmd,s;
        int ans;
        cin>>cmd>>s;
        if(cmd=="add"){
        //cout<<"add command"<<endl;
        build_tree(s,tree);
        }
        else
        {
            ans=ans_me(s,tree);
            cout<<ans<<"\n";
            t++;
        }

    }
}
