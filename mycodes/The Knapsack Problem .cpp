#include<bits/stdc++.h>

using namespace std;
vector<int>p(2000,-1);
vector<vector<int> >memory(2000,p);
struct object
{
    int weight,cost;
};
int napsack(int index,int w,int cap,vector<object>&items)
{
    if(index==items.size())
    {
        return 0;
    }
    if(memory[index][w]!=-1)
    {
        return memory[index][w];
    }
    int profit1,profit2;
    if(w+items[index].weight<=cap)
    {
        profit1=items[index].cost+napsack(index+1,w+items[index].weight,cap,items);
    }
    else
        profit1=0;
    profit2=napsack(index+1,w,cap,items);
    return memory[index][w]=max(profit1,profit2);
}

int main()
{
    vector<object>items;
    int s,n,i,j;
    scanf("%d %d",&s,&n);
    for(i=1;i<=n;i++)
    {
        object tmp;
        scanf("%d %d",&tmp.weight,&tmp.cost);
        items.push_back(tmp);
    }
    printf("%d\n",napsack(0,0,s,items));
}
