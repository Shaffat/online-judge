#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define mod 100000007
ll t,k;
ll solve(ll index, ll need, vector<ll>&coins)
{
    vector<int>memory(need+1,0);
    memory[0]=1;
    int i,j;
    for(i=0;i<coins.size();i++)
    {
        for(j=coins[i];j<=need;j++)
        {
            memory[j]+=memory[j-coins[i]];
            memory[j]%=mod;
        }
    }
    return memory[need];
}
int main()
{

    ll test,n,i,j,res;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        vector<ll>coins;
        scanf("%lld %lld",&n,&k);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            coins.push_back(j);
        }

        res=solve(0,k,coins);
        printf("Case %lld: %lld\n",t,res);
    }
}
