#include<bits/stdc++.h>
using namespace std;

vector<int>primes;
vector<bool>ar(10000,0);
void sieve()
{
    int i,j;
    ar[1]=1;
    for(i=4;i<1e4;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e4);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e4;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e4;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}


int solve(int n)
{
    int counter=0,i,j;
    for(i=0;i<primes.size();i++)
    {
        if(n%primes[i]==0){
            counter++;
            while(n%primes[i]==0){
                n/=primes[i];
            }
        }
    }
    if(n>1){
        counter++;
    }
    return counter;
}

int main()
{
    int n;
    sieve();
    while(scanf("%d",&n))
    {
        if(n==0) break;
        printf("%d : %d\n",n,solve(n));
    }
}
