#include<bits/stdc++.h>
using namespace std;
vector<int>col(10,0);
vector<vector<int> >result(1e6+1,col);

vector<int>memory(1e6+1,-1);

int f(int x)
{
    int total=1;
    while(x!=0)
    {
        int nxt=x%10;
        if(nxt)
        total*=x%10;
        x/=10;
    }
    return total;
}

int calculate(int x)
{
    if(x<10)
    {
        return x;
    }
    if(memory[x]!=-1)
    {
        return memory[x];
    }
    return memory[x]=calculate(f(x));
}

int main()
{
   int i,j,q,l,r,k;
   for(i=1;i<=1e6;i++)
   {
       j=calculate(i);
       result[i][j]++;
       for(j=0;j<=9;j++)
       {
           result[i][j]+=result[i-1][j];
       }
   }
   scanf("%d",&q);
   for(i=1;i<=q;i++)
   {
       scanf("%d %d %d",&l,&r,&k);
       printf("%d\n",result[r][k]-result[l-1][k]);

   }
}
