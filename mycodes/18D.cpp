#include<bits/stdc++.h>
using namespace std;

void sieve(int N,int *ar)

{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}
int main()
{
    int ar[10000];
   sieve(9999,ar);
   vector<int>number(1000000);
   number.at(1)=1;
   for(int i=2;i<100000;i+=2)
   {
       number.at(i)=1;
   }
   for(int i=3;i<10000;i+=2)
   {
       if(ar[i]==0&&i%10!=3)
       {
           int m=i,k=1;
           while(m*k<1000000)
           {
               number.at(m*k)=1;
               k++;
           }
       }
   }
   int n;
   while(cin>>n)
   {
       if(n==-1)
       {
           break;

       }
       if(number.at(n)==0)
       {
           cout<<n<<" "<<"YES"<<endl;

       }
       else cout<<n<<" "<<"NO"<<endl;
   }

}
