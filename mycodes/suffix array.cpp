#include<bits/stdc++.h>

using namespace std;

struct attribute
{
    int pos,s,t;
};

bool operator <(attribute a, attribute b)
{
    if(a.s!=b.s)
    {
        return a.s<b.s;
    }
    else
        return a.t<b.t;
}

int main()
{
    int i,j,k,l,m,iteration=0;
    string s;
    cin>>s;
    vector<attribute>v;
    vector<attribute>position(s.size());
    for(i=0;i<s.size();i++)
    {
        //cout<<"i="<<i<<endl;
        attribute tmp;
        tmp.pos=i;
        tmp.s=s[i];
        tmp.t=-1;
        //cout<<"tmp pos="<<tmp.pos<<endl;
        v.push_back(tmp);
//        cout<<"position"<<endl;
        position[i]=tmp;
        //cout<<"position "<<i<<" p="<<tmp.pos<<" s="<<tmp.s<<" t="<<tmp.t<<endl;
        //cout<<"done"<<endl;

    }
    sort(v.begin(),v.end());
//    for(i=0;i<v.size();i++)
//    {
//        cout<<v[i].pos<<" s="<<v[i].s<<" t="<<v[i].t<<endl;
//    }

    int last=log2(s.size())+1;
    for(iteration=0;iteration<=last;iteration++)
    {
        //cout<<"iteration="<<iteration<<endl;
        int pre_s=-1,pre_t=-1,r=0;
        for(i=0;i<v.size();i++)
        {
            if(v[i].s!=pre_s || v[i].t!=pre_t)
            {
                pre_s=v[i].s;
                pre_t=v[i].t;
                r++;
            }
            //cout<<"i="<<i<<" r="<<r<<endl;
            position[v[i].pos].s=r;
            v[i].s=r;
        }
//        for(i=0;i<v.size();i++)
//        {
//            cout<<v[i].pos<<" s="<<v[i].s<<endl;
//        }
        for(i=0;i<v.size();i++)
        {
            //cout<<"i="<<i<<endl;
            int p,nxt;
            p=v[i].pos;
            nxt=p+(1<<iteration);
            //cout<<"pos="<<p<<" nxt="<<nxt<<endl;
            if(nxt>=s.size()) {
                    v[i].t=-1;
                    position[p].t=-1;
                    //cout<<"hey update "<<p<<" to -1"<<endl;
            }
            else
            {
                v[i].t=position[nxt].s;
                position[p].t=position[nxt].s;
                //cout<<"update "<<p<<" to "<<position[nxt].s<<endl;
            }
        }
//        for(i=0;i<v.size();i++)
//        {
//            cout<<"pos="<<v[i].pos<<" s="<<v[i].s<<" "<<v[i].t<<endl;
//        }

        sort(v.begin(),v.end());
    }
    for(i=0;i<v.size();i++)
    {
        cout<<v[i].pos<<endl;
    }

}
