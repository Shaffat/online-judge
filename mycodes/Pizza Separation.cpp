#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j;
    scanf("%d", &n);
    vector<int>pieces(n+1,0);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        pieces[i]+=pieces[i-1]+j;
    }
    int dif=2e9;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            int res;
            if(n-j+1<i)
            {
                res=pieces[n]-pieces[j-1]+pieces[i-(n-j+1)];
                //cout<<"len="<<i<<" from "<<j<<" to "<<n<<" and 1 to "<<i-(n-j+1)<<endl;
            }
            else
                res=pieces[n]-pieces[j-1];
            dif=min(dif,abs(360-res-res));
        }
    }
    cout<<dif<<endl;
}
