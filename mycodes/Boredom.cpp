#include<bits/stdc++.h>
using namespace std;
#define ll long long int

ll solve(ll pos,vector<ll>&elements,vector<ll>&counters,vector<ll>&memory)
{
   //cout<<"pos="<<pos<<" size="<<elements.size()<<endl;
    if(pos>=elements.size()-1)
    {
        if(pos==(elements.size()-1))
        {
            int cur=elements[pos];
            //cout<<"cur="<<cur<<" count="<<counters[cur]<<endl;
            return cur*counters[cur];
        }
        return 0;
    }
    if(memory[pos]!=-1)
    {
        return memory[pos];
    }
    ll case1=0,case2=0,case3=0,cur=elements[pos];
    //cout<<"cur="<<cur<<endl;
    if(pos+1<elements.size())
    {
        int next=elements[pos+1];
        //cout<<"next="<<next<<endl;
        if(next>cur+1)
        {
            case1=solve(pos+1,elements,counters,memory)+cur*counters[cur];
        }
        else
        {
            //cout<<"equal"<<endl;
            case1=solve(pos+2,elements,counters,memory)+cur*counters[cur];
            //cout<<"case1="<<case1<<endl;
            case2=solve(pos+1,elements,counters,memory);
            //cout<<"case2="<<case2<<endl;
        }

    }
    return memory[pos]=max(case1,case2);
}

int main()
{
    ll n,i,j;
    scanf("%lld",&n);
    vector<ll>counters(1e5+1,0);
    vector<ll>elements;
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        if(counters[j]==0)
        {
            elements.push_back(j);
        }
        counters[j]++;
    }
    vector<ll>memory(1e5,-1);
    sort(elements.begin(),elements.end());
    cout<<solve(0,elements,counters,memory);

    return 0 ;
}
