#include<bits/stdc++.h>

using namespace std;


string sum(string &a,string &b)
{
    //cout<<"a="<<a<<"\n b="<<b<<endl;
    if(a.size()<b.size())
    {
        swap(a,b);
    }
    reverse(b.begin(),b.end());
    while(b.size()<a.size())
    {
        b.push_back('0');
    }
    reverse(b.begin(),b.end());
    //cout<<"a="<<a<<"\n b="<<b<<endl;
    string res="";
    int i,j,carry=0;
    for(i=a.size()-1;i>=0;i--)
    {
        int cur=(a[i]-'0')+(b[i]-'0')+carry;
        //cout<<"cur="<<cur<<" a="<<a[i]<<" b="<<b[i]<<" carry="<<carry<<endl;
        if(cur>9)
        {
            carry=1;
            res.push_back('0'+cur-10);
        }
        else
        {
            carry=0;
            res.push_back('0'+cur);
        }
    }
    if(carry)
    {
        res.push_back('0'+carry);
    }
    reverse(res.begin(),res.end());
    return res;
}


int main()
{
    ios_base::sync_with_stdio(0);
//    freopen("datas/data4.in","r",stdin);
//    freopen("out.txt","w",stdout);
    string a,b,res;
    cin>>a>>b;
 //   cout<<a.size()<<" "<<b.size()<<endl;
    res=sum(a,b);
    cout<<res<<endl;
}
