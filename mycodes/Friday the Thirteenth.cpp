/*
ID: msdipu11
PROG: friday
LANG: C++14
*/
#include<bits/stdc++.h>
using namespace std;

bool is_leap(int y)
{
    if(y%400==0)
    {
        return 1;
    }
    if(y%4==0 && y%100!=0)
    {
        return 1;
    }
    return 0;
}

int main()
{
    freopen("friday.in","r",stdin);
    freopen("friday.out","w",stdout);
    int i,j,n,cur_year,end_day,cur_month;
    vector<int>month(13);
    month[1]=31;
    month[2]=28;
    month[3]=31;
    month[4]=30;
    month[5]=31;
    month[6]=30;
    month[7]=31;
    month[8]=31;
    month[9]=30;
    month[10]=31;
    month[11]=30;
    month[12]=31;
    scanf("%d",&n);
    cur_year=1900;
    end_day=2;
    vector<int>total(7,0);
    for(i=cur_year;i<cur_year+n;i++)
    {
        if(is_leap(i))
        {
            month[2]=29;
        }
        else
            month[2]=28;
        for(j=1;j<=12;j++)
        {
            total[(end_day+13)%7]++;
            end_day=(end_day+month[j])%7;
        }

    }
    for(i=1;i<=7;i++)
    {
        if(i==7)
        {
            printf("%d\n",total[i%7]);
        }
        else
        printf("%d ",total[i%7]);
    }


}
