#include<bits/stdc++.h>
using namespace std;

struct roundAndWin
{
    double r,w;
};

roundAndWin solve(int a,int b,vector<vector<double> >&memory,vector<vector<int> >&done )
{
    int c=min(a,b);
    if(c==0) return 1;
    if(done[a][b]==2)
    {
        return memory[a][b];
    }
    roundAndWin res,tmp;
    res.w=0;
    res.r=0;
    if(done[a+c][b-c]!=1){
        tmp=solve(a+c,b-c,memory,done);
        if(done[a+c][b-c]==0){
            done[a+c][b-c]=1;
        }
        res.w+=tmp.w;
        res.r+=tmp.r+1;
    }
     if(done[a-c][b+c]!=1){
        tmp=solve(a-c,b+c,memory,done);
        if(done[a-c][b+c]==0){
            done[a-c][b+c]=1;
        }
        res.w+=tmp.w;
        res.r+=tmp.r+1;
    }
    done[a][b]=2;
    res.r/=2;
    res.w/=2;
    return res;
}


int main()
{

}
