#include<bits/stdc++.h>
using namespace std;

list <int> solve(list <int> &l)
{
    int sz=l.size();

    list <int> :: iterator it;
    //this loop iterates the given list from last node to middle node
    for(it = l.end(); it != l.begin(); it--)
    {
        if(sz<=l.size()/2) break;
        sz--;
    }
    return l;
}

int main()
{
  int i,j,n;
  scanf("%d",&n);
  list<int>ls;
  list<int>res;
  for(i=1;i<=n;i++)
  {
      scanf("%d",&j);
      ls.push_back(j);
  }
  res=solve(ls);
  list <int> :: iterator it;
   for(it = res.begin(); it != res.end(); ++it)
        cout <<*it<<" ";
  return 0;
}
