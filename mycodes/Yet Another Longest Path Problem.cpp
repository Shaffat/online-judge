#include<bits/stdc++.h>
using namespace std;

void solve(int n,vector<vector<int> >&connection)
{
    int i,j;
    vector<int>vis(n+1,0);
    vector<int>level(n+1,0);
    vis[1]=1;
    level[1]=0;
    queue<int>q;
    q.push(1);
    while(!q.empty())
    {

        int cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            int nxt=connection[cur][i];
            if(!vis[nxt])
            {
                level[nxt]=level[cur]+1;
                vis[nxt]=1;
                q.push(nxt);
                if(level[cur]%2==0)
                {
                    printf("%d %d\n",cur,nxt);
                }
                else
                    printf("%d %d\n",nxt,cur);
            }
        }
    }
    return;
}

int main()
{
    int test,t,i,j,n,u,v;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        for(i=1;i<n;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        printf("Case %d:\n",t);
        solve(n,connection);
    }
}
