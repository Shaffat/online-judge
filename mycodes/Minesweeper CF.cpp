#include<bits/stdc++.h>
using namespace std;

char board[101][101];

bool valid(int r,int c,int n,int m)
{
    if(r<1 || r>n || c<1 || c>m)
    {
        return false;
    }
    return true;
}

int bombcount(int r,int c,int n,int m)
{
    int counter=0,cur_r,cur_c;
    cur_r=r-1;
    cur_c=c;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r+1;
    cur_c=c;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r;
    cur_c=c-1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r;
    cur_c=c+1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r-1;
    cur_c=c-1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r-1;
    cur_c=c+1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r+1;
    cur_c=c-1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    cur_r=r+1;
    cur_c=c+1;
    if(valid(cur_r,cur_c,n,m))
    {
        if(board[cur_r][cur_c]=='*')
        {
            counter++;
        }
    }
    return counter;
}

int main()
{
    int n,m,i,j;
    scanf("%d %d",&n,&m);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf(" %c",&board[i][j]);
        }
    }
    bool chk=1;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            if(board[i][j]=='*')
            {
                continue;
            }
            else if(board[i][j]=='.')
            {
                if(bombcount(i,j,n,m)>0)
                {
                    chk=0;
                    break;
                }
            }
            else
            {
                if(bombcount(i,j,n,m)!=board[i][j]-'0')
                {
                    chk=0;
                    break;
                }
            }
        }
        if(!chk)
        {
            break;
        }
    }
    if(chk)
    {
        cout<<"YES\n";
    }
    else
        cout<<"NO\n";
}
