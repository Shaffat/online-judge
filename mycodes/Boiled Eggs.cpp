#include<bits/stdc++.h>
using namespace std;

int solve(int index,int limit,int weight,vector<int>&eggs,vector<vector<vector<int> > >&memory)
{
    //cout<<"index"<<index<<" limit="<<limit<<" weight="<<weight<<endl;
    if(index>=eggs.size()||limit<=0||weight<0)
    {
        return 0;
    }
    if(memory[index][limit][weight]!=-1)
    {
        return memory[index][limit][weight];
    }
    int case1=0,case2=0;
    if(weight>=eggs[index])
    {
        case1=solve(index+1,limit-1,weight-eggs[index],eggs,memory)+1;
    }
    case2=solve(index+1,limit,weight,eggs,memory);
    return memory[index][limit][weight]=max(case1,case2);
}

int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,i,j,p,q;
        scanf("%d %d %d",&n,&p,&q);
        vector<int>eggs;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            eggs.push_back(j);
        }
        vector<int>col1(40,-1);
        vector<vector<int> >col2(40,col1);
        vector<vector<vector<int> > >memory(40,col2);
        int res=solve(0,p,q,eggs,memory);
        printf("Case %d: %d\n",t,res);
    }
}
