#include<bits/stdc++.h>
using namespace std;


bool solve(string &s)
{
    //cout<<"s="<<s<<endl;
    int odd=0,even=0,i,j;
    for(i=0;i<s.size();i+=2)
    {
        int j=s[i]-'0';
        j*=2;
        if(j>9)
        {
            j-=9;
        }
        odd+=j;
    }
    //cout<<"odd="<<odd<<endl;
    for(i=1;i<s.size();i+=2)
    {
        even+=s[i]-'0';
    }
    //cout<<"even="<<even<<endl;
    even+=odd;
    if(even%10==0)
    {
        return true;
    }
    return false;
}

string adjust(string &s)
{
    int i,j;
    string res="";
    for(i=0;i<s.size();i++)
    {
        if(s[i]!=' ')
        {
            res.push_back(s[i]);
        }
    }
    return res;
}

int main()
{
    map<string,int>id;
    string s,res;
    while(getline(cin,s))
    {
        if(s=="0000 0000 0000 0000")
        {
            break;
        }
        id[s]+=1;
        res=adjust(s);
        if(solve(res)&&id[s]==1)
        {
            printf("Yes\n");
        }
        else
            printf("No\n");
    }
}
