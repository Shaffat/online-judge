#include<bits/stdc++.h>
using namespace std;

struct pos
{
    long long int index,value;
};

bool operator<(pos a, pos b)
{
    if(a.value!=b.value)
    {
        return a.value<b.value;
    }
    return a.index<b.index;
}
void update(long long int n,long long int value,long long int index,vector<long long int>&tree)
{
    while(index<=n)
    {
        tree[index]+=value;
        index+=index&(-index);
    }
}

long long int query(long long int index,vector<long long int>&tree)
{
    long long int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index&(-index);
    }
    return sum;
}

long long int Inverstion_counting(vector<pos>&data)
{
    long long int i,j,counter=0;
    vector<long long int>tree(data.size()+1,0);
    for(i=0;i<data.size();i++)
    {
        long long int sorted_pos=i+1;
        long long int real_pos=data[i].index;
        long long int smallervalues=query(real_pos,tree);
        update(data.size(),1,real_pos,tree);
        counter+=max(0ll,real_pos-smallervalues-1);
    }
    return counter;
}
int main()
{
    long long int test,t=1;
    scanf("%lld",&test);
    while(t<=test)
    {
        vector<pos>data;
        long long int i,j,n;
        scanf("%lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            pos tmp;
            tmp.index=i;
            tmp.value=j;
            data.push_back(tmp);
        }
        sort(data.begin(),data.end());
        long long int res=Inverstion_counting(data);
        printf("%lld\n",res);
        t++;
    }
}
