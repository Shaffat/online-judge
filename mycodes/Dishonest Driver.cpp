
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())



bool ok(ll len,string &s)
{
   ll i,j;
   //cout<<"len="<<len<<" s="<<s<<endl;
   for(i=len;i<s.size();i+=len)
   {
       for(j=0;j<len;j++)
       {
           if(s[i+j]!=s[j])
            return false;
       }
   }
   //cout<<"valid "<<endl;
   return true;
}
ll compress(ll st,ll e,string &s,vll &memory)
{
    //cout<<"compress "<<st<<" "<<e<<endl;
    ll i,j,mn=1e9,l;
    string tmp="";
    FOR(i,st,e)
    {
        tmp.push_back(s[i]);
    }


    l=tmp.size();
    //cout<<"l="<<l<<endl;
    FOR(i,1,sqrt(l))
    {
        //cout<<"partition of len "<<i<<endl;
        if(l%i==0)
        {
            //cout<<i<<" is a candidate"<<endl;
            if(ok(i,tmp)){
                //cout<<i<<" len is valid memory["<<st<<"]["<<st+i-1<<"]="<<memory[st][st+i-1]<<endl;
                mn=min(mn,memory[st][st+i-1]);
            }
            if(ok(l/i,tmp) && i!=1){
                //cout<<l/i<<" len is valid memory["<<st<<"]["<<st+(l/i)-1<<"]="<<memory[st][st+(l/i)-1]<<endl;
                mn=min(mn,memory[st][st+(l/i)-1]);
            }
        }
    }
    return mn;
}

void solve(ll st,ll e,string &s,vector<vector<ll> >&memory)
{
    //cout<<"st="<<st<<" e="<<e<<endl;
    if(st==e){
        //cout<<"equal basecase "<<st<<" "<<e<<endl;
        memory[st][e]=1;
        return;
    }
    if(memory[st][e]!=-1){
         memory[st][e];
         return;
    }
    ll i,j,res1,res2=1e9,res3,res4=1e9;

    //cout<<"res1="<<res1<<endl;
    FOR(i,1,e-st)
    {
        solve(st,st+i-1,s,memory);

        solve(st+i,e,s,memory);
        res3=compress(st,st+i-1,s,memory);

        res2=compress(st+i,e,s,memory);
        res4=min(res1,res3+res2);
    }
    res1=e-st+1;
    cout<<"memory["<<st<<"]["<<e<<"]="<<min(res1,res4)<<endl;;
    memory[st][e]=min(res1,res4);
    return;
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    ll n,i,j,res;
    string s;
    cin>>n>>s;
    vl col(s.size()+1,-1);
    vll memory(s.size()+1,col);
    solve(0,s.size()-1,s,memory);
    res=memory[0][s.size()-1];
    cout<<res<<endl;
}
