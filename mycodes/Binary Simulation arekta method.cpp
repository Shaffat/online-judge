#include<bits/stdc++.h>

using namespace std;

int main()
{
    int n,test,i,q,j,t=1,u,v;
    scanf("%d",&test);
    while(t<=test)
    {
        string s;
        cin>>s;
        n=s.size();
        vector<int>tree(n+1,0);
        vector<int>values(n+1,0);
        for(i=0;i<s.size();i++)
        {
            if(s[i]=='1')
            {
                values[i+1]=1;
            }
        }
        int cumulative=1;
        scanf("%d",&q);
        getchar();
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {

          char c;
          scanf("%c",&c);
          if(c=='I')
          {
              scanf("%d %d",&u,&v);
              tree[u]+=1;
              tree[v+1]-=1;
          }
          else
          {
              if(cumulative)
              {
                  cumulative=0;
                  for(i=1;i<s.size();i++)
                  {
                      tree[i]+=tree[i-1];
                  }
              }
              scanf("%d",&u);
              int res=tree[u]+values[u];
              //cout<<"res="<<res<<endl;
              printf("%d\n",res%2);
          }


        }

        t++;
    }
}

