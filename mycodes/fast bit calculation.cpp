#include<bits/stdc++.h>

typedef long long int llu;

using namespace std;

vector<llu>memory(35,-1);

int mostSig( int n ) {
    int ret = -1 ;

    for(int i = 0 ; i < 32 ; i ++ ) {
        if( n & ( 1 << i ) ) {
            ret = i ;
        }
    }

    return ret ;
}

llu solve(int n)
{
    long long int bit,i,j,restriction=1;
    bit=mostSig(n);
    if(bit==0 || n==0)
    {
        return 0;
    }
    if(((1LL<<(bit+1LL))-1)==n)
    {
        restriction=0;
    }
    if(restriction==0)
    {
        //cout<<"no restriction n="<<n<<" bit="<<bit<<endl;
        if(n==3)
        {
            return 1;
        }
        if(memory[bit]!=-1)
        {
            return memory[bit];
        }
        long long int extra=n&((1ll<<bit)-1);
        return memory[bit]=solve((1<<bit)-1)+solve(extra)+max(extra-(1ll<<(bit-1))+1,0ll);

        ///exmaple 1100=     111        +       100      +  (100-100+1)
        ///             (n-1bits) MSB 0 bits + (n-1bits) MSB  1 bits + new adjacents
    }
    else
    {
        long long int extra=n&((1ll<<bit)-1);
        //cout<<"n="<<n<<" extra="<<extra<<endl;
        return solve((1ll<<bit)-1)+solve(extra)+max(extra-(1<<(bit-1))+1,0ll);
    }
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test ,t=1,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        printf("Case %d: %lld\n",t,solve(n));
    }
}
