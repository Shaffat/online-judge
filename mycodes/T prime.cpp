#include<bits/stdc++.h>

using namespace std;
int ar[1000001];
void sieve()

{
    int i,N=1000000;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
            for(int j=i*i;j<=N;j+=i)
            {
                ar[j]=1;
            }
        }
    }
}

int main()
{
    sieve();
    double test,n;
    while(cin>>test)
    {

        while(test--)
        {
            cin>>n;
           double s=sqrt(n);
          if(floor(s)==ceil(s))
          {
              int k=s;
              if(ar[k]==0)
              {
                  cout<<"YES\n";
              }
              else
              {
                  cout<<"NO\n";
              }
          }
          else
          {
              cout<<"NO\n";
          }
        }
    }
}


