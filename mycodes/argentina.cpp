#include<bits/stdc++.h>
using namespace std;
struct person
{
    string s;
    int atk,def;
};

bool operator <(person a,person b)
{
    if(a.atk!=b.atk)
    {
        return a.atk>b.atk;
    }
    if(a.def!=b.def)
    {
        return a.def<b.def;
    }
    if(a.s!=b.s)
    {
        return a.s<b.s;
    }
    return false;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t=1;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        int i,j,a,d;
        string s;
        vector<person>player;
        for(i=1;i<=10;i++)
        {
            cin>>s>>a>>d;
            person tmp;
            tmp.s=s;
            tmp.atk=a;
            tmp.def=d;
            player.push_back(tmp);
        }
        sort( player.begin() , player.end() ) ;

        vector<string>atkers,defenders;
        for(i=0;i<5;i++)
        {
            atkers.push_back(player[i].s);
        }
        for(i=5;i<10;i++)
        {
            defenders.push_back(player[i].s);
        }
        sort(atkers.begin(),atkers.end());
        sort(defenders.begin(),defenders.end());
        printf("Case %d:\n(%s, ",t,atkers[0].c_str());
        for(i=1;i<5;i++)
        {
            if(i==4)
            {
                printf("%s)\n",atkers[i].c_str());
            }
            else
                printf("%s, ",atkers[i].c_str());
        }
        printf("(%s, ",defenders[0].c_str());
        for(i=1;i<5;i++)
        {
            if(i==4)
            {
                printf("%s)\n",defenders[i].c_str());
            }
            else
                printf("%s, ",defenders[i].c_str());
        }



    }
}
