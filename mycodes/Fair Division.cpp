#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int p,v;
};
bool operator <(pos a, pos b)
{
    if(a.v!=b.v)
    {
        return a.v>b.v;
    }
    return a.p<b.p;
}

int ok(int n,int mn,int need,int p,vector<int>&taka)
{
    //cout<<"p="<<p<<endl;
    int i,j;
    for(i=1;i<=n;i++)
    {
        need-=min(taka[i],min(need-((n-i)*mn),p));
        //cout<<"need="<<need<<endl;
    }
    if(need==0)
    {
        return true;
    }
    else
        return false;
}


int findHighest(int n,int need,int mn,int mx,vector<int>&taka)
{
    int first=mn,last=mx,mid,res=0,d=0;
    while(first<=last)
    {
        if(d) break;
        if(first==last)
        {
            d++;
        }
        mid=(first+last)/2;
        if(ok(n,mn,need,mid,taka))
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}

int main()
{
    int n,i,j,t,need,test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int total=0,mn=1e9,mx=-1;
        scanf("%d %d",&need,&n);
        vector<pos>postaka;
        vector<int>taka(n+1);
        for(i=1;i<=n;i++)
        {
            pos tmp;
            scanf("%d",&j);
            taka[i]=j;
            total+=j;
            mn=min(mn,j);
            mx=max(mx,j);
            tmp.p=i;
            tmp.v=j;
            //cout<<"i="<<i<<endl;
            postaka.push_back(tmp);
        }

        sort(postaka.begin(),postaka.end());
        vector<int>ans(n+1);
        if(total<need || need/n==0)
        {
            printf("IMPOSSIBLE\n");
        }
        else
        {
           int cantpay=0;
           for(i=0;i<n;i++)
           {
             //cout<<"start need ="<<need<<" cant pay="<<cantpay<<endl;
             int cur=need/(n-cantpay);
             //cout<<"here "<<endl;
             if(cur==0 && need%(n-cantpay)>0){
               // cout<<"need avarage is less than people"<<endl;
                for(int k=0;k<n;k++)
                {
                    if(need==0) break;

                    int p=postaka[k].p;
                    ans[p]++;
                    need--;
                }
             }
             if(need==0) break;
             //cout<<"cur="<<cur<<" need="<<need<<endl;
             for(j=0;j<n;j++)
             {
                 if(need==0) break;
                 int p=postaka[j].p;
                 if(taka[p]<cur && taka[p]>0)
                 {
                     need-=taka[p];
                     ans[p]+=taka[p];
                     taka[p]=0;
                     cantpay++;
                     //cout<<p<<" is going all out"<<endl;
                 }
                 else if(taka[p]>=cur)
                 {
                     need-=cur;
                     ans[p]+=cur;
                     taka[p]-=cur;
                 }
                 //cout<<"at pos "<<p<<" taka given "<<ans[p]<<" remain taka="<<taka[p]<<endl;
             }
             //cout<<"after need="<<need<<" cantpay="<<cantpay<<endl;
           }
           for(i=1;i<=n;i++)
           {
               printf("%d ",ans[i]);
           }
           printf("\n");
        }

    }
}
