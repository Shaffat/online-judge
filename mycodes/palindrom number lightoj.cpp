#include<bits/stdc++.h>

using namespace std;

int main()
{
    int test,t=1;
    cin>>test;
    while(t<=test)
    {
        string s;
        cin>>s;
        int n=s.size(),i,j,chk=1,mid;
        if(n%2==0)
        {
            mid=(n-1)/2;
            i=mid;
            j=mid+1;

            while(i>=0 && j<s.size())
            {

                if(s.at(i)!=s.at(j))
                {
                    chk=0;
                    break;
                }
                i--;
                j++;
            }
        }
        else
        {
            mid=(n-1)/2;
            i=mid-1;
            j=mid+1;

            while(i>=0 && j<s.size())
            {
                if(s.at(i)!=s.at(j))
                {

                    chk=0;
                    break;
                }
                i--;
                j++;
            }
        }
        if(chk==1)
        {
            printf("Case %d: Yes\n",t);
        }
        else
        {
            printf("Case %d: No\n",t);
        }
        t++;
    }
}
