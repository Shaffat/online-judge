#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()
{
    int n,i,j;
    while(scanf("%d",&n))
    {
        if(n==0) break;

        int total=0;
        vector<int>numbers;

        for(i=1;i<n;i++){
            for(j=i+1;j<=n;j++){
                total+=GCD(i,j);
            }
        }
        printf("%d\n",total);
    }
}
