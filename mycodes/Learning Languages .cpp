#include<bits/stdc++.h>
using namespace std;

#define REP(i,a,b)       for(int i=a;i<=b;i++)
#define RREP(i,a,b)      for(int i=a;i>=b;i--)
#define sc3(a,b,c)       scanf("%d %d %d",&a,&b,&c)
#define sc2(a,b)       scanf("%d %d",&a,&b)
#define sc1(a)         scanf("%d",&a)

int find_parent(int n,vector<int>&parent)
{
    if(parent[n]==n)
    {
        return n;
    }
    return parent[n]=find_parent(parent[n],parent);
}

int solve()
{
    int n,i,j,m,k,l;
    sc2(n,m);
    vector<int>parent(n+1);
    int no_language=0;
    REP(i,1,n)
    {
        parent[i]=i;
    }
    vector<int>v;
    vector<vector<int> >connection(m+1,v);
    vector<int>ranking(n+1,0);

    for(i=1;i<=n;i++)
    {
        sc1(k);
        if(k==0)
        {
            no_language++;
        }
        for(j=1;j<=k;j++)
        {
            sc1(l);
            connection[l].push_back(i);
        }
    }
    for(i=1;i<=m;i++)
    {
        int initialnode;
        for(j=0;j<connection[i].size();j++)
        {
            if(j==0)
            {
                initialnode=connection[i][j];
                continue;
            }
                ;
                int u=find_parent(initialnode,parent);
                int v=find_parent(connection[i][j],parent);
                //cout<<"parent of "<<initialnode<<" is "<<u<<" parent of "<<connection[i][j]<<" is "<<v<<endl;
                if(u!=v)
                {
                   if(ranking[u]>=ranking[v])
                   {
                       parent[v]=u;
                       ranking[u]++;
                   }

                   else
                    {
                        parent[u]=v;
                        ranking[v]++;
                    }
                }
                //cout<<" now parent of "<<initialnode<<" is "<<parent[u]<<" parent of "<<connection[i][j]<<" is "<<parent[v]<<endl;

        }
    }

    int count=0;
    REP(i,1,n)
    {
        parent[i]=find_parent(parent[i],parent);
    }
    vector<bool>dis(n+1,0);
    REP(i,1,n)
    {
        if(dis[parent[i]]==0)
        {
            dis[parent[i]]=1;
            count++;
        }
    }
    if(no_language==n)
    {
        count++;
    }
    return count-1;
}

int main()
{
    cout<<solve()<<endl;
}
