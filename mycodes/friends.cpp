#include<bits/stdc++.h>

using namespace std;

int find_parent(int r,int *ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    ar[r]=find_parent(ar[r],ar);
    return ar[r];
}
int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,m,i,j;
        scanf("%d %d",&n,&m);

        vector<int>total(n+1,0);
        vector<int>rank_(n+1,0);
        int parent[n+1];
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }

        for(i=1;i<=m;i++)
        {
            int u,v;
            scanf("%d %d",&u,&v);
            int x=find_parent(u,parent);
            int y=find_parent(v,parent);
            if(x!=y)
            {
                if(rank_[x]==rank_[y])
                {
                    parent[y]=x;
                    rank_[x]++;
                }
                else if(rank_[x]>rank_[y])
                {
                    parent[y]=x;
                }
                else
                {
                    parent[x]=y;
                }
            }

        }
        for(i=1;i<=n;i++)
        {
            int x=find_parent(i,parent);
            total[x]++;
        }
        int mx=0;
        for(i=1;i<=n;i++)
        {
            if(mx<total[i])
            {
                mx=total[i];
            }
        }
        printf("%d\n",mx);
        t++;

    }
}
