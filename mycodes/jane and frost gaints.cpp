#include<bits/stdc++.h>

struct point
{
    int x,y;
};
int r,c;
using namespace std;

void fireattacks(queue<point>q,vector<vector<int> >&vis,vector<vector<int> >&maze)
{
    while(!q.empty())
    {
        point cur;
        cur.x=q.front().x;
        cur.y=q.front().y;
        q.pop();
        int nxt_r,nxt_c,i,j;
        nxt_r=cur.x+1;
        nxt_c=cur.y;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]!=-1)
            {
                vis[nxt_r][nxt_c]=1;
                maze[nxt_r][nxt_c]=maze[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
            }
        }
        nxt_r=cur.x-1;
        nxt_c=cur.y;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]!=-1)
            {
                vis[nxt_r][nxt_c]=1;
                maze[nxt_r][nxt_c]=maze[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
            }
        }
        nxt_r=cur.x;
        nxt_c=cur.y+1;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]!=-1)
            {
                vis[nxt_r][nxt_c]=1;
                maze[nxt_r][nxt_c]=maze[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
            }
        }
        nxt_r=cur.x;
        nxt_c=cur.y-1;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]!=-1)
            {
                vis[nxt_r][nxt_c]=1;
                maze[nxt_r][nxt_c]=maze[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
            }
        }

    }
}

int solve(point jane,vector<vector<int> >&maze)
{
    vector<int>col(c+1,0);
    vector<vector<int> >vis(r+1,col);
    vector<vector<int> >dis(r+1,col);
    queue<point>q;
    vis[jane.x][jane.y]=1;
    q.push(jane);
    while(!q.empty())
    {
        point cur;
        cur.x=q.front().x;
        cur.y=q.front().y;
        if(cur.x==r || cur.x==1 || cur.y==c|| cur.y==1)
        {
            return dis[cur.x][cur.y]+1;
        }
        q.pop();
        int nxt_r,nxt_c,i,j;
        nxt_r=cur.x+1;
        nxt_c=cur.y;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]>dis[cur.x][cur.y]+1)
            {
                vis[nxt_r][nxt_c]=1;
                dis[nxt_r][nxt_c]=dis[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
                //cout<<"pushed "<<nxt_r<<" "<<nxt_c<<" "<<dis[nxt_r][nxt_c]<<" "<<maze[nxt_r][nxt_c]<<endl;
            }
        }
        nxt_r=cur.x-1;
        nxt_c=cur.y;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]>dis[cur.x][cur.y]+1)
            {
                vis[nxt_r][nxt_c]=1;
                dis[nxt_r][nxt_c]=dis[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
                //cout<<"pushed "<<nxt_r<<" "<<nxt_c<<" "<<dis[nxt_r][nxt_c]<<" "<<maze[nxt_r][nxt_c]<<endl;
            }
        }
        nxt_r=cur.x;
        nxt_c=cur.y+1;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]>dis[cur.x][cur.y]+1)
            {
                vis[nxt_r][nxt_c]=1;
                dis[nxt_r][nxt_c]=dis[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
                //cout<<"pushed "<<nxt_r<<" "<<nxt_c<<" "<<dis[nxt_r][nxt_c]<<" "<<maze[nxt_r][nxt_c]<<endl;
            }
        }
        nxt_r=cur.x;
        nxt_c=cur.y-1;
        if(nxt_r>0 &&nxt_r<=r&&nxt_c>0 && nxt_c<=c)
        {
            if(vis[nxt_r][nxt_c]==0 && maze[nxt_r][nxt_c]>dis[cur.x][cur.y]+1)
            {
                vis[nxt_r][nxt_c]=1;
                dis[nxt_r][nxt_c]=dis[cur.x][cur.y]+1;
                point tmp;
                tmp.x=nxt_r;
                tmp.y=nxt_c;
                q.push(tmp);
                //cout<<"pushed "<<nxt_r<<" "<<nxt_c<<" "<<dis[nxt_r][nxt_c]<<" "<<maze[nxt_r][nxt_c]<<endl;
            }
        }

    }
    return -1;
}

int main()
{
//     freopen("in.txt","r",stdin);
//     freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&r,&c);
        vector<int>col(c+1,0);
        vector<int>col1(c+1,2e9);
        vector<vector<int> >maze(r+1,col1);
        vector<vector<int> >vis(r+1,col);
        //vector<vector<int> >dis(r+1,col);
        queue<point>fire;
        int i,j;
        point jane;
        for(i=1;i<=r;i++)
        {
            getchar();
            for(j=1;j<=c;j++)
            {
                char k;
                scanf("%c",&k);
                if(k=='#')
                {
                    maze[i][j]=-1;
                }
                else
                {
                    if(k=='J')
                    {
                        jane.x=i;
                        jane.y=j;
                    }
                    if(k=='F')
                    {
                        point tmp;
                        tmp.x=i;
                        tmp.y=j;
                        fire.push(tmp);
                        vis[i][j]=1;
                        maze[i][j]=0;
                    }
                }
            }
        }
        fireattacks(fire,vis,maze);
//        for(i=1;i<=r;i++)
//        {
//            for(j=1;j<=c;j++)
//            {
//                cout<<maze[i][j]<<" ";
//            }
//            cout<<endl;
//        }
        int res=solve(jane,maze);
        if(res>-1)
        {
            printf("Case %d: %d\n",t,res);
        }
        else
            printf("Case %d: IMPOSSIBLE\n",t);
            t++;
    }
}
