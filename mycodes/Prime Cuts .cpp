#include<bits/stdc++.h>
using namespace std;

vector<int>primes;
vector<int>counting(1010,-1);
void sieve()
{
    vector<int>ar(1010,0);
    ar[1]=0;
    int i,j;
    for(i=4;i<=1000;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1000);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<=1000;j+=i)
            {
                ar[j]=1;
            }
        }
    }
    primes.push_back(1);
    primes.push_back(2);
    counting[1]=0;
    counting[2]=1;
    for(i=3;i<=1000;i++)
    {
        if(ar[i]==0)
        {
            //cout<<"i="<<i<<endl;
            primes.push_back(i);
            //cout<<"size="<<primes.size()<<endl;
        }
        counting[i]=primes.size()-1;
        //cout<<"counting="<<counting[i]<<endl;
    }
}


int main()
{

    sieve();
    int n,c,i,j;
    while(scanf("%d %d",&n,&c)!=EOF)
    {
        printf("%d %d:",n,c);
        int amount=counting[n]+1;
        if(amount%2==0)
        {
            c=c*2;
            if(c>amount)
            {
                for(i=0;i<amount;i++)
                {
                    printf(" %d",primes[i]);
                }
            }
            else
            {
                int extra=amount-c,up,down;
                up=extra/2;
                down=amount-extra/2;
                for(i=up;i<down;i++)
                {
                    printf(" %d",primes[i]);
                }
            }
        }
        else
        {
            c=c*2-1;
            if(c>amount)
            {
                for(i=0;i<amount;i++)
                {
                    printf(" %d",primes[i]);
                }
            }
            else
            {
                int extra=amount-c,up,down;
                up=extra/2;
                down=amount-extra/2;
                for(i=up;i<down;i++)
                {
                    printf(" %d",primes[i]);
                }
            }
        }
        printf("\n\n");
    }

}
