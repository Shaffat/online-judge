#include<bits/stdc++.h>
using namespace std;
#define ll long long int

struct answer
{
    ll mine,cost;
};

answer cost_count(ll x,vector<vector<int> >&cumulative_cost)
{
    ll total=0,i,j,k=0;
    for(i=1;i<cumulative_cost.size();i++)
    {
        if(cumulative_cost[i].size()>=x)
        {
            j=cumulative_cost[i].size()-x;
            k+=j;
            total+=cumulative_cost[j-1];
        }
    }
    answer tmp;
    tmp.cost=total;
    tmp.mine=k;
    return tmp;
}

ll solve(vector<vector<int> >&cumulative_cost,int mx)
{
    ll first,last,mid,res=2e13,f=0;
    first=0;
    last=mx;
    while(first<=last)
    {
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f=1;
        }
        mid=(first+last)/2;
        answer tmp=cost_count(mid,cumulative_cost);
        if(tmp.mine+cumulative_cost[1].size()>mid)
        {
            last=mid-1;
            res=min(tmp.cost,res);
        }
        else
            first=mid+1;
    }
    return res;

}

int main()
{
    ll n,m,i,j;
    scanf("%lld %lld",&n,&m);
    for(i=)
}
