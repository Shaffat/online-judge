#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>res;
ll sumofdigit(ll x)
{
    ll sum=0;
    while(x>0)
    {
        sum+=x%10;
        x/=10;
    }
    return sum;
}

void solve(ll number)
{
    ll i;
    for(i=max(1ll,number-100);i<=number;i++)
    {
        if(i+sumofdigit(i)==number)
        {
            res.push_back(i);
        }
    }
    return;
}


int main()
{
    ll n,i;
    cin>>n;
    solve(n);
    cout<<res.size()<<endl;
    for(i=0;i<res.size();i++)
    {
        printf("%lld\n",res[i]);
    }
}
