#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int test,t,i,j,n,b;
    scanf("%lld",&test);
    vector<double>ten(1e6+1);
    ten[0]=0;
    for(i=1;i<=1000000;i++)
    {
        ten[i]=ten[i-1]+log10(i);
        //cout<<"i="<<i<<" ="<<ten[i]<<endl;
    }

    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&n,&b);
        long long int x=ten[n]/(log10(b));
        printf("Case %lld: %lld\n",t,x+1);
    }

}
