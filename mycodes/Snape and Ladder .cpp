#include<bits/stdc++.h>
using namespace std;

int main()
{
    int t;
    scanf("%d",&t);
    while(t--)
    {
        double b,l,res1,res2;
        scanf("%lf %lf",&b,&l);
        if(l<b) swap(b,l);

        res2=sqrt((b*b)+(l*l));
        res1=sqrt((l*l)-(b*b));

        printf("%.6lf %.6lf\n",res1,res2);
    }
}
