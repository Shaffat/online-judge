#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define mod 1000000007

ll find_common(string &s)
{
    ll i=0,j=s.size()-1,ans=0;
    while(i<j)
    {
        if(s[i]!=s[j]) break;
        else
            ans++;
        i++;
        j--;
    }
    return ans;
}

vector<ll>power(5e5+1,1);
void preprocess()
{
    ll i;
    for(i=1;i<=5e5;i++)
    {
        power[i]=(power[i-1]*26)%mod;
    }
    return;
}

ll solve(string &s,ll n)
{
    if(n<s.size()) return 0;
    ll common=find_common(s),len=s.size(),i,j,cur,ans=0;
    //cout<<"common="<<common<<endl;
    vector<int>done(1e6,0);
    if(common>0)
    {
        i=2;
        cur=i*len-(i-1)*common;
        //cout<<"optimum="<<cur<<endl;
        while(cur<=n)
        {
            //cout<<"optimum len="<<cur<<endl;
            if(!done[cur]){
            ll tmp=(n-cur)*power[n-cur];
            ans+=tmp;
            ans%=mod;
            done[cur]=1;
            }
            i++;
            cur=i*len-(i-1)*common;
        }

        cur=len;

        for(i=cur;i<=n;i+=len)
        {
            if(!done[i]){
            ans+=(n-i+1)*power[n-i];
            done[i]=1;
            }
            ans%=mod;
        }
    }
    else
    {
        cur=len;

        for(i=cur;i<=n;i+=len)
        {
            //cout<<"brute len="<<i<<endl;
            if(!done[cur]){
            ans+=(n-i+1)*power[n-i];
            ans%=mod;
            done[i]=1;
            }
        }
    }
    return ans;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,i,j,res;
    cin>>test;
    string s;
    preprocess();
    for(t=1;t<=test;t++)
    {
        ll n,m;
        cin>>n>>m;
        cout<<"Case "<<t<<":"<<endl;
        for(i=1;i<=m;i++)
        {
            cin>>s;
            res=solve(s,n);
            cout<<res<<endl;
        }
    }
}
