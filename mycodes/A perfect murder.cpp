#include<bits/stdc++.h>
using namespace std;


int solve_again(int node,int parent,int kill,vector<vector<int> >&connection,vector<vector<int> >&memory,vector<int>&vis)
{
    //cout<<"calling node="<<node<<" kill="<<kill<<" parent="<<parent<<endl;
    vis[node]=1;
    if(memory[node][kill]!=-1)
    {
        return memory[node][kill];
    }
    int killed=0,i,j,nxt;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        //cout<<"nxt="<<nxt<<endl;
        if(nxt==parent)
        {
            continue;
        }
        else if(!kill)
        {
            //cout<<"calling1"<<endl;
            killed+=max(solve_again(nxt,node,1,connection,memory,vis)+1,solve_again(nxt,node,0,connection,memory,vis));
        }
        else
        {
            //cout<<"calling 2"<<endl;
            killed+=solve_again(nxt,node,0,connection,memory,vis);
        }

    }
    //cout<<"memory["<<node<<"]["<<kill<<"]="<<killed<<endl;
    return memory[node][kill]=killed;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t,n,m,i,j,a,b;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&m);
        vector<int>col(2,-1);
        vector<vector<int> >memory(n+1,col);
        vector<vector<int> >connection(n+1);
        vector<int>vis(n+1,0);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&a,&b);
            connection[a].push_back(b);
            connection[b].push_back(a);
        }
        int res=0;
        for(i=1;i<=n;i++)
        {
            if(!vis[i])
            {
                res+=max(solve_again(i,i,0,connection,memory,vis),solve_again(i,i,1,connection,memory,vis)+1);
            }
        }


        printf("Case %d: %d\n",t,res);
    }
}
