#include<bits/stdc++.h>
using namespace std;

int solve(int start,int goal,int n,vector<vector<int> >&connections)
{
    int i,j;
    vector<int>vis(n+1,0);
    vector<int>dis(n+1,0);
    queue<int>q;
    q.push(start);
    vis[start]=1;
    while(!q.empty())
    {
        int cur=q.front();
        if(cur==goal)
        {
            return dis[goal]*100;
        }
        q.pop();
        for(i=0;i<connections[cur].size();i++)
        {
            int nxt=connections[cur][i];
            if(!vis[nxt])
            {
                vis[nxt]=1;
                dis[nxt]=dis[cur]+1;
                q.push(nxt);
            }
        }
    }
    return -1;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    printf("SHIPPING ROUTES OUTPUT\n\n");
    while(t<=test)
    {
        map<string,int>mp;
        int i,j,m,n,p;
        string u,v;
        scanf("%d %d %d",&m,&n,&p);
        vector<vector<int> >connection(m+1);
        for(i=0;i<m;i++)
        {
            cin>>u;
            mp[u]=i;
        }
        for(i=1;i<=n;i++)
        {
            cin>>u>>v;
            connection[mp[u]].push_back(mp[v]);
            connection[mp[v]].push_back(mp[u]);
        }
        printf("DATA SET  %d\n\n",t);
        for(i=1;i<=p;i++)
        {
            int k;
            scanf("%d",&k);
            cin>>u>>v;
            int res=solve(mp[u],mp[v],m,connection);
            if(res>=0)
            {
                printf("$%d\n",res*k);
            }
            else
            {
                printf("NO SHIPMENT POSSIBLE\n");
            }
        }
        t++;
        printf("\n");
    }
    printf("END OF OUTPUT\n");
}
