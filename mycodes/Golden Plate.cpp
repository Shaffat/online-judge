#include<bits/stdc++.h>
using namespace std;


int calculate(int w,int h)
{
    if(w==1 && h==1) return 1;
    return (2*w)+(2*(h-2));
}

int main()
{
    int w,h,n,i,j,cur_w,cur_h,total=0;
    cin>>w>>h>>n;
    for(i=1;i<=n;i++)
    {
        cur_w=w-4*(i-1);
        cur_h=h-4*(i-1);
        total+=calculate(cur_w,cur_h);
    }
    cout<<total<<endl;
}
