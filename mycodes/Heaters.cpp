#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,n,r,one=0,possible=1;
    cin>>n>>r;
    vector<int>lamp(n+1);
    for(i=1;i<=n;i++)
    {
        cin>>j;
        lamp[i]=j;
        if(j)
        {
            one=1;
        }
    }
    if(!one)
    {
        cout<<"-1"<<endl;
        return 0;
    }
    if(r>=n)
    {
        if(one)
        {
            cout<<"-1"<<endl;
            return 0;
        }
    }
    int cur=r,done=0,target=0,pre;
    int counter=0;
    int chk=1;
    int right=0;
    while(cur<=n)
    {
        if(lamp[cur]==1)
        {
            target=1;
            counter++;
        }
        else
        {
            target=0;
            int have=0;
            for(i=cur-1;i>cur-r;i--)
            {
                if(lamp[i]==1)
                {
                    have=1;
                }
            }
            if(have)
            {
                counter++;
            }
            else
            possible=0;
            have=0;
            if(cur+1>n) have=1;
            for(i=cur+1;i<=min(cur+r-1,n);i++)
            {
                if(lamp[i]==1)
                {
                    right=i;
                    have=1;
                }

            }
            if(have)
            {
                counter++;
            }
            else
            possible=0;
        }
        pre=cur;
        done=cur+r-1;
        cur+=2*(r-1)+1;
    }
    if(right+r-1<n)
    {
        int have=0;
        for(i=n-r+1;i<=n;i++)
        {
            if(lamp[i]==1)
                {
                    right=i;
                    have=1;
                }
        }
        if(have)
        {
            counter++;
        }
        else
        possible=0;
    }
    if(possible)
    {
        cout<<counter<<endl;
    }
    else
        cout<<"-1"<<endl;
}
