#include<bits/stdc++.h>
using namespace std;

#define ll long long int

struct matrix{

    ll r,c;
};


bool operator <(matrix a, matrix b){

    if(a.r!=b.r)
        return a.r<b.r;
    return false;
}


ll leftmost(vector<matrix>&v,ll c){
    ll first,last,mid,res=-1,f=0;

    first=0;
    last=v.size()-1;
    while(first<=last){
            if(f) break;
            if(first==last) f++;
            mid=(first+last)/2;
            //cout<<"first="<<first<<" last="<<last<<" mid="<<mid<<" row ="<<v[mid].r<<" target="<<c<<endl;
            if(v[mid].r==c){
                res=mid;
                last=mid-1;
            }
            else if(v[mid].r<c){
                first=mid+1;
            }
            else if(v[mid].r>c){
                last=mid-1;
            }

    }
return res;
}


ll rightmost(vector<matrix>&v,ll c){
    ll first,last,mid,res=-1,f=0;

    first=0;
    last=v.size()-1;
    while(first<=last){
            if(f) break;
            if(first==last) f++;
            mid=(first+last)/2;
            if(v[mid].r==c){
                res=mid;
                first=mid+1;
            }
            else if(v[mid].r<c){
                first=mid+1;
            }
            else if(v[mid].r>c){
                last=mid-1;
            }

    }

    return res;
}


int main()
{
    ll test,t,n,i,j,r,c,l,m;
    scanf("%lld",&test);
    for(t=1;t<=test;t++){
        scanf("%lld",&n);
        vector<matrix>v;
        for(i=1;i<=n;i++){
            matrix tmp;
            scanf("%lld %lld",&tmp.r,&tmp.c);
            v.push_back(tmp);
        }
        ll total=0;
        sort(v.begin(),v.end());
        for(i=0;i<v.size();i++){
            l=leftmost(v,v[i].c);
            m=rightmost(v,v[i].c);
            if(l!=-1){
                total+=m-l+1;
            }
        }
        printf("%lld\n",total);
    }
}
