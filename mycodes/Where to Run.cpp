#include<bits/stdc++.h>

using namespace std;
vector<int>col(1<<15,0);
vector<vector<int> >testcase(15,col);
vector<double>col1(1<<15);
vector<vector<double> >memory(15,col1);

int n,t;

int stateCount(int pos,int bitmask,vector<vector<int> >&connection)
{
    int i,j,total=0;
    for(i=0;i<connection[pos].size();i++)
    {
        j=connection[pos][i];
        if((bitmask&(1<<j))==0)
        {
            total++;
        }
    }
    return total;
}

double solve(int bitmask,int pos,vector<vector<int> >&connection,vector<vector<int> >&cost)
{
    int i,j,state,nxt;
    cout<<"pos="<<pos<<" bitmask="<<bitmask<<endl;
    state=stateCount(pos,bitmask,connection);
    if(state==0)
    {
        return 0;
    }
    cout<<"state="
    double res=5;
    if(testcase[pos][bitmask]==t)
    {
        return memory[pos][bitmask];
    }
    for(i=0;i<connection[pos].size();i++)
    {
       nxt=connection[pos][i];
       if((bitmask&(1<<nxt))==0)
       {
           res+=solve(bitmask|(1<<nxt),nxt,connection,cost)+cost[pos][i];
       }
    }
    cout<<"res="<<res<<endl;
    testcase[pos][bitmask]=t;
    return memory[pos][bitmask]=res/(state+1);
}

int main()
{
    int m,w,i,j,test,u,v;
    double res;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&m);
        vector<int>col;
        vector<vector<int> >connection(n,col);
        vector<vector<int> >cost(n,col);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            connection[v].push_back(u);
            cost[u].push_back(w);
            cost[v].push_back(w);
        }
        res=solve(1,0,connection,cost);
        printf("Case %d: %lf\n",t,res);
    }


}

