#include<bits/stdc++.h>
using namespace std;

int solve(int node,int parent,vector<vector<int> >&connection,vector<vector<int> >&cost,vector<vector<int> >&rev,vector<int>&vis)
{
    int i,j,child,res=0;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(child==parent) continue;
        if (!vis[child])
        {
            //cout<<"going from "<<node<<" to "<<child<<" rev="<<rev[node][i]<<endl;
            vis[child]=1;
            if(rev[node][i])
                res+=cost[node][i];
            res+=solve(child,node,connection,cost,rev,vis);
        }
        else if(vis[child]==1)
        {
            //cout<<"going1 from "<<node<<" to "<<child<<" rev="<<rev[node][i]<<endl;
            vis[node]=2;
            if(rev[node][i])
            {
                return cost[node][i];
            }
            else
                return 0;
        }
    }
    vis[node]=2;
    return res;
}


int main()
{
    int n,m,i,j,u,v,w,test,t,total;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        total=0;
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        vector<vector<int> >rev(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            rev[u].push_back(0);
            connection[v].push_back(u);
            rev[v].push_back(1);
            cost[u].push_back(w);
            cost[v].push_back(w);
            total+=w;
        }
        vector<int>vis(n+1,0);
        vis[1]=1;
        //cout<<"total="<<total<<endl;
        int res=solve(1,1,connection,cost,rev,vis);
        //cout<<"res="<<res<<endl;
        res=min(res,total-res);
        printf("Case %d: %d\n",t,res);
    }

}
