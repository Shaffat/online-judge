#include<bits/stdc++.h>

using namespace std;

vector<long long int>memory(33,-1);

long long int solve(long long int number)
{
    if(number==0)
    {
        return 0;
    }
    if(number==1)
    {
        return 1;
    }
    int bits=floor(log2(number));

    if(number==((1<<(bits+1))-1))
       {
           if(memory[bits]!=-1)
            {
                return memory[bits];
            }
            return memory[bits]=2*solve((1<<bits)-1)+(number-(1<<bits))+1;
       }
       return solve((1<<bits)-1)+solve(number-(1<<bits))+(number-(1<<bits))+1;

}

int main()
{
    long long int x,y,t=1;
    while(scanf("%lld %lld",&x,&y))
    {
        if(x==0 && y==0)
        {
            return 0;
        }
        if(x>0)
        {
            x--;
        }

        long long int res=solve(y)-solve(x);
        printf("Case %lld: %lld\n",t,res);
        t++;
    }

}
