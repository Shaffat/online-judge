#include<bits/stdc++.h>

using namespace std;

int main()
{
    long long int n,k,i,j,time=0,mini=3e9,chk=1;
    scanf("%lld %lld",&n,&k);
    vector<int>shares(n+1);
    vector<int>reduced(n+1);
    for(i=1;i<=n;i++)
    {
        long long int l;
        scanf("%lld",&l);
        shares[i]=l;
        mini=min(mini,l);
    }

    for(i=1;i<=n;i++)
    {
        reduced[i]=shares[i]-mini;
        if(reduced[i]%k!=0)
        {
            chk=0;
            break;
        }
        time+=reduced[i]/k;
    }
    if(chk)
    {

            printf("%lld\n",time);
    }
    else
        printf("-1\n");


}
