#include<bits/stdc++.h>

using namespace std;

bool primes[100001];
void sieve()
{
    int i,j;
    for(i=4;i<=100000;i+=2)
    {
        primes[i]=1;
    }
    primes[i]=1;
    for(i=3;i*i<=100000;i+=2)
    {
       if(primes[i]==0)
        {

             for(j=i*i;j<=100000;j+=i)
             {
                primes[j]=1;
             }
       }
    }
}


int main()
{
    sieve();
    int i,j,n,k,counter=0;
    scanf("%d %d",&n,&k);
    vector<int>result;

    while(result.size()<k-1 && primes[n]==1)
    {

        for(i=2;i<=sqrt(n);i++)
        {

            if(n%i==0)
            {
                result.push_back(i);
                n=n/i;
                break;
            }
        }

    }
    result.push_back(n);
    if(result.size()<k)
    {
        printf("-1\n");
    }
    else
    {
        for(i=0;i<result.size();i++)
        {
            printf("%d ",result[i]);
        }
    }


}
