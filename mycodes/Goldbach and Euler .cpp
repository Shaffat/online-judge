#include<bits/stdc++.h>
using namespace std;

vector<bool>ar(1000001,0);
vector<int>primes;

void sieve()
{
    int i,j;
    ar[0]=1;
    ar[1]=0;
    for(i=4;i<=1000000;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1000000);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<=1000000;j+=i)
            {
                ar[j]=1;
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<=1000000;i+=2)
    {
        if(ar[i]==0)
        {
               // cout<<i<<" ";
            primes.push_back(i);
        }
    }
}

void segemented_sieve()
{
    int i,j,k=1,low=1e6,high=1e6+1e6;
    for(k=1;k<100;k++)
    {
        //cout<<low<<" "<<high<<endl;
        vector<bool>segment(1000100,0);
        for(i=0;i<primes.size();i++)
        {
//            cout<<"i="<<i<<endl;
//            cout<<"p="<<primes[i]<<endl;
            if(primes[i]>sqrt(high))
            {
                break;
            }
            int lowlimit=low/(primes[i]);
            lowlimit*=primes[i];
            if(lowlimit<low)
            {
                lowlimit+=primes[i];
            }
            //cout<<"lowlimit="<<lowlimit<<endl;
            for(j=lowlimit;j<=high;j+=primes[i])
            {
                if(j-low>1e6)
                {
                    break;
                }
                //cout<<j-low<<endl;
                segment[j-low]=1;
            }
        }
        //cout<<"here"<<endl;
        for(i=1;i<=1e6;i+=2)
        {
            if(segment[i]==0)
            {
                primes.push_back(i+low);
            }
        }
        low+=1e6;
        high+=1e6;
    }
}

int main()
{
    sieve();
    segemented_sieve();
    int n,i;
    while(scanf("%d",&n)!=EOF)
    {

        vector<int>::iterator low;
        int chk=0;
         int first,last;

        if(n%2==0)
        {
            low=lower_bound(primes.begin(),primes.end(),n/2);
            int pos=low-primes.begin();
            if(primes[pos]==(n/2))
            {
                pos--;
            }
            for(i=pos;i>=0;i--)
            {
                int another=n-primes[i];
                if(binary_search(primes.begin(),primes.end(),another))
                {
                    chk=1;
                    first=primes[i];
                    last=another;
                    break;
                }
            }
        }
        else
        {
            if(binary_search(primes.begin(),primes.end(),n-2))
            {
                chk=1;
                last=n-2;
                first=2;
            }
        }

        if(chk)
        {
            if(first>last)
            {
                swap(first,last);
            }
            printf("%d is the sum of %d and %d.\n",n,first,last);
        }
        else
        {
            printf("%d is not the sum of two primes!\n",n);
        }
    }
}
