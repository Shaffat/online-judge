
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())
#define mod 10000007
vl primes;
vl memory(1000001,-1);
void sieve()
{
    //cout<<"sieve"<<endl;
    ll i,j;
    vl ar(1000001,0 );
    ar[1]=1;
    for(i=4; i<=1e6; i+=2)
    {
        ar[i]=1;
    }
    //cout<<"here"<<endl;
    for(i=3; i<=sqrt(1e6); i+=2)
    {
        if(ar[i]==0)
        {

            for(j=i*i; j<=1e6; j+=i)
            {
                ar[j]=1;
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<=1e6;i+=2){
        if(ar[i]==0)
        primes.push_back(i);
    }
    //cout<<"done"<<endl;
    return;
}

ll solve(ll n)
{
    ll i,j,res=1;
    for(i=0;i<primes.size();i++)
    {
        if(primes[i]<=n)
        {
            //cout<<"p="<<primes[i]<<endl;
            ll counter=1,tmp=primes[i];
            while(n>=tmp)
            {
                counter+=n/tmp;
                tmp*=primes[i];
            }
            //cout<<"counter="<<counter<<endl;
            ll res1=(counter*(counter+1))/2;
            res1%=mod;
            res*=res1;
            res%=mod;
        }
    }
    return res;
}

int main()
{
    ll test,t,n;
    sieve();

    while(scanf("%lld",&n))
    {
        if(n==0)
            return 0;
        printf("%lld\n",solve(n));
    }
}
