#include<bits/stdc++.h>
using namespace std;

struct plane
{
    unsigned long long int index;
    unsigned long long int cost;
};

bool operator <(plane a, plane b)
{
    if(a.cost!=b.cost)
    {
        return a.cost<b.cost;
    }
    else
        return a.index>a.index;
}
int main()
{
     int n,k,i,j;
     scanf("%d %d",&n,&k);
     vector<plane>depart;
     vector<unsigned long long int>result(n+1);
     for(i=1;i<=n;i++)
     {
         plane tmp;
         int l;
         scanf("%d",&l);
         tmp.index=i;
         tmp.cost=l;
         depart.push_back(tmp);
     }
     priority_queue<plane>schedule;
     for(i=0;i<k;i++)
     {
         plane tmp;
         tmp.cost=depart[i].cost;
         tmp.index=depart[i].index;
         schedule.push(tmp);
     }
//     priority_queue<plane> v;
//     v=schedule;
//     while(!v.empty())
//     {
//         cout<<v.top().index<<" "<<v.top().cost<<endl;
//         v.pop();
//     }
 unsigned long long int total=0;
 unsigned long long int time=k+1;
     for(i=k;i<n;i++)
     {
         schedule.push(depart[i]);
         unsigned long long int indx=schedule.top().index;
         result[indx]=time;
         total+=(time-indx)*depart[indx-1].cost;
         time++;
         schedule.pop();
     }
     while(!schedule.empty())
     {
         unsigned long long int indx=schedule.top().index;
         result[indx]=time;
         total+=(time-indx)*depart[indx-1].cost;
         time++;
         schedule.pop();
     }
     cout<<total<<endl;
     for(i=1;i<=n;i++)
     {
         cout<<result[i]<<" ";
     }

}
