#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>primes;
vector<bool>ar(100000,0);

ll GCD(ll a,ll b)
{
    while(a%b!=0)
    {
        ll temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

void sieve()
{
    ll i,j;
    ar[1]=1;
    for(i=4;i<1e5;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e5);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e5;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e5;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}

ll solve(ll n)
{
    ll i,j,counter=0,greaterthan1=0,gcd=-1;
    for(i=0;i<primes.size();i++){
        if(n%primes[i]==0){
            counter=0;
            while(n%primes[i]==0){
                n/=primes[i];
                counter++;
            }
            if(counter>1){
                greaterthan1=1;
            }
            if(gcd==-1){
                gcd=counter;
            }else{
                gcd=GCD(gcd,counter);
            }

        }
    }
    if(n>1){
       gcd=1;
    }
    return gcd;
}

ll findGreatestOdd(ll n){

    if(n%2==1) return n;
    ll i,j,mx=0;
    for(i=1;i<=sqrt(n);i++)
    {
        if(n%i==0){
            if(i%2==1){
                mx=max(mx,i);
            }
            if((n/i)%2==1){
                mx=max(mx,(n/i));
            }
        }
    }
    return mx;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    sieve();
    ll i,j,n;
    while(scanf("%lld",&n)){
        if(n==0) break;
        ll res=solve(n);
        if(n<0){
            res=findGreatestOdd(res);
        }
        printf("%lld\n",res);
    }
}

