#include<bits/stdc++.h>
using namespace std;

struct node
{
    int r,c;
};

bool fillup(vector<vector<char> >&woods,node cur,int a,int b,int c,int d)
{
    cout<<"fill node="<<cur.r<<" "<<cur.c<<endl;
    bool A=0,B=0,C=0,D=0;
    char match;
    node pre;
    pre.r=cur.r-1;
    pre.c=cur.c;
    if(pre.r>=0 && pre.r<50 && pre.c>=0 && pre.r<50)
    {
        if(woods[pre.r][pre.c]=='A')
        {
            match=woods[pre.r][pre.c];
            A=1;
        }
        else if(woods[pre.r][pre.c]=='B')
        {
            match=woods[pre.r][pre.c];
            B=1;
        }
        else if(woods[pre.r][pre.c]=='C')
        {
            match=woods[pre.r][pre.c];
            C=1;
        }
        else if(woods[pre.r][pre.c]=='D')
        {
            match=woods[pre.r][pre.c];
            D=1;
        }

    }
    pre.r=cur.r+1;
    pre.c=cur.c;
    if(pre.r>=0 && pre.r<50 && pre.c>=0 && pre.r<50)
    {
        if(woods[pre.r][pre.c]=='A')
        {
            match=woods[pre.r][pre.c];
            A=1;
        }
        else if(woods[pre.r][pre.c]=='B')
        {
            match=woods[pre.r][pre.c];
            B=1;
        }
        else if(woods[pre.r][pre.c]=='C')
        {
            match=woods[pre.r][pre.c];
            C=1;
        }
        else if(woods[pre.r][pre.c]=='D')
        {
            match=woods[pre.r][pre.c];
            D=1;
        }

    }
    pre.r=cur.r;
    pre.c=cur.c+1;
    if(pre.r>=0 && pre.r<50 && pre.c>=0 && pre.r<50)
    {
        if(woods[pre.r][pre.c]=='A')
        {
            match=woods[pre.r][pre.c];
            A=1;
        }
        else if(woods[pre.r][pre.c]=='B')
        {
            match=woods[pre.r][pre.c];
            B=1;
        }
        else if(woods[pre.r][pre.c]=='C')
        {
            match=woods[pre.r][pre.c];
            C=1;
        }
        else if(woods[pre.r][pre.c]=='D')
        {
            match=woods[pre.r][pre.c];
            D=1;
        }

    }
    pre.r=cur.r;
    pre.c=cur.c-1;
    if(pre.r>=0 && pre.r<50 && pre.c>=0 && pre.r<50)
    {
        if(woods[pre.r][pre.c]=='A')
        {
            match=woods[pre.r][pre.c];
            A=1;
        }
        else if(woods[pre.r][pre.c]=='B')
        {
            match=woods[pre.r][pre.c];
            B=1;
        }
        else if(woods[pre.r][pre.c]=='C')
        {
            match=woods[pre.r][pre.c];
            C=1;
        }
        else if(woods[pre.r][pre.c]=='D')
        {
            match=woods[pre.r][pre.c];
            D=1;
        }

    }
    if(b>0 && B==0)
    {
        cout<<"filled B"<<endl;
        woods[cur.c][cur.r]='B';
        return true;
    }
    else if(c>0 && C==0)
    {
        cout<<"filled C"<<endl;
        woods[cur.c][cur.r]='C';
        return true;
    }
    else if(a>0 && A==0)
    {
        cout<<"filled A"<<endl;
        woods[cur.c][cur.r]='A';
        return true;
    }
    else if(d>0 && D==0)
    {
        cout<<"filled D"<<endl;
        woods[cur.c][cur.r]='D';
        return true;
    }
    else
    {
        cout<<"filled "<<match<<endl;
        woods[cur.r][cur.c]=match;
        return false;
    }

}


void bfs(vector<vector<char> >&woods,int a,int b,int c,int d)
{
    vector<bool>col(50,0);
    vector<vector<bool> >vis(50,col);
    node cur;
    cur.r=0;
    cur.c=0;
    vis[0][0]=1;
    woods[0][0]='A';
    a--;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        cout<<"cur="<<cur.r<<" "<<cur.c<<endl;
        q.pop();
        node nxt;
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c-1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c+1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        ///
        nxt.r=cur.r;
        nxt.c=cur.c;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        ///


        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c-1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c+1;
        if(nxt.r>=0 && nxt.r<50 && nxt.c>=0 && nxt.c<50)
        {
            if(!vis[nxt.r][nxt.c])
            {
                if(fillup(woods,nxt,a,b,c,d))
                {
                    if(woods[nxt.r][nxt.c]=='A')
                    {
                        a--;
                    }
                    else if(woods[nxt.r][nxt.c]=='B')
                    {
                        b--;
                    }
                    else if(woods[nxt.r][nxt.c]=='C')
                    {
                        c--;
                    }
                    else
                        d--;
                }
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
            }
        }
    }
    return;
}


int main()
{
    int a,b,c,d,i,j;
    cin>>a>>b>>c>>d;
    vector<char>col1(50,0);
    vector<vector<char> >woods(50,col1);
    bfs(woods,a,b,c,d);
    cout<<"50 50\n";
    for(i=0;i<50;i++)
    {
        for(j=0;j<50;j++)
        {
            cout<<woods[i][j];
        }
        cout<<endl;
    }
}
