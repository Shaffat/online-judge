#include<bits/stdc++.h>
using namespace std;
vector<int>primes;


void sieve()
{
    int i,j;
    vector<bool>ar(100001,0);
    for(i=4;i<=100000;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(100000);i+=2)
    {
        for(j=i*i;j<=100000;j+=i)
        {
            ar[j]=1;
        }
    }
    primes.push_back(2);
    for(i=3;i<=100000;i++)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}
bool isvalid(int n)
{
    int i,j,nxt;
    vector<int>p;
    for(i=0;i<primes.size();i++)
    {
        if(n%primes[i]==0)
        {
            p.push_back(primes[i]);
            while(n%primes[i]==0)
            {
                n/=primes[i];
            }

        }
    }
    if(n!=1)
    {
        p.push_back(n);
    }
    if(p.size()!=2)
    {
        return false;
    }
    return true;
}

int resultgenerator(int n)
{
    if(n<=6)
    {
        return 6;
    }
    int i=n;
    while(!isvalid(i))
    {
        i++;
    }
    return i;
}

int main()
{
    sieve();
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n;
        scanf("%d",&n);
        int res;

        res=resultgenerator(n);
        printf("%d\n",res);
        t++;
    }
}
