#include<bits/stdc++.h>
using namespace std;
#define ll long long int
char s[100];
vector<ll>col(100,-1);
vector<vector<ll> >memory(100,col);
vector<vector<ll> >testcase(100,col);
ll t;
ll solve(ll i,ll j)
{
    //cout<<"i="<<i<<" j="<<j<<endl;
    if(i>j)
    {
        return 0;
    }
    if(testcase[i][j]==t)
    {
        //cout<<"memorize"<<endl;
        return memory[i][j];
    }

    ll total=0;
    if(s[i]==s[j])
    {
        total=solve(i+1,j)+solve(i,j-1)+1;
    }
    else
        total=solve(i+1,j)+solve(i,j-1)-solve(i+1,j-1);
    testcase[i][j]=t;
    return memory[i][j]=total;
}


int main()
{
    //ios_base::sync_with_stdio(0);
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll n,i,j,res,test;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%s",&s);
        //printf("s is %s\n",s);
//        vector<ll>col(60,-1);
//        vector<vector<ll> >memeory(60,col);
        res=solve(0,strlen(s)-1);
        printf("Case %lld: %lld\n",t,res);
        //cout<<"Case "<<t<<": "<<res<<"\n";
    }
}
