#include<bits/stdc++.h>
using namespace std;

#define ll long long int
ll solve(ll index,ll need,vector<ll>&coins,vector<vector<ll> >&memory)
{
    if(need<0)
    {
        return 0;
    }
    if(index>=coins.size())
    {
        if(need==0)
            return 1;
        return 0;
    }
    if(memory[index][need]!=-1)
    {
        return memory[index][need];
    }
    ll res1,res2;
    res1=solve(index,need-coins[index],coins,memory);
    res2=solve(index+1,need,coins,memory);
    return memory[index][need]=res1+res2;
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    vector<ll>coins;
    coins.push_back(5);
    coins.push_back(10);
    coins.push_back(20);
    coins.push_back(50);
    coins.push_back(100);
    coins.push_back(200);
    coins.push_back(500);
    coins.push_back(1000);
    coins.push_back(2000);
    coins.push_back(5000);
    coins.push_back(10000);
    double n;

    vector<ll>col(30001,-1);
    vector<vector<ll> >memory(11,col);
    while(scanf("%lf",&n))
    {
        ll total=n*100+0.5,res;
        if(total==0)
        {
            return 0;
        }
        //cout<<"total="<<total<<endl;
        res=solve(0,total,coins,memory);
        printf("%6.2lf%17lld\n",n,res);

    }
}
