#include<bits/stdc++.h>
using namespace std;

int memory[21][5];
int price[21][5];
int n;
int solve(int next,int bitmask)
{
    //cout<<"next "<<next<<" bitmask="<<bitmask<<endl;
    if(next>n)
    {
        return 0;
    }
    if(memory[next][bitmask]!=-1)
    {
        //cout<<"activated"<<endl;
        return memory[next][bitmask];
    }
    int cost1=2e9,i=0;
    for(i=0;i<3;i++)
    {
        int k=1<<i;
        //cout<<(k&bitmask)<<endl;
        if(((bitmask&k)==0))
        {
               cost1=min(cost1,solve(next+1,1<<i)+price[next][i]);
        }
    }
   return memory[next][bitmask]=cost1;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j;
        scanf("%d",&n);
        for(i=1;i<=n;i++)
        {
            for(j=0;j<=2;j++)
            {
                scanf("%d",&price[i][j]);
            }
        }
        memset(memory,-1,sizeof(memory));
        int res=solve(1,0);
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
