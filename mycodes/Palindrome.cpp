
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

#define mod 1000000007
ll solve(ll pos,string &s,vl &memory)
{
    if(pos>=s.size())
        return 1;
    if(memory[pos]!=-1)
        return memory[pos];
    ll i,j,miror,res=0;
    miror=s.size()-pos-1;
    if(s[pos]!='?' && s[miror]!='?'){
        if(s[pos]==s[miror])
            return solve(pos+1,s,memory);
        else
            return 0;
    }
    else if(s[pos]!='?')
    {
        return solve(pos+1,s,memory);
    }
    else
    {
        for(i='a';i<='z';i++)
        {
            if(s[miror]==s[pos] || s[miror]=='?')
            {
                res+=solve(pos+1,s,memory);
                res%=mod;
            }
        }
    }
    return memory[pos]=res;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,n,m,i,j,u,v;
    cin>>test;
    while(test--)
    {
        cin>>n>>m;
        ll ok=1,res=0;
        string s;
        cin>>s;
        for(i=1;i<=m;i++)
        {
            cin>>u>>v;
            u--;
            v--;
            if(s[u]!='?')
            {
                if(s[v]!='?')
                {
                    if(s[u]!=s[v])
                        ok=0;
                }
                else
                    s[v]=s[u];
            }
            else if(s[v]!='?')
            {
                if(s[u]!='?')
                {
                    if(s[u]!=s[v])
                        ok=0;
                }
                else
                    s[u]=s[v];
            }
        }
        cout<<"ok="<<ok<<" s="<<s<<endl;
        if(ok)
        {
            vector<ll>memory(s.size()+1,-1);
            res=solve(0,s,memory);
        }
        else
            res=0;
        cout<<res<<endl;
    }
}
