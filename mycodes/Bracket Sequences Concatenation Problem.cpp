#include<bits/stdc++.h>
using namespace std;

int need(string &s)
{
    int counter=0,i;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='(')
        {
            counter++;
        }
        else
            counter--;
    }
    return counter;
}
bool valid_last(string &s)
{
    int counter=0,i;
    for(i=s.size()-1;i>=0;i--)
    {
        if(s[i]=='(')
        {
            counter++;
        }
        else
            counter--;
        if(counter>0)
        {
            return false;
        }
    }
    return true;
}

bool valid_first(string s)
{
    int i,counter=0;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='(')
        {
            counter++;
        }
        else
            counter--;
        if(counter<0)
        {
            return false;
        }
    }
    return true;
}



int main()
{
    ios_base::sync_with_stdio(0);
    long long int n,i,j,mx=-1;
    string s;
    vector<long long int>first(400000,0);
    vector<long long int>last(400000,0);
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        long long int val=need(s);
        if(val>=0)
        {
            if(val==0)
            {

                if(valid_first(s) && valid_last(s))
                {
                    first[val]++;
                }
            }
            else if(valid_first(s))
            {
                first[val]++;
            }
            mx=max(mx,val);
        }
        else
        {
            if(valid_last(s))
            {
                last[-val]++;
            }
        }
    }
    long long int res=0;
    res+=first[0]*first[0];
    for(i=1;i<=mx;i++)
    {
        //cout<<"i="<<i<<"first="<<first[i]<<" last="<<last[i]<<endl;
        res+=first[i]*last[i];
    }
    cout<<res<<endl;
}
