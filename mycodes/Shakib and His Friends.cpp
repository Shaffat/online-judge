
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


ll bfs(ll root,vll &connection,vl &dis, vl &vis, vl &level,vl &levelcounter,ll counter)
{
    ll i,j,cur,l=0,nxt;
    queue<ll>q;
    q.push(root);
    vis[root]=1;
    dis[root]=0;
    levelcounter[0]=counter;
    level[0]=1;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            nxt=connection[cur][i];
            if(!vis[nxt])
            {
                vis[nxt]=1;
                dis[nxt]=dis[cur]+1;
                ll tmp= dis[nxt];
                if(levelcounter[tmp]!=counter)
                {
                    levelcounter[tmp]=counter;
                    level[tmp]=0;
                }
                level[tmp]++;
                l=max(l,tmp);
                q.push(nxt);
            }
        }
    }
    return l;
}

ll solve(ll pos,ll n,vl &level,vl &memory)
{
    if(pos>n)
        return 0;
    if(memory[pos]!=-1)
        return memory[pos];
    ll res1,res2,res;
    res1=solve(pos+1,n,level,memory);
    res2=solve(pos+2,n,level,memory)+level[pos];
    res=max(res1,res2);
    return memory[pos]=res;
}

int main()
{
    ll test,t,i,j,u,v,l,m,n,res;
    sc1(test);
    for(t=1;t<=test;t++)
    {
        res=0;
        sc2(n,m);
        vl vis(n+1,0);
        vl dis(n+1,0);
        vl level(n+1,0);
        vl levelcounter(n+1,-1);
        vll connection(n+1);
        for(i=1;i<=m;i++)
        {
            sc2(u,v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        for(i=1;i<=n;i++)
        {
            if(vis[i]==0)
            {
                l=bfs(i,connection,dis,vis,level,levelcounter,i);
                vl memory(l+10,-1);
                res+=solve(0,l,level,memory);
            }
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
