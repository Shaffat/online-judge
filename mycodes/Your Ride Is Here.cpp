/*
ID: msdipu11
PROG: ride
LANG: C++14
*/
#include<bits/stdc++.h>
using namespace std;
int val(string &s)
{
    int i,j=1;
    for(i=0;i<s.size();i++)
    {
        j*=(s[i]-'A'+1);
        j%=47;
    }
    return j;
}

int main()
{
    freopen("ride.in","r",stdin);
    freopen("ride.out","w",stdout);
    string s1,s2;
    cin>>s1>>s2;
    int v1,v2;
    v1=val(s1);
    v2=val(s2);

    if(v1==v2)
    {
        cout<<"GO\n";
    }
    else
        cout<<"STAY\n";
}
