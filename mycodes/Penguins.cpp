#include<bits/stdc++.h>
using namespace std;

struct node
{
    int id,flow;
};

struct iceburg
{
    int id,n,m,x,y;
};
double findDistance(iceburg a, iceburg b)
{
    double res=((a.x-b.x)*(a.x-b.x))+((a.y-b.y)*(a.y-b.y));
    res=sqrt(res);
    return res;
}

void build_graph(int N,double D,vector<iceburg>&ice,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    int i,j,u,v,w;
    for(i=0;i<ice.size();i++)
    {
        u=ice[i].id;
        v=ice[i].id+N+1;
        w=ice[i].m;
        graph[u].push_back(v);
        graph[v].push_back(u);
        capacity[u][v]=w;
        capacity[v][u]=0;
    }
    for(i=0;i<ice.size();i++)
    {
        for(j=i+1;j<ice.size();j++)
        {
            if(findDistance(ice[i],ice[j])<=D)
            {
                u=ice[i].id;
                v=ice[j].id;
                //cout<<u<<" is connected to "<<v<<endl;
                graph[u+N+1].push_back(v);
                graph[v].push_back(u+N+1);
                capacity[u+N+1][v]=1e9;
                capacity[v][u+N+1]=0;

                graph[u].push_back(v+N+1);
                graph[v+N+1].push_back(u);
                capacity[u][v+N+1]=0;
                capacity[v+N+1][u]=1e9;

            }
        }
    }
}

void AddSourceSink(int N,int sink,vector<iceburg>&ice,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    int i,j,u,v;
    for(i=1;i<=N;i++)
    {
        if(i==sink) continue;
        graph[0].push_back(i);
        capacity[0][i]=ice[i-1].n;
    }
    return;
}

int send_flow(int source,int sink,int n,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    //cout<<"---------trying to send flow-------"<<endl;
    int i,j,res=0;
    node cur;
    vector<int>path(n+1,-1);
    cur.id=source;
    cur.flow=1e9;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        //cout<<"cur is "<<cur.id<<" flow is "<<cur.flow<<endl;
        q.pop();
        if(cur.id==sink)
        {
            res=cur.flow;
            break;
        }
        for(i=0;i<graph[cur.id].size();i++)
        {
            int nxt=graph[cur.id][i];
            if(path[nxt]==-1 && capacity[cur.id][nxt]>0)
            {
                path[nxt]=cur.id;
                node tmp;
                tmp.id=nxt;
                tmp.flow=min(cur.flow,capacity[cur.id][nxt]);
                //cout<<"flow sent from "<<cur.id<<" to "<<nxt<<" is "<<tmp.flow<<endl;
                q.push(tmp);
            }
        }

    }
    if(res>0)
    {
        int child,parent;
        child=sink;
        while(child!=source)
        {
            parent=path[child];
            capacity[parent][child]-=res;
            capacity[child][parent]+=res;
            child=parent;
        }
    }
    return res;
}

void Copy_Cap(vector<vector<int> >&v1,vector<vector<int> >&v2)
{
    int i,j;
    for(i=0;i<250;i++)
    {
        for(j=0;j<250;j++)
        {
            v2[i][j]=v1[i][j];
        }
    }
    return;
}

int main()
{
    //freopen("in.txt","r",stdin);
    int i,j,test,t,N,n,m,counter,total=0;
    double D;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        printf("Case %d:",t);
        counter=0;
        total=0;
        scanf("%d %lf",&N,&D);
        vector<iceburg>v;
        vector<int>col(250,0);
        vector<vector<int> >capacity(250,col);
        vector<vector<int> >Duplicate(250,col);
        vector<vector<int> >graph(250);
        for(i=1;i<=N;i++)
        {
            iceburg tmp;
            tmp.id=i;
            scanf("%d %d %d %d",&tmp.x,&tmp.y,&tmp.n,&tmp.m);
            total+=tmp.n;
            v.push_back(tmp);
        }
        build_graph(N,D,v,graph,capacity);
        for(i=0;i<N;i++)
        {
            Copy_Cap(capacity,Duplicate);
            AddSourceSink(N,v[i].id,v,graph,Duplicate);
            int f=0,mxflow=0;
            //cout<<"sink is "<<v[i].id<<endl;;
            while(f=send_flow(0,v[i].id,250,graph,Duplicate))
            {
                mxflow+=f;
            }
            if(mxflow+v[i].n==total)
            {
                printf(" %d",i);
                counter++;
            }
        }
        if(counter) printf("\n");
        else
            printf(" -1\n");
    }

}
