
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

bool valid(string &input,string &output,vector<ll>&banned)
{
    ll i,j;
    if(input.size()<output.size()) return false;
    //cout<<"here  sz="<<output.size()<<endl;
    for(i=0; i<output.size(); i++)
    {
        //cout<<"i1="<<i<<" char is "<<input[i]<<endl;
        ll c=input[i];
        if(input[i]!=output[i])
        {
            return false;

        }
        else
        {
            if(banned[c]==-1)
            {
                banned[c]=0;
            }
            else if(banned[c]==1)
            {
                //cout<<"Not valid1"<<endl;
                return false;
            }
        }
    }
    //cout<<"i="<<i<<" inputsz="<<input.size()<<endl;
    if(i<input.size())
    {
        //cout<<"banning charcter at "<<i<<endl;
        if(banned[input[i]]==0) {
                //cout<<"this is not valid"<<endl;
                return false;
        }
        banned[input[i]]=1;
    }
    return true;
}



string process(string &s)
{
    string res="";
    ll i,j;
    FOR(i,1,s.size()-2)
    {
        res.push_back(s[i]);
    }
    return res;
}

int main()
{
//   freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll n,i,j,t=1,ok;
    while(cin>>n)
    {
        ok=1;
        if(n==0) return 0;
        string s1,s2;
        vector<ll>banned(1000,-1);
        set<char>st;
        string res="";
        FOR(i,1,n)
        {
            cin>>s1>>s2;
            s1=process(s1);
            s2=process(s2);
            //cout<<"s1="<<s1<<" s2="<<s2<<endl;
            if(!valid(s1,s2,banned))
            {
                //cout<<"this is not valid"<<endl;
                ok=0;
            }

        }
        if(!ok)
            cout<<"Case "<<t<<": I_AM_UNDONE"<<endl;
        else
        {
            int done=0,mxNum=-1,mxSmall=-1,mxbig=-1;
            for(i='0'; i<='9'; i++)
            {
                if(banned[i]==0)
                {
                    mxNum=i;
                }
            }
            for(i='a'; i<='z'; i++)
            {
                if(banned[i]==0)
                {
                    mxSmall=i;
                }
            }
            for(i='A'; i<='Z'; i++)
            {
                if(banned[i]==0)
                {
                    mxbig=i;
                }
            }
            if(mxNum!=-1)
            {
                for(i='0'; i<=mxNum; i++)
                {
                    if(banned[i]==0 || banned[i]==-1)
                    {
                        banned[i]=1;
                        res.push_back(i);
                    }
                }
            }
            if (mxbig!=-1 || mxSmall!=-1)
            {
                for(i='0'; i<='9'; i++)
                {
                    if(banned[i]==-1|| banned[i]==-1)
                    {
                        res.push_back(i);
                    }
                }
            }
            if(mxbig!=-1)
            {
                for(i='A'; i<=mxbig; i++)
                {
                    if(banned[i]==0|| banned[i]==-1)
                    {
                        banned[i]=1;
                        res.push_back(i);
                    }
                }
            }
            if (mxSmall!=-1)
            {
                for(i='A'; i<='Z'; i++)
                {
                    if(banned[i]==-1)
                    {
                        res.push_back(i);
                    }
                }
            }
            if(mxSmall!=-1)
            {
                for(i='a'; i<=mxSmall; i++)
                {
                    if(banned[i]==0 || banned[i]==-1)
                    {
                        res.push_back(i);
                    }
                }
            }
            //cout<<"mxNum "<<mxNum<<" mxbig "<<mxbig<<" mxsmall "<<mxSmall<<endl;
            if(res.size()==0)
            {
                for(i='0'; i<='9'; i++)
                {
                    if(done) break;
                    if(banned[i]==-1)
                    {
                        res.push_back(i);
                        done=1;
                        break;
                    }
                }
                for(i='A'; i<='Z'; i++)
                {
                    if(done) break;
                    if(banned[i]==-1)
                    {
                        res.push_back(i);
                        done=1;
                        break;
                    }
                }
                for(i='a'; i<='z'; i++)
                {
                    if(done) break;
                    if(banned[i]==-1)
                    {
                        res.push_back(i);
                        done=1;
                        break;
                    }
                }
            }
            if(res.size()>0) done=1;
            if(done)
                cout<<"Case "<<t<<": ["<<res<<"]"<<endl;
            else
                cout<<"Case "<<t<<": I_AM_UNDONE"<<endl;
        }
        //cout<<"done test "<<t<<endl;
        t++;
    }
}
