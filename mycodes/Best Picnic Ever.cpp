#include<bits/stdc++.h>
using namespace std;

void solve(int node,int n,vector<vector<int> >&connection,vector<int>&counter)
{
    int i,j;
    vector<int>vis(n+1,0);
    queue<int>q;
    vis[node]=1;
    counter[node]++;
    q.push(node);
    while(!q.empty())
    {
        int cur=q.front(),child;

        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            child=connection[cur][i];

            if(!vis[child])
            {

                vis[child]=1;
                q.push(child);
                counter[child]++;
            }
        }
    }

    return;
}

int main()
{
    int test,t,n,m,k,i,j,u,v;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d",&k,&n,&m);
        vector<int>done(n+1,0);
        vector<int>counter(n+1,0);
        vector<int>people;
        vector<vector<int> >connection(n+1);
        int city=0;
        for(i=1;i<=k;i++)
        {
            scanf("%d",&u);
            people.push_back(u);
        }
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
        }
        for(i=0;i<k;i++)
        {
            int tmp;
            tmp=people[i];
            if(done[tmp]==0)
            {
                city++;
                done[tmp]=1;
                solve(tmp,n,connection,counter);
            }
        }

        int res=0;
        for(i=1;i<=n;i++)
        {
            if(counter[i]==city)
            {
                res++;
            }
        }
        printf("Case %d: %d\n",t,res);
    }
}
