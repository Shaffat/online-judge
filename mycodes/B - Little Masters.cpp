#include <cstdio>
#include <cmath>

int main() {

    int test, x, y, r;
    double distance;
    scanf("%d", &test);
    for (int i = 0; i < test; ++i) {
        scanf("%d %d %d", &x, &y, &r);
        distance = sqrt((x * x) + (y * y));
        printf("%0.2lf %0.2lf\n", r - distance, r + distance);
    }

    return (0);
}
