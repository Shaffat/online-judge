#include<bits/stdc++.h>

using namespace std;

int is_vowel(char c)
{
    if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u') return 1;
    return 0;
}


int main()
{
    string s,t;
    int i,j,k=1;
    cin>>s>>t;
    if(s.size()!=t.size())
        k=0;
    else
    {
        for(i=0;i<s.size();i++)
        {
            if(is_vowel(s[i])!=is_vowel(t[i]))
            {
                k=0;
            }
        }
    }
    if(k)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}
