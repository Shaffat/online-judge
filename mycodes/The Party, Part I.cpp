#include<bits/stdc++.h>

using namespace std;
int dis[1001];
int vis[1001];
int bfs(int source, vector< vector<int> >&connection)
{
    int i,j;
    queue<int>q;
    q.push(source);
    while(!q.empty())
    {
        int current=q.front();
        q.pop();
        vis[current]=1;
        //cout<<"current="<<current<<endl;
        for(i=0;i<connection[current].size();i++)
        {
            int next=connection[current][i];
            if(vis[next]==0)
            {
                vis[next]=1;
                dis[next]=dis[current]+1;
                q.push(next);
            }
        }
    }
}

int main()
{
    int test,t=0;
    scanf("%d",&test);
    while(test--)
    {
        if(t)
        {
            printf("\n");
        }
        int i,j,p,d,u,v;
        scanf("%d %d",&p,&d);
        vector<int>vi;
        vector<vector<int> >connection(p+1,vi);
        for(i=1;i<=d;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        memset(dis,0,sizeof(dis));
        memset(vis,0,sizeof(vis));
        bfs(0,connection);
        for(i=1;i<=p-1;i++)
        {
            printf("%d\n",dis[i]);
        }
        t++;
    }

}
