
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/

#include<bits/stdc++.h>
using namespace std;

bool ok(string &s1,string &s2,string &s3)
{
    int i,j;
    vector<int>res(100,0);
    for(i=0;i<s1.size();i++)
    {
        j=s1[i];
        res[j]++;
    }
    for(i=0;i<s2.size();i++)
    {
        j=s2[i];
        res[j]++;
    }
    for(i=0;i<s3.size();i++)
    {
        j=s3[i];
        res[j]--;
    }

    for(i=0;i<100;i++)
    {
        if(res[i]!=0)
            return false;
    }
    return true;
}

int main()
{
   string s1,s2,s3;
   cin>>s1>>s2>>s3;
   if(ok(s1,s2,s3))
   {
       cout<<"YES"<<endl;
   }
   else
    cout<<"NO"<<endl;
   return 0;
}
