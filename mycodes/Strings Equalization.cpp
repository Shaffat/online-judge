#include<bits/stdc++.h>
using namespace std;

bool duplicate(string &s)
{
    int i;
    vector<int>counter(140,0);
    for(i=0;i<s.size();i++)
    {
        counter[s[i]]++;
        if(counter[s[i]]>1)
            return true;
    }

    return false;
}

bool noDiff(string &s,string &t)
{
    int i;
    vector<int>counter(140,0);
    for(i=0;i<s.size();i++)
    {
        counter[s[i]]++;
    }
    for(i=0;i<t.size();i++)
    {
        if(counter[t[i]]==0)
            return false;
    }
    return true;
}


int main()
{
    int n,i,j,test;
    string s,t;
    cin>>test;
    while(test--)
    {
        cin>>s>>t;
        if(noDiff(s,t) && duplicate(s) && s.size()==t.size())
        {
            cout<<"YES"<<endl;
        }
        else
            cout<<"NO"<<endl;
    }
}
