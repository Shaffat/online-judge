#include<bits/stdc++.h>
using namespace std;

struct direction
{
    int lake,time_remains;
};

vector<int>v(10000,-1);
vector<vector<int> >testcase(30,v);
vector<vector<int> >memory(30,v);
vector<direction>v1(10000);
vector<vector<direction> >path(30,v1);
int test,t,n;
int solve(int lake,int available_time,vector<int>&speed,vector<int>&decrease,vector<int>&time)
{
//    cout<<"lake="<<lake<<" available="<<available_time<<endl;
    if(lake>n || available_time<=0)
    {
        return 0;
    }
    if(testcase[lake][available_time]==t)
    {
        return memory[lake][available_time];
    }
    int i,j,res=0,cur,fish=0;
    direction dir;
    for(i=0;i<=(available_time/5);i++)
    {
        int next_available=available_time-i*5-time[lake]*5;
        int add;
        if(i==0)
        {
            add=0;
        }
        else
        {
            add=max(speed[lake]-((i-1)*decrease[lake]),0);
        }
        fish+=add;
        //cout<<"lake="<<lake<<" available="<<available_time<<" next="<<next_available<<" fish="<<fish<<endl;
        cur=solve(lake+1,next_available,speed,decrease,time)+fish;
        //cout<<"res of lake="<<lake<<" available="<<available_time<<"="<<cur<<endl;
        if(cur>=res)
        {
            res=cur;
            dir.lake=lake+1;
            dir.time_remains=next_available;
            path[lake][available_time]=dir;
        }
    }
    testcase[lake][available_time]=t;
    return memory[lake][available_time]=res;
}

void path_print(int lake,int available,vector<int>&dis,vector<int>&time_status)
{
    while(lake<=n && available>=0)
    {
        direction next=path[lake][available];
        time_status[lake]=available-next.time_remains-dis[lake]*5;
        lake=next.lake;
        available=next.time_remains;
    }
    return;
}

int main()
{
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int h,i,j;
        scanf("%d %d",&n,&h);
        vector<int>speed(n+1);
        vector<int>decrease(n+1);
        vector<int>time(n+1,0);
        vector<int>lake(n+1,0);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            speed[i]=j;
        }
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            decrease[i]=j;
        }
        for(i=1;i<n;i++)
        {
            scanf("%d",&j);
            time[i]=j;
        }
        int res=solve(1,h*60,speed,decrease,time);
        path_print(1,h*60,time,lake);
        printf("Case %d:\n",t);
        for(i=1;i<n;i++)
        {
            printf("%d, ",lake[i]);
        }
        printf("%d\n",lake[i]);
        printf("Number of fish expected: %d\n",res);
    }
}
