#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

bool valid(pos p,int n)
{
    if(p.x<n && p.y<n && p.x>=0 && p.y>=0)
        return true;
    return false;
}

int solve(pos p,int target,int n,vector<string>&grid)
{
    //cout<<"pos "<<p.x<<" "<<p.y<<" target "<<target<<endl;
    pos cur,child;
    vector<int>col(12,0);
    vector<vector<int> >vis(12,col);
    vector<vector<int> >dis(12,col);

    vis[p.x][p.y]=1;
    dis[p.x][p.y]=0;

    int i,j;
    queue<pos>q;

    q.push(p);
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        //cout<<"cur ="<<cur.x<<" "<<cur.y<<" dis="<<dis[cur.x][cur.y]<<endl;
        child.x=cur.x+1;
        child.y=cur.y;

        if(valid(child,n))
        {
            if(grid[child.x][child.y]==target)
                return dis[cur.x][cur.y]+1;
            if((grid[child.x][child.y]=='.' || grid[child.x][child.y]<target)&& !vis[child.x][child.y] && grid[child.x][child.y]!='#' )
            {
                vis[child.x][child.y]=1;
                dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                q.push(child);
            }
        }

        child.x=cur.x-1;
        child.y=cur.y;

        if(valid(child,n))
        {
            if(grid[child.x][child.y]==target)
                return dis[cur.x][cur.y]+1;
            if((grid[child.x][child.y]=='.' || grid[child.x][child.y]<target) && !vis[child.x][child.y] && grid[child.x][child.y]!='#' )
            {
                vis[child.x][child.y]=1;
                dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                q.push(child);
            }
        }
        child.x=cur.x;
        child.y=cur.y+1;

        if(valid(child,n))
        {
            if(grid[child.x][child.y]==target)
                return dis[cur.x][cur.y]+1;
            if((grid[child.x][child.y]=='.'|| grid[child.x][child.y]<target) && !vis[child.x][child.y] && grid[child.x][child.y]!='#')
            {
                vis[child.x][child.y]=1;
                dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                q.push(child);
            }
        }
        child.x=cur.x;
        child.y=cur.y-1;

        if(valid(child,n))
        {
            if(grid[child.x][child.y]==target)
                return dis[cur.x][cur.y]+1;
            if((grid[child.x][child.y]=='.'|| grid[child.x][child.y]<target) && !vis[child.x][child.y] && grid[child.x][child.y]!='#')
            {
                vis[child.x][child.y]=1;
                dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                q.push(child);
            }
        }
    }

    return -1;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j,n,k,res;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        k=-1;
        res=0;
        string s;
        cin>>n;
        vector<string>grid(n+1);
        vector<pos>start(150);
        for(i=0;i<n;i++)
        {
            cin>>s;
            grid[i]=s;
            for(j=0;j<n;j++)
            {
                if(grid[i][j]!='.'&& grid[i][j]!='#')
                {
                    k++;
                    //cout<<"here "<<grid[i][j]<<endl;
                    start[grid[i][j]].x=i;
                    start[grid[i][j]].y=j;
                }
            }
        }
        //cout<<"k="<<k<<endl;
        for(i='A';i<'A'+k;i++)
        {
            pos tmp;
            int f;
            tmp.x=start[i].x;
            tmp.y=start[i].y;
            f=solve(tmp,i+1,n,grid);
            //cout<<"f="<<f<<" i="<<char(i+1)<<endl;
            if(f==-1)
            {
                res=-1;
                break;
            }
            else{
                res+=f;
            }
        }
        if(res!=-1)
            cout<<"Case "<<t<<": "<<res<<endl;
        else
            cout<<"Case "<<t<<": Impossible"<<endl;
    }
}
