#include<bits/stdc++.h>

using namespace std;



double calculate(double p,double q,double r,double s,double t,double u,double x)
{
    double ans=p*(exp(-x));
    ans+=q*(asin(x));
    ans+=r*acos(x);
    ans+=s*atan(x);
    ans+=t*x*x;
    ans+=u;
    //cout<<"ans="<<ans<<" x="<<x<<endl;
    return ans;
}


double solve(double p,double q,double r,double s,double t,double u)
{
    double res=-1,add=0.00001,start=1,counter=1e5,diff=2e9,cur;
    while(counter--)
    {
        cur=abs(calculate(p,q,r,s,t,u,start));
        if(cur<1e-4){
            cout<<"cur="<<cur<<" diff="<<diff<<endl;
            if(diff>cur){
            res=start;
            diff=cur;
            }
        }
        start-=add;
    }

    return res;
}


int main()
{
    double p,q,r,s,t,u;

    while(scanf("%lf %lf %lf %lf %lf %lf",&p,&q,&r,&s,&t,&u)==6)
    {
        cout<<calculate(p,q,r,s,t,u,0.7554)<<" correct ans"<<endl;
        double ans=-1,x=1;
        ans=solve(p,q,r,s,t,u);
        if(ans==-1)
            printf("No solution\n");
        else
            printf("%.6lf\n",ans);
    }
}
