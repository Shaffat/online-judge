#include<bits/stdc++.h>
using namespace std;

void preprocess(int n,int m,vector<vector<int> >&matrix,vector<vector<int> >&sum)
{
    int i,j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++){
            sum[i+1][j+1]=sum[i][j+1]+sum[i+1][j]-sum[i][j]+matrix[i][j];
            //cout<<"sum at ["<<i+1<<"]["<<j+1<<"]="<<sum[i+1][j+1]<<" ";
        }
        //cout<<endl;
    }
    return;
}


int solve(int n,int f,vector<vector<int> >&sum)
{

    int i,j,k,m,mx=0;
    for(i=1;i<=n;i++){
        for(j=1;j<=f;j++)
        {
            for(k=i;k<=n;k++)
            {
                for(m=j;m<=f;m++)
                {
                    //cout<<"from "<<i<<" "<<j<<" to "<<k<<" "<<m<<" res=";
                    int rect1,rect2,total;
                    total=(k-i+1)*(m-j+1);
                    rect1=sum[k][m]-sum[i-1][m]-sum[k][j-1]+sum[i-1][j-1];
                    //cout<<rect1<<endl;
                    if(total==rect1)
                        mx=max(total,mx);
                }
            }
        }
    }
    return mx;
}

int main()
{
    int n,m,i,j,k;
    while(scanf("%d %d",&n,&m))
    {
        if(n==0 && m==0) break;
        vector<int>col(110,0);
        vector<vector<int> >matrix(110,col);
        vector<vector<int> >sum(110,col);
        int i,j;
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                scanf("%d",&k);
                matrix[i][j]=(k+1)%2;
            }
        }
        preprocess(n,m,matrix,sum);
        int res=solve(n,m,sum);
        printf("%d\n",res);
    }
}
