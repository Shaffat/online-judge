#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct selection
{
    ll a,b,dis;
};

bool operator <(selection a,selection b)
{
    if(a.dis!=b.dis)
    {
        return a.dis<b.dis;
    }
}


int main()
{
    ll i,j,n,m,k,pre,total=0;
    scanf("%lld %lld",&n,&k);
    priority_queue<selection>pq;
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        if(i==1)
        {
            pre=j;
        }
        else
        {
            selection tmp;
            tmp.a=pre;
            tmp.b=j;
            tmp.dis=j-pre;
            pq.push(tmp);
            total+=tmp.dis;
            pre=j;
        }
    }
    //cout<<"total="<<total<<endl;
    k--;
    while(k--)
    {

        total-=pq.top().dis;
        //cout<<"removed "<<pq.top().a<<" "<<pq.top().b<<" "<<pq.top().dis<<endl;
        pq.pop();
    }
    printf("%lld\n",total);
}
