#include<bits/stdc++.h>
typedef unsigned long long int llu;
using namespace std;

string x,y;
int lcs(int i,int j,vector<vector<int> >&memory)
{
    if(i<0||j<0)
    {
        return 0;
    }
    if(memory[i][j]!=-1)
    {
        return memory[i][j];
    }
    if(x[i]==y[j])
    {
        return memory[i][j]=lcs(i-1,j-1,memory)+1;
    }
    else
    {
        return memory[i][j]=max(lcs(i-1,j,memory),lcs(i,j-1,memory));
    }
}

llu solve(int superlen,int xi,int yi,vector< vector <vector <llu> > >&memory)
{
    if(superlen==0)
    {
        if(xi==x.size() && yi==y.size())
        {
            return 1;
        }
        return 0;
    }
    if(memory[superlen][xi][yi]!=-1)
    {
        return memory[superlen][xi][yi];
    }
    llu count=0;
    if(x[xi]==y[yi])
    {
        count+=solve(superlen-1,xi+1,yi+1,memory);
    }
    else
    {
        if(xi<x.size() && yi<y.size())
        {
            count+=solve(superlen-1,xi+1,yi,memory);
            count+=solve(superlen-1,xi,yi+1,memory);
        }
        else
        {
            if(xi<x.size())
            {
                count+=solve(superlen-1,xi+1,yi,memory);
            }
            else
            {
                count+=solve(superlen-1,xi,yi+1,memory);
            }
        }
    }
    return memory[superlen][xi][yi]=count;
}

int main()
{
   int test,t=1;
   scanf("%d",&test);
   getchar();
   while(t<=test)
   {
       cin>>x>>y;
       vector<int>lyi(51,-1);
       vector<vector<int> >lxi(51,lyi);
       int lcslen=lcs(x.size()-1, y.size()-1,lxi);
       vector<llu>yi(51,-1);
       vector<vector<llu> >xi(51,yi);
       vector<vector<vector<llu> > >memory(70,xi);

       llu k=solve(x.size()+y.size()-lcslen,0,0,memory);
       printf("Case %d: %d %llu\n",t,x.size()+y.size()-lcslen,k);
       t++;
   }
}

