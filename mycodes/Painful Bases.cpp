#include<bits/stdc++.h>
using namespace std;

#define ull unsigned long long int
map<char,ull>mp;

ull base,n,k;
string s;
ull solve(ull bitmask,ull reminder,vector<vector<ull> >&memory)
{
    //cout<<"bitmask="<<bitmask<<" rem="<<reminder<<" n="<<n<<" k="<<k<<endl;
    if(bitmask==((1<<n)-1))
    {
        if(reminder==0)
        {
            //cout<<"returned"<<endl;
            return 1;
        }
        else
            return 0;
    }
    if(memory[bitmask][reminder]!=-1)
    {
        return memory[bitmask][reminder];
    }
    ull i,j,total=0,cur_reminder;
    for(i=0;i<n;i++)
    {
        //cout<<i<<"th pos i taken?="<<(bitmask&(1<<i))<<endl;
        if((bitmask&(1<<i))==0)
           {

               cur_reminder=reminder*base;
               cur_reminder+=mp[s[i]];
               cur_reminder%=k;
               total+=solve((bitmask|(1<<i)),cur_reminder,memory);
           }
    }
    return memory[bitmask][reminder]=total;
}

int main()
{
    mp['0']=0;
    mp['1']=1;
    mp['2']=2;
    mp['3']=3;
    mp['4']=4;
    mp['5']=5;
    mp['6']=6;
    mp['7']=7;
    mp['8']=8;
    mp['9']=9;
    mp['A']=10;
    mp['B']=11;
    mp['C']=12;
    mp['D']=13;
    mp['E']=14;
    mp['F']=15;
    ull test,t,i,j,res;
    scanf("%llu",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%llu %llu",&base,&k);
        cin>>s;
        n=s.size();
        vector<ull>col(k,-1);
        vector<vector<ull> >memory(1<<n,col);
        res=solve(0,0,memory);
        printf("Case %llu: %llu\n",t,res);
    }
}
