#include<bits/stdc++.h>
using namespace std;

#define ll long long int

struct area
{
    ll price,total;
};
void preprocess(ll n,ll m,vector<vector<ll> >&matrix,vector<vector<ll> >&sum)
{
    ll i,j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++){
            sum[i+1][j+1]=sum[i][j+1]+sum[i+1][j]-sum[i][j]+matrix[i][j];
            //cout<<"sum at ["<<i+1<<"]["<<j+1<<"]="<<sum[i+1][j+1]<<" ";
        }
        //cout<<endl;
    }
    return;
}


area solve(ll n,ll f,ll mn,vector<vector<ll> >&sum)
{

    ll i,j,k,m,mx=0;
    area res;
    res.price=0;
    res.total=0;
    for(i=1;i<=n;i++){
        for(j=1;j<=f;j++)
        {
            for(k=i;k<=n;k++)
            {
                for(m=j;m<=f;m++)
                {
                    //cout<<"from "<<i<<" "<<j<<" to "<<k<<" "<<m<<" res=";
                    ll rect1,rect2,total;
                    total=(k-i+1)*(m-j+1);
                    rect1=sum[k][m]-sum[i-1][m]-sum[k][j-1]+sum[i-1][j-1];
                    //cout<<rect1<<endl;
                    if(rect1<=mn){
                        if(total>mx){
                            mx=total;
                            res.total=mx;
                            res.price=rect1;
                        }
                        else if(total==mx){
                            res.total=mx;
                            res.price=min(res.price,rect1);
                        }

                    }
                }
            }
        }
    }
    return res;
}

int main()
{

    ll n,m,i,j,k,t,test,p;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld %lld",&n,&m,&p);
        vector<ll>col(110,0);
        vector<vector<ll> >matrix(110,col);
        vector<vector<ll> >sum(110,col);
        ll i,j;
        for(i=0;i<n;i++)
        {
            for(j=0;j<m;j++)
            {
                scanf("%lld",&k);
                matrix[i][j]=k;
            }
        }
        //cout<<"done"<<endl;
        preprocess(n,m,matrix,sum);
        area res=solve(n,m,p,sum);
        printf("Case #%lld: %lld %lld\n",t,res.total,res.price);
    }
}
