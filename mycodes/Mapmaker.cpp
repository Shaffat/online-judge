#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,k,id=0,i,j,b,d,u,l,v;
    scanf("%d %d",&n,&k);
    map<string,int>mp;
    vector<int>all_d;
    vector<vector<int> >up,low,c;
    string s;
    for(i=1;i<=n;i++)
    {
        int e,total=0;
        cin>>s>>b>>e>>d;
        mp[s]=id;
        all_d.push_back(d);
        vector<int>dimension(d+1);
        up.push_back(dimension);
        low.push_back(dimension);
        c.push_back(dimension);
        for(j=1;j<=d;j++)
        {
            scanf("%d %d",&l,&u);
            up[id][j]=u;
            low[id][j]=l;
        }
        c[id][d]=e;
        total=e*low[id][d];
        for(j=d-1;j>=1;j--)
        {
            c[id][j]=c[id][j+1]*(up[id][j+1]-low[id][j+1]+1);
            total+=low[id][j]*c[id][j];
        }
        c[id][0]=b-total;
        id++;
    }
    for(i=1;i<=k;i++)
    {
        cin>>s;
        id=mp[s];
        int memory=c[id][0],g=0;
        cout<<s<<"[";
        for(j=1;j<=all_d[id];j++)
        {
            scanf("%d",&v);
            if(!g)
            {
                printf("%d",v);
            }
            else
            {
                printf(", %d",v);
            }
            g++;
            memory+=v*c[id][j];
        }
        printf("] = %d\n",memory);
    }
}
