#include<bits/stdc++.h>

using namespace std;

struct node
{
    int u,v,c;
};

bool operator < (node x,node y)
{
    if(x.c!=y.c)
    {
        return x.c>y.c ;
    }
    else
        return false;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,i,j,m;
        scanf("%d",&n);
        vector<int>busyness(n+1);
        for(i=1;i<=n;i++)
        {
            int k;
            scanf("%d",&k);
             busyness[i]=k;
        }
        scanf("%d",&m);
        vector<node>edges;
        for(i=1;i<=m;i++)
        {
            int u,v;

            scanf("%d %d",&u,&v);
            int c=busyness[v]-busyness[u];


            node temp;
            temp.u=u;
            temp.v=v;

            temp.c=c*c*c;
            edges.push_back(temp);

        }
        vector<int>dis(n+1,2e9);
        dis[1]=0;
        for(i=1;i<n;i++)
        {

            for(j=0;j<edges.size();j++)
            {


                int u,v,c;
                u=edges[j].u;
                v=edges[j].v;
                c=edges[j].c;
                //cout<<"for "<<u<<" "<<v<<" "<<c<<endl;
                if(dis[v]>(dis[u]+c))
                {
                    dis[v]=dis[u]+c;
                }

            }
        }

        for(i=0;i<edges.size();i++)
        {
                int u,v,c;
                u=edges[i].u;
                v=edges[i].v;
                c=edges[i].c;

                if(dis[v]>(dis[u]+c))
                {
                    dis[v]=-1;
                }
        }


        vector<int>query;
        int a;
        scanf("%d",&a);
        while(a--)
        {
            int k;
            scanf("%d",&k);
            query.push_back(k);
        }
        printf("Case %d:\n",t);
        t++;
        for(i=0;i<query.size();i++)
        {
            if(dis[query[i]]<3 || dis[query[i]]>1e9)
            {
                printf("?\n");
            }
            else
            {
                printf("%d\n",dis[query[i]]);
            }

        }


    }
}
