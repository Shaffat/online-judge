#include<bits/stdc++.h>
using namespace std;

int solve(int x,int y,int d)
{
    return (x+y)-((x/d)+(y*d));
}

int main()
{
    int i,j,n,total=0,ans;
    scanf("%d",&n);
    vector<int>v;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v.push_back(j);
        total+=j;
    }
    sort(v.begin(),v.end());
    ans=total;
    for(i=v.size()-1;i>0;i--)
    {
        for(j=1;j<=sqrt(v[i]);j++)
        {
            if(v[i]%j==0)
            {
                ans=min(ans,total-solve(v[i],v[0],j));
                ans=min(ans,total-solve(v[i],v[0],v[i]/j));
            }

        }
    }
    printf("%d\n",ans);
}
