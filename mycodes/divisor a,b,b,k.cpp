#include<bits/stdc++.h>
using namespace std;

#define REP(i,a,b)       for(int i=a;i<=b;i++)
#define RREP(i,a,b)      for(int i=a;i>=b;i--)
#define sc3(a,b,c) scanf("%d %d %d",&a,&b,&c)
#define sc2(a,b) scanf("%d %d",&a,&b);
#define sc1(a) scanf("%d",&a);


struct anything
{
    int sod;
    int nod;
};
anything result[100001];
int sum[100001];
int num[100001];
int prime[100001];
vector<int>primes;


void sieve()
{
    int i,j;
    prime[1]=1;
    for(i=4;i<=100000;i+=2) prime[i]=1;
    for(i=3;i<=sqrt(100000);i+=2)
    {
        if(prime[i]==0)
        {
            for(j=i*i;j<=100000;j+=i)
            {
                prime[j]=1;
            }
        }
    }
}


anything SODandNOD(int n)
{
    int i,j;
    int divisor=1,sum=1;
    for(i=0;i<primes.size();i++)
    {
        if(n==1)
        {
            break;
        }
        if(primes[i]>n)
        {
            break;
        }
        int count=1,p=primes[i],s=1,t=1;
        while(n%p==0)
        {
            n/=p;
            t*=p;
            s+=t;
            count++;
        }


        divisor*=count;
        sum*=s;
    }

    anything result;
    result.nod=divisor;
    result.sod=sum;
    return result;
}


void solve()
{
    int i;
    REP(i,1,100000)
    {
        if(prime[i]==0)
        {
            result[i].nod=2;
            result[i].sod=i+1;
        }
        else
        {
            anything tmp;
            tmp=SODandNOD(i);
            result[i].sod=tmp.sod;
            result[i].nod=tmp.nod;
        }
    }
    return;
}


int main()
{
    int a,b,k,i,j,t;
    sieve();
    REP(i,1,100000)
    {
        if(prime[i]==0)
        {
            primes.push_back(i);
        }
    }
    solve();
    sc1(t);
    while(t--)
    {
        sc3(a,b,k);
        int te;
        te=a/k;
        te*=k;
        if(te<a)
        {
            te+=k;
        }
        int s=0,d=0;
        for(i=te;i<=b;i+=k)
        {
            s+=result[i].sod;
            d+=result[i].nod;
        }
        printf("%d %d\n",d,s);
    }
}
