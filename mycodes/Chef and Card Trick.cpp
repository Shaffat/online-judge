#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t,n,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>v;

        int breakpoint=-1,chk=1;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            v.push_back(j);
        }
        for(i=1;i<n;i++)
        {
            if(v[i]<v[i-1])
            {
                breakpoint=i;
                break;
            }
        }
        //cout<<"breakpoint="<<breakpoint<<endl;
        if(breakpoint!=-1)
        {
            if(v[0]<v[n-1]) chk=0;
            vector<int>v1;
            vector<int>v2;
            for(i=0;i<breakpoint;i++)
            {
                v1.push_back(v[i]);
            }
            sort(v1.begin(),v1.end());
            for(i=0;i<v1.size();i++)
            {
                if(v1[i]!=v[i])
                    chk=0;
            }
            for(i=breakpoint;i<n;i++)
            {
                v2.push_back(v[i]);
            }
            sort(v2.begin(),v2.end());
            for(i=0;i<v2.size();i++)
            {
                if(v2[i]!=v[breakpoint+i])
                {
                    chk=0;
                }
            }
        }
        if(chk)
        {
            printf("YES\n");
        }
        else
            printf("NO\n");
    }
}
