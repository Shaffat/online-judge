#include<bits/stdc++.h>

using namespace std;
struct description
{
    int r,c;
};

int min_multiplication(int start,int ending,vector<description>&matrix,vector<vector<int> >&memory)
{
    //cout<<"start="<<start<<" ending="<<ending<<endl;
    if(start==ending)
    {
        return 0;
    }
    if(memory[start][ending]!=-1)
    {
        return memory[start][ending];
    }
    int moves=2e9,i,j;
    for(i=0;i<ending-start;i++)
    {
        ///all combination loop
        moves=min(min_multiplication(start,start+i,matrix,memory)+min_multiplication(start+i+1,ending,matrix,memory)+(matrix[start].r*matrix[start+i].c*matrix[ending].c),moves);
    }
    return memory[start][ending]=moves;
}

int main()
{
    int n,i,j;
    vector<description>matrix;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        int r,c;
        scanf("%d %d",&r,&c);
        description tmp;
        tmp.r=r;
        tmp.c=c;
        matrix.push_back(tmp);
    }
    vector<int>initial(n+1,-1);
    vector<vector<int> >memory(n+1,initial);
    cout<<min_multiplication(0,matrix.size()-1,matrix,memory);
}
