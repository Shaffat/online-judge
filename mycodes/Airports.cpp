#include<bits/stdc++.h>

using namespace std;

struct node
{
    int n,w;
};

bool operator < (node x,node y)
{
    if(x.w!=y.w)
    {
        return x.w > y.w;
    }
    return false;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,m,a,i,j;
        scanf("%d %d %d",&n,&m,&a);
        vector<int>edges[n+1];
        vector<int>cost[n+1];
        vector<int>vis(n+1,0);
        vector<int>mst(n+1,2e9);
        for(i=1;i<=m;i++)
        {
            int u,v,c;
            scanf("%d %d %d",&u,&v,&c);
            edges[u].push_back(v);
            edges[v].push_back(u);
            cost[u].push_back(c);
            cost[v].push_back(c);
        }
        int port=0;
        while(1)
        {
            priority_queue<node>q;

            int chk=0;
            for(i=1;i<=n;i++)
            {
                if(mst[i]==2e9)
                {
                    chk=1;
                    break;
                }
            }
            int sum=0;
            if(chk==0)
            {
                for(i=1;i<=n;i++)
                {
                    sum+=mst[i];
                }

              printf("Case %d: %d %d\n",t,sum,port);
              break;

            }
            port++;
            mst[i]=a;
            node start;
            start.n=i;
            start.w=a;
            q.push(start);
            while(!q.empty())
            {
                node cur_top=q.top();
                q.pop();
                int cur_node=cur_top.n;
                if(vis[cur_node])
                {
                    continue;
                }
                vis[cur_node]=1;
                for(i=0;i<edges[cur_node].size();i++)
                {
                    int nxt_node=edges[cur_node][i];
                    int nxt_cost=cost[cur_node][i];
                    if(nxt_cost<mst[nxt_node] && nxt_cost<a && vis[nxt_node]==0)
                    {
                        mst[nxt_node]=nxt_cost;
                        node nwnode;
                        nwnode.n=nxt_node;
                        nwnode.w=nxt_cost;
                        q.push(nwnode);
                    }
                }
            }

        }
        t++;
    }
}
