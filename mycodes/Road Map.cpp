#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,n,r1,r2;
    scanf("%d %d %d",&n,&r1,&r2);
    vector<int>v;
    vector<int>parent(n+1);
    vector<vector<int> >connection(n+1,v);
    vector<bool>vis(n+1,0);
    for(i=1;i<=n;i++)
    {
        if(i==r1)continue;
        int u,v;
        scanf("%d",&u);
        cout<<"i="<<i<<" u="<<u<<endl;
        connection[i].push_back(u);
        connection[u].push_back(i);
    }
    parent[r2]=r2;
    vis[r2]=1;
    cout<<"r2="<<r2<<endl;
    cout<<connection[r2].size()<<" s"<<endl;
    for(i=0;i<connection[r2].size();i++)
    {
        int adjacent=connection[r2][i];
        cout<<"           adjacent="<<adjacent<<endl;
        parent[adjacent]=r2;
        vis[adjacent]=1;
        queue<int>q;
        q.push(adjacent);
        while(!q.empty())
        {
            int current=q.front();
            cout<<"current="<<current<<endl;
            q.pop();
            for(j=0;j<connection[current].size();j++)
            {
                int next=connection[current][j];
                cout<<"next="<<next<<endl;
                if(!vis[next])
                {
                    cout<<"next="<<next<<endl;
                    vis[next]=1;
                    parent[next]=adjacent;
                    q.push(next);
                }
            }
        }
    }
    for(i=1;i<=n;i++)
    {
        if(i==r2)
        {
            continue;
        }
        else
            printf("%d ",parent[i]);
    }

}
