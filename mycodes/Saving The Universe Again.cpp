#include<bits/stdc++.h>
using namespace std;

int destruction(string s)
{
    int power=1,total=0,i;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='S')
        {
            total+=power;
        }
        else
            power*=2;
    }
    return total;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t=1,i,j,d;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>d>>s;
        int power=1,total;
        vector<int>pos,active;
        for(i=0;i<s.size();i++)
        {
            if(s[i]=='C')
            {
                pos.push_back(i);
            }
        }
        int moves=0;
        for(i=pos.size()-1;i>=0;i--)
        {
            int cur=pos[i];
            while(cur<s.size()-(pos.size()-i-1)-1)
            {
                //cout<<"s="<<s<<" destruction="<<destruction(s)<<" cur="<<cur<<endl;
                if(destruction(s)<=d)
                {
                    break;
                }
                else
                {
                    moves++;
                    char tmp=s[cur];
                    s[cur]=s[cur+1];
                    s[cur+1]=tmp;
                    cur++;
                }
            }
        }
        if(destruction(s)>d)
        {
            cout<<"Case #"<<t<<": IMPOSSIBLE"<<endl;
        }
        else
             cout<<"Case #"<<t<<": "<<moves<<endl;
    }

}
