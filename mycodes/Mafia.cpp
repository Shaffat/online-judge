
#include<bits/stdc++.h>

using namespace std;
long long int total;

long long int solve(int root,int parent,vector<int>&mafia,vector<vector<int> >&connection)
{
    //cout<<"root="<<root<<endl;
    int i,j,need=0;
    if(mafia[root]<1)
    {
        need+=1;
    }
    else
    {
        need-=mafia[root]-1;
    }
    for(i=0;i<connection[root].size();i++)
    {
        int nxt=connection[root][i];
        if(nxt==parent) continue;
        need+=solve(nxt,root,mafia,connection);
    }
    total+=abs(need);
    //cout<<"root="<<root<<" total="<<total<<" need="<<need<<endl;
    return need;
}

int main()
{
    int n,i,j,k,w,v,d,test,t=1,f;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        vector<int>mafia(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%d %d %d",&j,&w,&v);
            mafia[j]=w;
            f=j;
            for(k=1;k<=v;k++)
            {
                scanf("%d",&d);
                connection[j].push_back(d);
                connection[d].push_back(j);
            }
        }

        solve(f,f,mafia,connection);
        printf("Case %d: %d\n",t,total);
        total=0;
    }
}
