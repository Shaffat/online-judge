
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct num
{
    ll counter,val;
};

bool operator<(num a, num b)
{
    if(a.counter!=b.counter)
        return a.counter<b.counter;
    return a.val<b.val;
}

struct pathnode
{
    ll id,rem,taken;
};


num solve(ll pos,ll mod,ll r,ll q,string &s,vector<vector<pathnode> >&path,vector<vector<num> >&memory)
{
//    cout<<"pos="<<pos<<" mod="<<mod<<endl;
    if(pos>=s.size())
    {
        num tmp;
        if(mod==r)
        {
            tmp.counter=0;
            tmp.val=-1;
        }
        else
            tmp.counter=-1e9;
        return tmp;
    }
    if(memory[pos][mod].counter!=-1)
        return memory[pos][mod];
    ll cur=s[pos]-'0',nwmod;
    nwmod=mod*10+cur;
    nwmod%=q;
    num res1,res2,res;
    res.counter=-1e9;
    res1=solve(pos+1,mod,r,q,s,path,memory);

//    cout<<"taking pos"<<pos<<" at ["<<pos<<"]["<<mod<<"]"<<endl;
    res2=solve(pos+1,nwmod,r,q,s,path,memory);
    res2.counter+=1;
//    cout<<"res1="<<res1.counter<<" val="<<res1.val<<" res2="<<res2.counter<<" val="<<res2.val<<endl;
//    cout<<endl;
    if(res<res2)
    {
        res2.val=s[pos]-'0';
        res=res2;
        path[pos][mod].id=pos+1;
        path[pos][mod].rem=nwmod;
        path[pos][mod].taken=1;
//        cout<<"taken path["<<pos<<"]["<<mod<<"]="<<path[pos][mod].taken<<endl;
    }
    if(res<res1)
    {
        res=res1;
        path[pos][mod].id=pos+1;
        path[pos][mod].rem=mod;
        path[pos][mod].taken=0;
    }
//    cout<<"memory["<<pos<<"]["<<mod<<"] res="<<res.counter<<" val="<<res.val<<endl;
//    cout<<"path["<<pos<<"]["<<mod<<"]="<<path[pos][mod].id<<","<<path[pos][mod].rem<<","<<path[pos][mod].taken<<endl;
    return memory[pos][mod]=res;
}

void printpath(vector<vector<pathnode> >&path,string &s)
{
    pathnode cur,nxt;
    cur=path[0][0];
    //cout<<"sz="<<s.size()<<endl;

    //cout<<"\n\ncur id="<<cur.id<<" rem="<<cur.rem<<" taken="<<cur.taken<<endl;
    while(cur.id!=-1)
    {
        //cout<<"cur id="<<cur.id<<" rem="<<cur.rem<<" taken="<<cur.taken<<endl;
        if(cur.taken)
            cout<<s[cur.id-1];
        nxt.id=path[cur.id][cur.rem].id;
        nxt.rem=path[cur.id][cur.rem].rem;
        nxt.taken=path[cur.id][cur.rem].taken;
//        cout<<"nxt="<<nxt.id<<" rem="<<nxt.rem<<" taken="<<nxt.taken<<endl;
//        cout<<"for 1,1 "<<path[1][1].id<<","<<path[1][1].rem<<" taken="<<path[1][1].taken<<endl;
        cur=nxt;
        //cout<<"nxt id="<<cur.id<<" rem="<<cur.rem<<" taken="<<cur.taken<<endl;
    }
    cout<<endl;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,i,j,r,q;
    cin>>test;
    while(test--)
    {
        string n;
        cin>>n>>r>>q;
        pathnode tmp;
        num tmp1;

        tmp.id=-1;
        tmp.rem=0;
        tmp.taken=0;

        tmp1.counter=-1;
        tmp1.val=0;

        vector<num>col(q,tmp1);
        vector<pathnode>col1(q,tmp);

        vector<vector<num> >memory(n.size()+10,col);
        vector<vector<pathnode> >path(n.size()+10,col1);

        tmp1=solve(0,0,r,q,n,path,memory);
        //cout<<"res "<<tmp1.counter<<" val="<<tmp1.counter<<endl;
        if(tmp1.counter>0)
            printpath(path,n);
        else
            cout<<"Not found"<<endl;
    }
}
