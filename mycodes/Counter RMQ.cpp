#include<bits/stdc++.h>
using namespace std;

struct range
{
    int s,e,v;
};
bool operator <(range a, range b)
{
    if(a.v<b.v)
    {
        return true;
    }
    return false;
}
int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j,n,q;
        scanf("%d %d",&n,&q);
        vector<int>v(n+1,2e9);
        vector<range>queries;
        for(i=1;i<=q;i++)
        {
            range tmp;
            int s, e, v;
            scanf("%d %d %d",&tmp.s,&tmp.e,&tmp.v);
            queries.push_back(tmp);
        }
        sort(queries.begin(),queries.end());
        for(i=0;i<queries.size();i++)
        {
            int tmp=queries[i].v;
            for(j=queries[i].s;j<=queries[i].e;j++)
            {
                v[j]=tmp;
            }
        }

        printf("Case %d: ",t);
        for(i=1;i<=n;i++)
        {
            if(i==n)
            {
                printf("%d",v[i]);
            }
            else
            printf("%d ",v[i]);
        }
        printf("\n");
        t++;
    }

}
