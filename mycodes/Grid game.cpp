#include<bits/stdc++.h>
using namespace std;

void erase_grid(vector<vector<int> >&grid)
{
    int i,j,chk;
    for(i=1;i<=4;i++)
    {
        chk=1;
        for(j=1;j<=4;j++)
        {
            if(grid[i][j]==-1)
            {
                chk=0;
                break;
            }
        }
        if(chk)
        {
            for(j=1;j<=4;j++)
            {
                grid[i][j]=-1;
            }
        }

    }
    for(i=1;i<=4;i++)
    {
        chk=1;
        for(j=1;j<=4;j++)
        {
            if(grid[j][i]==-1)
            {
                chk=0;
                break;
            }
        }
        if(chk)
        {
            for(j=1;j<=4;j++)
            {
                grid[j][i]=-1;
            }
        }

    }
    return;
}

void set_tile(int type,vector<vector<int> >&grid)
{
    int r=1,c=1,i,j,chk=0;
    if(type==1)
    {
        for(i=1;i<=4;i++)
        {
            for(j=1;j<=3;j+=2)
            {
                if(grid[i][j]==-1 && grid[i][j+1]==-1)
                {
                    grid[i][j]=1;
                    grid[i][j+1]=1;
                    r=i;
                    c=j;
                    chk=1;
                    break;
                }
            }
            if(chk) break;
        }
    }
    else
    {
      for(i=1;i<=3;i+=2)
      {
          for(j=1;j<=4;j++)
          {
              if(grid[i][j]==-1 && grid[i+1][j]==-1)
              {
                  grid[i][j]=1;
                  grid[i+1][j]=1;
                  r=i;
                  c=j;
                  chk=1;
                  break;
              }
          }
          if(chk) break;
      }
    }
    cout<<r<<" "<<c<<endl;
    return;
}

int main()
{
    string s;
    int i,j;
    vector<int>col(5,-1);
    cin>>s;
    vector<vector<int> >grid(5,col);
    for(i=0;i<s.size();i++)
    {
        set_tile(s[i]-'0',grid);
        erase_grid(grid);
    }
}
