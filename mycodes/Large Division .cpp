#include<bits/stdc++.h>
using namespace std;

typedef long long int ll;
bool largedivision(string a, ll b)
{
    ll i,j,tmp=0;
    string res="";
    for(i=0;i<a.size();i++)
    {
        if(tmp>=b)
        {
            break;
        }
        tmp*=10;
        tmp+=a[i]-'0';
    }
    if(tmp<b && tmp%b!=0)
    {
        return false;
    }
    res+=tmp/b+'0';
    tmp%=b;
    for(i=i;i<a.size();i++)
    {
        tmp*=10;
        tmp+=a[i]-'0';
        if(tmp<b)
        {
            res+='0';
        }
        else
        {
            res+=tmp/b+'0';
            tmp%=b;
        }
    }
    if(tmp==0)
    {
        return true;
    }
    else
        return false;
}


int main()
{
   ll b,test,t=1;
   string a;
   scanf("%lld",&test);
   while(t<=test)
   {
       cin>>a;
       if(a[0]=='-')
       {
           a.erase(a.begin()+0);
       }
       scanf("%lld",&b);
       if(largedivision(a,b))
       {
           printf("Case %lld: divisible\n",t);
       }
       else
       {
           printf("Case %lld: not divisible\n",t);
       }
       t++;
   }
}
