#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long int llu;
llu segment_hash(int l,int r,vector<llu>&v,vector<llu>&power)
{
    return v[r]-v[l-1]*power[r-l+1];
}

bool is_palindrome(int l,int r,int n,vector<llu>&frward,vector<llu>&bckward,vector<llu>&power)
{
    if(segment_hash(l,r,frward,power)==segment_hash(n-r+1,n-l+1,bckward,power))
       {
           //cout<<"palindrome found"<<endl;
           return true;
       }
       return false;
}

int solve(string s)
{
    int i,j,res;
    vector<llu>frward(s.size()+1);
    vector<llu>bckward(s.size()+1);
    vector<llu>power(s.size()+1);
    vector<int>even,odd;
    frward[0]=0;
    bckward[0]=0;
    power[0]=1;
    for(i=0;i<s.size();i++)
    {
        power[i+1]=power[i]*29;
        frward[i+1]=frward[i]*29+s[i];
        bckward[i+1]=bckward[i]*29+s[s.size()-i-1];
    }
    for(i=1;i<=s.size();i++)
    {
        if(is_palindrome(s.size()-i+1,s.size(),s.size(),frward,bckward,power))
        {
            res=i;
        }
    }
    return res;
}
int main()
{
    int test,t=1;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s;
        cin>>s;
        cout<<"Case "<<t<<": "<<s.size()+s.size()-solve(s)<<endl;
    }
}
