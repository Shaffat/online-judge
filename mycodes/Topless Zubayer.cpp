#include<bits/stdc++.h>

using namespace std;

int n,dr,dc;
vector< vector<int> >grid;
vector< vector<int> >memory;
int wayfinder(int row,int column)
{
    if(row==n||column==n || grid[row][column]==-1)
    {
        return 2e9;
    }
    else
    {
        if(row==dr &&column==dc)
        {
            return 0;
        }
        if(memory[row][column]!=-1)
        {
            return memory[row][column];
        }
        else
        {
            memory[row][column]=min(wayfinder(row+1,column)+1,wayfinder(row,column+1)+1);
            //  `cout<<"row="<<row<<" column= "<<column<<"  = "<<memory[row][column]<<endl;
            return memory[row][column];
        }
    }
}

int main()
{
    //freopen("in.txt", "r", stdin);
    int i,j;
    cin>>n>>dr>>dc;

    vector<int>v1;
    vector<int>v;
    for(i=0;i<n;i++)
    {
        grid.push_back(v1);
        memory.push_back(v);
        for(j=0;j<n;j++)
        {
            int k;
            scanf("%d",&k);
            grid[i].push_back(k);
            memory[i].push_back(-1);
        }
    }
    cout<<wayfinder(0,0);
}
