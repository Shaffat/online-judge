#include<bits/stdc++.h>

using namespace std;


int get_diff(int n,vector<int>&v,vector<int>&done)
{
    int one=0,two=0,i,j;
    for(i=1;i<=n;i++)
    {
        if(done[i]==0)
        {
            if(v[i]==-1)
            {
                one++;
            }
            else
                two++;
        }
    }
    return abs(one-two);
}

int main()
{
    int i,j,n,k,ans=0;
    scanf("%d %d",&n,&k);
    vector<int>v(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&v[i]);
    }
    for(i=1;i<=n;i++)
    {
        vector<int>done(n+1,0);
        j=i;
        while(j>0)
        {
            done[j]=1;
            j-=k;
        }
        j=i;
        while(j<=n)
        {
            done[j]=1;
            j+=k;
        }
        ans=max(ans,get_diff(n,v,done));
    }
    printf("%d\n",ans);
}
