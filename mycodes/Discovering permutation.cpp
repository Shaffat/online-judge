#include<bits/stdc++.h>

using namespace std;

int counter;
void get_permutation(string fixed,string current)
{
    int i,j;
    if(counter==0)
    {
        return;
    }
    if(current.size()==0)
    {
        cout<<fixed<<endl;
        counter--;
        i++;
        return;
    }

    for(i=0;i<current.size();i++)
    {
        string temp="";
        fixed.push_back(current[i]);
        for(j=0;j<current.size();j++)
        {
            if(i==j)
            {
                continue;
            }
            temp.push_back(current[j]);
        }
        get_permutation(fixed,temp);
        fixed.erase(fixed.begin()+fixed.size()-1);
    }

}

int main()
{
    int i,j,test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,k;
        scanf("%d %d",&n,&k);
        string s="";
        for(i=0;i<n;i++)
        {
            char c='A'+i;
            s.push_back(c);
        }
        counter=k;
        printf("Case %d:\n",t);
        get_permutation("",s);

        t++;
    }
}
