#include<bits/stdc++.h>
using namespace std;

struct point
{
  int x,y;
};

bool is_valid(point x,int r,int c)
{
    if(x.x<=r && x.x>=1 && x.y<=c && x.y>=1)
    {
        return true;
    }
    return false;
}
bool is_ok(int r,int c,vector<vector<char> >&cells,queue<point>&wolfs,vector<vector<int> >&vis)
{
    while(!wolfs.empty())
    {
        point cur=wolfs.front();
        wolfs.pop();
        point next;
        next.x=cur.x+1;
        next.y=cur.y;
        if(is_valid(next,r,c))
        {
            if(vis[next.x][next.y]==0 && cells[next.x][next.y]!='D')
            {
                if(cells[next.x][next.y]=='S')
                {
                    return false;
                }
                vis[next.x][next.y]=1;
                wolfs.push(next);
            }
        }
        next.x=cur.x-1;
        next.y=cur.y;
        if(is_valid(next,r,c))
        {
            if(vis[next.x][next.y]==0 && cells[next.x][next.y]!='D')
            {
                if(cells[next.x][next.y]=='S')
                {
                    return false;
                }
                vis[next.x][next.y]=1;
                wolfs.push(next);
            }
        }
        next.x=cur.x;
        next.y=cur.y+1;
        if(is_valid(next,r,c))
        {
            if(vis[next.x][next.y]==0 && cells[next.x][next.y]!='D')
            {
                if(cells[next.x][next.y]=='S')
                {
                    return false;
                }
                vis[next.x][next.y]=1;
                wolfs.push(next);
            }
        }
        next.x=cur.x;
        next.y=cur.y-1;
        if(is_valid(next,r,c))
        {
            if(vis[next.x][next.y]==0 && cells[next.x][next.y]!='D')
            {
                if(cells[next.x][next.y]=='S')
                {
                    return false;
                }
                vis[next.x][next.y]=1;
                wolfs.push(next);
            }
        }
    }
    return true;
}

int main()
{
    int r,c,i,j;
    scanf("%d %d",&r,&c);
    vector<char>col(c+1);
    vector<vector<char> >cells(r+1,col);
    vector<point>wolfs_v;
    queue<point>wolfs;
    vector<int>col2(c+1,0);
    vector<vector<int> >vis(r+1,col2);
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            char ch;
            scanf(" %c",&ch);
            point tmp;
            if(ch=='W')
            {
                tmp.x=i;
                tmp.y=j;
                wolfs.push(tmp);
                wolfs_v.push_back(tmp);
                vis[i][j]=1;
            }
            cells[i][j]=ch;
        }
    }
//    for(i=1;i<=r;i++)
//    {
//        for(j=1;j<=c;j++)
//        {
//            cout<<cells[i][j]<<" ";
//        }
//        cout<<endl;
//    }
    for(i=0;i<wolfs_v.size();i++)
    {
        point cur;
        cur.x=wolfs_v[i].x;
        cur.y=wolfs_v[i].y;
        point next;
        next.x=cur.x+1;
        next.y=cur.y;
        //cout<<"cur x="<<cur.x<<" y="<<cur.y<<endl;
        if(is_valid(next,r,c))
        {
            //cout<<"next x="<<next.x<<" y="<<next.y<<" cell="<< cells[next.x][next.y]<<endl;
            if(cells[next.x][next.y]=='.')
            {
                cells[next.x][next.y]='D';
            }
        }
        next.x=cur.x-1;
        next.y=cur.y;
        if(is_valid(next,r,c))
        {
            //cout<<"next x="<<next.x<<" y="<<next.y<<endl;
            if(cells[next.x][next.y]=='.')
            {
                cells[next.x][next.y]='D';
            }
        }
        next.x=cur.x;
        next.y=cur.y+1;
        if(is_valid(next,r,c))
        {
            //cout<<"next x="<<next.x<<" y="<<next.y<<endl;
            if(cells[next.x][next.y]=='.')
            {
                cells[next.x][next.y]='D';
            }
        }
        next.x=cur.x;
        next.y=cur.y-1;
        if(is_valid(next,r,c))
        {
            //cout<<"next x="<<next.x<<" y="<<next.y<<endl;
            if(cells[next.x][next.y]=='.')
            {
                cells[next.x][next.y]='D';
            }
        }
    }

    if(is_ok(r,c,cells,wolfs,vis))
    {
        cout<<"Yes"<<endl;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                cout<<cells[i][j];
            }
            cout<<endl;
        }
    }
    else
        cout<<"No"<<endl;


}
