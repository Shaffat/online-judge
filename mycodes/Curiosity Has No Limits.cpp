#include<bits/stdc++.h>
using namespace std;


bool chk(int a,int b,int t1,int t2)
{
    if(((t1|t2)==a) &&((t1&t2)==b))
        return true;
    return false;
}

int n;
bool solve(int index,int val,vector<int>&a,vector<int>&b,vector<int>&t_v,vector<vector<int> >&memory)
{
    if(index>=n)
    {
        return true;
    }
    if(memory[index][val]!=-1)
    {
        return memory[index][val];
    }
    for(int i=0;i<=3;i++)
    {
        if(chk(a[index-1],b[index-1],val,i)){
            if(solve(index+1,i,a,b,t_v,memory))
            {
               t_v[index]=i;
               return true;
            }
        }
    }
    return memory[index][val]=0;
}

int main()
{
    int i,j;
    vector<int>a;
    vector<int>b;
    scanf("%d",&n);
    vector<int>t(n+1);
    for(i=1;i<n;i++)
    {
        scanf("%d",&j);
        a.push_back(j);
    }
    for(i=1;i<n;i++)
    {
        scanf("%d",&j);
        b.push_back(j);
    }
    vector<int>col(4,-1);
    vector<vector<int> >memory(n+5,col);
    if(solve(1,1,a,b,t,memory))
    {
        printf("YES\n");
        t[0]=1;
        for(i=0;i<n;i++)
            printf("%d ",t[i]);
    }
    else if(solve(1,2,a,b,t,memory))
    {
        printf("YES\n");
        t[0]=2;
        for(i=0;i<n;i++)
            printf("%d ",t[i]);
    }
    else if(solve(1,3,a,b,t,memory))
    {
        printf("YES\n");
        t[0]=3;
        for(i=0;i<n;i++)
            printf("%d ",t[i]);
    }
    else if(solve(1,0,a,b,t,memory))
    {
        printf("YES\n");
        t[0]=0;
        for(i=0;i<n;i++)
            printf("%d ",t[i]);
    }
    else
        printf("NO\n");
}
