#include<bits/stdc++.h>
using namespace std;

int solve(int remainng,int curindex,vector<int>negindex,vector<int>sum)
{
    if(curindex>=negindex.size() || remainng<=0)
    {
        return 0;
    }
    int sum1=solve(remainng-1,curindex+1,negindex,sum)+sum[negindex[curindex]-1]-negindex[curindex];
    int sum2=solve(remainng,curindex+1,negindex,sum);
    return max(sum1,sum2);
}


int main()
{
    int test;
    scanf("%d",&test);
    cout<<"here"<<endl;
    while(test--)
    {
        int n,k,i,j;
        scanf("%d %d",&n,&k);
        vector<int>sum(n+1);
        vector<int>negindex;
        vector<int>negvalue;
        sum[0]=0;

        for(i=1;i<=n;i++)
        {
            int k;
            scanf("%d",&k);
            sum[i]+=sum[i-1]+k;
            if(k<0)
            {
                negindex.push_back(i);
                negvalue.push_back(j);
            }
        }
        int f=negindex.size();
        int replaceable=min(k,f);
        printf("%d\n",solve(replaceable,0,negindex,sum));
    }
}

