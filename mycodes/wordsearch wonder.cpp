#include<bits/stdc++.h>

using namespace std;

struct pos_in_grid
{
    int r, c;
};

bool operator <(pos_in_grid a,pos_in_grid b)
{
    if(a.r==b.r)
    {
        return a.c <b.c;
    }
    return a.r < b.r;
}

int main()
{
    int n,l,i,j;
    scanf("%d",&n);
    getchar();
    vector< vector<char> >matrix(n+1);
    vector< vector<pos_in_grid> > pos(130);
    for(i=1; i<=n; i++)
    {
        for(j=1; j<=n; j++)
        {
            char c;
            int k;
            scanf("%c",&c);
            matrix[i].push_back(c);
            k=c;
            pos_in_grid tmp;
            tmp.r=i;
            tmp.c=j;
            pos[k].push_back(tmp);

        }
        getchar();
    }
    string s;
    vector<string> inputs;
    while(cin>>s)
    {
        if(s=="0")
        {
            break;
        }
        inputs.push_back(s);
    }
    for(i=0; i<inputs.size(); i++)
    {
        string current=inputs[i];
        pos_in_grid initial_pos;
        pos_in_grid final_pos;
        int k=current[0];

        int chk=1;

        for(j=0; j<pos[k].size(); j++)
        {


            initial_pos.r=pos[k][j].r;
            initial_pos.c=pos[k][j].c;
            if(current.size()==1)
            {
                final_pos.r=initial_pos.r;
                final_pos.c=initial_pos.c;
                chk=0;
            }
            else
            {


                pos_in_grid guess;
                int nxt_char=current[1];
                guess.r=initial_pos.r+1;
                guess.c=initial_pos.c;
                if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                {
                    chk=0;
                    pos_in_grid cur_pos;
                    cur_pos.r=guess.r+1;
                    cur_pos.c=guess.c;
                    for(int w=2; w<current.size(); w++)
                    {
                        int cur_char=current[w];
                        if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                        {
                            chk=1;
                            break;
                        }
                        if(w==current.size()-1)
                        {
                            final_pos.r=cur_pos.r;
                            final_pos.c=cur_pos.c;
                        }
                        cur_pos.r+=1;
                    }
                }
                if(chk)
                {
                    guess.r=initial_pos.r-1;
                    guess.c=initial_pos.c;
                    if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                    {
                        chk=0;
                        pos_in_grid cur_pos;
                        cur_pos.r=guess.r-1;
                        cur_pos.c=guess.c;
                        for(int w=2; w<current.size(); w++)
                        {
                            int cur_char=current[w];
                            if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                            {
                                chk=1;
                                break;
                            }
                            if(w==current.size()-1)
                            {
                                final_pos.r=cur_pos.r;
                                final_pos.c=cur_pos.c;
                            }
                            cur_pos.r-=1;

                        }
                    }
                }
                if(chk)
                {
                    guess.r=initial_pos.r;
                    guess.c=initial_pos.c+1;
                    if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                    {
                        chk=0;
                        pos_in_grid cur_pos;
                        cur_pos.r=guess.r;
                        cur_pos.c=guess.c+1;
                        for(int w=2; w<current.size(); w++)
                        {
                            int cur_char=current[w];
                            if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                            {
                                chk=1;
                                break;
                            }
                            if(w==current.size()-1)
                            {
                                final_pos.r=cur_pos.r;
                                final_pos.c=cur_pos.c;
                            }
                            cur_pos.c+=1;

                        }
                    }
                }
                if(chk)
                {
                    guess.r=initial_pos.r;
                    guess.c=initial_pos.c-1;
                    if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                    {
                        chk=0;
                        pos_in_grid cur_pos;
                        cur_pos.r=guess.r;
                        cur_pos.c=guess.c-1;
                        for(int w=2; w<current.size(); w++)
                        {
                            int cur_char=current[w];
                            if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                            {
                                chk=1;
                                break;
                            }
                            if(w==current.size()-1)
                            {
                                final_pos.r=cur_pos.r;
                                final_pos.c=cur_pos.c;
                            }
                            cur_pos.c-=1;

                        }
                    }
                }
                if(chk)
                {
                    guess.r=initial_pos.r+1;
                    guess.c=initial_pos.c+1;
                    if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                    {
                        chk=0;
                        pos_in_grid cur_pos;
                        cur_pos.r=guess.r+1;
                        cur_pos.c=guess.c+1;
                        for(int w=2; w<current.size(); w++)
                        {
                            int cur_char=current[w];
                            if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                            {
                                chk=1;
                                break;
                            }
                            if(w==current.size()-1)
                            {
                                final_pos.r=cur_pos.r;
                                final_pos.c=cur_pos.c;
                            }
                            cur_pos.r+=1;
                            cur_pos.c+=1;

                        }
                    }
                }
                if(chk)
                {
                    guess.r=initial_pos.r-1;
                    guess.c=initial_pos.c-1;
                    if(binary_search(pos[nxt_char].begin(),pos[nxt_char].end(),guess))
                    {
                        chk=0;
                        pos_in_grid cur_pos;
                        cur_pos.r=guess.r-1;
                        cur_pos.c=guess.c-1;
                        for(int w=2; w<current.size(); w++)
                        {
                            int cur_char=current[w];
                            if(!binary_search(pos[cur_char].begin(),pos[cur_char].end(),cur_pos))
                            {
                                chk=1;
                                break;
                            }
                            if(w==current.size()-1)
                            {
                                final_pos.r=cur_pos.r;
                                final_pos.c=cur_pos.c;
                            }
                            cur_pos.r-=1;
                            cur_pos.c-=1;

                        }
                    }
                }
            }
            if(!chk)
            {
                break;
            }
        }

        if(chk)
        {
            printf("Not Found\n");
        }
        else
        {
            printf("%d,%d %d,%d",initial_pos.r,initial_pos.c,final_pos.r,final_pos.c);

        }
        if(i<inputs.size()-1)
        {
            printf("\n");
        }

    }
}
