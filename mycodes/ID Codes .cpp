#include<iostream>
#include<string>
#include<algorithm>
//#include<bits/stdc++.h>
using namespace std;

bool last(string &s)
{
    int i,j;
    for(i=0;i<s.size()-1;i++)
    {
        if(s[i]<s[i+1])
            return false;
    }
    return true;
}


int revfirst(string &s)
{
    int i,j;
    for(i=s.size()-1;i>0;i--)
    {
        if(s[i-1]<s[i])
            return i-1;
    }
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ios_base::sync_with_stdio(0);
    string s;
    while(cin>>s)
    {

        if(s=="#") return 0;
        if(last(s))
            cout<<"No Successor"<<endl;
        else
        {
            int i=revfirst(s),j,d=-1;
            string nw="",tmp="";
            for(j=0;j<i;j++)
            {
                nw.push_back(s[j]);
            }
            //cout<<"i="<<i<<endl;
            for(j=s.size()-1;j>i;j--)
            {
               // cout<<"s[j]="<<s[j]<<" i="<<s[i]<<endl;
                if(s[j]>s[i])
                {
                    d=j;
                    break;
                }
            }
            //cout<<"d="<<d<<endl;
            if(d==-1)
            {
                for(j=i-1;j>=0;j--)
                {
                    if(s[j]<s[i])
                    {
                        swap(s[j],s[i]);
                        break;
                    }
                }
                cout<<s<<endl;
            }
            else{
                for(j=i;j<s.size();j++)
                {
                    if(j!=d)
                    tmp.push_back(s[j]);
                }
                nw.push_back(s[d]);
                sort(tmp.begin(),tmp.end());
                nw+=tmp;
                cout<<nw<<endl;
            }

        }
    }
}

//wlneeb
