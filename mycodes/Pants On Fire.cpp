#include<bits/stdc++.h>
using namespace std;


bool ok(int cur,int b,vector<vector<int> >&connection,vector<int>&vis)
{
    if(cur==b)
    {
        return true;
    }
    int i,j;
    for(i=0;i<connection[cur].size();i++)
    {
        j=connection[cur][i];
        if(!vis[j])
        {
            vis[j]=1;
            bool tmp=ok(j,b,connection,vis);
            if(tmp==true)
            {
                return true;
            }
        }
    }
    return false;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int n,m,i,j,c=0;
    string a,b,s;
    map<string,int>mp;
    cin>>n>>m;
    vector<vector<int> >connection(500);
    for(i=1;i<=n;i++)
    {
        cin>>a>>s>>s>>s>>b;
        if(mp[a]==0)
        {
            c++;
            mp[a]=c;
        }
        if(mp[b]==0)
        {
            c++;
            mp[b]=c;
        }
        int tmp=mp[a],tmp1=mp[b];
        connection[tmp].push_back(tmp1);
    }
    for(i=1;i<=m;i++)
    {
        cin>>a>>s>>s>>s>>b;
        if(mp[a]==0)
        {
            c++;
            mp[a]=c;
        }
        if(mp[b]==0)
        {
            c++;
            mp[b]=c;
        }
        bool fact,inverse;
        vector<int>vis(500,0);
        fact=ok(mp[a],mp[b],connection,vis);
        vector<int>vis1(500,0);
        inverse=ok(mp[b],mp[a],connection,vis1);
        if(fact)
        {
            cout<<"Fact\n";
        }
        else if(inverse)
        {
            cout<<"Alternative Fact\n";
        }
        else
        {
            cout<<"Pants on Fire\n";
        }
    }
}
