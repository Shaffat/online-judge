#include<bits/stdc++.h>
using namespace std;
vector<int>primes;
void get_primes()
{
    vector<bool>v(1e6,0);
    int i,j;
    for(i=4;i<1e6;i+=2)
    {
        v[i]=1;
    }
    for(i=3;i<=sqrt(1e6);i+=2)
    {
        if(v[i]==0)
        {
            for(j=i*i;j<1e6;j+=i)
            {
                v[j]=1;
            }
        }
    }
    for(i=2;i<1e6;i++)
    {
        if(v[i]==0)
        {
            primes.push_back(i);
        }
    }
}

void build_graph(int target,int n,int m,vector<vector<int> >&connection,vector<vector<int> >&cost)
{
    //cout<<"target="<<target<<endl;
    int i,j,counter=0;
    for(i=1;i<n;i++)
    {

        counter++;
        if(i==n-1)
        {
            connection[i].push_back(i+1);
            cost[i].push_back(target-counter+1);
        }
        else
        {
            connection[i].push_back(i+1);
            cost[i].push_back(1);
        }
    }
    for(i=1;i<n;i++)
    {
        for(j=i+2;j<=n;j++)
        {
            if(j==i || j==i+1 || j==i-1)
            {
                continue;
            }
            if(counter>=m)
            {
                break;
            }
            counter++;
            connection[i].push_back(j);
            cost[i].push_back(1e9);
        }
    }
    return;
}

int main()
{
    get_primes();
    int n,m,target,i,j;
    scanf("%d %d",&n,&m);
    vector<int>::iterator low,up;
    up=upper_bound(primes.begin(),primes.end(),n-1);
    target=primes[up-primes.begin()];
//    cout<<primes[primes.size()-1]<<endl;
//    cout<<"t="<<target<<endl;
//    for(i=0;i<30;i++)
//    {
//        cout<<primes[i]<<" ";
//    }
    vector<vector<int> >connection(n+1);
    vector<vector<int> >cost(n+1);
    build_graph(target,n,m,connection,cost);
    printf("%d %d\n",target,target);
    for(i=1;i<n;i++)
    {
        for(j=0;j<connection[i].size();j++)
        {
            printf("%d %d %d\n",i,connection[i][j],cost[i][j]);
        }
    }
}
