#include<bits/stdc++.h>

using namespace std;

struct connection
{
    int u,v,w;
};

bool operator <(connection x,connection y)
{
    if(x.w!=y.w)
    {
        return x.w<y.w;
    }
    return false;
}

int find_parent(int r,int *ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    ar[r]=find_parent(ar[r],ar);
    return ar[r];
}
int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,w,i,j,mx,sum;
        scanf("%d %d",&n,&w);
        int preparent[n+1];
        for(i=1;i<=n;i++)
        {
            preparent[i]=i;
        }
        vector<connection>edges;
        vector<int>prerank(n+1,0);
        vector<int>trail;
        int mst=0;
        for(i=1;i<=w;i++)
        {
            int u,v,c;
            scanf("%d %d %d",&u,&v,&c);
            if(mst==1)
            {
                if(c>=mx)
                {
                    trail.push_back(sum);
                    continue;
                }
            }
            connection nw;
            nw.u=u;
            nw.v=v;
            nw.w=c;
            edges.push_back(nw);
            int x,y;
            x=find_parent(u,preparent);
            y=find_parent(v,preparent);
            if(x!=y)
            {
                if(prerank[x]==prerank[y])
                {
                    preparent[y]=x;
                    prerank[x]++;
                }
                else if(prerank[x]>prerank[y])
                {
                    preparent[y]=x;
                }
                else
                {
                    preparent[x]=y;
                }
            }
            int chk=1;
            for(j=1;j<n;j++)
            {
               // cout<<preparent[preparent[j]]<<" ";
                if(mst==1)
                {
                    break;
                }
                int x,y;
                x=find_parent(j,preparent);
                y=find_parent(j+1,preparent);
                if(x!=y)
                {
                    chk=0;

                }
            }
           // cout<<preparent[j]<<endl;
            if(chk)
            {
                mst=1;
            }
            if(!mst)
            {
                trail.push_back(-1);
            }
            else
            {
                int parent[n+1];
                for(j=1;j<=n;j++)
                {
                    parent[j]=j;
                }
                vector<int>rankp(n+1,0);

                sort(edges.begin(),edges.end());
                int counter=0;
                sum=0;
                mx=-1;
                for(j=0;j<edges.size();j++)
                {
                    //cout<<"here";
                    connection cur_edge=edges[j];
                  // cout<<cur_edge.u<<" "<<cur_edge.v<<" "<<cur_edge.w<<endl;

                    x=find_parent(edges[j].u,parent);
                    y=find_parent(edges[j].v,parent);
                    if(x!=y)
                    {
                        //cout<<"here"<<endl;
                        if(rankp[x]==rankp[y])
                        {
                            parent[y]=x;
                            rankp[x]++;
                        }
                        else if(rankp[x]>rankp[y])
                        {
                            parent[y]=x;
                        }
                        else
                            {
                                parent[x]=y;
                            }
                            mx=max(cur_edge.w,mx);
                        sum+=cur_edge.w;
                        counter++;
                        //cout<<sum<<" ";
                    }
                    if(counter==n-1)
                        {
                            break;
                        }
                }
               // cout<<sum<<endl;
                trail.push_back(sum);

            }
        }
        printf("Case %d:\n",t);
        t++;
        for(i=0;i<trail.size();i++)
        {
            printf("%d\n",trail[i]);
        }

    }
}
