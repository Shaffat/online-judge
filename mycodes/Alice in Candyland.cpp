#include<bits/stdc++.h>
using namespace std;


int calulate_duplicates(int bitmask,int n,vector<vector<int> >&candies,vector<int>&total,vector<int>&row,vector<int>&col)
{
    int i,j,cur=0,total_sum=0;

    for(i=0;i<n;i++)
    {

        if((bitmask&(1<<i))!=0){
                total_sum+=row[i];
            for(j=n;j<2*n;j++)
            {
                if((bitmask&(1<<j))!=0)
                {
                    cur+=candies[i][j-n];
                }
            }
        }
    }
    for(j=n;j<2*n;j++)
    {
        if((bitmask&(1<<j))!=0)
            {
                total_sum+=col[j-n];
            }
    }
    total[bitmask]=total_sum;
    return cur;

}

void preprocess(int n,vector<vector<int> >&candies,vector<int>&memory,vector<int>&total,vector<int>&row,vector<int>&col)
{
    int i,j,last=(1<<(2*n))-1;
    for(i=0;i<=last;i++)
    {
        memory[i]=calulate_duplicates(i,n,candies,total,row,col);
    }
    return;
}


int solve(int n,int bitmask,int k,vector<int>&duplicate,vector<int>&memory,vector<int>&total)
{
    //cout<<"bitmask="<<bitmask<<endl;
    int i,j;
    if(k==0)
    {
        int ans;
        ans=total[bitmask]-duplicate[bitmask];
        return memory[bitmask]=ans;
    }
    if(memory[bitmask]!=-1)
    {
        //cout<<"memorized"<<endl;
        return memory[bitmask];
    }
    int res=0;
    for(i=0;i<2*n;i++)
    {
        if((bitmask&(1<<i))==0)
        {
            int cur=solve(n,(bitmask|(1<<i)),k-1,duplicate,memory,total);
            res=max(cur,res);
        }
    }
    return memory[bitmask]=res;
}



int main()
{
    int test,t,n,k,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&k);
        vector<int>row_sum(17,0);
        vector<int>col_sum(17,0);
        vector<int>col1(n);
        vector<vector<int> >candy(n,col1);
        for(i=0;i<n;i++)
        {
            int cur=0;
            for(j=0;j<n;j++)
            {
                int c;
                scanf("%d",&c);
                candy[i][j]=c;
                cur+=c;
            }
            row_sum[i]=cur;
            //cout<<i<<"th row sum="<<row_sum[i]<<endl;
        }
        for(i=0;i<n;i++)
        {
            int cur=0;
            for(j=0;j<n;j++)
            {
                cur+=candy[j][i];
            }
            col_sum[i]=cur;
            //cout<<i<<"th col sum="<<col_sum[i]<<endl;
        }

        vector<int>duplicate(1<<16,0);
        vector<int>total(1<<16,0);
        vector<int>memory(1<<16,-1);
        preprocess(n,candy,duplicate,total,row_sum,col_sum);
        //cout<<"process done!"<<endl;
        int res=solve(n,0,k,duplicate,memory,total);
        printf("%d\n",res);
    }
}
