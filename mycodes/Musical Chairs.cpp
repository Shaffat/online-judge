
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector <long long int >
#define vll vector< vector <long long int > >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


void chairbylevel(vl&chairtimer,vll&chairs)
{
    ll i,j;
    for(i=0;i<chairtimer.size();i++)
    {
        for(j=0;j<chairtimer.size();j++)
        {
            if(chairtimer[j]>=i)
                chairs[i].push_back(j);
        }
    }
    return;
}

ll findme(ll pos,vl &v)
{
    ll f=0,l=v.size()-1;
    while(f<=l)
    {
        ll m=(f+l)/2;
        if(v[m]==pos)
            return m;
        if(v[m]<pos)
        {
            f=m+1;
        }
        else
            l=m-1;
    }
}

ll solve(ll pos,ll song,vl&chairtimer, vl &songs ,vll&chairs,vll &memory)
{
    //cout<<"pos="<<pos<<" song="<<song<<endl;
    if(song>=songs.size())
        return 1;
    if(memory[pos][song]!=-1)
        return memory[pos][song];
    ll i,j,n,moves,clck,rclck,l,r,p;
    n=songs.size()-song+1;
    moves=songs[song]%n;
    p=findme(pos,chairs[song]);
//    cout<<"moves="<<moves<<" songlen="<<songs[song]<<endl;
//    cout<<"number of chairs="<<n<<" my pos="<<p<<endl;
    l=p-moves;
    if(l<0)
    {
        l+=n;
    }
    r=p+moves;
    if(r>=n)
    {
        r-=n;
    }
//    cout<<"l="<<l<<" r="<<r<<endl;
    ll res1=0,res2=0,lc,rc;
    lc=chairs[song][l];
    rc=chairs[song][r];
//    cout<<"left chair="<<lc<<" right chair="<<rc<<endl;
//    cout<<"lc timer="<<chairtimer[lc]<<" rc timer="<<chairtimer[rc]<<endl;
    if(chairtimer[lc]>song)
    {
        res1=solve(lc,song+1,chairtimer,songs,chairs,memory);
    }
    if(chairtimer[rc]>song)
    {
        res1=solve(rc,song+1,chairtimer,songs,chairs,memory);
    }
    return memory[pos][song]=max(res1,res2);
}

int main()
{
    ll i,j,n,p;
//    cout<<"here"<<endl;
    sc2(n,p);
    vl songs;
    vl chair(n,2e9);
    vll chairs(n+1);
    //cout<<"here1"<<endl;
    for(i=1;i<n;i++)
    {
        sc1(j);
        songs.push_back(j);
    }
    //cout<<"here2"<<endl;
    for(i=0;i<n-1;i++)
    {
        sc1(j);
        //cout<<"chair="<<j<<" i="<<i<<endl;
        chair[j-1]=i;
    }
    //cout<<"here3"<<endl;
    chairbylevel(chair,chairs);
    vl col(n+100,-1);
    vll memory(n+100,col);
    ll res=solve(p-1,0,chair,songs,chairs,memory);
    if(res)
    {
        printf("Yes\n");
    }
    else
        printf("No\n");
}
