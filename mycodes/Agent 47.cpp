#include<bits/stdc++.h>
using namespace std;

int solve(int bitmask,vector<int>&life,vector<vector<int> >&damage,vector<int>&memory)
{
    //cout<<"bitmask="<<bitmask<<endl;
    if(bitmask==((1<<(life.size()-1))-1))
    {
        return 0;
    }
    if(memory[bitmask]!=-1)
    {
        return memory[bitmask];
    }
    int total=2e9,i,j,n=life.size()-1;
    for(i=0;i<n;i++)
    {
        if((bitmask&(1<<i))==0)
        {
            //cout<<"killing "<<i<<endl;
            int case1;
            int shot=life[i];
            for(j=0;j<n;j++)
            {
                //cout<<"j="<<j<<" bitmask "<<bitmask<<" killed check="<<(1<<i)<<endl;
                if((bitmask&(1<<j))==(1<<j))
                {
                    if(damage[j][i]>0)
                    {
                        double l=damage[j][i],m=shot;
                        //cout<<j<<" was killed so i can use"<<endl;
                        shot=min(m,ceil(life[i]/l));
                    }
                }
            }
            //cout<<"killing "<<i<<" with shot "<<shot<<endl;
            case1=solve((bitmask|(1<<i)),life,damage,memory)+shot;
            //cout<<"after killing "<<i<<" total shot="<<case1<<endl;
            total=min(total,case1);
        }
    }
    return memory[bitmask]=total;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,j,test,t=1,n,k;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>life(n+1);
        vector<vector<int> >damage(n+1,life);
        for(i=0;i<n;i++)
        {
            scanf("%d",&life[i]);
        }
        for(i=0;i<n;i++)
        {
            string s;
            cin>>s;
            for(j=0;j<n;j++)
            {
                damage[i][j]=s[j]-'0';
            }
        }
        vector<int>memory(100000,-1);
        int res=solve(0,life,damage,memory);
        printf("Case %d: %d\n",t,res);
    }
}
