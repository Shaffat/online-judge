/*
ID: msdipu11
PROG: barn1
LANG: C++14
*/

#include<bits/stdc++.h>

using namespace std;

int solve(int m,int pos,vector<int>&stalls,vector<vector<int> >&memory)
{
    if(pos>=stalls.size()) return 0;
    if(m<=0) return 1e9;
    int i,j,mn=1e9,cur;
    if(memory[m][pos]!=-1)
    {
        return memory[m][pos];
    }
    for(i=pos;i<stalls.size();i++)
    {
        cur=solve(m-1,i+1,stalls,memory)+stalls[i]-stalls[pos]+1;
        mn=min(mn,cur);
    }
    return memory[m][pos]=mn;
}

int main()
{
    freopen("barn1.in","r",stdin);
    freopen("barn1.out","w",stdout);
    int m,pos,i,j,s,c,res;
    scanf("%d %d %d",&m,&s,&c);
    vector<int>col(s+1,-1);
    vector<vector<int> >memory(m+1,col);
    vector<int>stalls;
    for(i=1;i<=c;i++)
    {
        scanf("%d",&j);
        stalls.push_back(j);
    }
    sort(stalls.begin(),stalls.end());
    res=solve(m,0,stalls,memory);
    printf("%d\n",res);
}
