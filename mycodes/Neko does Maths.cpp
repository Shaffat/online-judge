#include<bits/stdc++.h>
using namespace std;

long long int GCD(long long int a,long long int b)
{
    while(a%b!=0)
    {
        long long int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}


int main()
{
    long long int a,b,i,j,lcm,cur,k=0,res=0;
    scanf("%lld %lld",&a,&b);

    lcm=(a*b)/GCD(a,b);
    //cout<<"lcm="<<lcm<<endl;
    for(i=0;i<=5000000;i++)
    {
        cur=((a+i)*(b+i))/GCD(a+i,b+i);


        if(cur<lcm)
        {
            lcm=cur;
            res=i;
        }
    }
    printf("%d\n",res);
}
