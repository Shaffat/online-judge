#include<bits/stdc++.h>
using namespace std;

#define ll unsigned long long int


double power(double base, ll p)
{
    if(p==0)
    {
        return 1;
    }
    if(p==1)
    {
        return base;
    }
    if(p%2==0)
    {
        double cur=power(base,p/2);
        if(cur>6e9)
        {
            return 1.2e19;
        }
        return cur*cur;
    }
    else
    {
        double cur=power(base,p/2);
        if(cur>6e9)
        {
            return 1.2e19;
        }
        return cur*cur*base;
    }
}



ll solve(double n,double r,double p)
{
    ll first,last,mid,res,f=0;
    first=0;last=1e4;
    while(first<=last)
    {
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f=1;
        }
        //cout<<"first="<<first<<" "<<last<<endl;
        mid=(first+last)/2;
        //cout<<"cur="<<power(r,mid)<<" need="<<(p-n)/n<<" mid="<<mid<<endl;
        if((power(r,mid)-1)>=(((p-n)/n)))
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}
int main()
{
    ll i,j,test,t,res;
    double r,n,p;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lf %lf %lf",&n,&r,&p);
        r/=100;
        r+=1;
        res=solve(n,r,p);
        printf("Case %lld: %lld\n",t,res);
    }

}
