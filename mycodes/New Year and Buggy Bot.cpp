#include<bits/stdc++.h>
using namespace std;
struct pos
{
    int row,col;
};
int r,c;
int maze[100][100];
string s;
bool solve(int up,int down,int left,int right,pos start,pos ending)
{
    int i,j;
    pos cur=start;
    for(i=0;i<s.size();i++)
    {
        if(s[i]-'0'==left)
        {
            cur.col--;
            if(cur.col<1 || maze[cur.row][cur.col]==-1)
            {
                return false;
            }
        }
        if(s[i]-'0'==right)
        {
            cur.col++;
            if(cur.col>c || maze[cur.row][cur.col]==-1)
            {
                return false;
            }
        }
        if(s[i]-'0'==up)
        {
            cur.row--;
            if(cur.row<1 || maze[cur.row][cur.col]==-1)
            {
                return false;
            }
        }
        if(s[i]-'0'==down)
        {
            cur.row++;
            if(cur.row>r || maze[cur.row][cur.col]==-1)
            {
                return false;
            }
        }
        if(cur.row==ending.row && cur.col==ending.col)
        {
            return true;
        }
    }
    return false;
}


int main()
{
    int i,j;
    pos start,ending;
    scanf("%d %d",&r,&c);
    for(i=1;i<=r;i++)
    {
        for(j=1;j<=c;j++)
        {
            char k;
            scanf(" %c",&k);
            if(k=='#')
            {
                maze[i][j]=-1;
            }
            else
            {
                if(k=='S')
                {
                    start.row=i;
                    start.col=j;
                }
                if(k=='E')
                {
                    ending.row=i;
                    ending.col=j;
                }
                maze[i][j]=0;
            }
        }

    }
    cin>>s;
    string cmd;
    cmd="0123";
    int counter=0;
    if(solve(cmd[0]-'0',cmd[1]-'0',cmd[2]-'0',cmd[3]-'0',start,ending))
    {
        counter++;
    }
    while(next_permutation(cmd.begin(),cmd.end()))
    {
        //cout<<cmd<<endl;
        if(solve(cmd[0]-'0',cmd[1]-'0',cmd[2]-'0',cmd[3]-'0',start,ending))
        {
            counter++;
        }
    }
    cout<<counter<<endl;
}
