#include<bits/stdc++.h>
using namespace std;
vector<int>col(3,-1);
vector<vector<int> >memory(1001,col);
vector<vector<int> >testcase(1001,col);
int n,t;
int solve(int floor,int side,vector<vector<int> >&side_change,vector<vector<int> >&snack_time)
{
    //cout<<"floor="<<floor<<" side="<<side<<endl;
    if(floor>n)
    {
        return 0;
    }
    if(testcase[floor][side]==t)
    {
        return memory[floor][side];
    }
    int case1,case2;
    case1=solve(floor+1,side,side_change,snack_time);
    case2=solve(floor+1,(side+1)%2,side_change,snack_time)+side_change[floor][side];
    testcase[floor][side]=t;
    //cout<<"memory["<<floor<<"]["<<side<<"]="<<min(case1,case2)+snack_time[floor][side]<<endl;
    return memory[floor][side]=min(case1,case2)+snack_time[floor][side];
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int i,j;
        scanf("%d",&n);
        vector<int>col(2,0);
        vector<vector<int> >side_change(n+1,col);
        vector<vector<int> >snack_time(n+1,col);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            snack_time[i][0]=j;
        }
         for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            snack_time[i][1]=j;
        }
         for(i=1;i<n;i++)
        {
            scanf("%d",&j);
            side_change[i][0]=j;
        }
        for(i=1;i<n;i++)
        {
            scanf("%d",&j);
            side_change[i][1]=j;
        }
        int res=min(solve(1,0,side_change,snack_time),solve(1,1,side_change,snack_time));
        printf("Case %d: %d\n",t,res);
    }
}
