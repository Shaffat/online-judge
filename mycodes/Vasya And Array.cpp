#include<bits/stdc++.h>
using namespace std;


struct segment
{
    int s,e;
};

bool operator <(segment a,segment b)
{
    if(a.s!=b.s)
    {
        return a.s<b.s;
    }
    return false;
}

void initForOne(int l,int r,vector<int>&ar)
{
    for(int i=l;i<=r;i++)
    {
        ar[i]=1e9;
    }
    return;
}


int main()
{
    int n,m,i,j,t,l,r;
    scanf("%d %d",&n,&m);
    vector<int>v(n+100,0);
    vector<segment>ve;
    for(i=1;i<=m;i++)
    {
        scanf("%d %d %d",&t,&l,&r);
        if(t==1)
        {
            initForOne(l,r,v);
        }
        else
        {
            segment tmp;
            tmp.s=l;
            tmp.e=r;
            ve.push_back(tmp);
        }
    }
    for(i=1;i<=n;i++)
    {
        printf("%d ",v[i]);
    }
    sort(ve.begin(),ve.end());
    int valid=1;
    for(i=0;i<ve.size();i++)
    {
        int ok=0;
        l=ve[i].s;
        r=ve[i].e;
        for(j=l;j<r;j++)
        {
            if(v[j]!=v[j+1] && v[j]!=0 && v[j+1]!=0)
            {
                ok=1;
                break;
            }
            else
            {
                if(v[j]==0)
                {
                    v[j]=1;
                    v[j+1]=1e9;
                }
            }
        }
        if(ok==0)
        {
            valid=0;
            break;
        }
    }
    for(i=1;i<=n;i++)
    {
        if(v[i]==0)
        {
            v[i]=1e9;
        }
    }
    if(valid)
    {
        printf("YES\n");
        for(i=1;i<=n;i++)
        {
            printf("%d ",v[i]);
        }
    }
    else
        printf("NO\n");
}
