
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int main()
{
    ll test,t=1,i,j,nonzero,minimum;
    sc1(test);
    while(test--)
    {
        vl digits(10);
        minimum=2e9;
        FOR(i,0,9)
        {
            sc1(j);
            digits[i]=j;
            if(i>0)
            {
                if(minimum>j)
                {
                    nonzero=i;
                    minimum=j;
                }
            }
        }
        if(minimum==0)
        {
            printf("%d\n",nonzero);
        }
        else if(digits[0]==0)
        {
            printf("10\n");
        }
        else if(minimum-digits[0]>=1)
        {
            printf("1");
            for(i=1; i<=digits[0]+1; i++)
            {
                printf("0");
            }
            printf("\n");
        }
        else
        {
            for(i=1; i<=minimum+1; i++)
            {
                printf("%d",nonzero);
            }
            printf("\n");
        }
    }

}
