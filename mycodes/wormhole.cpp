#include<bits/stdc++.h>
using namespace std;

struct edges
{
    int u,v,w;
};

int main()
{
    int test,i,j,n,m;
    scanf("%d",&test);
    while(test--)
    {
        int neg=0;
        scanf("%d %d",&n,&m);
        vector<int>dis(n+1,2e9);
        dis[0]=0;
        vector<edges>e;
        for(i=1;i<=m;i++)
        {
            int u,v,w;
            scanf("%d %d %d",&u,&v,&w);
            edges temp;
            temp.u=u;
            temp.v=v;
            temp.w=w;
            e.push_back(temp);
            if(w<0)
            {
                neg=1;
            }
        }
        if(!neg)
        {
            printf("not possible\n");
        }
        else
        {
            for(j=1;j<n;j++)
            {
                for(i=0;i<e.size();i++)
                {
                    int u,v,w;
                    u=e[i].u;
                    v=e[i].v;
                    w=e[i].w;
                    if(dis[v]>(dis[u]+w))
                    {
                        dis[v]=dis[u]+w;
                    }
                }
            }
            int chk=0;
            for(i=0;i<e.size();i++)
            {
                int u,v,w;
                    u=e[i].u;
                    v=e[i].v;
                    w=e[i].w;
                    if(dis[v]>(dis[u]+w))
                    {
                        chk=1;
                        break;
                    }
            }
            if(!chk)
            {
                printf("not possible\n");
            }
            else
            {
                printf("possible\n");
            }
        }
    }

}
