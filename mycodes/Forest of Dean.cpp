
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct info
{
    ll p,val;
};
info tree[400000];



void update(ll node,ll val,ll s,ll e,ll q_s,ll q_e)
{
    if(s>q_e||e<q_s)
    {
        return;
    }
    if(s>=q_s && e<=q_e)
    {
        tree[node].p+=val;
        //cout<<"before ="<<tree[node].val<<endl;
        tree[node].val+=val*(e-s+1);
         //cout<<"add propagtion at node "<<node<<" s="<<s<<" e="<<e<<" val="<<tree[node].val<<" p="<<tree[node].p<<endl;
        return;
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    update(left,val,s,mid,q_s,q_e);
    update(right,val,mid+1,e,q_s,q_e);
    tree[node].val=tree[left].val+tree[right].val+(e-s+1)*tree[node].p;
    //cout<<"updated value at node "<<node<<" s="<<s<<" e="<<e<<" total="<<tree[node].val<<endl;
}


ll query(ll node,ll s,ll e,ll q_s,ll q_e,ll carry)
{
   // cout<<"node="<<node<<" s="<<s<<" e="<<e<<" carry="<<carry<<endl;
    if(s>q_e||e<q_s)
    {
        return 0;
    }
    if(s>=q_s && e<=q_e)
    {
        return tree[node].val+carry*(e-s+1);
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    return query(left,s,mid,q_s,q_e,carry+tree[node].p)+query(right,mid+1,e,q_s,q_e,carry+tree[node].p);
}


void build_tree(ll node,ll start,ll ending,vector<ll>&values,map<ll,ll>&ranking)
{

    ll left,right,mid;
    if(start==ending)
    {
        tree[node].p=0;
        tree[node].val=ranking[values[start]];
        //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
        return;
    }
    left=(node<<1);
    right=(node<<1)+1;
    mid=(start+ending)/2;
    build_tree(left,start,mid,values,ranking);
    build_tree(right,mid+1,ending,values,ranking);
    tree[node].p=0;
    tree[node].val=tree[left].val+tree[right].val;
     //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
    return;
}

ll finder(ll target,vl &v)
{
    ll res=-1,first=0,last=v.size()-1,mid,d=0,i,j;
    while(first<=last)
    {
        if(d) break;
        if(first==last)
            d++;
        mid=(first+last)/2;
        if(v[mid]==target)
            return mid;
        if(v[mid]<target)
        {
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return -1e9;
}

int main()
{
    ll test,t,i,j,h,k,n;
    sc1(test);
    for(t=1;t<=test;t++)
    {
        k=1;
        sc2(n,h);
        map<ll,ll>rankid;
        vector<ll>rankmember(n+10,0);
        vector<ll>v1;
        vector<ll>v;
        for(i=1;i<=n;i++)
        {
            ll r;
            sc1(j);
            v1.push_back(j);
        }

        sort(v1.begin(),v1.end());

        for(i=0;i<v1.size();i++)
        {
            ll r=rankid[v1[i]];
            if(r==0)
            {
                v.push_back(v1[i]);
                rankid[v1[i]]=k;
                r=k;
                k++;
            }
            rankmember[r]++;
        }

        build_tree(1,1,k-1,rankmember,rankid);
        ll ok=1;
        for(i=0;i<v.size();i++)
        {
            //cout<<"i="<<i<<endl;
            ll cur,nxt,val,p;
            cur=v[i];
            val=query(1,1,k-1,cur,cur,0);

            //cout<<"val="<<val<<endl;
            if(val<0)
            {
                ok=0;
                break;
            }
            nxt=cur+h-1;
            p=finder(nxt,v);
            //cout<<"nxt="<<nxt<<" p="<<p<<endl;
            if(p-i==h-1)
            {
                update(1,-val,1,k-1,i+1,p+1);
            }
            else if(val>0)
            {
                ok=0;
                break;
            }
        }
        if(ok)
            printf("True\n");
        else
            printf("False\n");

    }
}
