#include<bits/stdc++.h>
using namespace std;
long long int GCD(long long int a,long long int b)
{
    while(a%b!=0)
    {
        int c=a%b;
        a=b;
        b=c;
    }
    return b;
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    long long int test,t,n,i,j;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld",&n);
        long long int res=1;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            if(i==1)
            {
                res=j;
            }
            else
                res=(res*j)/GCD(res,j);
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
