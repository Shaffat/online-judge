#include<bits/stdc++.h>
using namespace std;

vector<int>col(20000,-1);
vector<vector<int> >testcase(100,col);

vector<double>col1(20000);
vector<vector<double> >memory(100,col1);

int t;
double p;

double solve(int pos,int need,vector<double>&chance,vector<int>&money)
{
    //cout<<"pos="<<pos<<" need="<<need<<endl;
    if(need<=0)
        return 1;
    if(pos>=chance.size())
        return 0;
    if(testcase[pos][need]==t)
        return memory[pos][need];
    double res,cur1,cur2;
    cur1=solve(pos+1,need-money[pos],chance,money)*(1-chance[pos]);
    cur2=solve(pos+1,need,chance,money);
    testcase[pos][need]=t;
    //cout<<"memory["<<pos<<"]["<<need<<"]="<<max(cur1,cur2)<<endl;
    return memory[pos][need]=max(cur1,cur2);
}

int ansme(vector<double>&chance,vector<int>&money)
{
    int first=0,last=10000,mid,i,j,f=0,res=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;
        mid=(first+last)/2;
        //cout<<"how much "<<mid<<" cost ----------------------------------"<<endl;
        double cur=1.0-solve(0,mid,chance,money);
        if(cur<p)
        {
            first=mid+1;
            res=mid;
        }
        else
            last=mid-1;
    }
    return res;

}
int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,i,j,n,m,res;
    double d;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        vector<int>money;
        vector<double>chance;
        scanf("%lf %d",&p,&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d %lf",&m,&d);
            money.push_back(m);
            chance.push_back(d);
        }
        res=ansme(chance,money);
        printf("Case %d: %d\n",t,res);
    }
}
