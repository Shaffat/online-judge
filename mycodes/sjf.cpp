#include<bits/stdc++.h>
using namespace std;

struct proc
{
    int id,burst,arrival;
};

bool operator<(proc a, proc b)
{
    if(a.burst!=b.burst)
    {
        return a.burst>b.burst;
    }
    return false;
}
int main()
{
    int i,j,n,k,l,m;
    vector<proc>v;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&j,&k);
        proc tmp;
        tmp.arrival=j;
        tmp.burst=k;
        tmp.id=i;
        v.push_back(tmp);
    }
    vector<int>response(n+1);
    vector<int>waiting(n+1);
    vector<int>turnaround(n+1);
    int time=v[0].arrival,nxt=1;
    priority_queue<proc>q;
    q.push(v[0]);
    for(i=1;i<v.size();i++)
    {
        nxt=i;
        if(v[i].arrival>time)
        {
            break;
        }
        else
            q.push(v[i]);
    }
    while(!q.empty())
    {
        proc cur=q.top();
        q.pop();
        response[cur.id]=time;
        waiting[cur.id]=time-cur.arrival;
        turnaround[cur.id]=waiting[cur.id]+cur.burst;
        time+=cur.burst;
        while(nxt<v.size())
        {
            if(v[nxt].arrival>time)
            {
                if(q.empty())
                {
                    time=v[nxt].arrival;
                    q.push(v[nxt]);
                }
                else
                    break;
            }
            else
            {
                q.push(v[nxt]);
                nxt++;
            }
        }
    }
    for(i=1;i<=n;i++)
    {
        cout<<"process"<<i<<"  waiting="<<waiting[i]<<" turnaround="<<turnaround[i]<<endl;
    }
}
