#include<iostream>
#include<stdio.h>
#include<algorithm>
#include<math.h>
#include<string>
using namespace std;
int stringtoint(string s)
{
    int res=0,j=1,i;
    for(i=s.size()-1;i>=0;i--)
    {
        res+=(s[i]-'0')*j;
        j*=10;
    }
    return res;
}
string intostring(int n)
{
    string res="";
    while(n>0)
    {
        int i=n%10;
        res.insert(res.begin(),'0'+i);
        n/=10;
    }
    return res;
}

string sum(string a,string b)
{
    int i,j;
    string res="";
    if(a.size()!=b.size())
    {
        if(a.size()<b.size())
        {
            while(a.size()<b.size())
            {
                a.insert(a.begin(),'0');
            }
        }
        if(b.size()<a.size())
        {
            while(b.size()<a.size())
            {
                b.insert(b.begin(),'0');
            }
        }
    }
    //cout<<a<<" "<<b<<endl;
    int carry=0;
    for(i=a.size()-1;i>=0;i--)
    {
        int l,m,total;
        l=a[i]-'0';
        m=b[i]-'0';
        total=l+m+carry;
        if(total<10)
        {
            carry=0;
            res.push_back('0'+total);
        }
        else
        {
            carry=1;
            res.push_back('0'+total-10);
        }
    }
    if(carry)
    {
        res.push_back('1');
    }
    reverse(res.begin(),res.end());
    return res;
}

string mul(string s,int n)
{
    //cout<<"activated"<<endl;
    int i,j,carry=0;
    string res;
    for(i=s.size()-1;i>=0;i--)
    {
        int l=s[i]-'0',total;
        total=l*n+carry;
        //cout<<"total="<<total<<endl;
        carry=total/10;
        res.push_back('0'+total%10);
    }
    //cout<<"carry="<<carry<<endl;
    if(carry)
    {
        while(carry>0)
        {
            res.push_back('0'+carry%10);
            carry/=10;
        }
    }
    reverse(res.begin(),res.end());
    return res;
}

string multiplication(string a,string  b)
{
    string res="";
    int l=0,j,i;
    for(i=b.size()-1;i>=0;i--)
    {
        string tmp1,tmpres;
        int n=b[i]-'0';
        tmpres=mul(a,n);
        for(j=1;j<=l;j++)
        {
            tmpres.push_back('0');
        }
        res=sum(res,tmpres);
        //cout<<"res="<<res<<endl;
        l++;

    }
    return res;
}
int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,n,a;
    while(scanf("%d %d",&n,&a)!=EOF)
    {
        if(a==0)
        {
            printf("0\n");
            continue;
        }
        string powerup=intostring(a);
        string initial=intostring(a);
        string res=initial;
        for(i=2;i<=n;i++)
        {
            string tmp;
            initial=multiplication(initial,powerup);
            //cout<<"initial="<<initial<<endl;
            tmp=mul(initial,i);
            //cout<<"tmp="<<tmp<<endl;
            res=sum(res,tmp);
        }
        cout<<res<<endl;
    }
}
