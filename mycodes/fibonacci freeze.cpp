#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long int llu;

string add(string a,string b)
{
    int i,j,left=0;
    string result="";
    while(a.size()<b.size())
    {
        a.insert(a.begin()+0,'0');
    }
    while(b.size()<a.size())
    {
        b.insert(b.begin()+0,'0');
    }

    for(i=a.size()-1;i>=0;i--)
    {
        int l,m,sum;
        l=a[i]-'0';
        m=b[i]-'0';
        sum=l+m+left;
        if((sum)>=10)
        {
            result.insert(result.begin()+0,'0'+sum-10);
            left=sum/10;

        }
        else
        {
            result.insert(result.begin()+0,'0'+sum);
            left=sum/10;
        }

    }
    if(left>0)
    {
        result.insert(result.begin()+0,'0'+left);
    }
    return result;
}

vector<string>memory(5001,"-1");
string fib_generator(int n)
{
    if(n<=0)
    {
        return "0";
    }
    if(n==1)
    {
        return "1";
    }
    if(memory[n]!="-1")
    {
        return memory[n];
    }
    return memory[n]=add(fib_generator(n-2),fib_generator(n-1));

}
int main()
{
    int n;
    while(scanf("%d",&n)!=EOF)
    {
        cout<<"The Fibonacci number for "<<n<<" is "<<fib_generator(n)<<"\n";
    }
}
