
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct snakes
{
    int x,y;
};

bool operator <(snakes a, snakes b)
{
    if(a.x!=b.x)
    {
        return a.x<b.x;
    }
    return a.y<b.y;
}

bool ok(snakes a, snakes b)
{
    if(a.x==b.x && a.y==b.y) return 1;
    return 0;
}


int fw(int sk1,int sk2,vector<snakes>&snake1,vector<snakes>&snake2,map<snakes,int>&memory)
{
    //cout<<"fw sk1="<<sk1<<" sk2="<<sk2<<" sk1="<<snake1[sk1].x<<","<<snake1[sk1].y<<" sk2="<<snake2[sk2].x<<","<<snake2[sk2].y<<endl;
    if(sk1>=snake1.size() || sk2 >= snake2.size())
        return 0;
    if(ok(snake1[sk1],snake2[sk2])==0)
        return 0;
    if(memory[snake1[sk1]]>0)
        return memory[snake1[sk1]];
    return memory[snake1[sk1]]=fw(sk1+1,sk2+1,snake1,snake2,memory)+1;
}

int rw(int sk1,int sk2,vector<snakes>&snake1,vector<snakes>&snake2,map<snakes,int>&memory)
{
    //cout<<"rw sk1="<<sk1<<" sk2="<<sk2<<" sk1="<<snake1[sk1].x<<","<<snake1[sk1].y<<" sk2="<<snake2[sk2].x<<","<<snake2[sk2].y<<endl;
    if(sk1>=snake1.size() || sk2 <0)
        return 0;
    if(ok(snake1[sk1],snake2[sk2])==0)
        return 0;
    if(memory[snake1[sk1]]>0){
        //cout<<"memorize"<<endl;
        return memory[snake1[sk1]];
    }
    return memory[snake1[sk1]]=rw(sk1+1,sk2-1,snake1,snake2,memory)+1;
}

int main()
{
  map<snakes,int>skpos1;
  map<snakes,int>skpos2;
  vector<snakes>snake1;
  vector<snakes>snake2;
  map<snakes,int>memory1;
  map<snakes,int>memory2;
  map<snakes,int>memory3;
  map<snakes,int>memory4;
  //fw(1,2,snake1,snake2,memory1);
  int n,i,j,x,y,m;
  scanf("%d",&n);
  for(i=1;i<=n;i++)
  {
      scanf("%d %d",&x,&y);
      snakes tmp;
      tmp.x=x;
      tmp.y=y;
      skpos1[tmp]=i;
      snake1.push_back(tmp);
      //cout<<"pos="<<skpos1[tmp]<<endl;
  }
  scanf("%d",&m);
  for(i=1;i<=m;i++)
  {
      scanf("%d %d",&x,&y);
      snakes tmp;
      tmp.x=x;
      tmp.y=y;
      skpos2[tmp]=i;
      snake2.push_back(tmp);
      //cout<<"pos="<<skpos2[tmp]<<endl;
  }

  int res=0;
  for(i=0;i<snake1.size();i++)
  {
      int cur=skpos2[snake1[i]];
      //cout<<"cur="<<cur<<endl;
      if(cur>0)
      {
          //cout<<"calling fw "<<i<<" "<<cur-1<<endl;
          int tmp,tmp1;
          tmp=fw(i,cur-1,snake1,snake2,memory1);
          tmp1=rw(i,cur-1,snake1,snake2,memory2);
          res=max(res,max(tmp,tmp1));
          //cout<<"tmp="<<tmp<<" tmp1="<<tmp1<<endl;
      }
  }
  //cout<<"snake 2"<<endl;
  for(i=0;i<snake2.size();i++)
  {
      int cur=skpos1[snake2[i]];
      //cout<<"cur="<<cur<<endl;
      if(cur>0)
      {
          int tmp,tmp1;
          tmp=fw(i,cur-1,snake2,snake1,memory3);
          tmp1=rw(i,cur-1,snake2,snake1,memory4);
          //cout<<"tmp="<<tmp<<" tmp1="<<tmp1<<endl;
          res=max(res,max(tmp,tmp1));
      }
  }
  printf("%d\n",res);

}
