
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())



ll query(ll index,ll s,ll e,ll q_s,ll q_e,vl &tree)
{
    if(s>q_e || e<q_s) return 1e9;
    if(s>=q_s && e<=q_e)
    {
        return tree[index];
    }
    ll mid,i,j,left,right;
    mid=(s+e)/2;
    left=2*index;
    right=(2*index)+1;
    return min(query(left,s,mid,q_s,q_e,tree),query(right,mid+1,e,q_s,q_e,tree));
}


void buildTree(ll index,ll s,ll e,vl &v,vl &tree)
{
    if(s>e) return;
    ll mid,i,j,left,right;
    if(s==e)
    {
        tree[index]=v[s];
        return;
    }
    mid=(s+e)/2;
    left=2*index;
    right=(2*index)+1;
    buildTree(left,s,mid,v,tree);
    buildTree(right,mid+1,e,v,tree);
    tree[index]=min(tree[left],tree[right]);
    return;
}

bool ok(ll n,ll x , vl &tree)
{
    ll i,j;
    FOR(i,1,n-x+1)
    {
        ll ans=query(1,1,n,i,i+x-1,tree);
        if(ans>=x) return true;
    }
    return false;
}

ll solve(ll n, vl &tree)
{
    ll first,last,mid,i,j,d=0,res=0;
    first=1;
    last=n;
    while(first<=last)
    {
        if(d) break;
        if(first==last) d++;

        mid=(first+last)/2;
        if(ok(n,mid,tree))
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
    ll n,k,a,b,i,j;
    sc1(n);
    vl tree1(4*n);
    vl tree2(4*n);
    vl h1(n+1);
    vl h2(n+1);
    FOR(i,1,n){
        sc2(a,b);
        h1[i]=a;
        cout<<"h1="<<a<<endl;
        h2[i]=n-1-b;
        cout<<"h2="<<n-1-b<<endl;
    }
    buildTree(1,1,n,h1,tree1);
    buildTree(1,1,n,h2,tree2);
    ll res1,res2;
    res1=solve(n,tree1);
    res2=solve(n,tree2);
    printf("%lld\n",max(res1,res2));
}
