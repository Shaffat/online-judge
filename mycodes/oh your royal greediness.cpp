#include<bits/stdc++.h>
using namespace std;
struct scedule
{
    int value;
    bool start;
};
bool operator <(scedule a,scedule b)
{
    if(a.value!=b.value)
    {
        return a.value<b.value;
    }
    else if(a.start!=b.start)
    {
        return a.start>b.start;
    }
    else return false;
}
int main()
{
    int n,t=1;
    while(scanf("%d",&n))
    {
        if(n==-1)
        {
            break;
        }
        int i,j,extra=0,s,f;
        vector<scedule>solve;
        for(i=1; i<=n; i++)
        {
            scanf("%d %d",&s,&f);
            scedule tmp;
            tmp.value=s;
            tmp.start=1;
            solve.push_back(tmp);
            tmp.value=f;
            tmp.start=0;
            solve.push_back(tmp);
        }
        sort(solve.begin(),solve.end());
        int res=0;
        for(i=0;i<solve.size();i++)
        {
            if(solve[i].start==1)
            {
                extra++;
            }
            else
                extra--;
            res=max(res,extra);

        }
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
