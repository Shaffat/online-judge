#include<bits/stdc++.h>
using namespace std;
int main()
{
    int n;
    while(cin>>n)
    {
        vector<long long>v;
        int odd=0,even=0,maxim=0;
        for(int i=1;i<=n;i++)
        {
            long long c;
            cin>>c;
            v.push_back(c);
            if(c%2==0)
            {
                even++;
            }
            else
                odd++;
             if(maxim<c)
             {
                 maxim=c;
             }

        }
        if(even==n-1)
        {
            cout<<"2"<<endl;
            continue;
        }
        int counter,result;
        for(long long i=2;i<=sqrt(maxim)+1;i++)
        {
            counter=0;
            for(long long j=0;j<v.size();j++)
            {
                if(v.at(j)%i==0)
                {
                    counter++;
                }
            }
            if(counter==n-1)
            {
                result=i;
                break;
            }
        }
        cout<<result<<endl;
    }
}

