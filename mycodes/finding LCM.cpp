#include<bits/stdc++.h>
using namespace std;
long long int GCD(long long int a,long long int b)
{
    while(a%b!=0)
    {
        long long int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()
{
    long long int a,b,c,test,t=1,L,l_of_ab,res,i;
    scanf("%lld",&test);
    while(t<=test)
    {
        scanf("%lld %lld %lld",&a,&b,&L);
        l_of_ab=a*b/GCD(a,b);
        res=2e12;
        for(i=1;i<=sqrt(L);i++)
        {
            if(L%i==0)
            {
                if(l_of_ab*i/GCD(l_of_ab,i)==L)
                {
                    res=min(res,i);
                }
                if(l_of_ab*(L/i)/GCD(l_of_ab,(L/i))==L)
                {
                    res=min(res,L/i);
                }

            }

        }
        if(res==2e12)
        {
            printf("Case %lld: impossible\n",t);
        }
        else
            printf("Case %lld: %lld\n",t,res);
        t++;
    }
}
