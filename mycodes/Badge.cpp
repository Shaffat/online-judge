#include<bits/stdc++.h>
using namespace std;

int solve(int node,vector<int>&p,vector<int>&vis)
{
    if(vis[node])
    {
        return node;
    }
    vis[node]=1;
    return solve(p[node],p,vis);
}


int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<int>p(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        p[i]=j;
    }
    for(i=1;i<=n;i++)
    {
        vector<int>vis(n+1,0);
        printf("%d ",solve(i,p,vis));
    }

}
