#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
         double Ox, Oy, Ax, Ay, Bx, By,Dx,Dy,OA,AD,theta,result;
         scanf("%lf %lf %lf %lf %lf %lf",&Ox,&Oy,&Ax,&Ay,&Bx,&By);
         Dx=(Ax+Bx)/2.0;
         Dy=(Ay+By)/2.0;
         //cout<<"Dx="<<Dx<<" Dy="<<Dy<<endl;
         OA=sqrt(((Ox-Ax)*(Ox-Ax))+((Oy-Ay)*(Oy-Ay)));
         AD=sqrt(((Ax-Dx)*(Ax-Dx))+((Ay-Dy)*(Ay-Dy)));
         //cout<<"OA="<<OA<<" AD="<<AD<<endl;
         theta=2*asin(AD/OA);
         //cout<<theta<<endl;
         result=OA*theta;
         printf("Case %d: %.6lf\n",t,result);
         t++;
    }
}
