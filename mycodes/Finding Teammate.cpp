#include<bits/stdc++.h>
using namespace std;

#define ll long long int

int main()
{
    ll test,n,i,j;
    vector<ll>factorial(1e6+1,1);
    for(i=1;i<=1e6;i++)
    {
        factorial[i]=factorial[i-1]*i;
        factorial[i]%=1000000007;
    }
    scanf("%lld",&test);
    while(test--)
    {
        ll sum=1;
        scanf("%lld",&n);
        vector<ll>counter(1e6+1,0);
        vector<ll>numbers;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            if(counter[j]==0)
            {
                numbers.push_back(j);
            }
            counter[j]++;
//            cout<<"counter of j="<<j<<" "<<counter[j]<<endl;
        }
        sort(numbers.begin(),numbers.end());
        for(i=0;i<numbers.size();i++)
        {
            cout<<"i="<<i<<" num="<<numbers[i]<<" counter="<<counter[numbers[i]]<<endl;
            ll cur=numbers[i],m,l;
            if(counter[cur]%2==0 && counter[cur]>0)
            {
               cout<<"even count of "<<cur<<" ="<<counter[cur]<<endl;
                l=counter[cur];
                sum*=factorial[l-1]%1000000007;
                sum%=1000000007;
            }
            else if(counter[cur]%2==1)
            {
                cout<<"odd count of "<<cur<<" ="<<counter[cur]<<endl;
                l=counter[cur]-1;
                cout<<"even l="<<l<<endl;
                if(l>0){
                sum*=factorial[l]%1000000007;
                sum%=1000000007;
                }
                cout<<"nxt_number="<<numbers[i+1]<<endl;
                sum*=counter[numbers[i+1]];
                sum%=1000000007;
                counter[numbers[i+1]]--;
                cout<<"sum="<<sum<<endl;
            }
        }
        printf("%lld\n",sum);
    }
}
