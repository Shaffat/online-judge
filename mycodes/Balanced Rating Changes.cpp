
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())using namespace std;

int main()
{
    int n,i,j;
    vi posodd,poseven;
    vi negodd, negeven;
    vi a;
    cin>>n;
    FOR(i,1,n)
    {
        cin>>j;
        if(j%2==0)
        {
            if(j>=0)
                poseven.push_back(j);
            else
                negeven.push_back(j);
        }
        else
        {
            if(j>=0)
                posodd.push_back(j);
            else
                negodd.push_back(j);
        }
        a.push_back(j);
    }
    //cout<<"here"<<endl;
    if(posodd.size()>negodd.size())
    {
        int cut=(posodd.size()-negodd.size())/2;
        FOR(i,0,n-1)
        {
            if(a[i]%2)
            {
                if(cut && a[i]>0)
                {
                    a[i]=(a[i]/2)+1;
                    cut--;
                }
                else
                {
                    a[i]=a[i]/2;
                }
            }
        }
    }
    else
    {
        //cout<<"here1"<<endl;
        int cut=(negodd.size()-posodd.size())/2;
        //cout<<"cut="<<cut<<endl;
        FOR(i,0,n-1)
        {
           // cout<<"at i="<<i<<" cut="<<cut<<endl;
            if(a[i]%2)
            {
                if(cut && a[i]<0)
                {
                    a[i]=(a[i]/2)-1;
                    cut--;
                }
                else
                {
                    a[i]=a[i]/2;
                }
            }
            else{
               //cout<<"even"<<endl;
                a[i]=a[i]/2;
            }
            //cout<<"ok1"<<endl;
        }
    }
    //cout<<"ok"<<endl;
    FOR(i,0,n-1)
    {
        cout<<a[i]<<endl;
    }

}
