
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

ll counter(ll pos,ll n,vl &v)
{
    ll first,last,mid,d=0,res=pos;
    first=pos+1;
    last=n-1;
    while(first<=last)
    {
        if(d) break;
        if(first==last)
            d=1;
        mid=(first+last)/2;
        if(v[mid]-v[pos]<32)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    //cout<<"for pos="<<pos<<" res="<<res<<endl;
    return res;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,n,i,j;
    cin>>test;
    while(test--)
    {
        cin>>n;
        vl v;
        ll res=0;
        for(i=1;i<=n;i++)
        {
            cin>>j;
            v.push_back(j);
        }
        SORT(v);
        for(i=0;i<v.size();i++)
        {
            res+=counter(i,n,v)-i;
        }
        cout<<res<<endl;

    }
}
