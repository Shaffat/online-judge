#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t,day=0,duplicates=0,preh,prem,h,m,f=0;
    cin>>test;
    getchar();
    for(t=1;t<=test;t++)
    {
        string s;
        getline(cin,s);
        //cout<<"s="<<s<<endl;
        if(!f)
        {
            f=1;
            day=1;
            preh=(s[1]-'0')*10+s[2]-'0';
            //cout<<"preh="<<preh<<endl;
            if(preh==12)
            {
                preh=0;

            }
            if(s[7]=='p')
            {
                //cout<<"pm"<<endl;
                preh+=12;
            }
            prem=(s[4]-'0')*10+s[5]-'0';
            duplicates=1;
            //cout<<"preh="<<preh<<" m="<<prem<<endl;
        }
        else
        {
            h=(s[1]-'0')*10+s[2]-'0';
            if(h==12)
            {
                //cout<<"12 am"<<endl;
                h=0;
                //cout<<"h="<<h<<endl;
            }
            if(s[7]=='p')
            {
                h+=12;
            }
            m=(s[4]-'0')*10+s[5]-'0';
            //cout<<"h="<<h<<" m="<<m<<" p_h="<<preh<<" m="<<prem<<endl;
            if(h>preh|| h==preh && m>prem)
            {
                duplicates=1;
                preh=h;
                prem=m;
            }
            else if(h==preh && m==prem)
            {
                duplicates++;
                if(duplicates>10)
                {
                    day++;
                    duplicates=1;
                    //cout<<"day passed"<<endl;
                }
            }
            else
            {
                preh=h;
                prem=m;
                duplicates=1;
                day++;
                //cout<<"day increased"<<endl;
            }
        }
    }
    cout<<day<<endl;
}
