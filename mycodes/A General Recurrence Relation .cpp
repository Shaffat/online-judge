#include<bits/stdc++.h>

using namespace std;

#define ll long long int

void multiplication(vector<vector<double> >&a,vector<vector<double> >&b,vector<vector<double> >&res)
{
    int i,j,k;
    for(i=0;i<a.size();i++)
    {
        for(j=0;j<b[0].size();j++)
        {
            double sum=0;
            for(k=0;k<b.size();k++)
            {
                sum+=(a[i][k]*b[k][j]);
            }
            res[i][j]=sum;
        }
    }
    return ;
}

void power(int p,vector<vector<double> >&base,vector<vector<double> >&res)
{
    int i,j;

    if(p==1)
    {
        for(i=0;i<base.size();i++)
        {
            for(j=0;j<base[0].size();j++)
            {
                res[i][j]=base[i][j];
            }
        }
        return;
    }
    else if(p%2==0)
    {
        power(p/2,base,res);
        vector<double>col(2);
        vector<vector<double> >tmp(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,res);
    }
    else
    {
        power(p/2,base,res);
        vector<double>col(2);
        vector<vector<double> >tmp(2,col);
        vector<vector<double> >tmp2(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,tmp2);
        multiplication(tmp2,base,res);
    }
    return;
}



double solve(int n,double f0,double f1,double a,double b)
{
    if(n==0)
    {
        return f0;
    }
    else if(n==1)
    {
        return f1;
    }
    else
    {
        vector<double>col(1);
        vector<vector<double> >matrix(2,col);
        vector<vector<double> >final_res(2,col);
        matrix[0][0]= f1;
        matrix[1][0]= f0;
        vector<double>col1(2);
        vector<vector<double> >base(2,col1);
        vector<vector<double> >res(2,col1);
        base[0][0]=a;
        base[0][1]=b;
        base[1][0]=1;
        base[1][1]=0;
        power(n-1,base,res);
        multiplication(res,matrix,final_res);
        return final_res[0][0];
    }
}

int main()
{
    int t,test,n,m;
    double a,b,f0,f1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lf %lf %lf %lf %d",&f0,&f1,&a,&b,&n);
        m=2e9;
        double res=solve(n,f0,f1,a,b);
        printf("%.9lf\n",res);
    }
}
