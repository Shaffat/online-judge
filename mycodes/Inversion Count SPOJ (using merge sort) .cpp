#include<bits/stdc++.h>
using namespace std;

long long int conquer(int start,int ending,int mid,vector<long long int>&data)
{
    //cout<<"caliing "<<start<<" "<<ending<<endl;
    vector<long long int>left;
    vector<long long int>right;
    int i,j,n,m,k=start,correct=0;
    long long int inverstion=0;
    for(i=start;i<=mid;i++)
    {
        left.push_back(data[i]);
    }
    for(i=mid+1;i<=ending;i++)
    {
        right.push_back(data[i]);
    }
    n=left.size();
    m=right.size();
    i=0;
    j=0;
    correct=0;
    while(i<n  && j<m)
    {
        if(right[j]<left[i])
        {
            //cout<<"j="<<j<<" correct="<<correct<<endl;
            inverstion+=max(j-correct+n,0);
            data[k]=right[j];
            j++;k++;
        }
        else
        {
            data[k]=left[i];
            i++;k++;
        }
        correct++;
    }
    while(i<n)
    {
        data[k]=left[i];
        k++;
        i++;
    }
    while(j<m)
    {
        data[k]=right[j];
        j++;
        k++;
    }
    //cout<<"for range "<<start<<" and "<<ending<<" inverstion="<<inverstion<<endl;
    return inverstion;
}
long long int divide(int start,int ending,vector<long long int>&data)
{
  //cout<<"caliing divide "<<start<<" "<<ending<<endl;
    if(start>=ending)
    {
        return 0;
    }
    else
    {
        int mid=(start+ending)/2;
        long long int res1,res2,res3;
        res1=divide(start,mid,data);
        res2=divide(mid+1,ending,data);
        res3=conquer(start,ending,mid,data);
        return res1+res2+res3;
    }
}

int main()
{
    long long int test,t=1;
    scanf("%lld",&test);
    while(t<=test)
    {
        vector<long long int>data;
        long long int i,j,n;
        scanf("%lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            data.push_back(j);
        }
        long long int res=divide(0,data.size()-1,data);
        printf("%lld\n",res);
        t++;
    }
}
