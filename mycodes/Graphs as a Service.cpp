#include<bits/stdc++.h>
using namespace std;


struct node
{
    int id,cost;
};

struct edge
{
    int u,v,w;
};

bool operator <(edge a, edge b)
{
    if(a.w!=b.w)
        return a.w<b.w;
    return false;
}

int possible,n;

bool newnode(int x,vector<vector<int> >&shotest)
{
    int i;
    for(i=1;i<=n;i++)
    {
        if(shotest[x][i]!=-1)
            return false;
    }
    return true;
}


void solve(int cur,vector<stack<node> >&connections,vector<vector<int> >&shortest,vector<edge>&input,vector<int>&vis)
{
    if(cur>=input.size()){
        possible=1;
        return;
    }
    int x,y,z,i,j;
    x=input[cur].u;
    y=input[cur].v;
    z=input[cur].w;
    if(newnode(x,shortest))
    {
        swap(x,y);
    }
    cout<<"x="<<x<<" y="<<y<<endl;
    if(shortest[x][y]==-1||shortest[x][y]>z)
    {
        cout<<"shotest="<<shortest[x][y]<<" need="<<z<<endl;
        shortest[x][y]=z;
        shortest[y][x]=z;
        cout<<"update ["<<x<<"]["<<y<<"]="<<shortest[x][y]<<endl;
        for(i=1;i<=n;i++)
        {
            if(i==y) continue;
            if(shortest[x][i]!=-1)
            {
                shortest[y][i]=shortest[x][i]+z;
                shortest[i][y]=shortest[y][i];
                cout<<"update "<<i<<" "<<y<<" ="<<shortest[i][y]<<endl;
            }
        }
        cout<<"try to connect "<<x<<" and "<<y<<" wight="<<z<<endl;
        solve(cur+1,connections,shortest,input,vis);
        if(possible)
        {
            vis[x]=1;
            vis[y]=1;
            node tmp;
            tmp.id=y;
            tmp.cost=z;
            connections[x].push(tmp);
            return;
        }
        else
        {
            cout<<"removing connect "<<x<<" and "<<y<<" wight="<<z<<endl;
            for(i=1;i<=n;i++)
            {
                if(i==y) continue;
                if(shortest[x][i]!=-1)
                {
                    shortest[y][i]=shortest[x][i]-z;
                    shortest[i][y]=shortest[y][i];
                }
            }
            shortest[x][y]=-1;
            shortest[y][x]=-1;
        }
    }

    for(i=1;i<=n;i++)
    {
        if(i==x || i==y)
        {
            continue;
        }
        if(shortest[x][i]<z && shortest[x][i]!=-1 && shortest[i][y]==-1)
        {
            int add=z-shortest[x][i];
            for(j=1;j<=n;j++)
            {
                if(shortest[i][j]!=-1)
                {
                    shortest[y][j]=shortest[i][j]+add;
                    shortest[j][y]=shortest[y][j];
                }
            }
            shortest[i][y]=add;
            shortest[y][i]=add;
            cout<<"try to connect "<<i<<" and "<<y<<" wight="<<add<<endl;
            solve(cur+1,connections,shortest,input,vis);
            if(possible)
            {
                node tmp;
                tmp.id=y;
                tmp.cost=add;
                connections[i].push(tmp);
                vis[x]=1;
                vis[y]=1;
                return;
            }
            else
            {
                cout<<"removing connect "<<x<<" and "<<y<<" wight="<<add<<endl;
                for(j=1;j<=n;j++)
                {
                    if(shortest[i][j]!=-1)
                    {
                        shortest[y][j]=shortest[i][j]-add;
                        shortest[j][y]=shortest[y][j];
                    }
                }
                shortest[i][y]=-1;
                shortest[y][i]=-1;
            }
        }
    }
    cout<<"cant set "<<input[cur].u<<" "<<input[cur].v<<" "<<input[cur].w<<endl;
    possible=0;
    return;

}


void extra(vector<stack<node> >&connections,vector<int>&vis)
{
    int i,j,x;
    for(i=1;i<=n;i++)
    {
        if(vis[i]==1)
        {
            x=i;
            break;
        }
    }
    for(i=1;i<=n;i++)
    {
        if(vis[i]==0)
        {
            node tmp;
            tmp.id=x;
            tmp.cost=23;
            connections[i].push(tmp);
        }
    }

    return;
}

int main()
{
    int test,t,m,i,j,x,y,z;
    scanf("%d",&test);
    for(t=1;t<=test;t++){
        scanf("%d %d",&n,&m);
        possible=0;
        vector<edge>v;
        for(i=1;i<=m;i++)
        {
            edge tmp;
            scanf("%d %d %d",&tmp.u,&tmp.v,&tmp.w);
            v.push_back(tmp);
        }
        sort(v.begin(),v.end());
        vector<stack<node> >connections(n+1);
        vector<int>col(n+1,-1);
        vector<vector<int> >shortest(n+1,col);
        vector<int>vis(n+1,0);
        solve(0,connections,shortest,v,vis);
        if(possible)
        {
            extra(connections,vis);
            int total=0;
            for(i=1;i<=n;i++)
            {
                total+=connections[i].size();
            }
            printf("Case #%d: %d\n",t,total);
            for(i=1;i<=n;i++)
            {
                while(!connections[i].empty())
                {
                    node tmp;
                    tmp=connections[i].top();
                    connections[i].pop();
                    printf("%d %d %d\n",i,tmp.id,tmp.cost);
                }
            }
        }
        else
            printf("Case #%d: Impossible\n",t);
    }
}
