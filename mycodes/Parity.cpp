#include<bits/stdc++.h>
using namespace std;

int parity(int n)
{
    int i,j=0;
    for(i=0;i<=log2(n);i++)
    {
        if((n&(1<<i))==(1<<i))
        {
            j++;
        }
    }
    return j;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,res;
        scanf("%d",&n);
        res=parity(n);
        if(res%2==0)
        {
            printf("Case %d: even\n",t);
        }
        else
            printf("Case %d: odd\n",t);

    }
}
