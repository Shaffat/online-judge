
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())using namespace std;

struct bk
{
    ll h,w;
};



bool ok(bk a,bk b)
{
    if(a.w>b.w && a.h>b.h)
        return true;
    return false;
}

ll solve(ll pos, vector<bk>&books, vl &memory)
{

    if(memory[pos]!=-1)
        return memory[pos];
    ll res=0,mx,i;
    for(i=0;i<books.size();i++)
    {
        //cout<<"trying to take "<<i<<" after taking "<<pos<<" h="<<books[i].h<<","<<books[i].w<<" and "<<books[pos].h<<","<<books[pos].w<<endl;
        bk tmp;
        tmp.h=books[i].w;
        tmp.w=books[i].h;
        if(ok(books[i],books[pos])|| ok(tmp,books[pos]))
        {
            //cout<<"after "<<pos<<" can take "<<i<<endl;
            res=max(res,solve(i,books,memory)+1);
        }
    }
    //cout<<"memory["<<pos<<"]="<<res<<endl;
    return memory[pos]=res;
}

int main()
{
    ll n,i,j,h,w,res=0;
    sc1(n);
    vector<bk>v;
    FOR(i,1,n)
    {
        sc2(h,w);
        bk tmp;
        tmp.h=h;
        tmp.w=w;
        v.push_back(tmp);
    }
    vl memory(n,-1);
    FOR(i,0,n-1)
    {
        res=max(res,solve(i,v,memory)+1);
    }
    printf("%lld\n",res);
}
