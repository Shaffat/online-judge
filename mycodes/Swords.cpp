#include<bits/stdc++.h>
using namespace std;



int main()
{
    long long int  n,i,j,mx=-1,gcd=-1,total=0,diff=0,lcm=-1,z=-1;
    scanf("%lld",&n);
    vector<int>v;
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        v.push_back(j);
        mx=max(mx,j);
        if(j>0)
        {
            if(gcd==-1)
            {
                gcd=j;
                lcm=j;
                z=j;
            }
            else{
                z=__gcd(z,j);
                gcd=__gcd(lcm,j);
                lcm=(lcm*j)/gcd;
            }

        }
    }
    //cout<<"lcm="<<lcm<<endl;
    for(i=0;i<v.size();i++)
    {
        total+=(lcm-v[i])/z;
    }
    if(n==2)
    {
        //cout<<"mx="<<mx<<endl;
        diff=max(diff,mx-v[0]);
        diff=max(diff,mx-v[1]);
        printf("1 %lld\n",diff);
    }
    else
    printf("%lld %lld",total,z);
}
