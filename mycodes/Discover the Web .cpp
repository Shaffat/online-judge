#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1,i,j;
    scanf("%d",&test);
    while(t<=test)
    {
        printf("Case %d:\n",t);
        string s,current="";
        stack<string>backward;
        stack<string>forwards;
        current="http://www.lightoj.com/";
        while(getline(cin,s))
        {
            if(s=="")
            {
                continue;
            }
            if(s=="QUIT")
            {
                break;
            }
            else if(s[0]=='V')
            {
                s.erase(s.begin()+0,s.begin()+6);
                while(!forwards.empty())
                {
                    forwards.pop();
                }
                backward.push(current);
                current=s;
                cout<<s<<endl;
            }
            else if(s=="BACK")
            {
                if(backward.empty())
                {
                    printf("Ignored\n");
                }
                else
                {
                    forwards.push(current);
                    current=backward.top();
                    backward.pop();
                    cout<<current<<endl;
                }
            }
            else
            {
                if(forwards.empty())
                {
                    printf("Ignored\n");
                }
                else
                {
                    string tmp;
                    backward.push(current);
                    current=forwards.top();
                    forwards.pop();
                    cout<<current<<endl;
                }
            }
        }
        t++;

    }
}
