#include<bits/stdc++.h>

using namespace std;
vector<int>start(10001);
vector<int>low(10001);
vector<int>vis(10001,0);
vector<int>component_vis(10001,0);
int timer,t;

void remove_cut_edge(int node,int parent,vector<vector<int> >&connection,vector<vector<int> >&newconnection,vector<int>&degree)
{
   int i,j,counter=0,child;
   start[node]=low[node]=timer;
   timer++;
   for(i=0;i<connection[node].size();i++)
   {
       child=connection[node][i];
       if(child==parent)
       {
           continue;
       }
       if(vis[child]!=t)
       {
           vis[child]=t;
           remove_cut_edge(child,node,connection,newconnection,degree);
           if(start[node]>=low[child])
           {
               //newconnection elimainates cut edges
               newconnection[node].push_back(child);

           }
           else
           {
               //cut_edge
               //how many cut edges ar connection to nodes
               degree[node]++;
               degree[child]++;
           }
           low[node]=min(low[node],low[child]);
       }
       else
       {
           newconnection[node].push_back(child);
           low[node]=min(low[node],low[child]);
       }

   }
   if(start[parent]>=low[node] && node!=parent)
   {
       //backward insert for 2-3 edge now inserting 3-2
       newconnection[node].push_back(parent);
   }
   return;
}

void component_finder(int node,int root,vector<vector<int> >&newconnection,vector<int>&parent)
{
    int i,j,child;
    for(i=0;i<newconnection[node].size();i++)
    {
        child=newconnection[node][i];
        if(component_vis[child]!=t)
        {
            component_vis[child]=t;
            parent[child]=root;
            component_finder(child,root,newconnection,parent);
        }
    }
    return;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,i,j,n,m,u,v;
    scanf("%d",&test);
    t=1;
    while(t<=test)
    {
        timer=1;
        scanf("%d %d",&n,&m);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >new_connection(n+1);
        vector<int>degree(n+1,0);
        vector<int>parent(n+1);
        vector<int>degree_of_components(n+1,0);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        vis[0]=t;
        remove_cut_edge(0,0,connection,new_connection,degree);
        for(i=0;i<n;i++)
        {
           if(component_vis[i]!=t)
           {
               component_vis[i]=t;
               parent[i]=i;
               component_finder(i,i,new_connection,parent);
           }
        }

        for(i=0;i<n;i++)
        {
            int bap=parent[i];
            degree_of_components[bap]+=degree[i];
        }
        double single_degree=0;
        for(i=0;i<n;i++)
        {
            if(degree_of_components[i]==1)
            {
                single_degree++;
            }
        }

        int res=ceil(single_degree/2);
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
