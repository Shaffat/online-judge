#include<bits/stdc++.h>
using namespace std;

int main()

{
    long long int test,t=1,i,j,k,u,v,w,n,maxnode,mx;
    cin>>test;
    while(t<=test)
    {

        cin>>n;

        vector<long long int>nodes[n+10];
        vector<long long int>edge[n+10];
        long long int vis[n+10];
        long long int dis[n+10];


        for(i=1;i<n;i++)
        {
            scanf("%lld %lld %lld",&u,&v,&w);
            nodes[u].push_back(v);
            edge[u].push_back(w);
            nodes[v].push_back(u);
            edge[v].push_back(w);
        }
      vector<int>vis1(40000),vis2(40000);
      long long int dis1[n+10],dis2[n+10];

      queue<long long int>q;
      q.push(u);
      vis1[u]=1;
      dis1[u]=0;

      while(!q.empty())
      {
          long long current=q.front();
          q.pop();
          for(i=0;i<nodes[current].size();i++)
          {
              if(!vis1[nodes[current].at(i)])
              {
                  q.push(nodes[current].at(i));
                  vis1[nodes[current].at(i)]=1;
                  dis1[nodes[current].at(i)]=dis1[current]+edge[current].at(i);
              }
          }
      }
      mx=0;
      for(i=0;i<n;i++)
      {
          if(mx<=dis1[i])
          {
              maxnode=i;
          }
      }
      q.push(maxnode);
      vis2[maxnode]=1;
      dis2[maxnode]=0;
      while(!q.empty())
      {
          long long current=q.front();
          q.pop();
          for(i=0;i<nodes[current].size();i++)
          {
              if(!vis2[nodes[current].at(i)])
              {
                  q.push(nodes[current].at(i));
                  vis2[nodes[current].at(i)]=1;
                  dis2[nodes[current].at(i)]=dis2[current]+edge[current].at(i);
              }
          }
      }
      mx=0;
      for(i=0;i<n;i++)
      {
          if(mx<=dis2[i])
          {
              mx=dis2[i];
          }
      }
      printf("Case %lld: %lld\n",t,mx);
      t++;

    }
}
