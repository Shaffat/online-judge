#include<bits/stdc++.h>
using namespace std;

int is_valid(int x,int y)
{
    if(x<=0 || x>8 || y<=0 || y>8 )
    {
        return false;
    }
    return true;
}

int main()
{
    int test,t=1,r,c,nxt_r,nxt_c;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int counter=0;
        scanf("%d %d",&r,&c);
        nxt_r=r-1;
        nxt_c=c-2;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r-1;
        nxt_c=c+2;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r-2;
        nxt_c=c-1;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r-2;
        nxt_c=c+1;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r+1;
        nxt_c=c-2;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r+1;
        nxt_c=c+2;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r+2;
        nxt_c=c-1;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        nxt_r=r+2;
        nxt_c=c+1;
        if(is_valid(nxt_r,nxt_c))
        {
            counter++;
        }
        printf("Case %d: %d\n",t,counter);
    }
}
