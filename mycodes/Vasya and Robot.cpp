#include<bits/stdc++.h>
using namespace std;

struct point
{
    int l,r,u,d;
};


int solve(int n,int l,int r,int u,int d,vector<point>&total)
{
    int first=0,last=n,mid,res=-1,i,j,f=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        int chk=0;
        for(i=mid;i<=n;i++)
        {
            point tmp;
            tmp.d=total[i].d-total[i-mid].d;
            tmp.u=total[i].u-total[i-mid].u;
            tmp.l=total[i].l-total[i-mid].l;
            tmp.r=total[i].r-total[i-mid].r;
            if(tmp.l>=l && tmp.r>=r && tmp.u>=u && tmp.d>=d)
            {
                chk=1;
                break;
            }
        }
        if(chk)
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}


int main()
{
    int n,i,j,d_x,d_y,c_x,c_y,need_l=0,need_r=0,need_u=0,need_d=0;
    string s;
    cin>>n>>s;
    cin>>d_x>>d_y;
    point tmp;
    tmp.l=0;
    tmp.r=0;
    tmp.d=0;
    tmp.u=0;
    c_x=0;
    c_y=0;
    vector<point>total(n+2);
    total[0]=tmp;
    for(i=0;i<s.size();i++)
    {
        total[i+1]=total[i];
        if(s[i]=='R')
        {
            c_x++;
            total[i+1].r++;
        }
        if(s[i]=='L')
        {
            c_x--;
            total[i+1].l++;
        }
        if(s[i]=='U')
        {
            c_y++;
            total[i+1].u++;
        }
        if(s[i]=='D')
        {
            c_y--;
            total[i+1].d++;
        }
    }
    int dif_x=d_x-c_x,dif_y=d_y-c_y;



    if(c_x<d_x)
    {
        need_r=(d_x-c_x)/2;
    }
    else
    {
        need_r=(c_x-d_x)/2;
    }
    if(c_y<d_y)
    {
        need_u=(d_y-c_y)/2;
    }
    else
    {
        need_d=(c_y-d_y)/2;
    }
    cout<<"cur_x="<<c_x<<" c_y="<<c_y<<endl;
    cout<<solve(n,need_l,need_r,need_u,need_d,total);
}
