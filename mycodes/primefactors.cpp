#include<bits/stdc++.h>

using namespace std;

bool ar[1000010];

void sieve()

{
    int i,N=1000000;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}


int main()
{
    sieve();
    int i,j,k,l,n,temp;
    while(scanf("%d",&n))
    {
        temp=n;
        if(n==0)
        {
            break;
        }
        vector<int>factors;
        if(n<0)
        {
            factors.push_back(-1);

            n*=-1;
        }

        while(n>1)
        {

            int chk=0;
            for(i=2;i<=sqrt(n);i++)
            {
                if(ar[i]==0)
                {
                    if(n%i==0)
                    {
                        chk=1;
                        break;

                    }

                }
            }
            if(chk==1)
            {
                factors.push_back(i);

                n/=i;
            }
            else
            {

                factors.push_back(n);
                n=1;
            }

        }

        printf("%d =",temp);

        for(i=0;i<factors.size();i++)
        {
            if(i==factors.size()-1)
            {
                printf(" %d\n",factors.at(i));
            }
            else
            {
                printf(" %d x",factors.at(i));
            }
        }


    }

}
