#include<bits/stdc++.h>
using namespace std;

map<char,int>weight;

int calculate(string &s)
{
    int total=0,i;
    for(i=0;i<s.size();i++)
    {
        total+=weight[s[i]];
    }
    return total;
}

int solve(string &s)
{
    //cout<<s<<endl;
    string tmp="",nongroup="";
    int i,j,total=0,first=0;
    stack<char>stk;
    vector<string>groups;
    vector<int>counter;
    for(i=0;i<s.size();i++)
    {

        if(s[i]=='(')
        {
            if(first==1)
                tmp.push_back(s[i]);
            stk.push(s[i]);
            first=1;
        }
        else if(s[i]==')')
        {

            stk.pop();
            if(stk.empty())
            {
                groups.push_back(tmp);
                int cur=1;
                if(i+1<s.size())
                {
                    if(s[i+1]>='2' && s[i+1]<='9')
                    {
                        cur=s[i+1]-'0';
                        i++;
                    }

                }
                total+=solve(tmp)*cur;
                //cout<<tmp<<" found "<<cur<<" times"<<endl;
                tmp="";
                first=0;
            }
            else
                tmp.push_back(s[i]);

        }
        else
        {
            if(stk.empty())
            {
//                cout<<s[i]<<" is stack empty and nongroup of "<<s<<endl;
//                cout<<"before total="<<total<<endl;
                if(i+1<s.size())
                {
                    if(s[i+1]>='2' && s[i+1]<='9')
                    {
                        //cout<<"char is "<<s[i]<<" occured "<<s[i+1]-'0'<<"times\n";
                        total+=weight[s[i]]*(s[i+1]-'0');
                        i++;
                    }
                    else
                        total+=weight[s[i]];

                }
                else
                {
                    //cout<<"char is "<<s[i]<<" occured one time"<<endl;
                    total+=weight[s[i]];
                }
                //cout<<"after total="<<total<<endl;
            }
            else
            {
                tmp.push_back(s[i]);
            }
        }
    }
    //cout<<"value of "<<s<<"="<<total<<endl;
    return total;
}



int main()
{
    weight['C']=12;
    weight['H']=1;
    weight['O']=16;
    string s,nongroup;
    cin>>s;
    int res=solve(s);
    cout<<res<<endl;
}
