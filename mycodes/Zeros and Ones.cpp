#include<bits/stdc++.h>

using namespace std;

void preprocess(string &s,vector<int>&sum)
{
    int i,j;
    for(i=0;i<s.size();i++)
    {
        sum[i+1]=s[i]-'0'+sum[i];
    }
    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    string s;
    int i,j,res,u,v,q,t=1;
    while(cin>>s)
    {
        cout<<"Case "<<t<<":"<<endl;
        t++;
        vector<int>sum(s.size()+10,0);
        preprocess(s,sum);
        cin>>q;
        while(q--)
        {
            cin>>u>>v;
            if(u>v) swap(u,v);

            v++;
            res=sum[v]-sum[u];
            //cout<<"res="<<res<<endl;
            if(res==0 || res==(v-u)){
                cout<<"Yes"<<endl;
            }

            else
                cout<<"No"<<endl;
        }
    }
}
