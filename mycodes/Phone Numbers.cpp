#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,k,i,j,last=-1,letter,lowest=2e9;
    string s,res;
    cin>>n>>k;
    cin>>s;
    vector<bool>mark(1000,0);
    for(i=0;i<s.size();i++)
    {
        int c=s[i];
        mark[c]=1;
        lowest=min(lowest,c);
    }
    if(k>n)
    {
        res=s;
        while(res.size()<k)
        {
            res.push_back(lowest);
        }
        cout<<res<<endl;
        return 0;
    }
    for(i=min(n-1,k-1);i>=0;i--)
    {
        int c=s[i],chk=0;
        for(j=s[i]+1;j<='z';j++)
        {
            if(mark[j]==1)
            {
                last=i;
                letter=j;
                chk=1;
                break;
            }
        }
        if(chk)
        {
            break;
        }
    }
    for(i=0;i<last;i++)
    {
        res.push_back(s[i]);
    }
    res.push_back(letter);

    while(res.size()<k)
    {
        res.push_back(lowest);
    }
    cout<<res<<endl;
}
