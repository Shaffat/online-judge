#include<bits/stdc++.h>

using namespace std;

void sieve(int N,int *ar)

{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}

int main()
{
    string s1,s2;
    cin>>s1>>s2;
    if(s1==s2)
    {
        cout<<"-1"<<endl;
    }
    else
    {
        cout<<max(s1.size(),s2.size())<<endl;
    }
}
