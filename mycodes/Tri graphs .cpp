#include<bits/stdc++.h>

typedef long long int ll;
using namespace std;

ll matrix[100001][4];
ll memory[100001][4];
int vis[100001][4];
bool isvalid(int row,int col,int r)
{
    if(row>r ||row<1||col>3 || col <1)
    {
        return false;
    }
    return true;
}

ll solve(int row,int c,int r)
{
    //cout<<row<<" "<<c<<endl;
    if(row==r && c==2)
    {
        return memory[row][c]=matrix[r][2];
    }
    if(!isvalid(row,c,r))
    {
        return 2e10;
    }
    if(vis[row][c]!=0)
    {
        return memory[row][c];
    }
    ll case1,case2,case3,case4;
    case1=solve(row,c+1,r);
    case2=solve(row+1,c-1,r);
    case3=solve(row+1,c,r);
    case4=solve(row+1,c+1,r);
    vis[row][c]=1;
    return memory[row][c]=min(case1,min(case2,min(case3,case4)))+matrix[row][c];
}

int main()
{
    int r,t=1;
    while(scanf("%d",&r))
    {
        if(r==0)
        {
            break;
        }
        int i,j;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=3;j++)
            {
                scanf("%lld",&matrix[i][j]);
                //matrix[i][j]=1;
            }
        }
        memset(vis,0,sizeof(vis));
        ll result=solve(1,2,r);
        printf("%d. %lld\n",t,result);
        t++;
    }

}
