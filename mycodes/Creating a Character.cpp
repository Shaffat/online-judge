#include<bits/stdc++.h>
using namespace std;

int main()
{
    int str,in,exp,t,res=0;
    cin>>t;
    while(t--)
    {
        res=0;
        cin>>str>>in>>exp;
        if(str+exp>in){
            if(str>in){
                //cout<<"more str"<<endl;
                res=(exp/2) +((str-in)/2);
                if(exp==0) res=1;
            }
            else{
                exp=exp-((in-str)+1);
                res=(exp/2) +1;
            }
        }
        cout<<res<<endl;
    }
}
