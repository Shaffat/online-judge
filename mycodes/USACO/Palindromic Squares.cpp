/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

#include<bits/stdc++.h>
using namespace std;

string base_convert(int number,int base)
{
    string res="";
    while(number>=base)
    {
        int r,cur;
        r=number%base;
        number/=base;
        //cout<<"number="<<number<<"r="<<r<<endl;
        if(r<10)
        {
            res.insert(res.begin(),'0'+r);
        }
        else
        {
            res.insert(res.begin(),'A'+r-10);
        }
    }
    if(number>0)
    {
        int cur=number;
        if(cur<10)
        {
            res.insert(res.begin(),'0'+cur);
        }
        else
        {
            res.insert(res.begin(),'A'+cur-10);
        }
    }
    //cout<<"conversion is "<<res<<endl;
    return res;
}

bool is_palin(string &s)
{
    int i=0,j=s.size()-1;
    while(i<=j)
    {
        //cout<<"i="<<i<<" j="<<j<<" and "<<s[i]<<" and "<<s[j]<<endl;
        if(s[i]!=s[j]) return false;
        i++;
        j--;
    }
    //cout<<"i am good"<<endl;
    return true;
}

int main()
{
    freopen("palsquare.in","r",stdin);
    freopen("palsquare.out","w",stdout);
//cout<<"120 convert="<<base_convert(120*120,11);
    int base,i,j;
    scanf("%d",&base);
    string s;
    for(i=1;i<=300;i++)
    {
        s=base_convert(i*i,base);
        //cout<<"i="<<i<<" s="<<s<<endl;
        if(is_palin(s))
            cout<<base_convert(i,base)<<" "<<s<<endl;
    }

    return 0;

}
