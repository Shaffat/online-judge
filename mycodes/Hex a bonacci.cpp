#include<bits/stdc++.h>

using namespace std;
int a,b,c,d,e,f;

unsigned long long int solve(int n,vector< unsigned long long int >&memory)
{
    if(n<0)
    {
        return 0;
    }
    if(n==0)
    {
        return a;
    }
    if(n==1)
    {
        return b;

    }
    if(n==2)
    {
        return c;
    }
    if(n==3)
    {
        return d;
    }
    if(n==4)
    {
        return e;
    }
    if(n==5)
    {
        return f;
    }
    if(memory[n]!=-1)
    {
        return memory[n];
    }
    else
    {
        memory[n]=(solve(n-1,memory)%10000007+solve(n-2,memory)%10000007+solve(n-3,memory)%10000007+solve(n-4,memory)%10000007+solve(n-5,memory)%10000007+solve(n-6,memory)%10000007)%10000007;

        return memory[n];
    }
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n;
        vector<unsigned long long int>memory(100001,-1);
        scanf("%d %d %d %d %d %d %d",&a,&b,&c,&d,&e,&f,&n);
        printf("Case %d: %llu\n",t,solve(n,memory)%10000007);

        t++;

    }
}
