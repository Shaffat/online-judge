#include<bits/stdc++.h>

using namespace std;

#define ll long long int
int main()
{
    ll l,r,add=0,res=0,mid,i,j;
    set<ll>s;
    cin>>l>>r;
    if(l>r) swap(l,r);
    ll cycle[24]={4, 4, 7, 7, 1, 1, 8, 8, 9, 9, 7, 7, 6, 6, 3, 3, 9, 9, 2, 2, 1, 1, 3, 3};
    ll cyclewithPrefix[27]={1, 2, 3, 4, 4, 7, 7, 1, 1, 8, 8, 9, 9, 7, 7, 6, 6, 3, 3, 9, 9, 2, 2, 1, 1, 3, 3};
    if(l<3)
    {
        for(i=l;i<=min(26ll,r);i++)
        {
            s.insert(cyclewithPrefix[i]);
        }
    }
    else
    {
        l-=3;
        r-=3;
        for(i=l;i<=min(l+23,r);i++)
        {
            s.insert(cycle[i%24]);
        }
    }
    cout<<s.size()<<endl;
}
