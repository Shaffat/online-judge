#include<bits/stdc++.h>
using namespace std;
#define ll long long int
int main()
{
    ll n,m,i,j,test,t;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        ll total=0;
       vector<ll>mods(1e5,0);
       scanf("%lld %lld",&n,&m);
       for(i=1;i<=n;i++)
       {
           scanf("%lld",&j);
           total+=j;
           total%=m;
           mods[total]++;

       }
       ll res=mods[0];
       for(i=0;i<m;i++)
       {
           if(mods[i]>0)
           {
               res+=(mods[i]*(mods[i]-1))/2;
           }
       }
       printf("Case %lld: %lld\n",t,res);
    }

}
