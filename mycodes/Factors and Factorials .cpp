#include<bits/stdc++.h>

using namespace std;

vector<int>primes;
vector<bool>ar(100,0);
vector<int>v;
vector<vector<int> >result(101,v);
void sieve()
{
    int i,j;
    for(i=4;i<100;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<10;i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<100;j+=i)
            {
                ar[j]=1;
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<100;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}

void solve()
{
    int i,j;
    for(i=2;i<=100;i++)
    {
        int previous_last=result[i-1].size()-1;
        //cout<<"       ****       i="<<i<<endl;
        int num=i;
        for(j=0;j<primes.size();j++)
        {
            int count=0;
            int p=primes[j];
            //cout<<"prime ="<<p<<" num="<<num<<endl;
            if(num==1 && j>=result[i-1].size())
            {
                break;
            }
            while(num%p==0)
            {
                num/=p;
                count++;
            }
            if(j>previous_last)
            {
                //cout<<"here"<<endl;
                result[i].push_back(count);
            }
            else
                result[i].push_back(count+result[i-1][j]);
        }

    }
}

int main()
{
    sieve();
    solve();
    int n;
    while(scanf("%d",&n))
    {
        if(n==0)
        {
            break;
        }
        if(n/10>=10)
        {
            printf("%d! = ",n);
        }
        else if(n/10>0)
        {
            printf(" %d! = ",n);
        }
        else
            printf("  %d! = ",n);
        int chk=0;
        for(int i=0;i<result[n].size();i++)
        {
            if(i==15)
            {
                printf("\n      ");
            }
            if(chk)
            {
                printf(" ");
            }
            int tmp=result[n][i];
            if(tmp/10>0)
            {
                printf("%d",tmp);
            }
            else
                printf(" %d",tmp);
                chk++;
        }
        printf("\n");
    }
}
