#include<bits/stdc++.h>
using namespace std;
int main()
{
    priority_queue<int , vector<int>, greater<int> >usb,ps;

    int a,b,c,m;
    cin>>a>>b>>c>>m;
    for(int i=1;i<=m;i++)
    {
        int k;
        string s;
        cin>>k>>s;
        if(s=="USB")
        {
            usb.push(k);
        }
        else ps.push(k);
    }
    long long cost=0,count=0;
    while(a>0 && !usb.empty())
    {
        cost+=usb.top();
        count++;
        usb.pop();
        a--;
    }

    while(b>0 && !ps.empty())
    {
        cost+=ps.top();
        count++;
        ps.pop();
        b--;
    }
    while(c>0 &&(!ps.empty()||!usb.empty()))
    {
        if(!ps.empty() && !usb.empty())
        {
            if(ps.top()<usb.top())
            {
                cost+=ps.top();
                count++;
                ps.pop();
            }
            else
            {
                cost+=usb.top();
                count++;
                usb.pop();
            }
        }
       else if(!ps.empty())
        {
            cost+=ps.top();
            count++;
            ps.pop();
        }
        else
        {
            cost+=usb.top();
            count++;
            usb.pop();
        }
        c--;
    }
    cout<<count<<" "<<cost<<endl;
}
