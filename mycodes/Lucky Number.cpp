#include<bits/stdc++.h>
using namespace std;

#define sz 1429431+4
vector<int>tree(sz,0);
vector<bool>lucky(sz,1);
vector<int>numbers;

void update(int index,int val)
{
    while(index<sz)
    {
        tree[index]+=val;
        index+=index&(-index);
    }
    return;
}
int query(int index)
{
    int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index&(-index);
    }
    return sum;
}

int find_index(int target)
{
    int first,last,mid,f=0,killed,alive,res=-1;
    first=1;
    last=sz-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;

        mid=(first+last)/2;
        killed=query(mid);
        alive=mid-killed;
//        cout<<"first="<<first<<" last="<<last<<" mid="<<mid<<endl;
//        cout<<"target="<<target<<" killed "<<killed<<" at "<<mid<<" so alive="<<alive<<endl;
        if(alive>=target)
        {
            if(alive==target)
            {
                res=mid;
            }
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}

void preprocess()
{
    int i,j,chk,cur,idx;
    for(i=2;i<sz;i+=2)
    {
        update(i,1);
        lucky[i]=0;
    }
    for(i=3;i<sz;i+=2)
    {
        if(lucky[i])
        {
            vector<int>killed;
            cur=i;
            chk=1;
            while(chk)
            {
                idx=find_index(cur);
                //cout<<"trying to kill "<<idx<<endl;
                if(idx!=-1)
                {
                    //cout<<"----------------- Killing "<<idx<<"-----------------\n";
                    lucky[idx]=0;
                    killed.push_back(idx);
                    cur+=i;
                }
                else
                    chk=0;
            }
            for(j=0;j<killed.size();j++)
            {
                update(killed[j],1);
            }
        }
    }

    numbers.push_back(1);
    for(i=3;i<sz;i+=2)
    {
        if(lucky[i])
        {
            numbers.push_back(i);
        }
    }
}

int main()
{
    preprocess();
    int test,t,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        n--;
        printf("Case %d: %d\n",t,numbers[n]);
    }
}
