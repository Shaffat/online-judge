#include<bits/stdc++.h>

using namespace std;


struct schedule
{
    int station,start,finish;
};

bool operator <(schedule a, schedule b)
{
    if(a.start!=b.start)
    {
        return a.start<b.start;
    }
    return a.finish<b.finish;
}
void get_time(schedule &train,string &s)
{
    train.start=((s[0]-'0')*10 + s[1]-'0')*60+(s[3]-'0')*10+s[4]-'0';
    train.finish=((s[6]-'0')*10 + s[7]-'0')*60+(s[9]-'0')*10+s[10]-'0';
}



int main()
{

//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int n,t,i,j,a,b;

    cin>>n;
    for(i=1;i<=n;i++)
    {
        scanf("%d %d %d",&t,&a,&b);
        cout<<"t="<<t<<" a="<<a<<" b="<<b<<endl;
        getchar();
        vector<schedule>train;
        string s;
        if(i==1)
        {
            getline(cin,s);
        }
        for(j=1;j<=a;j++)
        {
            getline(cin,s);
            cout<<"j="<<j<<"a_s="<<s<<endl;
            schedule tmp;
            tmp.station=0;
            get_time(tmp,s);
            train.push_back(tmp);
        }
        for(j=1;j<=b;j++)
        {
            getline(cin,s);
            cout<<"b_s="<<s<<endl;
            schedule tmp;
            tmp.station=1;
            get_time(tmp,s);
            train.push_back(tmp);
        }
        int add_a=0,add_b=0;
        sort(train.begin(),train.end());
        vector<bool>vis(1000,0);
        for(j=0;j<train.size();j++)
        {
            if(vis[j]==0)
            {
                cout<<"station"<<train[j].station<<" start="<<train[j].start<<" finish="<<train[j].finish<<endl;
                if(train[j].station==0)
                {
                    add_a++;
                }
                else
                    add_b++;
                vis[j]=1;
                schedule cur;
                cur.station=(train[j].station+1)%2;
                cur.start=train[j].finish+t;
                for(int k=j+1;k<train.size();k++)
                {
                    if(train[k].station==cur.station && train[k].start>=cur.start && vis[k]==0)
                    {
                        cout<<"visited "<<" station="<<train[k].station<<" start="<<train[k].start<<endl;
                        cur.station=(cur.station+1)%2;
                        cur.start=train[k].finish+t;
                        vis[k]=1;
                    }
                }

            }
        }
        printf("Case #%d: %d %d\n",i,add_a,add_b);
    }

}
