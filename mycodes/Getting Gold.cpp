#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int r,c;
};

bool is_valid(pos tmp,int r,int c)
{
    if(tmp.r<0 || tmp.r>=r || tmp.c<0 || tmp.c>=c)
    {
        //cout<<"invalid"<<endl;
        return false;
    }
    //cout<<"valid"<<endl;
    return true;
}

bool moveable(pos cur,int r,int c,vector<string>&maze)
{
    pos nxt;
    nxt.r=cur.r+1;
    nxt.c=cur.c;
    if(is_valid(nxt,r,c))
    {
        if(maze[nxt.r][nxt.c]=='T')
        {
            return false;
        }
    }
    nxt.r=cur.r-1;
    nxt.c=cur.c;
    if(is_valid(nxt,r,c))
    {
        if(maze[nxt.r][nxt.c]=='T')
        {
            return false;
        }
    }
    nxt.r=cur.r;
    nxt.c=cur.c+1;
    if(is_valid(nxt,r,c))
    {
        if(maze[nxt.r][nxt.c]=='T')
        {
            return false;
        }
    }
    nxt.r=cur.r;
    nxt.c=cur.c-1;
    if(is_valid(nxt,r,c))
    {
        if(maze[nxt.r][nxt.c]=='T')
        {
            return false;
        }
    }
    return true;
}
int bfs(pos p,int r,int c,vector<string>&maze)
{
    pos cur;
    vector<int>col(100,0);
    vector<vector<int> >vis(100,col);
    queue<pos>q;
    int gold=0;
    if(moveable(p,r,c,maze)){
    q.push(p);
    }
    vis[p.r][p.c]=1;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        //cout<<"cur="<<cur.r<<" "<<cur.c<<endl;
        pos nxt;
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
        if(is_valid(nxt,r,c))
        {
            if((maze[nxt.r][nxt.c]=='.'||maze[nxt.r][nxt.c]=='G')&&vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                if(maze[nxt.r][nxt.c]=='G')
                {
                    gold++;
                }
                if(moveable(nxt,r,c,maze))
                {
                    //cout<<"moveable"<<endl;
                    q.push(nxt);
                }
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
         //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
        if(is_valid(nxt,r,c))
        {
            if((maze[nxt.r][nxt.c]=='.'||maze[nxt.r][nxt.c]=='G')&&vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                if(maze[nxt.r][nxt.c]=='G')
                {
                    gold++;
                }
                 if(moveable(nxt,r,c,maze))
                {
                    //cout<<"moveable"<<endl;
                    q.push(nxt);
                }
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
         //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
        if(is_valid(nxt,r,c))
        {
            if((maze[nxt.r][nxt.c]=='.'||maze[nxt.r][nxt.c]=='G')&&vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                if(maze[nxt.r][nxt.c]=='G')
                {
                    gold++;
                }
                 if(moveable(nxt,r,c,maze))
                {
                    //cout<<"moveable"<<endl;
                    q.push(nxt);
                }
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
         //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
        if(is_valid(nxt,r,c))
        {
            if((maze[nxt.r][nxt.c]=='.'||maze[nxt.r][nxt.c]=='G')&&vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                if(maze[nxt.r][nxt.c]=='G')
                {
                    gold++;
                }
                 if(moveable(nxt,r,c,maze))
                {
                    //cout<<"moveable"<<endl;
                    q.push(nxt);
                }
            }
        }
    }
    return gold;
}



int main()
{
    int r,c,i,j;
    while(cin>>c>>r){
    string s;
    vector<string>maze;
    pos p;
    for(i=0;i<r;i++)
    {
        cin>>s;
        //cout<<s<<endl;
        for(j=0;j<s.size();j++)
        {
            if(s[j]=='P')
            {
                p.r=i;
                p.c=j;
            }
        }
        maze.push_back(s);
    }
//    for(i=0;i<r;i++)
//    {
//        for(j=0;j<c;j++)
//        {
//            cout<<maze[i][j];
//        }
//        cout<<endl;
//    }
    //cout<<p.r<<" "<<p.c<<endl;
    int res=bfs(p,r,c,maze);
    cout<<res<<endl;

    }
}
