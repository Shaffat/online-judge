
#include <bits/stdc++.h>

using namespace std;

unsigned long long GCD(unsigned long long int a,unsigned long long int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main() {

    unsigned long long int n, a, b, p, q, i, sum = 0, aa, bb, cc, re,k;

    cin >> n >> a >> b >> p >> q;
    aa = n / a;
    bb = n / b;
    cc = aa + bb;
    k= a*b/GCD(a,b);
    //cout<<k<<endl;
    re = n / k;
    //cout<<re<<endl;
    if (p > q) {
        bb -= re;
        //cout<<aa<<" "<<bb<<endl;
        aa *= p;
        bb *= q;

        cout<<aa + bb << endl;
    }
    else {
        aa -= re;
        //cout<<aa<<" "<<bb<<endl;
        aa *= p;
        bb *= q;
        cout<<aa  + bb  << endl;
    }


    return (0);
}
