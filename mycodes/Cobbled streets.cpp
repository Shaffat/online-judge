#include<bits/stdc++.h>

using namespace std;

struct road
{
    int a,b,c;
};
bool operator <(road a,road b)
{
    if(a.c!=b.c)
    {
        return a.c<b.c;
    }
    return false;
}

int find_parent(int r,vector<int>&ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    return ar[r]=find_parent(ar[r],ar);
}

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int p,n,m,i,j;
        scanf("%d %d %d",&p,&n,&m);
        vector<road>roads;
        for(i=1;i<=m;i++)
        {
            int a,b,c;
            scanf("%d %d %d",&a,&b,&c);
            road temp;
            temp.a=a;
            temp.b=b;
            temp.c=c;
            roads.push_back(temp);
        }
        sort(roads.begin(),roads.end());
        int k=0,sum=0;
        vector<int>ranked(n+1,0);
        vector<int>parent(n+1);
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }
        for(i=0;i<roads.size();i++)
        {
            int u,v;
            u=roads[i].a;
            v=roads[i].b;
            int x,y;
            x=find_parent(u,parent);
            y=find_parent(v,parent);
            if(x!=y)
            {
                if(ranked[x]==ranked[y])
                {
                    parent[y]=x;
                    ranked[x]++;
                }
                else if(ranked[x]>ranked[y])
                {
                    parent[y]=x;
                }
                else
                    parent[x]=y;
                sum+=roads[i].c;
                k++;

            }
             if(k==n-1)
                {
                    break;
                }
        }
        printf("%d\n",sum*p);

    }
}
