
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

double solve(ll pos,ll n,ll k,vector<vector<double> >&memory)
{
    //cout<<"pos="<<pos<<" k="<<k<<endl;
    if(pos<=0)
        return 0;
    if(memory[pos][k]!=-1)
        return memory[pos][k];
    ll i,j;
    double x=1;
    FOR(i,1,k-1)
    {
       // cout<<"i="<<i<<endl;
        x+=solve(pos-i,n,k,memory)+1;
        //cout<<"x="<<x<<endl;
    }
    //cout<<"memory["<<pos<<"]["<<k<<"]="<<memory[pos][k]<<endl;
    return memory[pos][k]=x/(k-1);
}

int main()
{
    ll test,t,n,k;
    vector<double>col(11,-1);
    vector<vector<double> >memory(1000001,col);

    sc1(test);
    FOR(t,1,test)
    {
        sc2(n,k);
        double res=solve(n,n,k,memory);
        printf("%.9lf\n",res);
    }
}
