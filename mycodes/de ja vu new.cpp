
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())using namespace std;


vector<ll>v(1000002,0);

int main()
{
    freopen("in11.txt","r",stdin);
    freopen("out11.txt","w",stdout);
    ll test,t,i,j,n,m,r,cur,total,mx=-1;

    scanf("%lld %lld",&n,&r);
    for(i=1; i<=n; i++)
    {
        scanf("%lld",&j);
        v[j]++;
        //cout<<"v["<<j<<"]="<<v[j]<<endl;
        mx=max(j,mx);
    }

    total=0;
    ROF(i,mx,1)
    {
        //cout<<"i="<<i<<endl;
        if(v[i]==0) continue;
        total++;
        //cout<<"monster count="<<v[i]<<endl;
        ROF(j,i-1,max(i-r,0ll))
        {
            ll nxt=j-r;
            //cout<<"moving monster from"<<j<<" to "<<nxt<<endl;
            if(nxt>0)
            {
                v[nxt]+=v[j];
            }
        }
        i-=r;
    }
    printf("%lld\n",total);

}
