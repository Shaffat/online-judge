#include<bits/stdc++.h>

using namespace std;

struct vertex
{
    int n,c;
};

bool operator <(vertex a, vertex b)
{
    if(a.c!=b.c)
    {
        return a.c>b.c;
    }
    return false;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,e,i,j,u,v;
        scanf("%d %d",&n,&e);
        vector< int >startdis(n+1,2e9);
        vector< int >enddis(n+1,2e9);
        vector< vector<int> >roads(n+1);
        for(i=1;i<=e;i++)
        {
            scanf("%d %d",&u,&v);
            roads[u].push_back(v);
            roads[v].push_back(u);
        }
        int start,End;
        scanf("%d %d",&start,&End);
        startdis[start]=0;
        enddis[End]=0;
        vertex s;
        s.n=start;
        s.c=0;
        priority_queue<vertex>q;
        q.push(s);
        while(!q.empty())
        {
            int current=q.top().n;
            q.pop();
            for(i=0;i<roads[current].size();i++)
            {
                int newnode=roads[current][i];
                int newcost=startdis[current]+1;
                if(startdis[newnode]>newcost)
                {
                    vertex temp;
                    temp.n=newnode;
                    temp.c=newcost;
                    startdis[newnode]=newcost;
                    q.push(temp);
                }
            }
        }
        s.n=End;
        s.c=0;
        q.push(s);
        while(!q.empty())
        {
            int current=q.top().n;
            q.pop();
            for(i=0;i<roads[current].size();i++)
            {
                int newnode=roads[current][i];
                int newcost=enddis[current]+1;
                if(enddis[newnode]>newcost)
                {
                    vertex temp;
                    temp.n=newnode;
                    temp.c=newcost;
                    enddis[newnode]=newcost;
                    q.push(temp);
                }
            }
        }
        int mintime=0;
        for(i=0;i<n;i++)
        {
            int temp;
            temp=startdis[i]+enddis[i];
            mintime=max(mintime,temp);
        }
        printf("Case %d: %d\n",t,mintime);
        t++;
    }

}
