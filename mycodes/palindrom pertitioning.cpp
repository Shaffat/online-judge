#include<bits/stdc++.h>

using namespace std;

void  palindromecheck(string s,int lengh ,vector<vector<bool> >&palindrome)
{
    int i,j;
   if(lengh>=s.size())
   {
       return;
   }
   for(i=0;i<s.size();i++)
   {
       if(i+lengh>=s.size())
       {
           break;
       }
       if(s[i]==s[i+lengh])
       {
           palindrome[i][i+lengh]=1&palindrome[i+1][i+lengh-1];
       }
       else palindrome[i][i+lengh]=0;
   }
   palindromecheck(s,lengh+1,palindrome);
}

int palindrom_partitioning(int start,int ending ,vector<int>&memory,vector<vector<bool> >&palidrom)
{

    if(start>ending)
    {
        return 0;
    }

    int minpalindrome=2e9,i,j;
    if(memory[start]!=-1)
    {
        //cout<<"memorization"<<endl;
        return memory[start];
    }
    if(palidrom[start][ending]==1)
    {
        return 1;
    }
    for(i=start;i<=ending;i++)
    {
        if(palidrom[start][i])
        minpalindrome=min(1+palindrom_partitioning(i+1,ending,memory,palidrom),minpalindrome);
    }
    return memory[start]=minpalindrome;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    getchar();
    while(t<=test)
    {
        string s;
        int i,j;
        cin>>s;
        vector<bool>value(s.size()+1,0);
        vector<vector<bool> >palindrome(s.size()+1,value);
         for(i=0;i<s.size();i++)
        {
            palindrome[i][i]=1;
        }
        for(i=0;i<s.size()-1;i++)
        {
            if(s[i]==s[i+1])
            {
                palindrome[i][i+1]=1;
            }
            else
                palindrome[i][i+1]=0;
        }
        palindromecheck(s,2,palindrome);

        vector<int>memory(s.size()+1,-1);
        printf("Case %d: %d\n",t,palindrom_partitioning(0,s.size()-1,memory,palindrome));
        t++;
}
}
