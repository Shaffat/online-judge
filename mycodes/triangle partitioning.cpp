#include<bits/stdc++.h>

using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        double ab,bc,ac,ae,ad,de,p,p2,areaABC,rati0,areADE,AreTrapijiam;
        scanf("%lf %lf %lf %lf",&ab,&ac,&bc,&rati0);

        p=(ab+bc+ac)/2;
        areaABC=sqrt(p*(p-ab)*(p-bc)*(p-ac));
        //cout<<"Area ABC="<<areaABC<<endl;
        double first=0,last=ab,res;
        int f=0;
        while(first<=last)
        {
            //cout<<"f="<<f<<endl;
            if(first==last)
            {
                f++;
            }
            ad=(first+last)/2;
            ae=ad*(ac/ab);
            de=ad*(bc/ab);
            p2=(ad+ae+de)/2;
            areADE=sqrt(p2*(p2-ad)*(p2-ae)*(p2-de));
            AreTrapijiam=areaABC-areADE;
            double cur_ratio;
            cur_ratio=areADE/AreTrapijiam;

            //cout<<"cur_ratio="<<cur_ratio<<endl;
            if(abs(cur_ratio-rati0)<=0.0000001)
            {
                res=ad;
                break;
            }

            else if(cur_ratio>rati0)
            {
                last=ad;
            }
            else
            {
                first=ad;
            }
            //cout<<"abs value="<<abs(cur_ratio-rati0)<<endl;
           // printf("first=%.8lf last=%.8lf ad=%.8lf curratio=%.8lf ratio=%.8lf\n",first,last,cur_ratio,rati0);
            if(f)
            {
                break;
            }
        }
        printf("Case %d: %.8lf\n",t,res);
        t++;
    }
}
