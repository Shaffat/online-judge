#include<bits/stdc++.h>

using namespace std;

vector<int>primes;
vector<bool>ar(10000,0);
void sieve()
{
    int i,j;
    ar[1]=1;
    for(i=4;i<1e4;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e4);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e4;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e4;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}

int main()
{
    sieve();
    int i,j;
    for(i=0;i<100;i++)
    {
        cout<<primes[i]<<" ";
    }
}
