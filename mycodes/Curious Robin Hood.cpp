#include<bits/stdc++.h>
using namespace std;

vector<int>sacks(100001);
vector<int>sacktree(400001);

void building_sacktree(int node,int b,int e)
{
    if(b==e)
    {
        sacktree[node]=sacks[b];
        return;
    }
    int left=2*node;
    int right=2*node+1;
    int mid=(b+e)/2;
    building_sacktree(left,b,mid);
    building_sacktree(right,mid+1,e);
    sacktree[node]=sacktree[left]+sacktree[right];
}

void update(int node, int b,int e,int i, int value)
{
    if(i<b||i>e)
    {
        return ;
    }
    if(b==i && e==i)
    {
        sacktree[node]=value;
        return;
    }
    int left=2*node;
    int right=2*node+1;
    int mid=(b+e)/2;
    update(left,b,mid,i,value);
    update(right,mid+1,e,i,value);
    sacktree[node]=sacktree[left]+sacktree[right];

}
int query(int node,int b,int e, int i ,int j)
{
    if(e<i||b>j)
    {
        return 0;
    }
    if(b>=i && e<=j)
    {
        return sacktree[node];
    }
    int left=2*node;
    int right=2*node+1;
    int mid=(b+e)/2;
    int res1=query(left,b,mid,i,j);
    int res2=query(right,mid+1,e,i,j);
    return res1+res2;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,q,i,j;
        scanf("%d %d",&n,&q);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            sacks[i]=j;
        }
        building_sacktree(1,1,n);

        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            int operation;
            scanf("%d",&operation);
            if(operation==1)
            {
                int a;
                scanf("%d",&a);
                a++;
                printf("%d\n",sacks[a]);
                update(1,1,n,a,0);
                sacks[a]=0;

            }
            else if(operation==2)
            {
                int a,b;
                scanf("%d %d",&a,&b);
                a++;
                update(1,1,n,a,sacks[a]+b);
                sacks[a]+=b;
            }
            else
            {
                int a,b,c;
                scanf("%d %d",&a,&b);
                a++;b++;
                printf("%d\n",query(1,1,n,a,b));
            }
        }
        t++;
    }

}
