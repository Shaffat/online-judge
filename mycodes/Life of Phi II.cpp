#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<bool>numbers(1e6 +1,0);
vector<ll>res(1e6 +1);

vector<ll>primes;
void sieve()
{

    ll i,j;
    //vector<int>numbers(2500,0);
    for(i=1;i<=1e6;i++)
    {
        res[i]=i;
    }
    numbers[1]=1;
    res[2]=1;
    res[1]=1;
    for(i=4;i<=1e6;i+=2)
    {
        numbers[i]=1;
        res[i]=i/2;
    }
    for(i=3;i<=1e6;i+=2)
    {
        //cout<<i<<" why???"<<endl;
        if(numbers[i]==0)
        {
            for(j=i;j<=1e6;j+=i)
            {
                numbers[j]=1;
                res[j]/=i;
                res[j]*=i-1;
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<=1e6;i+=2)
    {
        if(numbers[i]==0)
        {
            primes.push_back(i);
        }
    }
}


ll solve(ll n,ll m)
{
    return res[n]*res[m];
}



int main()
{
    sieve();
    ll m,n,i,j,onlyn,onlym,both;
    scanf("%lld %lld",&n,&m);
    both=solve(m,n);
    onlym=n*res[m]-both;
    onlyn=m*res[n]-both;
    //cout<<"both="<<both<<" all n="<<m*res[n]<<" all m="<<n*res[m]<<" phi of n="<<res[n]<<" phi of m="<<res[m]<<endl;
    printf("%lld %lld %lld\n",onlyn,onlym,both);
}
