#include<bits/stdc++.h>
using namespace std;

int solve(string &pattern,string &s,string &ans)
{
    int i,j,total=0,sz=s.size();
    for(i=0;i<s.size();i++)
    {
        //cout<<"i="<<i<<endl;
        for(j=i;j<=min(i+2,sz-1);j++)
        {
            //cout<<"j="<<j<<" ";
            if(s[j]!=pattern[j-i])
                total++;
            ans.push_back(pattern[j-i]);
        }
        //cout<<endl;
        i+=2;
    }
    return total;
}


int main()
{
    int n,cost=2e9,cur;
    string s,ans,pattern="RGB",tmp;
    cin>>n>>s;
    ans="";
    sort(pattern.begin(),pattern.end());
    cost=solve(pattern,s,ans);
    while(next_permutation(pattern.begin(),pattern.end()))
    {
        //cout<<"pattern="<<pattern<<endl;
        tmp="";
        cur=solve(pattern,s,tmp);
        if(cur<cost)
        {
            cost=cur;
            ans=tmp;
        }
    }
    cout<<cost<<endl<<ans;
}
