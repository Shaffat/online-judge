#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,r,c,fc=2e9,lc=-1,fr=2e9,lr=-1;
    scanf("%d %d",&r,&c);
    vector<char>col(c+1);
    vector<vector<char> >graph(r+1,col);
    for(i=1;i<=r;i++)
    {
        bool chk=0;
        for(j=1;j<=c;j++)
        {
            char ch;
            scanf(" %c",&ch);
            graph[i][j]=ch;
            if(ch=='*')
            {
                chk=1;
                fc=min(fc,j);
                lc=max(lc,j);
            }
        }
        if(chk)
        {
            fr=min(fr,i);
            lr=max(lr,i);
        }
    }
    for(i=fr;i<=lr;i++)
    {
        for(j=fc;j<=lc;j++)
        {
            printf("%c",graph[i][j]);
        }
        printf("\n");
    }
}
