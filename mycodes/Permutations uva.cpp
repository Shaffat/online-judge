#include<bits/stdc++.h>
using namespace std;

#define ll long long int

vector<ll>factorial(21);

string solve(ll n,string &s)
{
    string res="";
    sort(s.begin(),s.end());
    ll x=s.size(),i;
    for(i=x-1;i>=0;i--)
    {
        ll cur=factorial[i],letter;
        if(n%cur==0)
        {
            res.push_back(s[n/factorial[i] - 1]);
            s.erase(s.begin()+n/factorial[i] -1);
            reverse(s.begin(),s.end());
            res+=s;
            break;
        }
        else
        {
            res.push_back(s[n/factorial[i]]);
            s.erase(s.begin()+n/factorial[i]);
            n%=factorial[i];
        }
    }
    return res;
}

int main()
{
    ios_base::sync_with_stdio(0);
    string s,res;
    ll test,t,i,j,n;
    factorial[0]=1;
    for(i=1;i<=20;i++)
    {
        factorial[i]=factorial[i-1]*i;
    }
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>s>>n;
        res=solve(n+1,s);
        cout<<res<<endl;
    }
}
