#include<bits/stdc++.h>
using namespace std;

int solve(int i1,int i2,int i3,string s1,string s2,string s3,vector<vector<vector<int> > >&memory)
{
    //cout<<"i1="<<i1<<" i2="<<i2<<" i3="<<i3<<" s1="<<s1<<" s2="<<s2<<" s3="<<s3<<endl;
    if(i1>=s1.size()||i2>=s2.size()||i3>=s3.size())
    {
        return 0;
    }
    if(memory[i1][i2][i3]!=-1)
    {
        return memory[i1][i2][i3];
    }
    int max_Sequence=-1;
    if(s1[i1]==s2[i2]&&s2[i2]==s3[i3])
    {
        max_Sequence=max(max_Sequence,solve(i1+1,i2+1,i3+1,s1,s2,s3,memory)+1);
    }
    else
    {
        max_Sequence=max(max_Sequence,solve(i1+1,i2,i3,s1,s2,s3,memory));
        max_Sequence=max(max_Sequence,solve(i1,i2+1,i3,s1,s2,s3,memory));
        max_Sequence=max(max_Sequence,solve(i1,i2,i3+1,s1,s2,s3,memory));
    }
    //cout<<"memory["<<i1<<"]["<<i2<<"]["<<i3<<"]="<<max_Sequence<<endl;
    return memory[i1][i2][i3]=max_Sequence;
}

int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        string s1,s2,s3;
        cin>>s1>>s2>>s3;
        vector<int>col1(52,-1);
        vector<vector<int> >col2(52,col1);
        vector<vector<vector<int> > >memory(52,col2);
        int res=solve(0,0,0,s1,s2,s3,memory);
        printf("Case %d: %d\n",t,res);
    }
}
