#include<bits/stdc++.h>

#define ll long long int
using namespace std;

int main()
{
    ll n,m,i,j,res=0,f=0;
    scanf("%lld %lld",&n,&m);
    if(n<m) swap(n,m);
    res=((n*(n+1))/2)*((m*(m+1))/2);
    for(i=1;i<=min(n,m);i++)
    {
        f+=(n-i+1)*(m-i+1);
    }
    printf("%lld\n",res-f);
}
