#include<bits/stdc++.h>
using namespace std;

int data[100001];
int tree[400001];
int n;
void treebuilding(int node,int b,int e)
{
    if(b==e)
    {
        tree[node]=data[b];
        return;
    }
    int left=2*node;
    int right=2*node+1;
    int mid=(b+e)/2;
    treebuilding(left,b,mid);
    treebuilding(right,mid+1,e);
    tree[node]=min(tree[left],tree[right]);
}
int query(int node,int b,int e, int i,int j)
{
    //cout<<"node="<<node<<" b="<<b<<" e="<<e<<" i="<<i<<" j="<<j<<endl;
    if(e<i|| b>j)
    {
        //cout<<"out"<<endl;
        return 2e9;
    }
    if(i<=b && j>=e )
    {
        //cout<<"in "<<tree[node]<<endl;
        return tree[node];
    }
    //cout<<"partial"<<endl;
    int left=2*node;
    int right=2*node+1;
    int mid=(b+e)/2;
    int res1=query(left,b,mid,i,j);
    int res2=query(right,mid+1,e,i,j);
    return min(res1,res2);
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j,q;
        scanf("%d %d",&n,&q);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            data[i]=j;
        }

        treebuilding(1,1,n);
        printf("Case %d:\n",t);
        t++;
        for(i=1;i<=q;i++)
        {
            int b,e;
            scanf("%d %d",&b,&e);
            printf("%d\n",query(1,1,n,b,e));
        }

    }
}
