#include<bits/stdc++.h>
using namespace std;

bool valid(int r,int c,int n)
{
    if(r<0 || r>=n || c<0 || c>=n)
        return false;
    return true;
}

int solve(int r,int c,int n,int k,vector<vector<int> >&pennies,vector<vector<int> >&memory)
{
    if(r<0 || r>=n || c<0 || c>=n)
        return 0;
    if(memory[r][c]!=-1)
    {
        return memory[r][c];
    }
    int i,j,cost=0,tmp;

    for(i=1;i<=k;i++)
    {
        if(valid(r+i,c,n)==false) continue;
        if(pennies[r+i][c]<=pennies[r][c]) continue;
        tmp=solve(r+i,c,n,k,pennies,memory);
        cost=max(cost,tmp);
    }
    for(i=1;i<=k;i++)
    {
        if(valid(r-i,c,n)==false) continue;
        if(pennies[r-i][c]<=pennies[r][c]) continue;
        tmp=solve(r-i,c,n,k,pennies,memory);
        cost=max(cost,tmp);
    }
    for(i=1;i<=k;i++)
    {
        if(valid(r,c+i,n)==false) continue;
        if(pennies[r][c+i]<=pennies[r][c]) continue;
        tmp=solve(r,c+i,n,k,pennies,memory);
        cost=max(cost,tmp);
    }
    for(i=1;i<=k;i++)
    {
        if(valid(r,c-i,n)==false) continue;
        if(pennies[r][c-i]<=pennies[r][c]) continue;
        tmp=solve(r,c-i,n,k,pennies,memory);
        cost=max(cost,tmp);
    }

    return memory[r][c]=cost+pennies[r][c];

}

int main()
{
    int test,t,i,j,n,k;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        if(t>1) printf("\n");
        scanf("%d %d",&n,&k);
        vector<int>col(n);
        vector<vector<int> >pennies(n,col);

        vector<int>col1(n,-1);
        vector<vector<int> >memory(n,col1);

        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                int tmp;
                scanf("%d",&tmp);
                pennies[i][j]=tmp;
            }
        }
        int res=solve(0,0,n,k,pennies,memory);
        printf("%d\n",res);
    }
}
