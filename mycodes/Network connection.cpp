#include<bits/stdc++.h>

using namespace std;

int find_parent(int r,vector<int>&ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    return ar[r]=find_parent(ar[r],ar);
}

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int i,j,n;
        scanf("%d",&n);
        vector<int>parent(n+10,0);
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }
        vector<int>ranked(n+10,0);
        char c;
        int s=0,us=0;
        getchar();
        while(scanf("%c",&c)!=EOF)
        {

            int u,v;
            scanf("%d %d",&u,&v);
            //cout<<"u="<<u<<" v="<<v<<" c="<<c<<endl;
            if(c=='c')
            {

                int x,y;
                x=find_parent(u,parent);
                y=find_parent(v,parent);
                if(x!=y)
                {
                    if(ranked[x]==ranked[y])
                    {
                        parent[y]=x;
                        ranked[x]++;
                    }
                    else if(ranked[x]>ranked[y])
                    {
                        parent[y]=x;
                    }
                    else
                        parent[x]=y;
                }
            }
            else
            {
                int x,y;
                x=find_parent(u,parent);
                y=find_parent(v,parent);
                if(x!=y)
                {
                    us++;
                }
                else
                    s++;

            }
            getchar();
        }
        printf("%d,%d\n",s,us);

    }
}
