#include<bits/stdc++.h>

using namespace std;
vector< vector<int> >priorities(18);
int solve(int mask,int n,vector<int>&memory)
{

    if(mask>=(1<<n)-1)
    {
        return 0;
    }
    if(memory[mask]!=-1)
    {
        return memory[mask];
    }

    int i,j,row=0;
    for(i=0;i<n;i++)
    {
        if((mask&(1<<i))==(1<<i))
        {
            row+=1;
        }
    }

    int mx=-1;
    for(i=0;i<n;i++)
    {

        if((mask&(1<<i))==0)
        {
            int k=solve(mask|(1<<i),n,memory)+priorities[row][i];
            mx=max(mx,k);
        }
    }
    memory[mask]=mx;
    return mx;
}


int main()
{
    int test,t=1;
    scanf("%d",&test);

    while(t<=test)
    {
         int n,i,j;
         scanf("%d",&n);
         vector<int>memory((1<<n)+1,-1);
         vector<int>demo(n+1);
         for(i=0;i<n;i++)
         {

             for(j=0;j<n;j++)
             {
                int k;
                scanf("%d",&k);

                priorities[i].push_back(k);
             }
         }

         for(i=0;i<n;i++)
         {
             priorities[i].clear();
         }
         printf("Case %d: %d\n",t,solve(0,n,memory));
         t++;
    }
}
