#include<bits/stdc++.h>
using namespace std;

using namespace std;

int ar[1000010];

void sieve(int N,int *ar)

{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}

int main()
{
    sieve(1000000,ar);

    int test,k;
    while(scanf("%d",&test))
    {
        if(test==0)
        {
            break;
        }
        else
        {
            k=test;
            while(ar[k]!=0 && k>0)
            {
                k/=10;
            }
        }
        printf("%d\n",k);
    }
}
