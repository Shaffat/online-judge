#include<bits/stdc++.h>
using namespace std;

bool z_algo(int n,string &s,vector<int>&z_array)
{
    int i,j,l=0,r=0,nxt,matched;
    z_array[0]=0;
    //cout<<"0 ";
    for(i=1;i<s.size();i++)
    {
        if(r<i)
        {
            l=i;
            r=i;
        }
        if(l<=i && r>=i)
        {
            int nxt=min(z_array[i-l],r-i);
            //cout<<"i="<<i<<" nxt="<<nxt<<" l="<<l<<" r="<<r<<endl;
            if(i+nxt<r)
            {
                z_array[i]=nxt;
            }
            else
            {
                for(j=i+nxt;j<s.size();j++)
                {
                    //cout<<"matching between "<<j<<" and "<<nxt<<endl;
                    if(s[j]==s[nxt])
                    {
                        nxt++;
                        r++;
                    }
                    else
                        break;
                }
                z_array[i]=nxt;
                if(nxt==n) return true;
                l=i;
            }
            //cout<<" "<<z_array[i]<<" ";
        }

    }
    return false;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,q,i;
    cin>>test;
    string s,p,s1;
    for(t=1;t<=test;t++)
    {
        string s;
        cin>>s;
        cin>>q;
        for(i=1;i<=q;i++)
        {
            cin>>p;
            s1=p+"#"+s;
            vector<int>z_array(s1.size());
            if(z_algo(p.size(),s1,z_array))
            {
                cout<<"y"<<endl;
            }
            else
                cout<<"n"<<endl;
        }
    }
}
