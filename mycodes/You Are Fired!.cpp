#include<bits/stdc++.h>
using namespace std;

#define ll long long int

struct person
{
    string name;
    ll cost;
};

bool operator <(person a,person b)
{
    if(a.cost!=b.cost)
    {
        return a.cost>b.cost;
    }
    return false;
}

int main()
{
    ll n,d,k,i,j;
    cin>>n>>d>>k;
    vector<person>v;
    for(i=1;i<=n;i++)
    {
        person p;
        string s;
        cin>>p.name>>p.cost;
        v.push_back(p);
    }
    sort(v.begin(),v.end());
    ll save=0;
    for(i=0;i<k;i++)
    {
        save+=v[i].cost;
    }
    if(save>=d)
    {
        cout<<k<<endl;
        for(i=0;i<k;i++)
        {
            cout<<v[i].name<<", YOU ARE FIRED!"<<endl;
        }
    }
    else
    {
        cout<<"impossible"<<endl;
    }
    return 0;
}
