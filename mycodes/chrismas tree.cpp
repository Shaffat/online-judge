#include<bits/stdc++.h>

using namespace std;

int solve(int node,int k,vector<int>&vis,vector<vector<int> >&connection)
{
    int child;
    if(node==1)
    {
        child=connection[node].size();
    }
    else
        child=connection[node].size()-1;
    if(child<k)
    {
        return 1;
    }
    int i,j,res=1;

    vector<int>v;
    for(i=0;i<connection[node].size();i++)
    {
        int next=connection[node][i];

        if(vis[next]==0)
        {
            vis[next]=1;
            int cur=solve(next,k,vis,connection);

            v.push_back(cur);
        }
    }
    sort(v.begin(),v.end());
    int first=v.size()-1,last=v.size()-k;
    for(i=first;i>=last;i--)
    {
        res+=v[i];
    }
    return res;
}


int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,k,i,j,u,v;
        scanf("%d %d",&n,&k);
        vector<vector<int> >connection(n+1);
        vector<int>vis(n+1,0);
        for(i=1;i<n;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        vis[1]=1;
        int res=solve(1,k,vis,connection);
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
