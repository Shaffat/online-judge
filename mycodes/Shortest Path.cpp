#include<bits/stdc++.h>
using namespace std;
struct compInfo
{
    int root,leaf,x,cycle;
};


int findRootdisCycle(int node,int root,vector<vector<int> >&connection,vector<int>&parent,vector<int>&rootdis,
                vector<int>&level,vector<int>&vis,vector<vector<int> >&cost,vector<int>&degree)
{
    int cycle=1,i,j,cur;
    if(degree[node]!=2){
        cycle=0;
    }
    for(i=0;i<connection[node].size();i++)
    {
        int nxt=connection[node][i];
        if(!vis[nxt])
        {
            vis[nxt]=1;
            level[nxt]=level[node]+1;
            rootdis[nxt]=rootdis[node]+cost[node][i];
            parent[nxt]=root;
            int tmp=findRootdisCycle(nxt,root,connection,parent,rootdis,level,vis,cost,degree);
            cycle=(cycle&tmp);
        }
    }
    return cycle;
}


void findCmp(int root,vector<vector<int> >&connection,vector<int>&parent,vector<int>&rootdis,
             vector<int>&leafdis,vector<int>&level,vector<int>&vis,vector<vector<int> >&cost,
             vector<compInfo>&cmp,vector<int>&degree,vector<int>&vis1)
{
    int i,j,cur,cycle=1,leaf,x;
    level[root]=0;
    rootdis[root]=0;
    parent[root]=root;
    vis[root]=1;
    cycle=findRootdisCycle(root,root,connection,parent,rootdis,level,vis,cost,degree);
    cmp[root].root=root;
    cmp[root].cycle=cycle;

    if(cycle){
        if(level[connection[root][0]]>level[connection[root][1]]){
            leaf=connection[root][0];
            x=cost[root][0];
        }
        else
        {
            leaf=connection[root][1];
            x=cost[root][1];
        }

        cmp[root].leaf=leaf;
        cmp[root].x=x;
        queue<int>q1;

        vis1[leaf]=1;
        q1.push(leaf);
        leafdis[leaf]=0;
        while(!q1.empty())
        {
            cur=q1.front();
            q1.pop();
            for(i=0;i<connection[cur].size();i++)
            {
                int nxt=connection[cur][i];
                if(!vis1[nxt])
                {
                    if((leaf==nxt && cur ==root) || (leaf ==cur && root==nxt))
                        continue;
                    vis1[nxt]=1;
                    leafdis[nxt]=cost[cur][i]+leafdis[cur];
                    q1.push(nxt);
                }
            }
        }

    }
    return;
}

int query(int u,int v,vector<int>&level,vector<compInfo>&cmp,vector<int>&leafdis,vector<int>&rootdis,
          vector<int>&parent)
{
    if(parent[u]!=parent[v]) return -1;
    if(level[u]>level[v]) swap(u,v);
    int p, res1,res2;
    p=parent[u];
    if(cmp[p].cycle)
    {
        res1=rootdis[v]-rootdis[u];
        res2=rootdis[u]+leafdis[v]+cmp[p].x;
        return min(res1,res2);
    }
    else
        return rootdis[v]-rootdis[u];
}

int main()
{
    int i,j,n,e,m,u,v,w,t,test,q;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&e);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        vector<int>parent(n+1,0);
        vector<int>vis(n+1,0);
        vector<int>vis1(n+1,0);
        vector<int>leafdis(n+1,0);
        vector<int>rootdis(n+1,0);
        vector<compInfo>cmp(n+1);
        vector<int>degree(n+1,0);
        vector<int>level(n+1,0);
        for(i=1;i<=e;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            connection[v].push_back(u);
            cost[u].push_back(w);
            cost[v].push_back(w);
            degree[u]++;
            degree[v]++;
        }
        vector<int>roots;
        for(i=1;i<=n;i++)
        {
            if(degree[i]==1){
                roots.push_back(i);
            }
        }
        for(i=0;i<roots.size();i++)
        {
            if(!vis[roots[i]]){
                findCmp(roots[i],connection,parent,rootdis,leafdis,level,vis,cost,cmp,degree,vis1);
            }
        }
        for(i=1;i<=n;i++)
        {
            if(!vis[i]){
                findCmp(i,connection,parent,rootdis,leafdis,level,vis,cost,cmp,degree,vis1);
            }
        }
        scanf("%d",&q);
        printf("Case %d:\n",t);
        while(q--)
        {
            scanf("%d %d",&u,&v);
            int res=query(u,v,level,cmp,leafdis,rootdis,parent);
            printf("%d\n",res);
        }
    }
}
