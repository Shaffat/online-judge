
#include<bits/stdc++.h>

using namespace std;

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
   int test,t=1;
   scanf("%d",&test);
   while(t<=test)
   {
       long long int i,j,k,m,mx=0,total=0;
       scanf("%lld %lld",&k,&m);
       vector<long long int>books;
       for(i=1;i<=k;i++)
       {
           long long int l;
           scanf("%lld",&l);
           books.push_back(l);
           mx=max(mx,l);
           total+=l;
       }
       long long int first=mx,last=total,mid,f=0,current=0,counter=1,res;
       while(first<=last)
       {

           if(first==last)
           {
               f++;
           }
           int chk=1;
           mid=(first+last)/2;
           //cout<<"first="<<first<<" last="<<last<<" mid="<<mid<<endl;
           for(i=0;i<books.size();i++)
           {
               if(current+books[i]<=mid)
               {
                   current+=books[i];
               }
               else
               {
                   counter++;
                   current=books[i];
                   if(counter>m)
                   {
                       chk=0;
                       break ;
                   }
               }
               //cout<<"current="<<current<<" counter="<<counter<<endl;
           }
           if(chk)
           {
               last=mid-1;
               res=mid;
           }
           else
           {
               first=mid+1;
           }
           counter=1;
           current=0;
           if(f)
           {
               break;
           }

       }
       //cout<<res<<endl;
       j=books.size()-1;
       vector<long long int>result;
       current=0;
       long long int remaining_book=k,remaining_scriber=m,chk=0,rb;

       for(i=j;i>=0;i--)
       {
           if(current+books[i]<=res)
           {
               current+=books[i];
               if(remaining_book==remaining_scriber)
               {
                   remaining_book--;
                   remaining_scriber--;
                   result.push_back(current);
                   current=0;
               }
               else
               {
                   remaining_book--;
               }

           }
           else
           {
               result.push_back(current);
               //remaining_book--;
               remaining_scriber--;
               current=0;
               i++ ;
           }
       }
       long long int l=0;
       for(i=result.size()-1;i>=0;i--)
       {
           int cur=0;
           //cout<<"initial l="<<l<<endl;
           for(j=l;j<books.size();j++)
           {
               //cout<<"J="<<j<<endl;

               if(cur+books[j]>result[i])
               {
                   l=j;
                   break;
               }
               else
               {
                   cur+=books[j];
                   if(j==books.size()-1)
                   {
                       printf("%lld\n",books[j]);
                       break;
                   }
                   printf("%lld ",books[j]);
               }
           }
          // cout<<"last l="<<l<<endl;
           if(i>0)
           {
               printf("/ ");
           }
       }
       t++;
   }

}
