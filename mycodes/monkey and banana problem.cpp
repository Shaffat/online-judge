#include<bits/stdc++.h>

using namespace std;

int maximum_banana(int current_row,int current_column,vector< vector<int> >&memory,vector< vector<int> >&inputs,int n)
{
    //cout<<"current_row="<<current_row<<" current_column"<<current_column<<"column size="<<inputs[current_row].size()-1<<endl;
    if(current_row>=2*n-1||(current_column>inputs[current_row].size()-1 || current_column<0))
    {
        //cout<<"terminating"<<endl;
        return 0;
    }
    if(memory[current_row][current_column]!=-1)
    {
       // cout<<"memorization"<<endl;
        return memory[current_row][current_column];
    }
    else
    {
        if(current_row<n-1)
        {

            int l=maximum_banana(current_row+1,current_column,memory,inputs,n);
            int k=maximum_banana(current_row+1,current_column+1,memory,inputs,n);
            memory[current_row][current_column]=max(l,k)+inputs[current_row][current_column];
            //cout<<"memory["<<current_row<<"]["<<current_column<<"]"<<memory[current_row][current_column]<<endl;
            return memory[current_row][current_column];
        }
        else
        {

            int l=maximum_banana(current_row+1,current_column,memory,inputs,n);
            int k=maximum_banana(current_row+1,current_column-1,memory,inputs,n);
            memory[current_row][current_column]=max(l,k)+inputs[current_row][current_column];
            //cout<<"memory["<<current_row<<"]["<<current_column<<"]"<<memory[current_row][current_column]<<endl;
            return memory[current_row][current_column];
        }

    }


}

int main()
{
    int test,t=1,n,i,j;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d",&n);
        vector<vector<int> >inputs(2*n+6);
        vector<vector<int> >memory(2*n+6);
        int k=0;
        for(i=0;i<n;i++)
        {
           for(j=0;j<=i;j++)
            {
                int k;
                scanf("%d",&k);
                inputs[i].push_back(k);
                memory[i].push_back(-1);
            }
        }
        for(i=n;i<((2*n)-1);i++)
        {

            for(j=0;j<((2*n)-1)-i;j++)
            {
                int k;
                scanf("%d",&k);
                inputs[i].push_back(k);
                memory[i].push_back(-1);
            }
        }

        printf("Case %d: %d\n",t,maximum_banana(0,0,memory,inputs,n));
        t++;
    }
}
