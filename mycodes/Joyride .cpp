#include<bits/stdc++.h>
using namespace std;

#define ll long long int
struct ride
{
    ll time,cost;
};

vector<ll>col(1001,-1);
vector<vector<ll> >memory(1001,col);

ll solve(ll node,ll remain,ll t,ll one,vector<vector<ll> >&connection,vector<ride>&joyride)
{
    //cout<<"node="<<node<<" remain="<<remain<<endl;
    if(remain<0)
    {
        //cout<<"cant reach"<<endl;
        return 2e9;
    }
    if(remain==joyride[1].time && node==1 && one>0)
    {
        //cout<<"saved!!!"<<endl;
        return joyride[1].cost;
    }
    if(memory[node][remain]!=-1)
    {
        return memory[node][remain];
    }
    ll tmp=one;
    if(node==1)
    {
        tmp++;
    }
    ll res1,res2=2e9,i;
    res1=solve(node,remain-joyride[node].time,t,tmp,connection,joyride);
    for(i=0;i<connection[node].size();i++)
    {
        ll nxt=connection[node][i];
        //cout<<"from "<<node<<" to "<<nxt<<" state "<<node<<" "<<remain<<endl;
        ll cur=solve(nxt,remain-t-joyride[node].time,t,tmp,connection,joyride);
        res2=min(res2,cur);
    }
    //cout<<"memory["<<node<<"]["<<remain<<"]="<<min(res1,res2)+joyride[node].cost<<endl;
    return memory[node][remain]=min(res1,res2)+joyride[node].cost;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll x,n,m,t,i,j;
    scanf("%lld %lld %lld %lld",&x,&n,&m,&t);
    vector<vector<ll> >connection(n+1);
    vector<ride>joyride(n+1);
    for(i=1;i<=m;i++)
    {
        ll a,b;
        scanf("%lld %lld",&a,&b);
        connection[a].push_back(b);
        connection[b].push_back(a);
    }
    for(i=1;i<=n;i++)
    {
        ride tmp;
        ll p,r;
        scanf("%lld %lld",&r,&p);
        tmp.time=r;
        tmp.cost=p;
        joyride[i]=tmp;
    }
    ll res=solve(1,x,t,0,connection,joyride);
    if(res<2e9)
    {
        printf("%lld\n",res);
    }
    else
        printf("It is a trap.\n");
}
