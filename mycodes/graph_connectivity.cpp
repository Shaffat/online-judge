#include<bits/stdc++.h>
using namespace std;

int find_parent(int r,int *ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    ar[r]=find_parent(ar[r],ar);
    return ar[r];
}

int main()
{
    int test,t=1,i;
    map<char,int>digit;

    for(i='A';i<='Z';i++)
    {
        digit[i]=i-'A'+1;

    }

    scanf("%d",&test);
    //cerr << test << endl ;
     //getchar();
    while(t<=test)
    {
        char largenode,s,f;
        scanf(" %c",&largenode);
        //cerr << largenode << endl ;
        int n=digit[largenode];
        //cerr << "Size" << n << endl ;
        int u,v,j;
        int parent[n+1];
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }
        vector<int>rank_(n+1,0);

        string w;
        getchar()
        while(getline( cin , w ))
        {
            if(w=="")
            {
                break;
            }

            int u,v;
            char p=w[0],q=w[1];

            u=digit[p];
            v=digit[q];
           // cout<<u<<" "<<v<<endl;
            int x,y;
            x=find_parent(u,parent);
            y=find_parent(v,parent);
            if(x!=y)
            {
                if(rank_[y]==rank_[x])
                {
                    parent[y]=x;
                    rank_[x]++;
                }
                else if(rank_[x]>rank_[y])
                {
                    parent[y]=x;
                }
                else
                    parent[x]=y;
            }
        }
        vector<int>total(n+1,0);
        set<int>o;
        for(i=1;i<=n;i++)
        {
            int x=find_parent(i,parent);
            total[x]++;
        }
        int mx=0;
        for(i=1;i<=n;i++)
        {
            //cout<<total[i]<<" ";
            if(total[i]>0)
            {
                mx++;
            }
        }
        printf("%d",mx);
        t++;
        if(t<=test)
        {
            printf("\n\n");
        }

    }

}

