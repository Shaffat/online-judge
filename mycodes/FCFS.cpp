#include<bits/stdc++.h>

using namespace std;


void FCFS(int n,vector<int>&arrival,vector<int>&bursttime)
{
    int i,j;
    vector<int>endtime(n+1,1);
    vector<int>turnaround(n+1,1);
    vector<int>waiting(n+1,1);
    for(i=1;i<=n;i++)
    {
        //cout<<"previous end="<<endtime[i-1]<<" burst="<<bursttime[i-1]<<endl;
        endtime[i]=endtime[i-1]+bursttime[i-1];
        //cout<<"endtime["<<i<<"]="<<endtime[i]<<endl;
    }
    //cout<<"waiting time\n";
    for(i=1;i<=n;i++)
    {
        waiting[i]=endtime[i]-arrival[i];
        cout<<waiting[i]<<" ";
    }
    cout<<endl;
    cout<<"turnaround"<<endl;
    for(i=1;i<=n;i++)
    {
        turnaround[i]=waiting[i]+bursttime[i];
    }
    for(i=1;i<=n;i++)
    {
        cout<<turnaround[i]<<" ";
    }
    cout<<endl;
}


int main()
{
    int n,i,j,k;
    scanf("%d",&n);
    vector<int>bursttime(n+1,1);
    vector<int>arrival(n+1,1);
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&j,&k);
        bursttime[i]=k;
        arrival[i]=j;
    }
    FCFS(n,arrival,bursttime);
}


