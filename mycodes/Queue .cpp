#include<bits/stdc++.h>
using namespace std;
vector<int>memory(2e6+1,-1);
struct node
{
    int id,behind;
};
bool operator <(node a,node b)
{
    if(a.behind!=b.behind)
    {
        return a.behind>b.behind;
    }
    return false;
}
int solve(int id,vector<int>connection)
{
    if(connection[id]==0 || connection[id]==-1)
    {
        return 1;
    }
    if(memory[id]!=-1)
    {
        return memory[id];
    }
    memory[id]=1+solve(connection[id],connection);
    //cout<<"id="<<id<<" behind="<<memory[id]<<endl;
    return memory[id];
}
int main()
{
    int n,i,j,u,v;
    scanf("%d",&n);
    vector<int>connection(2e6+1,-1);
    set<int>myset;
    vector<int>ve;
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u]=v;
        if(u!=0)
        {
            int len=myset.size();
            myset.insert(u);
            if(len<myset.size())
            {
                ve.push_back(u);
            }
        }
        if(v!=0){
            int len=myset.size();
            myset.insert(v);
            if(len<myset.size())
            {
                ve.push_back(v);
            }
        }
    }
    vector<node>q;
    for(i=0;i<ve.size();i++)
    {
        node tmp;
        tmp.id=ve[i];
        if(connection[ve[i]]==-1)
        {
            //cout<<"blank id="<<tmp.id<<endl;
            tmp.behind=0;
        }
        else
        tmp.behind=solve(ve[i],connection);
        //cout<<"id="<<tmp.id<<" behind="<<tmp.behind<<endl;
        q.push_back(tmp);
    }
    sort(q.begin(),q.end());
    for(i=0;i<q.size();i++)
    {
        printf("%d ",q[i]);
    }

}
