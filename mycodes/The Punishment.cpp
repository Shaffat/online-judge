#include<bits/stdc++.h>
using namespace std;

struct info
{
    int nxt;
    int counter;
    bool finish;
};
void build_tree(string &s,vector<vector<info> >&tree)
{
    int i,j,pos=0;
    for(i=0;i<s.size();i++)
    {
        if(tree[pos][s[i]].nxt==-1)
        {
            info tmp;
            tmp.nxt=-1;
            tmp.finish=0;
            tmp.counter+=1;
            //cout<<"pos="<<pos<<" char not found so make a new"<<endl;
            tree[pos][s[i]].nxt=tree.size();
            vector<info>v(130,tmp);
            tree.push_back(v);
        }
//        else
//            cout<<"already have one so next pos="<<tree[pos][s[i]].nxt<<endl;
        if(i==s.size()-1)
        {
            tree[pos][s[i]].finish=1;
        }
        pos=tree[pos][s[i]].nxt;
    }
    return;
}

void depthSearch(int pos,string &common,vector<vector<info> >&tree,string &ans,int &counter)
{
    int i,j;
    for(i='a';i<='z';i++)
    {
        string tmp=common,tmp1="a";
        tmp1[0]+=i-'a';
        if(tree[pos][i].finish)
        {
            int cur=tree[pos][i].counter;
            if(cur>counter)
            {
                counter=cur;


                if(tmp+tmp1<ans){
                    ans=tmp+tmp1;
                }
            }
            else if(cur==counter)
            {

                if(tmp+tmp1<ans){
                    ans=tmp+tmp1;
                }

            }
        }
        string cur=tmp+tmp1;
        depthSearch(pos,cur,tree,ans,counter);
    }
    return;
}

bool ans_me(string &s,vector<vector<info> >&tree,string &ans,int &counter)
{
    int i,j,finished_at,done,pos=0;
    for(i=0;i<s.size();i++)
    {
        if(i==s.size()-1)
        {
            done =1;
            finished_at=i;
            if(tree[pos][s[i]].finish)
            {
                counter=tree[pos][s[i]].counter;
                ans=s;
            }
            pos=tree[pos][s[i]].nxt;
            break;
        }

        if(tree[pos][s[i]].nxt==-1)
            return 0;
        pos=tree[pos][s[i]].nxt;

    }
    cout<<"pos="<<pos<<endl;
    if(pos!=-1)
    depthSearch(pos,s,tree,ans,counter);


}

int main()
{
    int i,j,n,m;
    string s;
    info tmp;
    tmp.nxt=-1;
    tmp.finish=0;
    tmp.counter=0;

    vector<vector<info> >tree;
    vector<info>v(130,tmp);
    tree.push_back(v);
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        //cout<<"s="<<s<<endl;
        build_tree(s,tree);
    }
    cin>>m;
    for(i=1;i<=m;i++)
    {
        cin>>s;
        string ans="";
        int counter=-1;
        if(ans_me(s,tree,ans,counter))
            cout<<ans<<" "<<counter<<endl;
        else
            cout<<"-1"<<endl;
    }
}

