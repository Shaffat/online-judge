#include<bits/stdc++.h>
using namespace std;

struct segment
{
    int l,r,p;
};

bool operator <(segment a, segment b)
{
    if(a.l!=b.l)
    {
        return a.l<b.l;
    }
    return a.r<b.r;
}

int main()
{
    int test,t,i,j,q,l,r,first,last,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>pos(n+1,2);
        vector<segment>v;
        segment tmp;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&l,&r);
            tmp.l=l;
            tmp.r=r;
            tmp.p=i-1;
            v.push_back(tmp);
        }
        sort(v.begin(),v.end());
        int taken=1;
        first=v[0].l;
        last=v[0].r;
        pos[v[0].p]=1;
        for(i=1;i<v.size();i++)
        {
            if((v[i].l>=first && v[i].l<=last)||(v[i].r>=first && v[i].r<=last))
            {
                taken++;
                first=min(v[i].l,first);
                last=max(last,v[i].r);
                pos[v[i].p]=1;
            }
        }
        if(taken==n || taken==0)
        {
            printf("-1\n");
        }
        else
        {
            for(i=0;i<n;i++)
                printf("%d ",pos[i]);
            printf("\n");
        }
    }
}
