///Modular Inverse

#include<bits/stdc++.h>
using namespace std;

#define mod 1000000007
#define ll long long int
ll find_mod_inverse(ll A)
{
    int i,j,a=A,b=mod,x,xMinusOne,xMinusTwo,tmp,k,r;
    xMinusOne=0;
    xMinusTwo=1;
    while(a%b!=0)
    {
        k=a/b;
        r=a%b;
        a=b;
        b=r;
        x=xMinusTwo-(k*xMinusOne);
        xMinusTwo=xMinusOne;
        xMinusOne=x;
    }
    return (x+mod)%mod;

}


int main()
{
    ll inverse=find_mod_inverse(6),i,j,n,t,res,res2;
    //cout<<inverse<<endl;
    scanf("%lld",&t);
    while(t--){
        scanf("%lld",&n);
        res=0;
        res=((((n%mod)*((n+1)%mod))%mod)*(((2*n)+1)%mod))%mod;
        //cout<<"res="<<res<<endl;
        res*=inverse;
        //cout<<"res="<<res<<endl;
        res%=mod;
        //cout<<"res="<<res<<endl;
        res2=((((n%mod)*((n-1)%mod))%mod)*(((2*n)-1)%mod))%mod;
        //cout<<"res2="<<res2<<endl;
        res2*=inverse;
        res2%=mod;
        res-=res2;
        res+=mod;
        res%=mod;
        printf("%lld\n",res);
    }

}
