#include<bits/stdc++.h>
using namespace std;

struct node
{
    int n,w;
};

bool operator < (node x, node y)
{
    if(x.w!=y.w)
    {
        return x.w>y.w;
    }
    return false;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,i,j,totalcost=0;
        scanf("%d",&n);
        vector<int>edges[n];
        vector<int>cost[n];
        vector<int>vis(n,0);
        vector<int>mst(n,1<<29);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                int k;
                scanf("%d",&k);
                if(k>0)
                {
                    edges[i].push_back(j);
                    edges[j].push_back(i);
                    cost[i].push_back(k);
                    cost[j].push_back(k);
                    totalcost+=k;
                }
            }
        }
        node start;
        start.n=0;
        start.w=0;
        mst[0]=0;
        priority_queue<node>q;
        q.push(start);
        while(!q.empty())
        {
             int cur_top=q.top().n;
             q.pop();
             if(vis[cur_top])
             {
                 continue;
             }
             vis[cur_top]=1;
             for(i=0;i<edges[cur_top].size();i++)
             {
                 int nxt_node=edges[cur_top][i];
                 int nxt_cost=cost[cur_top][i];
                 if(mst[nxt_node]>nxt_cost && vis[nxt_node]==0)
                 {
                     mst[nxt_node]=nxt_cost;
                     node nwnode;
                     nwnode.n=nxt_node;
                     nwnode.w=nxt_cost;
                     q.push(nwnode);
                 }

             }
        }
        printf("Case %d: ",t);
        int chk=0,spare=0;
        for(i=0;i<n;i++)
        {
            spare+=mst[i];
            if(mst[i]>300000000)
            {
                chk=1;
            }
        }
        if(chk)
        {
            printf("-1\n");
        }
        else
        {
            printf("%d\n",totalcost-spare);
        }
        t++;

    }
}
