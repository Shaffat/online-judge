#include<bits/stdc++.h>


using namespace std;

int solve(int row,int column,vector<vector<int> >&rowsum,vector<vector<int> >&columnsum,vector<vector<int> >&memory)
{
    if(row<=0 || column<=0)
    {
        return 0;
    }
    if(memory[row][column]!=-1)
    {
        return memory[row][column];
    }
    int ret1=rowsum[row][column]+solve(row-1,column,rowsum,columnsum,memory);
    int ret2=columnsum[column][row]+solve(row,column-1,rowsum,columnsum,memory);
    return memory[row][column]=max(ret1,ret2);

}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int r,c,i,j;
        scanf("%d %d",&r,&c);
        vector<int>columns(c+1,0);
        vector<int>rows(r+1,0);
        vector<int>demory(c+1,-1);
        vector<vector<int> >uranimum(r+1,columns);
        vector<vector<int> >radium(c+1,rows);
        vector<vector<int> >memory(r+1,demory);



        for(i=1;i<=r;i++)
        {
            int sum=0;

            for(j=1;j<=c;j++)
            {
                int k;
                scanf("%d",&k);
                sum+=k;
                uranimum[i][j]=sum;
            }
        }
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                int k;
                scanf("%d",&k);
                radium[j][i]=radium[j][i-1]+k;
            }
        }
        printf("Case %d: %d\n",t,solve(r,c,uranimum,radium,memory));
        t++;
    }
}
