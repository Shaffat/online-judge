#include<bits/stdc++.h>
using namespace std;
int total;
bool can_do(int len,int k,vector<int>&broken){
    int counter=0,i,j,last=-2e9 -7;
    total=0;
    for(i=0;i<broken.size();i++){
        if(broken[i]-last>=len){
            if(i>0)
             total+=broken[i-1]-last+1;
            counter++;
            last=broken[i];
        }
        //cout<<"k="<<k<<" last="<<last<<" cur="<<broken[i]<<endl;
        if(counter>k){
            total=2e9;
            return false;
        }
    }
    total+=broken[i-1]-last+1;
    return true;
}

int solve(int n,int m,int k,vector<int>&broken)
{
    int first=1,last=m+1,mid,res=2e9,f=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;
        mid=(first+last)/2;
        //cout<<"mid="<<mid<<endl;
        if(can_do(mid,k,broken)){
            //cout<<"ok"<<endl;
            last=mid-1;
            res=min(total,res);
        }
        else
            first=mid+1;
    }
    return res;
}

int main()
{
    int n,m,k,i,j,res;
    scanf("%d %d %d",&n,&m,&k);
    vector<int>broken;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        broken.push_back(j);
    }
    res=solve(n,m,k,broken);

    printf("%d\n",res);
}
