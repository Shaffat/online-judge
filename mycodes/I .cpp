#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j,m,k,t,test,total;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        int extra=(n*(n+1))/2,base;
        total=0;
        vector<int>coins;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            coins.push_back(j);
            total+=j;
        }
        if((total-extra)%n!=0)
        {
            printf("-1\n");
        }
        else
        {
            base=(total-extra)/n  +1;

            //cout<<"total="<<total<<" extra="<<extra<<" base="<<base<<endl;
            int res=0,give_;
            for(i=0;i<coins.size();i++)
            {
                res+=abs(coins[i]-base);
                //cout<<"add "<<abs(coins[i]-base)<<endl;
                base++;
            }
            printf("%d\n",res/2);
        }
    }
}
