#include<bits/stdc++.h>
using namespace std;
struct points
{
    int l,r;
};

int stopage(int row,int dir,vector<string>&lawn)
{
    int res=-1,i;
    if(dir==1)
    {
        for(i=lawn[row].size()-1;i>=0;i--)
        {
            if(lawn[row][i]=='W')
            {
                return i;
            }
        }
    }
    else
    {
        for(i=0;i<lawn[row].size();i++)
        {
            if(lawn[row][i]=='W')
            {
                return i;
            }
        }
    }
    return -1;
}
int main()
{
    ios_base::sync_with_stdio(0);
    int n,m,i,j;
    string s;
    vector<string>lawn;
    cin>>n>>m;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        lawn.push_back(s);
    }
    for(i=n-1;i>=0;i--)
    {
        int chk=0;
        for(j=0;j<lawn[i].size();j++)
        {
            if(lawn[i][j]=='W')
            {
                chk=1;
                break;
            }
        }
        if(!chk)
        {
            lawn.erase(lawn.begin()+i);
        }
        else
            break;
    }
    int start=0,stop,nextstart,dir=1,moves=0;
    for(i=0;i<lawn.size();i++)
    {
        stop=stopage(i,dir,lawn);
        if(stop==-1)
        {
            stop=start;
        }
        if(i<lawn.size()-1)
        {
            nextstart=stopage(i+1,dir,lawn);
            if(nextstart==-1)
            {
                nextstart=stop;
            }
        }
        else
        {
            nextstart=stop;
        }
        if(dir==1)
        {
            stop=max(stop,nextstart);
        }
        else
            stop=min(stop,nextstart);
        //cout<<"row="<<i<<" start="<<start<<" stop="<<stop<<endl;
        moves+=abs(start-stop);
        start=stop;
        dir=(dir+1)%2;
        if(i<lawn.size()-1)
        {
            moves++;
        }
    }
    cout<<moves<<endl;
}
