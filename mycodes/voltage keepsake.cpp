#include<bits/stdc++.h>

using namespace std;

struct run
{
    int a,b;
};

double solve(vector<run>&device,int supply)
{
    double high,low,mid,available,need,f=0,current,required,res=0;
    high=1e15;
    low=0;
    int i,j;
    while(high>=low)
    {
        //cout<<"high="<<high<<" low="<<low<<endl;
        if(f || abs(high-low)<1e-7)
        {
            break;
        }
        if(high==low)
        {
            f++;
        }
        mid=(high+low)/2.0;
        //cout<<"mid="<<mid<<endl;
        available=mid*supply;
        //cout<<"available="<<available<<endl;
        for(i=0;i<device.size();i++)
        {
            current=device[i].b;
            required=device[i].a*mid;
            if(current<required)
            {
                available-=abs(current-required);
            }
        }
        if(available<0)
        {
            high=mid;
        }
        else
        {
            low=mid;
            res=mid;
        }
    }
    return res;
}

int main()
{
    int i,j,supply,n,a,b;
    long long int required=0;
    double res;
    run tmp;
    scanf("%d %d",&n,&supply);
    vector<run>devices;
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&a,&b);
        tmp.a=a;
        tmp.b=b;
        required+=a;
        devices.push_back(tmp);

    }
    if(required>supply)
    res=solve(devices,supply);
    else
    res=-1;

    printf("%0.6lf\n",res);
}
