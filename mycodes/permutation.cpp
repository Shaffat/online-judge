
#include<bits/stdc++.h>
using namespace std;

int counter=0;
void permutation(string fixed,string current)
{
    int i,j;
    if(counter>10)
    {
        return;
    }
   if(current.size()==0)
   {
       //cout<<"1fixed="<<fixed<<" 1current="<<current<<endl;

       cout<<fixed<<endl;
       counter++;

       return;

   }
   for(i=0;i<current.size();i++)
   {

       fixed.push_back(current[i]);
       //cout<<"pushing in fixed "<<current[i]<<endl;
       string temp;
       for(j=0;j<current.size();j++)
       {
           if(j==i)
           {
               continue;
           }
           temp.push_back(current[j]);
       }
     // cout<<"fixed="<<fixed<<"   "<<"temp="<<temp<<endl;

       permutation(fixed,temp);
       //cout<<"function called ="<<fixed<<" "<<temp<<endl;
       fixed.erase(fixed.begin()+fixed.size()-1);


   }

}

int main()
{
    string s;
    cin>>s;
    cout<<"permutation-\n";
    permutation("",s);

    cout<<"\n"<<counter<<endl;
}
