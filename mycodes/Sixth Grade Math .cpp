#include<bits/stdc++.h>
using namespace std;
long long int GCD(long long int a,long long int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()
{
    int a,b,t=1,test;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&a,&b);
        int gcd=GCD(a,b);
        int lcm=a*b/gcd;
        printf("%d %d %d\n",t,lcm,gcd);
        t++;
    }

}
