
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


bool ok(ll mx,ll a,ll b,ll c)
{
    //cout<<"mx="<<mx<<" a="<<a<<" b="<<b<<" c="<<c<<endl;
    if(a>mx && c>mx)
        return false;
    ll tmp,i,j;
    if(a>mx)
    {
        //cout<<"a crossed mx"<<endl;
        i=mx-c;
        tmp=min(b,i);
        b-=tmp;
        b+=a-mx;
        c+=tmp;
        a=mx;
        //cout<<"a="<<a<<" b="<<b<<" c="<<c<<endl;
        if(max3(a,b,c)>mx) return false;
    }
    else if(c>mx)
    {
        //cout<<"c crossed mx"<<endl;
        i=mx-a;
        tmp=min(b,i);
        b-=tmp;
        b+=c-mx;
        a+=tmp;
        c=mx;
        //cout<<"a="<<a<<" b="<<b<<" c="<<c<<endl;
        if(max3(a,b,c)>mx) return false;
    }
    else if(b>mx)
    {
        //cout<<"b crossed mx"<<endl;
        i=mx-a;
        j=mx-c;
        b-=i+j;
        //cout<<"a="<<a<<" b="<<b<<" c="<<c<<endl;
        if(max3(a,b,c)>mx) return false;
    }
    return true;
}

int main()
{
    ll test,t,i,j,a,b,c,ans,a1,b1,c1,a2,b2,c2;
    sc1(test);
    while(test--)
    {
        sc3(a,b,c);
        ll first=1,last=1000,mid,d=0;
        while(first<=last)
        {
            if(d) break;
            if(first==last)
                d++;
            mid=(first+last)/2;
            if(ok(mid,a,b,c))
            {
                ans=mid;
                last=mid-1;
            }
            else
                first=mid+1;
        }
        printf("%lld\n",ans);
    }
}
