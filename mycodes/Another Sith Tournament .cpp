#include<bits/stdc++.h>
using namespace std;

double solve(int n,int bitmask,int alive,vector<vector<double> >&matrix,vector<vector<double> >&memory)
{
    //cout<<"bitmask="<<bitmask<<"alive="<<alive<<endl;
    if(bitmask==((1<<n)-1))
    {
        if(alive==0)
            return 1;
        else
            return 0;
    }
    if(memory[bitmask][alive]!=-1)
        return memory[bitmask][alive];
    int nxt,i,j;
    double res1=0,res2=0,res=0;
    for(i=0;i<n;i++)
    {
        if((bitmask&(1<<i))==0)
        {
            nxt=i;
            res1=solve(n,bitmask|(1<<nxt),nxt,matrix,memory);
            res1*=matrix[nxt][alive];
//            cout<<"memory["<<(bitmask|(1<<nxt))<<"]["<<alive<<"]="<<res<<endl;
//            cout<<"res1="<<res1<<endl;
            res2=solve(n,(bitmask|(1<<nxt)),alive,matrix,memory);
            res2*=matrix[alive][nxt];
//            cout<<"memory["<<(bitmask|(1<<nxt))<<"]["<<alive<<"]="<<res<<endl;
//            cout<<"res2="<<res2<<endl;
            res=max(res,res1+res2);
        }
    }
    //cout<<"memory["<<bitmask<<"]["<<alive<<"]="<<res<<endl;
    return memory[bitmask][alive]=res;
}

int main()
{
    int n,i,j;
    double p,res=0,cur=0;
    scanf("%d",&n);
    vector<double>col(n);
    vector<vector<double> >matrix(n,col);
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            scanf("%lf",&p);
            matrix[i][j]=p;
        }
    }
    vector<double>col1(n,-1);
    vector<vector<double> >memory((1<<n),col1);
    for(i=0;i<n;i++)
    {
        //cout<<"caliing from "<<i<<endl;
        cur=solve(n,(1<<i),i,matrix,memory);
        res=max(res,cur);
    }
    printf("%.8lf\n",res);

}
