#include<bits/stdc++.h>
using namespace std;

int gcd(int a,int b)
{
    while(a%b!=0)
    {
        int c,d;
        c=a%b;
        a=b;
        b=c;
    }
    return b;
}

int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,a,b,acount=0,bcount=0,common=0,i,j,lcm;
        scanf("%d %d %d",&n,&a,&b);
        lcm=(a*b)/gcd(a,b);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            if(j%lcm==0 && j>0)
                common++;
            else if(j%a==0 && j>0)
                acount++;
            else if(j%b==0 && j>0)
                bcount++;
        }
        if(common>0) acount++;
        if(acount>bcount) printf("BOB\n");
        else
            printf("ALICE\n");
    }
}
