#include<bits/stdc++.h>
using namespace std;
struct node
{
    int r,c;
};

bool is_valid(int &n,int &r,int &c)
{
    if(r<0 || r>=n || c<0 || c>=n)
        return false;
    return true;
}
void lvl_0_w(int n,queue<node>&q,vector<string>&maze,vector<vector<int> >&vis)
{
    //cout<<"for level 0 w"<<endl;
    int i,j;
    for(i=0;i<n;i++)
    {
        //cout<<"i="<<i<<endl;
        node tmp;
        if(maze[i][0]=='w')
        {
            tmp.r=i;
            tmp.c=0;
            //cout<<tmp.r<<" "<<tmp.c<<endl;
            q.push(tmp);
            vis[i][0]=1;
        }
    }
    //cout<<"lvl 0 done\n";
    return;
}
void lvl_0_b(int n,queue<node>&q,vector<string>&maze,vector<vector<int> >&vis)
{
     //cout<<"for level 0 b"<<endl;
    int i,j;
    for(i=0;i<n;i++)
    {
        node tmp;
        if(maze[0][i]=='b')
        {
            //cout<<"i="<<i<<endl;
            tmp.r=0;
            tmp.c=i;
            //cout<<tmp.r<<" "<<tmp.c<<endl;
            q.push(tmp);
            vis[0][i]=1;
        }
    }
    //cout<<"lvl 0 done\n";
    return;
}

int bfs(int &n,vector<string>&maze,char c)
{
    int i,j;
    queue<node>q;
    vector<int>col1(n+1,0);
    vector<vector<int> >vis(n+1,col1);
    vector<vector<int> >dis(n+1,col1);
    if(c=='w')
    {
        lvl_0_w(n,q,maze,vis);
    }
    else
    {
        lvl_0_b(n,q,maze,vis);
    }
    while(!q.empty())
    {
        node cur=q.front();
        //cout<<"cur="<<cur.r<<" "<<cur.c<<endl;
        q.pop();
        if(c=='w')
        {
            if(cur.c==n-1)
            {
                return dis[cur.r][cur.c];
            }
        }
        else
        {
            if(cur.r==n-1)
            {
                return dis[cur.r][cur.c];
            }
        }
        node nxt;
        nxt.r=cur.r-1;
        nxt.c=cur.c-1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0)
            {
                //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0){
            //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
            vis[nxt.r][nxt.c]=1;
            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
            q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0){
            //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
            vis[nxt.r][nxt.c]=1;
            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
            q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0){
            //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
            vis[nxt.r][nxt.c]=1;
            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
            q.push(nxt);
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0){
            //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
            vis[nxt.r][nxt.c]=1;
            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
            q.push(nxt);
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c+1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(maze[nxt.r][nxt.c]==c && vis[nxt.r][nxt.c]==0){
            //cout<<"nxt="<<nxt.r<<" "<<nxt.c<<endl;
            vis[nxt.r][nxt.c]=1;
            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
            q.push(nxt);
            }
        }
    }
    return 2e9;
}

int main()
{
    int n,i,j,t=1;
    ios_base::sync_with_stdio(0);
    while(cin>>n)
    {
        if(n==0)
        {
            return 0;
        }
        string s;
        vector<string>maze;
        for(i=1;i<=n;i++)
        {
            cin>>s;
            maze.push_back(s);
        }
        int B,W;
        B=bfs(n,maze,'b');
        W=bfs(n,maze,'w');
        //cout<<"B="<<B<<" W="<<W<<endl;
        if(B<W)
        {
            cout<<t<<" B\n";
        }
        else
            cout<<t<<" W\n";
        t++;
    }

}

