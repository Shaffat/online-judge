#include<bits/stdc++.h>
using namespace std;
int maskfinder(int n)
{
    int mask=0,i=1;
    while(n>0)
    {
        if(n%10==7 || n%10==4)
        {
            mask+=(n%10)*i;
            i*=10;
        }
        n/=10;
    }
    return mask;
}

int main()
{
    int a,b,i,c;
    cin>>a>>b;
    for(i=a+1;i<1e8;i++)
    {
        if(maskfinder(i)==b)
        {
            c=i;
            break;
        }
    }
    cout<<c<<endl;
}
