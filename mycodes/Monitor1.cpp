#include <bits/stdc++.h>

using namespace std;

#define lli unsigned long long int

lli gcd(lli a, lli b) {
    if (b == 0) return a;
    else {
        return gcd(b, a % b);
    }
}

int main() {

    lli a, b, x, y, r1, r2, Max, k;

    cin >> a >> b >> x >> y;

    k = gcd(x, y);

    x /= k;
    y /= k;

    r1 = a / x;
    r2 = b / y;

    cout << r1 << " " << r2 << endl;

    Max = min(r1, r2);

    cout << Max * x << " " << Max * y << endl;

    return (0);
}
