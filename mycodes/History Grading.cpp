#include<bits/stdc++.h>
using namespace std;

void adjust(string &s,vector<int>&v)
{
    //cout<<"s="<<s<<endl;
    int i,j=0;
    for(i=0;i<s.size();i++)
    {
        //cout<<"s["<<i<<"]="<<s[i]<<endl;
        if(s[i]==' ')
        {
            //cout<<"space j="<<j<<endl;
            v.push_back(j);
            j=0;
        }
        else
        {
            j*=10;
            j+=s[i]-'0';
            //cout<<"j="<<j<<endl;
        }
    }
    v.push_back(j);
    return ;
}

vector<int>col(20,-1);
vector<vector<int> >memory(20,col);
vector<vector<int> >testcase(20,col);
int t=0;

int solve(int l,int r,vector<int>&s1,vector<int>&s2)
{
    cout<<"l="<<l<<" r="<<r<<"s1="<<s1[l]<<" s2="<<s2[r]<<endl;
    int res;
    if(l>=s1.size()||r>=s2.size())
    {
        return 0;
    }
    if(testcase[l][r]==t)
    {
        return memory[l][r];
    }
    if(s1[l]==s2[r])
    {
        res=solve(l+1,r+1,s1,s2)+1;
    }
    else
        res=max(solve(l+1,r,s1,s2),solve(l,r+1,s1,s2));
    testcase[l][r]=t;
    cout<<"memory["<<l<<"]["<<r<<"]="<<memory[l][r]<<endl;
    return memory[l][r]=res;
}

int main()
{
    int n,i,j,res;
    string s1,s2;
    getline(cin,s1);
    while(getline(cin,s1))
    {
        cout<<"s1="<<s1<<endl;
        vector<int>v1;
        adjust(s1,v1);
        cout<<"s1 v=";
        for(i=0;i<v1.size();i++)
        {
            cout<<v1[i]<<" ";
        }
        cout<<endl;
        while(getline(cin,s2))
        {
            t++;
            vector<int>v2;
            if(s2.size()<=2)
            {
                break;
            }
            cout<<"s1="<<s1<<endl;
            cout<<"s2="<<s2<<endl;
            adjust(s2,v2);
            cout<<"s2 v=";
            for(i=0;i<v2.size();i++)
            {
                cout<<v2[i]<<" ";
            }
            cout<<endl;
            res=solve(0,0,v1,v2);
            printf("%d\n",res);
            t++;

        }
    }
}
