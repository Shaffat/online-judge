#include<bits/stdc++.h>
using namespace std;


int count_one(int n)
{
    int i,j,res=0;
    for(i=0;i<=log2(n);i++)
    {
        if((n&(1<<i))==(1<<i))
            res++;
    }
    return res;
}

int a,b,c,cbit,aone,bone;


int solve(int index,int carry,int a_one,int b_one,vector<vector<vector<vector<int> > > >&memory)
{
    if(a_one<0 || b_one<0)
    {
        return 0;
    }
    if(index>cbit)
    {
        if(carry==0 && a_one==0 && b_one==0)
        {
            return 1;
        }
        return 0;
    }
    if(memory[index][a_one][b_one][carry]!=-1)
    {
        return memory[index][a_one][b_one][carry];
    }
    int c_val,res=0;
    if((c&(1<<index))==0)
    {
        c_val=0;
    }
    else
        c_val=1;
    if(c_val)
    {
        if(carry)
        {
            res+=solve(index+1,0,a_one,b_one,memory);
            res+=solve(index+1,1,a_one-1,b_one-1,memory);
        }
        else
        {
            res+=solve(index+1,0,a_one-1,b_one,memory);
            res+=solve(index+1,0,a_one,b_one-1,memory);
        }
    }
    else
    {
        if(carry)
        {
            res+=solve(index+1,1,a_one-1,b_one,memory);
            res+=solve(index+1,1,a_one,b_one-1,memory);
        }
        else
        {
            res+=solve(index+1,0,a_one,b_one,memory);
            res+=solve(index+1,1,a_one-1,b_one-1,memory);
        }
    }
    return memory[index][a_one][b_one][carry]=res;
}

int main()
{
    int test,t,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int res=0;
        scanf("%d %d %d",&a,&b,&c);
        if((a+b)==c)
        {
            cbit=log2(c);
            aone=count_one(a);
            bone=count_one(b);
            vector<int>col1(2,-1);
            vector<vector<int> >col2(bone+1,col1);
            vector<vector<vector<int> > >col3(aone+1,col2);
            vector<vector<vector<vector<int> > > >memory(cbit+1,col3);
            res=solve(0,0,aone,bone,memory);

        }
        printf("%d\n",res);
    }
}
