#include<bits/stdc++.h>
using namespace std;

#define ll long long int
int n,m;
ll solve(int pos,int pre,vector<int>&numbers,vector<vector<ll> >&memory)
{
    if(pos>n)
    {
        return 1;
    }
    if(memory[pos][pre]!=-1)
    {
        return memory[pos][pre];
    }
    ll res=0,i,j;
    for(i=0;i<numbers.size();i++)
    {
        if(abs(pre-numbers[i])<=2)
        {
            res+=solve(pos+1,numbers[i],numbers,memory);
        }
    }
    return memory[pos][pre]=res;

}

int main()
{
    int test,t,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&m,&n);
        vector<int>numbers;
        vector<ll>col1(11,-1);
        vector<vector<ll> >memory(11,col1);
        for(i=1;i<=m;i++)
        {
            scanf("%d",&j);
            numbers.push_back(j);
        }
        ll res=0;
        for(i=0;i<numbers.size();i++)
        {
            res+=solve(2,numbers[i],numbers,memory);
        }
        printf("Case %d: %lld\n",t,res);
    }
}
