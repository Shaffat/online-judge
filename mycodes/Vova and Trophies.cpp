#include<bits/stdc++.h>
using namespace std;

int solve(string &s,vector<int>&total_g,int n)
{
    //cout<<"total="<<n<<endl;
    int first=0,last=s.size(),mid,i,j,res=0,cur,f=0,g;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        int done=0;
        cur=0;
        //cout<<"first="<<first<<" last="<<last<<" mid="<<mid<<endl;
        for(i=0;i<=s.size()-mid;i++)
        {
            //cout<<"start="<<i<<" ";
            g=total_g[mid+i]-total_g[i];
            //cout<<"g ="<<g<<endl;
            if(g>=mid-1)
            {
                done=1;
                if(g==mid-1)
                {
                    if(g<n)
                        res=max(res,mid);
                    else
                        res=max(res,mid-1);
                }
                else
                    res=max(res,mid);
            }
        }
        if(done)
            first=mid+1;
        else
            last=mid-1;

    }
    return res;
}



int main()
{
    int n,i,j,g=0,c;
    string s;
    cin>>n>>s;
    vector<int>sum(s.size()+1,0);
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='G') {
            c=1;
            g++;
        }
        else
            c=0;
        sum[i+1]=sum[i]+c;
    }
    cout<<solve(s,sum,g);
}
