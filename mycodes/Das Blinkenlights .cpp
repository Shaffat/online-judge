#include<bits/stdc++.h>

using namespace std;


int gcd(int a,int b)
{
    while(a%b!=0)
    {
        int c=a%b;
        a=b;
        b=c;
    }
    return b;
}

int main()
{
    int p,q,s,res;
    scanf("%d %d %d",&p,&q,&s);
    res=(p*q)/gcd(p,q);
    if(res<=s)
    {
        cout<<"yes"<<endl;
    }
    else
        cout<<"no"<<endl;
}
