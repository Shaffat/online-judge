#include<bits/stdc++.h>
using namespace std;
vector<int>memory(1e5,-1);
int solve(int n)
{
    int i,j,x;
    x=log2(n);
    int w=2*(n-(1<<x))+1;
    if(w==n)
        return 2*n;
    if(memory[n]!=-1)
        return memory[n];
    return solve(w)+n-w;
}

int main()
{
    int n;
    while(scanf("%d",&n)!=EOF)
    {
        int res=solve(n);
        printf("%d\n",res);
    }
}
