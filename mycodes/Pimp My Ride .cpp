#include<bits/stdc++.h>
using namespace std;

int solve(int bitmask,int n,vector<vector<int> >&price,vector<int>&memory)
{
    if(bitmask==(1<<n)-1)
    {
        //cout<<"returning bitmask="<<(1<<(n))-1<<endl;
        return 0;
    }
    if(memory[bitmask]!=-1)
    {
        //cout<<"memorization"<<endl;
        return memory[bitmask];
    }
    int i,j,res=1e9;
    for(i=0;i<n;i++)
    {
        //cout<<"bitmask="<<bitmask<<" can i take i="<<i<<"  job="<<(bitmask&(1<<i))<<endl;
        if((bitmask&(1<<i))==0)
        {
            //cout<<"take i="<<i<<" job"<<endl;
            int cur=price[i][i];
            for(j=0;j<n;j++)
            {

                if(i==j)
                {
                    continue;
                }
                //cout<<"is j="<<j<<" done before="<<(bitmask&(1<<j))<<" equal "<<(1<<j)<<endl;
                if(((bitmask&(1<<j))==(1<<j)))
                   {
                       //cout<<"extra cost for job "<<j<<endl;
                       cur+=price[i][j];
                   }
            }
            //cout<<"calling bitmask="<<(bitmask|(1<<i))<<" cur="<<cur<<endl;
            cur+=solve((bitmask|(1<<i)),n,price,memory);
            res=min(cur,res);
        }
    }
    //cout<<"memory["<<bitmask<<"]="<<res<<endl;
    return memory[bitmask]=res;
}

int main()
{
    int test,t,n,i,j,p;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>col(n+1,-1);
        vector<vector<int> >price(n+1,col);
        vector<int>memory(1<<(n+1),-1);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                scanf("%d",&p);
                price[i][j]=p;
            }
        }
        int res=solve(0,n,price,memory);
        printf("Case %d: %d\n",t,res);
    }
}
