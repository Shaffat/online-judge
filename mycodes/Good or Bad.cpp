#include<bits/stdc++.h>

using namespace std;

int is_bad(string &s)
{
    int i,j,mxconstant=0,mxvowel=0,tmp=0;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='A'|| s[i]=='E' || s[i]=='I' || s[i]=='O' || s[i]=='U'||s[i]=='?')
        {
            tmp++;
            mxvowel=max(mxvowel,tmp);
        }
        else
            tmp=0;
    }
    tmp=0;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='A' || s[i]=='E' || s[i]=='I' || s[i]=='O' || s[i]=='U')
        {
            tmp=0;
        }
        else
        {
            tmp++;
            mxconstant=max(mxconstant,tmp);
        }
    }
    if(mxconstant>=5 || mxvowel>=3)
    {
        return 1;
    }
    return 0;
}

int is_good(string &s,int index,int type,int amount,vector<vector<vector<int> > >&memory)
{
  if(type==0)
  {///constant
      if(amount>=5)
      {
          return 0;
      }
  }
  if(type==1)
  {
      ///vowel
      if(amount>=3)
      {
          return 0;
      }
  }
  if(index>=s.size())
  {
      return 1;
  }
  if(memory[index][type][amount]!=-1)
  {
      return memory[index][type][amount];
  }
  int res;
  if(s[index]=='?')
  {
      res=max(is_good(s,index+1,type,amount+1,memory),is_good(s,index+1,(type+1)%2,1,memory));
  }
  else if(s[index]=='A' || s[index]=='E' || s[index]=='I' || s[index]=='O' || s[index]=='U')
  {
      if(type==1)
      {
          res=is_good(s,index+1,type,amount+1,memory);
      }
      else
        res=is_good(s,index+1,1,1,memory);
  }
  else
  {
      if(type==0)
      {
          res=is_good(s,index+1,type,amount+1,memory);
      }
      else
        res=is_good(s,index+1,0,1,memory);
  }
  return memory[index][type][amount]=res;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        int good,bad;
        cin>>s;
        bad=is_bad(s);
        vector<int>col1(51,-1);
        vector<vector<int> >col2(2,col1);
        vector<vector<vector<int> > >memory(s.size(),col2);
        good=is_good(s,0,0,0,memory);
//        cout<<"bad="<<bad<<" good="<<good<<endl;
        if(good==1 && bad==1)
        {
            cout<<"Case "<<t<<": MIXED"<<endl;
        }
        else if(good)
        {
            cout<<"Case "<<t<<": GOOD"<<endl;
        }
        else
            cout<<"Case "<<t<<": BAD"<<endl;
    }
}
