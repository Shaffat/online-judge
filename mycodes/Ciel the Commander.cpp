#include<bits/stdc++.h>
using namespace std;

///count nodes of a connected component
int countNode(int node,vector<vector<int> >&connection,vector<int>&vis,vector<int>&centroid){
    int i,j,child,nxt,ok=1,total=1;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        if(!vis[nxt] && !centroid[nxt]){
            vis[nxt]=1;
            child=0;
            child=countNode(nxt,connection,vis,centroid);
            total+=child;
        }
    }
    return total;
}

///dfs build path from root to centroid
int dfs(int node,int n,vector<vector<int> >&connection,vector<int>&hasMore,vector<int>&vis){
    int i,j,child,nxt,ok=1,total=1;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        if(!vis[nxt]){
            vis[nxt]=1;
            child=0;
            child=dfs(nxt,n,connection,hasMore,vis);
            if(child>(n/2)){
                ok=0;
                hasMore[node]=nxt;
            }
            total+=child;
        }
    }
    if(ok){
        hasMore[node]=node;
    }
    return total;
}

///find centroid from path
int findCentroid(vector<int>&hasMore,int n,int root){
    while(hasMore[root]!=root){
        root=hasMore[root];
    }
    return root;
}

///build centroids of all levels in centroid tree
int buildCentroidLevels(int n,vector<vector<int> >&connections,vector<vector<int> >&levels)
{
    vector<int>centroids(n+1,0);
    int i,j,ok;
    for(i=1;i<=40;i++)
    {
        ok=0;
        vector<int>vis(n+1,0);
        vector<int>hasMore(n+1,0);
        for(j=1;j<=n;j++){
            if(centroids[j]){
                vis[j]=1;
            }
            hasMore[j]=i;
        }
        vector<int>counter(n+1,0);
        vector<int>vis1(n+1,0);
        for(j=1;j<=n;j++){
            if(!vis1[j] && !centroids[j]){
                vis1[j]=1;
                counter[j]=countNode(j,connections,vis1,centroids);
            }
        }
        for(j=1;j<=n;j++){
            if(vis[j]==0){
                int cnt=counter[j];
                hasMore[j]=j;

                ok=1;
                vis[j]=1;
                dfs(j,cnt,connections,hasMore,vis);
                int c= findCentroid(hasMore,n,j);

                centroids[c]=1;
                levels[i].push_back(c);

            }
        }
        if(ok==0){
            return i-1;
        }
    }
}


int main()
{
    int n,i,j,u,v;
    scanf("%d",&n);
    vector<int>col;
    vector<vector<int> >connection(n+1);
    for(i=1;i<n;i++){
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }
    vector<vector<int> >levels(35,col);
    int x=buildCentroidLevels(n,connection,levels);
    if(x>26){
        printf("Impossible!");
    }
    else{
        vector<char>res(n+1);
        for(i=1;i<=x;i++){
            for(j=0;j<levels[i].size();j++){
                int k=levels[i][j];
                res[k]='A'+i-1;
            }
        }
        for(i=1;i<=n;i++){
            printf("%c ",res[i]);
        }
    }
}
