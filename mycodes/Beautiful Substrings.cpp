
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

ll counter(ll x, vl &pos)
{
    //cout<<"x="<<x<<endl;
    ll first,last,mid,i,j,f=0,res=-1;
    first=0;
    last=pos.size()-1;
    //cout<<"size="<<pos.size()<<endl;
    while(first<=last)
    {
        if(f) break;
        if(first==last)
            f++;
        mid=(first+last)/2;
        if(pos[mid]>=x)
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    if(res==-1)
        return 0;
    res=pos.size()-res;
    return res;
}


int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,n,m,k,res,c,i,j;
    string a,b;
    cin>>test;
    while(test--)
    {
        cin>>n>>m>>k;
        vector<ll>col(200,0);
        vector<vector<ll> >choices(200,col);
        vector<ll>col1;
        vector<vector<ll> >options(200,col1);
        cin>>a>>b;

        res=0;
        for(i=0;i<=a.size()-k;i++)
        {
            ll x,y;
            x=a[i];
            y=a[i+k-1];
            choices[x][y]=1;
        }
        for(i=0;i<b.size();i++)
        {
            ll x,y;
            x=b[i];
            options[x].push_back(i);
        }
        for(i='a';i<='z';i++)
        {
            for(j='a';j<='z';j++)
            {
                if(choices[i][j])
                {
                    for(c=0;c<options[i].size();c++)
                    {
                        ll x=options[i][c];
                        //cout<<"selected pair "<<char(i)<<" , "<<char(j)<<" pos="<<x<<endl;
                        res+=counter(x,options[j]);
                    }
                }
            }
        }
        cout<<res<<endl;
    }
}
