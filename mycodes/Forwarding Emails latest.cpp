#include<bits/stdc++.h>
using namespace std;
int timer;
void erase_cycle(int cur,vector<vector<int> >&connection,vector<int>&start,
                 vector<int>&low,vector<int>&vis,vector<int>&parent,vector<int>&child)
{
    //cout<<"in earse cur="<<cur<<endl;
    int i,j,nxt;
    start[cur]=timer;
    low[cur]=timer;
    for(i=0;i<connection[cur].size();i++)
    {
        nxt=connection[cur][i];
        //cout<<"nxt="<<nxt<<endl;
        if(!vis[nxt])
        {
            vis[nxt]=1;
            erase_cycle(nxt,connection,start,low,vis,parent,child);
            if(low[nxt]<=start[cur])
            {
                parent[cur]=parent[nxt];
            }
        }
        else
        {
            cout<<"cycle from "<<cur<<" to "<<nxt<<endl;
            low[cur]=min(low[cur],start[nxt]);
            if(low[cur]<=start[cur])
            {
                parent[cur]=nxt;
            }
        }
    }
    child[parent[cur] ]=child[cur]+1;
    return;
}




int dfs(int cur,vector<int>&vis,vector<int>&child,vector<vector<int> >connection,vector<int>&parent,vector<int>&memory)
{
    cout<<"cur="<<cur<<" parent="<<parent[cur]<<endl;
    int i,j,nxt,total=child[parent[cur]];
    vis[parent[cur]]=1;
    //cout<<"visited of parent="<<vis[parent[cur]]<<endl;
    if(memory[parent[cur]]!=-1)
        return memory[parent[cur]];
    for(i=0;i<connection[cur].size();i++)
    {
        nxt=connection[cur][i];
        cout<<"nxt="<<nxt<<" parent of nxt="<<parent[nxt]<<endl;
        if(vis[parent[nxt]]==0)
        {
            total+=dfs(parent[nxt],vis,child,connection,parent,memory);
        }
    }
    cout<<"total of "<<cur<<" = "<<total<<endl;
    return memory[parent[cur]]=total;

}
int main()
{
    int test,t,n,m,i,j,u,v;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        vector<int>parent(n+1);
        vector<int>child(n+1,0);
        vector<int>start(n+1);
        vector<int>low(n+1);
        vector<int>vis(n+1,0);
        vector<int>memory(n+1,-1);
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            parent[i]=i;
        }
        timer=0;
        for(i=1;i<=n;i++)
        {
            if(vis[i]==0)
            {
                vis[i]=1;
                erase_cycle(i,connection,start,low,vis,parent,child);
            }
        }
        cout<<"done"<<endl;

        for(i=1;i<=n;i++)
        {
            cout<<"parent of "<<i<<" = "<<parent[i]<<endl;
            cout<<"child in cycle of "<<i<<" = "<<child[parent[i]]<<endl;
            vis[i]=0;
        }
        int mx=-1,node=1e9;
        for(i=1;i<=n;i++)
        {
            cout<<"solving"<<endl;
            int cur=dfs(i,vis,child,connection,parent,memory);
            cout<<"from "<<i<<" u can send "<<cur<<" persons"<<endl;
            if(cur>=mx)
            {
                if(cur==mx)
                {
                    node=min(i,node);
                }
                else
                    node=i;
            }
        }
        printf("Case %d: %d\n",t,node);
    }
}
