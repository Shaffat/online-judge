
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int main()
{

    ll n,i,j,r1,c1,r2,c2,q;


    sc2(n,q);
    vl rowcumulativeeven(n+1,0);
    vl rowcumulativeodd(n+1,0);
    vl colcumulativeeven(n+1,0);
    vl colcumulativeodd(n+1,0);
    FOR(i,1,n)
    {
        sc1(j);
        if(j%2)
        {
           rowcumulativeodd[i]=rowcumulativeodd[i-1]+1;
           rowcumulativeeven[i]=rowcumulativeeven[i-1];
        }
        else
        {
           rowcumulativeodd[i]=rowcumulativeodd[i-1];
           rowcumulativeeven[i]=rowcumulativeeven[i-1]+1;
        }

    }
    FOR(i,1,n)
    {
        sc1(j);
        if(j%2)
        {
           colcumulativeodd[i]=colcumulativeodd[i-1]+1;
           colcumulativeeven[i]=colcumulativeeven[i-1];
        }
        else
        {
           colcumulativeodd[i]=colcumulativeodd[i-1];
           colcumulativeeven[i]=colcumulativeeven[i-1]+1;
        }

    }
    //cout<<"here "<<q<<endl;
    while(q--)
    {
        sc2(r1,c1);
        sc2(r2,c2);
        if(r1<r2)
            swap(r1,r2);
        if(c1<c2)
            swap(c1,c2);
        if(rowcumulativeeven[r1]-rowcumulativeeven[r2-1]==r1-r2+1 && colcumulativeeven[c1]-colcumulativeeven[c2-1]==c1-c2+1)
        {
            printf("YES\n");
        }
        else if(rowcumulativeodd[r1]-rowcumulativeodd[r2-1]==r1-r2+1 && colcumulativeodd[c1]-colcumulativeodd[c2-1]==c1-c2+1)
        {
            printf("YES\n");
        }
        else
            printf("NO\n");

    }

}
