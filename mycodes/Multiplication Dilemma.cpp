
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


void specialBreakdown(ll n, vl &v)
{
    ll i,j,p=1,dir=1;
    if(n<0){
        dir=-1;
        n*=-1;
    }
    while(n>0)
    {
        ll tmp,num;
        tmp=n%10;
        num=tmp*p;
        p*=10;
        n/=10;
        if(num>0)
        v.push_back(dir*num);
    }
    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,i,j,a,b;
    cin>>test;
    while(test--)
    {
        cin>>a>>b;
        vector<ll>numbersa;
        vector<ll>numbersb;
        specialBreakdown(a,numbersa);
        specialBreakdown(b,numbersb);
        for(i=0;i<numbersa.size();i++)
        {
            if(i>0)
                cout<<" + ";
            for(j=0;j<numbersb.size();j++)
            {
                cout<<numbersa[i]<<" x "<<numbersb[j];
                if(j<numbersb.size()-1)
                    cout<<" + ";
            }
        }
        cout<<endl;
    }
}
