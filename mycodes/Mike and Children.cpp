#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,n,mx=0;
    scanf("%d",&n);
    vector<int>total(1e6 + 1,0);
    vector<int>v;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v.push_back(j);
    }
    for(i=0;i<v.size()-1;i++)
    {
        for(j=i+1;j<v.size();j++)
        {
            total[v[i]+v[j]]++;
        }
    }
    for(i=0;i<=1e6;i++)
    {
        mx=max(total[i],mx);
    }
    cout<<mx<<endl;
}
