#include<bits/stdc++.h>
using namespace std;

int intconvert(string s,string order)
{
    //cout<<"calculate"<<endl;
    int i,j=1,sum=0,pos;
    for(i=order.size()-1;i>=0;i--)
    {
        pos=order[i]-'0';
        //cout<<"pos="<<pos<<" ";
        sum+=j*(s[pos]-'0');
        //cout<<"sum="<<sum<<endl;
        j*=10;
    }
    return sum;
}
bool ok(string s,string order)
{
    int zero=0,nonozero=0,i,j,pos;
    pos=order[0]-'0';
    if(s[pos]=='0')
    {
        return false;
    }
    return true;
}
int main()
{
    int a,b,c=-1,tmp,i=0;
    cin>>a>>b;
    string s,order;
    tmp=a;
    while(tmp>0)
    {
        s.insert(s.begin()+0,'0'+tmp%10);
        order.push_back(i+'0');
        i++;
        tmp/=10;
    }
    if(ok(s,order))
    {
        //cout<<"order="<<order<<endl;;
        tmp=intconvert(s,order);
        //cout<<"sum="<<tmp<<endl;
        if(tmp<=b)
        {
            c=max(c,tmp);
        }
    }
    while(next_permutation(order.begin(),order.end()))
    {
        //cout<<"order="<<order<<endl;
        if(ok(s,order))
        {
            tmp=intconvert(s,order);
            //cout<<"sum="<<tmp<<endl;
            if(tmp<=b)
            {
                c=max(c,tmp);
            }
        }
    }
    cout<<c<<endl;
}
