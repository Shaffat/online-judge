#include<bits/stdc++.h>
using namespace std;

int main()
{


    int nodes,edges;
    while(cin>>nodes)
    {


        vector<int>coloured(400);
        vector<int>colour(400);
        vector<int>graph[400];

        if(nodes==0)
        {
            break;
        }
        cin>>edges;
        int i,j;
        for(i=1;i<=edges;i++)
        {
            int u,v;
            cin>>u>>v;
            graph[u].push_back(v);
            graph[v].push_back(u);
        }
        coloured.at(0)=1;
        colour.at(0)=1;
        queue<int>bfs;
        bfs.push(0);
        int bi=1;
        while(!bfs.empty())
        {
            int current=bfs.front();
            bfs.pop();
            for(int k=0;k<graph[current].size();k++)
            {
                if(coloured.at(graph[current].at(k))==1)
                {
                    if(colour.at(current)==colour.at(graph[current].at(k)))
                    {
                        bi=0;
                        break;
                    }
                }
                else
                {
                    bfs.push(graph[current].at(k));
                    colour.at(graph[current].at(k))=(colour.at(current)+1)%2;
                    coloured.at(graph[current].at(k))=1;
                }
            }
        }
        if(!bi)
        {
            cout<<"NOT BICOLORABLE."<<endl;

        }
        else
        {
            cout<<"BICOLORABLE."<<endl;
        }

    }
}
