#include<bits/stdc++.h>

using namespace std;

int main()
{
    int test,i;
    scanf("%d",&test);
    i=1;
    while(i<=test)
    {
        int cities,roads,j,k,l,m;
        scanf("%d %d",&cities,&roads);
        vector<int>pomcities(cities+100);
        vector<int> roadmap[cities+100];
        for(j=1;j<=roads;j++)
        {
            int u,v;
            scanf("%d %d",&u,&v);
            roadmap[u].push_back(v);
        }
        for(j=1;j<=cities;j++)
        {
            vector<int>vis(cities+100);
            queue<int>bfs;
            bfs.push(j);
             while(!bfs.empty())
             {
                 int current=bfs.front();
                 bfs.pop();
                 for(m=0;m<roadmap[current].size();m++)
                 {
                     if(!vis[roadmap[current].at(m)])
                     {
                         if(roadmap[current].at(m)==j)
                         {
                             pomcities.at(j)=1;
                             break;
                         }
                         else
                         {
                             bfs.push(roadmap[current].at(m));
                             vis[roadmap[current].at(m)]=1;
                         }
                     }
                 }
             }
        }
        int counter=0;
        for(m=1;m<=cities;m++)
        {
            if(!pomcities[m])
            {
                counter++;
            }
        }
        printf("Case %d: %d\n",i,counter);
        i++;

    }
}
