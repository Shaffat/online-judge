#include<bits/stdc++.h>
using namespace std;

typedef unsigned long long int llu;

llu solve(llu n,llu k,llu a,llu b)
{
    llu total=0,next,low;
    //cout<<"n="<<n<<endl;
    while(n!=1)
    {
        if(n%k==0)
        {
            next=n/k;
            //cout<<"next="<<next<<endl;
            if(b<=(n-next)*a)
            {
                total+=b;
            }
            else
            {
                total+=(n-next)*a;
            }
            n/=k;
            //cout<<"1 n="<<n<<" total="<<total<<endl;
        }
        else
        {
            if(n<k)
            {
                total+=(n-1)*a;
                n=1;
                //cout<<"2 n="<<n<<" total="<<total<<endl;
            }
            else
            {
                low=(n/k)*k;
                total+=(n-low)*a;
                n=low;
                //cout<<"3 n="<<n<<" total="<<total<<endl;
            }
        }
    }
    return total;
}

int main()
{
    llu n,k,a,b,total;
    cin>>n>>k>>a>>b;
    if(k==1)
    {
        total=(n-1)*a;
    }
    else
    {
        total=solve(n,k,a,b);
    }
    cout<<total<<endl;
}
