#include<bits/stdc++.h>
using namespace std;
#define ll long long int


bool is_prime(ll n)
{
    ll i,j;
    for(i=2;i<=sqrt(n);i++)
    {
        if(n%i==0)
        {
            return false;
        }
    }
    return true;
}

int main()
{
    ll test,t,a,b;
    scanf("%lld",&test);
    while(test--)
    {
        scanf("%lld %lld",&a,&b);
        if(a-b==1){
        if(is_prime(a+b))
        {
            printf("YES\n");
        }
        else
            printf("NO\n");
        }
        else
            printf("NO\n");
    }
}
