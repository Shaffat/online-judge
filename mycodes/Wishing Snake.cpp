#include<bits/stdc++.h>
using namespace std;

struct node{
    int id,start,finish;
};

struct superNode
{
    int id,inDegree,outDegree;
};

bool operator <(node a, node b)
{
    if(a.finish!=b.finish)
        return a.finish>b.finish;
    return false;
}


int t;
void topsort(int id,vector<vector<int> >&connection,vector<node>&watch)
{
    watch[id].start=t;
    t++;
    int i,j;
    for(i=0;i<connection[id].size();i++)
    {
        j=connection[id][i];
        if(watch[j].start==-1)
        topsort(j,connection,watch);
    }
    //cout<<"finish of "<<id<<" is "<<t<<endl;
    watch[id].finish=t;
    t++;
    return;

}

void findSCC(int id,int root,vector<vector<int> >&revConnection,vector<int>&parent,vector<int>&vis)
{
    int i,j;
    parent[id]=root;
    //cout<<"id is "<<id<<" root is "<<root<<endl;
    for(i=0;i<revConnection[id].size();i++)
    {
        j=revConnection[id][i];
        if(!vis[j])
        {
            vis[j]=1;
            findSCC(j,root,revConnection,parent,vis);
        }
    }
    return ;
}

bool isSicle(int n,int snkroot,vector<superNode>&superNodes,vector<int>&parent)
{
    int rootCount=0,i,j,root;
    for(i=0;i<=n;i++)
    {
        if(parent[i]==i)
        {
            //cout<<i<<" is a supernode"<<endl;
            if(superNodes[i].inDegree<=1 && superNodes[i].outDegree<=1)
            {
                if(superNodes[i].inDegree==0){
                    root=i;
                    rootCount++;
                }
            }
            else
                return false;
        }
    }

    if (rootCount==1 && root==snkroot) return true;
    return false;
}


bool ansme(int n,vector<vector<int> >&connection,vector<int>&parent,int snkroot)
{
    vector<superNode>superNodes(n+1);

    vector<int>roots;
    int i,j;

    for(i=1;i<=n;i++)
    {
        superNode tmp;
        tmp.id=i;
        tmp.inDegree=0;
        tmp.outDegree=0;
        superNodes[i]=tmp;
    }

    for(i=0;i<=n;i++)
    {
        for(j=0;j<connection[i].size();j++)
        {
            int child=connection[i][j];
            //cout<<"connection from "<<i<<" to "<<child<<" parents "<<parent[i]<<" and "<<parent[child]<<endl;
            if(parent[i]!=parent[child])
            {
                superNodes[child].inDegree++;
                superNodes[parent[i]].outDegree++;
            }
        }
    }
    if(snkroot!=-1)
    {
        snkroot=parent[snkroot];
    }

    return isSicle(n,snkroot,superNodes,parent);
}

bool solve(int n,vector<vector<int> >&connection,vector<vector<int> >&revConnection,int snkroot)
{

    t=0;
    int i,j;
    vector<node>watch(n+1);
    vector<int>parent(n+1);
    for(i=0;i<=n;i++)
    {
        parent[i]=i;
        node tmp;
        tmp.id=i;
        tmp.start=-1;
        tmp.finish=-1;
        watch[i]=tmp;
    }

    for(i=0;i<=n;i++)
    {
        //cout<<"watch start="<<watch[i].start<<endl;
        if(watch[i].start==-1)
        {
            //cout<<"running topsort from "<<i<<endl;
            topsort(i,connection,watch);
        }
    }
    sort(watch.begin(),watch.end());
    vector<int>vis(n+1,0);
    for(i=0;i<=n;i++)
    {
        int id=watch[i].id;
        if(vis[id]==0)
        {
            vis[id]=1;
            //cout<<"root="<<id<<endl;
            findSCC(id,id,revConnection,parent,vis);
        }
    }

    return ansme(n,connection,parent,snkroot);
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,i,j,n,k,u,v,q,c,ok;
    scanf("%d",&test);
    for(q=1;q<=test;q++)
    {
        ok=0;
        c=0;
        vector<int>mapping(2000,-1);
        scanf("%d",&n);
        vector<vector<int> >connection(1000);
        vector<vector<int> >revConnection(1000);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&k);
            for(j=1;j<=k;j++)
            {
                ok++;
                scanf("%d %d",&u,&v);
                if(mapping[u]==-1)
                {
                    mapping[u]=c;
                    c++;
                }
                if(mapping[v]==-1)
                {
                    mapping[v]=c;
                    c++;
                }
                //cout<<"real connection "<<mapping[u]<<" to "<<mapping[v]<<endl;
                connection[mapping[u]].push_back(mapping[v]);
                revConnection[mapping[v]].push_back(mapping[u]);
                ok++;
            }
        }

        bool res;
        res=solve(c-1,connection,revConnection,mapping[0]);
        if(res || (ok==0))
        {
            printf("Case %d: YES\n",q);
        }
        else
            printf("Case %d: NO\n",q);
    }
}
