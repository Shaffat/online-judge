#include<bits/stdc++.h>
using namespace std;
int And(vector<int>&res)
{
    int i,j,f=res[0];
    for(i=1;i<res.size();i++)
    {
        if(f==1 && res[i]==1)
        {
            f=1;
        }
        else
        {
            f=0;
        }
    }
    return f;
}
int Or(vector<int>&res)
{
    int i,j,f=res[0];
    for(i=1;i<res.size();i++)
    {
        if(f==1 || res[i]==1)
        {
            f=1;
        }
        else
            f=0;
    }
    return f;
}

int Xor(vector<int>&res)
{
    int i,j,counter=0;
    for(i=0;i<res.size();i++)
    {
        if(res[i]==1)
        {
            counter++;
        }
    }
    return counter%2;
}
int invert(int num)
{
    return (num+1)%2;
}

int solve(int gate,vector<vector<int> >&dependency,vector<int>&memory,vector<int>&type)
{
    int i,j;
    if(memory[gate]!=-1)
    {
        return memory[gate];
    }
    vector<int>res;
    for(i=0;i<dependency[gate].size();i++)
    {
        res.push_back(solve(dependency[gate][i],dependency,memory,type));
    }
    if(type[gate]==1)
    {
        return memory[gate]=And(res);
    }
    else if(type[gate]==2)
    {
        return memory[gate]=Or(res);
    }
    else if(type[gate]==3)
    {
        return memory[gate]=Xor(res);
    }
    else if(type[gate]==4)
    {
        return memory[gate]=invert(res[0]);
    }
}


int main()
{
    int n,m,i,j,u,v;
    scanf("%d %d",&n,&m);
    map<string,int>mp;
    mp["And"]=1;
    mp["Or"]=2;
    mp["Xor"]=3;
    mp["Invert"]=4;
    string s;
    vector<int>type(n+1);
    vector<int>memory(n+1,-1);
    vector<vector<int> >depends(n+1);
    memory[0]=1;
    for(i=1;i<=n;i++)
    {
        string s;
        cin>>s;
        type[i]=mp[s];
    }
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        depends[v].push_back(u);
    }
    for(i=1;i<=n;i++)
    {
        solve(i,depends,memory,type);
        printf("%d\n",memory[i]);
    }
}
