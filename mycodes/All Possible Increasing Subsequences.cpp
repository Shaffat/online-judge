#include<bits/stdc++.h>
using namespace std;

#define ll long long int

struct pos
{
    ll val;
    ll pos;
};
ll total;
bool operator <(pos a,pos b)
{
    if(a.val!=b.val)
    {
        return a.val<b.val;
    }
    return a.pos<b.pos;
}

void update(ll index,ll value,ll n,vector<ll>&tree)
{
    while(index<=n)
    {
        tree[index]+=value;
        tree[index]%=1000000007;
        index+=index&(-index);
    }
    return;
}

ll query(ll index,vector<ll>&tree)
{
    ll sum=0;
    while(index>0)
    {
        sum+=tree[index];
        sum%=1000000007;
        index-=index&(-index);
    }
    return sum;
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll test,t,i,j,n;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        total=0;
        scanf("%lld",&n);
        pos tmp;
        ll counter=1,pre=-1,pre_total=0;
        vector<pos>all_pos;
        vector<ll>tree(n+1,0);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            tmp.pos=i;
            tmp.val=j;
            all_pos.push_back(tmp);
        }
        sort(all_pos.begin(),all_pos.end());
        for(i=0;i<all_pos.size();i++)
        {
            ll cur_pos=all_pos[i].pos,sequences,cur_val=all_pos[i].val,cur_res;
            if(pre==cur_val)
            {
                //cout<<"its duplicate val "<<cur_val<<" pos="<<cur_pos<<endl;
                cur_res=query(cur_pos,tree)+1;
                //cout<<"its subsequence total="<<cur_res<<" duplicate="<<pre_total<<endl;
                update(cur_pos,cur_res-pre_total,n,tree);
                pre_total+=cur_res-pre_total;
                pre_total%=1000000007;
            }
            else
            {
                //cout<<"its unique val "<<cur_val<<" pos="<<cur_pos<<endl;
                pre=cur_val;
                pre_total=0;
                cur_res=query(cur_pos,tree)+1;
                //cout<<"its subsequence total="<<cur_res<<" duplicate="<<pre_total<<endl;
                update(cur_pos,cur_res,n,tree);
                pre_total+=cur_res;
                pre_total%=1000000007;
            }

        }
        total=query(n,tree)%1000000007;
        printf("Case %lld: %lld\n",t,total);

    }
}
