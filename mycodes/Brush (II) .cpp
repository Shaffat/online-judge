#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,test,n,w,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&n,&w);
        int x,y,moves=0,last=-2e9;
        vector<int>y_cordinates;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&x,&y);
            y_cordinates.push_back(y);
        }
        sort(y_cordinates.begin(),y_cordinates.end());
        for(i=0;i<y_cordinates.size();i++)
        {
            if(y_cordinates[i]>last)
            {
                moves++;
                last=y_cordinates[i]+w;
            }
        }
        printf("Case %d: %d\n",t,moves);
        t++;
    }

}
