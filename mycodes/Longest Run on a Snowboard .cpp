#include<bits/stdc++.h>
using namespace std;
int r,c;
bool is_valid(int row,int col)
{
    if(row>0 && row<=r && col>0 && col<=c)
    {
        return true;
    }
    return false;
}

int solve(int row,int col,vector<vector<int> >&area,vector<vector<int> >&memory)
{
    if(memory[row][col]!=-1)
    {
        return memory[row][col];
    }
    int total=1,i,j,cur_r,cur_c,cur;
    cur_r=row-1;
    cur_c=col;
    if(is_valid(cur_r,cur_c))
    {
        if(area[row][col]>area[cur_r][cur_c])
        {
            cur=solve(cur_r,cur_c,area,memory)+1;
            total=max(total,cur);
        }
    }
    cur_r=row+1;
    cur_c=col;
    if(is_valid(cur_r,cur_c))
    {
        if(area[row][col]>area[cur_r][cur_c])
        {
            cur=solve(cur_r,cur_c,area,memory)+1;
            total=max(total,cur);
        }
    }
    cur_r=row;
    cur_c=col+1;
    if(is_valid(cur_r,cur_c))
    {
        if(area[row][col]>area[cur_r][cur_c])
        {
            cur=solve(cur_r,cur_c,area,memory)+1;
            total=max(total,cur);
        }
    }
    cur_r=row;
    cur_c=col-1;
    if(is_valid(cur_r,cur_c))
    {
        if(area[row][col]>area[cur_r][cur_c])
        {
            cur=solve(cur_r,cur_c,area,memory)+1;
            total=max(total,cur);
        }
    }
    return memory[row][col]=total;
}

int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        string s;
        int i,j,k,res=0;
        cin>>s>>r>>c;
        vector<int>col(c+1);
        vector<int>col1(c+1,-1);
        vector<vector<int> >area(r+1,col);
        vector<vector<int> >memory(r+1,col1);
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                scanf("%d",&area[i][j]);
            }
        }
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                res=max(res,solve(i,j,area,memory));
            }
        }
        cout<<s<<": "<<res<<endl;
    }
}
