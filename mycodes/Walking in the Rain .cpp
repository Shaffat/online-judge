#include<bits/stdc++.h>
using namespace std;

vector<int>tiles;
vector<int>memory(1001,-1);
int solve(int pos)
{
    if(pos>=tiles.size())
    {
        return -1;
    }
    if(pos==tiles.size()-1)
    {
        return tiles[pos];
    }
    if(memory[pos]!=-1)
    {
        return memory[pos];
    }
    int case1=0,case2=0;
    case1=min(tiles[pos],solve(pos+1));
    case2=min(tiles[pos],solve(pos+2));
    return memory[pos]=max(case1,case2);
}

int main()
{
    int n,i,j,res;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        tiles.push_back(j);
    }

    if(n==1)
    {
        res=tiles[0];
    }
    else
        res=solve(0);
    cout<<res<<endl;
}
