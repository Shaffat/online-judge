#include<bits/stdc++.h>

using namespace std;
vector<int>v(501,-1);
vector<vector<int> >memory(501,v);
vector<vector<int> >do_i_know(501,v);
int t,total;
int solve(int page,int start,int maxsize,vector<vector<int> >&linewithnote)
{
   cout<<" Page="<<page<<" start="<<start<<" maxsize="<<maxsize<<endl;
   if(start>=total)
   {
       return 0;
   }
   if(do_i_know[page][start]==t)
   {
       //cout<<"ACTIVATED"<<endl;
       return memory[page][start];
   }
   int i,j,res=2e9,counter=0;
   int limit=min(total-start,maxsize);
   vector<bool>chk(101,0);
   for(i=1;i<=limit;i++)
   {
       //cout<<"taken page from "<<start+1<<" "<<start+i<<endl;
       for(j=0;j<linewithnote[start+i].size();j++)
       {
           if(chk[linewithnote[start+i][j]]==0)
           {
               counter++;
               chk[linewithnote[start+i][j]]=1;
           }
       }
       //cout<<"foot note= "<<counter<<endl;
       if(counter+i>maxsize)
       {
           break;
       }
       else {
           int x=solve(page+1,start+i,maxsize,linewithnote)+counter;
           //cout<<"res for ["<<page+1<<"]["<<start+i<<"]="<<x<<endl;
           res=min(res,x);
       }
   }
   do_i_know[page][start]=t;
   return memory[page][start]=res;

}

int main()
{
    int test;
    t=1;

    scanf("%d",&test);


    while(t<=test)
    {
        int i,j,w,s,n;
        scanf("%d %d %d",&n,&s,&w);
        vector<vector<int> >v(n+1);
        for(i=1;i<=w;i++)
        {
            int k,m;
            scanf("%d",&k);
            for(j=1;j<=k;j++)
            {
                scanf("%d",&m);
                v[m].push_back(i);
            }
        }
        total=n;
        int res=solve(0,0,s,v);
        if(res==2e9)
        {
            res=-1;
        }
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
