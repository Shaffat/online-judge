#include<bits/stdc++.h>

using namespace std;

int LongestProperSuffix(string &pattern,vector<int>&lps)
{
    int i,j;
    lps[0]=-1;
    //cout<<"i=0 lps="<<lps[0]<<endl;
    for(i=1;i<pattern.size();i++)
    {
        lps[i]=-1;
        int cur=i-1;
        //cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
        if(pattern[lps[cur]+1]==pattern[i])
        {
            lps[i]=lps[cur]+1;
        }
        else
        {
            while(cur!=-1)
            {
//                cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
//                cout<<"trying to match "<<lps[cur]+1<<" and "<<i<<endl;
                    if(pattern[lps[cur]+1]==pattern[i])
                    {
                        lps[i]=lps[cur]+1;
                        break;
                    }
                    else
                    {
                        cur=lps[cur];
                    }
            }
        }
        //cout<<"i="<<i<<" lps="<<lps[i]<<endl;
    }
    return lps[pattern.size()-1];
}
int main()
{
    int i,j;
    string s,s1;
    while(cin>>s)
    {
        s1=s;
        reverse(s1.begin(),s1.end());
        s1+="#"+s;
        //cout<<"s1="<<s1<<endl;
        vector<int>lps(s1.size()+1);
        int matched=LongestProperSuffix(s1,lps)+1;
        //cout<<"matched="<<matched<<endl;
        for(int i=s.size()-matched-1;i>=0;i--)
        {
            //cout<<"i="<<i<<endl;
            s.push_back(s[i]);
        }
        cout<<s<<endl;
    }
}
