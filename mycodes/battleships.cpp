#include<bits/stdc++.h>
using namespace std;

vector<char>cl(101);
vector<vector<char> >sea(101,cl);
vector<bool>chk(101,0);
vector<vector<bool> >vis(101,chk);
struct shipdefine
{
    int r,c;
};
vector<shipdefine>ships;
int alive=0;
int n;
int solve()
{
    int i,j;
    for(i=0;i<ships.size();i++)
    {
        int cur_r,cur_c;
        cur_r=ships[i].r;
        cur_c=ships[i].c;
        if(!vis[cur_r][cur_c])
        {
            vis[cur_r][cur_c]=1;
            alive++;
            queue<shipdefine>q;
            shipdefine start;
            start.r=cur_r;
            start.c=cur_c;
            q.push(start);
            while(!q.empty())
            {
                int parent_row,parent_column;
                parent_row=q.front().r;
                parent_column=q.front().c;
                q.pop();
                if(parent_row-1>0)
                {
                    if(!vis[parent_row-1][parent_column]&&(sea[parent_row-1][parent_column]=='x'||sea[parent_row-1][parent_column]=='@'))
                    {
                        vis[parent_row-1][parent_column]=1;
                        shipdefine tmp;
                        tmp.r=parent_row-1;
                        tmp.c=parent_column;
                        q.push(tmp);
                    }
                }
                if(parent_row+1<=n)
                {
                    if(!vis[parent_row+1][parent_column]&&(sea[parent_row+1][parent_column]=='x'||sea[parent_row+1][parent_column]=='@'))
                    {
                        vis[parent_row+1][parent_column]=1;
                        shipdefine tmp;
                        tmp.r=parent_row+1;
                        tmp.c=parent_column;
                        q.push(tmp);
                    }
                }
                if(parent_column-1>0)
                {
                    if(!vis[parent_row][parent_column-1]&&(sea[parent_row][parent_column-1]=='x'||sea[parent_row][parent_column-1]=='@'))
                    {
                        vis[parent_row][parent_column-1]=1;
                        shipdefine tmp;
                        tmp.r=parent_row;
                        tmp.c=parent_column-1;
                        q.push(tmp);
                    }
                }
                if(parent_column+1<=n)
                {
                    if(!vis[parent_row][parent_column+1]&&(sea[parent_row][parent_column+1]=='x'||sea[parent_row][parent_column+1]=='@'))
                    {
                        vis[parent_row][parent_column+1]=1;
                        shipdefine tmp;
                        tmp.r=parent_row;
                        tmp.c=parent_column+1;
                        q.push(tmp);
                    }
                }
            }
        }
    }
}

int main()
{
   int test,t=1;
   scanf("%d",&test);
   while(t<=test)
   {
       getchar();
       int i,j;
       scanf("%d",&n);
       for(i=1;i<=n;i++)
       {
           getchar();
           for(j=1;j<=n;j++)
           {
              char c;
              scanf("%c",&c);
              sea[i][j]=c;
              if(c=='x')
              {
                  shipdefine tmp;
                  tmp.r=i;
                  tmp.c=j;
                  ships.push_back(tmp);
              }

           }
       }
      solve();
       int result=alive;
       alive=0;
       for(i=0;i<=100;i++)
       {
           for(j=0;j<=100;j++)
           {
               vis[i][j]=0;
           }
       }
       printf("Case %d: %d\n",t,result);
       ships.clear();
       t++;

   }
}
