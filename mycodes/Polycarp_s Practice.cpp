#include<bits/stdc++.h>
using namespace std;

struct index
{
    int idx,
    val;
};

bool operator <(index a,index b)
{
    if(a.val!=b.val)
    {
        return a.val >b.val;
    }
    return false;
}

int main()
{
    int n,i,j,k;
    scanf("%d %d",&n,&k);
    vector<index>pblms;
    vector<bool>done(n+1,0);
    vector<int>pos;
    for(i=1;i<=n;i++)
    {
        index tmp;
        scanf("%d",&j);
        tmp.idx=i;
        tmp.val=j;
        pblms.push_back(tmp);
    }
    sort(pblms.begin(),pblms.end());
    int sum=0;
    for(i=0;i<k;i++)
    {
        sum+=pblms[i].val;
        pos.push_back(pblms[i].idx);
    }
    sort(pos.begin(),pos.end());
    for(i=0;i<k-1;i++)
    {
        //cout<<"pos="<<pos[i]<<endl;
        done[pos[i]]=1;
    }
    cout<<sum<<endl;
    int cnt=0;
    for(i=1;i<=n;i++)
    {
      cnt++;
      if(done[i]==1)
      {
          cout<<cnt<<" ";
          cnt=0;
      }

    }
    cout<<cnt<<endl;

}
