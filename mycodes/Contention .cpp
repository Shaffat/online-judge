#include<bits/stdc++.h>
using namespace std;

struct order
{
    int l,r;
};

bool operator < (order a, order b)
{
    if(a.r!=b.r){
        return a.r>b.r;
    }
    return a.l>b.l;
}

bool ok(int n,int k,vector<order>&ticket){
    cout<<"k="<<k<<" n="<<n<<endl;
    int i,j,available=n;
    for(i=0;i<ticket.size();i++){
        int tmpL,tmpr;
        tmpL=ticket[i].l;
        tmpr=min(available,ticket[i].r);
        if((tmpr-tmpL)+1>=k){
            available=tmpr-k;
        }
        else
            return false;
    }
    return true;
}

int solve(int n,vector<order>&tickets){
    sort(tickets.begin(),tickets.end());
    int i,j,first,last,mid,res=0,f=0;
    first=0,last=n;
    while(first<=last){
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if(ok(n,mid,tickets)){
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main(){
    int test,t,i,j,l,r,n,q,ans;
    scanf("%d",&test);
    for(t=1;t<=test;t++){
        vector<order>tickets;
        scanf("%d %d",&n,&q);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d",&l,&r);
            order tmp;
            tmp.l=l;
            tmp.r=r;
            tickets.push_back(tmp);
        }
        ans=solve(n,tickets);
        printf("Case #%d: %d\n",t,ans);
    }

}
