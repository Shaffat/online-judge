#include<stdio.h>
#include<iostream>
#include<string>
#include<math.h>
#include<algorithm>

using namespace std;
typedef long long int ll;

ll hashvalue(string &s,ll m,ll b,ll mod)
{
    ll i,j,power=1,value=0;
    for(i=m-1;i>=0;i--)
    {
        value+=(s[i]*power)%mod;
        value%=mod;
        power*=b;
        power%=mod;
    }
    return value;
}
ll solve(string &s,string &p)
{
    if(p.size()>s.size())
    {
        return 0;
    }
    ll counter=0;
    ll i,j,m=p.size(),b=347,power=1,mod=1e9 +7;
    for(i=1;i<m;i++)
    {
        power*=b;
        power%=mod;
    }
    ll hash_txt=hashvalue(s,m,b,mod);
    ll hash_pattern=hashvalue(p,m,b,mod);
    ll b2=349,power2=1,mod2=1e10 +7;
    for(i=1;i<m;i++)
    {
        power2*=b2;
        power2%=mod2;
    }
    ll hash_txt2=hashvalue(s,m,b2,mod2);
    ll hash_pattern2=hashvalue(s,m,b2,mod2);
    if(hash_txt2==hash_pattern2 && hash_txt==hash_pattern)
    {
        counter++;
    }
    for(i=m;i<s.size();i++)
    {
        hash_txt=(hash_txt-((s[i-m]*power)%mod))%mod;
        hash_txt=(hash_txt+mod)%mod;
        hash_txt=(hash_txt*b)%mod;
        hash_txt=(hash_txt+s[i])%mod;
        hash_txt2=(hash_txt2-((s[i-m]*power2)%mod2))%mod2;
        hash_txt2=(hash_txt2+mod2)%mod2;
        hash_txt2=(hash_txt2*b2)%mod2;
        hash_txt2=(hash_txt2+s[i])%mod2;
        if(hash_txt2==hash_pattern2 && hash_txt==hash_pattern)
        {
            counter++;
        }
    }
    return counter;
}
int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        string a,b;
        cin>>b>>a;

        printf("%lld\n",solve(b,a));
    }
}
