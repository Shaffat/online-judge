#include<bits/stdc++.h>
using namespace std;

void LongestProperSuffix(string &pattern,vector<int>&lps)
{
    int i,j;
    lps[0]=-1;
    //cout<<"i=0 lps="<<lps[0]<<endl;
    for(i=1;i<pattern.size();i++)
    {
        lps[i]=-1;
        int cur=i-1;
        //cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
        if(pattern[lps[cur]+1]==pattern[i])
        {
            lps[i]=lps[cur]+1;
        }
        else
        {
            while(cur!=-1)
            {
//                cout<<"cur="<<cur<<" lps="<<lps[cur]<<endl;
//                cout<<"trying to match "<<lps[cur]+1<<" and "<<i<<endl;
                    if(pattern[lps[cur]+1]==pattern[i])
                    {
                        lps[i]=lps[cur]+1;
                        break;
                    }
                    else
                    {
                        cur=lps[cur];
                    }
            }
        }
        //cout<<"i="<<i<<" lps="<<lps[i]<<endl;
    }
    return;
}

void divisor(int n,vector<int>&l,vector<int>&r)
{
    int i,j;
    for(i=1;i<=sqrt(n);i++)
    {
        if(n%i==0)
        {
            l.push_back(i);
            if(n/i!=i)
            {
                r.push_back(n/i);
            }
        }
    }
    reverse(r.begin(),r.end());
//    for(i=0;i<l.size();i++)
//    {
//        cout<<"l "<<l[i]<<endl;
//    }
//    for(i=0;i<r.size();i++)
//    {
//        cout<<"r "<<r[i]<<endl;
//    }
    return;
}


bool is_ok(int len,int sz,vector<int>&lps)
{
    //cout<<"len="<<len<<endl;
    int cur,i,j;
    cur=2*len -1;
    //cout<<"cur="<<cur<<endl;
    while(cur<sz)
    {
        //cout<<"cur="<<cur<<" lps="<<lps[cur]+1<<endl;
        if(lps[cur]+1<len)
            return false;
        cur+=len;
    }
    return true;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ios_base::sync_with_stdio(0);
    string s;
    while(cin>>s)
    {
        if(s==".") break;
        int i,j,res=-1,done=0;
        vector<int>lps(s.size());
        LongestProperSuffix(s,lps);
        vector<int>l_divisor;
        vector<int>r_divisor;
        divisor(s.size(),l_divisor,r_divisor);

        for(i=0;i<l_divisor.size();i++)
        {
            //cout<<"l_d="<<l_divisor[i]<<endl;
            if(is_ok(l_divisor[i],s.size(),lps))
            {
                done=1;
                res=s.size()/l_divisor[i];
                break;
            }
        }
        if(!done)
        {
            for(i=0;i<r_divisor.size();i++)
            {
                //cout<<"r_d="<<r_divisor[i]<<endl;
                if(is_ok(r_divisor[i],s.size(),lps))
                {
                    done=1;
                    res=s.size()/r_divisor[i];
                    break;
                }
            }
        }
        if(!done)
            res=1;
        cout<<res<<endl;
    }
}
