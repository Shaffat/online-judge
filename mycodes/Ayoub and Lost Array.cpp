#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define mod 1000000007;

vector<ll>col(3,-1);
vector<vector<ll> >memory(200001,col);


ll counter(ll l,ll r,ll m)
{
    ll round_l,round_r,res;
    round_l=(l/3)*3;
    if(round_l<l) round_l+=3;
    round_r=(r/3)*3;
    res=round_r/3-round_l/3;
    //cout<<"round_l ="<<round_l<<" r"<<round_r<<endl;
    if(m==1)
    {
        if(round_l-2>=l) res++;
        if(round_r+1<=r) res++;
    }
    if(m==2)
    {
        if(round_l-1>=l) res++;
        if(round_r+2<=r) res++;
    }
     if(m==0)
    {
        res=r/3-(max(l-1,0ll)/3);
    }
   // cout<<"from "<<l<<" to "<<r<<" m="<<m<<"  ans is "<<res<<endl;
    return res;
}

ll solve(ll index,ll m,ll n,ll l,ll r)
{
    if(index>n)
    {
        if(m==0) return 1;
        return 0;
    }
    if(memory[index][m]!=-1)
        return memory[index][m];
    ll i,j,res=0;
    for(i=0;i<3;i++)
    {
        res+=solve(index+1,(m+i)%3,n,l,r)*counter(l,r,i);
        res%=mod;
    }
    return memory[index][m]=res;

}


int main()
{
   ll n,l,r,res;
   cin>>n>>l>>r;
   res=solve(1,0,n,l,r);
   cout<<res<<endl;
}

