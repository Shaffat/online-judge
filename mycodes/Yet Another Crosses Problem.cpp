#include<bits/stdc++.h>
using namespace std;

void colneed(int r,int c,vector<int>&needC,vector<string>&matrix)
{
    int i,j;
    for(i=0;i<c;i++)
    {
        int counter=0;
        for(j=0;j<r;j++)
        {
            if(matrix[j][i]=='.')
            {
                counter++;
            }
        }
        needC[i]=counter;
    }
    return;
}

void rowneed(int r,int c,vector<int>&needC,vector<string>&matrix)
{
    int i,j;
    for(i=0;i<r;i++)
    {
        int counter=0;
        for(j=0;j<c;j++)
        {
            if(matrix[i][j]=='.')
            {
                counter++;
            }
        }
        needC[i]=counter;
    }
    return;
}


int solve(vector<int>&row,vector<int>&col,int r,int c,vector<string>&matrix)
{
    int i,j,res=2e9,ans;
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            if(matrix[i][j]=='.')
            {
                res=min(res,row[i]+col[j]-1);
            }
            else
                res=min(res,row[i]+col[j]);
        }
    }
    return res;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int n,m,i,j,test,r,c,t;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>r>>c;
        vector<string>matrix;
        for(i=1;i<=r;i++)
        {
            cin>>s;
            matrix.push_back(s);
        }
        vector<int>col(c+1);
        vector<int>row(r+1);
        rowneed(r,c,row,matrix);
        colneed(r,c,col,matrix);
        int res=solve(row,col,r,c,matrix);
        cout<<res<<endl;
    }
}
