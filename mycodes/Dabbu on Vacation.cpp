#include<bits/stdc++.h>

using namespace std;

int main()
{
    double D,d,h,area1,r2,area2,a,b;
    int test,t;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>D>>d>>h;
        a=D/2;
        b=d-D/2;
        area1=3*((a*a)-(b*b))*h;
        area2=4*(a*a*a);
        //cout<<area1<<" "<<area2<<endl;
        if(abs(area1-area2)<=0.001)
        {
            cout<<"Case "<<t<<": Yes"<<endl;
        }
        else
            cout<<"Case "<<t<<": No"<<endl;
    }
}
