#include<bits/stdc++.h>

using namespace std;

int main()
{
    int i,j,n,odd=0,even=0,max_odd=-2e9,max_even=-2e9,pos_odd=0;
    vector<int>even_numbers;
    vector<int>odd_numbers;
    vector<int>neg_odd_numbers;

    int temp1=0,temp2=0;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        int k;
        scanf("%d",&k);
       if(k%2!=0)
       {

           if(k>0)
           {
               odd_numbers.push_back(k);
               pos_odd+=k;

           }
           else
           {
               neg_odd_numbers.push_back(k);
           }

           max_odd=max(max_odd,k);
       }
       else
       {

           if(k>0)
           {

           even_numbers.push_back(k);
           }

           max_even=max(max_even,k);
       }
    }

    sort(odd_numbers.begin(),odd_numbers.end());
    sort(neg_odd_numbers.begin(),neg_odd_numbers.end());
   int l=odd_numbers.size();
   int chk=0;
   if(l%2==0)
   {
       chk=1;
   }

   for(i=0;i<even_numbers.size();i++)
       {
           even+=even_numbers[i];
       }


   if(odd_numbers.size()==0)
   {
       odd=max_odd;
   }
   else if(pos_odd%2!=0)
   {
       odd=pos_odd;
   }
   else
   {
       temp1=pos_odd;
       for(i=neg_odd_numbers.size()-1;i>=0;i--)
       {
           if((temp1-neg_odd_numbers[i])%2!=0)
           {
               temp1+=neg_odd_numbers[i];
               break;
           }

       }
       temp2=pos_odd-odd_numbers[0];
       if(temp1==pos_odd)
       {
           odd=temp2;
       }
       else
       odd=max(temp1,temp2);
   }

    cout<<odd+even<<endl;
}
