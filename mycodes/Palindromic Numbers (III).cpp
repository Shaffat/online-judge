#include<bits/stdc++.h>
using namespace std;

bool is_max(string &s)
{
    int chk=0,i;
    for(i=0;i<s.size();i++)
    {
        if(s[i]!='9')
        {
            return false;
        }
    }
    return true;

}

bool is_palindrome(string &s)
{
    int i=0,j=s.size()-1;
    while(i<=j)
    {
        if(s[i]!=s[j])
        {
            return 0;
        }
        i++;
        j--;
    }
    return 1;

}


string min_palindrome(int n)
{
    int i,j;
    string s="";
    s.push_back('1');
    for(i=1;i<n-1;i++)
    {
        s.push_back('0');
    }
    s.push_back('1');
    return s;
}

string build_palindrome(string &s)
{
    int i=0,j=s.size()-1;
    string res=s;
    //cout<<"res="<<res<<endl;
    while(i<=j)
    {
        res[j]=res[i];
        i++;
        j--;
    }
    //cout<<"built res="<<res<<endl;
    return res;
}
int find_pos(string &s)
{
    //cout<<"find pos"<<endl;
    int i,j,k;
    if(s.size()%2==0)
    {
        k=s.size()/2-1;
    }
    else
    {
        k=s.size()/2;
    }
    for(i=0;i<=k;i++)
    {
        if(s[i]!='9')
        {
            //cout<<"i="<<i<<" "<<s[i]<<endl;
            j=i;
        }
    }
    return j;
}

void build_res(string &s,int pos)
{
    //cout<<"build res"<<endl;
    s[pos]=s[pos]+1;
    //cout<<pos<<" pos="<<s[pos]<<endl;
    s[s.size()-pos-1]=s[pos];
    //cout<<s.size()-pos-1<<" miror="<<s[s.size()-pos-1]<<endl;
    int i=pos+1,j=s.size()-i-1;
    //cout<<"i="<<i<<" j="<<j<<endl;
    while(i<=j)
    {
        s[i]='0';
        s[j]='0';
        i++;
        j--;
    }
    //cout<<"s="<<s<<endl;
    return;
}


string ans_me_non(string &s)
{
    int i,j,chk=0,n=s.size(),k;
    string res;
    if(s.size()%2==0)
    {
        k=s.size()/2-1;
    }
    else
    {
        k=s.size()/2;
    }
    int big=-1,small=-1;
    for(i=0;i<=k;i++)
    {
        //cout<<"index "<<i<<"="<<s[i]<<" index ="<<n-i-1<<"="<<s[n-i-1]<<endl;
        if(s[i]<s[n-i-1])
        {
            small=i;
        }
        else if(s[i]>s[n-i-1])
            big=i;
    }
    res=build_palindrome(s);
    //cout<<"res="<<res<<endl;
    //cout<<"big="<<big<<" small="<<small<<endl;
    if(small>big)
    {
        int pos=find_pos(s);
       // cout<<"pos="<<pos<<endl;
        build_res(res,pos);
    }
    return res;

}

string ans_me_is(string &s)
{
     int pos=find_pos(s);
      build_res(s,pos);
      return s;
}

string nxt_palindrome(string &s)
{
    int i,j,pos;
    if(is_palindrome(s))
    {
        //cout<<"is"<<endl;
        if(is_max(s))
        {
            //cout<<"mx"<<endl;
            return min_palindrome(s.size()+1);
        }
        else
        {
            //cout<<"not max"<<endl;
            return ans_me_is(s);
        }

    }
    else
    {
        //cout<<"non called"<<endl;
        return ans_me_non(s);
    }


}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>s;
        cout<<"Case "<<t<<": "<<nxt_palindrome(s)<<endl;
    }
}
