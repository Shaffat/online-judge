#include<bits/stdc++.h>
using namespace std;

int find_parent(int node,vector<int>&parent)
{
    if(parent[node]==node)
    {
        return node;
    }
    return parent[node]=find_parent(parent[node],parent);
}

void build_graph(string s1,string s2,vector<vector<int> >&connection)
{
    vector<int>nodes(140,0);
    int counter=0;
    int i,j,l,m;
    for(i=0;i<s1.size();i++)
    {
        l=s1[i];
        m=s2[i];
        if(l==m)
        {
            continue;
        }
        connection[l][m]=1;
        connection[m][l]=1;
    }
}

string solve(vector<vector<int> >&connection)
{
    string s;
    int i,j,counter=0;
    vector<int>parent(140);
    vector<int>ranking(140,0);
    for(i=1;i<=139;i++)
    {
        parent[i]=i;
    }
    for(i='a';i<='z';i++)
    {
        for(j='a';j<='z';j++)
        {
            if(connection[i][j]==1)
            {
                int u,v;
                u=find_parent(i,parent);
                v=find_parent(j,parent);
                char x,y,xp,yp;
                x=i;y=j;xp=u,yp=v;

                if(u!=v)
                {
                    s.push_back(i);
                    s.push_back(j);
                    counter++;
                    if(ranking[u]>=ranking[v])
                    {
                        ranking[u]++;
                        parent[v]=u;
                    }
                    else
                    {
                        ranking[v]++;
                        parent[u]=v;
                    }
                    xp=parent[i];
                    yp=parent[j];
                    //cout<<"after\n"<<"u="<<xp<<" v="<<yp<<endl;
                }
            }
        }
    }
    return s;
}

int main()
{
    int n,i,j;
    string s1,s2,res;
    cin>>n;
    cin>>s1>>s2;
    vector<int>col1(138,0);
    vector<vector<int> >connection(138,col1);
    build_graph(s1,s2,connection);
    res=solve(connection);
    cout<<res.size()/2<<endl;
    for(i=0;i<res.size();i+=2)
    {
        printf("%c %c\n",res[i],res[i+1]);
    }
}
