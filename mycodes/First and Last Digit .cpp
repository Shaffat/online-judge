#include<bits/stdc++.h>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(0);
    int t,sum;
    cin>>t;
    while(t--)
    {
        string s;
        cin>>s;
        reverse(s.begin(),s.end());
        while(s[0]=='0')
        {
            s.erase(s.begin()+0);
        }
        cout<<s<<endl;
    }
}
