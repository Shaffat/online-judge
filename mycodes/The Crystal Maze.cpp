#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

int m,n;

int vis[501][501];
int graph[501][501];
int dis[501][501];
pos parent[501][501];

bool valid(pos cur)
{
    if(cur.x<0 || cur.x>=m ||cur.y<0 || cur.y>=n)
        return false;
    return true;
}

void bfs(pos root)
{
    //cout<<"bfs"<<endl;

    pos cur,nxt;
    int i,j,crystal=0;
    queue<pos>q;
    q.push(root);
    //printf("Root %d %d\n", root.x, root.y);
    //memset(vis, false, sizeof(vis));

    vis[root.x][root.y]=1;

    if(graph[root.x][root.y] == 1)
        crystal++;

    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        //cout<<"cur="<<cur.x<<" "<<cur.y<<endl;
        nxt.x=cur.x+1;
        nxt.y=cur.y;
        if(valid(nxt))
        {
            if(vis[nxt.x][nxt.y]==0 &&(graph[nxt.x][nxt.y]!=-1))
            {
                vis[nxt.x][nxt.y]=1;
                if(graph[nxt.x][nxt.y]==1) crystal++;
                q.push(nxt);
                parent[nxt.x][nxt.y].x=root.x;
                parent[nxt.x][nxt.y].y=root.y;
            }
        }
        nxt.x=cur.x-1;
        nxt.y=cur.y;
        if(valid(nxt))
        {
            if(vis[nxt.x][nxt.y]==0 &&(graph[nxt.x][nxt.y]!=-1))
            {
                vis[nxt.x][nxt.y]=1;
                if(graph[nxt.x][nxt.y]==1) crystal++;
                q.push(nxt);
                parent[nxt.x][nxt.y].x=root.x;
                parent[nxt.x][nxt.y].y=root.y;
            }
        }
        nxt.x=cur.x;
        nxt.y=cur.y+1;
        if(valid(nxt))
        {
            if(vis[nxt.x][nxt.y]==0 &&(graph[nxt.x][nxt.y]!=-1))
            {
                vis[nxt.x][nxt.y]=1;
                if(graph[nxt.x][nxt.y]==1) crystal++;
                q.push(nxt);
                parent[nxt.x][nxt.y].x=root.x;
                parent[nxt.x][nxt.y].y=root.y;
            }
        }
        nxt.x=cur.x;
        nxt.y=cur.y-1;
        if(valid(nxt))
        {
            if(vis[nxt.x][nxt.y]==0 &&(graph[nxt.x][nxt.y]!=-1))
            {
                vis[nxt.x][nxt.y]=1;
                if(graph[nxt.x][nxt.y]==1) crystal++;
                q.push(nxt);
                parent[nxt.x][nxt.y].x=root.x;
                parent[nxt.x][nxt.y].y=root.y;

            }
        }
    }
/*
    cout<<"after"<<endl;
    for(i=0;i<m;i++)
    {
        for(j=0;j<n;j++)
        {
            cout<<vis[i][j]<<" ";
        }
        cout<<endl;
    }
*/
//    cout << "Crystal is " << crystal << endl;
//    cout << "Bfs Parent " << root.x << " " << root.y << endl;
    dis[root.x][root.y]=crystal;
}

int main()
{

//    freopen("in.txt", "r", stdin);
//    freopen("out.txt", "w", stdout);

    //ios_base::sync_with_stdio(0);
    int test,t,i,j,q,res, xx, yy;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        memset(vis,0,sizeof(vis));
        cin>>m>>n>>q;
        //cin.ignore();
//
//        vector<int>col(n+1);
//        vector<vector<int> >graph(m+1,col);
//        vector<int>col1(n+1,0);
//        vector<vector<int> >vis(m+1,col1);
//        vector<vector<int> >dis(m+1,col1);
//        vector<pos>col2(n+1);
//        vector<vector<pos> >parent(m+1,col2);

        for(i=0;i<m;i++)
        {
            string s;

            cin>>s;
            //cout << s << endl;
            //cout<<"i="<<i<<" s="<<s<<endl;
            for(j=0;j<s.size();j++)
            {
                if(s[j]=='#')
                    graph[i][j]=-1;
                else if(s[j]=='.')
                    graph[i][j]=0;
                else
                    graph[i][j]=1; ///
            }
        }

        for(i=0;i<m;i++)
        {
            for(j=0;j<n;j++)
            {
                //cout<<"i="<<i<<" j="<<j<<" vis="<<vis[i][j]<<" graph="<<graph[i][j]<<endl;
                if((graph[i][j] != -1) && vis[i][j]==0)
                {
                    //cout<<"root selected"<<endl;

                    //printf("available %d %d\n", i, j);
                    pos tmp;
                    tmp.x=i;
                    tmp.y=j;
                    parent[i][j]=tmp;
                    bfs(tmp);
                }
            }
        }
        cout<<"Case "<<t<<":"<<endl;
        for(i=1;i<=q;i++)
        {
            cin>>xx>>yy;
            xx--;
            yy--;
            if(vis[xx][yy] == 1)
            {
//                cout << "Called" << endl;
//
//                printf("Parrent %d %d\n", parent[xx][yy].x, parent[xx][yy].y);
                res=dis[parent[xx][yy].x][parent[xx][yy].y];
            }
            else
                res=0;
            cout<<res<<endl;
        }

    }
}
