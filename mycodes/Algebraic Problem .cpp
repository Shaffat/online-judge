#include<bits/stdc++.h>
using namespace std;
#define llu unsigned long long int
vector<int>testcase1(1e6+1,0);
vector<llu>memory(1e6+1);
vector<int>testcase2(1e6+1,0);
vector<llu>ab(1e6+1);
llu llmax=18446744073709551615;
int t;

llu solveab(llu n,llu q)
{
    if(n==0)
    {
        return 1;
    }
    if(n==1)
    {
        return q;
    }
    if(testcase2[n]==t)
    {
        return ab[n];
    }
    testcase2[n]=t;
    return ab[n]=solveab(n/2,q)*solveab(n-n/2,q);
}
llu solve(llu n,llu p,llu q)
{
    if(n==0)
    {
        return 2;
    }
    if(n==1)
    {
        return p;
    }
    if(testcase1[n]==t)
    {
        return memory[n];
    }
    testcase1[n]=t;
    if(n%2==0)
    {
        memory[n]=solve(n/2,p,q)*solve(n/2,p,q)-2*solveab(n/2,q);
        cout<<"n="<<n<<" memory="<<memory[n]<<" ab="<<solveab(n/2,q)<<" anther="<<solve(n/2,p,q)<<endl;
        return memory[n];
    }
    else
    {
        long long int dif=p-q;
        llu multi;
        cout<<"dif="<<dif<<endl;
        if(dif<0)
        {
            dif*=-1;
            multi=llmax-(dif-1);
        }
        else
            multi=dif;
        cout<<"n="<<n<<" multi="<<multi<<endl;
        memory[n]=solve(n-1,p,q)*multi;
        cout<<"n="<<n<<" memory="<<memory[n]<<endl;
        return memory[n];
    }
}

int main()
{
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        llu p,q,n;
        scanf("%llu %llu %llu",&p,&q,&n);
        llu res=solve(n,p,q);
        printf("Case %d: %llu\n",t,res);
    }
}
