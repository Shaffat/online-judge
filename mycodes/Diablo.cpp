#include<bits/stdc++.h>
using namespace std;



int query(int index,int s,int e,int qs,int qe,vector<int>&tree)
{
    if(qs>qe)
    {
        return 0;
    }
    if(s>=qs && e<=qe)
    {
        return tree[index];
    }
    if(qs>e || qe<s)
    {
        return 0;
    }
    int left=2*index,right=2*index + 1,mid=(s+e)/2;
    return query(left,s,mid,qs,qe,tree)+query(right,mid+1,e,qs,qe,tree)+tree[index];
}

void update(int index,int s,int e,int qs,int qe,vector<int>&tree)
{
    if(qs>qe) return;
    if(s>=qs && e<=qe)
    {
        //cout<<"updated in index "<<index<<" s="<<s<<" e="<<e<<endl;
        tree[index]+=1;
        return;
    }
    if(qs>e || qe<s)
    {
        return ;
    }
    int left=2*index,right=2*index + 1,mid=(s+e)/2;
    update(left,s,mid,qs,qe,tree);
    update(right,mid+1,e,qs,qe,tree);
    return;
}


void update_active(int index,int s,int e,int val,int qs,int qe,vector<int>&active_tree)
{
    if(s==qs && e==qe)
    {
        if(val)
        {
            active_tree[index]=s;
        }
        else
        active_tree[index]=2e6;
        return;
    }
    if(s>qe || e<qs)
    {
        return ;
    }
    int left=2*index,right=2*index + 1,mid=(s+e)/2;
    update_active(left,s,mid,val,qs,qe,active_tree);
    update_active(right,mid+1,e,val,qs,qe,active_tree);
    active_tree[index]=min(active_tree[left],active_tree[right]);
    return;
}

int query_active(int index,int s,int e,int qs,int qe,vector<int>&active_tree)
{
    if(qs>qe)
    {
        return 2e6;
    }
    if(s>=qs && e<=qe)
    {
        return active_tree[index];
    }
    if(qs>e || qe<s)
    {
        return 2e6;
    }
    int left=2*index,right=2*index + 1,mid=(s+e)/2;
    return min(query_active(left,s,mid,qs,qe,active_tree),query_active(right,mid+1,e,qs,qe,active_tree));
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    int test,t,i,j,n,q,cur;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int cur=1;
        char c;
        scanf("%d %d",&n,&q);
        vector<int>values(n+q+1,-1);
        vector<int>tree(4*(n+q+1),0);
        vector<int>active_tree(4*(n+q),2e6);

        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            values[i]=j;
            update_active(1,1,n+q,1,i,i,active_tree);
            //cout<<"nxt_active for "<<i<<" "<<query_active(1,1,n+q,i,n+q,active_tree)<<endl;;
            cur++;
        }
        printf("Case %d:\n",t);
        cur=n+1;
        for(i=1;i<=q;i++)
        {
            scanf(" %c %d",&c,&j);

            if(c=='a')
            {
                //cout<<"query add"<<endl;
                values[cur]=j;
                update_active(1,1,n+q,1,cur,cur,active_tree);
                //cout<<"nxt_active for "<<cur<<" "<<query_active(1,1,n+q,cur,n+q,active_tree)<<endl;
                cur++;

            }
            else
            {
                //cout<<"query call"<<endl;

                if(j>2*n)
                {
                    printf("none\n");
                }
                else{
                int propagate=query(1,1,n+q,j,j,tree),nxt_valid,add;
                //cout<<"propagate="<<propagate<<" pos="<<j+propagate<<endl;
                nxt_valid=query_active(1,1,n+q,j+propagate,n+q,active_tree);
                //cout<<"nxt_valid="<<nxt_valid<<endl;
                if(nxt_valid>n+q)
                {
                    printf("none\n");
                }
                else if(values[nxt_valid]==-1)
                {
                    printf("none\n");
                }
                else{
                printf("%d\n",values[nxt_valid]);
                update(1,1,n+q,j,n+q,tree);
                update_active(1,1,n+q,0,nxt_valid,nxt_valid,active_tree);
                //cout<<"nxt_active for "<<j<<" "<<query_active(1,1,n+q,j,n+q,active_tree)<<endl;
                //cout<<"C query end"<<endl;
                }
                }
            }
        }
    }
}
