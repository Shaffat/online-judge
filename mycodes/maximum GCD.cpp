#include<bits/stdc++.h>

using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

int main()

{
    int test,first,second;
    scanf("%d",&test);
    string line = "" ;
    getline( cin , line ) ;

    while(test--)
    {
        vector<int>v;
        int c,j,i;
        getline( cin , line ) ;

        istringstream iss( line ) ;

        while( iss >> c ) {
        	v.push_back( c ) ;
        }
        /*
        while(scanf("%d",&c)!=EOF)
        {
            v.push_back(c);
        }
        */
        int maxim=-1;
        for(i=0;i<v.size();i++)
        {
            for(j=i+1;j<v.size();j++)
            {
                int k=GCD(v.at(i),v.at(j));
                if(maxim<k)
                {
                    maxim=k;
                }
            }
        }
        printf("%d\n",maxim);
    }

}
