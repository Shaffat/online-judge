#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,m,n,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&m,&n);
        //cout<<m<<" "<<n<<endl;
        if(m==1 || n==1)
        {
            printf("Case %d: %d\n",t,max(m,n));
        }
        else if(m==2||n==2)
        {
            int res=0;
            res=(m*n)/8;
            res*=4;
            res+=min(4,(m*n)%8);
            printf("Case %d: %d\n",t,res);
        }
        else
        {
            double l,k;
            l=m;
            k=n;
            int res=ceil(l*k/2);
            printf("Case %d: %d\n",t,res);
        }
        t++;
    }
}
