#include<bits/stdc++.h>
using namespace std;
#define ll long long int
ll cost(string &s)
{
    ll res=0,i,j;
    if(s.size()>=3)
    {
        if(s[0]==s[1] && s[1]==s[2])
        {
            s[1]='.';
            res++;
        }
    }
    if(s.size()>=2)
    {
        if(s[0]==s[1])
        {
            s[0]='.';
            res++;
        }
    }
    //cout<<"s="<<s<<endl;
    for(i=1;i<s.size();i++)
    {
        if(s[i]==s[i-1])
        {
            //cout<<i<<" and "<<i-1<<" matched"<<endl;
            s[i]='.';
            res++;
        }
    }
    return res;
}

int main()
{
    string s;
    ll i,j,k,res;
    cin>>s>>k;
    if(s.size()==1)
    {
        cout<<k/2<<endl;
    }
    else
    {
        res=k*cost(s);
//        cout<<"s="<<s<<endl;
//        cout<<"res="<<res<<" k="<<k<<" cost="<<res<<endl;
        if(s[0]==s[s.size()-1])
        {
            res+=k-1;
        }
        cout<<res<<endl;
    }
}
