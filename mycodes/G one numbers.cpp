#include<bits/stdc++.h>

using namespace std;

vector<bool>primes(100,0);
void sieve()
{
    primes[0]=1;
    primes[1]=1;
    int i,j;
    for(i=4;i<=100;i+=2)
    {
        primes[i]=1;
    }
    for(i=3;i<=sqrt(100);i+=2)
    {
        if(primes[i]==0)
        {
            for(j=i*i;j<=100;j+=i)
            {
                primes[j]=1;
            }
        }
    }
}

int solve(string s,int index,int sum,bool restriction,vector<vector<int> >&memory)
{
    if(index<0)
    {
        if(primes[sum]==0)
        {
            return 1;
        }
        return 0;
    }
    if(memory[index][sum]!=-1 && restriction==0)
    {
        return memory[index][sum];
    }
    int i,j,result=0;
    if(restriction)
    {
        int range=s[index]-'0';
        for(i=0;i<=range;i++)
        {
            if(i<range)
            {
                result+=solve(s,index-1,sum+i,0,memory);
            }
            else
            {
                result+=solve(s,index-1,sum+i,1,memory);
            }
        }

    }
    else
    {
        for(i=0;i<=9;i++)
        {
            result+=solve(s,index-1,sum+i,0,memory);
        }
    }
    if(restriction)
    {
        return result;
    }
    else
    {
        return memory[index][sum]=result;
    }
}

string ConvertAndAdjust(int k)
{
    if(k==0)
    {
        return "0";
    }
    string nw="";
    while(k!=0)
    {
        int i=k%10;
        k/=10;
        nw.push_back('0'+i);
    }
    return nw;
}

int main()
{
    sieve();
    int test;
    vector<int>sum(99,-1);
    vector<vector<int> >memory(11,sum);
    scanf("%d",&test);
    while(test--)
    {
        int a,b;
        scanf("%d %d",&a,&b);
        string s1,s2;
        a--;
        s1=ConvertAndAdjust(a);
        s2=ConvertAndAdjust(b);
        printf("%d\n",solve(s2,s2.size()-1,0,1,memory)-solve(s1,s1.size()-1,0,1,memory));
    }
}
