#include<stdio.h>
#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

string sum(string a,string b)
{
    int i,j;
    string res="";
    if(a.size()!=b.size())
    {
        if(a.size()<b.size())
        {
            while(a.size()<b.size())
            {
                a.insert(a.begin(),'0');
            }
        }
        if(b.size()<a.size())
        {
            while(b.size()<a.size())
            {
                b.insert(b.begin(),'0');
            }
        }
    }
    //cout<<a<<" "<<b<<endl;
    int carry=0;
    for(i=a.size()-1;i>=0;i--)
    {
        int l,m,total;
        l=a[i]-'0';
        m=b[i]-'0';
        total=l+m+carry;
        if(total<10)
        {
            carry=0;
            res.push_back('0'+total);
        }
        else
        {
            carry=1;
            res.push_back('0'+total-10);
        }
    }
    if(carry)
    {
        res.push_back('1');
    }
    //reverse(res.begin(),res.end());
    int chk=0;
    i=0;
    while(i<res.size()-1)
    {
        if(res[i]=='0')
        {
            res.erase(res.begin());
        }
        else break;
    }
    return res;
}

int main()
{

    int test;
    scanf("%d",&test);
    while(test--)
    {
        string a,b;
        cin>>a>>b;
        reverse(a.begin(),a.end());
        reverse(b.begin(),b.end());
        cout<<sum(a,b)<<endl;
    }
}
