#include<bits/stdc++.h>
using namespace std;


void solve(string &s,vector<int>&vis,vector<int>&dis)
{
    int i,j;
    char root=s[0];
    stack<char>stk;
    stk.push(root);
    vis[root]=1;
    for(i=1;i<s.size();i++)
    {
        vis[s[i]]=1;
        if(s[i]==stk.top())
        {
            if(s[i]!=root)
            {
                dis[s[i]]++;
            }
            stk.pop();
            if(!stk.empty())
            {
                dis[stk.top()]++;
            }
        }
        else
            stk.push(s[i]);
    }
    return;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,i,j;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s;
        cin>>s;
        vector<int>vis(200,0);
        vector<int>dis(200,0);
        solve(s,vis,dis);
        cout<<"Case "<<t<<endl;
        for(i='A';i<='Z';i++)
        {
            if(vis[i])
            {
                char c=i;
                cout<<c<<" = "<<dis[i]<<endl;
            }
        }
    }
}
