#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int n,k,m,i,j;
    double total=0,p=0,res=0;
    vector<long long int>v;
    scanf("%lld %lld %lld",&n,&k,&m);
    vector<long long int>cumulative_sum(n+1,0);
    for(i=0;i<n;i++)
    {
        scanf("%lld",&j);
        v.push_back(j);
        total+=j;
    }
    p=n;
    sort(v.begin(),v.end());
    for(i=1;i<=n;i++)
    {
        cumulative_sum[i]=cumulative_sum[i-1]+v[i-1];
    }
    res=(total+min(n*k,m))/p;
    for(i=1;i<=min(m,n-1);i++)
    {
        //cout<<"deleting "<<i<<" elements"<<endl;
        double cur=total-cumulative_sum[i]+min(m-i,k*(n-i));
        //cout<<"cur="<<cur<<endl;
        p=n-i;
        res=max(res,cur/p);

    }
    printf("%.14lf\n",res);

}
