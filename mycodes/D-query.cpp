#include<bits/stdc++.h>

using namespace std;

int last_pos[1000001];

struct q_ry{
    int start,ending,id;
};

bool operator <(q_ry a, q_ry b)
{
    if(a.ending!=b.ending)
        return a.ending<b.ending;
    return a.start>b.start;
}

void update(int val,int idx,int n,vector<int>&tree)
{
    while(idx<=n)
    {
        tree[idx]+=val;
        idx+=(idx&(-idx));
    }
    return;
}

int query(int idx,vector<int>&tree)
{
    int sum=0;
    while(idx>0)
    {
        sum+=tree[idx];
        idx-=(idx &(-idx));
    }
    return sum;
}

int main()
{
    int n,q,i,j,last=0;
    scanf("%d",&n);
    vector<int>v(n+1);
    vector<int>tree(n+1,0);
    for(i=1;i<=n;i++)
    {
        update(1,i,n,tree);
        scanf("%d",&v[i]);
    }
    //cout<<"total="<<query(n,tree)<<endl;
    vector<q_ry>queries;
    scanf("%d",&q);
    for(i=1;i<=q;i++)
    {
        q_ry tmp;
        scanf("%d %d",&tmp.start,&tmp.ending);
        tmp.id=i;
        queries.push_back(tmp);
    }

    sort(queries.begin(),queries.end());
    set<int>s;
    vector<int>ans(q+1);
    for(i=0;i<queries.size();i++)
    {
        int start=queries[i].start;

        cout<<"query start="<<queries[i].start<<" ending="<<queries[i].ending<<endl;
        cout<<"loop starts from "<<last+1<<" to "<<queries[i].ending<<endl;

        for(j=last+1;j<=queries[i].ending;j++)
        {
            cout<<"does "<<v[j]<<" exist ?"<<endl;
            int f,l;
            f=s.size();
            s.insert(v[j]);
            if(s.size()==f){
                cout<<"yes last pos was"<<last_pos[v[j]]<<endl;
                update(-1,last_pos[v[j]],n,tree);
                cout<<"updating with -1"<<endl;
                last_pos[v[j]]=j;
            }
            else
            {
                cout<<"No. updating last pos"<<endl;
                last_pos[v[j]]=j;
            }
        }
        ans[queries[i].id]=query(queries[i].ending,tree)-query(queries[i].start-1,tree);
        cout<<"ans is "<<ans[i]<<endl;
        last=max(last,queries[i].ending);
    }
    for(i=1;i<=q;i++)
    {
        printf("%d\n",ans[i]);
    }


}
