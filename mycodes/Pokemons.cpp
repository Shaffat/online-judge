#include<bits/stdc++.h>
using namespace std;

int main()
{
    double money;
    while(scanf("%lf",&money)!=EOF)
    {
        vector<double>price;
        int n,i,j;
        scanf("%d",&n);
        vector<double>mxsell(n);
        double mx_value=-1e9,p,mx=-1e18;
        for(i=1;i<=n;i++)
        {
            scanf("%lf",&p);
            price.push_back(p);
        }
        for(i=n-1;i>=0;i--)
        {
            mxsell[i]=mx_value;
            mx_value=max(mx_value,price[i]);
        }
        for(i=0;i<n;i++)
        {
            double profit=money*((mxsell[i]-price[i])/price[i]);
            mx=max(mx,profit);
        }
        printf("%.2lf\n",mx);
    }
}
