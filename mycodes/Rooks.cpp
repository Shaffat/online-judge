#include<bits/stdc++.h>
using namespace std;
long long int solve(long long int n,long long int k,long long int row,long long int remains)
{
    if(row<remains)
    {
        return 0;
    }
    if(remains==1)
    {
        return row*(n-(k-remains));
    }
    return (n-(k-remains))*solve(n,k,row-1,remains-1)+solve(n,k,row-1,remains);
}


int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        long long int  n,k,res;
        scanf("%lld %lld",&n,&k);

        vector<long long int>col(31,-1);
        vector<vector<long long int> >memory(31,col);
        if(n<k)
        {
            printf("Case %d: 0\n",t);
            t++;
            continue;
        }
        if(k==0)
        {
            printf("Case %d: 1\n",t);
            t++;
            continue;
        }
        res=solve(n,k,n,k);
        printf("Case %d: %lld\n",t,res);
        t++;
    }
}
