#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t,n,i,j;
    cin>>test;
    while(test--){
        string s;
        cin>>n;
        cin>>s;
        if(s[0]=='>' || s[s.size()-1]=='<'){
            cout<<"0"<<endl;
        }
        else{
            int l=0,r=0;
            for(i=0;i<s.size();i++){
                if(s[i]=='<'){
                    l++;
                }
                else
                    break;
            }
            for(i=s.size()-1;i>=0;i--){
                if(s[i]=='>'){
                    r++;
                }
                else
                    break;
            }
            cout<<min(l,r)<<endl;
        }
    }
}
