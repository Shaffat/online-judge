#include<bits/stdc++.h>
using namespace std;
#define ll long long int

struct rec{
ll i,j,k;
};

bool operator <(rec a, rec b){
    if(a.i!=b.i){
        return a.i<b.i;
    }
    return false;
}



int main()
{
    ll test,t,i,j,n,k,l,f;
    scanf("%lld",&test);
    for(t=1;t<=test;t++){
        scanf("%lld",&n);
        vector<ll>col(101,0);
        vector<vector<ll> >rectangle(101,col);
        for(l=1;l<=n;l++){
            scanf("%lld %lld %lld",&i,&j,&k);
            for(int m= 0;m<=k;m++){
                for(f=i;f<=j;f++){
                    rectangle[m][f]=1;
                }
            }
        }
        ll total=0;
        for(i=0;i<=100;i++){
            for(j=0;j<=100;j++){
                total+=rectangle[i][j];
            }
        }
        printf("%lld\n",total);

    }
}
