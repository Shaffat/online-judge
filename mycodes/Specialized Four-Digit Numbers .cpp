#include<stdio.h>
#include<vector>
using namespace std;

int converter(int number, int base)
{
    vector<int>pos;
    int k=base,sum=0;
    while(number)
    {
        sum+=number%base;
        number/=base;
    }

    return sum;
}

int main()
{
   int i,j;
   for(i=2992;i<=9999;i++)
   {
       if((converter(i,12)==converter(i,16))&&(converter(i,16)==converter(i,10)))
       {
           printf("%d\n",i);
       }
   }
}
