#include<bits/stdc++.h>
using namespace std;

double solve(int  pos,int n,vector<int>&gold,vector<double>&memory)
{
    //cout<<"pos="<<pos<<endl;
    if(pos==n)
    {
        return gold[pos];
    }
    if(memory[pos]!=-1)
    {
        return memory[pos];
    }
    int i,j;
    double res=0,total=0;
    for(i=1;i<=6;i++)
    {
        if(pos+i>n)
        {
            break;
        }
        else
        {
            total+=solve(pos+i,n,gold,memory);
        }
    }
    //cout<<"at pos="<<pos<<" total="<<total<<" option="<<i-1<<endl;
    total/=i-1;
    total+=gold[pos];

    return memory[pos]=total;
}

int main()
{
    int test,t=1,n,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>gold(n+1);
        vector<double>memory(n+1,-1);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            gold[i]=j;
        }
        printf("Case %d: %.10lf\n",t,solve(1,n,gold,memory));
    }
}
