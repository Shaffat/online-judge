#include<bits/stdc++.h>

using namespace std;

int ar[1001];
void sieve()
{
    int i,j,N=1000;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(N);i++)
    {
        for(j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
    }
}

int main()
{
    sieve();
    vector<int>v;
    for(int i=2;i<=1000;i++)
    {
        if(ar[i]==0)
        {
            v.push_back(i);
        }
    }
    int test,t;
    scanf("%d",&test);

        t=1;

        while(t<=test)
        {
            int n,i=0,j;
            scanf("%d",&n);
            vector<int>result(n+1,0);
            while(v[i]<=n)
            {
                int temp=v[i];
                while(temp<=n)
                {
                    result[v[i]]+=n/temp;
                    temp*=v[i];
                }
                i++;
            }
            printf("Case %d: %d = ",t,n);
            for(j=0;j<i;j++)
            {
                printf("%d (%d)",v[j],result[v[j]]);
                if(j<=i-2)
                {
                   printf(" * ");
                }
            }
            printf("\n");

           t++;


        }

}
