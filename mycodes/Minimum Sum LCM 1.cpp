#include <bits/stdc++.h>
#define LL long long int

using namespace std;

vector < LL > primes;
bool arr[10000000+5];
void Sieve() {
    //cout << "Debug 1" << endl;


    LL i, j, sz = sqrt(10000000);
    memset(arr, true, sizeof(arr));
    for (i = 4; i <= sz; i += 2) {
        arr[i] = false;
    }
    for (i = 3; i <= sz; i += 2) {
        if (arr[i]) {
            for (j = i * i; j <= 10000000; j += i) {
                arr[j] = false;
            }
        }
    }
    for (i = 2; i <= 10000000; ++i) {
        if (arr[i])
            primes.push_back(i);
    }
}

int main() {
    //cout << "Debug 0" << endl;
    LL n, cas = 0, multi, sum, i, primeNeed;
    Sieve();

//    for (n = 0; n < 30; ++n) {
//        cout << primes[n] << " ";
//    }


    while(scanf("%lld", &n) && n) {
        multi = 1;
        sum = primeNeed = 0;

        if (n == 1) {
            sum = 2;
        }
        else {
            for (i = 0; i < primes.size(); ++i) {
                if (n % primes[i] == 0) {
                    multi = 1;
                    while(n % primes[i] == 0 && n > 1) {
                        multi *= primes[i];
                        n /= primes[i];
                    }
                    sum += multi;
                    ++primeNeed;
                }
            }
            if (n > 1) {
                ++primeNeed;
                sum += n;
            }
            if (primeNeed <= 1){
                ++sum;
            }
        }
        printf("Case %lld: %lld\n", ++cas, sum);
    }

    return (0);
}

