#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long int llu;
unsigned long long int ways[100001];

llu countways(llu n,llu m)
{
    llu i,a=2,b=3,t;
     if(n==1)
        return 2;
    else if(n==2)
        return 3;
    else {
            for(i=3;i<=n;i++)
            {
                t=(a+b)%m;
                a=b%m;
                b=t%m;
            }
    }
    return t;
}



llu bigmod(llu base,llu power,llu mod,vector<long long int>&bigmodmemory)
{
    if(power==1)
    {
        return base;
    }
    if(bigmodmemory[power]!=-1)
    {
        return bigmodmemory[power];
    }
    return bigmodmemory[power]=(bigmod(base,power/2,mod,bigmodmemory)%mod*bigmod(base,power-power/2,mod,bigmodmemory)%mod)%mod;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        llu n,k,m,power;
        scanf("%llu %llu %llu",&n,&k,&m);
        power=countways(n,m);
       cout<<power<<endl;
//        cout<<n<<k<<m<<endl;
        vector<long long int>memo(power+1,-1);
        printf("Case %d: %llu\n",t,bigmod(k,power,m,memo));
        t++;
    }
}
