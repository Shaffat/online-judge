#include<bits/stdc++.h>
using namespace std;



int main()
{
   int t=1,test;
   scanf("%d",&test);
   while(t<=test)
   {
       int n,i,j,k,l,m,cnt=0;
       int city[100][100];
       int ar[100];
       scanf("%d",&n);
       for(i=1;i<=n;i++)
       {
           for(j=1;j<=n;j++)
           {
               scanf("%d",&k);
               city[i][j]=k;
           }
       }

       if(n==10)
       {
           //row check
           for(i=1;i<=10;i++)
           {
               int chk=1;
                for(int f=1;f<=12;f++)
                       {
                           ar[f]=0;
                       }
               for(j=1;j<=10;j++)
               {
                   k=city[i][j];
                   if(ar[k]==1)
                   {
                       chk=0;
                       break;
                   }
                   ar[k]=1;
               }
               if(chk)
               {
                   cnt++;
               }
           }

           //column check
           for(i=1;i<=10;i++)
           {
               int chk=1;
                for(int f=1;f<=12;f++)
                   {
                       ar[f]=0;
                   }
               for(j=1;j<=10;j++)
               {
                   k=city[j][i];
                   if(ar[k]==1)
                   {
                       chk=0;
                       break;
                   }
                   ar[k]=1;
               }
               if(chk)
               {
                   cnt++;
               }
           }

       }
       if(n>4)
       {
           //5 column each row
           for(i=1;i<n;i++)
           {
               for(j=1;j<=n-4;j++)
               {
                   for(int f=1;f<=12;f++)
                   {
                       ar[f]=0;
                   }
                   int chk=1;
                   for(k=j;k<=j+4;k++)
                   {
                       int f1=city[i][k],f2=city[i+1][k];
                       //cout<<"f1= "<<f1<<" f2="<<f2<<" i="<<i<<" k="<<k<<" "<<ar[f1]<<" "<<ar[f2]<<endl;
                       if(ar[f1]==1)
                       {

                           chk=0;
                           break;
                       }
                       ar[f1]=1;
                       if(ar[f2]==1)
                       {
                           chk=0;
                           break;
                       }
                       ar[f2]=1;
                   }
                   if(chk)
                   {
                       //cout<<"i= "<<i<<" j= "<<j<<endl;
                       cnt++;
                   }
               }
               //cout<<cnt<<endl;
           }
           //5 row each column
           for(i=1;i<n;i++)
           {
               for(j=1;j<=n-4;j++)
               {
                   int chk=1;
                   for(int f=1;f<=12;f++)
                   {
                       ar[f]=0;
                   }
                   for(k=j;k<=j+4;k++)
                   {
                       int f1=city[k][i],f2=city[k][i+1];
                        if(ar[f1]==1)
                       {

                           chk=0;
                           break;
                       }
                       ar[f1]=1;
                       if(ar[f2]==1)
                       {
                           chk=0;
                           break;
                       }
                       ar[f2]=1;
                   }
                   if(chk)
                   {
                       cnt++;
                   }
               }
           }
       }
       printf("Case %d: %d\n",t,cnt);
       t++;

   }

}
