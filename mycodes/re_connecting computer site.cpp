#include<bits/stdc++.h>

using namespace std;

struct edge
{
    int u,v,w;
};
bool operator <(edge x,edge y)
{
    if(x.w!=y.w)
    {
        return x.w<y.w;
    }
    return false;
}

int find_rep(int r,int *ar)
{
    if(ar[r]==r)
    {
        return r;
    }
    ar[r]=find_rep(ar[r],ar);
    return ar[r];
}

int main()
{
    int n,chk=0;
    while(scanf("%d",&n)!=EOF)
    {

            int m,k,i,j,pcost=0,ncost=0,c;
            if(chk==1)
            {
                printf("\n");
            }
            vector<edge>edges;

            for(i=1;i<n;i++)
            {
                int u,v,w;
                scanf("%d %d %d",&u,&v,&w);
                pcost+=w;
                edge nw;
                nw.u=u;
                nw.v=v;
                nw.w=w;
                edges.push_back(nw);
            }
            scanf("%d",&k);
            for(i=1;i<=k;i++)
            {
                int u,v,w;
                scanf("%d %d %d",&u,&v,&w);
                edge nw;
                nw.u=u;
                nw.v=v;
                nw.w=w;
                edges.push_back(nw);
            }
            scanf("%d",&m);
            for(i=1;i<=m;i++)
            {
                int u,v,w;
                scanf("%d %d %d",&u,&v,&w);
            }
            int parent[n+1];
            for(i=1;i<=n;i++)
            {
                parent[i]=i;
            }
            vector<int>rank_(n+1,0);
            sort(edges.begin(),edges.end());
            c=0;
            for(i=0;i<edges.size();i++)
            {
                int u,v;
                u=find_rep(edges[i].u,parent);
                v=find_rep(edges[i].v,parent);
                if(u!=v)
                {
                    if(rank_[u]==rank_[v])
                    {
                        parent[v]=u;
                        rank_[u]++;
                    }
                    else if(rank_[u]>rank_[v])
                    {
                        parent[v]=u;
                    }
                    else
                    {
                        parent[u]=v;
                    }
                    c++;
                    ncost+=edges[i].w;
                    if(c==n-1)
                    {
                        break;
                    }
                }


            }
            printf("%d\n%d\n",pcost,ncost);
            chk=1;
    }

}
