#include<bits/stdc++.h>
using namespace std;
#define mx 300001

int ar[mx];
int tree[4*mx];

void build_tree(int idx,int first,int last)
{
    int mid,i,j;
    if(first==last)
    {
        tree[idx]=ar[first];
        return;
    }
    mid=(first+last)/2;
    build_tree(2*idx,first,mid);
    build_tree((2*idx)+1,mid+1,last);
    tree[idx]=tree[2*idx]^tree[(2*idx)+1];
    return;
}

int query(int idx,int f,int l,int q_f,int q_l)
{
    int mid,i,j;
    if(f>q_l || l<q_f) return 0;
    if(f>=q_f && l<=q_l)
        return tree[idx];
    mid=(f+l)/2;
    return query(idx*2,f,mid,q_f,q_l)^query((2*idx)+1,mid+1,l,q_f,q_l);
}

int solve(int n)
{
    int i,j,res=0;
    for(i=1;i<n;i++)
    {
        for(j=i+1;j<=n;j+=2)
        {
            int mid=(i+j-1)/2;
            if(query(1,1,n,i,mid)==query(1,1,n,mid+1,j))
                res++;
        }
    }
    return res;
}

int main()
{
    int n,i,j,res;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        ar[i]=j;
    }
    build_tree(1,1,n);
    res=solve(n);
    printf("%d\n",res);
}
