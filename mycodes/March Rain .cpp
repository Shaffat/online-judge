#include<bits/stdc++.h>
using namespace std;

bool valid(vector<long long int>&holes,long long int k,long long int l){

    long long int i,j,last=-2e9;
    for(i=0;i<holes.size();i++){
        if(holes[i]-last>=l){
            k--;
            if(k<0) return false;
            else
                last=holes[i];
        }
    }
    return true;
}


long long int solve(long long int k,vector<long long int>&v){
    long long int first,last,mid,i,j,f=0,res=0;
    first=0;
    last=1e9;
    while(first<=last){
        if(f) break;
        if(first==last)f=1;
        mid=(first+last)/2;
        if(valid(v,k,mid)){
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}

int main(){

    long long int test,t,n,k,i,j,m;
    scanf("%lld",&test);
    for(i=1;i<=test;i++){
        scanf("%lld %lld",&n,&k);
        vector<long long int>holes;
        for(j=1;j<=n;j++){
            scanf("%lld",&m);
            holes.push_back(m);
        }
        long long int res=solve(k,holes);
        printf("%lld\n",res);
    }
}

