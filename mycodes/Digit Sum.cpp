#include<bits/stdc++.h>
using namespace std;

#define ll long long int


ll counter(ll pos, ll restriction, string &a,vector<ll>&count_memory,vector<ll>&total)
{
    cout<<"pos "<<pos<<" restriction="<<restriction<<endl;
    if(pos>=a.size()) return 1;
    ll i,j,res=0,r,cur;
    if(restriction==0 && count_memory[pos]!=-1)
    {
        cout<<"unrestricted memory for pos="<<pos<<" is "<<count_memory[pos]<<endl;
        return count_memory[pos];
    }
    if(!restriction)
    {
        for(i=0;i<=9;i++)
        {
            cur=counter(pos+1,0,a,count_memory,total);
            cout<<"unrestricted pos="<<pos<<" digit="<<i<<" ";
            cout<<"appeared "<<cur<<endl;
            res+=cur;
            total[i]=cur;
        }
    }
    else
    {
        r=0;
        for(i=0;i<=a[pos]-'0';i++)
        {
            if(i==a[pos]-'0') r=1;
            cur=counter(pos+1,r,a,count_memory,total);
            cout<<"unrestricted pos="<<pos<<" digit="<<i<<" ";
            cout<<"appeared "<<cur<<endl;
            res+=cur;
            total[i]+=cur;
        }
    }
    if(!restriction)
    {
        cout<<"unrestricted pos="<<res<<"total appearance ="<<res<<endl;
        count_memory[pos]=res;
    }
    return res;
}



int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,res,asum,bsum,i,j;
    string a,b;
    cin>>test;
    while(test--)
    {
        asum=0;
        bsum=0;
        cin>>a>>b;
        vector<ll>memory(20,-1);
        vector<ll>total(20,0);
        counter(0,1,a,memory,total);
        for(i=1;i<=9;i++)
        {
            cout<<"in a "<<i<<" occured "<<total[i]<<" times"<<endl;
            asum+=i*total[i];
        }
        vector<ll>memory1(20,-1);
        vector<ll>total1(20,0);
        counter(0,1,b,memory1,total1);
        for(i=1;i<=9;i++)
        {
            cout<<"in b "<<i<<" occured "<<total1[i]<<" times"<<endl;
            bsum+=i*total1[i];
        }
        cout<<bsum-asum<<endl;
    }
}
