#include<bits/stdc++.h>
using namespace std;


int solve(int enemy,int myteam,vector<int>&oposite,vector<int>&team,vector<vector<int> >&memory)
{
    //cout<<"enemy="<<enemy<<" team="<<myteam<<endl;
    if(enemy>=oposite.size()|| myteam>=team.size())
    {
        return 0;
    }
    if(memory[enemy][myteam]!=-1)
    {
        return memory[enemy][myteam];
    }
    int res1,res2,score=0;
    if(oposite[enemy]<team[myteam])
    {
        score=2;
    }
    else if(oposite[enemy]==team[myteam])
    {
        score=1;
    }
    res1=solve(enemy+1,myteam+1,oposite,team,memory)+score;
    res2=solve(enemy+1,myteam,oposite,team,memory);
    return memory[enemy][myteam]=max(res1,res2);

}

int main()
{
    int test,i,j,t=1,n;
    scanf("%d",&test);
    while(t<=test)
    {
        vector<int>v(51,-1);
        vector<vector<int> >memory(51,v);
        scanf("%d",&n);
        vector<int>team;
        vector<int>oposite;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            team.push_back(j);
        }
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            oposite.push_back(j);
        }
        sort(team.begin(),team.end());
        sort(oposite.begin(),oposite.end());
        i=0;
        j=0;
        sort(team.begin(),team.end());
        reverse(team.begin(),team.end());
        sort(oposite.begin(),oposite.end());
        reverse(oposite.begin(),oposite.end());
        int res=solve(0,0,oposite,team,memory);
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
