#include <cstdio>
#include <iostream>
#include <map>
using namespace std;

map<string, int> month;

struct sDays{
    string mon, occ;
    int date;
    int mn;
};
int main() {
    month["January"] = 1;
    month["February"] = 2;
    month["March"] = 3;
    month["April"] = 4;
    month["May"] = 5;
    month["June"] = 6;
    month["July"] = 7;
    month["August"] = 8;
    month["September"] = 9;
    month["October"] = 10;
    month["November"] = 11;
    month["December"] = 12;

    sDays days[20];

    days[0].date = 13;
    days[0].mn = 2;
    days[0].mon = "February";
    days[0].occ = "Pohela Falgun";

    days[1].date = 14;
    days[1].mn = 2;
    days[1].mon = "February";
    days[1].occ = "Valentine's Day";

    days[2].date = 21;
    days[2].mn = 2;
    days[2].mon = "February";
    days[2].occ = "International Mother Language Day";

    days[3].date = 8;
    days[3].mn = 3;
    days[3].mon = "March";
    days[3].occ = "International Women's Day";

    days[4].date = 21;
    days[4].mn = 3;
    days[4].mon = "March";
    days[4].occ = "International Color Day";

    days[5].date = 26;
    days[5].mn = 3;
    days[5].mon = "March";
    days[5].occ = "Bangladesh Independence Day";

    days[6].date = 1;
    days[6].mn = 4;
    days[6].mon = "April";
    days[6].occ = "April Fools Day";

    days[7].date = 14;
    days[7].mn = 4;
    days[7].mon = "April";
    days[7].occ = "Pohela Boishakh";

    days[8].date = 1;
    days[8].mn = 5;
    days[8].mon = "May";
    days[8].occ = "May Day";

    days[9].date = 19;
    days[9].mn = 6;
    days[9].mon = "June";
    days[9].occ = "Father's Day";

    days[10].date = 18;
    days[10].mn = 7;
    days[10].mon = "July";
    days[10].occ = "Mandela Day";

    days[11].date = 20;
    days[11].mn = 8;
    days[11].mon = "August";
    days[11].occ = "World Mosquito Day";


    days[12].date = 31;
    days[12].mn = 10;
    days[12].mon = "October";
    days[12].occ = "Halloween";

    days[13].date = 19;
    days[13].mn = 11;
    days[13].mon = "November";
    days[13].occ = "World Toilet Day";

    days[14].date = 10;
    days[14].mn = 12;
    days[14].mon = "December";
    days[14].occ = "Human Rights Day";

    days[15].date = 16;
    days[15].mn = 12;
    days[15].mon = "December";
    days[15].occ = "Victory Day of Bangladesh";

    days[16].date = 25;
    days[16].mn = 12;
    days[16].mon = "December";
    days[16].occ = "Christmas Day";

    int test, i, day, mNumber, j;
    string str;
    scanf("%d", &test);
    getchar();
    for (i = 1; i <= test; ++i) {
        cin >> str >> day;
        getchar();
        mNumber = month[str];
        printf("Case %d: ", i);
        if (mNumber == 12 && day >= 25) {
            printf("Pohela Falgun\n");
        }
        else {
            for (j = 0; j < 17; j++) {
                if(days[j].mn > mNumber) {
                    printf("%s\n", days[j].occ.c_str());
                    j = 17;
                }
                else if (days[j].mn == mNumber) {
                    if (days[j].date > day) {
                        printf("%s\n", days[j].occ.c_str());
                        j = 17;
                    }
                    else if (days[j].date == day) {
                        printf("%s\n", days[++j].occ.c_str());
                        j = 17;
                    }
                    else {

                    }
                }
            }
        }

    }


    return (0);
}
