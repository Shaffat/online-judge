#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int i,n,x,y,z;
    while(cin>>x>>y>>z)
    {
        int up,down;
        up=abs(x*y);
        down=abs(z-y);
        int g=GCD(up,down);
        up/=g;
        down/=g;
        cout<<up<<"/"<<down<<endl;
    }
}
