#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int n,i,j;
        scanf("%d",&n);
        vector<int>input;
        vector<int>output;
        vector<int>isstack;
        vector<int>isqueue;
        int s=1,q=1;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            input.push_back(j);
            isstack.push_back(j);
            isqueue.push_back(j);
        }
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            output.push_back(j);
        }
        reverse(isstack.begin(),isstack.end());
        for(i=0;i<output.size();i++)
        {
            if(output[i]!=isstack[i])
            {
                s=0;
            }
            if(output[i]!=isqueue[i])
            {
                q=0;
            }
        }
        if(s==1 && q==1)
        {
            printf("both\n");
        }
        else if(s==1)
        {
            printf("stack\n");
        }
        else if(q==1)
        {
            printf("queue\n");
        }
        else
            printf("neither\n");
    }
}
