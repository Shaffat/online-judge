#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

bool valid(int x,int y,int n,int m)
{
    if(x<=n && x>=1 && y<=m && y>=1)
        return true;
    return false;
}


pos getDownLeft(pos a,int k)
{
    pos nw;
    nw.y=a.y-2*k;
    nw.x=a.x-k;
    return nw;
}

pos getDownRight(pos a,int k)
{
    pos nw;
    nw.y=a.y-2*k;
    nw.x=a.x+k;
    return nw;
}

pos getUpLeft(pos a,int k)
{
    pos nw;
    nw.y=a.y+2*k;
    nw.x=a.x-k;
    return nw;
}

pos getUpRight(pos a,int k)
{
    pos nw;
    nw.y=a.y+2*k;
    nw.x=a.x+k;
    return nw;
}


pos getRightUp(pos a,int k)
{
    pos nw;
    nw.x=a.x+2*k;
    nw.y=a.y+k;
    return nw;
}

pos getRightDown(pos a,int k)
{
    pos nw;
    nw.x=a.x+2*k;
    nw.y=a.y-k;
    return nw;
}

pos getLeftUp(pos a,int k)
{
    pos nw;
    nw.x=a.x-2*k;
    nw.y=a.y+k;
    return nw;
}

pos getLeftDown(pos a,int k)
{
    pos nw;
    nw.x=a.x-2*k;
    nw.y=a.y-k;
    return nw;
}

void bfs(pos root,int n,int m,int k,vector<vector<int> >&counter,vector<vector<int> >&vis,vector<vector<int> >&dis,vector<vector<int> >&res)
{
    //cout<<"root ="<<root.x<<" "<<root.y<<" k="<<k<<endl;
    int i=1,j;
    queue<pos>q;
    q.push(root);
    dis[root.x][root.y]=0;
    vis[root.x][root.y]=1;
    counter[root.x][root.y]++;
    pos child,cur;
        while(!q.empty())
        {
            cur=q.front();
            q.pop();
            //cout<<"cur="<<cur.x<<" "<<cur.y<<" counter="<<counter[cur.x][cur.y]<<" dis="<<dis[cur.x][cur.y]<<" res="<<res[cur.x][cur.y]<<endl;
            child=getDownLeft(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getDownRight(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getUpLeft(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getUpRight(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getLeftDown(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getLeftUp(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getRightDown(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
            child=getRightUp(cur,i);
            if(valid(child.x,child.y,n,m))
            {
                if(!vis[child.x][child.y])
                {
                    vis[child.x][child.y]=1;
                    dis[child.x][child.y]=dis[cur.x][cur.y]+1;
                    double f=dis[child.x][child.y];
                    int r=ceil(f/k);
                    res[child.x][child.y]+=r;
                    counter[child.x][child.y]++;
                    q.push(child);
                }

            }
        }

    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,n,m,i,j,c,k;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        k=0;
        cin>>n>>m;


        vector<int>col(m+1,0);
        vector<vector<int> >counter(n+1,col);

        vector<vector<int> >res(n+1,col);

        for(i=1;i<=n;i++)
        {
            string s;
            cin>>s;
            for(j=1;j<=m;j++)
            {

                char c;
                c=s[j-1];
                //cout<<"c="<<c<<endl;
                if(c!='.')
                {
                    k++;
                    vector<vector<int> >vis(n+1,col);
                    vector<vector<int> >dis(n+1,col);
                    pos tmp;
                    tmp.x=i;
                    tmp.y=j;
                    bfs(tmp,n,m,c-'0',counter,vis,dis,res);
                }

            }
        }
        //cout<<"k="<<k<<endl;
        int ans=2e9;
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=m;j++)
            {
                if(counter[i][j]==k)
                {
                    ans=min(ans,res[i][j]);
                }
            }
        }
       if(ans==2e9)
        cout<<"Case "<<t<<": -1"<<endl;
       else
        cout<<"Case "<<t<<": "<<ans<<endl;
    }
}
