#include<bits/stdc++.h>
using namespace std;

struct node
{
    int x,y;
};

vector<string>room;
int n,m,k;

bool invalid(int x,int y,int n,int m)
{
    if(x<0 || x>=n || y<0 || y>=m)
    {
        return true;
    }
    return false;
}

int bfs(node s,node t,vector<string>&room,vector<vector<int> >&vis,vector<vector<int> >&dis,int n,int m,int k)
{
    int i,j;
    vis[s.x][s.y]=1;
    queue<node>q;
    q.push(s);
    node cur,nxt;
    while(!q.empty())
    {
        cur.x=q.front().x;
        cur.y=q.front().y;
        cout<<"cur="<<cur.x<<" "<<cur.y<<" dis="<<dis[cur.x][cur.y]<<endl;
        q.pop();
        if(cur.x==t.x & cur.y==t.y)
        {
            return dis[cur.x][cur.y];
        }
        for(i=1;i<=k;i++)
        {
            nxt.x=cur.x;
            nxt.y=cur.y+i;
            if(!invalid(nxt.x,nxt.y,n,m))
            {
                if(vis[nxt.x][nxt.y]==0 && room[nxt.x][nxt.y]=='.')
                {
                    cout<<"nxt="<<nxt.x<<" "<<nxt.y<<endl;
                    cout<<"vis="<<vis[nxt.x][nxt.y]<<" can="<<room[nxt.x][nxt.y]<<endl;
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                    q.push(nxt);
                }
                else if(vis[nxt.x][nxt.y]>vis[cur.x][cur.y]+1)
                {
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                }
                else break;
            }
            else
                break;
        }
        for(i=1;i<=k;i++)
        {
            nxt.x=cur.x;
            nxt.y=cur.y-i;
            if(!invalid(nxt.x,nxt.y,n,m))
            {
                if(vis[nxt.x][nxt.y]==0 && room[nxt.x][nxt.y]=='.')
                {
                    cout<<"nxt="<<nxt.x<<" "<<nxt.y<<endl;
                    cout<<"vis="<<vis[nxt.x][nxt.y]<<" can="<<room[nxt.x][nxt.y]<<endl;
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                    q.push(nxt);
                }
                else if(vis[nxt.x][nxt.y]>vis[cur.x][cur.y]+1)
                {
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                }
                else
                    break;
            }
            else
                break;
        }
        for(i=1;i<=k;i++)
        {
            nxt.x=cur.x+i;
            nxt.y=cur.y;
            if(!invalid(nxt.x,nxt.y,n,m))
            {
                if(vis[nxt.x][nxt.y]==0 && room[nxt.x][nxt.y]=='.')
                {
                    cout<<"nxt="<<nxt.x<<" "<<nxt.y<<endl;
                    cout<<"vis="<<vis[nxt.x][nxt.y]<<" can="<<room[nxt.x][nxt.y]<<endl;
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                    q.push(nxt);
                }
                else if(vis[nxt.x][nxt.y]>vis[cur.x][cur.y]+1)
                {
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                }
                 else  break;
            }
            else
                break;
        }
        for(i=1;i<=k;i++)
        {
            nxt.x=cur.x-i;
            nxt.y=cur.y;
            if(!invalid(nxt.x,nxt.y,n,m))
            {
                if(vis[nxt.x][nxt.y]==0 && room[nxt.x][nxt.y]=='.')
                {
                    cout<<"nxt="<<nxt.x<<" "<<nxt.y<<endl;
                    cout<<"vis="<<vis[nxt.x][nxt.y]<<" can="<<room[nxt.x][nxt.y]<<endl;
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                    q.push(nxt);
                }
                else if(vis[nxt.x][nxt.y]>vis[cur.x][cur.y]+1)
                {
                    vis[nxt.x][nxt.y]=1;
                    dis[nxt.x][nxt.y]=dis[cur.x][cur.y]+1;
                }

                else
                    break;
            }
            else
                break;
        }
    }
    return -1;
}

int main()
{
    int i,j,n,m,k;
    string s;
    cin>>n>>m>>k;
    vector<string>room;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        room.push_back(s);
        //cout<<"s="<<s<<endl;
    }
    node d,t;
    cin>>d.x>>d.y>>t.x>>t.y;
    d.x--;
    d.y--;
    t.x--;
    t.y--;
    vector<int>col1(n+1,0);
    vector<vector<int> >dis(m+1,col1);
    vector<vector<int> >vis(m+1,col1);
    cout<<bfs(d,t,room,vis,dis,n,m,k);
}
