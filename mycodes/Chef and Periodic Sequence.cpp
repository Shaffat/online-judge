#include<bits/stdc++.h>
using namespace std;

bool infinity(int nonzero,vector<int>&v)
{
    if(nonzero==-1) return true;
    int i,j;
    int cur_pos=nonzero+1;
    int cur_val=v[nonzero]+1;
    while(cur_pos<v.size())
    {
        if(v[cur_pos]!=-1)
        {
            if(v[cur_pos]!=cur_val)
                return false;
        }
        cur_pos++;
        cur_val++;
    }
    cur_pos=nonzero-1;
    cur_val=v[nonzero]-1;
    while(cur_pos>=0)
    {
        if(v[cur_pos]!=-1)
        {
            if(v[cur_pos]!=cur_val)
                return false;
        }
        cur_pos--;
        cur_val--;
    }
    return true;
}
bool ok(int pos,int k,vector<int>&v)
{
    int i,j;
    int cur_pos,cur_val;
    cur_pos=pos;
    cur_val=v[pos];
    while(cur_pos>=0)
    {
        if(cur_val==0) cur_val=k;
        if(v[cur_pos]!=cur_val && v[cur_pos]!=-1){
            //cout<<"doesnt match  curpos="<<cur_pos<<" val="<<cur_val<<" k="<<k<<endl;
            return false;
        }
        cur_pos--;
        cur_val--;
    }
    cur_pos=pos;
    cur_val=v[pos];
    while(cur_pos<v.size())
    {
        if(cur_val==k+1) cur_val=1;
        if(v[cur_pos]!=cur_val && v[cur_pos]!=-1){
            //cout<<"doesnt match  curpos="<<cur_pos<<" val="<<cur_val<<" k="<<k<<endl;
            return false;
        }
        cur_pos++;
        cur_val++;
    }
    return true;
}


int solve(int nonzero_pos,vector<int>&v)
{
    int i,j,first,last,mid,res=-1,f=0;
    if(nonzero_pos>=0)
    first=v[nonzero_pos];
    else
        first=1;
    last=first+1e5+1;
    for(i=last;i>=first;i--)
    {
        if(ok(nonzero_pos,i,v)) return i;
    }
    return res;
}

int main()
{
    int test,t,i,j,n,nonozero,mx;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        nonozero=-1,mx=-1;
        scanf("%d",&n);
        vector<int>v(n);
        for(i=0;i<n;i++)
        {
            scanf("%d",&j);
            v[i]=j;
            if(mx<j)
            {
                mx=j;
                nonozero=i;
            }
        }
       int res=solve(nonozero,v);
       if(infinity(nonozero,v))
       {
           printf("inf\n");
       }
       else if(res<0)
       {
           printf("impossible\n");
       }
       else
        printf("%d\n",res);
    }
}
