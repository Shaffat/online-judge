#include<bits/stdc++.h>
using namespace std;
#define ll long long int
int main()
{
    ll n,x,y,i,j,d,test,done,res1,res2,res3;
    double d1,res;
    cin>>test;
    while(test--)
    {
        cin>>n>>x>>y>>d;
        d1=d;
        done=0;
        res=2e10;
        if(abs(x-y)%d==0)
        {
            done=1;
            res=min(res,ceil(abs(x-y)/d1));
        }
        if(abs(y-n)%d==0)
        {
            done=1;
            res=min(res,ceil(abs(n-y)/d1)+ceil(abs(n-x)/d1));
        }
        if(abs(y-1)%d==0)
        {
            done=1;
            res=min(res,ceil(abs(1-y)/d1)+ceil(abs(1-x)/d1));
        }
        if(!done) cout<<"-1"<<endl;
        else
        cout<<res<<endl;
    }
}
