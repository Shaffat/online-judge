
#include <bits/stdc++.h>

using namespace std;

struct team{
    int id, solve, totalPenalty;
};

struct teamFrozen{
    int solve, achivePenalty, M, frozenPenalty;
    vector < int > submissions;
};

teamFrozen frozenStanding[55];

void clearPreviousStanding(int n) {
    for (int i = 0; i <= n; ++i) {
        frozenStanding[i].submissions.clear();
    }
}

int main() {

   // freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);

    int test, t, n, id, s, tp, x, m, i, j;
    vector < int > leckStanding;
    scanf("%d", &test);
    for (t = 1; t <= test; ++t) {
        scanf("%d", &n);
        clearPreviousStanding(n);
        leckStanding.clear();
        for (i = 0; i < n; ++i) {
            scanf("%d %d %d %d", &id, &s, &tp, &m);
            frozenStanding[id].M = m;
            frozenStanding[id].solve = s;
            frozenStanding[id].achivePenalty = tp;
            frozenStanding[id].frozenPenalty = 0;
            for (j = 0; j < m; ++j) {
                scanf("%d", &x);
                frozenStanding[id].frozenPenalty += x;
                frozenStanding[id].submissions.push_back(x);
            }
        }
        for (i = 0; i < n; ++i) {
            scanf("%d", &x);
            leckStanding.push_back(x);
        }

        int currentSolve = INT_MAX, currentPenalty = 0, extraTime;
        bool flag = true;

        for (i = 0; i < n; ++i) {
            id = leckStanding[i];
            if (frozenStanding[id].solve > currentSolve) {
                // not possible
                flag = false;
            }
            else if (frozenStanding[id].solve == currentSolve && frozenStanding[id].achivePenalty < currentPenalty) {
                // not possible
                flag = false;
            }
            extraTime = 0;
            for (j = 0; j < frozenStanding[id].M; ++j) {
                //extraTime += frozenStanding[id].submissions[j];
                if (currentSolve > frozenStanding[id].solve + 1) {
                    frozenStanding[id].solve += 1;
                    frozenStanding[id].achivePenalty += frozenStanding[id].submissions[j];
                }
                else if (currentSolve == frozenStanding[id].solve + 1) {
                    if (currentPenalty <= frozenStanding[id].achivePenalty + frozenStanding[id].submissions[j]) {
                        frozenStanding[id].solve += 1;
                        frozenStanding[id].achivePenalty += frozenStanding[id].submissions[j];
                    }
                    else {
                        break;
                    }
                }
                else break;
            }
            currentSolve = frozenStanding[id].solve;
            currentPenalty = frozenStanding[id].achivePenalty;
        }

        if (flag) {
            printf("Case %d: We respect our judges :)\n", t);
        }
        else {
            printf("Case %d: Say no to rumour >:\n", t);
        }


    }


    return (0);
}
