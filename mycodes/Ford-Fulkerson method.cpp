#include<bits/stdc++.h>
using namespace std;
struct node
{
    int vertext,flow;
};
int send_flow(int s,int t,vector<vector<int> >&connections,vector<vector<int> >&capacity,vector<int>&parent)
{
    int i,j,cur_node,nxt_node;

    parent[s]=s;
    node cur,nxt;
    cur.vertext=s;
    cur.flow=2e9;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        if(cur.vertext==t)
        {
            return cur.flow;
        }
        q.pop();
        cur_node=cur.vertext;
        //cout<<"cur_node="<<cur.vertext<<" flow="<<cur.flow<<endl;
        for(i=0;i<connections[cur.vertext].size();i++)
        {
            nxt_node=connections[cur.vertext][i];
            //cout<<"next node="<<nxt_node<<"  capacity="<<capacity[cur_node][nxt_node]<<" parent="<<parent[nxt_node]<<endl;
            if(parent[nxt_node]==-1 && capacity[cur_node][nxt_node]>0)
            {
                parent[nxt_node]=cur_node;
                nxt.vertext=nxt_node;
                nxt.flow=min(cur.flow,capacity[cur_node][nxt_node]);
                q.push(nxt);
            }
        }
    }
    return 0;
}

int get_maxflow(int n,int s,int t,vector<vector<int> >&connections,vector<vector<int> >&capacity)
{
    cout<<"s="<<s<<" t="<<t<<endl;
    int i,j,res,total_flow=0;
    vector<int>parent(n+1);
     for(i=1;i<=n;i++)
     {
        parent[i]=-1;
     }
    while(res=send_flow(s,t,connections,capacity,parent))
    {
        cout<<"path\n";
        cout<<"flow="<<res<<endl;
        total_flow+=res;
        int cur=t,pre;
        while(cur!=s)
        {
            pre=parent[cur];

            capacity[pre][cur]-=res;
            capacity[cur][pre]+=res;
            cout<<pre<<" "<<cur<<endl;
            cur=pre;
        }
        for(i=1;i<=n;i++)
        {
            parent[i]=-1;
        }

    }
    return total_flow;
}
int main()
{
    int n,m,u,v,w,i;
    scanf("%d %d",&n,&m);
    vector<vector<int> >connection(n+1);
    vector<int>col(n+1,0);
    vector<vector<int> >capacity(n+1,col);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d %d",&u,&v,&w);
        connection[u].push_back(v);
        connection[v].push_back(u);
        capacity[u][v]+=w;
    }
    cout<<get_maxflow(n,1,n,connection,capacity);
}
