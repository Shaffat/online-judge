#include<bits/stdc++.h>
using namespace std;

int count_pair(string &s)
{
    int i,j,counter=0;
    for(i=1;i<s.size();i++)
    {
        //cout<<"i="<<i<<endl;
        if(s[i]==s[i-1])
        {
            //cout<<" match in "<<i<<" "<<i-1<<endl;
            s.erase(s.begin()+i);
            s.erase(s.begin()+i-1);
            counter++;
            i=max(-1,i-2);
        }
    }
    return counter;
}


int main()
{
    string s;
    cin>>s;
    int i,j,counter=0,tmp;
    while(1)
    {
        tmp=count_pair(s);
        counter+=tmp;
        if(tmp==0) break;
    }
    if(counter%2==1) cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}
