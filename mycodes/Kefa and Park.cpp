#include<bits/stdc++.h>
using namespace std;

int solve(int node,int total,int m,vector<int>&vis,vector<vector<int> >&connection,vector<int>&cats)
{
    if(total>m) return 0;

    if(connection[node].size()==1 && node!=1){
        return 1;
    }

    int i,j,child,res=0;

    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(vis[child]==0){
            vis[child]=1;
            if(cats[child]==0){
                res+=solve(child,0,m,vis,connection,cats);
            }
            else{
                res+=solve(child,total+1,m,vis,connection,cats);
            }
        }
    }
    return res;

}

int main()
{
    int n,m,i,j,u,v;
    scanf("%d %d",&n,&m);
    vector<int>vis(n+1,0);
    vector<int>col;
    vector<vector<int> >connection(n+1,col);
    vector<int>cat(n+1,0);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        cat[i]=j;
    }
    for(i=1;i<n;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }
    vis[1]=1;

    int res=solve(1,cat[1],m,vis,connection,cat);
    printf("%d\n",res);

}
