
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/

#include<bits/stdc++.h>
using namespace std;


struct salon
{
    int cost;
    double p;
};

bool operator <(salon a, salon b)
{
    if(a.p!=b.p)
    {
        return a.p<b.p;
    }
    return false;
}

double solve(int pos,vector<salon>&coach,vector<double>&memory)
{
    if(coach.size()==pos)
    {
        return 0;
    }
    if(memory[pos]!=-1)
    {
        return memory[pos];
    }

    double res1,res2,res;
    res1=(solve(pos+1,coach,memory)+coach[pos].cost);
    cout<<"res1="<<res1<<endl;
    res1*=(1-coach[pos].p);
    res2=solve(pos+1,coach,memory)*coach[pos].p;
    cout<<"memory["<<pos<<"]="<<res1+res2<<" res1="<<res1<<" res2="<<res2<<" cost="<<coach[pos].cost<<endl;
    return memory[pos]=res1+res2;
}

int main()
{
    //ios_base::sync_with_stdio(0);

    int n,i,j;
    double p,total=0;
    scanf("%d",&n);
    vector<salon>coach;
    for(i=1;i<=n;i++)
    {
        salon tmp;
        scanf("%d %lf",&tmp.cost,&tmp.p);
        coach.push_back(tmp);
    }
    sort(coach.begin(),coach.end());

    vector<double>memory(n+1,-1);

    solve(0,coach,memory);

    for(i=n-1;i>=0;i--)
    {
        total+=memory[i]+total;
    }
    printf("%.9lf\n",total/n);
    return 0;
}
