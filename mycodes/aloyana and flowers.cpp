#include<bits/stdc++.h>
using namespace std;
struct range
{
    int l,r;
};
vector<range>sub;
vector<int>memory(101,-1);
int solve(int pos,vector<int>&cumulative)
{
    if(pos>=sub.size())
    {
        return 0;
    }
    if(memory[pos]!=-1)
    {
        return memory[pos];
    }
    int l,r;
    l=sub[pos].l;
    r=sub[pos].r;
    int case1=solve(pos+1,cumulative)+cumulative[r]-cumulative[l-1];
    int case2=solve(pos+1,cumulative);
    return memory[pos]=max(case1,case2);
}
int main()
{
    int n,m,i,j,l,r,res;
    scanf("%d %d",&n,&m);
    vector<int>v(n+1,0);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v[i]=v[i-1]+j;
    }
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&l,&r);
        range tmp;
        tmp.l=l;
        tmp.r=r;
        sub.push_back(tmp);
    }
    res=solve(0,v);
    cout<<res<<endl;
}
