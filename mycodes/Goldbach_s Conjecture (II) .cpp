#include<bits/stdc++.h>

using namespace std;
vector<int>numbers(1e5,0);
vector<int>primes;
void sieve()
{
    int i,j;
    numbers[1]=1;
    for(i=4;i<1e5;i+=2)
    {
        numbers[i]=1;
    }
    for(i=3;i<=sqrt(1e5);i+=2)
    {
        if(numbers[i]==0)
        {
            //cout<<"prime="<<i<<" "<<endl;
            for(j=i*i;j<1e5;j+=i)
            {
                numbers[j]=1;
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e5;i+=2)
    {
        if(numbers[i]==0)
        {
            primes.push_back(i);
        }
    }
}
int counting(int n)
{
    int res=0,i,j;
    for(i=0;i<primes.size();i++)
    {
        if(primes[i]>n/2)
        {
            break;
        }
        if(numbers[n-primes[i]]==0)
        {
            res++;
        }
    }
    return res;

}

int main()
{
    int i,j;
    sieve();
    while(scanf("%d",&i))
    {
        if(i==0)
        {
            return 0;
        }
        printf("%d\n",counting(i));
    }
}
