#include<bits/stdc++.h>
using namespace std;

#define REP(i,a,b)       for(int i=a;i<=b;i++)
#define RREP(i,a,b)      for(int i=a;i>=b;i--)
#define sc3(a,b,c)       scanf("%d %d %d",&a,&b,&c)
#define sc2(a,b)       scanf("%d %d",&a,&b)
#define sc1(a)         scanf("%d",&a)

int main()
{
    int n,k,i,j;
    sc2(n,k);
    vector<int>v;
    int mn=2e9,total=0;

    REP(i,1,n)
    {
        sc1(j);
        if(j<0)
        {
            if(k>0)
            {
                k--;
                j*=-1;
            }
        }
        if(abs(j)<mn)
        {
            mn=abs(j);
        }
        total+=j;
    }

    if(k>0)
    {
        if(k%2==1)
        {
            total-=mn;
        }
    }
    cout<<total<<endl;
}
