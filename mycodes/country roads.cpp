#include<bits/stdc++.h>

using namespace std;

struct vertex
{
    int v,w;
};

bool operator <(vertex a,vertex b)
{
    if(a.w!=b.w)
    {
        return a.w>b.w;
    }
    return false;
}
void djsktra(int node,vector<vector<int> >&connection,vector<vector<int> >&cost,vector<int>&dis)
{
    int i,j;
    priority_queue<vertex>q;
    vector<int>vis(10001,0);
    vertex start;
    start.v=node;
    start.w=0;
    q.push(start);
    dis[node]=0;
    while(!q.empty())
    {
        vertex cur=q.top();
        q.pop();
        if(vis[cur.v]==1)
        {
            continue;
        }
        vis[cur.v]=1;
        int cur_node=cur.v,cur_w=cur.w;
        for(i=0;i<connection[cur_node].size();i++)
        {
            int nxt_node=connection[cur_node][i];
            int nxt_cost=max(cost[cur_node][i],cur_w);
            if(dis[nxt_node]>nxt_cost)
            {
                dis[nxt_node]=nxt_cost;
                vertex tmp;
                tmp.v=nxt_node;
                tmp.w=nxt_cost;
                q.push(tmp);
            }
        }
    }
    return;
}


int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,m,i,j,u,v,w,root;
        scanf("%d %d",&n,&m);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            connection[v].push_back(u);
            cost[u].push_back(w);
            cost[v].push_back(w);
        }
        scanf("%d",&root);
        vector<int>dis(n+1,2e9);
        djsktra(root,connection,cost,dis);
        printf("Case %d:\n",t);
        for(i=0;i<n;i++)
        {
            if(dis[i]>=1e9)
            {
                printf("Impossible\n");
            }
            else
            printf("%d\n",dis[i]);
        }
    }
}
