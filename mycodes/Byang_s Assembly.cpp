#include<bits/stdc++.h>
using namespace std;
#define ll long long int

void print(vector<ll>&registers)
{
    ll i,j;
    for(i='A';i<='F';i++)
    {
        cout<<registers[i]<<" ";
    }
    cout<<registers['P']<<endl;
}

ll getVal(string &s)
{
    ll i,j=1,res=0;
    for(i=s.size()-1;i>=0;i--)
    {
        if(s[i]==' ' || s[i]=='-' || s[i]=='+')
            break;
        res+=(s[i]-'0')*j;
        j*=10;
    }
    if(s[i]=='-')
        res*=-1;
    //cout<<"val is "<<res<<endl;
    return res;
}

string commandName(string &s)
{
    ll i,j;
    string res="";
    for(i=0;i<s.size();i++)
    {
        if(s[i]==' ') break;
        res.push_back(s[i]);
    }
    return res;
}


void LODCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    x=s[4];
    val=getVal(s);
    registers[x]=val;
    registers['P']++;
    return;
}


void INCCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    x=s[4];
    registers[x]++;
    registers['P']++;
    return;
}

void ADDCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    x=s[4];
    y=s[6];
    registers[x]+=registers[y];
    registers['P']++;
    return;
}
void MULCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    x=s[4];
    y=s[6];
    registers[x]*=registers[y];
    registers['P']++;
    return;
}

void JMPCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    val=getVal(s);
    registers['P']=val;
    return;
}

void CMPCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    x=s[4];
    y=s[6];
    if(registers[x]==registers[y]){
        registers['C']=0;
    }
    else if(registers[x]<registers[y])
    {
        registers['C']=-1;
    }
    else
        registers['C']=1;
    registers['P']++;
    return;
}

void JCZCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    val=getVal(s);
    if(registers['C']==0)
        registers['P']=val;
    else
        registers['P']++;
    return;
}

void JCPCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    val=getVal(s);
    if(registers['C']>0){
        //cout<<"its positive"<<endl;
        registers['P']=val;
    }
    else{
        //cout<<"INCREMENT   ________________"<<endl;
        registers['P']++;
    }
    //cout<<"counter is at "<<registers['P']<<endl;
    return;
}

void JCNCmd(string &s,vector<ll>&registers)
{
    ll x,y,val;
    val=getVal(s);
    if(registers['C']<0)
        registers['P']=val;
    else
        registers['P']++;
    return;
}


void RunCmd(string &s,vector<ll>&registers)
{
    string cmdName=commandName(s);
    if(cmdName=="LOD")
    {
        LODCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="ADD")
    {
        ADDCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="INC")
    {
        INCCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="MUL")
    {
        MULCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="JMP")
    {
        JMPCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="CMP")
    {
        CMPCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="JCZ")
    {
        JCZCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="JCN")
    {
        JCNCmd(s,registers);
        //print(registers);
    }
    else if(cmdName=="JCP")
    {
        JCPCmd(s,registers);
        //print(registers);
    }
    return;
}



int main()
{
    ios_base::sync_with_stdio(0);
    ll i,j,n,m,x,y,p,counter=0;
    vector<ll>registers(200,0);
    string s;
    vector<string>instructions;
    while(getline(cin,s))
    {
        instructions.push_back(s);
    }
    while(registers['P']<instructions.size() && counter<=1000 )
    {
        counter++;
        //cout<<"execute "<<instructions[registers['P']]<<endl;
        RunCmd(instructions[registers['P']],registers);
    }
    print(registers);

}
