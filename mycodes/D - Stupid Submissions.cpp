#include<bits/stdc++.h>
using namespace std;

struct verdict
{
    string s;
    int x;
};

int main()
{
    ios_base::sync_with_stdio(0);

    int t;
    cin>>t;

    for(int T=1;T<=t;T++)
    {
        int n,m,k;
        cin>>n>>m>>k;

        int last=k;

        string arr[n];
        for(int i=0;i<n;i++)
        {
            cin>>arr[i];

            if(i>=k && arr[i]=="S")
                last=i;
        }

        verdict a[m];
        for(int i=0;i<m;i++)
        {
            cin>>a[i].s;

            if(a[i].s=="W")
                cin>>a[i].x;
        }

        int counter=0;
        for(int i=0;i<m;i++)
        {
            if(a[i].s=="A")
                k=last+1;
            else
            {
                if(a[i].x<=k)
                {
                    if(arr[a[i].x-1]=="S")
                        counter++;
                }
                else if(a[i].x>k)
                {
                    if(arr[a[i].x-1]=="S")
                        k=a[i].x;
                }
            }
            //cout<<k<<endl;
        }
        cout<<counter<<endl;
    }

    return 0;
}
/*
5 5 2
S S B S B
W 3
W 4
W 4
A
W 4
*/
