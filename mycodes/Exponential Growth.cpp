#include<bits/stdc++.h>

using namespace std;

#define ll long long int

#define mod 1000000007
ll solve(ll  n,ll p)
{
    if(p==0)
        return 1;
    if(p==1)
        return n;
    ll x=solve(n,p/2);
    x=(x*x)%mod;
    if(p%2==1)
    {
        x*=n;
    }
    x%=mod;
    return x;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll n,p;
    cin>>n>>p;
    cout<<solve(n,p)<<endl;
}
