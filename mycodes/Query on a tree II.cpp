#include<bits/stdc++.h>
using namespace std;

#define ll long long int

void dfs(ll node,vector<vector<ll> >&connection,vector<ll>&vis,vector<ll>&level,vector<vector<ll> >&cost,
         vector<vector<ll> >&disSparse,vector<vector<ll> >&parentSparse)
{
  ll i,j,child;
  for(i=0;i<connection[node].size();i++)
  {
      child=connection[node][i];
      if(!vis[child])
      {
          vis[child]=1;
          parentSparse[child][0]=node;
          disSparse[child][0]=cost[node][i];
          level[child]=level[node]+1;
          dfs(child,connection,vis,level,cost,disSparse,parentSparse);
      }
  }
  return;
}


void lcaInit(ll n,vector<vector<ll> >&connection,vector<vector<ll> >&cost,vector<vector<ll> >&disSparse,
             vector<ll>&level,vector<vector<ll> >&parentSpars)
{
    vector<ll>vis(n+1,0);
    ll i,j;
    vis[1]=1;
    level[1]=0;
    dfs(1,connection,vis,level,cost,disSparse,parentSpars);
    for(i=1;i<=16;i++)
    {
        for(j=1;j<=n;j++)
        {
            if(parentSpars[j][i-1]!=-1)
            {
                parentSpars[j][i]=parentSpars[parentSpars[j][i-1]][i-1];
                if(parentSpars[j][i]!=-1)
                {
                    disSparse[j][i]=disSparse[j][i-1]+disSparse[parentSpars[j][i-1]][i-1];
                }
            }
        }
    }
    return;
}



ll disQuery(ll p,ll q,vector<vector<ll> >&disSparse,vector<vector<ll> >&parentSparse,vector<ll>&level)
{

    ll ptmp=p,qtmp=q,i,j,lca,total=0;
    if(level[p]<level[q])
    {
        swap(ptmp,qtmp);
    }

    for(i=16;i>=0;i--)
    {
        if(level[ptmp]-(1<<i)>=level[qtmp])
        {
            //cout<<"before ptmp="<<ptmp<<"total="<<total<<endl;
            total+=disSparse[ptmp][i];
            ptmp=parentSparse[ptmp][i];
            //cout<<"after ptmp="<<ptmp<<"total="<<total<<endl;
        }
    }
    //cout<<"ptmp="<<ptmp<<" qtmp="<<qtmp<<" total="<<total<<endl;
    if(ptmp!=qtmp){
        for(i=16;i>=0;i--)
        {
            if(parentSparse[ptmp][i]!=-1 && parentSparse[qtmp][i]!=-1 && parentSparse[ptmp][i]!=parentSparse[qtmp][i])
            {
                total+=disSparse[ptmp][i]+disSparse[qtmp][i];
                //cout<<"ptmp="<<ptmp<<" qtmp="<<qtmp<<" dis="<<total<<endl;
                ptmp=parentSparse[ptmp][i];
                qtmp=parentSparse[qtmp][i];
            }
        }
        total+=disSparse[ptmp][0]+disSparse[qtmp][0];
    }
    return total;
}

ll posQuery(ll p,ll q,ll x,vector<vector<ll> >&disSparse,vector<vector<ll> >&parentSparse,vector<ll>&level)
{
    ll ptmp=p,qtmp=q,i,j,lca,res;
    if(level[p]<level[q])
    {
        swap(ptmp,qtmp);
    }
    for(i=16;i>=0;i--)
    {
        if(level[ptmp]-(1<<i)>=level[qtmp])
        {
            ptmp=parentSparse[ptmp][i];
        }
    }
    if(ptmp==qtmp)
    {
        lca=ptmp;
    }
    else
    {
        for(i=16;i>=0;i--)
        {
            if(parentSparse[ptmp][i]!=-1 && parentSparse[qtmp][i]!=-1 && parentSparse[ptmp][i]!=parentSparse[qtmp][i])
            {
                ptmp=parentSparse[ptmp][i];
                qtmp=parentSparse[qtmp][i];
            }
        }
        lca=parentSparse[ptmp][0];
    }
    //cout<<"lca="<<lca<<"level="<<level[lca]<<" p="<<p<<" level="<<level[p]<<" q="<<q<<" level="<<level[q]<<endl;
    ll targetLevel;
    if(level[p]-level[lca]+1>=x)
    {
        targetLevel=level[p]-x+1;
        //cout<<"targetlevel1="<<targetLevel<<endl;
        for(i=16;i>=0;i--)
        {
            if(level[p]-(1<<i)>=targetLevel)
            {
                p=parentSparse[p][i];
            }
        }
        res=p;
    }
    else
    {
        targetLevel=x-level[p]+level[lca]-1;
        //cout<<"targetlevel2="<<targetLevel<<endl;
        for(i=16;i>=0;i--)
        {
            if(level[q]-(1<<i)>=targetLevel)
            {
                q=parentSparse[q][i];
            }
        }
        res=q;
    }
    return res;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll i,j,test,t,n,m,u,v,w;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        if(t>n) cout<<"\n";
        cin>>n;
        vector<vector<ll> >connection(n+1);
        vector<vector<ll> >cost(n+1);
        for(i=1;i<n;i++)
        {
            cin>>u>>v>>w;
            connection[u].push_back(v);
            connection[v].push_back(u);
            cost[u].push_back(w);
            cost[v].push_back(w);
        }
        vector<ll>col1(20,-1);
        vector<vector<ll> >parentSparse(n+1,col1);
        vector<ll>col2(20,0);
        vector<vector<ll> >disSparse(n+1,col2);
        vector<ll>level(n+1);
        lcaInit(n,connection,cost,disSparse,level,parentSparse);
//        for(i=1;i<=n;i++)
//        {
//            cout<<"for node "<<i<<endl;
//            for(j=0;j<=16;j++)
//            {
//                cout<<"("<<parentSparse[i][j]<<","<<disSparse[i][j]<<")";
//            }
//            cout<<endl;
//        }
        string s;
        while(cin>>s)
        {
            if(s=="DONE") break;
            ll res;
            if(s=="DIST")
            {
                ll p,q;
                cin>>p>>q;
                res=disQuery(p,q,disSparse,parentSparse,level);
            }
            else
            {
                ll p,q,x;
                cin>>p>>q>>x;
                res=posQuery(p,q,x,disSparse,parentSparse,level);
            }
            cout<<res<<"\n";
        }
    }
}
