#include<bits/stdc++.h>
using namespace std;

void preprocess(int n,vector<string>&matrix,vector<vector<int> >&sum)
{
    int i,j;
    for(i=0;i<n;i++)
    {
        for(j=0;j<matrix[i].size();j++){
            sum[i+1][j+1]=sum[i][j+1]+sum[i+1][j]-sum[i][j]+matrix[i][j]-'0';
            //cout<<"sum at ["<<i+1<<"]["<<j+1<<"]="<<sum[i+1][j+1]<<" ";
        }
        //cout<<endl;
    }
    return;
}

int solve(int n,vector<vector<int> >&sum)
{

    int i,j,k,m,mx=0;
    for(i=1;i<=n;i++){
        for(j=1;j<=n;j++)
        {
            for(k=i;k<=n;k++)
            {
                for(m=j;m<=n;m++)
                {
                    //cout<<"from "<<i<<" "<<j<<" to "<<k<<" "<<m<<" res=";
                    int rect1,rect2,total;
                    total=(k-i+1)*(m-j+1);
                    rect1=sum[k][m]-sum[i-1][m]-sum[k][j-1]+sum[i-1][j-1];
                    //cout<<rect1<<endl;
                    if(total==rect1)
                        mx=max(total,mx);
                }
            }
        }
    }
    return mx;
}

int Gettest(string &s)
{
    //cout<<"s="<<s<<endl;
    int m=1,j,total=0,i;
    for(i=s.size()-1;i>=0;i--)
    {
        total+=(s[i]-'0')*m;
        m*=10;
    }
    return total;
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    ios_base::sync_with_stdio(0);
    int test,i,j,t,n;
    string s,s1,s2;
    getline(cin,s2);
    test=Gettest(s2);
    //cout<<"test="<<test<<endl;
    for(t=1;t<=test;t++)
    {
        if(t>1) cout<<endl;
        getline(cin,s1);
        //cout<<"empty s="<<s1<<endl;
        cin>>s;
        //cout<<"non  empty s="<<s<<endl;
        n=s.size();
        vector<string>matrix;
        matrix.push_back(s);
        for(i=1;i<n;i++){
            cin>>s;
            matrix.push_back(s);
            //cout<<"s="<<s<<endl;
        }
        vector<int>col(100,0);
        vector<vector<int> >sum(100,col);
        preprocess(n,matrix,sum);
        int ans=solve(n,sum);
        cout<<ans<<endl;
    }
}
