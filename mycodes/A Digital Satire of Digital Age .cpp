#include<bits/stdc++.h>
using namespace std;

long long int weight(long long int n)
{
    char c=n;
    string s="";
    //cout<<c<<" "<<n<<endl;
    long long int bit=log2(n);
    long long int i,j,sum=0;
    for(i=0;i<=bit;i++)
    {
        if(((1<<i) & n)==0)
        {
            //s+="0";
            sum+=250;
        }
        else{
            //s+="1";
            sum+=500;
        }
    }
//    reverse(s.begin(),s.end());
//    cout<<s<<endl;
    return sum;
}


void down_left(string &left,string &right)
{
    while(right.size()<6)
    {
        right.push_back('.');
    }
    while(left.size()<6)
    {
        left.push_back('.');
    }
    //cout<<"a="<<left<<" b="<<right<<endl;
    cout<<"........||.../\\...\n";
    cout<<"........||../..\\..\n";
    cout<<".../\\...||./....\\.\n";
    cout<<"../..\\..||/"<<right<<"\\\n";
    cout<<"./....\\.||\\______/\n";
    cout<<"/"<<left<<"\\||........\n";
    cout<<"\\______/||........\n";
    //cout<<"------------------\n";
    return;
}
void up_left(string &left,string &right)
{
    while(right.size()<6)
    {
        right.push_back('.');
    }
    while(left.size()<6)
    {
        left.push_back('.');
    }
    cout<<".../\\...||........\n";
    cout<<"../..\\..||........\n";
    cout<<"./....\\.||.../\\...\n";
    cout<<"/"<<left<<"\\||../..\\..\n";
    cout<<"\\______/||./....\\.\n";
    cout<<"........||/"<<right<<"\\\n";
    cout<<"........||\\______/\n";
    //cout<<"------------------\n";
    return;
}

void equal_both(string &left,string &right)
{
    while(right.size()<6)
    {
        right.push_back('.');
    }
    while(left.size()<6)
    {
        left.push_back('.');
    }
    cout<<"........||........\n";
    cout<<".../\\...||.../\\...\n";
    cout<<"../..\\..||../..\\..\n";
    cout<<"./....\\.||./....\\.\n";
    cout<<"/"<<left<<"\\||/"<<right<<"\\\n";
    cout<<"\\______/||\\______/\n";
    cout<<"........||........\n";
    //cout<<"------------------\n";
    return;
}



int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
     long long int i,j,k,left,right,n;
    string s;
    vector<long long int>preprocess(130);
    for(i='A';i<='Z';i++)
    {
        preprocess[i]=weight(i);
    }
    cin>>n;
    for(i=1;i<=n;i++)
    {
        cout<<"Case "<<i<<":"<<endl;
        string s,l="",r="";
        int left_row=-1,right_row=-1,left_cost=0,right_cost=0,chk=0,chk1=0;
        for(j=1;j<=8;j++)
        {
            //cout<<"j="<<j<<endl;
            cin>>s;
            //cout<<"s="<<s<<endl;
            for(k=0;k<=7;k++)
            {
                if(s[k]!='.')
                {
                    if(!chk)
                    {
                        chk=1;
                        left_row=j;
//                        cout<<"update chk="<<chk<<endl;
//                        cout<<"found top of left k="<<k<<endl;
                    }
                }
                if(s[k]>='A' && s[k]<='Z')
                {
                    l.push_back(s[k]);
                    left_cost+=preprocess[s[k]];
                }
            }
            for(k=10;k<s.size();k++)
            {
                if(s[k]!='.')
                {
                    if(!chk1)
                    {
                        chk1=1;
                        //cout<<"update chk1="<<chk1<<endl;
                        right_row=j;
                        //cout<<"found top of right k="<<k<<endl;
                    }
                }
                if(s[k]>='A' && s[k]<='Z')
                {
                    r.push_back(s[k]);

                    right_cost+=preprocess[s[k]];
                }
            }
            //cout<<"left_row="<<left_row<<" cost="<<left_cost<<" right_row="<<right_row<<" cost="<<right_cost<<endl;
        }
        //cout<<"l="<<l<<" r="<<r<<endl;
        if(left_cost==right_cost)
        {
            if(left_row==right_row)
            {
                cout<<"The figure is correct."<<endl;
            }
            else
                equal_both(l,r);
        }
        else if(left_cost<right_cost)
        {
            if(left_row<right_row)
            {
                cout<<"The figure is correct."<<endl;
            }
            else
                up_left(l,r);
        }
        else
        {
            //cout<<"i should be here"<<endl;
            if(left_row>right_row)
            {
                cout<<"The figure is correct."<<endl;
            }
            else
                down_left(l,r);
        }

    }

}
