#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int test,t=1;
    scanf("%lld",&test);
    while(t<=test)
    {
        long long int a,b,c;
        scanf("%lld %lld %lld",&a,&b,&c);
        if((a+b>c)&&(a+c)>b&&(b+c)>a)
        {
            if(a==b && a==c)
            {
                printf("Case %lld: Equilateral\n",t);
            }
            else if(a==b || a==c || b==c)
            {
                printf("Case %lld: Isosceles\n",t);
            }
            else
                printf("Case %lld: Scalene\n",t);
        }
        else
        {
            printf("Case %lld: Invalid\n",t);
        }
        t++;
    }
}
