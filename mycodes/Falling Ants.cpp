#include<bits/stdc++.h>
using namespace std;

int main()
{
    int t,i,j,l,w,h,mx_h,mx_v;

    while(scanf("%d",&t))
    {
        if(t==0)
        {
            return 0;
        }
        mx_h=0;
        mx_v=0;
        for(i=1;i<=t;i++)
        {
            scanf("%d %d %d",&l,&w,&h);
            if(h>mx_h)
            {
                mx_h=h;
                mx_v=l*w*h;
            }
            else if(h==mx_h)
            {
                mx_v=max(mx_v,h*w*l);
            }
        }
        printf("%d\n",mx_v);
    }
}
