

#include <bits/stdc++.h>


using namespace std;

int main() {

    long long int c, t,n, test, i;

    cin >> test;
    for (i = 1; i <= test; ++i) {
        cin >> n >> c;
        double N = n;
        if (n == 0) {
            cout << "Case " << i << ": 0" << endl;
        }

        else
            cout << "Case " << i << ": " << ceil((c / 2) / N) << endl;
    }

    return (0);
}
