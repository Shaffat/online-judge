#include<bits/stdc++.h>

using namespace std;

vector<int>coins;
vector <vector<long long int> >memory;

long long int solve(int index, int total)
{
    //cout<<"index="<<index<<" total="<<total<<endl;
    if(total<0||index>=5)
    {
        return 0;
    }
    if(total==0)
    {
        return 1;
    }

    if(memory[index][total]!=-1)
    {

        return memory[index][total];
    }
    long long int count1,count2,i,j;

    count1=solve(index,total-coins[index]);
    count2=solve(index+1,total);
    //cout<<"memory["<<index<<"]["<<total<<"]="<<memory[index][total]<<endl;
    return memory[index][total]=count1+count2;

}
int main()
{
    //freopen("in.txt", "r", stdin);
    //freopen("out.txt", "w", stdout);
    vector<long long int>v(30001,-1);

    for(int i=0; i<=5; i++)
    {
        memory.push_back(v);
    }
    coins.push_back(1);
    coins.push_back(5);
    coins.push_back(10);
    coins.push_back(25);
    coins.push_back(50);

    long long  int k,res;
    while(scanf("%lld",&k)!=EOF)
    {


        res=solve(0,k);

        if(res>1)
        {
            printf("There are %lld ways to produce %lld cents change.\n",res,k);
        }
        else
        {
            printf("There is only 1 way to produce %lld cents change.\n",k);
        }
    }

}
