#include<bits/stdc++.h>
using namespace std;
int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}


bool bfs(vector<vector<int> >&connection,int n)
{
    int i,j,total=0;
    vector<bool>vis(n,0);
    vis[0]=1;
    total++;
    queue<int>q;
    q.push(0);
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            int nxt=connection[cur][i];
            if(!vis[nxt])
            {
                total++;
                q.push(nxt);
                vis[nxt]=1;
            }
        }
    }
    if(total==n)
    return true;
    else
        return false;

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,j,test,t,n;
    vector<int>col(51,0);
    vector<vector<int> >coprime(51,col);
    for(i=2;i<=50;i++)
    {
        for(j=2;j<=50;j++)
        {
            if(GCD(i,j)==1)
            {
                coprime[i][j]=1;

            }
        }
    }
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        if(t>1) printf("\n");
        scanf("%d",&n);
        vector<int>a;
        vector<int>b;
        int takeanother=0;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            a.push_back(j);
            b.push_back(j);
            if(j==47)
            {
                takeanother=1;
            }
        }
        vector<vector<int> >connection(n);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                if(coprime[a[i]][a[j]])
                {
                    connection[i].push_back(j);
                }
            }
        }
        if(bfs(connection,n))
        {
            printf("0\n");
        }
        else
        {
            printf("1\n");
            if(!takeanother)
            b[0]=47;
            else
                b[0]=43;
        }
        for(i=0;i<b.size();i++)
        {
            if(i==b.size()-1)
            {
                printf("%d\n",b[i]);
                break;
            }
            printf("%d ",b[i]);
        }

    }


}
