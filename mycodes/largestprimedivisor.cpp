#include<bits/stdc++.h>

using namespace std;

vector<bool>ar(40000001,0);
vector<long long int>primes;

void sieve()
{
    long long int i,j;
    ar[1]=1;
    long long int limit=sqrt(40000000);
    for(i=4;i<=limit;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=limit;i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<=40000000;j+=i)
            {
                ar[j]=1;
            }
        }
    }
    for(i=2;i<=40000000;i++)
    {
        if(ar[i]==0)
        primes.push_back(i);
    }
}

long long int solve(long long int n)
{
    long long int i,j,res=-1,range=sqrt(n),lastprime;
    int  chk=0;
    //cout<<range<<" n="<<n<<endl;

    for(i=0;i<primes.size();i++)
    {

        if(primes[i]>range)
        {
            break;
        }
        if(n%primes[i]==0)
        {
            //cout<<"here n="<<n<<endl;
            //lastprime=i;
            chk++;
        }
        while(n%primes[i]==0)
        {
            lastprime=primes[i];
            //cout<<"lastprime="<<lastprime<<endl;
            n/=primes[i];
            //cout<<"divided by prime="<<primes[i]<<" and n="<<n<<endl;
        }

    }
    if(chk>1)
    {
        res=max(n,lastprime);

        //cout<<res<<endl;
    }
    else if(chk==1&&n!=1)
    {
        res=max(n,primes[i]);
    }


    return res;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    sieve();
    //cout<<primes.size();
    long long int n;
    while(scanf("%lld",&n))
    {
        if(n==0)
        {
            break;
        }
        bool neg=0;
        if(n<0)
        {
            neg=1;
            n*=-1;
        }
        long long int res=solve(n);
        //cout<<res<<endl;


        printf("%lld\n",res);
    }
}
