#include<bits/stdc++.h>

using namespace std;

int main()
{
    string a,b;
    int i,j=1;
    cin>>a>>b;
    if(a.size()!=b.size())
    {
        cout<<"NO";
        return 0;
    }
    if(a==b)
    {
        cout<<"YES";
        return 0;
    }
    int allzero1=1,allzero2=1;
    for(i=0;i<a.size();i++)
    {
        if(a[i]!='0')
        {
            allzero1=0;
        }
    }
    for(i=0;i<b.size();i++)
    {
        if(b[i]!= '0')
        {
            allzero2=0;
        }
    }
    if(allzero1 || allzero2)
    {
        cout<<"NO";
    }
    else
        cout<<"YES";

}
