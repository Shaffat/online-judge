#include<bits/stdc++.h>
using namespace std;



void LPS_generator(string &pattern,vector<int>&lps)
{
    int i,j;
    lps[1]=0;
    cout<<"1 = 0"<<endl;
    for(i=2;i<pattern.size();i++)
    {
        if(pattern[lps[i-1]+1]==pattern[i])
        {
            lps[i]=lps[i-1]+1;
        }
        else
        {
            int done=0,cur=i-1;
            while(!done)
            {
                if(cur==0) {
                        lps[i]=0;
                        break;
                }
                if(pattern[lps[cur]+1]==pattern[i])
                {
                    done=1;
                    lps[i]=lps[cur]+1;
                }
                else
                {
                    cur=lps[cur];
                }
            }
        }
        cout<<i<<" "<<lps[i]<<endl;
    }
    return;
}


int main()
{
    string s;
    cin>>s;
    s.insert(s.begin()+0,' ');
    vector<int>lps(s.size()+1);
    LPS_generator(s,lps);

}
