#include<bits/stdc++.h>
using namespace std;



int main()
{
    long long n,k;
    while(cin>>n>>k)
    {


        vector<long long >divisors_front,divisors_last,merged;
        stack<long long >last;
        for(long long i=1;i<=sqrt(n);i++)
        {
            if(n%i==0)
            {
                divisors_front.push_back(i);
                if(n/i!=i)
                {
                    last.push(n/i);
                }
            }
        }
        for(long long i=0;i<divisors_front.size();i++)
        {
            merged.push_back(divisors_front.at(i));
        }
        while(!last.empty())
        {
            merged.push_back(last.top());
            last.pop();
        }


        if(merged.size()<k)
        {
            cout<<"-1"<<endl;
        }
        else
        {
            cout<<merged.at(k-1)<<endl;;
        }
    }
}
