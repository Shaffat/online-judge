#include<bits/stdc++.h>
using namespace std;
struct point
{
    int x,y;
};



int area(point a, point b,point c)
{
    return a.x*(b.y-c.y)+b.x*(c.y-a.y)+c.x*(a.y-b.y);
}



int testcase[(1<<17)];
int memory [(1<<17)];
int t;
int solve(int bitmask,vector<point>&dust)
{
    cout<<"bitmask="<<bitmask<<endl;
    if(bitmask==((1<<dust.size())-1))
    {
        return 0;
    }
    if(testcase[bitmask]==t){
        cout<<"memorization"<<endl;
        return memory[bitmask];
    }
    int i,j,cur,curmask,tmp,res=1e8;
    for(i=0;i<dust.size();i++)
    {
        if((bitmask&(1<<i))==0)
        {
            cur=i;
            break;
        }
    }
    bitmask|=(1<<cur);
    if(bitmask==((1<<dust.size())-1))
    {
        return 1;
    }
    for(i==cur+1;i<dust.size();i++)
    {
        if((bitmask&(1<<i))==0)
        {
            curmask=bitmask|(1<<i);
            for(j=cur+1;j<dust.size();j++)
            {
                if(area(dust[i],dust[j],dust[cur])==0)
                {
                    curmask|=(1<<j);
                }
            }
            tmp=solve(curmask,dust)+1;
            res=min(tmp,res);
        }
    }
    testcase[bitmask]=t;
    //cout<<"memory["<<bitmask<<"]="<<res<<endl;
    return memory[bitmask]=res;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,x,y,i,j;
        scanf("%d",&n);
        vector<point>dust;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&x,&y);
            point tmp;
            tmp.x=x;
            tmp.y=y;
            dust.push_back(tmp);
        }
        int res=solve(0,dust);
        printf("Case %d: %d\n",t,res);
    }
}

