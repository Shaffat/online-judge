#include<bits/stdc++.h>

using namespace std;

int main()
{
    int i,j,f_x,f_y,d_x,d_y,test,t,l,m,n;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        string s;
        f_x=0;
        f_y=0;
        cin>>m>>n>>d_x>>d_y>>l>>s;
        for(i=0;i<s.size();i++)
        {
            if(s[i]=='U')
                f_y++;
             if(s[i]=='D')
                f_y--;
            if(s[i]=='R')
                f_x++;
            if(s[i]=='L')
                f_x--;
        }
        //cout<<"x="<<f_x<<" y="<<f_y<<endl;
        if(f_x<0 || f_y<0 || f_x>m ||f_y>n)
            cout<<"Case "<<t<<": DANGER"<<endl;
        else if(f_x==d_x && f_y==d_y)
            cout<<"Case "<<t<<": REACHED"<<endl;
        else
            cout<<"Case "<<t<<": SOMEWHERE"<<endl;

    }

}
