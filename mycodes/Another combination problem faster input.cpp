#include<bits/stdc++.h>


using namespace std;


typedef unsigned long long int llu;
llu  mod=1e9+7;



int main()
{
    ios_base:: sync_with_stdio(0);cin.tie(0);
    int test,t=1;
    //cout<<mod<<endl;
    cin>>test;
    while(t<=test)
    {
        int n;
        cin>>n;
        llu a=n;
        llu b=n+1;
        llu c=(2*n)+1;
        if(a%6==0)
        {
            a/=6;
        }
        else if(b%6==0)
        {
            b/=6;
        }
        else if(c%6==0)
        {
            c/=6;
        }
        else if(a%2==0)
        {
            a/=2;
            if(b%3==0)
            {
                b/=3;
            }
            else c/=3;
        }
        else if(a%3==0)
        {
            a/=3;
            if(b%2==0)
            {
                b/=2;
            }
            else
                c/=2;
        }
        else
        {
            if(b%2==0)
            {
                b/=2;
                c/=3;
            }
            else
            {
                b/=3;
                c/=2;
            }
        }
        llu d=n,e=n+1;
        if(d%2==0)
        {
            d/=2;
        }
        else
            e/=2;
        llu x=(((a*b)%mod)*(c%mod))%mod;
        llu y=(d*e)%mod;
        llu result=(x+y)%mod;
        printf("Case %d: %llu\n",t,result);
        t++;
    }
}

