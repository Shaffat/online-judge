#include<bits/stdc++.h>
using namespace std;
struct cmp_and_AP
{
    int node,ap;
};

int t,timer;
vector<int>start(10000);
vector<int>low(10000);
vector<int>vis(10000,0);
vector<int>vis_cmp(10000,0);

void find_AP(int node,int root,int parent,vector<vector<int> >&connection,vector<int>&AP)
{
    int i,j,ap_chk=0,child,real=0;
    start[node]=low[node]=timer;
    timer++;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(child==parent)
        {
            continue;
        }
        if(vis[child]!=t)
        {
            real++;
            vis[child]=t;
            find_AP(child,root,node,connection,AP);
            if(start[node]<=low[child] && node!=root)
            {
                ap_chk=1;
            }
            low[node]=min(low[node],low[child]);
        }
        else
        {
            low[node]=min(low[node],start[child]);
        }
    }
    if(ap_chk)
    {
        AP[node]=1;
    }
    if(node==root)
    {
        if(real>1)
        {
            AP[node]=1;
        }
    }
    return;
}

cmp_and_AP solve(int node,vector<vector<int> >&connection,vector<int>&AP,vector<int>&visAP)
{
    int i,j,child;
    cmp_and_AP total;
    total.node=1;
    total.ap=0;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(AP[child]==1)
        {
            if(visAP[child]==0)
            {
                visAP[child]=1;
                total.ap++;
            }
        }
        else if(vis_cmp[child]!=t)
        {
            vis_cmp[child]=t;
            cmp_and_AP tmp=solve(child,connection,AP,visAP);
            total.ap+=tmp.ap;
            total.node+=tmp.node;
        }

    }
    return total;
}

int main()
{
    int test;
    t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        timer=1;
        int n,m,i,j,u,v,shafts=0,ap_chk=0;
        unsigned long long int res=1;
        scanf("%d %d",&n,&m);
        vector<vector<int> >connection(n+1);
        vector<int>AP(n+1,0);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        for(i=0;i<n;i++)
        {
            if(vis[i]!=t)
            {
                vis[i]=t;
                find_AP(i,i,i,connection,AP);
            }
        }
        for(i=0;i<n;i++)
        {
            if(AP[i]==1)
            {
                ap_chk=1;
            }
            if(AP[i]!=1&&vis_cmp[i]!=t)
            {
                vis_cmp[i]=t;
                vector<int>vis_ap(n,0);
                cmp_and_AP tmp=solve(i,connection,AP,vis_ap);
                //cout<<"i="<<i<<" ap connected "<<tmp.ap<<" nodes="<<tmp.node<<endl;
                if(tmp.ap==1)
                {
                    shafts++;
                    unsigned long long int k=tmp.node;
                    res*=k;
                }
            }
        }
        if(ap_chk)
        {
            printf("Case %d: %d %llu\n",t,shafts,res);
        }
        else
        {
            shafts=2;
            n--;
            res=(n*(n+1))/2;
            printf("Case %d: %d %llu\n",t,shafts,res);
        }
        t++;
    }
}
