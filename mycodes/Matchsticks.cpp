
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int main()
{
    ll i,j,n,test;
    map<int,int>mp;
    mp[2]=1;
    mp[3]=7;
    mp[4]=4;
    mp[5]=2;
    mp[6]=6;
    mp[7]=8;
    mp[8]=10;
    mp[9]=18;
    mp[10]=22;
    mp[11]=20;
    mp[12]=28;
    mp[13]=80;

    cin>>test;
    while(test--)
    {
        cin>>n;
        ll n1=n;
        if(n>14)
        {
            ll x=n%7+7;
            if(x==12)
            {
                cout<<"206";
                n-=7+x;
            }
            else
            {
                cout<<mp[x];
                n-=x;
            }
            //cout<<"x="<<x<<" n="<<n<<endl;
            while(n>0)
            {
                cout<<"8";
                n-=7;
            }
        }
        else
            cout<<mp[n];
        cout<<" ";
        if(n1%2)
        {
            cout<<"7";
            n1-=3;
        }
        while(n1>0)
        {
            cout<<"1";
            n1-=2;
        }
        cout<<endl;

    }
}
