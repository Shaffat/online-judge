#include<bits/stdc++.h>
using namespace std;

bool solve(int index,int counter,int need,vector<int>&data)
{
    //cout<<"i am at index="<<index<<"  counter="<<counter<<" need="<<need<<endl;
    if(index>=data.size() || counter>3 || need<0)
    {
        return 0;
    }
    if(need==0 && counter==3)
    {
        return 1;
    }
    bool res1,res2;
    res1=solve(index+1,counter+1,need-data[index],data);
    //cout<<"res1="<<res1<<endl;
    res2=solve(index+1,counter,need,data);
    //\cout<<"res2="<<res2<<endl;
    return res1|res2;

}

int main()
{
    int total=0,i,j;
    vector<int>data;
    for(i=1;i<=6;i++)
    {
        scanf("%d",&j);
        total+=j;
        data.push_back(j);
    }
    if(total%2==0 && solve(0,0,total/2,data))
    {
        printf("YES\n");
    }
    else
        printf("NO\n");
}
