#include<bits/stdc++.h>
using namespace std;

struct number
{
    int n,d;
};

bool operator <(number a,number b)
{
    if(a.d!=b.d)
    {
        return a.d<b.d;
    }
    return a.n>b.n;
}

int divisorcounter(int n)
{
    int i,j,cnt=0;
    for(i=1;i<=sqrt(n);i++)
    {
        if(n%i==0)
        {
            cnt++;
            if(n/i!=i)
            {
                cnt++;
            }
        }
    }
    return cnt;
}

int main()
{
    int i,j,test,t=1;
    vector<number>v;
    for(i=1;i<=1000;i++)
    {
        number tmp;
        tmp.n=i;
        tmp.d=divisorcounter(i);
        //cout<<"for i="<<i<<" d="<<tmp.d<<endl;
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d",&i);
        printf("Case %d: %d\n",t,v[i-1].n);
        t++;
    }
}
