#include<bits/stdc++.h>
using namespace std;

struct digit
{
    int num,pos;
};
bool operator <(digit a,digit b)
{
    if(a.num!=b.num)
    {
        return a.num<b.num;
    }
    return a.pos<b.pos;
}
int main()
{
    int n,i,j,k;
    scanf("%d",&n);
    vector<digit>v;
    for(i=0;i<n;i++)
    {
        digit tmp;
        scanf("%d",&j);
        tmp.num=j;
        tmp.pos=i;
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    int mn=v[0].num;
    int dis=2e9;
    for(i=1;i<n;i++)
    {
        if(v[i].num==v[i-1].num)
        {
            dis=min(dis,v[i].pos-v[i-1].pos);
        }
        else
            break;
    }
    cout<<dis<<endl;


}
