#include<bits/stdc++.h>

using namespace std;

int ar[10000010];

void sieve(int N,int *ar)

{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}



int main()
{
    int n,i;
    sieve(10000000,ar);
    while(cin>>n)
    {
        if(n<8)
        {
            cout<<"Impossible."<<endl;
        }
        else
        {
            if(n%2==0)
            {
                int k=n-4;
                for(i=2;i<=k;i++)
                    {

                        if(ar[i]==0 && ar[k-i]==0)
                            {
                                break;
                            }
                    }
                    cout<<"2 "<<"2 "<<i<<" "<<k-i<<endl;
            }

            else
            {
                int k=n-5;
                for(i=2;i<=k;i++)
                    {

                        if(ar[i]==0 && ar[k-i]==0)
                            {
                                break;
                            }
                    }
                    cout<<"2 "<<"3 "<<i<<" "<<k-i<<endl;
            }

        }
    }
}
