#include<bits/stdc++.h>
using namespace std;
double pi=acos(-1.0);
struct point
{
    int x,y;
};
bool valid_box(int r,int c,int n,int m)
{
    if(r<0 || r>=n || c<0 || c>=m)
    {
        return false;
    }
    return true;
}

double get_area(int dir_x,int dir_y,int mark)
{
    if(dir_x==1&&dir_y==1)
    {
        if(mark==1)
        {
            return 1-(pi/4);
        }
        if(mark==2)
        {
            return pi/4;
        }
        if(mark==3)
        {
            return 0;
        }
        if(mark==4)
        {
            return 0;
        }
    }
    if(dir_x==-1&&dir_y==1)
    {
        if(mark==1)
        {
            return 0;
        }
        if(mark==2)
        {
            return 0;
        }
        if(mark==3)
        {
            return pi/4;
        }
        if(mark==4)
        {
            return 1-(pi/4);
        }
    }
    if(dir_x==-1&&dir_y==-1)
    {
        if(mark==1)
        {
            return pi/4;
        }
        if(mark==2)
        {
            return 1-(pi/4);
        }
        if(mark==3)
        {
            return 0;
        }
        if(mark==4)
        {
            return 0;
        }
    }
    if(dir_x==1&&dir_y==-1)
    {
        if(mark==1)
        {
            return 0;
        }
        if(mark==2)
        {
            return 0;
        }
        if(mark==3)
        {
            return 1-(pi/4);
        }
        if(mark==4)
        {
            return pi/4;
        }
    }
}


void solve(int n,int m,vector<vector<int> >&valid_points,vector<vector<int> >&graph,vector<vector<double> >&dis)
{
    vector<point>col(m+10);
    vector<vector<point> >parent(n+10,col);
    vector<int>col1(m+10,0);
    vector<vector<int> >vis(n+10,col1);
    vector<vector<int> >vis_point(n+10,col1);
    int i,j;
    for(i=0;i<=n;i++)
    {
        for(j=0;j<=m;j++)
        {
            if(valid_points[i][j]==1 && vis_point[i][j]==0)
            {
                point root;
                root.x=i;
                root.y=j;
                parent[i][j]=root;
                queue<point>q;
                q.push(root);
                while(!q.empty())
                {
                    point cur=q.front();
                    q.pop();
                    int box_r,box_c;
                    box_r=cur.x-1;
                    box_c=cur.y;
                    if(valid_box(box_r,box_c,n,m))
                    {
                        if(vis[box_r][box_c]==0)
                        {
                            if(graph[box_r][box_c]==0)
                            {
                                vis[box_r][box_c]=1;
                                dis[root.x][root.y]+=1;
                                point nxt;
                                nxt.x=cur.x-1;
                                nxt.y=cur.y+1;
                                if(vis_point[nxt.x][nxt.y]==0)
                                {
                                    parent[nxt.x][nxt.y]=root;
                                    vis_point[nxt.x][nxt.y]=1;
                                    q.push(nxt);
                                }
                            }
                            else
                            {
                                dis[root.x][root.y]+=get_area(1,1,graph[box_r][box_c]);
                            }
                        }
                    }
                    box_r=cur.x-1;
                    box_c=cur.y-1;
                    if(valid_box(box_r,box_c,n,m))
                    {
                        if(vis[box_r][box_c]==0)
                        {
                            if(graph[box_r][box_c]==0)
                            {
                                vis[box_r][box_c]=1;
                                dis[root.x][root.y]+=1;
                                point nxt;
                                nxt.x=cur.x-1;
                                nxt.y=cur.y-1;
                                if(vis_point[nxt.x][nxt.y]==0)
                                {
                                    parent[nxt.x][nxt.y]=root;
                                    vis_point[nxt.x][nxt.y]=1;
                                    q.push(nxt);
                                }
                            }
                            else
                            {
                                dis[root.x][root.y]+=get_area(-1,1,graph[box_r][box_c]);
                            }
                        }
                    }
                    box_r=cur.x;
                    box_c=cur.y;
                    if(valid_box(box_r,box_c,n,m))
                    {
                        if(vis[box_r][box_c]==0)
                        {
                            if(graph[box_r][box_c]==0)
                            {
                                vis[box_r][box_c]=1;
                                dis[root.x][root.y]+=1;
                                point nxt;
                                nxt.x=cur.x+1;
                                nxt.y=cur.y+1;
                                if(vis_point[nxt.x][nxt.y]==0)
                                {
                                    parent[nxt.x][nxt.y]=root;
                                    vis_point[nxt.x][nxt.y]=1;
                                    q.push(nxt);
                                }
                            }
                            else
                            {
                                dis[root.x][root.y]+=get_area(1,-1,graph[box_r][box_c]);
                            }
                        }
                    }
                    box_r=cur.x;
                    box_c=cur.y-1;
                    if(valid_box(box_r,box_c,n,m))
                    {
                        if(vis[box_r][box_c]==0)
                        {
                            if(graph[box_r][box_c]==0)
                            {
                                vis[box_r][box_c]=1;
                                dis[root.x][root.y]+=1;
                                point nxt;
                                nxt.x=cur.x+1;
                                nxt.y=cur.y-1;
                                if(vis_point[nxt.x][nxt.y]==0)
                                {
                                    parent[nxt.x][nxt.y]=root;
                                    vis_point[nxt.x][nxt.y]=1;
                                    q.push(nxt);
                                }
                            }
                            else
                            {
                                dis[root.x][root.y]+=get_area(-1,-1,graph[box_r][box_c]);
                            }
                        }
                    }
                }
            }
        }
    }
    for(i=0;i<=n;i++)
    {
        for(j=0;j<=m;j++)
        {
            if(valid_points[i][j]==0)
            {
                dis[i][j]=0;
            }
            else
            {
                point p=parent[i][j];
                dis[i][j]=dis[p.x][p.y];
            }
        }
    }
    return;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int r,c,i,j,n,m,q;
        scanf("%d %d",&r,&c);
        n=2*r;
        m=2*c;
        vector<int>col(m+10,0);
        vector<int>col2(m+10,1);
        vector<vector<int> >graph(n+10,col);
        vector<vector<int> >valid_point(n+10,col2);
        vector<double>col1(m+10,0);
        vector<vector<double> >dis(n+10,col1);
        for(i=0;i<r;i++)
        {
            string s;
            cin>>s;
            for(j=0;j<s.size();j++)
            {
                int motif=s[j]-'0',start_r=2*i,start_c=2*j;
                if(motif==1)
                {
                    graph[start_r][start_c+1]=1;
                    graph[start_r+1][start_c]=2;
                    valid_point[start_r][start_c+1]=0;
                    valid_point[start_r+1][start_c+2]=0;
                    valid_point[start_r+1][start_c]=0;
                    valid_point[start_r+2][start_c+1]=0;
                }
                else
                {
                    graph[start_r][start_c]=4;
                    graph[start_r+1][start_c+1]=3;
                    valid_point[start_r][start_c+1]=0;
                    valid_point[start_r+1][start_c]=0;
                    valid_point[start_r+1][start_c+2]=0;
                    valid_point[start_r+2][start_c+1]=0;
                }
            }
        }

        solve(n,m,valid_point,graph,dis);

        scanf("%d",&q);
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            int x,y;
            scanf("%d %d",&x,&y);
            printf("%.8lf\n",dis[x][y]);
        }
        t++;

    }
}
