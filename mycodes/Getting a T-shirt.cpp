#include<bits/stdc++.h>
using namespace std;


struct node
{
    int id,flow;
};
int send_flow(int source,int sink,int n,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    int i,j,res=0;
    node cur;
    vector<int>path(n+1,-1);
    cur.id=source;
    cur.flow=1e9;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        //cout<<"node="<<cur.id<<" flow="<<cur.flow<<endl;
        q.pop();
        if(cur.id==sink)
        {
            res=cur.flow;
            break;
        }
        for(i=0;i<graph[cur.id].size();i++)
        {
            int nxt=graph[cur.id][i];
            if(path[nxt]==-1 && capacity[cur.id][nxt]>0)
            {
                path[nxt]=cur.id;
                node tmp;
                tmp.id=nxt;
                tmp.flow=min(cur.flow,capacity[cur.id][nxt]);
                q.push(tmp);
            }
        }

    }
    //cout<<"res="<<res<<endl;
    if(res>0)
    {
        int child,parent;
        child=sink;
        while(child!=source)
        {
            parent=path[child];
            capacity[parent][child]-=res;
            capacity[child][parent]+=res;
            child=parent;
        }
    }
    //cout<<"done\n";
    return res;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,n,m,i,j;
    map<string,int>mp;
    mp["XXL"]=1;
    mp["XL"]=2;
    mp["L"]=3;
    mp["M"]=4;
    mp["S"]=5;
    mp["XS"]=6;

    cin>>test;

    for(t=1;t<=test;t++){

        cin>>n>>m;
        vector<vector<int> >graph(10+m);
        vector<int>col(10+m,0);
        vector<vector<int> >capacity(10+m,col);
        for(i=1;i<=6;i++)
        {
            graph[0].push_back(i);
            graph[i].push_back(0);
            capacity[0][i]=n;
            capacity[i][0]=0;
        }
        for(i=7;i<7+m;i++)
        {
            string s1,s2;
            cin>>s1>>s2;
            graph[mp[s1]].push_back(i);
            graph[i].push_back(mp[s1]);
            capacity[mp[s1]][i]=1;
            capacity[i][mp[s1]]=0;
            graph[mp[s2]].push_back(i);
            graph[i].push_back(mp[s2]);
            capacity[mp[s2]][i]=1;
            capacity[i][mp[s2]]=0;
        }
        for(i=7;i<7+m;i++)
        {
            graph[i].push_back(7+m);
            capacity[i][7+m]=1;
        }
        int mxflow=0,flow=0;
        while(flow=send_flow(0,7+m,7+m,graph,capacity))
        {
            mxflow+=flow;
        }
        if(mxflow==m)
            cout<<"Case "<<t<<": YES\n";
        else
            cout<<"Case "<<t<<": NO\n";

    }

}
