#include<bits/stdc++.h>

using namespace std;

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int i,j,k,n;
        vector<int>stripes;
        scanf("%d",&n);
        for(i=0;i<n;i++)
        {
            scanf("%d",&k);
            stripes.push_back(k);
        }
        if(n%2==0||(stripes[n/2]!=((n/2)+1)))
        {
            //cout<<stripes[n/2]<<" and "<<(n/2)+1<<endl;
            printf("no\n");

        }
        else
        {
            int chk=1,fixed=stripes[n/2],gap=1;
            for(i=1;i<=n/2;i++)
            {
                if((fixed-stripes[(n/2)+i])!=gap || (fixed-stripes[(n/2)-i])!=gap)
                {
                    //cout<<fixed<<" and left="<<stripes[(n/2)-i]<<" and right="<<stripes[(n/2)+i]<<endl;
                    chk=0;
                    break;
                }
                gap++;
            }
            if(!chk)
            {
                printf("no\n");
            }
            else
            {
                printf("yes\n");
            }
        }
    }
}
