#include<stdio.h>
#include<vector>
#include<math.h>
#include<algorithm>

using namespace std;

struct nodes
{
    int u,v,w;
};

bool operator <(nodes x,nodes y)
{
    if(x.w!=y.w)
    {
        return x.w<y.w;
    }
    return false;
}

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int n,i,j,mx=0;
        scanf("%d",&n);
        vector<nodes>connection;
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=n;j++)
            {
                int c;
                scanf("%d",&c);
                nodes temp;
                temp.u=i;
                temp.v=j;
                temp.w=c;
                connection.push_back(temp);
            }
        }
        vector<int>parent(n+1);
        vector<int>ranked(n+1,0);
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }
        sort(connection.begin(),connection.end());
        int l=0;
        for(i=0;i<connection.size();i++)
        {
            int u,v,c;
            u=connection[i].u;
            v=connection[i].v;
            c=connection[i].w;
            //cout<<"u="<<u<<"v="<<v<<"c="<<c<<endl;
            if(parent[u]!=parent[v])
            {
                mx=max(mx,c);
                if(ranked[u]>=ranked[v])
                {
                    parent[v]=u;

                }
                else if(ranked[u]>ranked[v])
                {
                    parent[v]=u;
                }
                else
                    parent[u]=v;

                l++;

                if(l==n-1)
                {
                    break;
                }
            }
        }
        printf("%d\n",mx);
    }

}
