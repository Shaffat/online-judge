#include<bits/stdc++.h>
using namespace std;

#define ll long long int
ll calculate_mood(ll start,ll ending,vector<ll>path,vector<vector<ll> >&cost)
{
    cout<<"cycle from "<<start<<" to "<<ending<<endl;
    ll s,nxt,total=cost[ending][start],edge=1;
    nxt=ending;
    s=path[ending];
    while(nxt!=start)
    {
        total+=cost[s][nxt];
        nxt=s;
        s=path[s];
        edge++;
    }
    if(total>0)
    {
        return edge;
    }
    return 2e9;
}

ll all_cycle(ll node,vector<ll>path,vector<vector<ll> >&connection,vector<vector<ll> >&cost,vector<ll>&vis)
{
    ll i,j,mx=-1,tmp;
    for(i=0;i<connection[node].size();i++)
    {
        ll nxt=connection[node][i];
        if(vis[nxt]==0)
        {
            vis[nxt]=1;
            path[nxt]=node;
            mx=min(all_cycle(nxt,path,connection,cost,vis),mx);
        }
        else if(vis[nxt]==1)
        {
            tmp=calculate_mood(nxt,node,path,cost);
            mx=min(tmp,mx);
        }
    }
    vis[node]=2;
    return mx;

}
int main()
{
    vector<ll>col(301);
    vector<vector<ll> >cost(301,col);
    ll n,i,j,u,v,m,c1,c2,res=2e9;
    scanf("%lld %lld",&n,&m);
    vector<vector<ll> >connection(n+1);
    for(i=1;i<=m;i++)
    {
        scanf("%lld %lld %lld %lld",&u,&v,&c1,&c2);
        connection[u].push_back(v);
        connection[v].push_back(u);
        cost[u][v]=c1;
        cost[v][u]=c2;
    }
    vector<ll>vis(n+1,0);
    vector<ll>path(n+1,-1);
    for(i=1;i<=n;i++)
    {
        if(!vis[i])
        {
            vis[i]=1;
            res=min(res,all_cycle(i,path,connection,cost,vis));
        }
    }
    if(res==2e9)
    {
        res=0;
    }
    printf("%lld\n",res);
}
