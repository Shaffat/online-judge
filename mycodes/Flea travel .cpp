#include<bits/stdc++.h>

using namespace std;

int main()
{
    int i,j,n,counter=0;
    scanf("%d",&n);
    queue<int>q;
    for(i=1;i<=n;i++)
    {
        q.push(i);
    }
    bool chk=0;
    vector<bool>vis(n+1,0);
    for(i=1;i<=2*n;i++)
    {
        int moves=i-1;
        for(j=1;j<=moves;j++)
        {
            int tmp=q.front();
            q.pop();
            q.push(tmp);
        }
        if(vis[q.front()]==0)
        {
            vis[q.front()]=1;
            counter++;
        }
        if(counter==n)
        {
            chk=1;
            break;
        }
    }
    if(chk)
    {
        cout<<"YES"<<endl;
    }
    else
        cout<<"NO"<<endl;
}
