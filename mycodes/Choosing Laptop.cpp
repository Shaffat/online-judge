#include<bits/stdc++.h>
using namespace std;

struct detail
{
    int speed,ram,hdd,cost,index;
};

bool operator <(detail a,detail b)
{
  if(a.speed<b.speed && a.ram<b.ram && a.hdd<b.hdd)
  {
      return true;
  }
  else if(a.speed>b.speed && a.ram>b.ram && a.hdd>b.hdd)
  {
      return false;
  }
  else
    return a.cost>b.cost;
}

int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<detail>pc;
    for(i=1;i<=n;i++)
    {
        detail tmp;
        scanf("%d %d %d %d",&tmp.speed,&tmp.ram,&tmp.hdd,&tmp.cost);
        tmp.index=i;
        pc.push_back(tmp);
    }
    sort(pc.begin(),pc.end());
    for(i=0;i<n;i++)
    {
        cout<<"i="<<pc[i].index<<" speed="<<pc[i].speed<<" ram="<<pc[i].ram<<" hdd="<<pc[i].hdd<<" cost="<<pc[i].cost<<endl;
    }
    cout<<pc[0].index<<endl;
}
