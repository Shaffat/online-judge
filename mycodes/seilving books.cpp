#include<bits/stdc++.h>
using namespace std;

vector<int>col1(102,0);
vector<vector<int> >col2(102,col1);
vector<vector<vector<int> > >memory(102,col2);
vector<vector<vector<int> > >testcase(102,col2);
int t;
int solve(int pos,int left,int right,vector<int>&books)
{
    if(pos>=books.size()-1)
    {
        return 0;
    }
    if(testcase[pos][left][right]==t)
    {
        return memory[pos][left][right];
    }
    int case1=0,case2=0,case3=0,res;
    if(books[pos]>=books[left] && books[pos]<=books[right])
    {
        case1=solve(pos+1,pos,right,books)+1;
        case2=solve(pos+1,left,pos,books)+1;
    }
    case3=solve(pos+1,left,right,books);
    res=max(case1,max(case2,case3));
    testcase[pos][left][right]=t;
    return memory[pos][left][right]=res;
}

int main()
{
    int test;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int i,j,n,res;
        vector<int>books;
        books.push_back(-1);
        scanf("%d",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            books.push_back(j);
        }
        books.push_back(1e6);
        res=solve(1,0,books.size()-1,books);
        printf("Case %d: %d\n",t,res);
    }
}
