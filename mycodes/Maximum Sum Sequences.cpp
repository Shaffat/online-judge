#include<bits/stdc++.h>
using namespace std;

struct res
{
    int counter;
    int mx;
};


res solve(vector<int>&v)
{
    res ans;
    ans.counter=0;
    ans.mx=-1e9;
    int i,j;
    int cur=-1e9;
    for(i=0;i<v.size();i++)
    {
       int before=cur;
       cur=max(cur+v[i],v[i]);

       if(cur>ans.mx){
            ans.mx=cur;
            ans.counter=1;
            if(before+v[i]==cur){
                ans.counter++;
            }
       }
       else if(cur==ans.mx){
            ans.counter++;
             if(before+v[i]==cur){
                ans.counter++;
            }
       }

    }
    return ans;
}

int main()
{
    int n,i,j,test;
    scanf("%d",&test);
    while(test--){
        vector<int>v;
        scanf("%d",&n);
        for(i=1;i<=n;i++){
            scanf("%d",&j);
            v.push_back(j);
        }
        res ans=solve(v);
        printf("%d %d\n",ans.mx,ans.counter);
    }
}
