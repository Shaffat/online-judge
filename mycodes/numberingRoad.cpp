

#include <bits/stdc++.h>

int main() {
    bool flag;
    int r, n, roads, i,t = 1;
    while(scanf("%d %d", &r, &n)) {
        if (r == 0 && n == 0) {
            break;
        }
        printf("Case %d: ", t++);
        r -= n;
        if (r <= 0) {
            printf("0\n");
            continue;
        }
        flag = false;
        for ( i = 1; i < 27; ++i) {
            r -= n;
            if (r <= 0) {
                flag = true;
                break;
            }
        }
        if (flag) {
            printf("%d\n", i);
        }
        else {
            printf("impossible\n");
        }

    }

    return (0);
}
