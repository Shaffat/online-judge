#include<bits/stdc++.h>
using namespace std;

int main()
{
    double a,b,c;
    while(cin>>a>>b>>c)
    {
        double s=(a+b+c)/2;
        double triangle_area=sqrt(s*(s-a)*(s-b)*(s-c));
        double inner_rad=triangle_area/s;
        double outer_rad=(a*b*c)/(4*triangle_area);
        double outer=( 3.141592653589793*outer_rad*outer_rad);
        double inner=( 3.141592653589793*inner_rad*inner_rad);
        double yellow,red,purple;
        yellow=outer-triangle_area;
        purple=triangle_area-inner;
        printf("%.4lf %.4lf %.4lf\n",yellow,purple,inner);
    }
}
