#include<iostream>
#include<vector>
#include<map>
#include<string>
#include<math.h>
#include<fstream>
using namespace std;

struct tank{
    int id,r,c,angle,walk,exist;
    string name;
};

struct bullet{
    int r,c,angle,exist;
};

struct cmd
{
    string s;
    int angle;
};

bool valid(int r,int c){
    if(r>=0 && r<=12 && c>=0 && c<=12) return true;
    return false;
}

int n,m;
void elimination(vector<tank>&tanks,vector<bullet>&shots){
  //cout<<"elimination"<<endl;
    int i,j;
    for(i=1;i<=n;i++){
        int dead=0;
        for(j=0;j<shots.size();j++){
            if(shots[j].exist==1 && shots[j].r==tanks[i].r && shots[j].c==tanks[i].c && tanks[i].exist==1){
                shots[j].exist=0;
                dead=1;
            }
        }
        if(dead){
            tanks[i].exist=0;
        }
    }
    return;
}

void runCommands(vector<vector<vector<cmd> > >&commands,int t,vector<tank>&tanks,vector<bullet>&shots){
    //cout<<"Run Commands"<<endl;
    int i,j,angle;
    string s;
    for(i=1;i<=n;i++){
        for(j=0;j<commands[t][i].size();j++){
            s=commands[t][i][j].s;
            angle=commands[t][i][j].angle;
            if(s=="MOVE"){
                tanks[i].walk=1;
            }
            else if(s=="STOP"){
                tanks[i].walk=0;
            }
            else if(s=="SHOOT"){
                bullet tmp;
                tmp.r=tanks[i].r;
                tmp.c=tanks[i].c;
                tmp.angle=tanks[i].angle;
                tmp.exist=1;
                shots.push_back(tmp);
            }
            else if(s=="TURN"){
                tanks[i].angle=(tanks[i].angle + angle+360)%360;
            }
        }
    }
}

void moveTanks(vector<tank>&tanks){
    //cout<<"moveTanks"<<endl;
    int i,j;
    for(i=1;i<=n;i++){
        //cout<<"tanks x="<<tanks[i].r<<" y="<<tanks[i].c<<" angle="<<tanks[i].angle<<" move="<<tanks[i].walk<<endl;
        if(tanks[i].exist==1 && tanks[i].walk==1){
            if(tanks[i].angle==0){
                if(tanks[i].r+1<=12*6){
                    tanks[i].r++;
                }
            }
             if(tanks[i].angle==180){
                if(tanks[i].r-1>=0){
                    tanks[i].r--;
                }
            }
             if(tanks[i].angle==90){
                if(tanks[i].c+1<=6*12){
                    tanks[i].c++;
                }
            }
             if(tanks[i].angle==270){
                if(tanks[i].c-1>=0){
                    tanks[i].c--;
                }
            }
        }
    }
    return;
}

void moveShots(vector<bullet>&shots){
    //cout<<"moveShots"<<endl;
    int i,j;
    for(i=0;i<shots.size();i++){
            //cout<<"x="<<shots[i].r<<" y="<<shots[i].c<<" angle="<<shots[i].angle<<" exist="<<shots[i].exist<<endl;
        if(shots[i].exist==1){
            if(shots[i].angle==0){
                if(shots[i].r+2<=12*6){
                    shots[i].r+=2;
                }
                else
                    shots[i].exist=0;
            }
            else if(shots[i].angle==180){
                if(shots[i].r-2>=0){
                    shots[i].r-=2;
                }
                else
                    shots[i].exist=0;
            }
            else if(shots[i].angle==90){
                if(shots[i].c+2<=6*12){
                    shots[i].c+=2;
                }
                else
                    shots[i].exist=0;
            }
            else if(shots[i].angle==270){
                if(shots[i].c-2>=0){
                    shots[i].c-=2;
                }
                else
                    shots[i].exist=0;
            }
        }
    }
}

void solve(vector<tank>&tanks,vector<vector<vector<cmd> > >&commands){
//cout<<"solve"<<endl;
  int t,cnt=0,i,j;
  string s;
  vector<bullet>shots;
  for(t=0;t<=360;t++){
    //cout<<"time ="<<t<<endl;
    runCommands(commands,t,tanks,shots);
    moveTanks(tanks);
    moveShots(shots);
    elimination(tanks,shots);
  }
   for(i=1;i<=n;i++){
    if(tanks[i].exist==1){
        cnt++;
        s=tanks[i].name;
    }
   }
   if(cnt!=1){
    cout<<"NO WINNER!"<<endl;
   }
   else
    cout<<s<<endl;
}

int main(){
//    freopen("in.txt","r",stdin);
//        freopen("out.txt","w",stdout);
int i,j,k,l,m,a,t;
string s;
while(cin>>n>>m){
        if(n==0 && m==0) break;
    vector<tank>tanks(n+1);
    vector<cmd>col2;
    vector<vector<cmd> >col(30,col2);
    vector<vector<vector<cmd> > >commands(370,col);
    map<string,int>mp;
    for(i=1;i<=n;i++){
        tank tmp;
        tmp.id=i;
        cin>>tmp.name>>tmp.r>>tmp.c>>tmp.angle;
        tmp.r/=10;
        tmp.r*=6;
        tmp.c/=10;
        tmp.c*=6;
        tmp.exist=1;
        tmp.walk=0;
        tanks[i]=tmp;
        mp[tmp.name]=tmp.id;
    }
    for(i=1;i<=m;i++){
        cmd tmp;
        cin>>t>>s>>tmp.s;
        if(tmp.s=="TURN"){
            cin>>tmp.angle;
        }
        commands[t*6][mp[s]].push_back(tmp);
    }
    solve(tanks,commands);
}
}

