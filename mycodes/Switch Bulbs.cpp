
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


ll solve(ll m,ll pos,ll bitmask,ll target,vector<ll>&switches,vector<vector<ll> >&memory)
{
    //cout<<"pos="<<pos<<" target="<<target<<" bitmask="<<bitmask<<endl;
    if(bitmask==target){
        cout<<"memory["<<pos<<"]["<<bitmask<<"]="<<0<<endl;
        return memory[pos][bitmask]=0;
    }
    if(pos>m)
        return 1e9;
    if(memory[pos][bitmask]!=-1)
        return memory[pos][bitmask];
    ll i,j,res1,res2,nwbitmask;
    nwbitmask=(bitmask^(switches[pos]));
    //cout<<"at pos "<<pos<<" current mask="<<bitmask<<" nwmask="<<nwbitmask<<endl;
    res1=solve(m,pos+1,nwbitmask,target,switches,memory)+1;
    res2=solve(m,pos+1,bitmask,target,switches,memory);
    cout<<"memory["<<pos<<"]["<<bitmask<<"]="<<min(res1,res2)<<endl;;
    return memory[pos][bitmask]=min(res1,res2);
}



ll getTarget(string &s)
{
    ll i,j,target=0;
    REV(s);
    FOR(i,0,s.size()-1)
    {
        if(s[i]>'0')
        {
            target+=(1<<i);
        }
    }
    return target;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll test,t,n,m,i,j,q,k,l;
    cin>>test;
    FOR(t,1,test)
    {
        cin>>n>>m;
        vector<ll>switches(m+1);
        FOR(i,1,m)
        {
            cin>>k;
            ll tmp=0;
            FOR(j,1,k)
            {
                cin>>l;
                //cout<<"left shift "<<n-l-1<<endl;
                tmp+=(1<<(n-l-1));
            }
            switches[i]=tmp;
            //cout<<i<<"th switch ="<<tmp<<endl;
        }
        cin>>q;
        vector<ll>col(50000,-1);
        vector<vector<ll> >memory(50,col);
        cout<<"Case "<<t<<":\n";
        FOR(i,1,q)
        {
            ll target=0;
            string tmp;
            cin>>tmp;
            target=getTarget(tmp);
            //cout<<"target="<<target<<endl;
            ll res=solve(m,1,0,target,switches,memory);
            //cout<<res<<endl;
            if(res<100)
            {
                cout<<res<<endl;
            }
            else
                cout<<"-1"<<endl;
        }
    }

}

