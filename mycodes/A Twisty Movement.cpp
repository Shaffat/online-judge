#include<bits/stdc++.h>
using namespace std;


void rev(vector<int>&a,int start ,int last)
{
    while(start<=last)
    {
        int tmp;
        tmp=a[start];
        a[start]=a[last];
        a[last]=tmp;
        start++;
        last--;
    }
}

int main()
{
    int n,i,j,mx=-1;
    vector<int>order;
    vector<int>reverseorder;
    vector<int>before;
    vector<int>after;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        order.push_back(j);
    }
    for(i=0;i<=n;i++)
    {
        for(j=0;j<n;j++)
        {
            int start=j,last=j+i,pre,cur=0,k;
            if(last>=n)
            {
                continue;
            }
            rev(order,start,last);
            //cout<<"start="<<start<<" last="<<last<<endl;

            cur=1;
            for(k=1;k<n;k++)
            {
                if(order[k-1]>order[k])
                {
                    //cout<<"break on "<<k<<" cur="<<cur<<endl;
                    mx=max(mx,cur);
                    //cout<<"mx="<<mx<<endl;
                    cur=1;
                }
                else
                    cur++;
            }
             mx=max(mx,cur);
                //cout<<"mx="<<mx<<endl;
             rev(order,start,last);

        }
    }
    cout<<mx<<endl;
}
