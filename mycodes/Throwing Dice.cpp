#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct details
{
    ll up,down;
};
vector<int>col(160,0);
vector<vector<int> >testcase(40,col);
ll n,x,t;
ll gcd(ll a,ll b)
{
    while(a%b!=0)
    {
        ll c;
        c=b;
        b=a%b;
        a=c;
    }
    return b;
}
vector<details>col1(160);
vector<vector<details> >memory(40,col1);
details solve(ll index,ll sum)
{
    if(index>n)
    {
        details tmp;
        if(sum>=x)
        {
            tmp.up=1;
            tmp.down=1;
            return tmp;
        }
        else
        {
            tmp.up=0;
            tmp.down=1;
            return tmp;
        }
    }
    if(testcase[index][sum]==t)
    {
        return memory[index][sum];
    }
    details res,tmp;
    tmp=solve(index+1,sum+1);
    res.up=tmp.up;
    res.down=tmp.down;
    res.up+=solve(index+1,sum+2).up;
    res.up+=solve(index+1,sum+3).up;
    res.up+=solve(index+1,sum+4).up;
    res.up+=solve(index+1,sum+5).up;
    res.up+=solve(index+1,sum+6).up;
    res.down*=6;
    testcase[index][sum]=t;
    return memory[index][sum]=res;
}

int main()
{
    ll test;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&n,&x);
        details res=solve(1,0);
        if(res.up!=0)
        {
            ll d=gcd(res.down,res.up);
            res.up/=d;
            res.down/=d;
        }
        if(res.down==1||res.up==0)
        {
            printf("Case %lld: %lld\n",t,res.up);
        }
        else
            printf("Case %lld: %lld/%lld\n",t,res.up,res.down);
    }
}
