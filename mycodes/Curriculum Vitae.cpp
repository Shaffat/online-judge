#include<bits/stdc++.h>
using namespace std;


int dp(int index,int chk,vector<int>&game,vector<vector<int> >&memory)
{
    if (index>=game.size())
    {
        return 0;
    }
    if(memory[index][chk]!=-1)
    {
        return memory[index][chk];
    }
    if(chk==1)
    {
        if(game[index]==1)
        {
            return 1+dp(index+1,1,game,memory);
        }
        else
            return dp(index+1,1,game,memory);
    }
    else
    {
        if(game[index]==1)
        {
            return max(1+dp(index+1,1,game,memory),dp(index+1,0,game,memory));
        }
        else
            return 1+dp(index+1,0,game,memory);
    }
}

int main()
{
    int chk=0,i,j,n;
    cin>>n;
    vector<int>game;
    vector<int>value(2,-1);
    vector<vector<int> >memory(n+1,value);
    for(i=1;i<=n;i++)
    {
        cin>>j;
        game.push_back(j);
    }
    cout<<dp(0,0,game,memory);
}
