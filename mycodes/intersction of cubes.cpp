#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,x1,x2,y1,y2,z1,z2,X1=-1,X2=2e9,Y1=-1,Y2=2e9,Z1=-1,Z2=2e9;
        scanf("%d",&n);
        while(n--)
        {
            scanf("%d %d %d %d %d %d",&x1,&y1,&z1,&x2,&y2,&z2);
            X1=max(X1,x1);
            X2=min(X2,x2);
            Y1=max(Y1,y1);
            Y2=min(Y2,y2);
            Z1=max(Z1,z1);
            Z2=min(Z2,z2);

        }
        long long int volume=max((X2-X1)*(Y2-Y1)*(Z2-Z1),0);
        printf("Case %d: %lld\n",t,volume);
        t++;
    }
}
