#include<bits/stdc++.h>

using namespace std;

#define ll long long int
struct ans
{
    int f,g;
};
int sz=6,ans_sz=6;
void multiplication(vector<vector<int> >&a,vector<vector<int> >&b,vector<vector<int> >&res,int m)
{
    int i,j,k;
    for(i=0;i<a.size();i++)
    {
        for(j=0;j<b[0].size();j++)
        {
            int sum=0;
            for(k=0;k<b.size();k++)
            {
                sum+=(a[i][k]*b[k][j])%m;
                sum%=m;
            }
            res[i][j]=sum;
        }
    }
    return ;
}
void power(int p,vector<vector<int> >&base,vector<vector<int> >&res,int m)
{
    int i,j;

    if(p==1)
    {
        for(i=0;i<base.size();i++)
        {
            for(j=0;j<base[0].size();j++)
            {
                res[i][j]=base[i][j];
            }
        }
        return;
    }
    else if(p%2==0)
    {
        power(p/2,base,res,m);
        vector<int>col(sz);
        vector<vector<int> >tmp(sz,col);
        for(i=0;i<sz;i++)
        {
            for(j=0;j<sz;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,res,m);
    }
    else
    {
        power(p/2,base,res,m);
        vector<int>col(sz);
        vector<vector<int> >tmp(sz,col);
        vector<vector<int> >tmp2(sz,col);
        for(i=0;i<sz;i++)
        {
            for(j=0;j<sz;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,tmp2,m);
        multiplication(tmp2,base,res,m);
    }
    return;
}

ans solve(int n,int a1,int b1,int c1,int a2,int b2,int c2,int f0,int f1,int f2,int g0,int g1,int g2,int m)
{
    ans final_res;
    if(n==0)
    {
        final_res.f=f0;
        final_res.g=g0;
    }
    if(n==1)
    {
        final_res.f=f1;
        final_res.g=g1;
    }
    if(n==2)
    {
        final_res.f=f2;
        final_res.g=g2;
    }
    else
    {
        vector<int>col(sz);
        vector<vector<int> >p_matrix(sz,col);
        vector<vector<int> >power_matrix(sz,col);
        vector<int>col1(1);
        vector<vector<int> >base(sz,col1);
        vector<vector<int> >res(sz,col1);
        p_matrix[0][0]=a1;p_matrix[0][1]=b1;p_matrix[0][2]=0;p_matrix[0][3]=0;p_matrix[0][4]=0;p_matrix[0][5]=c1;

        p_matrix[1][0]=1;p_matrix[1][1]=0;p_matrix[1][2]=0;p_matrix[1][3]=0;p_matrix[1][4]=0;p_matrix[1][5]=0;

        p_matrix[2][0]=0;p_matrix[2][1]=1;p_matrix[2][2]=0;p_matrix[2][3]=0;p_matrix[2][4]=0;p_matrix[2][5]=0;

        p_matrix[3][0]=0;p_matrix[3][1]=0;p_matrix[3][2]=c2;p_matrix[3][3]=a2;p_matrix[3][4]=b2;p_matrix[3][5]=0;

        p_matrix[4][0]=0;p_matrix[4][1]=0;p_matrix[4][2]=0;p_matrix[4][3]=1;p_matrix[4][4]=0;p_matrix[4][5]=0;

        p_matrix[5][0]=0;p_matrix[5][1]=0;p_matrix[5][2]=0;p_matrix[5][3]=0;p_matrix[5][4]=1;p_matrix[5][5]=0;

        power(n-2,p_matrix,power_matrix,m);
        multiplication(power_matrix,base,res,m);
        final_res.f=res[0][0];
        final_res.g=res[3][0];
    }
    return  final_res;
}

int main()
{
    int test,t,q,a1,b1,c1,a2,b2,c2,f0,f1,f2,g0,g1,g2,n,m,i;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d",&a1,&b1,&c1,&a2,&b2,&c2,&f0,&f1,&f2,&g0,&g1,&g2,&m,&q);
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            scanf("%d",&n);
            ans res=solve(n,a1,b1,c1,a2,b2,c2,f0,f1,f2,g0,g1,g2,m);
            printf("%d %d\n",res.f,res.g);
        }
    }
}
