#include<bits/stdc++.h>
using namespace std;

bool ok(vector<int>&available,vector<int>&need)
{
    int i,j;
    for(i='a';i<='z';i++)
    {
        if(need[i]>available[i]){
//            cout<<"i="<<i<<" need="<<need[i]<<" have="<<available[i]<<endl;
//            cout<<"not ok"<<endl;
            return false;
        }
    }
    //cout<<"ok"<<endl;
    return true;
}


void preprocess(string s,vector<vector<int> >&counter)
{
    int i,j;
    for(i=0;i<s.size();i++)
    {
        counter[i][s[i]]++;
        if(i==0)
        {
            continue;
        }
        for(j='a';j<='z';j++)
        {
            counter[i][j]+=counter[i-1][j];
        }
    }
    return;
}

void need(vector<int>&v,string s)
{
    int i,j;
    for(i=0;i<s.size();i++)
    {
        v[s[i]]++;
    }
    return;
}

int solve(int n,string &s,vector<vector<int> >&sample)
{
    vector<int>require(140,0);
    need(require,s);
//    for(int j='a';j<='z';j++)
//    {
//        cout<<require[j]<<" ";
//    }
//    cout<<endl;
    int first=0,last=n-1,mid,f=0,res;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;

        mid=(first+last)/2;
        //cout<<"mid="<<mid<<endl;
        if(ok(sample[mid],require))
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int n,m,i,j,k,ans;
    string s1,s2;

    cin>>n>>s1>>m;
    vector<int>col(140,0);
    vector<vector<int> >data(n+1,col);
    preprocess(s1,data);
//    for(i=0;i<n;i++)
//    {
//        for(j='a';j<='z';j++)
//        {
//            cout<<data[i][j]<<" ";
//        }
//        cout<<endl;
//    }

    for(i=1;i<=m;i++)
    {
        cin>>s2;
        ans=solve(n,s2,data);
        cout<<ans+1<<endl;
    }

}
