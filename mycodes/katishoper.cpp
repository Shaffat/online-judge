#include<bits/stdc++.h>
using namespace std;



int oddCycle(int node,int parent,int timer,vector<vector<int> >&connection,vector<int>&vis,vector<int>&start)
{
    //printf("node is %d\n",node);
    int i,j,nxt,cur;
    int res=0;
    start[node]=timer;
    //printf("here1 ");
    for(i=0;i<connection[node].size();i++)
    {
        //printf("here 2\n");
        nxt=connection[node][i];
        //printf("next is %d",nxt);
        if(nxt==parent) continue;
        if(!vis[nxt])
        {
            vis[nxt]=1;
            res+=oddCycle(nxt,node,timer+1,connection,vis,start);
        }
        else if(vis[nxt]==1)
        {
            int len=abs(start[node]-start[nxt]+1);

            if(len%2==1)
            {
                res++;
            }
        }
    }
    vis[node]=2;
    //printf("done\n");
    return res;
}



int main()
{
    int u,v,n,m,i,j,res=0,cycle=0;
    scanf("%d %d",&n,&m);

    vector<int>col;
    vector<vector<int> >connection(n+1,col);
    vector<int>vis(n+1,0);
    vector<int>start(n+1);
    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }
    for(i=1;i<=n;i++)
    {
        //printf("%d\n",i);
        if(!vis[i])
        {
            //cout<<"root="<<i<<endl;
            vis[i]=1;
            res++;
            cycle+=oddCycle(i,i,0,connection,vis,start);
        }
    }
    if(cycle)
    {
        res--;
    }
    printf("%d\n",res);

}
