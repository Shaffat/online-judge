#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<int>uniset(n+1,0);
    vector<int>used(1e5+1,0);
    vector<int>num;
    set<int>s;
    for(i=0;i<n;i++)
    {
        scanf("%d",&j);
        if(i==0)
        {
            num.push_back(j);
            used[j]+=1;
            uniset[i]=0;
            s.insert(j);
        }
        else
        {
            num.push_back(j);
            if(used[j]!=0)
            {
                uniset[i]=s.size()-1;
            }
            else
                uniset[i]=s.size();
            s.insert(j);
            used[j]+=1;
        }
    }
    vector<int>taken(1e5+1,0);
    long long int res=0,tmp;
    for(i=n-1;i>0;i--)
    {
        if(taken[num[i]]==0)
        {
            taken[num[i]]=1;
            tmp=uniset[i];
            res+=tmp;
        }
    }
    for(i=0;i<=1e5;i++)
    {
        if(used[i]>1)
        {
            res++;
        }
    }
    cout<<res<<endl;
}
