#include<bits/stdc++.h>
using namespace std;


long long int lower_than(long long int n,vector<long long int>&v)
{
    long long int i,j,first,last,mid,res=-1,f=0;
    first=0;
    last=v.size()-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if(v[mid]<n)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
    long long int test,t,i,j,p,q,n,m;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        long long int res=0,total_hook,total_pos;
        scanf("%lld %lld",&n,&m);
        vector<long long int>pos;
        vector<long long int>hook;
        for(i=1;i<=n;i++)
        {
            scanf("%lld %lld",&p,&q);
            pos.push_back(p);
            hook.push_back(q);
        }
        sort(pos.begin(),pos.end());
        sort(hook.begin(),hook.end());
        for(i=1;i<=m;i++)
        {
            scanf("%lld %lld",&p,&q);
            total_hook=lower_than(q,hook)+1;
            total_pos=lower_than(p,pos)+1;
            //cout<<"left total_pos="<<total_pos<<" safe hook="<<total_hook<<endl;
            if(total_hook<total_pos)
            res+=abs(total_hook-total_pos);
            total_hook=n-lower_than(q+1,hook);
            total_pos=n-lower_than(p+1,pos);
            //cout<<"right total_pos="<<total_pos<<" safe hook="<<total_hook<<endl;
            if(total_hook<total_pos)
            res+=abs(total_hook-total_pos);
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
