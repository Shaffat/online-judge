#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct knight
{
    ll index,power,coin;
};
bool operator<(knight a, knight b)
{
    if(a.power!=b.power)
    {
        return a.power<b.power;
    }
    return false;
}
int main()
{
    ll n,k,i,j,c;
    scanf("%lld %lld",&n,&k);
    vector<knight>killer(n);
    vector<ll>total(n+1,0);
    for(i=0;i<n;i++)
    {
        killer[i].index=i;
        scanf("%lld",&killer[i].power);
    }
    for(i=0;i<n;i++)
    {
        scanf("%lld",&killer[i].coin);
    }
    sort(killer.begin(),killer.end());
    total[killer[0].index]=killer[killer[0].index].coin;
    for(i=1;i<n;i++)
    {
        ll murderer,victim;
        murderer=killer[i].index;
        victim=killer[i-1].index;
        total[murderer]=total[victim]+killer[i].coin;
    }
    for(i=0;i<n;i++)
    {
        printf("%lld ",total[i]);
    }
}
