#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,i,j,a,b,sum=0,mid;
    scanf("%d %d %d",&n,&a,&b);
    vector<int>v(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v[i]=j;

    }
    if(n%2==0)
    {
        mid=n/2;
    }
    else mid=n/2+1;
    for(i=1;i<mid;i++)
    {
        if(v[i]==1)
        {
            if(v[n-i+1]==0)
            {
                printf("-1\n");
                return 0;
            }
            else if(v[n-i+1]==2)
            {
                sum+=b;
            }
        }
        else if(v[i]==0)
        {
            if(v[n-i+1]==1)
            {
                printf("-1\n");
                return 0;
            }
            else if(v[n-i+1]==2)
            {
                sum+=a;
            }

        }
        else if(v[i]==2)
        {
            if(v[n-i+1]==1)
            {
                sum+=b;
            }
            else if(v[n-i+1]==0)
            {
                sum+=a;
            }
            else
                sum+=2*(min(a,b));
        }
    }
    if(n%2==0)
    {
        if(v[mid]==0)
        {
            if(v[n-mid+1]==1)
            {
                printf("-1\n");
                return 0;
            }
            else if(v[n-mid+1]==2)
            {
                sum+=a;
            }
        }
        else if(v[mid]==1)
        {
            if(v[n-mid+1]==0)
            {
                printf("-1\n");
                return 0;
            }
            else if(v[n-mid+1]==2)
            {
                sum+=b;
            }
        }
        else if(v[mid]==2)
        {
            if(v[n-mid+1]==0)
            {
                sum+=a;
            }
            else if(v[n-mid+1]==1)
            {
                sum+=b;
            }
            else if(v[n-mid+1]==2)
            {
                sum+=2*min(a,b);
            }
        }
    }
    else
    {
        //cout<<"sum="<<sum<<endl;
        if(v[mid]==2)
        {
            sum+=min(a,b);
        }
    }
    printf("%d\n",sum);
    return 0;
}
