#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int i,n,j,k,sum=0;
        scanf("%d %d",&n,&k);
        for(i=1;i*i<=n;i++)
        {
            if(n%i==0)
            {
                if(i%k!=0)
                {
                    sum+=i;
                }
                if(n/i!=i)
                {
                    if((n/i)%k!=0)
                    {
                        sum+=n/i;
                    }
                }
            }
        }
        printf("%d\n",sum);

    }
}
