#include<bits/stdc++.h>
using namespace std;


int solve(int first,int last,string &s,vector<vector<int> >&memory)
{
    if(first>last)
    {
        return 0;
    }
    if(memory[first][last]!=-1)
    {
        return memory[first][last];
    }
    int res;
    if(s[first]==s[last])
    {
        res=solve(first+1,last-1,s,memory);
    }
    else
    {
        res=min(solve(first+1,last,s,memory),solve(first,last-1,s,memory))+1;
    }
    return memory[first][last]=res;
}

int main()
{
    int test,t;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        vector<int>col(100,-1);
        vector<vector<int> >memory(100,col);
        cin>>s;
        int res=solve(0,s.size()-1,s,memory);
        cout<<"Case "<<t<<": "<<res<<endl;
    }
}
