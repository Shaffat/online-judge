#include<bits/stdc++.h>
using namespace std;

void z_algo(string &s,vector<int>&z_array)
{
    int i,j,l=0,r=0,nxt,matched;
    z_array[0]=0;
    cout<<"0 ";
    for(i=1;i<s.size();i++)
    {
        if(r<i)
        {
            l=i;
            r=i;
        }
        if(l<=i && r>=i)
        {
            int nxt=min(z_array[i-l],r-i);
            //cout<<"i="<<i<<" nxt="<<nxt<<" l="<<l<<" r="<<r<<endl;
            if(i+nxt<r)
            {
                z_array[i]=nxt;
            }
            else
            {
                for(j=i+nxt;j<s.size();j++)
                {
                    //cout<<"matching between "<<j<<" and "<<nxt<<endl;
                    if(s[j]==s[nxt])
                    {
                        nxt++;
                        r++;
                    }
                    else
                        break;
                }
                z_array[i]=nxt;
                l=i;
            }
            cout<<" "<<z_array[i]<<" ";
        }

    }
    return;
}

int main()
{
    string s;
    cin>>s;
    vector<int>z(s.size());
    z_algo(s,z);
}
