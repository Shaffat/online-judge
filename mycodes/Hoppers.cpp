#include<bits/stdc++.h>
using namespace std;



int oddCycle(int node,int parent,int timer,vector<vector<int> >&connection,vector<int>&vis,vector<int>&start)
{
    int i,j,nxt,cur;
    int res=0;
    start[node]=timer;
    cout<<"start of "<<node<<" ="<<timer <<endl;
    cout<<"here"<<endl;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        cout<<"nxt="<<nxt<<endl;
        if(nxt==parent) continue;
        if(!vis[nxt])
        {
            vis[nxt]=1;
            cur=oddCycle(nxt,node,timer+1,connection,vis,start);
            res|=cur;
        }
        else if(vis[nxt]==1)
        {
            int len=abs(start[node]-start[nxt]+1);
            cout<<"from "<<node<<" to "<<nxt<<" len="<<len<<endl;
            if(len%2==1)
            {
                return 1;
            }
        }
    }
    cout<<"done "<<endl;
    vis[nxt]=2;
    return res;
}



int main()
{
    int n,m,i,j,u,v,cmp=0,ok=0,res=0,gh;
    int timer=0;
    scanf("%d %d",&n,&m);
    vector<int>col;
    vector<vector<int> >connection(n+100,col);
    vector<int>vis(n+100,0);
    vector<int>start(n+100,0);

    for(i=1;i<=m;i++)
    {
        scanf("%d %d",&u,&v);
        connection[u].push_back(v);
        connection[v].push_back(u);
    }

    for(i=1;i<=n;i++)
    {
        //cout<<"i="<<i<<endl;
        if(vis[i]==0)
        {
            vis[i]=1;
            timer=0;
            oddCycle(i,i,timer,connection,vis,start);
        }
    }
    if(ok)
    {
        res--;
    }
    printf("%d\n",res);
}
