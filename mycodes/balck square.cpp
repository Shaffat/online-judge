#include<bits/stdc++.h>
using namespace std;
int main()
{
    int n,m,i,j;
    scanf("%d %d",&n,&m);
    vector<char>column(m+1);
    vector< vector<char> >sheet(n+1,column);
    int lowestrow=1000,lowestcolum=100,highestrow=-1,highestcolumn=-1;
    int b=0;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            char c;
            cin>>c;
            if(c=='B')
            {
                b++;
                lowestcolum=min(lowestcolum,j);
                highestcolumn=max(highestcolumn,j);
                highestrow=max(highestrow,i);
                lowestrow=min(lowestrow,i);
            }
        }
    }
    ///cout<<"highestrow="<<highestrow<<" lowestrow="<<lowestrow<<" highestcolumn="<<highestcolumn<<" lowestcolum="<<lowestcolum<<endl;
    int sq=max((highestrow-lowestrow+1),(highestcolumn-lowestcolum+1));
    if(b==0)
    {
        cout<<"1"<<endl;
    }
    else if(sq>min(n,m))
    {
        cout<<"-1"<<endl;
    }
    else
    {

        cout<<(sq*sq)-b<<endl;
    }
}
