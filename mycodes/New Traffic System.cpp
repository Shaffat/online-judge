#include<bits/stdc++.h>
using namespace std;

struct node
{
    int name,used,weight;
};
struct cost
{
    int w,t;
};
bool operator <(node a, node b)
{
    if(a.weight!=b.weight)
    {
        return a.weight>b.weight;
    }
    return false;
}

int solve(int n,int d,vector<vector<int> >&connection,vector<vector<cost> >&details)
{
   int i,j;
   node source;
   source.name=0;
   source.used=0;
   source.weight=0;
   vector<int>col(11,1e9);
   vector<vector<int> >dis(n,col);
   dis[0][0]=0;
   priority_queue<node>q;
   q.push(source);
   while(!q.empty())
   {
       node cur=q.top();
       q.pop();
       int name,use;
       name=cur.name;
       use=cur.used;
       if(use>d)
       {
           continue;
       }
       for(i=0;i<connection[name].size();i++)
       {
           int nxt=connection[name][i];
           int type=details[name][i].t;
           int c=details[name][i].w;
           if(dis[nxt][use+type]>dis[name][use]+c)
           {
               node tmp;
               tmp.name=nxt;
               tmp.used=use+type;
               tmp.weight=dis[name][use]+c;
               dis[nxt][use+type]=tmp.weight;
               q.push(tmp);
           }
       }
   }
   int res=1e9;
   for(i=0;i<=d;i++)
   {
       res=min(res,dis[n-1][i]);
   }
   return res;
}



int main()
{
    int t=1,test,n,m,k,d,i,j,u,v,w;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d %d %d",&n,&m,&k,&d);
        vector<vector<int> >connection(n);
        vector<vector<cost> >details(n);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost tmp;
            tmp.t=0;
            tmp.w=w;
            details[u].push_back(tmp);
        }
        for(i=1;i<=k;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost tmp;
            tmp.t=1;
            tmp.w=w;
            details[u].push_back(tmp);
        }
        int res=solve(n,d,connection,details);
        if(res>1e8)
        {
            printf("Case %d: Impossible\n",t);
        }
        else
        {
            printf("Case %d: %d\n",t,res);
        }
        t++;
    }
}
