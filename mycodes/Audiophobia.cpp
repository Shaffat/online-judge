#include<bits/stdc++.h>
using namespace std;
void floyed_warshall(int n,vector<vector<int> >&distance)
{
    int i,j,k;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            for(k=1;k<=n;k++)
            {
                //cout<<"distance of ["<<j<<"]["<<k<<"]="<<distance[j][k]<<" distance of ["<<j<<"]["<<i<<"]="<<distance[j][i]<<" distance of ["<<i<<"]["<<k<<"]="<<distance[i][k]<<endl;
                if(distance[j][k]>max(distance[j][i],distance[i][k]))
                {
                    distance[j][k]=max(distance[j][i],distance[i][k]);
                    //cout<<"updated distance["<<j<<"]["<<k<<"]="<<distance[j][k]<<endl;
                }
            }
        }
    }
    return;
}

int main()
{
    int c,s,q,i,j,u,v,w,t=1;
    while(scanf("%d %d %d",&c,&s,&q))
    {
        if(c==0 && s==0 && q==0)
        {
            break;
        }
        if(t>1)
        {
            printf("\n");
        }
        vector<int>col1(c+1,1e9);
        vector<vector<int> >connection(c+1,col1);
        vector<vector<int> >distance(c+1,col1);
        for(i=1;i<=s;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            distance[u][v]=w;
            distance[v][u]=w;
        }
        printf("Case #%d\n",t);
        floyed_warshall(c,distance);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d",&u,&v);
            if(distance[u][v]==1e9)
            {
                printf("no path\n");
            }
            else
            printf("%d\n",distance[u][v]);
        }
        t++;

    }
}
