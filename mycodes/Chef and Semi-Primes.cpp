#include<bits/stdc++.h>
using namespace std;

vector<int>prime(210,0);
vector<int>primes;
vector<int>semiprime(210,0);
void sieve()
{
    int i,j;
    prime[1]=0;
    for(i=4;i<=200;i+=2)
    {
        prime[4]=1;
    }
    primes.push_back(2);
    for(i=3;i<=200;i+=2)
    {
        if(!prime[i])
        {
            for(j=i*i;j<=200;j+=i)
            {
                prime[j]=1;
            }
        }
    }
    for(i=3;i<=200;i+=2)
    {
        if(!prime[i])
        {

           primes.push_back(i);
        }
    }
    for(i=0;i<primes.size()-1;i++)
    {
        for(j=i+1;j<primes.size();j++)
        {
            if(primes[i]*primes[j]<210)
            {
                semiprime[primes[i]*primes[j]]=1;;
            }
        }
    }
}


bool solve(int n)
{
    int i;
    for(i=2;i<=n;i++)
    {
        if(semiprime[i] && semiprime[n-i])
        {
            return true;
        }
    }
    return false;
}

int main()
{
    sieve();
    int i,j,t,test,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        if(solve(n))
        {
            printf("YES\n");
        }
        else
            printf("NO\n");
    }
}
