#include<bits/stdc++.h>
using namespace std;
vector<long long int>primes;

struct details
{
    int p,d;
};

void sieve()
{
    int i,j;
    vector<bool>num(100000,0);
    for(i=4;i<100000;i+=2)
    {
        num[i]=1;
    }
    num[0]=1;
    primes.push_back(2);
    for(i=3;i<=sqrt(100000);i+=2)
    {
        for(j=i*i;j<100000;j+=i)
        {
            num[j]=1;
        }
    }
    for(i=3;i<100000;i+=2)
    {
        if(num[i]==0)
        {
            primes.push_back(i);
        }
    }
    return;
}

bool is_currect(int gcd,int lcm,vector<long long int>&differnce)
{
   long long int  tmp,g,l;
    int i,j;
    for(i=0;i<primes.size();i++)
    {
        g=0;
        l=0;
        while(gcd>1 && gcd%primes[i]==0)
        {
            gcd/=primes[i];
            g++;
        }
        while(lcm>1 && lcm%primes[i]==0)
        {
            lcm/=primes[i];
            l++;
        }
        if(g>l)
        {
            return false;
        }
        if(g<l)
        {
            tmp=1;
            for(j=1;j<=l-g;j++)
            {
                tmp*=primes[i];
            }
            differnce.push_back(tmp);
        }
    }
    if(gcd!=1)
    {
        g=1;
        l=0;
        while(lcm>1 && lcm%gcd==0)
        {
            lcm/=gcd;
            l++;
        }
        if(g>l)
        {
            return false;
        }
        if(g<l)
        {
            tmp=1;
            for(j=1;j<=l-g;j++)
            {
                tmp*=gcd;
            }
            differnce.push_back(tmp);
        }
    }
    else if(lcm!=1)
    {
        tmp=lcm;
        differnce.push_back(tmp);
    }
    return true;
}


long long int solve(long long int index,long long int gcd,long long int cur,long long int l,long long int r,long long int lcm,vector<long long int>&difference)
{
    //cout<<"cur="<<cur<<endl;
    if(index>=difference.size())
    {
        if(cur>=l && cur<=r)
        {
            //cout<<"pair is "<<cur<<" and "<<gcd*(lcm/cur)<<endl;
            if((lcm/cur)*gcd>=l && (lcm/cur)*gcd<=r)
            {
                return 1;
            }
        }
        return 0;
    }

    long long int tmp,res=0,add=1,i;

    res+=solve(index+1,gcd,cur,l,r,lcm,difference);
    res+=solve(index+1,gcd,cur*difference[index],l,r,lcm,difference);
    return res;
}

int main()
{
    sieve();
    long long int l,r,x,y;
    cin>>l>>r>>x>>y;
    vector<long long int>difference;
    if(is_currect(x,y,difference))
    {
        if(difference.size()==0)
        {
            if(x>=l && x<=r)
                cout<<"1"<<endl;
            else
                cout<<"0"<<endl;
        }
        else
        cout<<solve(0,x,x,l,r,y,difference)<<endl;
    }
    else
    cout<<"0"<<endl;
}
