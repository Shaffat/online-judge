#include<bits/stdc++.h>
using namespace std;

bool done(int x)
{
    x++;
    double c,f;
    c=ceil(log2(x));
    f=floor(log2(x));
    if(c==f) return true;
    return false;
}

int main()
{
    int i,j,x,t=0;
    cin>>x;
    vector<int>v;
    while(1)
    {
        if(done(x)) break;
        t++;
        i=log2(x);
        i++;
        x^=((1<<i)-1);
        v.push_back(i);
        if(done(x)) break;
        t++;
        x++;
    }
    cout<<t<<endl;
    for(i=0;i<v.size();i++)
        cout<<v[i]<<" ";
}
