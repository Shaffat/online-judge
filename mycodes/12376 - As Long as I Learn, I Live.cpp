
#include<bits/stdc++.h>
using namespace std;


int value[101];
int color[101];
int dis[101];
int lastnode[101];
int dfs(int source, vector<vector<int> >&connection)
{
    //cout<<"source="<<source<<endl;
    color[source]=1;
    int i,j;
    if(dis[source]!=-1)
    {
        return dis[source];
    }
    int learn=0;
    lastnode[source]=source;
    for(i=0;i<connection[source].size();i++)
    {
        //cout<<"Source node="<<source<<" ";
        int next=connection[source][i];
        //cout<<"connect to ="<<next<<endl;

            int case1=dfs(next,connection);
            if(case1>learn)
            {
                learn=case1;
                lastnode[source]=next;
            }
        //}
    }
    color[source]=2;
    //cout<<" dis of "<<source<<"=";
    //cout<<learn+value[source]<<endl;
    return dis[source]=learn+value[source];
}

int finddeath(int node)
{
    if(lastnode[node]==node)
    {
        return node;
    }
    return finddeath(lastnode[node]);
}

void initialeverything(int n)
{
    int i;
    for(i=0;i<=n;i++)
    {
        lastnode[i]=i;
        dis[i]=-1;
        color[i]=0;
    }

}
int main()
{
   int test,t=1;
   scanf("%d",&test);
   while(t<=test)
   {
       int n,m,i,j,u,v;
       scanf("%d %d",&n,&m);
       initialeverything(n);
       vector<int>vi;
       vector< vector<int> >connection(n+1,vi);
       for(i=0;i<n;i++)
       {
           scanf("%d",&u);
           value[i]=u;
       }
       for(i=1;i<=m;i++)
       {
           scanf("%d %d",&u,&v);
           connection[u].push_back(v);
       }

       int res=dfs(0,connection);
       int node=finddeath(0);
       printf("Case %d: %d %d\n",t,res,node);
       t++;
   }
}
