#include<bits/stdc++.h>
using namespace std;

struct details
{
    int a,b;
};

int main()
{
    int i,j,a,b,n,cant=0;
    scanf("%d",&n);
    vector<details>bottles;
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&a,&b);
        details tmp;
        tmp.a=a;
        tmp.b=b;
        bottles.push_back(tmp);
    }
    for(i=0;i<bottles.size();i++)
    {
        int done=0;
        for(j=0;j<bottles.size();j++)
        {
            if(i==j)
            {
                continue;
            }
            if(bottles[i].a==bottles[j].b)
            {
                done=1;
            }
        }
        if(!done)
        {
            cant++;
        }
    }
    cout<<cant<<endl;
}
