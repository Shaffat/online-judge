#include<bits/stdc++.h>

using namespace std;
int n;
int solve(int c1,int c2,vector<vector<int> >&connection)
{
     vector<int>vis(n+1,0);
     vector<int>dis(n+1,0);
     vis[c1]=1;
     queue<int>q;
     q.push(c1);
     while(!q.empty())
     {
         int cur=q.front();
         //cout<<"cur="<<cur<<endl;
         if(cur==c2)
         {
             return dis[cur]-1;
         }
         int i,j;
         q.pop();
         for(i=0;i<connection[cur].size();i++)
         {
             int nxt=connection[cur][i];
             if(!vis[nxt])
             {
                 vis[nxt]=1;
                 dis[nxt]=dis[cur]+1;
                 q.push(nxt);
             }
         }
     }
}

int main()
{
    int test,t=1,f=0;
    scanf("%d",&test);
    while(t<=test)
    {
        if(f)
        {
            printf("\n");
        }
        f++;
       int i,j,c1,c2;
       scanf("%d",&n);
       vector<vector<int> >connection(n+1);
       for(i=1;i<=n;i++)
       {
           int k,u,v,l;
           scanf("%d %d",&j,&k);

           for(l=1;l<=k;l++)
           {
               scanf("%d",&u);
               connection[j].push_back(u);
           }

       }
       scanf("%d %d",&c1,&c2);
       printf("%d %d %d\n",c1,c2,solve(c1,c2,connection));
       t++;

    }
}
