#include<bits/stdc++.h>
using namespace std;

#include<bits/stdc++.h>
using namespace std;

void LongestProperSuffix(string &pattern,vector<int>&lps,vector<int>&first_counter)
{
    int i,j,cur;
    lps[0]=-1;
    first_counter[0]=1;
    for(i=1;i<pattern.size();i++)
    {
        cur=i-1;
        lps[i]=-1;
        if(pattern[i]==pattern[lps[cur]+1])
        {
            lps[i]=lps[cur]+1;
        }
        else
        {
            while(cur!=-1)
            {
                if(pattern[i]==pattern[lps[cur]+1])
                {
                    lps[i]=lps[cur]+1;
                    break;
                }
                else
                    cur=lps[cur];
            }
        }
        first_counter[lps[i]+1]++;
    }
    return;
}

void counting(int n,vector<int>&lps,vector<int>&first_counter,vector<int>&total_counter)
{
    int i,j;
    vector<int>exist(n+1,0);
    vector<int>uniq;
    for(i=0;i<=n;i++)
    {
        j=lps[i]+1;
        if(!exist[j])
        {
            uniq.push_back(j);
            exist[j]=1;
        }
    }
    sort(uniq.begin(),uniq.end());
    for(i=uniq.size()-1;i>0;i--)
    {
        j=uniq[i];
        total_counter[j]+=first_counter[j];
        total_counter[lps[j-1]+1]+=total_counter[j];
    }
    return;
}

void correct_suffix(int n,vector<int>&lps,vector<int>&correct)
{
    int cur=n;
    while(lps[cur]!=-1)
    {
        correct.push_back(lps[cur]+1);
        cur=lps[cur];
    }
    return;
}




int main()
{
    ios_base::sync_with_stdio(0);
    string s;
    int add=0,extra=0;
    cin>>s;
    vector<int>lps(s.size());
    vector<int>first_counter(s.size()+1,0);
    vector<int>total(s.size()+1,0);
    vector<int>correct;
    LongestProperSuffix(s,lps,first_counter);
    counting(s.size()-1,lps,first_counter,total);
    correct_suffix(s.size()-1,lps,correct);
    cout<<correct.size()+1<<endl;
    for(int i=correct.size()-1;i>=0;i--)
    {
        cout<<correct[i]<<" "<<total[correct[i]]+1<<endl;
    }
    cout<<s.size()<<" 1"<<endl;
}
