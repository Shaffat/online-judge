#include<bits/stdc++.h>

using namespace std;

struct board
{
    int row,column;
};

int main()
{
    string s;
    getline(cin,s);

    board start,destination;
    start.column=s.at(0);
    start.row=s.at(1);
    destination.column=s.at(3);
    destination.row=s.at(4);
    int vis[110][110],dis[110][110];
    for(int i=1;i<=109;i++)
    {
        for(int j=1;j<=109;j++)
        {
            vis[i][j]=0;
            dis[i][j]=0;

        }
    }
    queue<board>knight;
    knight.push(start);
    vis[start.row][start.column]=1;
    int moves=0;
    while(!knight.empty())
    {
        board current=knight.front();
        knight.pop();
        board temp;
        temp.column=current.column-2;
        temp.row=current.row-1;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column+2;
        temp.row=current.row-1;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column-2;
        temp.row=current.row+1;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column+2;
        temp.row=current.row+1;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column-1;
        temp.row=current.row-2;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column+1;
        temp.row=current.row-2;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column-1;
        temp.row=current.row+2;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }

        temp.column=current.column+1;
        temp.row=current.row+2;
        if(temp.column>=97 &&temp.row<=104 && temp.row>=49 &&temp.row<=56 && !vis[temp.row][temp.column])
        {
            if(temp.column==destination.column && temp.row==destination.row)
            {
                moves=dis[current.row][current.column]+1;
                break;
            }
            else
            {
                knight.push(temp);
                vis[temp.row][temp.column]=1;
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
            }
        }
    }

    cout<<"To get from "<<s.at(0)<<start.column<<" to "<<destination.row<<destination.column<<" takes "<<moves<<" knight moves."<<endl;

}
