///Shaffat Nur Dipu

#include<bits/stdc++.h>
using namespace std;
vector<int>v(101,0);
vector<vector<int> >isvalid(101,v);
vector<vector<int> >memory(101,v);
int t=1,w;

int solve(int index,int k,vector<int>&dustpoints)
{
    //cout<<"index="<<index<<" k="<<k<<endl;
    if(index>=dustpoints.size() || k<=0)
    {
        return 0;
    }
    if(isvalid[index][k]==t)
    {
        return memory[index][k];
    }
    int i,j,dust,nxt,res1,res2;
    ///get the next point to clean in next move
    vector<int>::iterator up;
    up=upper_bound(dustpoints.begin(),dustpoints.end(),dustpoints[index]+w);
    nxt=up-dustpoints.begin();
    dust=nxt-index;
    ///two option
    ///set brush this point or set brush in next point
    res1=solve(nxt,k-1,dustpoints)+dust;
    res2=solve(index+1,k,dustpoints);
    isvalid[index][k]=t;
    return memory[index][k]=max(res1,res2);
}


int main()
{
    int test,i,j,n,x,y,k;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d %d",&n,&w,&k);
        vector<int>dust;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&x,&y);
            dust.push_back(y);
        }
        sort(dust.begin(),dust.end());
        int res=solve(0,k,dust);
        printf("Case %d: %d\n",t,res);
        t++;
    }

}
