
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct person
{
    ll id,pos,group;
};

bool operator <(person a, person b)
{
    if(a.group!=b.group)
    {
        return a.group>b.group;
    }
    return false;
}

ll solve(vl &persons,ll k)
{
    sort(persons.begin(),persons.end());

    ll i,j,res=0;
    priority_queue<person>pq;
    queue<person>q;
    vector<ll>dp(persons.size()+10,0);
    dp[1]=1e9;
    dp[2]=1e9;
    person tmp;

    tmp.id=0;
    tmp.pos=persons[0];
    tmp.group=1e9;
    q.push(tmp);

    tmp.id=1;
    tmp.pos=persons[1];
    tmp.group=1e9;
    q.push(tmp);

    tmp.id=0;
    tmp.pos=persons[0];
    tmp.group=0;
    pq.push(tmp);

    for(i=2;i<persons.size();i++)
    {
        //cout<<"i="<<i<<" pos="<<persons[i]<<endl;

        if(persons[i]-persons[i-2]<=2*k)
        {
            dp[i+1]=dp[i-2]+1;
            //cout<<"dp["<<i+1<<"]="<<dp[i+1]<<" dp["<<i-2<<"]="<<dp[i-2]<<endl;
        }
        else
            dp[i+1]=1e9;
        while(!pq.empty())
        {
            tmp=pq.top();
            //cout<<"top element "<<tmp.id<<" pos="<<tmp.pos<<endl;
            if(persons[i]-persons[tmp.id]<=2*k)
            {
                dp[i+1]=min(dp[tmp.id]+1,dp[i+1]);
                //cout<<"dp["<<i+1<<"]="<<dp[i+1]<<endl;
                break;
            }
            else{
                //cout<<"poping pq"<<endl;
                pq.pop();
            }
        }
        tmp.id=i;
        tmp.pos=persons[i];
        tmp.group=dp[i];

        pq.push(q.front());
        q.pop();
        q.push(tmp);
    }
    return dp[persons.size()];
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll test,t,n,k,i,j;
    sc1(test);
    FOR(t,1,test)
    {
        sc2(n,k);
        vl v;
        FOR(i,1,n)
        {
            sc1(j);
            v.push_back(j);
        }
        ll res=solve(v,k);
        if(res>1e5) res=-1;
        printf("Case %lld: %lld\n",t,res);
    }
}
