#include<bits/stdc++.h>
using namespace std;

int main()
{
    vector<int>digits(10,0);
    int i,j,n;
    scanf("%d",&n);
    for(i=1;i<=6*n;i++)
    {
        scanf("%d",&j);
        digits[j]++;
    }
    int mn=2e9;
    for(i=0;i<=9;i++)
    {
        mn=min(mn,digits[i]);
    }
    int res=0;
    if(mn==0)
    {

        for(i=1;i<=9;i++)
        {
            if(digits[i]<1)
            {
                break;
            }
            res=i;
        }
    }
    else if(mn=1)
    {
        int l,k;
        for(i=1;i<=9;i++)
        {
            l=i;
            if(digits[i]<2)
            {
               break;
            }
        }
        res=l*10+l-1;
    }
    cout<<res<<endl;
}
