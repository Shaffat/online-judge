#include<bits/stdc++.h>
using namespace std;

vector<bool>prime(100100,0);
vector<int>nxt(100100);

void sieve()
{
    prime[1]=1;
    int i,j,cur;
    for(i=4;i<100100;i+=2)
    {
        prime[i]=1;
    }
    for(i=3;i<=sqrt(100100);i+=2)
    {
        if(prime[i]==0)
        {
            for(j=i*i;j<100100;j+=i)
            {
                prime[j]=1;
            }
        }
    }
    cur=-1;
    for(i=100099;i>=1;i--)
    {
        if(prime[i]==0)
        {
            cur=i;
        }
        nxt[i]=cur;
    }
}

int main()
{
    sieve();
    int n,m,rowcost=2e9,colcost=2e9,cur,i,j;
//    for(i=1;i<=100;i++)
//    {
//        cout<<i<<" mx="<<nxt[i]<<endl;
//    }
    scanf("%d %d",&n,&m);
    vector<int>col(m+1);
    vector<vector<int> >matrix(n+1,col);
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf("%d",&cur);
            matrix[i][j]=cur;
        }
    }
    for(i=1;i<=n;i++)
    {
        cur=0;
        for(j=1;j<=m;j++)
        {
            cur+=abs(nxt[matrix[i][j]]-matrix[i][j]);
        }
        //cout<<i<<"th row="<<cur<<endl;
        rowcost=min(rowcost,cur);
    }
     for(i=1;i<=m;i++)
    {
        cur=0;
        for(j=1;j<=n;j++)
        {
            cur+=abs(nxt[matrix[j][i]]-matrix[j][i]);
        }
        //cout<<i<<"th col="<<cur<<endl;
        colcost=min(colcost,cur);
    }
    cout<<min(rowcost,colcost)<<endl;
}
