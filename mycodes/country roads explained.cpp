#include<bits/stdc++.h>
using namespace std;

struct node
{
    int n,w;
};

bool operator <(node x,node y)
{
    if(x.w!=y.w)
    {
        return x.w>y.w;
    }
    return false;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,e,u,v,w,i,j,source;
        scanf("%d %d",&n,&e);
        vector<int>edges[n+1];
        vector<int>cost[n+1];
        vector<int>vis(n+1,0);
        vector<int>path(n+1,0);
        vector<int>totalcost(n+1,1<<29);
       for(i=1;i<=e;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            edges[u].push_back(v);
            edges[v].push_back(u);
            cost[u].push_back(w);
            cost[v].push_back(w);
        }
        scanf("%d",&source);
        priority_queue<node>q;
        node start;
        start.n=source;
        start.w=0;
        q.push(start);
        path[source]=0;
        totalcost[source]=0;

        while(!q.empty())
        {
            node cur_top=q.top();
            q.pop();
            int cur_node=cur_top.n;
            //cout<<"cur_node="<<cur_node<<endl;
            if(vis[cur_node])
            {
                //cout<<"skipped"<<endl;
                continue;
            }
            vis[cur_node]=1;
            for(i=0;i<edges[cur_node].size();i++)
            {
                int nxt_node=edges[cur_node][i];
                int nxt_cost=cost[cur_node][i];
                //cout<<"nxt node ="<<nxt_node<<" nxt cost="<<nxt_cost<<" totalcost["<<nxt_node<<"]="<<totalcost[nxt_node]<<endl;
                if(totalcost[nxt_node]>nxt_cost)
                {
                    node nwnode;
                    nwnode.n=nxt_node;
                    nwnode.w=nxt_cost;
                    q.push(nwnode);
                    totalcost[nxt_node]=nxt_cost;
                    if(path[cur_node]<nxt_cost)
                    {
                        path[nxt_node]=nxt_cost;
                    }
                    else
                    {
                        path[nxt_node]=path[cur_node];
                    }
                    //cout<<"path["<<nxt_node<<"]= "<<path[nxt_node]<<endl;
                }
            }
            //cout<<"\n\n";
        }
        printf("Case %d:\n",t);
        for(i=0;i<n;i++)
        {
            if(totalcost[i]>300000000)
            {
                printf("Impossible\n");
            }
            else
            printf("%d\n",path[i]);
        }
        t++;



    }
}

