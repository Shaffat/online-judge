#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        long long int n,m,i,j,r,c,last,middle;
        scanf("%lld",&n);
        m=ceil(sqrt(n));
        //cout<<"m="<<m<<endl;
        last=m*m;
        middle=(m-1)*(m-1)+m;
        //cout<<"last="<<last<<" middle="<<middle<<" dif "<<last-n<<endl;
        if(m%2==1)
        {
                   if(last-n>=(m-1))
                    {
                        c=m;
                        r=m-(middle-n);
                    }
                    else
                    {
                        //cout<<"yes"<<endl;
                        r=m;
                        c=last-n+1;
                    }

        }
        else
        {
                    if(last-n>=(m-1))
                    {
                        r=m;
                        c=m-(middle-n);
                    }
                    else
                    {
                        //cout<<"yes"<<endl;
                        c=m;
                        r=last-n+1;
                    }

        }
        printf("Case %d: %lld %lld\n",t,c,r);
        t++;
    }
}
