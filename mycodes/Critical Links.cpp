#include<bits/stdc++.h>
using namespace std;

struct critical
{
    int u,v;
};

bool operator <(critical a,critical b)
{
    if(a.u != b.u)
    {
        return a.u<b.u;
    }
    else
        return a.v<b.v;
}

int n,t=0,timer=0;
vector<int>vis(10001,0);
vector<int>start(10001);
vector<int>low(10001);


void solve(int node,int parent,vector<critical>&links,vector<vector<int> >&connection,vector<vector<int> >&hypocrite)
{
    //cout<<"node="<<node<<" parent="<<parent<<" t="<<t<<endl;
    int i,j,child;
    start[node]=low[node]=timer;
    timer++;
    //cout<<"start ="<<start[node]<<endl;
    for(i=0;i<connection[node].size();i++)
    {
        child=connection[node][i];
        if(child==parent)
        {
            continue;
        }
        if(vis[child]!=t)
        {
            vis[child]=t;
            solve(child,node,links,connection,hypocrite);
            //cout<<"start of node "<<start[node]<<" child "<<child<<" ="<<low[child]<<endl;
            if(low[child]>start[node] && hypocrite[node][i]==1)
            {
                //cout<<"tore paisi"<<endl;
                critical tmp;
                if(child>node)
                {
                    tmp.u=node;
                    tmp.v=child;
                }
                else
                {
                    tmp.u=child;
                    tmp.v=node;
                }
                links.push_back(tmp);
            }
            low[node]=min(low[child],low[node]);
        }
        else
        {
            low[node]=min(low[node],start[child]);
        }
    }
    //cout<<"low of node "<<node<<"="<<low[node]<<endl;
    return;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,i,j,l,u,e,v,k;
    scanf("%d",&test);
    t=1;
    while(t<=test)
    {
        vector<critical>links;
        timer=1;
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >hypocrite(n+1);
        for(i=0;i<n;i++)
        {
            vector<int>multi(n+1,0);
            scanf("%d",&u);
            char tmp;
            scanf(" %c",&tmp);
            scanf("%d",&e);
            scanf(" %c",&tmp);
            for(j=1;j<=e;j++)
            {
                scanf("%d",&v);
                connection[u].push_back(v);
                multi[v]++;
            }
            for(k=0;k<connection[u].size();k++)
            {
                if(multi[connection[u][k]]>1)
                {
                    hypocrite[u].push_back(0);
                }
                else
                    hypocrite[u].push_back(1);
            }
            /*
             for(k=0;k<hypocrite[u].size();k++)
            {
                cout<<hypocrite[u][k]<<" ";
            }
            cout<<endl;
            */
        }
        for(i=0;i<n;i++)
        {
            if(vis[i]!=t)
            {
                vis[i]=t;
                solve(i,i,links,connection,hypocrite);
            }
        }
        sort(links.begin(),links.end());
        printf("Case %d:\n",t);
        printf("%d critical links\n",links.size());
        for(i=0;i<links.size();i++)
        {
            printf("%d - %d\n",links[i].u,links[i].v);
        }
        t++;
    }

}
