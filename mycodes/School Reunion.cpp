#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct stay
{
    ll start,ending;
};

bool operator <(stay a,stay b)
{
    if(a.ending!=b.ending)
    {
        return a.ending <b.ending;
    }
    return false;
}

ll data[100001];
ll tree[400001];
ll n;
void treebuilding(ll node,ll b,ll e)
{
    if(b==e)
    {
        tree[node]=data[b];
        return;
    }
    ll left=2*node;
    ll right=2*node+1;
    ll mid=(b+e)/2;
    treebuilding(left,b,mid);
    treebuilding(right,mid+1,e);
    tree[node]=max(tree[left],tree[right]);
}
int query(ll node,ll b,ll e, ll i,ll j)
{
    //cout<<"node="<<node<<" b="<<b<<" e="<<e<<" i="<<i<<" j="<<j<<endl;
    if(e<i|| b>j)
    {
        //cout<<"out"<<endl;
        return 0;
    }
    if(i<=b && j>=e )
    {
        //cout<<"in "<<tree[node]<<endl;
        return tree[node];
    }
    //cout<<"partial"<<endl;
    ll left=2*node;
    ll right=2*node+1;
    ll mid=(b+e)/2;
    ll res1=query(left,b,mid,i,j);
    ll res2=query(right,mid+1,e,i,j);
    return max(res1,res2);
}


int main()
{
    ll test,t=1,i,j,s,e,n,p;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        vector<stay>guest;
        stay tmp;
        scanf("%lld %lld",&n,&p);
        for(i=1;i<=n;i++)
        {
            scanf("%lld %lld",&s,&e);
            tmp.start=s;
            tmp.ending=e;
            guest.push_back(tmp);
        }
        sort(guest.begin(),guest.end());


        for(i=0;i<guest.size();i++)
        {
             //cout<<"s="<<guest[i].start<<" "<<guest[i].ending<<endl;
            data[i+1]=guest[i].start;
            //cout<<"data="<<data[i+1]<<endl;
        }
        treebuilding(1,1,n);
        ll res=2e17;
        for(i=0;i<=guest.size()-p;i++)
        {
            ll mx_start=query(1,1,n,i+1,i+p);
            //cout<<"i="<<i<<" mx_start="<<mx_start<<endl;
            res=min(res,max(mx_start-guest[i].ending,0ll));
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
