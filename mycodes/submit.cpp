/*
ID: msdipu11
TASK: test
LANG: C++17
*/
#include <bits/stdc++.h>
using namespace std;

int main() {
    freopen("test.in","r",stdin);
    freopen("test.out","w",stdout);
    int a, b;
    cin >> a >> b;
    cout << a+b << endl;
    return 0;
}
