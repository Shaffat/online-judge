#include<bits/stdc++.h>

using namespace std;
typedef long long int ll;
ll bigmod(ll base,ll power,ll mod,vector<ll>&memory)
{
    if(power==0)
    {
        return 1;
    }
    if(power==1)
    {
        return base;
    }
    if(power<1e6)
    {
        if(memory[power]!=-1)
        return memory[power];
    }
    ll half=power/2;
    ll res;
    res=(bigmod(base,half,mod,memory)*bigmod(base,power-half,mod,memory))%mod;
    if(power<1e6)
    {
        memory[power]=res;
    }
    return res;
}

int main()
{
    ll b,p,m;
    while(scanf("%lld %lld %lld",&b,&p,&m)!=EOF)
          {
                vector<ll>v(1e6,-1);
                printf("%lld\n",bigmod(b,p,m,v));
          }
}
