#include<bits/stdc++.h>
using namespace std;

bool okRow(vector<vector<int> >&v)
{
    int i,j;
    for(i=0;i<v.size();i++)
    {
        for(j=1;j<v[i].size();j++)
        {
            if(v[i][j-1]>=v[i][j])
                return false;
        }
    }
    return true;
}

bool okCol(vector<vector<int> >&v)
{
    int i,j;
    for(i=0;i<v[0].size();i++)
    {
        for(j=1;j<v.size();j++)
        {
            if(v[j-1][i]>=v[j][i])
                return false;
        }
    }
    return true;
}

int main()
{
    int n,m,i,j,x,mn,mx;
    cin>>n>>m;
    vector<int>col(m);
    vector<vector<int> >v1(n,col);
    vector<vector<int> >v2(n,col);
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            cin>>x;
            v1[i][j]=x;
        }
    }
     for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
            cin>>x;
            v2[i][j]=x;
        }
    }
    for(i=0;i<n;i++)
    {
        for(j=0;j<m;j++)
        {
           mn=min(v1[i][j],v2[i][j]);
           mx=max(v1[i][j],v2[i][j]);
           v1[i][j]=mn;
           v2[i][j]=mx;
        }
    }



    if(okRow(v1) && okCol(v1) && okRow(v2) && okCol(v2))
        cout<<"Possible"<<endl;
    else
        cout<<"Impossible"<<endl;
}
