#include<bits/stdc++.h>

using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        double v1,v2,v3,a1,a2,dis,time1,time2,s1,s2;
        scanf("%lf %lf %lf %lf %lf",&v1,&v2,&v3,&a1,&a2);
        s1=(v1*v1)/(2*a1);
        s2=(v2*v2)/(2*a2);
        dis=s1+s2;
        if(a1==0)
        {
            time1=0;
        }
        else
        time1=v1/a1;
        if(a2==0)
        {
            time2=0;
        }
        else
        time2=v2/a2;
        printf("Case %d: %.10lf %.10lf\n",t,dis,max(time1,time2)*v3);
        t++;
    }
}
