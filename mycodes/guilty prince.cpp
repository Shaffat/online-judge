#include<bits/stdc++.h>
using namespace std;

struct road
{
    char value;
    int vis;
};

struct pos
{
    int row,column;

};
int main()
{
    int test,t=1;

    cin>>test;

    while(t<=test)
    {
        int w,h,irow,icolumn,counter=0,i,j;
        cin>>w>>h;

        road prince[h+10][w+10];

        for(i=1;i<=h;i++)
        {
            for(j=1;j<=w;j++)
            {
                scanf("%c",&prince[i][j].value);
                prince[i][j].vis=0;

                if(prince[i][j].value=='@')
                {
                    irow=i;
                    icolumn=j;
                }
            }
        }


        pos start;
        start.row=irow;
        start.column=icolumn;
        queue<pos>q;

        q.push(start);
        prince[irow][icolumn].vis=1;
        counter++;

        while(!q.empty())
        {
            pos current,temp;
            current.row=q.front().row;
            current.column=q.front().column;

            q.pop();
            temp.row=current.row-1;
            temp.column=current.column;
            if(!prince[temp.row][temp.column].vis && prince[temp.row][temp.column].value=='.'
               && temp.row>0 && temp.row<=h && temp.column>0 && temp.column<=w)
            {

                prince[temp.row][temp.column].vis=1;
                q.push(temp);
                counter++;
            }

            temp.row=current.row+1;
            temp.column=current.column;
            if(!prince[temp.row][temp.column].vis && prince[temp.row][temp.column].value=='.'
               && temp.row>0 && temp.row<=h && temp.column>0 && temp.column<=w)
            {

                prince[temp.row][temp.column].vis=1;
                q.push(temp);
                counter++;
            }

            temp.row=current.row;
            temp.column=current.column-1;
            if(!prince[temp.row][temp.column].vis && prince[temp.row][temp.column].value=='.'
               && temp.row>0 && temp.row<=h && temp.column>0 && temp.column<=w)
            {

                prince[temp.row][temp.column].vis=1;
                q.push(temp);
                counter++;
            }

            temp.row=current.row;
            temp.column=current.column+1;
            if(!prince[temp.row][temp.column].vis && prince[temp.row][temp.column].value=='.'
               && temp.row>0 && temp.row<=h && temp.column>0 && temp.column<=w)
            {

                prince[temp.row][temp.column].vis=1;
                q.push(temp);
                counter++;
            }

        }

        printf("Case %d: %d\n",t,counter);
          t++;
    }

}
