#include<bits/stdc++.h>
using namespace std;

void preprocess (vector<int>&pos,vector<int>&first,vector<int>&last){

    int i,j,x;
    for(i=0;i<pos.size();i++){
        x=pos[i];
        if(first[x]==-1){
            first[x]=i+1;
            last[x]=i+1;
        }else{
            last[x]=i+1;
        }
    }
    return;
}

int solve(int n,vector<int>&q){
    vector<int>first(n+1,2e9);
    vector<int>last(n+1,2e9);
    preprocess(q,first,last);
    int i,j,x,y,counter=0;
    for(i=1;i<=n;i++){
        for(j=i;j<=min(i+1,n);j++){
            x=i;
            y=j;
            //cout<<"x="<<i<<"y="<<j<<" first x="<<first[x]<<" last y="<<last[y]<<endl;
            if(x==y){
                if (first[x]==2e9){
                    //cout<<"doesnt exist"<<endl;
                    counter++;
                }
            }else if(first[x]>last[y] || first[x]==2e9){
                        counter++;
                        //cout<<"valid swap"<<endl;
                }
        }
    }
    return counter;
}

int main(){
    int n,k,i,j,res;
    scanf("%d %d",&n,&k);
    vector<int>v;
    for(i=1;i<=k;i++){
        scanf("%d",&j);
        v.push_back(j);
    }
    res=solve(n,v);
    printf("%d\n",res);
}
