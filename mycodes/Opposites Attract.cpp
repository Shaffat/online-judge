#include<bits/stdc++.h>
using namespace std;

int main()
{
    vector<int>col1(2,0);
    vector<vector<int> >rating(11,col1);
    int n,i,j;
    scanf("%d",&n);
    vector<vector<vector<int> > >cumulative_sum(n+1,rating);
    vector<int>persons(n+2);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        persons[i]=j;
        for(int k=0;k<=10;k++)
        {
            cumulative_sum[i][k][0]+=cumulative_sum[i-1][k][0];
            cumulative_sum[i][k][1]+=cumulative_sum[i-1][k][1];
        }
        if(j<0)
        {
            cumulative_sum[i][abs(j)][0]++;
        }
        else
            cumulative_sum[i][j][1]++;
    }
    long long int total=0,tmp;
    for(i=1;i<n;i++)
    {
        int cur=persons[i];
        int desired=-1*cur,sign;
        if(desired<0)
        {
            sign=0;
        }
        else
            sign=1;
        desired=abs(desired);
        tmp=cumulative_sum[n][desired][sign]-cumulative_sum[i][desired][sign];
        //cout<<"i="<<i<<" cur="<<cur<<" desire="<<desired<<" sign="<<sign<<" tmp="<<tmp<<endl;
        total+=tmp;

    }
    cout<<total<<endl;

}
