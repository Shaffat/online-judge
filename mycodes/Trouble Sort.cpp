#include<bits/stdc++.h>
using namespace std;

void troublesort(vector<int>&v)
{
    int done=0,i,j;
    while(!done)
    {
        done=1;
        for(i=0;i<v.size()-2;i++)
        {
            if(v[i]<v[i+2])
            {
                done=0;
            }
            int tmp=v[i];
            v[i]=v[i+2];
            v[i+2]=tmp;
        }
    }
    return;

}

int matchng(vector<int>&t,vector<int>&v)
{
    int nomatch=-1,i,j;
    for(i=0;i<v.size();i++)
    {
        if(v[i]!=t[i])
        {
            nomatch=i;
            break;
        }
    }
    return nomatch;
}

int main()
{
    int test,t=1,n,i,j,res;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        vector<int>trouble;
        vector<int>original;
        scanf("%d",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            trouble.push_back(j);
            original.push_back(j);
        }
        troublesort(trouble);
        sort(original.begin(),original.end());
        res=matchng(trouble,original);
        if(res==-1)
        {
            printf("Case #%d: OK\n",t);
        }
        else
        {
            printf("Case #%d: %d\n",t,res);
        }
    }
}
