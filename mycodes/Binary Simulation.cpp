#include<bits/stdc++.h>

using namespace std;

void update(int index,int n,int value,vector<int>&tree)
{
    while(index<=n)
    {
        tree[index]+=value;
        index+=index&(-index);
    }
}
int query(int index,vector<int>&tree)
{
    int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index&(-index);
    }
    return sum;
}

int main()
{
    int n,test,i,q,j,t=1,u,v;
    scanf("%d",&test);
    while(t<=test)
    {
        string s;
        cin>>s;
        n=s.size();
        vector<int>tree(n+1,0);
        vector<int>values(n+1,0);
        for(i=0;i<s.size();i++)
        {
            if(s[i]=='1')
            {
                values[i+1]=1;
            }
        }
        scanf("%d",&q);
        getchar();
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {

          char c;
          scanf("%c",&c);
          if(c=='I')
          {
              scanf("%d %d",&u,&v);
              update(u,n,1,tree);
              update(v+1,n,-1,tree);
          }
          else
          {
              scanf("%d",&u);
              int res=query(u,tree)+values[u];
              //cout<<"res="<<res<<endl;
              printf("%d\n",res%2);
          }
          getchar();

        }

        t++;
    }
}
