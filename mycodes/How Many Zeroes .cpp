#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>memory(10,-1);

ll solve(ll pos,ll restriction,ll zero,ll num)
{
    ll i,j,res=0,tmp,last,r=0;
    if(pos==0)
    {
        if(zero)
            return 1;
        else
            return 0;
    }
    if(memory[pos]!=-1 && restriction==0)
    {
        return memory[pos];
    }
    if(restriction)
    {
        last=num/pow(10,pos),r=0;
        last%=10;
    }
    else last=9;
    for(i=0;i<=last;i++)
    {
        if(i==last)
        {
            r=1;
        }
        if(i==0){
            if(zero>0)
            {
                res+=solve(pos-1,r,zero,num)+pow(10,pos);
            }
            else
                res+=solve(pos-1,r,zero,num);
        }
        else
            res+=solve(pos-1,r,1,num);
        cout<<"pos="<<pos<<" restrict="<<restriction<<" set digit="<<i<<" zero="<<zero<<" num="<<num<<" res="<<res<<endl;
    }
    if(restriction==0)
    {
        memory[pos]=res;
    }
    cout<<"pos="<<pos<<" restrict="<<restriction<<" zero="<<zero<<" num="<<num<<" res="<<res<<endl;
    return res;
}
int main()
{
    ll test,t,i,j,n,m,res1,res2,res;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&n,&m);
        if(n==0)
        {
            j=log10(m);
            res=solve(j,1,0,m);
        }
        else
        {
            n--;
            j=log10(n);
            res1=solve(j,1,0,n);
            j=log10(m);
            res2=solve(j,1,0,m);
            res=res2-res1;
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
