#include<bits/stdc++.h>
using namespace std;

struct node
{
    int id,flow;
};
int send_flow(int source,int sink,int n,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    int i,j,res=0;
    node cur;
    vector<int>path(n+1,-1);
    cur.id=source;
    cur.flow=1e9;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        //cout<<"node="<<cur.id<<" flow="<<cur.flow<<endl;
        q.pop();
        if(cur.id==sink)
        {
            res=cur.flow;
            break;
        }
        for(i=0;i<graph[cur.id].size();i++)
        {
            int nxt=graph[cur.id][i];
            if(path[nxt]==-1 && capacity[cur.id][nxt]>0)
            {
                path[nxt]=cur.id;
                node tmp;
                tmp.id=nxt;
                tmp.flow=min(cur.flow,capacity[cur.id][nxt]);
                q.push(tmp);
            }
        }

    }
    //cout<<"res="<<res<<endl;
    if(res>0)
    {
        int child,parent;
        child=sink;
        while(child!=source)
        {
            parent=path[child];
            capacity[parent][child]-=res;
            capacity[child][parent]+=res;
            child=parent;
        }
    }
    //cout<<"done\n";
    return res;
}


int main()
{
    //freopen("in.txt","r",stdin);
    //freopen("out1.txt","w",stdout);
    int test,t,i,j,u,v,n,m,c,d,b;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
//        for(i=1;i<=n+n+1;i++)
//            cout<<i<<endl;
        vector<vector<int> >connection(250);
        vector<int>col(250,0);
        vector<vector<int> >capacity(250,col);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&c);
            connection[i].push_back(n+i+1);
            connection[n+i+1].push_back(i);
            capacity[i][n+i+1]=c;
            capacity[n+i+1][i]=0;
            //cout<<i<<" "<<n+i+1<<endl;
        }
        scanf("%d",&m);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&c);
            connection[u+n+1].push_back(v);
            connection[v].push_back(u+n+1);
            //cout<<u+n+1<<" "<<v<<endl;;
            capacity[u+n+1][v]=c;
            capacity[v][u+n+1]=0;
        }

        scanf("%d %d",&b,&d);
        for(i=1;i<=b;i++)
        {
            scanf("%d",&u);
            connection[0].push_back(u);
            capacity[0][u]=1e9;
            //cout<<"0 "<<u<<endl;
        }
        for(i=1;i<=d;i++)
        {
            scanf("%d",&v);
            connection[v+n+1].push_back(n+1);
            capacity[v+n+1][n+1]=1e9;
            //cout<<v+n+1<<" "<<n+1<<endl;

        }
        int mxflow=0,flow=0;
        while(flow=send_flow(0,n+1,250,connection,capacity))
        {
            //cout<<flow<<endl;
            mxflow+=flow;
        }
        printf("Case %d: %d\n",t,mxflow);
    }

}
