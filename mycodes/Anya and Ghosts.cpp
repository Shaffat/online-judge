#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,m,t,r,k,candle=0,endtime=-1;
    scanf("%d %d %d",&m,&t,&r);
    vector<int>ghosts;
    for(i=1;i<=m;i++)
    {
        scanf("%d",&j);
        ghosts.push_back(j);
    }
    if(t<r)
    {
        cout<<"-1"<<endl;
        return 0;
    }
    vector<int>candels(30000,0);
    int total=0;
    for(i=0;i<ghosts.size();i++)
    {
        if(candels[ghosts[i]]>=r)
        {
            continue;
        }
        int candelsneed=r-candels[ghosts[i]];
        total+=candelsneed;
        candelsneed--;
        for(j=0;j<=candelsneed;j++)
        {
            int start=ghosts[i],endtime=ghosts[i]+t-1;
            //cout<<"start="<<start<<" end="<<endtime<<endl;
            for(k=start;k<=endtime-j;k++)
            {
                candels[k]++;
            }
        }

    }
    cout<<total<<endl;
}
