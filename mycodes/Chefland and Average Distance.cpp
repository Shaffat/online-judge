#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

int manhattandis(pos a, pos b)
{
    return abs(a.x-b.x)+abs(a.y-b.y);
}


int main()
{
    int test,t,n,m,i,j;
    string s;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        vector<pos>points;
        pos tmp;
        scanf("%d %d",&n,&m);
        for(i=1;i<=n;i++)
        {
            cin>>s;
            for(j=0;j<m;j++)
            {
                if(s[j]=='1')
                {
                    tmp.x=i;
                    tmp.y=j+1;
                    points.push_back(tmp);
                }
            }
        }
        vector<int>dis(n+m,0);
        for(i=0;i<points.size()-1;i++)
        {
            for(j=i+1;j<points.size();j++)
            {
                int res=manhattandis(points[i],points[j]);
                dis[res]++;
            }
        }
        for(i=1;i<=n+m-2;i++)
        {
            printf("%d ",dis[i]);
        }
        printf("\n");
    }

}
