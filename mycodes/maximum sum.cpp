#include<bits/stdc++.h>

using namespace std;

int main()
{
      int n,i,j,k,m,sum=-256;
      scanf("%d",&n);
      vector<int>initial(n+1,0);
      vector<vector<int> >row(n+1,initial);
      vector<vector<int> >column(n+1,initial);
      for(i=1;i<=n;i++)
      {
          for(j=1;j<=n;j++)
          {
              scanf("%d",&k);
              sum=max(k,sum);
              row[i][j]=row[i][j-1]+k;
              column[j][i]=column[j][i-1]+k;
          }
      }

      for(i=1;i<n;i++)
      {
          for(j=i+1;j<=n;j++)
          {
              for(k=1;k<n;k++)
              {

                 for(m=k+1;m<=n;m++)
                 {
                     int result=0;
                     for(int f=i;f<=j;f++)
                     {
                         result+=row[f][m]-row[f][k-1];
                     }
                     //cout<<"for row "<<j<<" to "<<i<<" column "<<k<<" to "<<m<<" sum="<<result<<endl;
                     sum=max(sum,result);
                 }
              }
          }
      }
      cout<<sum<<endl;
}

