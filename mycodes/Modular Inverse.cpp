#include<bits/stdc++.h>
using namespace std;

#define ll long long int
ll GCD(ll a,ll b)
{
    while(a%b!=0)
    {
        ll temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

ll find_mod_inverse(ll A,ll mod )
{
    int i,j,a=A,b=mod,x,xMinusOne,xMinusTwo,tmp,k,r;
    xMinusOne=0;
    xMinusTwo=1;
    while(a%b!=0)
    {
        k=a/b;
        r=a%b;
        a=b;
        b=r;
        x=xMinusTwo-(k*xMinusOne);
        xMinusTwo=xMinusOne;
        xMinusOne=x;
    }
    return (x+mod)%mod;
}

int main()
{
    ll test,t,a,b;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&a,&b);
        ll res;
        if(GCD(a,b)==1)
        {
            res=find_mod_inverse(a,b);
            if(b==1)
                res=1;
            if(res!=0)
            printf("%lld\n",res);
            else
            {
                printf("Not Exist\n");
            }
        }
        else
        {
            printf("Not Exist\n");
        }

    }
}



