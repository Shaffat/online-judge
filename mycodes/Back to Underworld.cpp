#include<bits/stdc++.h>
using namespace std;

int bfs(int root,vector<vector<int> >&connection,vector<int>&vis,vector<int>&dis)
{
    int i,j,odd=0,even=1;
    queue<int>q;
    q.push(root);
    vis[root]=1;
    dis[root]=0;
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            int child=connection[cur][i];
            if(!vis[child])
            {
                q.push(child);
                dis[child]=dis[cur]+1;
                vis[child]=1;
                if(dis[child]%2==0)
                {
                    even++;
                }
                else
                    odd++;
            }
        }
    }
    return max(even,odd);
}

int solve(vector<int>&nodes,vector<vector<int> >&connection)
{
    vector<int>vis(2e5,0);
    vector<int>dis(2e5,0);
    int i,j,res=0;
    for(i=0;i<nodes.size();i++)
    {
        if(!vis[nodes[i]])
        res+=bfs(nodes[i],connection,vis,dis);
    }
    return res;
}

int main()
{
    int i,j,u,v,t,m,n,test,res;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        set<int>s;
        vector<int>nodes;
        vector<vector<int> >connection(20001);
        scanf("%d",&m);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
            int cur=s.size();
            s.insert(u);
            if(cur!=s.size())
            {
                nodes.push_back(u);
            }
            cur=s.size();
            s.insert(v);
            if(cur!=s.size())
            {
                nodes.push_back(v);
            }
        }
        res=solve(nodes,connection);
        printf("Case %d: %d\n",t,res);
    }
}
