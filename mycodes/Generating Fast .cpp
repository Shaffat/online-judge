#include<iostream>
#include<algorithm>
using namespace std;

int main()

{
    ios_base::sync_with_stdio(0);
    int n,i,j;
    cin>>n;
    string s;
    while(n--)
    {

        cin>>s;
        sort(s.begin(),s.end());
        cout<<s<<endl;
        while(next_permutation(s.begin(),s.end()))
        {
            cout<<s<<endl;
        }

        cout<<endl;
    }
}
