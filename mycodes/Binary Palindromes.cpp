
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int solve(int pos,int one,int zero,vector<int>&lens,vector<vector<vector<int> > >&memory)
{
    if(pos>=lens.size())
    {
        return 0;
    }
    if(memory[pos][one][zero]!=-1)
    {
        return memory[pos][one][zero];
    }
    int res1=0,res2=0,res3=0,res4=0,res5=0,i,j;
    if(lens[pos]<=one)
    {
        res1=solve(pos+1,one-lens[pos],zero,lens,memory)+1;
    }
    if(lens[pos]<=zero)
    {
        res2=solve(pos+1,one,zero-lens[pos],lens,memory)+1;
    }
    if(lens[pos]%2)
    {
        for(i=1;i<=one;i+=2)
        {
            int zero_need=lens[pos]-i;
            if(zero_need<=zero)
            {
                int tmp=solve(pos+1,one-i,zero-zero_need,lens,memory)+1;
                res3=max(res3,tmp);
            }
        }
        for(i=1;i<=zero;i+=2)
        {
            int one_need=lens[pos]-i;
            if(one_need<=one)
            {
                int tmp=solve(pos+1,one-one_need,zero-i,lens,memory)+1;
                res4=max(res4,tmp);
            }
        }
    }
    else
    {
        for(i=2;i<=one;i+=2)
        {
            int zero_need=lens[pos]-i;
            if(zero_need<=zero)
            {
                int tmp=solve(pos+1,one-i,zero-zero_need,lens,memory)+1;
                res3=max(res3,tmp);
            }
        }
        for(i=2;i<=zero;i+=2)
        {
            int one_need=lens[pos]-i;
            if(one_need<=one)
            {
                int tmp=solve(pos+1,one-one_need,zero-i,lens,memory)+1;
                res4=max(res4,tmp);
            }
        }
    }
    res5=solve(pos+1,one,zero,lens,memory);
    return memory[pos][one][zero]=max4(max(res1,res2),res3,res4,res5);
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t,n,i,j;
    cin>>test;
    while(test--)
    {
        string s;
        cin>>n;
        vector<int>lens;

        int one=0,zero=0,res=0;
        for(i=1;i<=n;i++)
        {
            cin>>s;
            lens.push_back(s.size());
            for(j=0;j<s.size();j++)
            {
                if(s[i]=='0')
                    zero++;
                else
                    one++;
            }
        }
        i=0;
        sort(lens.begin(),lens.end());
        for(i=0;i<lens.size();i++)
        {
            cout<<"at "<<i<<" len is "<<lens[i]<<" one="<<one<<" zero="<<zero<<endl;
            if(one>=lens[i])
            {
                one-=lens[i];
                res++;
            }
            else if(zero>=lens[i])
            {
                zero-=lens[i];
                res++;
            }
            else
            {
                if(lens[i]%2)
                {
                    int oddzero,evenOne,evenzero,oddone;
                    if(zero%2)
                    {
                        oddzero=zero;
                        evenzero=zero-1;
                    }
                    else
                    {
                        oddzero=zero-1;
                        evenzero=zero;
                    }
                    if(one%2)
                    {
                        evenOne=one-1;
                        oddone=one;
                    }
                    else
                    {
                        evenOne=one;
                        oddone=one-1;
                    }
                    if(oddone+evenzero>=lens[i])
                    {
                        res++;
                        one-=oddone;
                        zero-=evenzero;
                    }
                    else if(oddzero+evenOne>=lens[i])
                    {
                        res++;
                        one-=evenOne;
                        zero-=oddzero;
                    }
                    else
                        break;
                }
                else
                {
                    int oddzero,evenOne,evenzero,oddone;
                    if(zero%2)
                    {
                        oddzero=zero;
                        evenzero=max(zero-1,0);
                    }
                    else
                    {
                        oddzero=max(zero-1,0);
                        evenzero=zero;
                    }
                    if(one%2)
                    {
                        evenOne=max(one-1,0);
                        oddone=one;
                    }
                    else
                    {
                        evenOne=one;
                        oddone=max(one-1,0);
                    }
                    if(evenOne+evenzero>=lens[i])
                    {
                        one-=evenOne;
                        zero-=evenzero;
                    }
                    else
                        break;
                }
            }
        }

        cout<<res<<endl;
    }
}
