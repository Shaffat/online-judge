#include<bits/stdc++.h>
using namespace std;

vector<int>primes;
vector<bool>p(2e6,0);
int m=1;

void sieve()
{
    int i,j;
    for(i=4;i<=1e6;i+=2)
        p[i]=1;
    for(i=3;i<=sqrt(1e6);i++)
    {
        if(p[i]==0)
        {
            for(j=i*i;j<=2e5;j+=i)
                p[j]=1;
        }
    }
    primes.push_back(2);
    for(i=3;i<=1e6;i+=2)
        if(p[i]==0) primes.push_back(i);
    return;
}

int solve(int n)
{
    if(n==1) return 0;
    int i,j,power=0,res=0;
    set<int>s;
    //cout<<"n="<<n<<endl;
    for(i=0;i<primes.size();i++)
    {
        if(primes[i]>n) break;
        if(n%primes[i]==0)
        {
            //cout<<"p="<<primes[i]<<endl;
            m*=primes[i];
            //cout<<"m="<<m<<endl;
            int counter=0;
            while(n%primes[i]==0)
            {
                counter++;
                n/=primes[i];
            }
            power=max(power,counter);
            s.insert(counter);
            //cout<<"counter="<<counter<<" power="<<power<<endl;
        }
    }
    //cout<<"power="<<power<<" primes="<<s.size()<<endl;
    if(s.size()>1||(ceil(log2(power))!=floor(log2(power)))){
        //cout<<"multiply"<<endl;
        res++;
    }
    //cout<<"log poweer="<<log2(power)<<endl;
    res+=ceil(log2(power));
    return res;
}

int main()
{
    sieve();
    int n,res;
    cin>>n;
    res=solve(n);
    cout<<m<<" "<<res;
}
