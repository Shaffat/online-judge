#include<bits/stdc++.h>
using namespace std;

#define ll  long long int

ll solve(ll n,ll k,ll pos,ll remains ,ll mod,vector<vector<vector<ll> > >&memory)
{
    //cout<<"pos="<<pos<<" remains="<<remains<<" mod="<<mod<<endl;
    if(remains<0) return 0;
    if(pos<0)
    {
        if(mod ==0 && remains==0)
            return 1;
        return 0;
    }
    ll res1=0,res2=0,nwmod;
    if(memory[pos][remains][mod]!=-1)
    {
        return memory[pos][remains][mod];
    }
    unsigned long long int tmp=(1ll<<pos);
    ll cur=tmp%k;
    nwmod=(cur+mod)%k;
    res1=solve(n,k,pos-1,remains-1,nwmod,memory);
    res2=solve(n,k,pos-1,remains,mod,memory);
    return memory[pos][remains][mod]=res1+res2;

}

int main()
{
    ll n,k,test,t,i,j,res;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&n,&k);
        vector<ll>col1(101,-1);
        vector<vector<ll> >col2(40,col1);
        vector<vector<vector<ll> > >memory(70,col2);
        if(k!=0 && n%2==0)
        {
            ll mod=((1ll<<(n-1))%k);
            res=solve(n-1,k,n-2,(n/2)-1,mod,memory);
        }
        else
            res=0;
        printf("Case %lld: %lld\n",t,res);
    }
}
