
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

struct status
{
    ll id,f,s;
};

struct edge
{
    ll u,v;
};

bool operator <(status a, status b)
{
    if(a.f!=b.f)
        return a.f>b.f;
    return false;
}

bool operator <(edge a, edge b)
{
    if(a.u!=b.u)
        return a.u<b.u;
    return false;
}

ll timer=0;
void topSort(ll node,vll &connection,vl &vis,vector<status>&Stopwatch)
{
    //cout<<"node="<<node<<endl;
    ll i,j,cur=node,nxt;
    Stopwatch[node].id=node;
    Stopwatch[node].s=timer;
    timer++;
    //cout<<"here1 sz="<<connection[node].size()<<endl;
    for(i=0;i<connection[node].size();i++)
    {
        nxt=connection[node][i];
        if(!vis[nxt])
        {
            vis[nxt]=1;
            topSort(nxt,connection,vis,Stopwatch);
        }
    }
    //cout<<"here"<<endl;
    Stopwatch[cur].f=timer;
    timer++;
    //cout<<"node "<<node<<" finished"<<endl;
    return;
}

void SCC(ll node,ll p, vll &TransposeGraph,vl &parent)
{
    //cout<<"cur="<<node<<endl;
    ll i,j,cur=node,nxt;
    for(i=0;i<TransposeGraph[cur].size();i++)
    {
        //cout<<"inside "<<endl;
        nxt=TransposeGraph[cur][i];
        //cout<<"nxt="<<nxt<<" for parent="<<p<<" cur="<<cur<<" i="<<i<<endl;
        if(parent[nxt]==-1)
        {
            parent[nxt]=p;
            SCC(nxt,p,TransposeGraph,parent);
        }
    }
    return;
}


void ReformSevenKingdom(ll n,vll &oldmap,vll &oldcost,vll &newmap,vll &newcost,vl &parent)
{
    ll i,j;
    map<edge,ll>posfinder;
    vector<set<int> >uniquecity(n+1);
    FOR(i,1,n)
    {
        for(j=0;j<oldmap[i].size();j++)
        {
            ll u,v,tmp,child,edgecost;
            child=oldmap[i][j];
            edgecost=oldcost[i][j];
            u=parent[i];
            v=parent[child];
            if(u!=v)
            {
                tmp=uniquecity[u].size();
                uniquecity[u].insert(v);
                if(tmp!=uniquecity[u].size())
                {
                    edge e;
                    e.u=u;
                    e.v=v;
                    posfinder[e]=newmap[u].size();
                    newmap[u].push_back(v);
                    newcost[u].push_back(edgecost);
                }
                else
                {
                    ll tmppos;
                    edge e;
                    e.u=u;
                    e.v=v;
                    tmppos=posfinder[e];
                    newcost[u][tmppos]=max(newcost[u][tmppos],edgecost);
                }
            }
        }
    }
    return;
}

ll query(ll node,vll &graph,vll &cost,vl &memory)
{
    if(memory[node]!=-1)
    {
        return memory[node];
    }
    ll i,j,cur,nxt,res=0;
    for(i=0;i<graph[node].size();i++)
    {
        nxt=graph[node][i];
        ll tmp=query(nxt,graph,cost,memory)+cost[node][i];
        res=max(res,tmp);
    }
    return memory[node]=res;
}

int main()
{
    ll i,j,n,m,q,u,v,w;
    sc3(n,m,q);
    vll connection(n+1);
    vll transpose(n+1);
    vll newconnection(n+1);
    vll cost(n+1);
    vll newcost(n+1);
    vl parent(n+1,-1);
    vl memory(n+1,-1);
    vl vis(n+1,0);
    vector<status>Stopwatch(n+1);
    FOR(i,1,m)
    {
        sc3(u,v,w);
        connection[u].push_back(v);
        cost[u].push_back(w);
        transpose[v].push_back(u);
    }
    //cout<<"Topsort starts"<<endl;
    FOR(i,1,n)
    {
        if(vis[i]==0)
        {
            //cout<<"root="<<i<<endl;
            vis[i]=1;
            topSort(i,connection,vis,Stopwatch);
        }
    }
    //cout<<"Topsort finshed"<<endl;

    Stopwatch[0].id=0;
    Stopwatch[0].s=1e9;
    Stopwatch[0].f=1e9;
    sort(Stopwatch.begin(),Stopwatch.end());
//    cout<<"Stopwatch sorted"<<endl;
//    cout<<"Finding SCC"<<endl;
    FOR(i,1,Stopwatch.size()-1)
    {
        ll id=Stopwatch[i].id;
        if(parent[id]==-1)
        {
            //cout<<"root="<<id<<endl;
            parent[id]=id;
            SCC(id,id,transpose,parent);
        }
    }

    ReformSevenKingdom(n,connection,cost,newconnection,newcost,parent);

    FOR(i,1,q)
    {
        ll x;
        sc1(x);
        x=parent[x];
        ll res=query(x,newconnection,newcost,memory);
        printf("%lld\n",res);
    }

}
