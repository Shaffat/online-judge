#include<bits/stdc++.h>
using namespace std;

void op_s(long long int n,vector<long long int>&v)
{
    int i;
    for(i=0;i<v.size();i++)
    {
        v[i]+=n;
    }
    return;
}

void op_m(long long int n,vector<long long int>&v)
{
    int i;
    for(i=0;i<v.size();i++)
    {
        v[i]*=n;
    }
    return;
}
void op_d(long long int n,vector<long long int>&v)
{
    int i;
    for(i=0;i<v.size();i++)
    {
        v[i]/=n;
    }
    return;
}


int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        long long int n,m,i,j,k,l;
        vector<long long int>v;
        scanf("%lld %lld",&n,&m);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            v.push_back(j);
        }
        for(i=1;i<=m;i++)
        {
            char c;
            scanf(" %c",&c);
            if(c=='S')
            {
                scanf("%lld",&k);
                op_s(k,v);
            }
            else if(c=='M')
            {
                scanf("%lld",&k);
                op_m(k,v);
            }
            else if(c=='D')
            {
                scanf("%lld",&k);
                op_d(k,v);
            }
            else if(c=='R')
            {
                reverse(v.begin(),v.end());
            }
            else
            {
                scanf("%lld %lld",&k,&l);
                long long int tmp=v[k];
                v[k]=v[l];
                v[l]=tmp;
            }
        }
        printf("Case %d:\n",t);
        for(i=0;i<v.size();i++)
        {
            if(i==v.size()-1)
            {
                printf("%lld\n",v[i]);
            }
            else
                printf("%lld ",v[i]);
        }
    }

}
