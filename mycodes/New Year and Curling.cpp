#include<bits/stdc++.h>
using namespace std;


bool intersect(int  x1,int  x2,double y1,double y2,double r)
{
    //cout<<"dis="<<((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))<<endl;
    if((((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))<=(r+r)*(r+r)))
    {
        return true;
    }
    return false;
}

int main()
{
   vector<int>xaxis;

   vector<double>yaxis(10001,-1000);
   int i,j,n,r;
   scanf("%d %d",&n,&r);
   for(i=1;i<=n;i++)
   {
       int l;
       scanf("%d",&l);
       xaxis.push_back(l);
   }
   for(i=0;i<xaxis.size();i++)
   {
       double first,last,mid,res,x1,x2,y1,y2;
       bool chk=0;
       first=r;
       last=3e6;
       while(first<=last)
       {
           cout<<first<<" and "<<last<<endl;
           if(chk)
           {
               break;
           }
           if(first==last || abs(first-last)<1e-6)
           {
               chk=1;
           }
           mid=(first+last)/2;
           int ok=1;
           int touch=0;
           for(j=max(0,xaxis[i]-r);j<=min(xaxis[i]+r,1000000);j++)
           {
               //cout<<"calling"<<endl;
               x1=xaxis[i];
               x2=j;
               y1=mid;
               y2=yaxis[xaxis[i]];
               if((((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))<(r+r)*(r+r)))
               {
                   ok=0;
               }
               if((((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))==(r+r)*(r+r)))
               {
                   touch=1;
               }
           }
           if(ok)
           {   //out<<"true"<<endl;
               last=mid;
               res=mid;
               if(touch)
               {
                   cout<<"touch"<<endl;
                   break;
               }
           }
           else
            first=mid;

       }
       //cout<<"i am done"<<endl;
       yaxis[xaxis[i]]=res;
       printf("%.7lf ",res);
   }
}
