#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int test,t,n,p,mn,avg,cost,last,ans,i,j;
    scanf("%lld",&test);
    for(t=1;t<=test;t++){
        scanf("%lld %lld",&n,&p);
        vector<int>skills;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            skills.push_back(j);
        }
        sort(skills.begin(),skills.end());
        mn=0;
        for(i=0;i<p;i++)
        {
            mn=max(mn,skills[i]);
        }
        cost=0;
        //cout<<"mn="<<mn<<endl;
        for(i=0;i<p;i++){
            cost+=mn-skills[i];
        }
        ans=cost;
        //cout<<"cost="<<cost<<endl;
        last=mn-skills[0];
        for(i=p;i<n;i++){
            int newMn=max(skills[i],mn);
            //cout<<"mn="<<mn<<" newmn="<<newMn<<" remove cost="<<mn-skills[i-p]<<endl;
            int newCost=(newMn-mn)*(p-1)+(newMn-skills[i])+cost-(mn-skills[i-p]);
            //cout<<"newcost="<<newCost<<endl;
            ans=min(newCost,cost);
            cost=newCost;
            mn=newMn;
        }
        printf("Case #%lld: %lld\n",t,ans);
    }
}
