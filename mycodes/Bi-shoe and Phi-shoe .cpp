#include<bits/stdc++.h>

using namespace std;

vector<int>primes;
void sieve()
{
    int i,j;
    vector<int>ar(2001,0);
    for(i=4;i<=2000;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(2000);i+=2)
    {
        for(j=i*i;j<=2000;j+=i)
        {
            ar[j]=i;
        }
    }
    primes.push_back(2);
    for(i=3;i<=2000;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
    return;
}

int coprime_counter(int n)
{
    long long int i,j,up=n,down=1;
    for(i=0;i<primes.size();i++)
    {
        //cout<<"p="<<primes[i]<<endl;
        if(primes[i]>n || n==1)
        {
            break;
        }
        if(n%primes[i]==0)
        {
            while(n%primes[i]==0)
            {
                n/=primes[i];
            }
            up*=primes[i]-1;
            down*=primes[i];
            //cout<<"up="<<up<<" down="<<down<<" n="<<n<<endl;
        }
    }
    //cout<<"up="<<up<<" down="<<down<<" n="<<n<<endl;
    if(n!=1)
    {
        up*=n-1;
        down*=n;
    }
    long long int total=up/down;
    return total;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    sieve();
    int i,j,k,test,t=1;
    vector<int>res(2000001,1e9);
    for(i=2;i<=1001000;i++)
    {
        //cout<<"i="<<i;
        j=coprime_counter(i);
        //cout<<" j="<<j<<endl;
        res[j]=min(i,res[j]);
    }
    for(i=1001000;i>=1;i--)
    {
        res[i]=min(res[i],res[i+1]);
    }
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        long long int n,total=0;
        scanf("%lld",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
//            cout<<j<<"="<<res[j]<<endl;
            total+=res[j];
        }
        printf("Case %d: %lld Xukha\n",t,total);
    }

}
