#include<bits/stdc++.h>
using namespace std;
vector<int>primes;
vector<bool>ar(1000000,0);
void sieve()
{
    int i,j;
    ar[1]=1;
    for(i=4;i<1e6;i+=2)
    {
        ar[i]=1;
    }
    for(i=3;i<=sqrt(1e6);i+=2)
    {
        if(ar[i]==0)
        {
            for(j=i*i;j<1e6;j+=i)
            {
                ar[j]=1;

            }
        }
    }
    primes.push_back(2);
    for(i=3;i<1e6;i+=2)
    {
        if(ar[i]==0)
        {
            primes.push_back(i);
        }
    }
}

int NumberOfDivisor(int n)
{
    int i,j;
    int divisor=1;
    for(i=0;i<primes.size();i++)
    {
        if(n==1)
        {
            break;
        }
        if(primes[i]>n)
        {
            break;
        }
        int count=1,p=primes[i];
        while(n%p==0)
        {
            n/=p;
            count++;
        }
        //cout<<p<<" ="<<count<<" & n="<<n<<endl;
        divisor*=count;
    }
    return divisor;
}

int mx;
int solve(int up,int low)
{
    int i,j,number;
    mx=-1;
    for(i=low;i<=up;i++)
    {
        int cur=NumberOfDivisor(i);
        //cout<<"for i="<<i<<" div="<<cur<<endl;
        if(mx<cur)
        {
            mx=cur;
            number=i;
        }
    }
    return number;
}

int main()
{
    sieve();
    int test,i,j;
    scanf("%d",&test);
    while(test--)
    {
        int l,u;
        scanf("%d %d",&l,&u);
        int res=solve(u,l);
        printf("Between %d and %d, %d has a maximum of %d divisors.\n",l,u,res,mx);
    }
}



