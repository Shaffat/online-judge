
#include<bits/stdc++.h>
using namespace std;

int less_than(int time,int s,int t,vector<int>&people)
{
    //cout<<"less"<<endl;
    int i,j,first,last,n=people.size()-1;
    first=s-time;
    last=first+t-s;
    return people[last]-people[first];

}

int in_between(int time,int s,int t,vector<int>&people)
{
    //cout<<"between"<<endl;
    int i,j,first,last,n=people.size()-1,total,taken=t-time;
    total=people[t-time];
    if(taken<t-s)
    {
        int remain=t-s-taken;
        total+=people[n]-people[n-remain];
    }
    return total;
}
int greater_or_equal(int time,int s,int t,vector<int>&people)
{
    //cout<<"greater"<<endl;
    int first,last,n=people.size()-1,total=0;
    first=n-time+s;
    last=first+t-s;
    return people[last]-people[first];
}

int solve(int s,int f,vector<int>&people){
    int i,j,mx=-1,cur,res,start,last,remain,n=people.size()-1;
    for(i=1;i<=n;i++)
    {
        if(i<s)
        {
            cur=less_than(i,s,f,people);
            if(cur>mx)
            {
                mx=cur;
                res=i;
            }
        }
        else if(i>=s && i<f)
            {
                cur=in_between(i,s,f,people);
                if(cur>mx)
                {
                    mx=cur;
                    res=i;
                }
            }
        else
            {
                cur=greater_or_equal(i,s,f,people);
                if(cur>mx)
                {
                    mx=cur;
                    res=i;
                }
            }
            //cout<<"for i="<<i<<" cur="<<cur<<endl;
    }
    return res;
}

int main()
{
    int i,j,n,s,f,res;
    scanf("%d",&n);
    vector<int>people(n+1,0);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        people[i]=people[i-1]+j;
    }
    scanf("%d %d",&s,&f);
    res=solve(s,f,people);
    printf("%d\n",res);
}
