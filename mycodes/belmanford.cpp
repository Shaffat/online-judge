#include<bits/stdc++.h>

using namespace std;

struct edges
{
    int u,v,w;
};

int main()
{
    int i,j,n,e;
    scanf("%d %d",&n,&e);
    vector< edges >edge;
    vector<int>dis(n+1,2e9);
    int parent[n+1];
    for(i=1;i<=n;i++)
    {
        parent[i]=i;
    }
    for(i=1;i<=e;i++)
    {
        int u,v,w;
        scanf("%d %d %d",&u,&v,&w);
        edges nw;
        nw.u=u;
        nw.v=v;
        nw.w=w;
        edge.push_back(nw);
    }
    for(i=1;i<n;i++)
    {
        for(j=0;j<edge.size();j++)
        {
            int u,v,w;
            u=edge[j].u;
            v=edge[j].v;
            w=edge[j].w;
            if(dis[v]>(dis[u]+w))
            {
                dis[v]=dis[u]+w;
                parent[v]=u;
            }
        }
    }
    int chk=0;
    for(j=0;j<edge.size();j++)
    {
        int u,v,w;
            u=edge[j].u;
            v=edge[j].v;
            w=edge[j].w;
            if(dis[v]>(dis[u]+w))
            {
                chk=1;
                break;
            }
    }
    if(chk)
    {
        cout<<"negetive cycle exist"<<endl;
    }

}
