#include<bits/stdc++.h>
using namespace std;
struct res
{
    int e,time;
};

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int c=a%b;
        a=b;
        b=c;
    }
    return b;
}

vector<int>prime;
res solve(vector<int>&doors)
{
    int i,j,left_E=doors.size();
    res total;
    total.e=0;
    total.time=0;
    for(i=0;i<doors.size();i++)
    {
        if(doors[i]<0)
        {
            ///if time negetive i am back at begining so rightside e++;
            total.e++;
            total.time+=abs(doors[i]);
        }
        else
            total.time+=doors[i];

    }
    if(total.e==left_E)
    {
        total.e=0;
        return total;
    }
    else
    {
        left_E-=total.e;
        total.e=left_E;
        int gcd=GCD(total.time,total.e);
        total.e/=gcd;
        total.time/=gcd;
        return total;
    }

}


int main()
{
    int test,t,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n;
        vector<int>doors;
        scanf("%d",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            doors.push_back(j);
        }
        res result=solve(doors);
        if(result.e==0)
        {
            printf("Case %d: inf\n",t);
        }
        else
        {
            printf("Case %d: %d/%d\n",t,result.time,result.e);
        }
    }

}
