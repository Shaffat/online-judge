#include<bits/stdc++.h>
using namespace std;


int count_vowel(string &s)
{
    int i,j,counter=0;
    for(i=0;i<s.size();i++)
    {
        if(s[i]=='a'||s[i]=='e'||s[i]=='i'||s[i]=='o'||s[i]=='u')
        {
            counter++;
        }
    }
    return counter;
}

int main()
{
    string s;
    int n,f=1;
    getline(cin,s);
    n=count_vowel(s);
    if(n!=5)
    {
        f=0;
    }
     getline(cin,s);
    n=count_vowel(s);
    if(n!=7)
    {
        f=0;
    }
    getline(cin,s);
    n=count_vowel(s);
    if(n!=5)
    {
        f=0;
    }

    if(f)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}
