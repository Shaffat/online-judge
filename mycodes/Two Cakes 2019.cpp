#include<bits/stdc++.h>
using namespace std;
vector<long long int>col(4,-1);
vector<vector<long long int> >memory(1e5 +1,col);

long long int n;


long long int solve(long long int idx,long long int comb,long long int x1,long long int x2,vector<vector<long long int> >&cakes)
{
    //cout<<"id="<<idx<<" x1="<<x1<<" x2="<<x2<<endl;
    if(idx>n)
        return 0;
    if(memory[idx][comb]!=-1)
        return memory[idx][comb];
    long long int i,j,res1,res2,d1=cakes[idx][0],d2=cakes[idx][1];
    res1=solve(idx+1,2,d1,d2,cakes)+abs(x1-d1)+abs(x2-d2);
    res2=solve(idx+1,1,d2,d1,cakes)+abs(x1-d2)+abs(x2-d1);
    return memory[idx][comb]=min(res1,res2);

}

int main()
{
    long long int i,j;
    scanf("%lld",&n);

    vector<vector<long long int> >v(n + 1);
    for(i=1;i<=2*n;i++)
    {
        scanf("%lld",&j);
        v[j].push_back(i);
    }
    long long int res=solve(1,0,1,1,v);
    cout<<res<<endl;
}
