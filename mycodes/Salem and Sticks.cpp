#include<bits/stdc++.h>
using namespace std;

int cost(int t, vector<int>&v)
{
    int i,j,res=0;
    for(i=0;i<v.size();i++)
    {
        if(abs(v[i]-t)>1)
        {
            if(v[i]>t)
            {
                res+=v[i]-t-1;
            }
            else
                res+=t-v[i]-1;
        }
    }
    return res;
}


int main()
{
    int n,i,j,k,res=-1,c=2e9;
    scanf("%d",&n);
    vector<int>v;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v.push_back(j);
    }
    for(i=1;i<=200;i++)
    {
        int tmp=cost(i,v);
        //cout<<"i="<<i<<" tmp="<<tmp<<endl;
        if(c>tmp)
        {

            c=tmp;
            res=i;
        }
    }
    cout<<res<<" "<<c<<endl;
}
