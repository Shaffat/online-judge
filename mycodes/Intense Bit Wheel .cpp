#include<bits/stdc++.h>

using namespace std;

#define ll long long int

int main()
{
    ll n,m,i,j,number,k;

    scanf("%lld %lld",&n,&m);

    while(m--)
    {
        scanf("%lld %lld",&number,&k);
        queue<ll>q;
        for(i=n-1;i>=0;i--)
        {
           if((number&(1ll<<i))==0){
                q.push(0);
           }
           else
            q.push(1);
        }
        k%=n;
        while(k--){
            ll tmp=q.front();
            q.pop();
            q.push(tmp);
        }
        ll res=0,x=n-1;
        while(!q.empty()){
            if(q.front()){
                res+=(1ll<<x);
            }
            q.pop();
            x--;
        }
        printf("%lld\n",res);
    }


}
