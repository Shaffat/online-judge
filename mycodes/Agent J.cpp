#include<bits/stdc++.h>

using namespace std;

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        double r1,r2,r3,a,b,c,EG,EF,GF,AGE,BEF,CGF,A,B,C,s,ABC,res;
        scanf("%lf %lf %lf",&r1,&r2,&r3);
        a=r2+r3;
        b=r1+r3;
        c=r1+r2;
        //cout<<"a="<<a<<" b="<<b<<" c="<<c<<endl;
        A=acos(((b*b)+(c*c)-(a*a))/(2*b*c));
        B=acos(((a*a)+(c*c)-(b*b))/(2*a*c));
        C=acos(((a*a)+(b*b)-(c*c))/(2*a*b));
        //cout<<"A="<<A<<" B="<<B<<" C="<<C<<endl;
        EG=r1*A;
        EF=r2*B;
        GF=r3*C;
        AGE=r1*EG/2;
        BEF=r2*EF/2;
        CGF=r3*GF/2;
        s=(a+b+c)/2;
        ABC=sqrt(s*(s-a)*(s-b)*(s-c));
        //cout<<"AGE="<<AGE<<" BEF="<<BEF<<" CGF="<<CGF<<" ABC="<<ABC<<endl;
        res=ABC-AGE-BEF-CGF;
        printf("Case %d: %.9lf\n",t,res);
        t++;
    }
}
