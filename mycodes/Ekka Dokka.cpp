#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        long long int n,m=1,w;
        scanf("%lld",&w);
        if(w%2==0)
        {
            while(w%2==0)
            {
                w/=2;
                m*=2;
            }
            n=w;
            printf("Case %d: %lld %lld\n",t,n,m);
        }
        else
        {
            printf("Case %d: Impossible\n",t);
        }
    }
}
