#include<bits/stdc++.h>

using namespace std;

#define ll long long  int
ll how_many(ll p,ll target,vector<ll>&scotter)
{
    ll first=0,last=scotter.size()-1,mid,i,j,res=scotter.size(),f=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f=1;
        mid=(first+last)/2;
        if(p*scotter[mid]>target)
        {
            res=mid;
            last=mid-1;
        }
        else
        {
            first=mid+1;
        }
    }
    return scotter.size()-res;
}


int main()
{
    ll test,t,i,j,kp,ks,n,target;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&kp,&ks);
        target=kp*ks;
        scanf("%lld",&n);
        vector<ll>guard;
        vector<ll>scotter;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            guard.push_back(j);
        }
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            scotter.push_back(j);
        }
        sort(guard.begin(),guard.end());
        sort(scotter.begin(),scotter.end());
        ll res=1;
        for(i=0;i<n;i++)
        {
            res*=how_many(guard[i],target,scotter)-i;
            res%= 1000000007;
        }
        printf("Case %lld: %lld\n",t,res);
    }
}
