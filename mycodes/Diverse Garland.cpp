#include<bits/stdc++.h>
using namespace std;
vector<int>col(6,-1);
vector<vector<int> >memory(200001,col);

struct detail
{
    int index,pre;
};
map<int,char>decode;

vector<detail>col1(4);
vector<vector<detail> >path(200001,col1);

string s;
int solve(int index,int pre)
{
    if(index>=s.size()) return 0;
    int i,j,cost=2e9,cur;
    if(memory[index][pre]!=-1)
        return memory[index][pre];
    for(i=1;i<=3;i++)
    {
        if(i==pre) continue;
        if(s[index]==decode[i])
        {
            cur=solve(index+1,i);
        }
        else
            cur=solve(index+1,i)+1;
        if(cur<cost)
        {
            detail nxt;
            nxt.index=index+1;
            nxt.pre=i;
            cost=cur;
            path[index][pre]=nxt;
        }
    }
    //cout<<"min cost from ["<<index<<"]["<<pre<<"] is to ["<<path[index][pre].index<<"]["<<path[index][pre].pre<<"]"<<endl;
    return memory[index][pre]=cost;
}


int main()
{
    decode[1]='R';
    decode[2]='G';
    decode[3]='B';
    int n,cost,i,j;
    cin>>n>>s;

    cost=solve(0,0);
    cout<<cost<<endl;
    j=path[0][0].pre;
    cout<<decode[j];
    for(i=1;i<n;i++)
    {
        cout<<decode[path[i][j].pre];
        j=path[i][j].pre;
    }
}
