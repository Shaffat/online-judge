#include<bits/stdc++.h>
using namespace std;

#define ll long long int

vector<ll>primes;
vector<bool>p(1e6,0);

void sieve()
{
    ll i,j;
    for(i=4;i<=2e5;i+=2)
        p[i]=1;
    for(i=3;i<=sqrt(2e5);i++)
    {
        if(p[i]==0)
        {
            for(j=i*i;j<=2e5;j+=i)
                p[j]=1;
        }
    }
    primes.push_back(2);
    for(i=3;i<=2e5;i+=2)
        if(p[i]==0) primes.push_back(i);
    return;
}

ll solve(ll n)
{
    if(n%2==0) return n/2;
    ll i,j,counter=0,cur=1;
    for(i=0;i<primes.size();i++)
    {
        if(n%primes[i]==0)
        {
            counter=1;
            n-=primes[i];
            break;
        }
    }
    if(n%2==0)
    counter+=n/2;
    else
        counter=1;
    return counter;
}

int main()
{
    sieve();
    //for(ll i=0;i<=100;i++)cout<<primes[i]<<" ";
    ll n;
    cin>>n;
    cout<<solve(n)<<endl;
}
