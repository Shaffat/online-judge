#include<bits/stdc++.h>

using namespace std;
int divisor;
int solve(string s,int index,int sum,int reminder,bool restriction,int k,vector< vector< vector<int> > >&memory)
{

    if(index>=s.size())
    {
        if(reminder==0 && (sum%k)==0)
        {
            return 1;
        }
        return 0;
    }

    if(memory[index][sum][reminder]!=-1 && restriction==0)
    {
        return memory[index][sum][reminder];
    }

    int i,j,cur_dig=s[index]-'0';
    int result=0;
    if(restriction)
    {
        for(i=0;i<=cur_dig;i++)
        {
            if(i<cur_dig)
            {
                result+=solve(s,index+1,sum+i,((reminder*10)+i)%k,0,k,memory);

            }
            else
            {
                result+=solve(s,index+1,sum+i,((reminder*10)+i)%k,1,k,memory);
            }
        }
    }
    else
    {
        for(i=0;i<=9;i++)
        {
            result+=solve(s,index+1,sum+i,((reminder*10)+i)%k,0,k,memory);

        }
    }
    if(restriction)
    {
        return result;
    }
    else{
            return memory[index][sum][reminder]=result;
        }

}

string building(int k)
{
    string nw="";
    while(k!=0)
    {
        int i,j;
        i=k%10;
        k/=10;
        nw.insert(nw.begin(),'0'+i);
    }
    return nw;

}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int ai,bi,k;
        scanf("%d %d %d",&ai,&bi,&k);
        string a,b;
        a=building(ai-1);
        b=building(bi);
        if(a.size()!=b.size())
        {
            if(a.size()>b.size())
            {
                while(b.size()!=a.size())
                {
                    b.insert(b.begin(),'0');
                }
            }
            else
            {
                 while(b.size()!=a.size())
                {
                    a.insert(a.begin(),'0');
                }
            }
        }
        if(k>90)
        {
            k=90;
        }
        vector<int>reminders(k+1,-1);
        vector<vector<int> >sum(95,reminders);
        vector<vector <vector<int> > >memory(14,sum);
        int acount=solve(a,0,0,0,1,k,memory);
        int bcount=solve(b,0,0,0,1,k,memory);
        printf("Case %d: %lld\n",t,bcount-acount);
        t++;

    }
}
