#include<bits/stdc++.h>

using namespace std;

void update(int index,int value,int n,vector<int>&tree)
{
    while(index<=n)
    {
        tree[index]+=value;
        index+=index&(-index);
    }
}

int query(int index,vector<int>&tree)
{
    int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index&(-index);
    }
    return sum;
}

int main()
{
//   freopen("in.txt","r",stdin);
//   freopen("out.txt","w",stdout);
    int n,k,i,j,m=0;
    while(scanf("%d %d",&n,&k)!=EOF)
    {

        m++;
        vector<int>curarray(n+10,1);
        vector<int>negtree(n+10,0);
        vector<int>zerotree(n+10,0);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            if(j<0)
            {
                update(i,1,n,negtree);
                curarray[i]=-1;
            }
            else if(j==0)
            {
                update(i,1,n,zerotree);
                curarray[i]=0;
            }
            else
            {
                curarray[i]=1;
            }
        }
        for(i=1;i<=k;i++)
        {
            //getchar();
            int u,v;
            char c;
            scanf(" %c",&c);
            if(c=='C')
            {
                scanf("%d %d",&u,&v);

                int chk=0;
                if(v<0)
                {
                    chk=-1;
                }
                if(v>0)
                {
                    chk=1;
                }
                if(chk==curarray[u])
                {
                    continue;
                }
                else
                {
                    if(chk==1)
                    {
                        if(curarray[u]==-1)
                        {
                            update(u,-1,n,negtree);
                        }
                        else
                        {
                            update(u,-1,n,zerotree);
                        }
                        curarray[u]=1;
                    }
                    else if(chk==0)
                    {
                        if(curarray[u]==-1)
                        {
                            update(u,-1,n,negtree);
                        }
                        update(u,1,n,zerotree);
                        curarray[u]=0;
                    }
                    else
                    {
                        if(curarray[u]==0)
                        {
                            update(u,-1,n,zerotree);
                        }
                        update(u,1,n,negtree);
                        curarray[u]=-1;
                    }
                }
            }
            else
            {
                scanf("%d %d",&u,&v);
                int zero,neg;
                zero=query(v,zerotree)-query(u-1,zerotree);
                if(!zero)
                {
                    neg=query(v,negtree)-query(u-1,negtree);
                    if(neg%2)
                    {
                        printf("-");
                    }
                    else
                        printf("+");
                }
                else
                    printf("0");
            }
        }
        printf("\n");
    }
}

