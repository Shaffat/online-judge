#include<bits/stdc++.h>
using namespace std;

#define REP(i,a,b)       for(int i=a;i<=b;i++)
#define RREP(i,a,b)      for(int i=a;i>=b;i--)
#define sc3(a,b,c)       scanf("%d %d %d",&a,&b,&c)
#define sc2(a,b)       scanf("%d %d",&a,&b)
#define sc1(a)         scanf("%d",&a)


int main()
{
    int n;
    vector<int>digits(7,0);
    vector<vector<int> >groups(100000);
    int i,j;
    sc1(n);
    int chk=0;
    REP(i,1,n)
    {
        sc1(j);
        digits[j]++;
        if(j==5||j==7)
        {
            chk=1;
        }
    }
    if(chk)
    {
        printf("-1\n");
        return 0;
    }
    for(i=1;i<=n/3;i++)
    {
        if(digits[1]>0)
        {
            groups[i].push_back(1);
            digits[1]--;
            if(digits[2]>0 && (digits[4]>0 ||digits[6]>0))
            {
                digits[2]--;
                groups[i].push_back(2);
                if(digits[4]>0)
                {
                    digits[4]--;
                    groups[i].push_back(4);
                }
                else
                {
                    digits[6]--;
                    groups[i].push_back(6);
                }
            }
            else if(digits[3]>0 && digits[6]>0)
            {
                digits[3]--;
                digits[6]--;
                groups[i].push_back(3);
                groups[i].push_back(6);
            }
            else
            {
                chk=1;
                break;
            }
        }
        else
        {
            chk=1;
            break;
        }

    }
    if(chk)
    {
        printf("-1\n");
        return 0;
    }
    else
    {
        for(i=1;i<=n/3;i++)
        {
            for(j=0;j<groups[i].size();j++)
            {
                printf("%d ",groups[i][j]);
            }
            printf("\n");
        }

    }
}
