#include <bits/stdc++.h>
#define ll long long int
#define SIZE 1000000

bool p[SIZE+5];
using namespace std;
vector < ll > primes;
void sieve() {
    int i, j, sz = sqrt(SIZE) + 1;

    for (i = 4; i <= SIZE; i += 2) {
        p[i] = true;
    }

    for (i = 3; i < sz; ++i) {
        if (p[i] == false) {
            for (j = i * i; j <= SIZE; j += i)
                p[j] = true;
        }
    }

    for (i = 2; i <= SIZE; ++i) {
        if(p[i] == false) {
            primes.push_back(i*i);
        }
    }

}


int main() {

    sieve();

    for (int i = 0; i <= 100; ++i) {
        printf("%lld\n", primes[i]);
    }

    return (0);
}
