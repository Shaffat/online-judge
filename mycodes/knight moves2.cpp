#include<bits/stdc++.h>

using namespace std;

struct board
{
    int row,column;
};

int main()
{
    string s;

    while(getline(cin,s))
    {
        board start,destination,current,temp;
        start.column=s.at(0);
        start.row=s.at(1);
        destination.column=s.at(3);
        destination.row=s.at(4);
        int vis[120][120],dis[120][120];
        for(int i=1;i<120;i++)
        {
            for(int j=1;j<120;j++)
            {
                vis[i][j]=0;
                dis[i][j]=0;
            }
        }
        queue<board>knight;
        knight.push(start);
        vis[start.row][start.column]=1;
        int moves=0;
        while(!knight.empty())
        {
            current.column=knight.front().column;
            current.row=knight.front().row;
            knight.pop();

            temp.row=current.row-1;
            temp.column=current.column-2;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row-1;
            temp.column=current.column+2;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row+1;
            temp.column=current.column-2;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row+1;
            temp.column=current.column+2;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row-2;
            temp.column=current.column-1;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row-2;
            temp.column=current.column+1;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row+2;
            temp.column=current.column-1;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

            temp.row=current.row+2;
            temp.column=current.column+1;
            if(temp.row>48 && temp.row<57 && temp.column>96 && temp.column<105 && vis[temp.row][temp.column]==0)
            {
                if(temp.row==destination.row && temp.column==destination.column)
                {
                    moves=dis[current.row][current.column]+1;
                    break;
                }
                dis[temp.row][temp.column]=dis[current.row][current.column]+1;
                vis[temp.row][temp.column]=1;
                knight.push(temp);
            }

        }
        cout<<"To get from "<<s.at(0)<<s.at(1)<<" to "<<s.at(3)<<s.at(4)<<" takes "<<moves<<" knight moves."<<endl;

    }
}
