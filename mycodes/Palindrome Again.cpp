
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())


map<string,int>numbers;

string shapeUp(string &s)
{
    ll i,j,start,ending;
    FOR(i,0,s.size()-1)
    {
        if(s[i]!=' ')
        {
            start=i;
            break;
        }
    }
    ROF(i,s.size()-1,0)
    {
        if(s[i]!=' ')
        {
            ending=i;
            break;
        }
    }
    string res="";
    FOR(i,start,ending)
    {
        res.push_back(s[i]);
    }
    return res;
}

ll findPos(string &text,string &pattern)
{
    //cout<<"text="<<text<<" pattern="<<pattern<<endl;
    if(text.size()<pattern.size()) return -1;
    ll i,j;
    FOR(i,0,text.size()-pattern.size())
    {
        ll ok=1;
        FOR(j,0,pattern.size()-1)
        {
            if(text[i+j]!=pattern[j])
            {
                ok=0;
                break;
            }
        }
        if(ok)
            return i;
    }
    //cout<<"didnt find him"<<endl;
    return -1;
}

string getSubstring(string &s,ll st,ll e)
{
    ll i;
    string res="";
    FOR(i,st,e)
    {
        res.push_back(s[i]);
    }
    return res;
}

ll getNumber(string &s)
{
    //cout<<"Running getNumber"<<endl;
    ll i,j,last=0,found,res=0;
    string hundred="hundred",thousand="thousand";
    //cout<<"here"<<endl;
    found=findPos(s,thousand);
    //cout<<"here"<<endl;
    if(found!=-1)
    {
        //cout<<"found thousand at pos "<<found<<endl;
        string tmp="";
        tmp=s.substr(last,found-1);
        tmp=getSubstring(s,last,found-1);
        tmp=shapeUp(tmp);
        res+=numbers[tmp]*1000;
        last+=found+thousand.size();
//        cout<<"number text is "<<tmp<<endl;
//        cout<<"last="<<last<<endl;
    }
    found=findPos(s,hundred);
    if(found!=-1)
    {
        //cout<<"found hundred at pos "<<found<<endl;
        string tmp="";

        tmp=getSubstring(s,last,found-1);
        tmp=shapeUp(tmp);
        //cout<<"number text is "<<tmp<<endl;
        res+=numbers[tmp]*100;
        last=found+hundred.size();
        //cout<<"last="<<last<<endl;
    }
    if(last<s.size())
    {
        //cout<<"snigle  last="<<last<<endl;
        string tmp="";
        //cout<<"s="<<s<<" sz="<<s.size()<<endl;
        tmp=getSubstring(s,last,s.size()-1);
        //cout<<"tmp="<<tmp<<endl;
        tmp=shapeUp(tmp);
        res+=numbers[tmp];
    }
    //cout<<"number is "<<res<<endl;
    return res;

}

string toBinary(ll n)
{
    string res="";
    while(n>0)
    {
        res.push_back('0'+n%2);
        n/=2;
    }
    REV(res);
    return res;
}

bool palindrome(string &s)
{
    ll i,j;
    i=0;
    j=s.size()-1;
    while(i<=j)
    {
        if(s[i]!=s[j])
            return false;
        i++;
        j--;
    }
    return true;
}


bool solve(string &s)
{
    ll num=getNumber(s);
    //cout<<"number is "<<num<<endl;
    string bin=toBinary(num);
    //cout<<"bin is "<<bin<<endl;
    return palindrome(bin);
}

int main()
{
    numbers["one"]=1;
    numbers["two"]=2;
    numbers["three"]=3;
    numbers["four"]=4;
    numbers["five"]=5;
    numbers["six"]=6;
    numbers["seven"]=7;
    numbers["eight"]=8;
    numbers["nine"]=9;
    numbers["ten"]=10;
    numbers["eleven"]=11;
    numbers["twelve"]=12;
    numbers["thirteen"]=13;
    numbers["fourteen"]=14;
    numbers["fifteen"]=15;
    numbers["sixteen"]=16;
    numbers["seventeen"]=17;
    numbers["eighteen"]=18;
    numbers["nineteen"]=19;
    numbers["twenty"]=20;
    numbers["twenty one"]=21;
    numbers["twenty two"]=22;
    numbers["twenty three"]=23;
    numbers["twenty four"]=24;
    numbers["twenty five"]=25;
    numbers["twenty six"]=26;
    numbers["twenty seven"]=27;
    numbers["twenty eight"]=28;
    numbers["twenty nine"]=29;
    numbers["thirty"]=30;
    numbers["thirty one"]=31;
    numbers["thirty two"]=32;
    numbers["thirty three"]=33;
    numbers["thirty four"]=34;
    numbers["thirty five"]=35;
    numbers["thirty six"]=36;
    numbers["thirty seven"]=37;
    numbers["thirty eight"]=38;
    numbers["thirty nine"]=39;
    numbers["forty"]=40;
    numbers["forty one"]=41;
    numbers["forty two"]=42;
    numbers["forty three"]=43;
    numbers["forty four"]=44;
    numbers["forty five"]=45;
    numbers["forty six"]=46;
    numbers["forty seven"]=47;
    numbers["forty eight"]=48;
    numbers["forty nine"]=49;
    numbers["fifty"]=50;
    numbers["fifty one"]=51;
    numbers["fifty two"]=52;
    numbers["fifty three"]=53;
    numbers["fifty four"]=54;
    numbers["fifty five"]=55;
    numbers["fifty six"]=56;
    numbers["fifty seven"]=57;
    numbers["fifty eight"]=58;
    numbers["fifty nine"]=59;
    numbers["sixty"]=60;
    numbers["sixty one"]=61;
    numbers["sixty two"]=62;
    numbers["sixty three"]=63;
    numbers["sixty four"]=64;
    numbers["sixty five"]=65;
    numbers["sixty six"]=66;
    numbers["sixty seven"]=67;
    numbers["sixty eight"]=68;
    numbers["sixty nine"]=69;
    numbers["seventy"]=70;
    numbers["seventy one"]=71;
    numbers["seventy two"]=72;
    numbers["seventy three"]=73;
    numbers["seventy four"]=74;
    numbers["seventy five"]=75;
    numbers["seventy six"]=76;
    numbers["seventy seven"]=77;
    numbers["seventy eight"]=78;
    numbers["seventy nine"]=79;
    numbers["eighty"]=80;
    numbers["eighty one"]=81;
    numbers["eighty two"]=82;
    numbers["eighty three"]=83;
    numbers["eighty four"]=84;
    numbers["eighty five"]=85;
    numbers["eighty six"]=86;
    numbers["eighty seven"]=87;
    numbers["eighty eight"]=88;
    numbers["eighty nine"]=89;
    numbers["ninety"]=90;
    numbers["ninety one"]=91;
    numbers["ninety two"]=92;
    numbers["ninety three"]=93;
    numbers["ninety four"]=94;
    numbers["ninety five"]=95;
    numbers["ninety six"]=96;
    numbers["ninety seven"]=97;
    numbers["ninety eight"]=98;
    numbers["ninety nine"]=99;

    ll test,t;
    string s;
    cin>>test;
    getchar();
    while(test--)
    {
        getline(cin,s);
        if(solve(s))
        {
            cout<<"YES"<<endl;
        }
        else
            cout<<"NO"<<endl;
    }


}
