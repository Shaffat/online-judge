#include<bits/stdc++.h>
///solution Zico bai
///hash+binary+cumulative sum
using namespace std;

typedef unsigned long long int llu;

llu segment_hash(int l,int r,vector<llu>&v,vector<llu>&power)
{
    //cout<<"value="<<v[r]-v[l-1]*power[r-l+1]<<endl;
    return v[r]-v[l-1]*power[r-l+1];
}

bool is_palindrome(int l,int r,int n,vector<llu>&frward,vector<llu>&bckward,vector<llu>&power)
{
    //cout<<"miror of "<<l<<" and "<<r<<" is "<<n-r+1<<" and "<<n-l+1<<endl;
    if(segment_hash(l,r,frward,power)==segment_hash(n-r+1,n-l+1,bckward,power))
       {
           //cout<<"palindrome found"<<endl;
           return true;
       }
       return false;
}

int Binary_search(int n,vector<int>&pos,vector<llu>&frward,vector<llu>&bckward,vector<llu>&power)
{
    //cout<<"NEW Search"<<endl;
    int i,j,first,last,mid,res,f=0;
    first=0;
    last=pos.size()-1;
    while(first<=last)
    {
        //cout<<"first="<<first<<" last="<<last<<endl;
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f++;
        }
        mid=(first+last)/2;
        int chk=0;
        for(i=1;i<=n-pos[mid]+1;i++)
        {
            if(is_palindrome(i,i+pos[mid]-1,n,frward,bckward,power))
            {
                //cout<<pos[mid]<<" length palindrome got"<<endl;
                chk=1;
                break;
            }
        }
        if(chk)
        {
            first=mid+1;
            res=pos[mid];
        }
        else
            last=mid-1;
        //cout<<"after first="<<first<<" last="<<last<<endl;
    }
    return res;
}
int solve(string s)
{
    int i,j;
    vector<llu>frward(s.size()+1);
    vector<llu>bckward(s.size()+1);
    vector<llu>power(s.size()+1);
    vector<int>even,odd;
    frward[0]=0;
    bckward[0]=0;
    power[0]=1;
    for(i=0;i<s.size();i++)
    {
        power[i+1]=power[i]*29;
        frward[i+1]=frward[i]*29+s[i];
        bckward[i+1]=bckward[i]*29+s[s.size()-i-1];
        if((i+1)%2==1)
        {
            odd.push_back(i+1);
        }
        else
            even.push_back(i+1);
    }
//    for(i=0;i<=s.size();i++)
//    {
//        cout<<"frward["<<i<<"]="<<frward[i]<<" bckward["<<i<<"]="<<bckward[i]<<endl;
//    }
    return max(Binary_search(s.size(),even,frward,bckward,power),Binary_search(s.size(),odd,frward,bckward,power));
}

int main()
{
    int n;
    string s;
    cin>>n>>s;
    cout<<solve(s);
    return 0;
}
