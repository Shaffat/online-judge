#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1,n,i,j;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d",&n);
        vector<int>sequence(1e6+1,0);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            sequence[j]++;
        }
        i=0;
        j=n-1;
        bool valid=1;
        while(i<=j)
        {
            int total=0;
            if(i!=j)
            {
                total=sequence[i]+sequence[j];
            }
            else
            {
                 if(sequence[i]==1)
                 {
                     total=2;
                 }
                 else total=3;
            }
            if(total!=2)
            {
                valid=0;
                break;
            }
            i++;
            j--;
        }
        if(valid)
        {
            printf("Case %d: yes\n",t);
        }
        else
        {
            printf("Case %d: no\n",t);
        }
        t++;
    }
}

