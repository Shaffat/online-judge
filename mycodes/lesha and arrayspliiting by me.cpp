#include<bits/stdc++.h>
using namespace std;
int main()
{
    int n;
    cin>>n;
    int ar[n+1],chker[n+1];
    vector<int>non_zero;
    for(int i=1;i<=n;i++)
    {
        chker[i]=0;
        cin>>ar[i];

        if(ar[i]!=0)
        {
            non_zero.push_back(i);
        }
    }
    if(non_zero.size()==0)
    {
        cout<<"NO"<<endl;
        return 0;
    }
    else
    {
        cout<<"YES"<<endl;
        cout<<non_zero.size()<<endl;
        for(int i=0;i<non_zero.size();i++)
        {
            int first=non_zero.at(i),last=non_zero.at(i);
            int chk1=1,chk2=1;
            while(chk1==1)
            {
                if(first>1 && ar[first-1]==0&&chker[first-1]==0)
                {
                    first--;
                    chker[first]=1;
                }
                else chk1=0;
            }
            while(chk2==1)
            {
                if(last<n&&ar[last+1]==0&&chker[last+1]==0)
                {
                    last++;
                    chker[last]=1;

                }
                else chk2=0;
            }
            cout<<first<<" "<<last<<endl;
        }
    }
}
