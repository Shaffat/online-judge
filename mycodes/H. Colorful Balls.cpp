
#include <bits/stdc++.h>


using namespace std;


int n;
char s[100000+5];
int dp[100000+5][4];

int colorInt(const char & ch) {
    if (ch == 'R') return 1;
    else if (ch == 'B') return 2;
    else if(ch == 'G') return 3;
    else if (ch == 'W') return 4;

    else return 0;
}

int solve(int index, int color) {
    if (index >= n) return (1);

    if (colorInt(s[index]) == color) return (0);

    //if (index == n - 1)

    if (dp[index][color] != -1) return dp[index][color];

    int & ans = dp[index][color];
    ans = 0;

    if (s[index] == 'W') {
        for (int i = 1; i < 4; ++i) {
            if (i != color) {
                ans += solve(index + 1, i);
                ans = ans % 1000000007;
            }
        }
    }
    else {
        ans = solve(index + 1, colorInt(s[index]));
        ans = ans % 1000000007;
    }
    //printf("Index %d = %d\n", index, ans);
    return ans;
}

int main() {
    int test, t = 1;
    scanf("%d", &test);
    getchar();
    for(t = 1; t <= test; ++t) {
        scanf("%s", &s);
        n = strlen(s);
        memset(dp, -1, sizeof(dp));
        printf("Case %d: %d\n", t, solve(0, 0));
    }

    return (0);
}
