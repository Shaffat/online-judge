#include<bits/stdc++.h>

using namespace std;

#define ll long long int

int main()
{
    ll t,test,n,a,b,i,j;
    scanf("%lld",&test);
    while(test--)
    {
        scanf("%lld",&n);
        vector<ll>a(n+1);
        vector<ll>b(n+1);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            a[i]=j;
        }
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            b[i]=j;
        }
        int chk=1;
        for(i=1;i<=n-2;i++)
        {
            if(a[i]>b[i])
            {
                chk=0;
            }
            ll m=b[i]-a[i];
            a[i]+=m;
            a[i+1]+=2*m;
            a[i+2]+=3*m;
        }
        for(i=n-1;i<=n;i++)
        {
            if(a[i]!=b[i])
            {
                chk=0;
            }
        }
        if(chk)
        {
            printf("TAK\n");
        }
        else
            printf("NIE\n");
    }
}
