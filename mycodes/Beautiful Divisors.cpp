#include<bits/stdc++.h>
using namespace std;

bool isbeautiful(int n)
{

    int i,j,total=log2(n),one,zero;
    if(total%2==1)
    {
        return 0;
    }
    int chk=1;
    zero=total/2;
    one=total/2+1;
    for(i=0;i<zero;i++)
    {

        int chk3=1<<i;
        //printf("res %d\n",n&chk3);
        if((n&(1<<i))==(1<<i))
        {
            return 0;
        }
    }
    for(i=zero;i<one+zero;i++)
    {
        //cout<<"in one"<<endl;
        if((n&(1<<i))==0)
        {
            return 0;
        }
    }
    return 1;
}


int main()
{
    int n,zero,one,i,mx=-1;
    scanf("%d",&n);
//    if(n==1)
//    {
//        printf("1\n");
//        return 0;
//    }
    for(i=1;i<=sqrt(n);i++)
    {
        if((n%i)==0)
        {
            if(isbeautiful(i))
            {
                mx=max(mx,i);
            }
            if(isbeautiful(n/i))
            {
                mx=max(mx,n/i);
            }
        }
    }
    cout<<mx<<endl;

}
