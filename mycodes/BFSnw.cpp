#include<bits/stdc++.h>
using namespace std;

int main()
{
    vector<int>adj[100];
    int vis[100],dis[100];
    int n,e;
    queue<int>q;
    cout<<"number of nodes and edges-"<<endl;
    cin>>n>>e;
    cout<<"enter the edges"<<endl;
    for(int i=1;i<=e;i++)
    {
        int a,b;
        cin>>a>>b;
        adj[a].push_back(b);
        adj[b].push_back(a);
    }

    for(int i=1;i<=n;i++)
    {
        dis[i]=0;
        vis[i]=0;
    }
    cout<<"enter source-"<<endl;
    int s;
    cin>>s;
    q.push(s);
    vis[s]=1;

    while(!q.empty())
    {
        int u=q.front();
        cout<<u<<" ";
        q.pop();
        for(int i=0;i<adj[u].size();i++)
        {
            int v=adj[u][i];
            if(!vis[v])
            {
                dis[v]=dis[u]+1;

                vis[v]=1;
                q.push(v);
            }
        }
    }
    cout<<endl;
    for(int i=1;i<=n;i++)
    {
        cout<<"distance of "<<i<<" from root node is "<<dis[i]<<endl;
    }
}
