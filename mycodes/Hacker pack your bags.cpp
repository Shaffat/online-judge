#include<bits/stdc++.h>
using namespace std;

struct voucher
{
    int r,l,cost;
};
bool operator < (voucher a,voucher b)
{
    if(a.cost!=b.cost)
    {
        return a.cost > b.cost;
    }
    return false;
}
int main()
{
    int n,x,l,r,i,j,cost,maxduration=-1;
    scanf("%d %d",&n,&x);
    vector< vector<voucher> >duration(200002);
    for(i=1; i<=n; i++)
    {
        scanf("%d %d %d",&l,&r,&cost);
        voucher tmp;
        tmp.l=l;
        tmp.r=r;
        tmp.cost=cost;
        duration[r-l+1].push_back(tmp);
        if((r-l+1)>maxduration)
        {
            maxduration=r-l+1;
        }
    }
    for(i=0; i<=maxduration; i++)
    {
        sort(duration[i].begin(),duration[i].end());
    }
    int totalcost=2e9+3;
    for(i=0; i<=maxduration; i++)
    {
        int need=x-i;
        int tempcost;
        if(need<0)
        {
            continue;
        }
        for(j=0; j<duration[i].size(); j++)
        {
            int curl=duration[i][j].l;
            int curr=duration[i][j].r;
            int curcost=duration[i][j].cost;
            tempcost=curcost;
            bool find=0;
            for(int k=0; k<duration[need].size(); k++)
            {
                if(duration[need][k].l!=curl || duration[need][k].r!=curr)
                {
                    tempcost+=duration[need][k].cost;
                    if(tempcost<totalcost)
                    {
                        totalcost=tempcost;
                    }
                    break;
                }
            }

        }
    }
    if(totalcost<2e9+3)
    {
        printf("%d\n",totalcost);
    }
    else
        printf("-1\n");
}
