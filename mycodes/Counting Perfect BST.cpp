#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>memory(1e5,-1);
ll solve(ll n)
{
    if(n==0||n==1)
    {
        return 1;
    }
    if(n==2)
    {
        return 2;
    }
    if(n==3)
    {
        return 5;
    }
    if(memory[n]!=-1)
    {
        return memory[n];
    }
    ll i,res=0;
    for(i=1;i<=n;i++)
    {
        res+=(solve(i-1)*solve(n-i))%100000007;
        res%=100000007;
    }
    return memory[n]=res;
}

vector<ll>perfect;
set<ll>checker;
void perfect_generate()
{
    ll i,j,l;
    for(i=2;i<=1e5;i++)
    {
        j=i*i;
        while(j<=1e10)
        {
            l=checker.size();
            checker.insert(j);
            if(l!=checker.size())
            {
                perfect.push_back(j);
            }
            j*=i;
        }
    }
}
ll finder(ll val,vector<ll>v)
{
    ll first=0,last=v.size()-1,pos=-1,loop=0,mid;
    while(loop<=300)
    {
        if(first>last)
        {
            break;
        }
        mid=(first+last)/2;
        if(v[mid]<=val)
        {
            pos=mid;
            first=mid+1;
        }
        else
        {
            last=mid-1;
        }
        loop++;
    }
    return pos;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    perfect_generate();
    sort(perfect.begin(),perfect.end());
    ll test,t=1;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        ll n,a,b;
        scanf("%lld %lld",&a,&b);

        if(a<=b)
        {
            vector<ll>::iterator a_pos,b_pos;
            ll up,low,res=0,len;
            a_pos=lower_bound(perfect.begin(),perfect.end(),a);
            b_pos=lower_bound(perfect.begin(),perfect.end(),b);
            low=a_pos-perfect.begin();
            up=b_pos-perfect.begin();
            if(perfect[up]>b)
            {
                up--;
            }
            if(up<low)
            {
                len=0;
            }
            else
            {
                len=up-low+1;
            }
            if(len>0)
            res=solve(len);
            printf("Case %lld: %lld\n",t,res);
        }

    }
}
