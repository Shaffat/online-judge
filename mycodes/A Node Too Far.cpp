#include<bits/stdc++.h>
using namespace std;

int solve(int node, int range,vector<vector<int> >&connection,int total)
{
    int i,j,counter=1;
    vector<int>vis(total+1,0);
    vector<int>dis(total+1,0);
    queue<int>q;
    vis[node]=1;
    q.push(node);
    while(!q.empty())
    {
        int cur=q.front();

        q.pop();
        if(dis[cur]>=range)
        {
            continue;
        }
        for(i=0;i<connection[cur].size();i++)
        {

            int nxt=connection[cur][i];
            if(!vis[nxt])
            {
                 counter++;
                 dis[nxt]=dis[cur]+1;
                 vis[nxt]=1;
                 q.push(nxt);
            }

        }
    }
    return counter;
}


int main()
{

    int n,t=1,i,j,query;
    while(scanf("%d",&n))
    {
        if(n==0)
        {
            break;
        }
        map<int,int>mp;
        vector<vector<int> >connection(2*n);
        int u,v,initial=0;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&u,&v);
            if(mp.count(u)==0)
            {
                mp[u]=initial;
                initial++;
            }
            if(mp.count(v)==0)
            {
                mp[v]=initial;
                initial++;
            }
            //cout<<"u="<<u<<" maping="<<mp[u]<<" v="<<v<<" mapping="<<mp[v]<<endl;
            connection[mp[u]].push_back(mp[v]);
            connection[mp[v]].push_back(mp[u]);
        }
        int source,range;
        while(scanf("%d %d",&source,&range))
        {
            if(source==0&&range==0)
            {
                break;
            }
            int res=solve(mp[source],range,connection,initial);
            //cout<<res<<endl;
            printf("Case %d: %d nodes not reachable from node %d with TTL = %d.\n",t,initial-res,source,range);
            t++;

        }
    }


}

