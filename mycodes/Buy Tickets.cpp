#include<stdio.h>
#include<vector>
#include<set>
#include<iostream>
using namespace std;

void update(int node,int val,int s,int e,int q_s,int q_e,vector<int>&tree)
{
    if(s>q_e||e<q_s)
    {
        return;
    }
    if(s>=q_s && e<=q_e)
    {
        tree[node]+=val;
        return;
    }
    int left,right,mid;
    left=2*node;
    right=2*node+1;
    mid=(s+e)/2;
    update(left,val,s,mid,q_s,q_e,tree);
    update(right,val,mid+1,e,q_s,q_e,tree);
}

int query(int node,int p,int s,int e,int q_s,int q_e,vector<int>&tree)
{
    if(s>q_e||e<q_s)
    {
        return 0;
    }
    if(s>=q_s && e<=q_e)
    {
        return tree[node]+p;
    }
    int left,right,mid;
    left=2*node;
    right=2*node+1;
    mid=(s+e)/2;
    return query(left,tree[node]+p,s,mid,q_s,q_e,tree)+query(right,tree[node]+p,mid+1,e,q_s,q_e,tree);
}


int main()
{
    int t,n;

    while(scanf("%d",&n)!=EOF){
           vector<int>tree(4*n,0);
           vector<int>values(n+1);
           int i,j,pos,val,s,e,b;
           for(i=1;i<=n;i++)
           {
               scanf("%d %d",&pos,&val);
               pos++;
               values[i]=j;
               s=pos;
               e=i-1;
               cout<<"s="<<s<<" e="<<e<<endl;
               if(s<=e){
                cout<<"add 1 from "<<s<<" to "<<e<<endl;
                update(1,1,1,n,s,e,tree);
               }

           }
           vector<int>latest(n+1);
           for(i=1;i<=n;i++)
           {
               int cur=query(1,0,1,n,i,i,tree);
               cout<<"propagation at "<<i<<"="<<cur<<endl;
               //latest[cur]=values[i];
           }
           for(i=1;i<=n;i++)
           {
               printf("%d ",latest[i]);
           }
            printf("\n");
    }

}
