#include<bits/stdc++.h>
using namespace std;

int prime[100000001];

void sieve(){
    int n,i,j;
    for(i=4;i<=100000000;i+=2){
        prime[i]=1;
    }
    prime[1]=0;
    for(i=3;i<=sqrt(100000000);i+=2){
        if(prime[i]==0){
            for(j=i*i;j<=100000000;j+=i){
                prime[j]=1;
            }
        }
    }
    return;
}

int solve(vector<int>&v){
    int i,j,res=1e9,start=0,ending=-1;
//    for(i=0;i<v.size();i++){
//        cout<<v[i]<<" ";
//    }
//    cout<<endl;
    for(i=0;i<v.size()-1;i++){
            int cur=v[i],ok=0;
        for(j=i+1;j<v.size();j++){
            cur+=v[j];
            //cout<<"cur="<<cur<<" p="<<prime[cur]<<endl;
            int tmp=j-i+1;
            if(prime[cur]==0 && res>tmp){
                    res=tmp;
                    ok=1;
                    start=i;
                    ending=j;

            }
        }

    }
    if(res==1e9){
        printf("This sequence is anti-primed.\n");
    }
    else{
        printf("Shortest primed subsequence is length %d:",ending-start+1);
        for(i=start;i<=ending;i++){
            printf(" %d",v[i]);
        }
        printf("\n");
    }
    return res;
}

int main(){
    sieve();
    //for(int i=2;i<=100;i++){
    //    if(prime[i]==0){
    //        cout<<i<<" ";
    //    }
    //}
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int n,i,test,j,res;
    scanf("%d",&test);
    while(test--){
        scanf("%d",&n);
        vector<int>v;
        for(i=1;i<=n;i++){
            scanf("%d",&j);
            v.push_back(j);
        }
        solve(v);

    }
}
