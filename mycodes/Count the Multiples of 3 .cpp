#include<bits/stdc++.h>
using namespace std;

struct info
{
    int zero,one,two,propagation;
};

info forOne(info val,int pr)
{
    int o,t,z;
    o=val.one;
    t=val.two;
    z=val.zero;
    val.zero=t;
    val.one=z;
    val.two=o;
    val.propagation=pr;
    return val;
}

info forTwo(info val,int pr)
{
    int o,t,z;
    o=val.one;
    t=val.two;
    z=val.zero;
    val.zero=o;
    val.one=t;
    val.two=z;
    val.propagation=pr;
    return val;
}

void combine(info a,info b,int node,vector<info>&tree)
{
    info c;
    tree[node].one=a.one+b.one;
    tree[node].two=a.two+b.two;
    tree[node].zero=a.zero+b.zero;
    return;
}

int query(int node,int propagate,int s,int e,int q_s,int q_e,vector<info>&tree)
{
    if(s>q_e||e<q_s)
    {
        return 0;
    }
    if(s>=q_s && e<=q_e)
    {
        if(propagate%3==1)
        {
            return tree[node].two;
        }
        else if(propagate%3==2)
        {
            return tree[node].one;
        }
        else
            return tree[node].zero;
    }
    int left,right,mid,prop;
    left=2*node;
    right=2*node+1;
    mid=(s+e)/2;
    prop=tree[node].propagation;
    return query(left,propagate+prop,s,mid,q_s,q_e,tree)+query(right,propagate+prop,mid+1,e,q_s,q_e,tree);
}


void update(int node,int s,int e,int q_s,int q_e,vector<info>&tree)
{
    if(s>q_e||e<q_s)
    {
        return;
    }
    if(s>=q_s && e<=q_e)
    {
        int cur=tree[node].propagation+1;
        tree[node]=forOne(tree[node],cur);
        return;
    }
    int left,right,mid,prop;
    left=2*node;
    right=2*node+1;
    mid=(s+e)/2;
    update(left,s,mid,q_s,q_e,tree);
    update(right,mid+1,e,q_s,q_e,tree);
    prop=tree[node].propagation;
    if(prop%3==1)
    {
        info cur_left=forOne(tree[left],0),cur_right=forOne(tree[right],0);
        combine(cur_left,cur_right,node,tree);
    }
    else if(prop%3==2)
    {
        info cur_left=forTwo(tree[left],0),cur_right=forTwo(tree[right],0);
        combine(cur_left,cur_right,node,tree);

    }
    else
        combine(tree[left],tree[right],node,tree);
    //cout<<"updated node="<<node<<" s="<<s<<" e="<<e<<" one="<<tree[node].one<<" two="<<tree[node].two<<" zero="<<tree[node].zero<<endl;
    return;

}


void build_tree(int node,int strt_segment,int end_segment,vector<info>&tree)
{
    if(strt_segment==end_segment)
    {
        info tmp;
        tmp.propagation=0;
        tmp.one=0;
        tmp.two=0;
        tmp.zero=1;
        tree[node]=tmp;
        return;
    }
    int left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(strt_segment+end_segment)/2;
    build_tree(left,strt_segment,mid,tree);
    build_tree(right,mid+1,end_segment,tree);
    tree[node].zero=tree[left].zero+tree[right].zero;
    tree[node].one=tree[left].one+tree[right].one;
    tree[node].two=tree[left].two+tree[right].two;
    //cout<<"s="<<strt_segment<<" e="<<end_segment<<" one="<<tree[node].one<<" two="<<tree[node].two<<" zero="<<tree[node].zero<<endl;
    return;

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t,i,j,n,q,res,type,s,e;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&q);
        info tmp;
        tmp.one=0;
        tmp.two=0;
        tmp.zero=0;
        tmp.propagation=0;
        vector<info>s_tree(4*n,tmp);
        build_tree(1,1,n,s_tree);
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d %d",&type,&s,&e);
            s++;
            e++;
            if(type==0)
            {
                update(1,1,n,s,e,s_tree);

            }
            else
            {
                //cout<<"-------------------------QUERY-----------------------------"<<endl;
                res=query(1,0,1,n,s,e,s_tree);
                printf("%d\n",res);
            }
        }
    }

}
