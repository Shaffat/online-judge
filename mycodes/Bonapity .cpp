#include<bits/stdc++.h>
using namespace std;

int main(){
    int t,i,j;
    string s1,s2;
    cin>>t;
    while(t--){
        cin>>s1>>s2;
        for(i=0;i<s1.size();i++){
            if(s1[i]>='A' && s1[i]<='Z'){
                s1[i]+=32;
            }
        }
        for(i=0;i<s2.size();i++){
            if(s2[i]>='A' && s2[i]<='Z'){
                s2[i]+=32;
            }
        }
        for(i=0;i<s1.size();i++){
            if(s1[i]=='b'){
                s1[i]='p';
            }
            if(s1[i]=='i'){
                s1[i]='e';
            }
        }
        for(i=0;i<s2.size();i++){
            if(s2[i]=='b'){
                s2[i]='p';
            }
            if(s2[i]=='i'){
                s2[i]='e';
            }
        }
        if(s1==s2){
            cout<<"Yes"<<endl;
        }
        else
            cout<<"No"<<endl;
    }
}
