#include<bits/stdc++.h>
using namespace std;


int value[101];
int color[101];
int dis[101];
int lastnode[101];
int dfs(int source, vector<vector<int> >&connection)
{
   color[source]=1;
   int i,j;
   int learn=0;
   //cout<<source<<endl;
   for(i=0;i<connection[source].size();i++)
   {
       int next=connection[source][i];
       if(value[next]>learn)
       {
           lastnode[source]=next;
           learn=value[next];
       }
   }
   int poins=0;
   if(color[lastnode[source]]==0)
    poins=dfs(lastnode[source],connection);
   return dis[source]=value[source]+poins;
}

int finddeath(int node)
{
    if(lastnode[node]==node)
    {
        return node;
    }
    return finddeath(lastnode[node]);
}

void initialeverything(int n)
{
    int i;
    for(i=0;i<=n;i++)
    {
        lastnode[i]=i;
        dis[i]=-1;
        color[i]=0;
    }

}
int main()
{
   int test,t=1;
   scanf("%d",&test);
   while(t<=test)
   {
       int n,m,i,j,u,v;
       scanf("%d %d",&n,&m);
       initialeverything(n);
       for(i=0;i<n;i++)
       {
           scanf("%d",&u);
           value[i]=u;
       }
       vector<int>vi;
       vector< vector<int> >connection(n+1,vi);
       for(i=1;i<=m;i++)
       {
          // cout<<"here"<<endl;
           scanf("%d %d",&u,&v);
           connection[u].push_back(v);
       }
       int res=dfs(0,connection);
       int node=finddeath(0);
       printf("Case %d: %d %d\n",t,res,node);
       t++;
   }
}
