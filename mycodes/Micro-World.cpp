#include<bits/stdc++.h>

using namespace std;

int main()
{
    long long int i,j,n,k,alive=1,mx=-1;
    scanf("%lld %lld",&n,&k);
    vector<long long int>bacteria;
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        bacteria.push_back(j);
        mx=max(mx,j);
    }
    vector<bool>status(mx+1,1);
    sort(bacteria.begin(),bacteria.end());
    reverse(bacteria.begin(),bacteria.end());
    int cur=bacteria[0];
    for(i=1;i<bacteria.size();i++)
    {
        //cout<<"cur="<<cur<<" bac="<<bacteria[i]<<endl;
        if(cur>bacteria[i] && cur<=bacteria[i]+k)
        {
            //cout<<"dead"<<endl;
            cur=bacteria[i];
            status[cur]=0;
        }
        else if(cur==bacteria[i])
        {
            if(status[cur]==1)
            {
                //cout<<"alive"<<endl;
                alive++;
            }
        }
        else
        {
            //cout<<"alive"<<endl;
            cur=bacteria[i];
            alive++;
        }
    }
    cout<<alive<<endl;
}
