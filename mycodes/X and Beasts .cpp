#include<bits/stdc++.h>
using namespace std;

int upgrade(int n){
    int i,j,res=0;
    while(n%2==0){
        res++;
        n/=2;
    }
    return res;

}


int main()
{
    int test,t,i,j,res,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++){
        vector<int>v;
        set<int>s;
        scanf("%d",&n);
        for(i=1;i<=n;i++){
            scanf("%d",&j);
            int pre=s.size();
            s.insert(j);
            if(pre!=s.size()){
                v.push_back(j);
            }
        }
        //cout<<v.size()<<endl;
        sort(v.begin(),v.end());
        res=0;
        for(i=0;i<v.size();i++){
            res+=upgrade(v[i]);
        }
        printf("%d\n",res);
    }
}
