#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}

void buildGraph(vector<int>&id,vector<vector<int> >&connection)
{
    int i,j;
    for(i=0;i<id.size();i++)
    {
        for(j=i+1;j<id.size();j++)
        {
            if(GCD(id[i],id[j])>1)
            {
                connection[i].push_back(j);
                connection[j].push_back(i);
            }
        }
    }
    return;
}


int bfs(int root,vector<vector<int> >&connection,vector<int>&vis)
{
    int i,j,counter=1;
    vis[root]=1;
    queue<int>q;
    q.push(root);
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            int child=connection[cur][i];
            if(!vis[child])
            {
                vis[child]=1;
                q.push(child);
                counter++;
            }
        }
    }
    return counter;
}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int n,m,i,j,team=0,alal;
    vector<int>v;
    scanf("%d",&n);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        v.push_back(j);
    }
    vector<vector<int> >connection(n+1);
    vector<int>vis(n+1,0);
    buildGraph(v,connection);
    team++;
    alal=bfs(0,connection,vis);
    for(i=1;i<n;i++)
    {
        if(vis[i]==0)
        {
            team++;
            bfs(i,connection,vis);
        }
    }
    printf("%d %d",team,alal);

}
