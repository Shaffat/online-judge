
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

#define ll long long int
using namespace std;


void solve(int id,int first,int last,vector<int>&a,vector<int>&b)
{
    //cout<<"id="<<id<<" first="<<first<<" last="<<last<<endl;
    if(last-first==1)
    {
        if(id%2==1)
        {
            a.push_back(first);
            b.push_back(last);
        }
        else
        {
            a.push_back(last);
            b.push_back(first);
        }
        return;
    }
    int mid=(first+last)/2;
    //cout<<"calling1 "<<first<<" to "<<mid<<" from "<<first<<" to "<<last<<endl;
    solve(id+1,first,mid,a,b);
    //cout<<"calling2 "<<mid+1<<" to "<<last<<" from "<<last<<" to "<<last<<endl;

    solve(id,mid+1,last,a,b);
    return;
}

int main()
{
    freopen("analogous.in","r",stdin);
    freopen("analogous.out","w",stdout);
//    vector<int>a;
//    vector<int>b;
//    solve(1,1,16,a,b);
//    for(int i=0;i<a.size();i++)
//    {
//        cout<<a[i]<<" ";
//    }
//    cout<<endl;
//    for(int i=0;i<b.size();i++)
//    {
//        cout<<b[i]<<" ";
//    }
//    cout<<endl;
    int n,i,j;

    while(scanf("%d",&n)){
        if(n==0) break;
        vector<int>a;
        vector<int>b;
        double up,down;
        up=ceil(log2(n));
        down=floor(log2(n));
        if(up==down)
        {
            solve(1,1,2*n,a,b);
            printf("Yes\n");
            j=0;
            for(i=0;i<n;i++)
            {
                printf("%d ",a[i]);
            }
            printf("\n");
            for(i=0;i<n;i++)
            {
                printf("%d ",b[i]);
            }
            printf("\n");
        }
        else{
            printf("No\n");
        }
    }
}
