

#include <bits/stdc++.h>

using namespace std;


int main() {

    int n, i, l , r, zeroL = 0, zeroR = 0, time = 0, oneL, oneR;
    scanf("%d", &n);
    for ( i = 0; i < n; ++i) {
        //cin >> l >> r;
        scanf("%d %d", &l, &r);
        if (l == 0) zeroL++;
        if (r == 0) zeroR++;
    }
    oneL = n - zeroL;
    oneR = n - zeroR;

    cout << min(oneL,zeroL)+min(oneR,zeroR)<<endl;

    return (0);
}
