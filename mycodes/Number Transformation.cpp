#include<bits/stdc++.h>
using namespace std;

vector<bool>prime(1001,0);
void sieve()
{
    int i,j;
    prime[1]=1;
    for(i=4;i<=1000;i+=2)
    {
        prime[i]=1;
    }
    for(i=3;i<=sqrt(1000);i+=2)
    {
        for(j=i*i;j<=1000;j+=i)
        {
            prime[j]=1;
        }
    }
}



int solve(int s,int t)
{
    vector<bool>vis(1001,0);
    vector<int>dis(1001,0);
    int i,j;
    queue<int>q;
    q.push(s);
    while(!q.empty())
    {
        int cur=q.front();
        if(cur==t)
        {
            return dis[cur];
        }
        q.pop();
        for(i=2;i<=sqrt(cur);i++)
        {
            if(cur%i==0)
            {
                if(prime[i]==0)
                {
                    int nxt=cur+i;
                    if(!vis[nxt]&&nxt<=t)
                    {
                        vis[nxt]=1;
                        dis[nxt]=dis[cur]+1;
                        q.push(nxt);
                    }
                }
               if(prime[cur/i]==0)
               {
                   int nxt=cur+(cur/i);
                   if(!vis[nxt]&&nxt<=t)
                   {
                       vis[nxt]=1;
                       dis[nxt]=dis[cur]+1;
                       q.push(nxt);
                   }

               }
            }
        }
    }
    return -1;

}
int main()
{
    int a,b,t=1;
    sieve();
    while(scanf("%d %d",&a,&b))
    {
        if(a==0 && b==0)
        {
            return 0;
        }
        int res=solve(a,b);
        printf("Case %d: %d\n",t,res);
        t++;
    }
}
