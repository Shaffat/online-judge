#include<bits/stdc++.h>
using namespace std;


void solve(int source,int des,int l,vector<vector<int> >&connection,vector<int>&parent)
{
    int i,j;
    queue<int>q;
    q.push(source);
    vector<int>vis(2*l+1,0);
    vis[source]=1;
    parent[source]=source;
    while(!q.empty())
    {
        int cur=q.front();
        q.pop();
        if(cur==des)
        {
            return;
        }
        for(i=0;i<connection[cur].size();i++)
        {
            int nxt=connection[cur][i];
            if(vis[nxt]==0)
            {
                parent[nxt]=cur;
                q.push(nxt);
                vis[nxt]=1;
            }

        }
    }
    return;
}
void route_finder(vector<int>&parent,int des,vector<string>&value)
{
    stack<string>stk;
    while(parent[des]!=des)
    {
        string tmp="";
        tmp=value[parent[des]]+" "+value[des];
        stk.push(tmp);
        des=parent[des];
    }
    while(!stk.empty())
    {
        cout<<stk.top()<<"\n";
        stk.pop();
    }
    return;
}


int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int i,j,link,f=0;;
    while(scanf("%d",&link)!=EOF)
    {
        if(f)
        printf("\n");
        f++;
        string s1,s2;
        int k=0;
        map<string ,int >mp;
        vector<vector<int> >connection(2*link+1);
        vector<string>values;
        for(i=1;i<=link;i++)
        {
            cin>>s1>>s2;
            if(mp.count(s1)==0)
            {
                mp[s1]=k;
                values.push_back(s1);
                k++;
            }
            if(mp.count(s2)==0)
            {
                mp[s2]=k;
                values.push_back(s2);
                k++;
            }
            connection[mp[s1]].push_back(mp[s2]);
            connection[mp[s2]].push_back(mp[s1]);
        }
        vector<int>parent(2*link+4,-1);
        cin>>s1>>s2;
         if(mp.count(s1)==0)
            {
                mp[s1]=k;
                values.push_back(s1);
                k++;
            }
        if(mp.count(s2)==0)
            {
                mp[s2]=k;
                values.push_back(s2);
                k++;
            }
        solve(mp[s1],mp[s2],link,connection,parent);
        if(s1==s2)
        {
            cout<<s2<<" "<<s2<<endl;
        }
        else if(parent[mp[s2]]==-1)
        {
            printf("No route\n");
        }
        else
        {
            route_finder(parent,mp[s2],values);
        }
    }
}
