#include<bits/stdc++.h>

using namespace std;

#define REP(i,a,b)       for(int i=a;i<=b;i++)
#define RREP(i,a,b)      for(int i=a;i>=b;i--)
#define sc3(a,b,c)       scanf("%d %d %d",&a,&b,&c)
#define sc2(a,b)       scanf("%d %d",&a,&b)
#define sc1(a)         scanf("%d",&a)

struct no_error_plz
{
    int index,force;
};


bool operator <(no_error_plz a,no_error_plz b)
{
    if(a.force!=b.force)
    {
        return a.force>b.force;
    }
    return false;
}
int main()
{
    int n,m,i,j,k;
    sc2(n,m);
    vector<int>row(1001,0);
    vector<vector<int> >table(1001,row);
    vector<int>force_lookup(n+1);
    vector<no_error_plz>forces;
    REP(i,1,n)
    {
        sc1(j);
        no_error_plz tmp;
        force_lookup[i]=j;
        tmp.index=i;
        tmp.force=j;
        forces.push_back(tmp);
    }
    REP(i,1,m)
    {
        sc2(j,k);
        table[j][k]=1;
        table[k][j]=1;
    }
    sort(forces.begin(),forces.end());
    int long long total=0;

    for(i=0;i<forces.size();i++)
    {

        int current_part=forces[i].index;
        for(j=1;j<=n;j++)
        {
            if(table[current_part][j]==1)
            {
                table[current_part][j]=0;
                table[j][current_part]=0;
                total+=force_lookup[j];
            }
        }
    }
    cout<<total<<endl;
}
