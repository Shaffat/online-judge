#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

bool valid(int r,int c,int n,int m)
{
    if(r>=1 && r<=n && c>=1 && c<=m)
        return true;
    return false;
}


int main()
{
    int n,m,i,j,k,ok=1;
    cin>>n>>m;
    vector<int>col(m+1,0);
    vector<vector<int> >matrix(n+1,col);
    vector<vector<int> >done(n+1,col);

    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            scanf("%d",&k);
            matrix[i][j]=k;
        }
    }
    vector<pos>v;
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=m;j++)
        {
            if(matrix[i][j]==1)
            {
                cout<<"for "<<i<<" "<<j<<endl;
                int f=0;
                if(valid(i-1,j-1,n,m) && valid(i-1,j,n,m) && valid(i,j-1,n,m))
                {
                    if (matrix[i-1][j-1]==1 && matrix[i-1][j]==1 && matrix[i][j-1]==1)
                    {
                        cout<<"valid1"<<endl;
                        f=1;
                        pos tmp;
                        tmp.x=i-1;
                        tmp.y=j-1;
                        v.push_back(tmp);
                    }
                }
                if(valid(i-1,j,n,m) && valid(i-1,j+1,n,m) && valid(i,j+1,n,m))
                {
                    if (matrix[i-1][j]==1 && matrix[i-1][j+1]==1 && matrix[i][j+1]==1)
                    {
                         cout<<"valid2"<<endl;
                        f=1;
                        pos tmp;
                        tmp.x=i-1;
                        tmp.y=j;
                        v.push_back(tmp);
                    }
                }
                if(valid(i,j-1,n,m) && valid(i+1,j,n,m) && valid(i+1,j-1,n,m))
                {
                    if (matrix[i][j-1]==1 && matrix[i+1][j]==1 && matrix[i+1][j-1]==1)
                    {
                         cout<<"valid3"<<endl;
                        f=1;
                        pos tmp;
                        tmp.x=i;
                        tmp.y=j-1;
                        v.push_back(tmp);
                    }
                }
                if(valid(i,j+1,n,m) && valid(i+1,j,n,m) && valid(i+1,j+1,n,m))
                {
                    if (matrix[i][j+1]==1 && matrix[i+1][j]==1 && matrix[i+1][j+1]==1)
                    {
                         cout<<"valid4"<<endl;
                        f=1;
                        pos tmp;
                        tmp.x=i;
                        tmp.y=j;
                        v.push_back(tmp);
                    }
                }
                if(f==0)
                    ok=0;
            }
        }
    }
    if(ok){
        cout<<v.size()<<endl;
        for(i=0;i<v.size();i++)
        {
            cout<<v[i].x<<" "<<v[i].y<<endl;
        }
    }
    else
        cout<<"-1"<<endl;
}
