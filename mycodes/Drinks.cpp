
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/

#include<bits/stdc++.h>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(0);
    int n,i,j;
    cin>>n;
    double total=0;
    for(i=1;i<=n;i++)
    {
        cin>>j;
        total+=double(j)/100;
    }
    //cout<<total<<endl;
    double res=total/n;
    res*=100;
    cout<<fixed<<setprecision(10)<<res<<endl;
    return 0;
}
