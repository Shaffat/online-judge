#include<bits/stdc++.h>
using namespace std;

struct node
{
    int r,c;
};

bool is_valid(int &n,int &r,int &c)
{
    if(r<0 || r>=n || c<0 || c>=n)
        return false;
    return true;
}

void bfs(node root,int n,vector<vector<int> >&grid,vector<vector<int> >&vis)
{
    vis[root.r][root.c]=1;
    queue<node>q;
    q.push(root);
    while(!q.empty())
    {
        node cur=q.front();
        q.pop();
        node nxt;
        nxt.r=cur.r-1;
        nxt.c=cur.c-1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c-1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c+1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c+1;
        if(is_valid(n,nxt.r,nxt.c))
        {
            if(!vis[nxt.r][nxt.c] && grid[nxt.r][nxt.c]==1)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
            }
        }
    }
}

int solve(int n,vector<vector<int> >&grid)
{
    int i,j,res=0;
    vector<int>col1(n+1,0);
    vector<vector<int> >vis(n+1,col1);
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            if(!vis[i][j] && grid[i][j]==1)
            {
                res++;
                node root;
                root.r=i;
                root.c=j;
                bfs(root,n,grid,vis);
            }
        }
    }
    return res;
}

int main()
{
    string s;
    int n,i,j,k,t=1;
    while(scanf("%d",&n)!=EOF)
    {
        vector<int>col(n+1);
        vector<vector<int> >grid(n+1,col);
        for(i=0;i<n;i++)
        {
            cin>>s;
            for(j=0;j<n;j++)
            {
                grid[i][j]=s[j]-'0';
            }
        }
        int res=solve(n,grid);
        printf("Image number %d contains %d war eagles.\n",t,res);
        t++;
    }
}
