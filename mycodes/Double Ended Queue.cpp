#include<bits/stdc++.h>
using namespace std;

int main()
{

    int test,i,j,t,x;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        cout<<"Case "<<t<<":"<<endl;
        int sz,cur=0,q;
        deque<int>deq;
        cin>>sz>>q;
        //cout<<"sz="<<sz<<" q="<<q<<endl;
        while(q--)
        {
            string s;
            cin>>s;
            //cout<<"s="<<s<<endl;
            if(s=="pushLeft" || s=="pushRight")
            {
                cin>>x;
                if(s=="pushLeft" && cur<sz)
                {
                    deq.push_front(x);
                    cout<<"Pushed in left: "<<x<<endl;
                    cur++;
                }
                else if(s=="pushRight" && cur<sz)
                {
                    deq.push_back(x);
                    cout<<"Pushed in right: "<<x<<endl;
                    cur++;
                }
                else
                    cout<<"The queue is full"<<endl;
            }
            else if(s=="popLeft" && deq.size()>0)
            {
                int k=deq.front();
                deq.pop_front();
                cout<<"Popped from left: "<<k<<endl;
                cur--;
            }

            else if(s=="popRight" &&deq.size()>0)
            {
                cout<<"Popped from right: "<<deq.back()<<endl;;
                deq.pop_back();
                cur--;
            }
            else
                cout<<"The queue is empty"<<endl;
        }

    }
}
