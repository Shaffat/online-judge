#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int n,i,j,day,test,t;
    scanf("%lld",&test);
    while(test--)
    {
        scanf("%lld",&n);
        day=1;
        vector<long long int>persons(n+1,0);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            persons[i]=persons[i-1]+j;
        }
        long long int cur=1;
        while(cur<n)
        {
            cur+=persons[cur];
            //cout<<"after day"<<day<<" people know="<<cur<<endl;
            if(cur>=n)
            {
                break;
            }
            day++;
        }
        printf("%lld\n",day);
    }

}
