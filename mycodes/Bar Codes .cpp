#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<ll>col(51,-1);
vector<vector<ll> >col2(51,col);
vector<vector<vector<ll> > >memory(52,col2);


ll t;
ll solve(ll n,ll k,ll m)
{
    ll i;
    if(n==0 && k==0)
    {
        return 1;
    }
    if(n<0 || k<0)
    {
        return 0;
    }
    if(memory[n][k][m]!=-1)
    {
        return memory[n][k][m];
    }
    ll res=0;
    for(i=1;i<=m;i++)
    {
        res+=solve(n-i,k-1,m);
    }
    return memory[n][k][m]=res;

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll test,i,j,n,k,m;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        ll res;
        scanf("%lld %lld %lld",&n,&k,&m);
        res=solve(n,k,m);
        printf("Case %lld: %lld\n",t,res);
    }
}
