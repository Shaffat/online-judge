#include<bits/stdc++.h>
using namespace std;

int main()
{
    long long int n,h,a,b,k,t1,f1,t2,f2,i;
    scanf("%lld %lld %lld %lld %lld",&n,&h,&a,&b,&k);
    while(k--)
    {
        scanf("%lld %lld %lld %lld",&t1,&f1,&t2,&f2);
        if(t1==t2)
        {
            cout<<abs(f1-f2)<<endl;
        }
        else if(f1>=a && f1<=b)
        {
            cout<<abs(f1-f2)+abs(t1-t2)<<endl;
        }
        else if(f1<a)
        {
            cout<<abs(f1-a)+abs(f2-a)+abs(t1-t2)<<endl;
        }
        else
            cout<<abs(f1-b)+abs(f2-b)+abs(t1-t2)<<endl;
    }
}
