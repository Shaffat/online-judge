#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int x,y;
};

double dis_calculate(pos a, pos b)
{
    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
double solve(int bitmask,int &n,vector<pos>&house,vector<vector<double> >&all_dis,vector<double>&memory)
{
    if(bitmask==((1<<n)-1))
    {
        return 0;
    }
    if(memory[bitmask]!=-1)
    {
        return memory[bitmask];
    }
    int i,j,cur;
    double res=2e9;
    for(i=0;i<n;i++)
    {
        if((bitmask&(1<<i))==0)
        {
            cur=i;
            break;
        }
    }
    bitmask|=(1<<cur);
    for(i=0;i<n;i++)
    {
        if((bitmask&(1<<i))==0)
        {
            double res1=solve(bitmask|(1<<i),n,house,all_dis,memory);
            res1+=all_dis[cur][i];
            res=min(res,res1);
        }
    }
//    cout<<"memory["<<bitmask<<"]="<<res<<endl;
    return memory[bitmask]=res;
}

int main()
{
    int n,i,j,x,y,t=1;
    while(1)
    {
        cin>>n;
        if(n==0)
        {
            return 0;
        }
        string s;
        vector<pos>house;
        n<<=1;
        for(i=1;i<=n;i++)
        {
            cin>>s>>x>>y;
            pos tmp;
            tmp.x=x;
            tmp.y=y;
            house.push_back(tmp);
        }
        vector<double>col(n+1);
        vector<vector<double> >all_dis(n+1,col);
        vector<double>memory(1<<n,-1);
        for(i=0;i<n;i++)
        {
            for(j=0;j<n;j++)
            {
                all_dis[i][j]=dis_calculate(house[i],house[j]);
            }
        }
        double res=solve(0,n,house,all_dis,memory);
        printf("Case %d: %.2lf\n",t,res);
        t++;
    }
}
