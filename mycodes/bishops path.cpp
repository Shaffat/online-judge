#include<bits/stdc++.h>
using namespace std;

bool solve(int r,int c,int currow,int curcol,int counter,vector<vector<bool> >&chess)
{
    cout<<"cur row="<<currow<<" cur col="<<curcol<<" counter="<<counter<<" r="<<r<<" c="<<c<<endl;
    if(counter==0)
    {
        return true;
    }
    if(currow<0||currow>r||curcol<0||curcol>c)
    {
        cout<<"false"<<endl;
        return false;
    }
    int upupr,upupc,updownc,updownr,downupc,downupr,downdownr,downdownc,i,j,k,l,m,n;
    k=min(currow,c-curcol);
    upupr=currow-k;
    upupc=curcol+k;
    k=min(curcol,r-currow);
    updownc=curcol-k;
    updownr=currow+k;
    k=min(currow,curcol);
    downupc=curcol-k;
    downupr=currow-k;
    k=(c-curcol,r-currow);
    downdownc=curcol+k;
    downdownr=currow+k;

      while(updownc<=upupc+1 && updownr>=upupr-1)
    {
        if(updownc==upupc+1 && updownr==upupr-1)
        {
            break;
        }
        if(updownr==currow && updownc==curcol)
        {
             updownc++;
             updownr--;
             continue;
        }
        if(!chess[updownr][updownc])
        {
            cout<<"up"<<endl;
            chess[updownr][updownc]=1;

            counter--;

            if(solve(r,c,updownr,updownc,counter,chess))
            {
                return true;
            }
            else
            {
                chess[updownr][updownc]=0;
                counter++;
            }
        }

        updownc++;
        updownr--;
    }
    while(downupc<=downdownc+1 && downupr<=downdownr+1)
    {

        if(downupc==downdownc+1 && downupr==downdownr+1)
        {
            return false;
        }
        if(downupc==curcol && downupr==currow)
        {
             downupc++;
             downupr++;
            continue;
        }
        if(!chess[downupr][downupc])
        {
            cout<<"down"<<endl;
            chess[downupr][downupc]=1;

            counter--;

            if(solve(r,c,downupr,downupc,counter,chess))
            {
                return true;
            }
            else
            {
                chess[downupr][downupc]=0;
                counter++;
            }
        }
        downupc++;
        downupr++;
    }

}
int main()
{
    vector<bool>ch(130,0);
    vector<vector<bool> >chess(130,ch);
    chess[0][1]=1;
    cout<<solve(0,3,0,2,1,chess);
}
