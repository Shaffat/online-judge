#include<bits/stdc++.h>
using namespace std;

#define ll long long int

ll column(ll r,ll c, ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kc==qc)
    {
        ans+=abs(qr-kr) - 1;
    }
    else
        ans+=r-1;

    return ans;
}

ll row(ll r,ll c,ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kr==qr)
    {
        ans+=abs(kc-qc) - 1;
    }
    else
        ans+=r-1;
    return ans;
}

ll top_left_diagonal(ll r,ll c,ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kr<qr && kc<qc)
    {
        if(qr-kr==qc-kc)
        {
            ans+=min(qr-1,qc-1)-abs(kr-qr);
        }
        else
            ans+=min(qr-1,qc-1);
    }
    else
        ans+=min(qr-1,qc-1);
    return ans;
}
ll bottom_left_diagonal(ll r,ll c,ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kr>qr && kc<qc)
    {
        if(kr-qr==qc-kc)
        {
            ans+=min(r-qr,qc-1)-abs(kr-qr);
        }
        else
            ans+=min(r-qr,qc-1);
    }
    else
        ans+=min(r-qr,qc);
    return ans;
}
ll top_right_diagonl(ll r,ll c,ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kr<qr && kc>qc)
    {
        if(abs(kr-qr)==abs(qc-kc))
        {
            ans+=min(qr-1,c-qc)-abs(kr-qr);
        }
        else
            ans+=min(qr-1,c-qc);
    }
    else
        ans+=min(qr-1,c-qc);
    return ans;
}
ll bottom_right_diagonal(ll r,ll c,ll qr,ll qc,ll kr,ll kc)
{
    ll ans=0;
    if(kr>qr && kc>qc)
    {
        if(abs(kr-qr)==abs(qc-kc))
        {
            ans+=min(r-qr,c-qc)-abs(kr-qr);
        }
        else
            ans+=min(r-qr,c-qc);
    }
    else
        ans+=min(r-qr,c-qc);
    return ans;
}

int main()
{
    ll test,t,i,j,r,c,x,y;
//    printf("%d\n", top_left_diagonal(5, 6, 4,5,2,3));
//    printf("%d\n", bottom_left_diagonal(5, 6, 4,5,2,3));
//    printf("%d\n", bottom_right_diagonal(5, 6, 3,3,4,4));
//    printf("%d\n", top_right_diagonl(5, 6, 3,3,2,4));
//    printf("%d\n", top_left_diagonal(5, 6, 4,5,2,3));
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld %lld %lld",&r,&c,&x,&y);
        ll total=0;
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=c;j++)
            {
                if(x==i && y==j) continue;
                total+=r*c-2;
                total-=top_left_diagonal(r,c,i,j,x,y);
                total-=bottom_left_diagonal(r,c,i,j,x,y);
                total-=top_right_diagonl(r,c,i,j,x,y);
                 total-=bottom_right_diagonal(r,c,i,j,x,y);
                 total-=column(r,c,i,j,x,y);
                 total-=row(r,c,i,j,x,y);
            }
        }
        printf("%lld\n",total);
    }

}
