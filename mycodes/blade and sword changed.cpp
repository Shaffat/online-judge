#include<bits/stdc++.h>
using namespace std;

 struct pos
 {
     int r,c;
 };
vector<int>a(2,0);
vector<vector<int> >b(400,a);
vector<vector<vector<int> > >stage(400,b);
vector<int>e(400,0);
vector< vector<int> >vis(400,e);
vector<int>g(400,0);
vector<vector<int> >dis(400,g);

int r,c,t;
int solve(pos start, pos goal,vector<pos>&teleports)
{
    int i,j;
    queue<pos>q;
    vis[start.r][start.c]=t;
    q.push(start);
    while(!q.empty())
    {
        pos cur=q.front();
        cout<<cur.r<<" "<<cur.c<<endl;
        if(cur.r==goal.r && cur.c==goal.c)
        {
            return dis[cur.r][cur.c];
        }
        q.pop();
        if(stage[cur.r][cur.c][1]==t)
        {

            if(vis[cur.r][cur.c]==t)
            {
                cout<<"its visited a teleport"<<endl;
                pos nxt;
                nxt.r=cur.r+1;
                nxt.c=cur.c;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        vis[nxt.r][nxt.c]=t;
                        dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                        q.push(nxt);
                    }

                }
                nxt.r=cur.r-1;
                nxt.c=cur.c;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        vis[nxt.r][nxt.c]=t;
                        dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                        q.push(nxt);
                    }

                }
                nxt.r=cur.r;
                nxt.c=cur.c+1;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        vis[nxt.r][nxt.c]=t;
                        dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                        q.push(nxt);
                    }

                }
                nxt.r=cur.r;
                nxt.c=cur.c-1;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        vis[nxt.r][nxt.c]=t;
                        dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                        q.push(nxt);
                    }

                }
            }
            else
            {
                cout<<"teleport found"<<endl;
                for(i=0;i<teleports.size();i++)
                {
                    if(teleports[i].r==cur.r && teleports[i].c==cur.c)
                    {
                        continue;
                    }
                    pos nxt;
                    vis[teleports[i].r][teleports[i].c]=t;
                    dis[teleports[i].r][teleports[i].c]=dis[cur.r][cur.c]+1;
                    q.push(teleports[i]);
                }
                if(teleports.size()>1)
                {
                    vis[cur.r][cur.c]=t;
                    dis[cur.r][cur.c]+=1;
                    q.push(cur);
                }

            }
        }
        else
        {
                pos nxt;
                nxt.r=cur.r+1;
                nxt.c=cur.c;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        if(stage[nxt.r][nxt.c][1]==t)
                        {
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);

                        }
                        else
                        {
                            vis[nxt.r][nxt.c]=t;
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);
                        }
                    }

                }
                nxt.r=cur.r-1;
                nxt.c=cur.c;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        if(stage[nxt.r][nxt.c][1]==t)
                        {
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);

                        }
                        else
                        {
                            vis[nxt.r][nxt.c]=t;
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);
                        }
                    }

                }
                nxt.r=cur.r;
                nxt.c=cur.c+1;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        if(stage[nxt.r][nxt.c][1]==t)
                        {
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);

                        }
                        else
                            {
                                vis[nxt.r][nxt.c]=t;
                                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                                q.push(nxt);
                            }
                    }

                }
                nxt.r=cur.r;
                nxt.c=cur.c-1;

                if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
                {
                    if(vis[nxt.r][nxt.c]!=t && stage[nxt.r][nxt.c][0]==0)
                    {
                        if(stage[nxt.r][nxt.c][1]==t)
                        {
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);

                        }
                        else
                        {
                            vis[nxt.r][nxt.c]=t;
                            dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                            q.push(nxt);
                        }
                    }

                }

        }

    }
    return -1;

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    t=1;
    int test,i,j;
    pos start,goal;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&r,&c);
        vector<pos>teleports;
        for(i=1;i<=r;i++)
        {
            getchar();
            for(j=1;j<=c;j++)
            {
                char x;
                scanf("%c",&x);
                if(x=='#')
                {
                    stage[i][j][0]=-1;
                }
                if(x=='*')
                {
                    pos tmp;
                    tmp.r=i;
                    tmp.c=j;
                    teleports.push_back(tmp);
                    stage[i][j][1]=t;
                }
                if(x=='D')
                {
                    goal.r=i;
                    goal.c=j;
                }
                if(x=='P')
                {
                    start.r=i;
                    start.c=j;
                }
            }
        }
        int res=solve(start,goal,teleports);
        if(res>0)
        {
            printf("Case %d: %d\n",t,res);
        }
        else
        {
            printf("Case %d: impossible\n",t);
        }
        t++;
    }

}

