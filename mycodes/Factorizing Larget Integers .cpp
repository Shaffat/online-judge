#include<bits/stdc++.h>

using namespace std;
#define sz 100000000
#define ll long long int
ll p[sz/32 +100];
vector<ll>primes;
ll check(ll n,ll pos)
{
    return n&(1<<pos);
}
ll set_p(ll n,ll pos)
{
    return n|(1<<pos);
}
void sieve()
{
    ll i,j;
    for(i=3;i<=sqrt(sz);i+=2)
    {
        if(check(p[i/32],i%32)==0)
        {
            for(j=i*i;j<=sz;j+=i)
            {
                p[j/32]=set_p(p[j/32],j%32);
            }
        }
    }
    primes.push_back(2);
    for(i=3;i<=sz;i+=2)
    {
        if(check(p[i/32],i%32)==0)
            primes.push_back(i);
    }
    return;
}

void solve(ll n)
{
    printf("%lld =",n);
    if(n==1)
    {
        printf(" 2^0\n");
        return;
    }
    ll before=0,i;
    for(i=0;i<primes.size();i++)
    {
        if(primes[i]>n) break;
        if(n%primes[i]==0)
        {
            if(before)
            {
                printf(" *");
            }
            before=1;
            ll counter=0;
            printf(" ");
            while(n%primes[i]==0)
            {
                n/=primes[i];
                counter++;
            }
            if(counter>1)
            printf("%lld^%lld",primes[i],counter);
            else
                printf("%lld",primes[i]);
        }
    }
    if(n>1)
    {
        if(before)
        {
            printf(" *");
        }
        printf(" %lld\n",n);
    }
    else
        printf("\n");
}

int main()
{
    sieve();
    ll n,t,i;
    scanf("%lld",&t);
    while(t--)
    {
        scanf("%lld",&n);
        solve(n);
    }

}
