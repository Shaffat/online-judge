#include<bits/stdc++.h>

using namespace std;

#define ll long long int

struct data{
    int start,h;
};

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    ll n,i,j,mx,area;
    while(scanf("%lld",&n))
    {
        mx=-1;
        if(n==0)
        {
            return 0;
        }
        data tmp;
        stack<data>pile;
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            if(!pile.empty())
            {
                data cur;
                cur=pile.top();
                tmp.h=j;
                tmp.start=i;
                while(cur.h>j)
                {
                    area=cur.h*(i-cur.start);
                    //cout<<"area for removed "<<cur.h<<" ="<<area<<endl;
                    mx=max(area,mx);
                    tmp.start=cur.start;
                    pile.pop();
                    if(!pile.empty())
                    cur=pile.top();
                    else
                        break;
                }
                pile.push(tmp);
            }
            else
            {

              tmp.start=i;
              tmp.h=j;
              pile.push(tmp);

            }
        }
        while(!pile.empty())
        {
            data cur;
            cur=pile.top();
            area=cur.h*(i-cur.start);
            mx=max(area,mx);
            pile.pop();
        }
        printf("%lld\n",mx);
    }
}
