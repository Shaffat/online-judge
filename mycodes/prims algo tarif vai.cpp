#include<bits/stdc++.h>

using namespace std;

struct vertex
{
    int node,weight;
};

bool operator < (vertex x,vertex y)
{
    if(x.weight!=y.weight)
    {
        return x.weight > y.weight;
    }

    return false;
}

int main()
{
    int n,e,i,j,u,v,w;
    scanf("%d %d",&n,&e);
    vector<int>edges[n+1];
    vector<int>cost[n+1];
    vector<int>weights(n+1,1<<29),vis(n+1);
    vector<int>lastupdate(n+1,0);
    for(i=1;i<=e;i++)
    {
        scanf("%d %d %d",&u,&v,&w);
        edges[u].push_back(v);
        edges[v].push_back(u);
        cost[u].push_back(w);
        cost[v].push_back(w);
    }

    vertex start;
    start.node=1;
    start.weight=0;
    weights[1]=0;
    priority_queue<vertex>q;
    q.push(start);
    while(!q.empty())
    {
        vertex cur_top=q.top();
        q.pop();
        int cur_node=cur_top.node;

        if(vis[cur_node]==1)
        {

            continue;
        }
        vis[cur_node]=1;
        for(i=0;i<edges[cur_node].size();i++)
        {
            int nxt_node=edges[cur_node][i];
            int nxt_weight=cost[cur_node][i];

            if(nxt_weight<weights[nxt_node] && vis[nxt_node]==0)
            {

                weights[nxt_node]=nxt_weight;
                vertex nw_node;
                nw_node.node=nxt_node;
                nw_node.weight=nxt_weight;
                q.push(nw_node);
                lastupdate[nxt_node]=cur_node;

            }
        }
    }

    cout<<"selected edges----"<<endl;
    for(i=2;i<=n;i++)
    {
        cout<<i<<" "<<lastupdate[i]<<endl;
    }

    cout<<"minimum spanning tree cost="<<endl;
    int mst=0;
    for(i=1;i<=n;i++)
    {
        mst+=weights[i];
    }
     cout<<mst<<endl;

}
