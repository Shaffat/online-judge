#include<bits/stdc++.h>
using namespace std;


bool ok(vector<int>&products,int p,int k)
{
    int i,j;
    for(i=0;i<products.size();i++)
    {
        if(abs(products[i]-p)>k)
            return false;
    }
    return true;
}


int solve(vector<int>&products,int k)
{
    int first,last,mid,res=-1,d=0,mx=-1,mn=2e9,i;
    for(i=0;i<products.size();i++)
    {
        mx=max(mx,products[i]);
        mn=min(mn,products[i]);
    }
    if(mx-mn>2*k)
        return -1;
    first=(mx+mn)/2;
    last=products[0]+k;
    while(first<=last)
    {
        if(d)
            break;
        if(first==last)
            d++;
        mid=(first+last)/2;
        //cout<<"mid="<<mid<<endl;
        if(ok(products,mid,k))
        {
            //cout<<"ok"<<endl;
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
    int test,t,i,j,n,k;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&k);
        vector<int>products;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            products.push_back(j);
        }
        int res=solve(products,k);
        printf("%d\n",res);
    }
}
