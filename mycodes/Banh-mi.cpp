#include<bits/stdc++.h>
using namespace std;

#define ll unsigned long long int

#define mod 1000000007
void update(ll index,ll n,ll value,vector<ll>&tree)
{
    while(index<=n)
    {

        tree[index]+=value;

        index+=index & (-index);

    }

}
ll query(ll index,vector<ll>&tree)
{

    ll sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index & (-index);
    }
    return sum;
}


ll poweroftwo(ll power)
{
    ll x,cur;
    if(power==0) return 1llu;
    if(power==1) return 2llu;
    if(power%2==0)
    {
        cur=poweroftwo(power/2);
        return (cur*cur)%mod;
    }
    else
    {
        cur=poweroftwo(power/2);
        return (cur*cur*2)%mod;
    }
}


int main()
{
    ios_base::sync_with_stdio(0);
    ll n,i,j,q,ones,zero,l,r,res;
    cin>>n>>q;
    string s;
    cin>>s;
    vector<ll>tree(n+1,0llu);
    for(i=0;i<n;i++)
    {
        if(s[i]=='1')
        {
            update(i+1,n,1,tree);
        }
    }
    for(i=1;i<=q;i++)
    {
        cin>>l>>r;
        ones=query(r,tree)-query(l-1,tree);
        zero=r-l+1;
        //cout<<"zero="<<zero<<endl;
        zero-=ones;
//         cout<<"ones="<<ones<<" zero="<<zero<<endl;
//        cout<<"r="<<r<<" l="<<l<<" m="<<r-l+1<<" one="<<ones<<endl;
        res=poweroftwo(ones)+mod-1llu;
        res%=mod;
        if(zero)
        res+=((poweroftwo(zero)+mod-1llu)%mod*res)%mod;
        cout<<res<<endl;
    }
}
