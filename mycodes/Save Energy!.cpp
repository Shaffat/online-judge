#include<bits/stdc++.h>
using namespace std;

double solve(long long int k,long long int d,long long int t)
{
   double first,last,mid,stopage,extratime,full,hallf,l=d,avgcooktime,avgcooked,total;
   first=0;
   last=2*t;
   if(d>k)
   {
       stopage=d;
       full=k;
   }
   else
   {
       stopage=ceil(k/l)*l;
       full=k;
   }
   avgcooktime=full+(stopage-full)/2;
   while(first<=last)
   {
       mid=(first+last)/2;
       avgcooked=floor(mid/stopage)*avgcooktime;
       extratime=mid-floor(mid/stopage)*stopage;
       total=avgcooked+min(full,extratime)+max(0.000,extratime-full)/2;
       if(abs(total-t)<0.0000000001)
       {
           return mid;
       }
       if(total>t)
       {
           last=mid;
       }
       else
        first=mid;
   }

}


int main()
{
    long long int n,k,d,t;
    double res;
    scanf("%lld %lld %lld",&k,&d,&t);
    printf("%.15lf\n",solve(k,d,t));
}
