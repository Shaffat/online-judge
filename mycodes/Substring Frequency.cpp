#include<bits/stdc++.h>
typedef unsigned long long int llu;
using namespace std;

llu get_hash(string &s, int &m, int &base)
{
    llu value=0,power=1;
    int i,j;
    for(i=m-1;i>=0;i--)
    {
        value+=power*s[i];
        power*=base;
    }
    return value;
}
llu memory[1000000][2];

llu get_power(int &base,int power,int id)
{
    if(power==0)
    {
        return 1;
    }
    if(power==1)
    {
        return base;
    }
    if(memory[power][id]!=0)
    {
        return memory[power][id];
    }
    return memory[power][id]=get_power(base,power/2,id)*get_power(base,power-power/2,id);
}

int solve(string& text,string& pattern)
{
    int i,j,counter=0,base1,base2,m;
    if(pattern.size()>text.size())
    {
        return 0;
    }
    llu power1,power2;
    m=pattern.size();
    base1=31;
    base2=37;
    power1=get_power(base1,m-1,1);
    power2=get_power(base2,m-1,0);
    llu texthash1,texthash2,patternhash1,patternhash2;
    texthash1=get_hash(text,m,base1);
    texthash2=get_hash(text,m,base2);
    patternhash1=get_hash(pattern,m,base1);
    patternhash2=get_hash(pattern,m,base2);
    if(texthash1==patternhash1 && texthash2==patternhash2)
    {
        counter++;
    }
    for(i=m;i<text.size();i++)
    {
        texthash1-=text[i-m]*power1;
        texthash1*=base1;
        texthash1+=text[i];
        texthash2-=text[i-m]*power2;
        texthash2*=base2;
        texthash2+=text[i];
        if(texthash1==patternhash1 && texthash2==patternhash2)
        {
            counter++;
        }
    }
    return counter;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int test,t=1;
    cin>>test;
    string text,pattern;
    for(t=1;t<=test;t++)
    {
        cin>>text>>pattern;
        int  counter;
        counter=solve(text,pattern);
        cout<<"Case "<<t<<": "<<counter<<endl;
    }

    return 0 ;
}
