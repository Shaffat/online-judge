#include<bits/stdc++.h>

using namespace std;


int highestlen(vector<int>&tail,int len,int key)
{
    //cout<<"key="<<key<<" len="<<len<<endl;
    int first,last,mid,d=0,res=0;
    first=1;
    last=len;
    while(first<=last)
    {
        if(d) break;
        if(first==last)
        {
            d++;
        }
        mid=(first+last)/2;
        //cout<<"mid="<<mid<<" tail="<<tail[mid]<<endl;
        if(tail[mid]<key)
        {
            res=mid;
            first=mid+1;
            //cout<<"res="<<res<<endl;
        }
        else
            last=mid-1;
    }
    return res+1;
}

void LIS(vector<int>&v,vector<int>&memory)
{

    int i,j,len=1;
//    for(i=0;i<v.size();i++)
//    {
//        cout<<v[i]<<" ";
//    }
//    cout<<endl;
    vector<int>tail(v.size()+1,1e9);
    tail[len]=v[0];
    memory[0]=len;
    //cout<<"tail of "<<len<<"="<<tail[1]<<" memory[0]="<<memory[0]<<endl;
    for(i=1; i<v.size(); i++)
    {
        //cout<<"i="<<i<<" val="<<v[i]<<endl;
        if(tail[1]>v[i])
        {
            //cout<<"i is smallest val="<<v[i]<<endl;
            tail[1]=v[i];
            //cout<<"tail of 1 is "<<tail[1]<<endl;
            memory[i]=1;
            //cout<<"memory["<<i<<"]="<<memory[i]<<endl;
        }
        else if(tail[len]<v[i])
        {
            //cout<<"i is biggest val="<<v[i]<<endl;
            len++;
            tail[len]=v[i];
            //cout<<"tail of "<<len<<"="<<tail[len]<<endl;
            memory[i]=len;
            //cout<<"memory["<<i<<"]="<<memory[i]<<endl;
        }
        else
        {
            //cout<<"i is middleman val="<<v[i]<<endl;
            int x=highestlen(tail,len,v[i]);
            //cout<<"x="<<x<<endl;
            tail[x]=min(tail[x],v[i]);
            //cout<<"tail of "<<x<<"="<<tail[x]<<endl;
            memory[i]=x;
            //cout<<"memory["<<i<<"]="<<memory[i]<<endl;
        }

    }
    return;
}

int solve(int n,vector<int>&v)
{
    int first,last,i,j,mid,f=0,res=0;
    vector<int>rv;

    for(i=0; i<v.size(); i++)
        rv.push_back(v[i]);
    reverse(rv.begin(),rv.end());
    vector<int>memory(n+1);
    vector<int>rmemory(n+1);
    LIS(v,memory);
    LIS(rv,rmemory);

    for(i=0; i<n; i++)
    {
        int x=n-i-1,y;

        y=min(rmemory[x],memory[i]);
        y*=2;
        y-=1;
        res=max(res,y);
    }
    return res;
}


int main()
{
    int test,t,i,j;
    scanf("%d",&test);
    for(t=1; t<=test; t++)
    {
        int n,i,j,ans;
        scanf("%d",&n);
        vector<int>v;

        for(i=1; i<=n; i++)
        {
            scanf("%d",&j);
            v.push_back(j);
        }
        ans=solve(n,v);

        printf("Case %d: %d\n",t,ans);
    }
}


