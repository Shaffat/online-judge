#include<bits/stdc++.h>

struct pos
{
    int r,c;
};

using namespace std;
int r,c;

int solve(pos start,pos goal,vector<vector<int> >&forest)
{
    vector<int>col(c+1,0);
    vector<vector<int> >dis(r+1,col);
    vector<vector<int> >vis(r+1,col);
    queue<pos>q;
    vis[start.r][start.c]=1;
    int res=-1;
    q.push(start);
    while(!q.empty())
    {
        pos cur=q.front();
        if(cur.r==goal.r && cur.c==goal.c)
        {
            return dis[cur.r][cur.c];
        }
        q.pop();
        pos next;
        next.r=cur.r+1;
        next.c=cur.c;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
         next.r=cur.r-1;
        next.c=cur.c;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r;
        next.c=cur.c+1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r;
        next.c=cur.c-1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r+1;
        next.c=cur.c+1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 &&  forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r+1;
        next.c=cur.c-1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r-1;
        next.c=cur.c+1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
        next.r=cur.r-1;
        next.c=cur.c-1;
        if(next.r<r && next.r>=0 && next.c<c && next.c>=0)
        {
            if(vis[next.r][next.c]==0 && forest[next.r][next.c]==0)
            {
                vis[next.r][next.c]=1;
                dis[next.r][next.c]=dis[cur.r][cur.c]+1;
                q.push(next);
            }
        }
    }
    return -1;
}

void setter(int row,int col, vector<vector<int> >&maze)
{
    maze[row][col]=-1;
    int next_row=row+2;
    int next_col=col+1;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row+2;
    next_col=col-1;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row-2;
    next_col=col+1;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row-2;
    next_col=col-1;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row-1;
    next_col=col+2;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row-1;
    next_col=col-2;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row+1;
    next_col=col+2;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }
    next_row=row+1;
    next_col=col-2;
    if(next_row<r &&next_row>=0 && next_col<c && next_col>=0)
    {
        maze[next_row][next_col]=-1;
    }

}

int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        scanf("%d %d",&r,&c);
        vector<int>col(c+1,0);
        vector<vector<int> >forest(r+1,col);
        int i,j;
        pos start,goal;
        for(i=0;i<r;i++)
        {
            getchar();
            for(j=0;j<c;j++)
            {
                char c;
                scanf("%c",&c);
                if(c=='A')
                {
                    start.r=i;
                    start.c=j;
                }
                else if(c=='B')
                {
                    goal.r=i;
                    goal.c=j;
                }
                else if(c=='Z')
                {
                    setter(i,j,forest);
                }
            }
        }
        forest[start.r][start.c]=0;
        forest[goal.r][goal.c]=0;
        int res=solve(start,goal,forest);
        if(res==-1)
        {
            printf("King Peter, you can't go now!\n");
        }
        else
        {
            printf("Minimal possible length of a trip is %d\n",res);
        }
        t++;
    }
}
