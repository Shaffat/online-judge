/*
ID: msdipu11
PROG: combo
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

bool Ok(vl &v,ll a,ll b,ll c,ll n)
{
//    cout<<"a="<<a<<" b="<<b<<" c="<<c<<" n="<<n<<endl;
//    cout<<"v[0]="<<v[0]<<" v[1]="<<v[1]<<" v[2]="<<v[2]<<endl;
//    cout<<"a dis="<<ABS(v[0]-a)<<" or "<<v[0]+n-a<<endl;
//    cout<<"b dis="<<ABS(v[1]-b)<<" or "<<v[1]+n-b<<endl;
//    cout<<"c dis="<<ABS(v[2]-c)<<" or "<<v[2]+n-c<<endl;
    if(ABS(v[0]-a)<=2 || (min(a,v[0])+n-max(a,v[0]))<=2)
    {
        if(ABS(v[1]-b)<=2 || (min(b,v[1])+n-max(v[1],b))<=2)
        {
            if(ABS(v[2]-c)<=2 || (min(c,v[2])+n-max(v[2],c))<=2)
            {
                return true;
            }
            else
                return false;
        }
        else
            return false;
    }
    else
        return false;

}


int main()
{
    freopen("combo.in","r",stdin);
    freopen("combo.out","w",stdout);
    ll n,i,j,k,counter=0;
    scanf("%lld",&n);
    vl master;
    vl farmer;
    FOR(i,1,3)
    {
        scanf("%lld",&j);
        master.push_back(j);
    }
    FOR(i,1,3)
    {
        scanf("%lld",&j);
        farmer.push_back(j);
    }

    FOR(i,1,n)
    {
        FOR(j,1,n)
        {
            FOR(k,1,n)
            {
                //cout<<i<<" "<<j<<" "<<k<<endl;
                if(Ok(master,i,j,k,n)||Ok(farmer,i,j,k,n))
                {
                    //cout<<"Ok!"<<endl;
                    counter++;
                }
            }
        }
    }
    printf("%lld\n",counter);
}
