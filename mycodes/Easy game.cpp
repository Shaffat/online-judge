#include<bits/stdc++.h>

using namespace std;

long long int solve(long long int start,long long int enD,vector<long long int>&input,vector<vector<long long int> >&memory)
{
    //cout<<"start="<<start<<" end="<<enD<<endl;
    if(start>enD )
    {
        return 0;
    }

    long long int i,j,current=0;
    if(memory[start][enD]!=-2e9)
    {
        return memory[start][enD];
    }
    for(i=start;i<=enD;i++)
    {
        current+=input[i];
        memory[start][enD]=max(current-solve(i+1,enD,input,memory),memory[start][enD]);
    }
    current=0;
    for(i=enD;i>=start;i--)
    {
        current+=input[i];
        memory[start][enD]=max(current-solve(start,i-1,input,memory),memory[start][enD]);
    }
    //cout<<"memory["<<start<<"]["<<enD<<"]="<<memory[start][enD]<<endl;
    return memory[start][enD];



}


int main()
{
    long long int test,t=1,n,i,j;
    scanf("%lld",&test);
    while(t<=test)
    {
        vector<long long int>input;
        scanf("%lld",&n);
        vector<vector<long long int> >memory(n+1);
        for(i=0;i<=n;i++)
        {
            for(j=0;j<=n;j++)
            {
                memory[i].push_back(-2e9);
            }
        }
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            input.push_back(j);
        }
        printf("Case %lld: %lld\n",t,solve(0,n-1,input,memory));
        t++;
    }
}
