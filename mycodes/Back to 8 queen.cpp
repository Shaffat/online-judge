#include<bits/stdc++.h>
using namespace std;

vector< vector<int> >board;

vector< vector<int> >result(94);
int s=0;

vector<int>difference(9);

int moves=2e9;

void addingdowndiagonal(int r,int c)
{
    int a,b;
    a=r-min(r,c);
    b=c-min(r,c);
    while(a!=8 && b!=8)
    {
        if(a==r && b==c)
        {
            a++;
            b++;
            continue;
        }
        board[a][b]+=1;
        a++;
        b++;
    }
}
void removingdowndiagonal(int r,int c)
{
    int a,b;
    a=r-min(r,c);
    b=c-min(r,c);
    while(a!=8 && b!=8)
    {
        if(a==r && b==c)
        {
            a++;
            b++;
            continue;
        }
        board[a][b]-=1;
        a++;
        b++;
    }
}

void addingupdiagoanl(int r,int c)
{
   int d=r+c,a,b;
   if(d>7)
   {
       a=7;
       b=d-7;
   }
   else
   {
       a=d;
       b=0;
   }

   while(a!=-1 && b!=8)
   {
       if(a==r && b==c)
       {
           a--;
           b++;
           continue;
       }
       board[a][b]+=1;
       a--;
       b++;
   }
}

void removingupdiagoanl(int r,int c)
{
   int d=r+c,a,b;
   if(d>7)
   {

       a=7;
       b=d-7;
   }
   else
   {
       a=d;
       b=0;
   }
   while(a!=-1 && b!=8)
   {
       if(a==r && b==c)
       {
           a--;
           b++;
           continue;
       }
       board[a][b]-=1;
       a--;
       b++;
   }
}

void addingrow(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]+=1;
    }
}
void removingrow(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==c)
        {
            continue;
        }
        board[r][i]-=1;
    }
}

void addingcolumn(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]+=1;
    }
}

void removingcolumn(int r,int c)
{
    for(int i=0;i<8;i++)
    {
        if(i==r)
        {
            continue;
        }
        board[i][c]-=1;
    }
}

bool Nqueen(int c,vector<int>&v)
{
    int i,j;
    if(c==8)
    {
        int temp=0;
        vector<int>dummy;
        result.push_back(dummy);

        for(i=0;i<=7;i++)
        {

            result[s].push_back(difference[i]);
        }
        s++;
        return true;
    }
    for(i=0;i<=8;i++)
    {
        if(i==8)
        {
            return false;
        }

        if(board[i][c]==0)
        {
            board[i][c]+=1;
            addingcolumn(i,c);
            addingrow(i,c);
            addingdowndiagonal(i,c);
            addingupdiagoanl(i,c);
            difference[c]=i;
            Nqueen(c+1,v);
            board[i][c]-=1;
            removingrow(i,c);
            removingcolumn(i,c);
            removingupdiagoanl(i,c);
            removingdowndiagonal(i,c);
        }

    }
}

int solve(vector<int>&v)
{
    int minimummove=2e9;
    for(int i=0;i<92;i++)
    {
        int temp=0;
        for(int j=0;j<result[i].size();j++)
        {
            //cout<<"v["<<j<<"]="<< v[j]<<" result["<<j<<"]="<<result[i][j]<<endl;
            if(v[j]!=result[i][j])
            {
                temp++;
            }
        }

        minimummove=min(minimummove,temp);
    }
    return minimummove;
}

int main()
{
    vector<int>v;
    int i,j,f=1;
    for(i=0;i<8;i++)
    {
        board.push_back(v);
        for(j=0;j<8;j++)
        {
            board[i].push_back(0);
        }
    }
    Nqueen(0,v);

    int a,b,c;
    while(scanf("%d",&a)!=EOF)
    {
        v.push_back(a-1);
        for(i=0;i<7;i++)
        {
            scanf("%d",&a);
            v.push_back(a-1);
        }

        printf("Case %d: %d\n",f,solve(v));
        v.clear();
        f++;

    }

}
