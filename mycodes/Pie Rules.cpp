#include<bits/stdc++.h>
using namespace std;
int memory[2][100];

int solve(int decider,int index,vector<int>&pies)
{
    if(index>=pies.size())
    {
        return 0;
    }
    if(memory[decider][index]!=-1)
        return memory[decider][index];
    if(decider)
    {
        int case1=pies[index]-solve(0,index+1,pies);
        int case2=solve(1,index+1,pies)-pies[index];
        return memory[decider][index]=max(case1,case2);
    }
    else
    {
        int case1=pies[index]-solve(1,index+1,pies);
        int case2=solve(0,index+1,pies)-pies[index];
        return memory[decider][index]=max(case1,case2);
    }
}

int main()
{
    int n,i,j=0,k;
    scanf("%d",&n);
    vector<int>pies;
    memset(memory,-1,sizeof(memory));
    for(i=1;i<=n;i++)
    {
        scanf("%d",&k);
        pies.push_back(k);
        j+=k;
    }
    int r=solve(1,0,pies);

    cout<<j-(j+r)/2<<" "<<(j+r)/2<<endl;

}
