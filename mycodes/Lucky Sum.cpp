#include<bits/stdc++.h>
using namespace std;

vector<long long int>lucky;
set<long long int>s;

void generator(int index,long long int sum)
{
    //cout<<"in="<<index<<" "<<sum<<endl;
    if(sum>1e10)
    {
        return;
    }
    if(index>10)
    {
        int pre=s.size();
        if(sum!=0)
        {
            s.insert(sum);
            if(pre!=s.size())
            {
                lucky.push_back(sum);
            }
        }

        return;
    }
    generator(index+1,sum);
    generator(index+1,sum*10+4);
    generator(index+1,sum*10+7);
}

int main()
{
    generator(0,0);
    sort(lucky.begin(),lucky.end());
    lucky.insert(lucky.begin(),0);
    long long int l,r,sum=0,i,counter;

    cin>>l>>r;
    for(i=1;i<lucky.size();i++)
    {
        //cout<<"number="<<lucky[i]<<endl;
        if(lucky[i]<l)
        {
            //cout<<"skipped ="<<lucky[i]<<endl;
            continue;
        }
        else if(lucky[i]>=r)
        {
            if(sum==0)
            {
                counter=(min(lucky[i],r)-max(lucky[i-1],l))+1;
            }
            else
                counter=(min(lucky[i],r)-max(lucky[i-1],l));
            //cout<<"counter="<<counter<<" first="<<max(lucky[i-1],l)<<" last="<<min(lucky[i],r)<<endl;
            sum+=counter*lucky[i];
            break;
        }
        else
        {
            if(sum==0)
            {
                counter=(min(lucky[i],r)-max(lucky[i-1],l))+1;
            }
            else
                counter=(min(lucky[i],r)-max(lucky[i-1],l));
            //cout<<"counter="<<counter<<" first="<<max(lucky[i-1],l)<<" last="<<min(lucky[i],r)<<endl;
            sum+=counter*lucky[i];
        }
    }

    cout<<sum<<endl;

}
