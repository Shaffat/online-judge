#include<bits/stdc++.h>

using namespace std;

bool ar[100001];
void sieve(int N)
{
    int i;
    for(i=1;i<=N;i++)
    {
        ar[i]=0;
    }
    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}
int main()
{
    sieve(100000);
    string s;
    while(cin>>s)
    {
        if(s=="0")
        {
            break;
        }
        int i,j,chk=0,k,mx=0;
        for(j=s.size()-1;j>=4;j--)
        {
            int f=s.at(j)-48;
            if(f%2==0)
            {
                continue;
            }
            int c=1,sum=0;
            for(i=j;i>=j-4;i--)
            {
                k=s.at(i)-48;
                sum+=k*c;
                c*=10;
            }
            if(ar[sum]==0)
            {
                mx=max(mx,sum);
                chk=1;
            }
        }
        if(chk==0)
        {
            for(j=s.size()-1;j>=3;j--)
        {
            int f=s.at(j)-48;
            if(f%2==0)
            {
                continue;
            }
            int c=1,sum=0;
            for(i=j;i>=j-3;i--)
            {
                k=s.at(i)-48;
                sum+=k*c;
                c*=10;
            }
            if(ar[sum]==0)
            {
                mx=max(mx,sum);
                chk=1;
            }
        }
        }
        if(chk==0)
        {
            for(j=s.size()-1;j>=2;j--)
        {
            int f=s.at(j)-48;
            if(f%2==0)
            {
                continue;
            }
            int c=1,sum=0;
            for(i=j;i>=j-2;i--)
            {
                k=s.at(i)-48;
                sum+=k*c;
                c*=10;
            }
            if(ar[sum]==0)
            {
                mx=max(mx,sum);
                chk=1;
            }
        }
        }
        if(chk==0)
        {
            for(j=s.size()-1;j>=1;j--)
        {
            int f=s.at(j)-48;
            if(f%2==0)
            {
                continue;
            }
            int c=1,sum=0;
            for(i=j;i>=j-1;i--)
            {
                k=s.at(i)-48;
                sum+=k*c;
                c*=10;
            }
            if(ar[sum]==0)
            {
                mx=max(mx,sum);
                chk=1;
            }
        }
        }
        if(chk==0)
        {
            for(i=s.size()-1;i>=0;i--)
            {
                k=s.at(i)-48;
                if(ar[k]==0)
                {
                    mx=max(mx,k);
                }
            }
        }
        printf("%d\n",mx);



    }
}

