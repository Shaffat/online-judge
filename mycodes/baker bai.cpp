#include<bits/stdc++.h>
using namespace std;

#define neg_infinite -2e9;
int m,n;
int solve(int r1, int c1,int r2,int c2,vector< vector<int> >&matrix,vector< vector <vector<int> > >&memory)
{
    // printf("%d %d %d %d\n",r1,c1,r2,c2);
    if(r1>m||r1<1||r2>m||r2<1||c1>n||c1<1||c2>n||c2<1)
    {
        return neg_infinite;
    }
    //cout<<"here"<<endl;
    if(r1==r2&&c1==c2)
    {
        if(r1==m && c1==n)
        {
            return matrix[m][n];
        }
        else
            return neg_infinite;
    }
    if(memory[r1][c1][r2]!=-1)
    {
        return memory[r1][c1][r2];
    }
    int cur=matrix[r1][c1]+matrix[r2][c2];
    int curresult=max(max(solve(r1,c1+1,r2,c2+1,matrix,memory),solve(r1,c1+1,r2+1,c2,matrix,memory)),max(solve(r1+1,c1,r2,c2+1,matrix,memory),solve(r1+1,c1,r2+1,c2,matrix,memory)));
    return memory[r1][c1][r2]=curresult+cur;


}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j;
        scanf("%d %d",&m,&n);
        vector<int>column(n+1);
        vector<vector<int> >matrix(m+1,column);
        for(i=1;i<=m;i++)
        {
            for(j=1;j<=n;j++)
            {
                scanf("%d",&matrix[i][j]);
            }
        }
        vector<int>value(m+1,-1);
        vector<vector<int> >columnmem(n+1,value);
        vector<vector<vector<int> > >memory(m+1,columnmem);

        int result=solve(2,1,1,2,matrix,memory)+matrix[1][1];
        printf("Case %d: %d\n",t,result);
        t++;
    }
}


