#include<bits/stdc++.h>
using namespace std;

struct info
{
    long long int id,val;
};

bool operator <(info a,info b)
{
    if(a.val!=b.val)
        return a.val>b.val;
    return a.id>b.id;
}

int main()
{
    long long int n,m,i,j,k,c,t,r,total=0,cur,got;
    scanf("%lld %lld",&n,&m);
    vector<long long int>cost(n+1);
    vector<long long int>dishes(n+1);
    priority_queue<info>q;
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        dishes[i]=j;
    }
    for(i=1;i<=n;i++)
    {
        scanf("%lld",&j);
        cost[i]=j;
        info tmp;
        tmp.id=i;
        tmp.val=j;
        q.push(tmp);
    }
    for(i=1;i<=m;i++)
    {
        //cout<<"i="<<i<<endl;
        scanf("%lld %lld",&t,&r);
        //cout<<"serving  "<<t<<" need "<<r<<endl;
        cur=0;
        got=min(dishes[t],r);
        dishes[t]-=got;
        cur+=cost[t]*got;
        r-=got;

//        cout<<"got "<<got<<" need "<<r<<endl;
//        cout<<"cur="<<cur<<endl;
        while(r>0 && !q.empty())
        {
            int top=q.top().id;
            got=min(dishes[top],r);
            //cout<<"cheapest is "<<top<<" got "<<got<<endl;
            cur+=got*cost[top];
            dishes[top]-=got;
            //cout<<"dishes left="<<dishes[top]<<endl;
            if(dishes[top]<=0){
                q.pop();
                //cout<<"no dishes left"<<endl;
            }
            r-=got;
        }
        //cout<<"last r="<<r<<endl;
        if(r>0){
            cur=0;
        }
        printf("%lld\n",cur);
    }

}
