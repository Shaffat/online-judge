
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define llu unsigned long long int
#define vi vector<int>
#define vii vector<vector<int> >
#define vl vector<ll>
#define vll vector<vector<ll> >
#define vlu vector<llu>
#define vllu vector<vector<llu> >
#define pb              push_back
#define PI              acos(-1.0)
#define sc1(a)          scanf("%lld",&a)
#define sc2(a,b)        scanf("%lld %lld",&a,&b)
#define sc3(a,b,c)      scanf("%lld %lld %lld",&a,&b,&c)
#define scd1(a)         scanf("%llf",&a)
#define scd2(a,b)       scanf("%llf %llf",&a,&b)
#define scd3(a,b,c)     scanf("%llf %llf %llf",&a,&b,&c)
#define min3(a,b,c)     min(a,min(b,c))
#define max3(a,b,c)     max(a,max(b,c))
#define min4(a,b,c,d)   min(a,min(b,min(c,d)))
#define max4(a,b,c,d)   max(a,max(b,max(c,d)))
#define SQR(a)          ((a)*(a))
#define FOR(i,a,b)      for(ll i=a;i<=b;i++)
#define ROF(i,a,b)      for(ll i=a;i>=b;i--)
#define MEM(a,x)        memset(a,x,sizeof(a))
#define ABS(x)          ((x)<0?-(x):(x))
#define SORT(v)         sort(v.begin(),v.end())
#define REV(v)          reverse(v.begin(),v.end())

int main()
{
    ll w,s,c,k,nonzero=0,doubleCross=0,x,y,melon=0;
    sc2(w,s);
    sc2(c,k);
    if(w>0) nonzero++;
    if(s>0) nonzero++;
    if(c>0) nonzero++;

    if(w>k && w<=(2*k))
    {
        x=s;
        y=c;
    }
    if(s>k && s<=(2*k))
    {
        x=w;
        y=c;
    }
    if(c>k && c<=(2*k))
    {
        x=s;
        y=w;
    }

    if(w>(2*k)){
        doubleCross++;
        x=s;
        y=c;
    }
    if(s>(2*k)){
        doubleCross++;
        x=w;
        y=c;
    }
    if(c>(2*k)){
        doubleCross++;
        x=w;
        y=s;
    }


    if(k==0)
        printf("NO\n");
    else if(s<=k && w+c<=(2*k))
    {
        printf("YES\n");
    }

    else if(nonzero==1)
    {
        printf("YES\n");
    }
    else if(w<=k && c<=k && s<=k)
        printf("YES\n");
    else if(doubleCross){
        if(x+y<k)
        printf("YES\n");
        else
        printf("NO\n");
    }
    else if(x+y<=k)
    {
        printf("YES\n");
    }
    else
        printf("NO\n");


}
