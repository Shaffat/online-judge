#include<bits/stdc++.h>
using namespace std;


struct node
{
    int id,flow;
};

struct career
{
    int id,counter;
};

bool operator <(career a,career b)
{
    if(a.counter!=b.counter)
    {
        return a.counter>b.counter;
    }
    return false;
}

int send_flow(int source,int sink,int n,vector<vector<int> >&graph,vector<vector<int> >&capacity)
{
    int i,j,res=0;
    node cur;
    vector<int>path(n+1,-1);
    cur.id=source;
    cur.flow=1e9;
    queue<node>q;
    q.push(cur);
    while(!q.empty())
    {
        cur=q.front();
        //cout<<"node="<<cur.id<<" flow="<<cur.flow<<endl;
        q.pop();
        if(cur.id==sink)
        {
            res=cur.flow;
            break;
        }
        for(i=0;i<graph[cur.id].size();i++)
        {
            int nxt=graph[cur.id][i];
            if(path[nxt]==-1 && capacity[cur.id][nxt]>0)
            {
                //cout<<"found "<<nxt<<" from "<<cur.id<<endl;
                path[nxt]=cur.id;
                node tmp;
                tmp.id=nxt;
                tmp.flow=min(cur.flow,capacity[cur.id][nxt]);
                q.push(tmp);
            }
        }

    }
    //cout<<"res="<<res<<endl;
    if(res>0)
    {
        int child,parent;
        child=sink;
        //cout<<child<<"->";
        while(child!=source)
        {
            parent=path[child];
            //cout<<parent<<"->";
            capacity[parent][child]-=res;
            capacity[child][parent]+=res;
            child=parent;
        }
        //cout<<"0\n";
    }
    //cout<<"done\n";
    return res;
}

void getEdge(int teamid,vector<vector<int> >&connection,vector<vector<int> >&capacity,string &one,string &two, string &three)
{
    int i,j;
    career tmp;
    vector<career>v(100,tmp);
    for(i=0;i<100;i++)
    {
        tmp.id=i;
        tmp.counter=0;
        v[i]=tmp;
    }
    for(i=0;i<one.size();i++)
    {
        tmp.id=one[i];
        v[tmp.id].counter++;
    }
    for(i=0;i<two.size();i++)
    {
        tmp.id=two[i];
        v[tmp.id].counter++;
    }
    for(i=0;i<three.size();i++)
    {
        tmp.id=three[i];
        v[tmp.id].counter++;
    }
    sort(v.begin(),v.end());
    int mx=v[0].counter;
    for(i=0;i<v.size();i++)
    {
        if(v[i].counter!=mx) break;

        connection[teamid].push_back(v[i].id);
        connection[v[i].id].push_back(teamid);
        capacity[teamid][v[i].id]=1;
        capacity[v[i].id][teamid]=0;
        //cout<<"connection between "<<teamid<<" and "<<v[i].id<<endl;
    }
    return;
}


void setSourceSink(int k,int n,vector<vector<int> >&connection,vector<vector<int> >&capacity)
{
    int i,j;
    for(i=1;i<=n;i++)
    {
        connection[0].push_back(i);
        connection[i].push_back(0);
        capacity[0][i]=1;
        capacity[i][0]=0;
    }
    for(i='A';i<='Z';i++)
    {
        connection[i].push_back(101);
        connection[101].push_back(i);
        capacity[i][101]=k;
        capacity[101][i]=0;
    }
    return;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n,i,j,k;
    vector<vector<int> >connection(110);
    vector<int>col(110,0);
    vector<vector<int> >capacity(110,col);
    cin>>n;
    for(i=1;i<=n;i++)
    {
        string s1,s2,s3;
        cin>>s1>>s2>>s3;
        getEdge(i,connection,capacity,s1,s2,s3);
    }
    cin>>k;
    setSourceSink(k,n,connection,capacity);

    int mxflow=0,curflow;
    while(1)
    {
        curflow=send_flow(0,101,110,connection,capacity);
        if(curflow==0) break;
        mxflow+=curflow;
    }
    cout<<mxflow<<endl;
}
