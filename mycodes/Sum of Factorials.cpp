#include<bits/stdc++.h>
using namespace std;
typedef long long int ll;
vector<ll>values;

int main()
{
    long long int i,j,n,test,t=1,factorial=1,cur;
    int k=20;
    values.push_back(1);
    for(i=1;i<=20;i++)
    {
        factorial*=i;
        values.push_back(factorial);
    }

    scanf("%lld",&test);
    while(t<=test)
    {
        scanf("%lld",&n);
        vector<int>v(30,0);
        int ok=0;
        for(i=20;i>=0;i--)
        {
            if(values[i]<=n)
            {
                v[i]=1;
                n-=values[i];
            }
        }
        printf("Case %lld: ",t);
        if(n==0) ok=1;
        int before=0;
        if(ok)
        {
            for(i=0;i<v.size();i++)
            {
                if(v[i]){
                    if(before) printf("+");
                    printf("%d!",i);
                    before=1;
                }
            }
            printf("\n");
        }
        else
            printf("impossible\n");
        t++;
    }


}
