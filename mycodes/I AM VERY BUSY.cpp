#include<bits/stdc++.h>

using namespace std;

struct scedule
{
    int s,f;
};

bool operator <(scedule a,scedule b)
{
    if(a.f!=b.f)
    {
        return a.f<b.f;
    }
    else
        return false;
}

int main()
{
     int t,n,sta,en;

     scanf("%d",&t);

     while(t--)
     {
         int n;
         scanf("%d",&n);
         int i,j,job=0,s,f;
         vector<scedule>selection;
         for(i=1;i<=n;i++)
         {
             scanf("%d %d",&s,&f);
             scedule tmp;
             tmp.s=s;
             tmp.f=f;
             selection.push_back(tmp);
         }
         sort(selection.begin(),selection.end());
         int previous_finish=-1;
         for(i=0;i<selection.size();i++)
         {
             if(selection[i].s>=previous_finish)
             {
                 job++;
                 previous_finish=selection[i].f;
             }
         }
         printf("%d\n",job);
     }
}
