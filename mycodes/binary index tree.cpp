#include<bits/stdc++.h>

using namespace std;

void update(int index,int n,int value,vector<int>&tree)
{
    while(index<=n)
    {

        tree[index]+=value;

        index+=index & (-index);

    }

}
int query(int index,vector<int>&tree)
{

    int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index & (-index);
    }
    return sum;
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,i,j,q;
        scanf("%d %d",&n,&q);
        vector<int>tree(n+1,0);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            update(i,n,j,tree);
        }
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            int a,u,v;
            scanf("%d",&a);
            if(a==1)
            {

                 scanf("%d",&u);
                 u++;
                 int res=query(u,tree)-query(u-1,tree);
                 printf("%d\n",res);
                 res*=-1;
                 update(u,n,res,tree);
            }
            if(a==2)
            {

                scanf("%d %d",&u,&v);
                u++;
                update(u,n,v,tree);

            }
            if(a==3)
            {
                scanf("%d %d",&u,&v);
                u++;v++;
                int res=query(v,tree)-query(u-1,tree);
                printf("%d\n",res);
            }
        }
        t++;
    }
}
