#include<bits/stdc++.h>
using namespace std;
struct info
{
    int nxt;
    bool finish;
};
void build_tree(int &ans,string &s,vector<vector<info> >&tree)
{
    int i,j,pos=0,newnode=0;
    for(i=0;i<s.size();i++)
    {
        if(tree[pos][s[i]].nxt==-1)
        {
            newnode=1;
            info tmp;
            tmp.nxt=-1;
            tmp.finish=0;
            tree[pos][s[i]].nxt=tree.size();
            vector<info>v(130,tmp);
            tree.push_back(v);
        }
        if(tree[pos][s[i]].finish==1)
        {
            ans=1;
        }
        if(i==s.size()-1)
        {
            if(!newnode) ans=1;
            tree[pos][s[i]].finish=1;
        }
        pos=tree[pos][s[i]].nxt;
    }
    return;
}


int main()
{
    ios_base::sync_with_stdio(0);
    int n,i,j,t,ans;
    string s;
    cin>>t;
    while(t--)
    {
        cin>>n;
        ans=0;
        info tmp;
        tmp.nxt=-1;
        tmp.finish=0;

        vector<vector<info> >tree;
        vector<info>v(130,tmp);
        tree.push_back(v);
        for(i=1;i<=n;i++)
        {
            cin>>s;
            build_tree(ans,s,tree);
        }
        if(ans)
        {
            cout<<"NO"<<endl;
        }
        else
            cout<<"YES"<<endl;
    }
}
