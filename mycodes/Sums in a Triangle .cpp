#include<bits/stdc++.h>
using namespace std;

int solve(int row,int col,int n,vector<vector<int> >&triangle,vector<vector<int> >&memory)
{
    if(row>n)
        return 0;
    if(memory[row][col]!=-1)
        return memory[row][col];
    int res1=triangle[row][col],res2=triangle[row][col];
    res1+=solve(row+1,col,n,triangle,memory);
    res2+=solve(row+1,col+1,n,triangle,memory);
    return memory[row][col]=max(res1,res2);
}


int main()
{
    int n,i,j,k,test,res;
    scanf("%d",&test);
    while(test--)
    {
        scanf("%d",&n);
        vector<int>col(n+10,-1);
        vector<vector<int> >memory(n+10,col);
        vector<int>col1(n+10,0);
        vector<vector<int> >triangle(n+10,col1);
        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
            {
                scanf("%d",&k);
                triangle[i][j]=k;
            }
        }
        res=solve(1,1,n,triangle,memory);
        printf("%d\n",res);
    }
}
