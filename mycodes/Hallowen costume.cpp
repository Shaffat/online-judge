#include<bits/stdc++.h>
using namespace std;
vector<int>v(100,-1);
vector<vector<int> >memorytracer(100,v);
vector<vector<int> >memory(100,v);
int testcase;
int solve(int start, int last,vector<int>&party)
{
    //cout<<"start "<<start<<" last "<<last<<endl;
    if(last<start)
    {
        return 0;
    }
    if(start==last)
    {
        return 1;
    }
    if(memorytracer[start][last]==testcase)
    {
        return memory[start][last];
    }
    int case1=1+solve(start+1,last,party);
    int case2=2e9;
    int i,j,cur=party[start];
    //cout<<"before loop start="<<start<<" last="<<last<<" cur="<<cur<<endl;

    for(i=start+1;i<party.size();i++)
    {

        if(party[i]==cur)
        {
            //cout<<"cur="<<cur<<" start="<<start<<" match found at="<<i<<endl;
            case2=min(case2,1+solve(start+1,i-1,party)+solve(i,last,party)-1);
        }
    }
    memorytracer[start][last]=testcase;
    memory[start][last]=min(case1,case2);
    //cout<<" RESULT OF MEMORY["<<start<<"]["<<last<<"]="<<memory[start][last]<<endl;
    return memory[start][last];

}


int main()
{
    int i,j,t,p;
    testcase=1;
    scanf("%d",&t);
    while(testcase<=t)
    {
        vector<int>party;
        scanf("%d",&p);
        for(i=1;i<=p;i++)
        {
            scanf("%d",&j);
            party.push_back(j);
        }
        int result=solve(0,party.size()-1,party);
        printf("Case %d: %d\n",testcase,result);
        testcase++;
    }

}
