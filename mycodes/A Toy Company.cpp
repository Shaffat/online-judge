#include<bits/stdc++.h>

using namespace std;


bool is_forbidden(string &s,vector<vector<vector<int> > >&forbidden,int &n)
{
    int i,j,k;
    for(i=0;i<n;i++)
    {
        if(forbidden[i][0][s[0]] && forbidden[i][1][s[1]] && forbidden[i][2][s[2]])
        {
            return true;
        }
    }
    return false;
}

int solve(string &s,string &d,int &n,vector<vector<vector<int> > >&forbidden)
{
    queue<string>q;
    map<string,int>mp,dis;
    mp[s]=1;
    dis[s]=0;
    q.push(s);
    int i,j;
    while(!q.empty())
    {
        string cur=q.front(),tmp;
        q.pop();
        //cout<<"cur="<<cur<<endl;
        if(cur==d)
        {
            return dis[cur];
        }
        for(i=0;i<cur.size();i++)
        {
            tmp=cur;
            if(tmp[i]=='a')
            {
                tmp[i]='z';
            }
            else
                tmp[i]-=1;
            if(!mp.count(tmp) && !is_forbidden(tmp,forbidden,n))
            {
                mp[tmp]=1;
                q.push(tmp);
                dis[tmp]=dis[cur]+1;
            }
            tmp=cur;
            if(tmp[i]=='z')
            {
                tmp[i]='a';
            }
            else
                tmp[i]+=1;
            if(!mp.count(tmp) && !is_forbidden(tmp,forbidden,n))
            {
                mp[tmp]=1;
                q.push(tmp);
                dis[tmp]=dis[cur]+1;
            }
        }
    }
    return -1;
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    string s,d,tmp;
    int i,j,n,test,t,k;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int res;
        vector<int>col(130,0);
        vector<vector<int> >col2(3,col);
        cin>>s>>d>>n;
        vector<vector<vector<int> > >forbidden(50,col2);
        for(i=0;i<n;i++)
        {
            for(j=0;j<3;j++)
            {
                cin>>tmp;
                for(k=0;k<tmp.size();k++)
                {
                    forbidden[i][j][tmp[k]]=1;
                }
            }
        }
        if(is_forbidden(s,forbidden,n)||is_forbidden(d,forbidden,n))
        {
            res=-1;
        }
        else
            res=solve(s,d,n,forbidden);
        printf("Case %d: %d\n",t,res);
    }

}
