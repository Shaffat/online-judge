#include<bits/stdc++.h>
using namespace std;


int t,n,q,k,r,test;

int enemy[15][15];


int memory[4100][15][15];
int testcase[4100][15][15];

int solve(int bitmask,int last,int remains)
{
    //cout<<"bitmask="<<bitmask<<" last="<<last<<" remains="<<remains<<endl;
    if(remains<0) return 0;

    if(bitmask==((1<<n)-1)) return 1;

    if(testcase[bitmask][last][remains]==t) {
            return memory[bitmask][last][remains];
    }
    int i,f,total=0;
    for(i=0;i<n;i++)
    {
        if((bitmask&(1<<i))==0){
            f=0;
            if(enemy[i][last]==t) f=1;
            total+=solve(bitmask|(1<<i),i,remains-f);
        }
    }
    testcase[bitmask][last][remains]=t;
    return memory[bitmask][last][remains]=total;
}



int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    memset(memory, -1, sizeof(memory));
    memset(testcase, -1, sizeof(testcase));
    memset(enemy, -1, sizeof(enemy));

    int i,j,k,l,m,o=11,e,q,res;

    scanf("%d",&test);

    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d",&n,&k,&q);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&m);
            for(j=1;j<=m;j++)
            {
                scanf("%d",&e);
                enemy[i-1][e-1]=t;
            }
        }
        int total[14];
        memset(total, 0, sizeof(total));
        for(i=0;i<n;i++)
        {
            for(j=0;j<=11;j++)
            {
                total[j]+=solve(1<<i,i,j);
            }
        }
        int mx=1;
        for(i=1;i<=n;i++)
        {
            mx*=i;
        }

        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            res=0;
            scanf("%d",&r);
            if(k!=0){
            r=min(o,r/k);
            res=total[r];
            }
            else
                res=mx;
            printf("%d\n",res);
        }
    }
//    cerr << testcase.size() << endl ;
//    cerr << testcase[0].size() << endl ;

    return 0 ;
}
