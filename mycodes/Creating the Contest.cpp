#include<bits/stdc++.h>
using namespace std;
void find_break(vector<int>&pblm,int &first,int &last)
{
    first=-1;
    last=-1;
    for(int i=0;i<pblm.size()-1;i++)
    {
        if(2*pblm[i]<pblm[i+1])
        {
            if(first==-1)
            {
                first=i;
                last=i;
            }
            else
                last=i;
        }
    }
    return;
}
int main()
{
    int n,i,j,first,last,res=-1;
    scanf("%d",&n);
    vector<int>pblm;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        pblm.push_back(j);
    }
    vector<int>::iterator l;
    for(i=0;i<pblm.size();i++)
    {
        j=2*pblm[i];
        l=upper_bound(pblm.begin(),pblm.end(),j);
        last=l-pblm.begin();
        //cout<<"i="<<i<<" last="<<last<<endl;

        res=max(res,last-i);
        //cout<<"res="<<res<<endl;
    }
    cout<<res<<endl;
}
