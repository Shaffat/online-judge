#include<bits/stdc++.h>
using namespace std;
#define lli unsigned long long int
void solve(lli &t1,lli &k1,lli &n,vector<lli>&arrival,vector<lli>&arrival2,queue<lli>&door1)
{
     for(lli i=0;i<n;i++)
    {
        //cout<<"busy window="<<door1.size()<<endl;
        if(door1.size()<k1)
        {
            door1.push(arrival[i]+t1);
            arrival2.push_back(arrival[i]+t1);
            //cout<<"i will out at "<<arrival2.back()<<endl;
        }
        else
        {
            int next=door1.front();
            //cout<<"all window busy"<<endl;
            //cout<<"first window free at "<<next<<" my arrival="<<arrival[i]<<endl;
            if(next<arrival[i])
            {
               // cout<<"got it free"<<endl;
               door1.pop();
               arrival2.push_back(arrival[i]+t1);
               door1.push(arrival[i]+t1);

              // cout<<"i will out at "<<arrival2.back()<<endl;

            }
            else
            {
               // cout<<"i will wait"<<endl;
                door1.pop();
                door1.push(next+t1);
                arrival2.push_back(next+t1);
               // cout<<"i will out at "<<arrival2.back()<<endl;
            }
        }
    }

}
void printer(vector<lli>v)
{
    for(int i=0;i<v.size();i++)
    {
        cout<<v[i]<<" ";
    }
    cout<<endl;
}
int main()
{
    unsigned long long int i,j,k1,k2,k3,t1,t2,t3,n,c,mx=0;
    vector<unsigned long long int>arrival,arrival2,arrival3;
    vector<unsigned long long int>out;
    queue<unsigned long long int>door1,door2,door3;
    scanf("%llu %llu %llu %llu %llu %llu %llu",&k1,&k2,&k3,&t1,&t2,&t3,&n);
    for(i=1;i<=n;i++)
    {
        scanf("%llu",&j);
        arrival.push_back(j);
    }
    solve(t1,k1,n,arrival,arrival2,door1);
    //cout<<"arrival2"<<endl;
    //printer(arrival2);
    solve(t2,k2,n,arrival2,arrival3,door2);
    //cout<<"arrival3"<<endl;
    //printer(arrival3);
    solve(t3,k3,n,arrival3,out,door3);
    //cout<<"out"<<endl;
    //printer(out);
    for(i=0;i<n;i++)
    {
        lli wait=abs(out[i]-arrival[i]);
        mx=max(mx,wait);
    }
    cout<<mx<<endl;

}
