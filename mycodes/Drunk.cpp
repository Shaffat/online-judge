#include<bits/stdc++.h>
using namespace std;

bool dfs(int source,vector<int>colour,vector<vector<int> >&connection)
{
    //cout<<"source="<<source<<endl;
    int i,j=1;
    colour[source]=1;
    for(i=0;i<connection[source].size();i++)
    {
        int next=connection[source][i];
        //cout<<"next="<<next<<" source="<<source<<endl;
        if(colour[next]==1)
        {
            return 0;
        }
        else if(colour[next]==0)
        {
           if(dfs(next,colour,connection)==0)
           {
               //cout<<"after visiting next="<<next<<" result= 0"<<endl;
               return 0;
           }
        }

    }
    colour[source]=2;
    return 1;

}
int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        map<string,int>prerequisite;
        int n,i,j,cnt=0;
        scanf("%d",&n);
        vector<int>v;
        vector<vector<int> >connection(10001,v);
        for(i=1;i<=n;i++)
        {
            string a,b;
            int u,v;
            cin>>a>>b;
            if(prerequisite.count(a)==0)
            {
                cnt++;
                prerequisite[a]=cnt;
                u=cnt;
            }
            else
            {
                u=prerequisite[a];
            }

            if(prerequisite.count(b)==0)
            {
                cnt++;
                prerequisite[b]=cnt;
                v=cnt;
            }
            else
            {
                v=prerequisite[b];
            }
            connection[u].push_back(v);
        }
        vector<int>colour(cnt+1,0);
        int chk=1;
        for(int i=1;i<=cnt;i++)
        {
            if(colour[i]==0 && chk==1)
            {
                chk=dfs(i,colour,connection);
            }
            else if(chk==0)
            {
                break;
            }
        }

        if(chk)
        {
            printf("Case %d: Yes\n",t);
        }
        else
            printf("Case %d: No\n",t);
        t++;

    }

}
