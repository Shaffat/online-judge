#include<bits/stdc++.h>
using namespace std;
typedef long long int ll;

struct nodes
{
    int row,col,cost;
};

bool operator <(nodes a, nodes b)
{
    if(a.cost!=b.cost)
    {
        return a.cost<b.cost;
    }
    return false;
}
bool isvalid(int row,int col,int r)
{
    if(row>r ||row<1||col>3 || col <1)
    {
        return false;
    }
    return true;
}

ll djstra(int row,int col,vector<vector<ll> >&graph,int r)
{
    vector<ll>initial(4,2e10);
    vector< vector<ll> >shortestdis(r+1,initial);
    priority_queue<nodes>djsktra;
    nodes first;
    first.row=row;
    first.col=col;
    shortestdis[row][col]=graph[row][col];

    djsktra.push(first);
    while(!djsktra.empty())
    {
        nodes CurrentRoot=djsktra.top();
        djsktra.pop();

        if(isvalid(CurrentRoot.row,CurrentRoot.col+1,r))
        {
            if(shortestdis[CurrentRoot.row][CurrentRoot.col+1]>
               shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row][CurrentRoot.col+1])
            {
                shortestdis[CurrentRoot.row][CurrentRoot.col+1]=
                shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row][CurrentRoot.col+1];
                nodes tmp;
                tmp.row=CurrentRoot.row;
                tmp.col=CurrentRoot.col+1;
                tmp.cost=graph[tmp.row][tmp.col];
                //cout<<"short dis of ["<<tmp.row<<"]["<<tmp.col<<"]="<<shortestdis[tmp.row][tmp.col]<<endl;
                djsktra.push(tmp);
            }
        }
        if(isvalid(CurrentRoot.row+1,CurrentRoot.col+1,r))
        {
            if(shortestdis[CurrentRoot.row+1][CurrentRoot.col+1]>
               shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col+1])
            {
                shortestdis[CurrentRoot.row+1][CurrentRoot.col+1]=
                shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col+1];
                nodes tmp;
                tmp.row=CurrentRoot.row+1;
                tmp.col=CurrentRoot.col+1;
                tmp.cost=graph[tmp.row][tmp.col];
                //cout<<"short dis of ["<<tmp.row<<"]["<<tmp.col<<"]="<<shortestdis[tmp.row][tmp.col]<<endl;
                djsktra.push(tmp);
            }
        }
         if(isvalid(CurrentRoot.row+1,CurrentRoot.col,r))
        {
            if(shortestdis[CurrentRoot.row+1][CurrentRoot.col]>
               shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col])
            {
                shortestdis[CurrentRoot.row+1][CurrentRoot.col]=
                shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col];
                nodes tmp;
                tmp.row=CurrentRoot.row+1;
                tmp.col=CurrentRoot.col;
                tmp.cost=graph[tmp.row][tmp.col];
                djsktra.push(tmp);
                //cout<<"short dis of ["<<tmp.row<<"]["<<tmp.col<<"]="<<shortestdis[tmp.row][tmp.col]<<endl;
            }
        }
         if(isvalid(CurrentRoot.row+1,CurrentRoot.col-1,r))
        {
            if(shortestdis[CurrentRoot.row+1][CurrentRoot.col-1]>
               shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col-1])
            {
                shortestdis[CurrentRoot.row+1][CurrentRoot.col-1]=
                shortestdis[CurrentRoot.row][CurrentRoot.col]+graph[CurrentRoot.row+1][CurrentRoot.col-1];
                nodes tmp;
                tmp.row=CurrentRoot.row+1;
                tmp.col=CurrentRoot.col-1;
                tmp.cost=graph[tmp.row][tmp.col];
                djsktra.push(tmp);
                //cout<<"short dis of ["<<tmp.row<<"]["<<tmp.col<<"]="<<shortestdis[tmp.row][tmp.col]<<endl;
            }
        }

    }
    return shortestdis[r][2];

}


int main()
{
    int r,t=1;
    while(scanf("%d",&r))
    {
        if(r==0)
        {
            break;
        }
        int i,j;
        vector<ll>in(4);
        vector< vector<ll> >matrix(r+1,in);
        for(i=1;i<=r;i++)
        {
            for(j=1;j<=3;j++)
            {
                scanf("%lld",&matrix[i][j]);
            }
        }
        ll result=djstra(1,2,matrix,r);
        printf("%d. %lld\n",t,result);
        t++;
    }

}
