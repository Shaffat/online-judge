#include<bits/stdc++.h>
using namespace std;
vector<double>memory(1e5 +1,-1);
double solve(int n)
{
    if(n==1)
    {
        return 0;
    }
    if(memory[n]!=-1)
    {
        return memory[n];
    }
    int i,j;
    double d=2,total=0;
    for(i=2;i<=sqrt(n);i++)
    {
        if(n%i==0)
        {
            d++;
            double ans=solve(i);
            total+=ans;
             if(n/i!=i)
            {
                d++;
                double ans=solve(n/i);
                total+=ans;
            }
        }

    }
    total+=d;
    total/=(d-1);
    return memory[n]=total;
}

int main()
{
    int test,t,n;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        printf("Case %d: %.10lf\n",t,solve(n));
    }
}
