#include<bits/stdc++.h>
using namespace std;

int GCD(int a,int b)
{
    while(a%b!=0)
    {
        int temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}
int main()
{
    int test,t,i,j,n,m,gcd;
    scanf("%d",&test);
    for(t=1;t<=test;t++){
        scanf("%d",&n);
        vector<int>team;
        for(i=1;i<=n;i++){
            scanf("%d",&j);
            team.push_back(j);
        }
        gcd=team[0];
        for(i=1;i<team.size();i++){
            gcd=GCD(gcd,team[i]);
        }
        int total=0;
        for(i=0;i<team.size();i++){
            total+=team[i]/gcd;
        }
        printf("%d %d\n",gcd,total);
    }
}
