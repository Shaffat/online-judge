#include<bits/stdc++.h>
using namespace std;

int main()
{
    int b,f,p,q,res,left,h,c;
    cin>>q;
    while(q--)
    {
        cin>>b>>p>>f>>h>>c;
        if(h<c)
        {
            swap(h,c);
            swap(f,p);
        }
        int tmp=min(b/2,p);
        left=b-tmp*2;

        res=tmp*h;

        res+=(min(left/2,f))*c;
        cout<<res<<endl;

    }
}
