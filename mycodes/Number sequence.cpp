#include<bits/stdc++.h>

using namespace std;

#define ll long long int

void multiplication(vector<vector<int> >&a,vector<vector<int> >&b,vector<vector<int> >&res,int m)
{
    int i,j,k;
    for(i=0;i<a.size();i++)
    {
        for(j=0;j<b[0].size();j++)
        {
            int sum=0;
            for(k=0;k<b.size();k++)
            {
                sum+=(a[i][k]*b[k][j])%m;
                sum%=m;
            }
            res[i][j]=sum;
        }
    }
    return ;
}

void power(int p,vector<vector<int> >&base,vector<vector<int> >&res,int m)
{
    int i,j;

    if(p==1)
    {
        for(i=0;i<base.size();i++)
        {
            for(j=0;j<base[0].size();j++)
            {
                res[i][j]=base[i][j];
            }
        }
        return;
    }
    else if(p%2==0)
    {
        power(p/2,base,res,m);
        vector<int>col(2);
        vector<vector<int> >tmp(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,res,m);
    }
    else
    {
        power(p/2,base,res,m);
        vector<int>col(2);
        vector<vector<int> >tmp(2,col);
        vector<vector<int> >tmp2(2,col);
        for(i=0;i<2;i++)
        {
            for(j=0;j<2;j++)
            {
                tmp[i][j]=res[i][j];
            }
        }
        multiplication(tmp,tmp,tmp2,m);
        multiplication(tmp2,base,res,m);
    }
    return;
}



int solve(int n,int a,int b,int m)
{
    if(n==0)
    {
        return a%m;
    }
    else if(n==1)
    {
        return b%m;
    }
    else
    {
        vector<int>col(1);
        vector<vector<int> >matrix(2,col);
        vector<vector<int> >final_res(2,col);
        matrix[0][0]= b;
        matrix[1][0]= a;
        vector<int>col1(2);
        vector<vector<int> >base(2,col1);
        vector<vector<int> >res(2,col1);
        base[0][0]=1;
        base[0][1]=1;
        base[1][0]=1;
        base[1][1]=0;
        power(n-1,base,res,m);
        multiplication(res,matrix,final_res,m);
        return final_res[0][0];
    }
}

int main()
{
    int t,test,a,b,n,m;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d %d %d",&a,&b,&n,&m);
        if(m==1)
        {
            m=10;
        }
        else if(m==2)
        {
            m=100;
        }
        else if(m==3)
        {
            m=1000;
        }
        else
            m=10000;
        int res=solve(n,a,b,m);
        printf("Case %d: %d\n",t,res);
    }
}
