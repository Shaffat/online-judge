#include<bits/stdc++.h>
using namespace std;
typedef long long int ll;
vector<ll>col(1e4+1,-1);
vector<vector<ll> >memory(1e3+1,col);

ll solve(ll tree,ll taken,ll capacity_add,ll restore,ll cur_mana,ll cur_capacity,vector<ll>&nest,vector<ll>&cost)
{
    cout<<"tree="<<tree<<" taken="<<taken<<" capcity="<<cur_capacity<<" mana="<<cur_mana<<endl;
    if(taken>nest[tree]||cur_mana<0||tree>=nest.size())
    {
        return 0;
    }
    if(memory[tree][taken]!=-1)
    {
        return memory[tree][taken];
    }
    ll case1=-1,case2;
    if(cur_mana>=cost[tree])
    {
        case1=solve(tree,taken+1,capacity_add,restore,cur_mana-cost[tree],cur_capacity+capacity_add,nest,cost)+1;
    }
    ll nxt_mana=min(cur_capacity,cur_mana+restore);
    case2=solve(tree+1,0,capacity_add,restore,nxt_mana,cur_capacity,nest,cost);
    cout<<"case1="<<case1<<" case2="<<case2<<endl;
    cout<<"memory["<<tree<<"]["<<taken<<"]="<<max(case1,case2)<<endl;
    return memory[tree][taken]=max(case1,case2);
}


int main()
{
  ll n,w,b,x,i,j,res;
  scanf("%lld %lld %lld %lld",&n,&w,&b,&x);
  vector<ll>nest(n+1);
  vector<ll>cost(n+1);
  for(i=1;i<=n;i++)
  {
      scanf("%lld",&j);
      nest[i]=j;
  }
  for(i=1;i<=n;i++)
  {
      scanf("%lld",&j);
      cost[i]=j;
  }
  res=solve(1,0,b,x,w,w,nest,cost);
  printf("%lld\n",res);

}
