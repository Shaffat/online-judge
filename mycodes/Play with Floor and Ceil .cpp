#include<bits/stdc++.h>
using namespace std;
struct result
{
    int a,b;
};

result solve(int fl,int cl,int x)
{
    //cout<<"here";
        if(fl==0)
        {
            result tmp;
            tmp.a=0;
            tmp.b=x;
            return tmp;
        }
        int p=0,q=0;
        p=x/fl;
        int mod=x%fl;
        p-=mod;
        q+=mod;
        result tmp;
        tmp.a=p;
        tmp.b=q;
        return tmp;
}

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        int x,k;
        scanf("%d %d",&x,&k);
        int f,c;
        if(x%k==0)
        {
            f=x/k;
            c=x/k;
        }
        else
        {
            f=x/k;
            c=f+1;
        }
        result tmp=solve(f,c,x);
        printf("%d %d\n",tmp.a,tmp.b);
    }
}
