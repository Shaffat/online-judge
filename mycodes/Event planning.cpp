#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,b,h,w,i,j;
    while(scanf("%d",&n)!=EOF)
    {
        scanf("%d %d %d",&b,&h,&w);
        int perheadcost=b/n;
        int k,l,m;
        int mini=2e9;
        for(i=1;i<=h;i++)
        {
            scanf("%d",&k);
            int chk=0;
            for(j=1;j<=w;j++)
            {
                scanf("%d",&m);
                if(m>=n)
                {
                    chk=1;
                }
            }
            if(chk)
            {
                if(k<=perheadcost)
                {
                    mini=min(mini,k);
                }
            }
        }
        if(mini==2e9)
        {
            printf("stay home\n");
        }
        else
            printf("%d\n",n*mini);
    }

}
