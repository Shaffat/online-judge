#include<bits/stdc++.h>
using namespace std;

struct segment
{
    int point,val;
};

bool operator <(segment a, segment b)
{
    if(a.point!=b.point)
    {
        return a.point<b.point;
    }
    return false;
}

void update(int index,int n,int val,vector<int>&tree)
{
    while(index<=n)
    {
        tree[index]+=val;
        index+=index&(-index);
    }
    return;
}
int query(int index,vector<int>&tree)
{
    int sum=0;
    while(index>0)
    {
        sum+=tree[index];
        index-=index&(-index);
    }
    return sum;
}

int find_pos(int val,vector<segment>&v)
{
    int first,last,i,j,mid,f=0,res;
    first=0,last=v.size()-1,res=last;
    while(first<=last)
    {
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f=1;
        }
        mid=(first+last)/2;
        if(v[mid].point<=val)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;

}

int main()
{
    int test,t,a,b,i,j,n,q,k;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&q);
        vector<segment>pos;
        segment tmp;
        tmp.point=-1;
        tmp.val=0;
        pos.push_back(tmp);
        for(i=1;i<=n;i++)
        {
            scanf("%d %d",&a,&b);
            tmp.point=a;
            tmp.val=1;
            pos.push_back(tmp);
            tmp.point=b+1;
            tmp.val=-1;
            pos.push_back(tmp);
        }
        sort(pos.begin(),pos.end());
        vector<int>tree(2*n+1,0);
        for(i=1;i<pos.size();i++)
        {
            tree[i]=tree[i-1]+pos[i].val;
        }

        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            scanf("%d",&j);
            k=find_pos(j,pos);

            int res=tree[k];
            printf("%d\n",res);
        }

    }
}
