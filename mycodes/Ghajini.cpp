#include<bits/stdc++.h>
using namespace std;

struct pos
{
    int mn,mx;
};

void build_tree(int index,int start,int last,vector<pos>&tree,vector<int>&v)
{
    if(start>last) return;
    if(start==last)
    {
        tree[index].mn=v[start];
        tree[index].mx=v[start];
        return;
    }
    int left,right,mid,mn,mx;
    left=2*index;
    right=2*index+1;
    mid=(start+last)/2;
    build_tree(left,start,mid,tree,v);
    build_tree(right,mid+1,last,tree,v);
    mn=min(tree[left].mn,tree[right].mn);
    mx=max(tree[left].mx,tree[right].mx);
    tree[index].mn=mn;
    tree[index].mx=mx;
    return;
}


pos query(int index,int s,int e,int qs,int qe,vector<pos>&tree)
{
    //cout<<"index="<<index<<" s="<<s<<" e="<<e<<" qs="<<qs<<" qe="<<qe<<endl;
    pos tmp;
    if(qs>e || qe<s)
    {
        tmp.mn=2e9;
        tmp.mx=-2e9;
        return tmp;
    }
    if(s>=qs && e<=qe)
    {
        return tree[index];
    }
    int left,right,mid,mn,mx;
    pos resl,resr;
    left=2*index;
    right=2*index+1;
    mid=(s+e)/2;
    resl=query(left,s,mid,qs,qe,tree);
    resr=query(right,mid+1,e,qs,qe,tree);
    tmp.mn=min(resl.mn,resr.mn);
    tmp.mx=max(resl.mx,resr.mx);
    return tmp;
}

int main()
{
    int test,t,n,d;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&d);
        int i,j,res=-2e9;
        vector<int>v(n+1);
        vector<pos>tree(4*n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            v[i]=j;
        }
        build_tree(1,1,n,tree,v);
        //cout<<"done tree"<<endl;
        for(i=1;i<=n-d+1;i++)
        {
            pos cur=query(1,1,n,i,i+d-1,tree);
            //cout<<i<<" to "<<i+d<<" mn="<<cur.mn<<" mx="<<cur.mx<<endl;
            res=max(cur.mx-cur.mn,res);
        }
        printf("Case %d: %d\n",t,res);
    }
}
