#include<bits/stdc++.h>
using namespace std;

int main()
{
    int i,j,test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,mx=-2e9,mn=2e9;
        scanf("%d",&n);
        string s,l;
        for(i=1;i<=n;i++)
        {
            getchar();
            int u,v,w;
            string k;
            cin>>k;
            scanf("%d %d %d",&u,&v,&w);
            int tmp=u*v*w;

            if(mx<tmp)
            {
                s=k;
                mx=tmp;
            }
            if(mn>tmp)
            {
                mn=tmp;
                l=k;
            }
        }
        if(mn!=mx)
        {
            cout<<"Case "<<t<<": "<<s<<" took chocolate from "<<l<<"\n";
        }
        else
            printf("Case %d: no thief\n",t);
        t++;
    }
}
