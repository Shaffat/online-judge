#include<bits/stdc++.h>
using namespace std;
#define ll long long int
vector<bool>numbers(1e6 +1,0);
vector<ll>res(1e6 +1);
vector<ll>ans(1e6 +1,0);
void sieve()
{

    ll i,j;
    //vector<int>numbers(2500,0);
    for(i=1;i<=1e6;i++)
    {
        res[i]=i;
    }
    numbers[1]=1;
    res[2]=1;
    res[1]=0;
    for(i=4;i<=1e6;i+=2)
    {
        numbers[i]=1;
        res[i]=i/2;
    }
    for(i=3;i<=1e6;i+=2)
    {
        //cout<<i<<" why???"<<endl;
        if(numbers[i]==0)
        {
            for(j=i;j<=1e6;j+=i)
            {
                numbers[j]=1;
                res[j]/=i;
                res[j]*=i-1;
            }
        }
    }
}

void solve()
{
    ll i;
    for(i=1;i<=1e6;i++)
    {
        ans[i]=res[i]+ans[i-1];
    }
}

int main()
{
    freopen("out.txt","w",stdout);
    freopen("in.txt","r",stdin);
    sieve();
    solve();
    ll n,u,v,result;
    scanf("%lld",&n);
    while(n--)
    {
        scanf("%lld %lld",&u,&v);
        u--;
        result=ans[v]-ans[u];
        printf("%lld\n",result);
    }
}

