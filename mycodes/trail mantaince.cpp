#include<bits/stdc++.h>

using namespace std;

struct node
{
    int n,w;
};

bool operator < (node x,node y)
{
    if(x.w!=y.w)
    {
        return x.w>y.w;
    }
    return false;
}
int Find(int r,int *ar)
{
    if(ar[r]=r)
    {
        return r;
    }
    ar[r]=Find(ar[r],ar);
    return ar[r];
}

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int n,w,i,j;
        scanf("%d %d",&n,&w);
        vector<int>edges[n+1];
        vector<int>cost[n+1];
        vector<int>trail;
        int parent[n+1];
        for(i=1;i<=n;i++)
        {
            parent[i]=i;
        }

        for(i=1;i<=w;i++)
        {
            int u,v,c,sum=0;
            scanf("%d %d %d",&u,&v,&c);
            edges[u].push_back(v);
            edges[v].push_back(u);
            cost[u].push_back(c);
            cost[v].push_back(c);
            int x,y;
            x=Find(u,parent);
            y=Find(v,parent);
            int chk=0;
            if(x!=y)
            {
                parent[x]=parent[y];
            }
            for(j=1;j<n;j++)
            {
                if(parent[j]!=parent[j+1])
                {
                    chk=1;
                    break;
                }
            }
            for(j=1;j<=n;j++)
            {
                cout<<parent[j]<<" ";
            }
            cout<<endl;
            if(chk)
            {
                continue;
            }
            vector<int>vis(n+1,0);
            vector<int>mst(n+1,2e9);
            priority_queue<node>q;
            node start;
            start.n=u;
            start.w=0;
            mst[u]=0;
            q.push(start);
           while(!q.empty())
            {
                node cur_top=q.top();
                q.pop();
                int cur_node=cur_top.n;
                if(vis[cur_node])
                {
                    continue;
                }
                vis[cur_node]=1;
                for(j=0;j<edges[cur_node].size();j++)
                {
                    int nxt_node=edges[cur_node][j];
                    int nxt_cost=cost[cur_node][j];
                    if(nxt_cost<mst[nxt_node]&&vis[nxt_node]==0)
                    {
                        mst[nxt_node]=nxt_cost;
                        node nwnode;
                        nwnode.n=nxt_node;
                        nwnode.w=nxt_cost;
                        q.push(nwnode);
                    }
                }

            }


            //cout<<"mst vector"<<endl;
            for(j=1;j<=n;j++)
            {
                sum+=mst[j];
            }
            //cout<<endl;
            //cout<<"chk="<<chk<<endl;

            trail.push_back(sum);
                //cout<<sum<<endl;



        printf("Case %d:\n",t);
        for(i=0;i<trail.size();i++)
        {
            printf("%d\n",trail[i]);
        }
        t++;
    }
    }
}
