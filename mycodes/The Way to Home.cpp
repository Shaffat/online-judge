#include<bits/stdc++.h>
using namespace std;

int nxt_pos(int cur,int d,string &s)
{
    int res=2e9,i,l=s.size()-1;
    for(i=cur+1;i<=min(l,cur+d);i++)
    {
        if(s[i]=='1')
        {
            res=i;
        }
    }
    return res;
}

int main()
{
    int n,d,i,j,done=0,jump=0;
    cin>>n>>d;
    string s;
    cin>>s;
    for(i=0;i<s.size();i++)
    {
        int nxtpoint=nxt_pos(i,d,s);
        //cout<<"nxt="<<nxtpoint<<endl;
        if(nxtpoint==n-1)
        {
            jump++;
            done=1;
            break;
        }
        jump++;
        i=nxtpoint-1;
    }
    if(done)
    {
        cout<<jump<<endl;
    }
    else
        cout<<"-1"<<endl;
}
