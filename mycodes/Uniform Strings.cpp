#include<bits/stdc++.h>
using namespace std;


int main()
{
    int test;
    cin>>test;
    while(test--)
    {
        int n,i,j,total=0;
        string s;
        cin>>s;
        i=1;
        if(s[0]!=s[1])
        {
            total++;
        }
        while(i!=0)
        {
            int next=i+1;
            if(next>7)
            {
                next=0;
            }
            //cout<<"i="<<i<<" next="<<next<<endl;
            if(s[i]!=s[next])
            {
                //cout<<s[i]<<" "<<s[next]<<endl;
                total++;
            }
            i=next;
        }
        //cout<<"total="<<total<<endl;
        if(total<=2)
        {
            cout<<"uniform\n";
        }
        else
            cout<<"non-uniform\n";
    }
}
