#include<bits/stdc++.h>
using namespace std;

struct poster_len
{
    int a,b,c;
};
bool operator < (poster_len A,poster_len B)
{
    if(A.a != B.a)
    {
        return A.a<B.a;
    }
    return false;
}

void preprocess(vector<poster_len>&poster,vector<int>&nxt_poster)
{
    vector<poster_len>::iterator up;
    for(int i=0;i<poster.size();i++)
    {
        //cout<<"original "<<poster[i].a<<endl;
        poster_len tmp;
        tmp.a=poster[i].a+poster[i].b-1;
        up=upper_bound(poster.begin(),poster.end(),tmp);
        nxt_poster[i]=up-poster.begin();
        //cerr <<"i="<<i<<"="<<nxt_poster[i] << endl;
    }
}

int solve(int idx,vector<poster_len>&posters,vector<int>&memory,vector<int>&nxt_poster)
{
    if(idx>=posters.size())
    {
        return 0;
    }
    if(memory[idx]!=-1)
    {
        return memory[idx];
    }
    int res1=0,res2=0;
    res1=solve(nxt_poster[idx],posters,memory,nxt_poster)+posters[idx].c;
    res2=solve(idx+1,posters,memory,nxt_poster);
    return memory[idx]=max(res1,res2);

}

int main()
{

    int test,t,n,a,b,c,i,j;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<poster_len>poster;
        poster_len tmp;
        for(i=1;i<=n;i++)
        {
            scanf("%d %d %d",&a,&b,&c);
            tmp.a=a;
            tmp.b=b;
            tmp.c=c;
            poster.push_back(tmp);
        }
        sort(poster.begin(),poster.end());
        vector<int>nxt_poster(n+100);
        vector<int>memory(n+100,-1);
        preprocess(poster,nxt_poster);
        int res=solve(0,poster,memory,nxt_poster);
        printf("Case %d: %d\n",t, res);

    }
    return 0 ;
}
