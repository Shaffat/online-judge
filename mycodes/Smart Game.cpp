#include<bits/stdc++.h>
using namespace std;

int op1(int n)
{
    return n*2;
}
int op2(int n)
{
    return n*3;
}
int op3(int n)
{
    return n/2;
}
int op4(int n)
{
    return n/3;
}
int op5(int n)
{
    return n+7;
}
int op6(int n)
{
    return n-7;
}

int bfs(int n,int m)
{
    map<int,int>nodes;
    queue<int>q;
    nodes[n]=0;
    q.push(n);
    while(!q.empty())
    {
        int cur,nxt;
        cur=q.front();
        if(cur==m)
        {
            return nodes[cur];
        }
        q.pop();
        nxt=op1(cur);
        if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
        nxt=op2(cur);
        if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
        nxt=op3(cur);
        if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
        nxt=op4(cur);
        if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
        nxt=op5(cur);
        if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
        nxt=op6(cur);
       if(nodes.count(nxt)==0)
        {
            q.push(nxt);
            nodes[nxt]=nodes[cur]+1;
        }
    }
}

int main()
{
    int n,m;
    scanf("%d %d",&n,&m);
    printf("%d\n",bfs(n,m));
}
