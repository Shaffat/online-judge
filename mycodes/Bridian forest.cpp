#include<bits/stdc++.h>
using namespace std;

struct position
{
    int r,c;
};

int main()
{
    int r,c,i,j;
    scanf("%d %d",&r,&c);
    getchar();
    vector< vector<char> >forest(r+1);
    vector< vector<int> >dis(r+1);
    vector< vector<int> >vis(r+1);
    position exit_,me;


    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            char d;

            scanf("%c",&d);
            forest[i].push_back(d);
            dis[i].push_back(0);
            vis[i].push_back(0);
            if(d=='E')
            {
                exit_.r=i;
                exit_.c=j;
            }
            if(d=='S')
            {
                me.r=i;
                me.c=j;
            }
        }
        getchar();
    }
    //cout<<exit_.r<<exit_.c<<" "<<me.r<<me.c<<endl;
    queue<position>q;
    q.push(exit_);
    vis[exit_.r][exit_.c]=1;
    dis[me.r][me.c]=0;
    while(!q.empty())
    {
        position cur=q.front();
        q.pop();
        position nxt;
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        //cout<<"cur.r="<<cur.r<<"    cur.c="<<cur.c<<" vis="<<vis[cur.r][cur.c]<<endl;
        if(nxt.r>=0 && nxt.r<r && nxt.c>=0 &&nxt.c>=0 && nxt.c<c)
        {

            if(!vis[nxt.r][nxt.c]&&forest[nxt.r][nxt.c]!='T')
            {
                //cout<<"nxt.r="<<nxt.r<<"    nxt.c="<<nxt.c<<"vis="<<vis[nxt.r][nxt.c]<<endl;
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);

            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(nxt.r>=0 && nxt.r<r &&nxt.c>=0 &&nxt.c>=0 && nxt.c<c)
        {
            if(!vis[nxt.r][nxt.c]&&forest[nxt.r][nxt.c]!='T')
            {
                //cout<<"nxt.r="<<nxt.r<<"    nxt.c="<<nxt.c<<"vis="<<vis[nxt.r][nxt.c]<<endl;
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(nxt.r>=0 && nxt.r<r &&nxt.c>=0 &&nxt.c>=0 && nxt.c<c)
        {
            if(!vis[nxt.r][nxt.c]&&forest[nxt.r][nxt.c]!='T')
            {
                //cout<<"nxt.r="<<nxt.r<<"    nxt.c="<<nxt.c<<"vis="<<vis[nxt.r][nxt.c]<<endl;
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(nxt.r>=0 && nxt.r<r &&nxt.c>=0 &&nxt.c>=0 && nxt.c<c)
        {
            if(!vis[nxt.r][nxt.c]&&forest[nxt.r][nxt.c]!='T')
            {
                //cout<<"nxt.r="<<nxt.r<<"    nxt.c="<<nxt.c<<"vis="<<vis[nxt.r][nxt.c]<<endl;
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                q.push(nxt);
            }
        }
    }
    int my_dis=dis[me.r][me.c];
    //cout<<my_dis<<endl;
    int result=0;
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
           // cout<<dis[i][j]<<" ";
            if(dis[i][j]!=0 && dis[i][j]<=my_dis && (me.r!=i ||me.c!=j))
            result+=forest[i][j]-'0';
            //cout<<"result="<<result<<" forest= "<<forest[i][j];
        }
       // cout<<"\n";

    }
    cout<<result<<endl;
}
