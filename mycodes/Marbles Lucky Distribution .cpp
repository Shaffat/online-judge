#include<bits/stdc++.h>
using namespace std;

int main()
{
    double n,m,k,res;
    cin>>m>>n>>k;

    int redonly=min(n,k-1);
    int blueOnly=max(k-1-redonly,0.0);

    if(n>=k)
    {
        res=(k-1 +((n-k+1)/(n-k+1+m)))/k;
    }
    else
    {
        res=n/k;
    }
    printf("%.9lf\n",res);
}
