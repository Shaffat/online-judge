#include<bits/stdc++.h>
using namespace std;

int main()
{
    int n,m,x,i,j;
    while(scanf("%d %d %d",&m,&n,&x)!=EOF)
    {
        vector<int>incomplete(n+1,0);
        vector<int>consume_time;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            consume_time.push_back(j);
        }
        sort(consume_time.begin(),consume_time.end());
        for(i=1;i<=x;i++)
        {
            for(j=0;j<n;j++)
            {
                if(!incomplete[j])
                {
                    if(m>0)
                    {
                        incomplete[j]=1;
                        //cout<<j<<"th cat got fish"<<endl;
                        m--;
                    }
                }
                if(incomplete[j])
                {
                    if(i%consume_time[j]==0)
                    {
                        incomplete[j]=0;
                        //cout<<j<<"th cat finshed a fish"<<endl;
                    }
                }
            }
        }
        int counter=0;
        for(i=0;i<n;i++)
        {
            if(incomplete[i])
            {
                counter++;
            }
        }
        printf("%d %d\n",m,counter);
    }
}
