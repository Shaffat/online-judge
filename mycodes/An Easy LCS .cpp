#include<bits/stdc++.h>
using namespace std;

string small(string a, string b)
{
    if(a.size()==b.size())
    {
        return min(a,b);
    }
    if(a.size()>b.size())
    {
        return a;
    }
    else
        return b;
}

string smallest_LCS(int i1,int i2,string s1,string s2,vector<vector<string> >&memory)
{
    if(i1>=s1.size()||i2>=s2.size())
    {
        return "";
    }
    if(memory[i1][i2]!="-1")
    {
        return memory[i1][i2];
    }
    if(s1[i1]==s2[i2])
    {
        string tmp;
        tmp.push_back(s1[i1]);
        return memory[i1][i2]=tmp+smallest_LCS(i1+1,i2+1,s1,s2,memory);
    }
    else
    {
        return memory[i1][i2]=small(smallest_LCS(i1+1,i2,s1,s2,memory),smallest_LCS(i1,i2+1,s1,s2,memory));
    }

}

int main()
{
    int test,t=1,i,j;
    string s1,s2;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        cin>>s1>>s2;
        string res;
        vector<string>c1(100,"-1");
        vector<vector<string> >memory(100,c1);
        res=smallest_LCS(0,0,s1,s2,memory);
        if(res=="")
        {
            printf("Case %d: :(\n",t);
        }
        else
        {
            printf("Case %d: ",t);
            cout<<res<<"\n";
        }
    }
}
