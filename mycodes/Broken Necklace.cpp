/*
ID: msdipu11
PROG: beads
LANG: C++14
*/
#include<bits/stdc++.h>
using namespace std;

int solve(int l,int r,string &s)
{
    //cout<<"for l="<<l<<" r="<<r<<endl;
    vector<bool>vis(s.size(),0);
    int f1=1,f2=1,total=0,colorl=0,colorr=0;
    char cl='a',cr='a';

    if(s[l]!='w')
    {
        cl=s[l];
        colorl=1;
    }
    if(s[r]!='w')
    {
        cr=s[r];
        colorr=1;
    }
    int cur_l=l,cur_r=r;
    while(f1||f2)
    {
        //cout<<"cur_l="<<cur_l<<" cur_r="<<cur_r<<" cl="<<cl<<" cr="<<cr<<" total="<<total<<endl;
        if((s[cur_l]==cl || s[cur_l]=='w'||colorl==0)&& vis[cur_l]==0)
        {
            if(colorl==0)
            {
                //cout<<"lcolour not set"<<endl;
                if(s[cur_l]!='w')
                {
                    cl=s[cur_l];
                    colorl=1;
                }
            }
            total++;
            vis[cur_l]=1;
            cur_l--;
            if(cur_l<0)
            {
                cur_l=s.size()-1;
            }
        }
        else
            f1=0;
        if((s[cur_r]==cr || s[cur_r]=='w'|| colorr==0) && vis[cur_r]==0)
        {
            if(!colorr)
            {
                //cout<<"rcolour not set"<<endl;
                if(s[cur_r]!='w')
                {
                    cr=s[cur_r];
                    colorr=1;
                }
            }
            total++;
            vis[cur_r]=1;
            cur_r++;
            if(cur_r>=s.size())
            {
                cur_r=0;
            }
        }
        else
            f2=0;
    }
    //cout<<"total="<<total<<endl;
    return total;
}

int main()
{
    freopen("beads.in","r",stdin);
    freopen("beads.out","w",stdout);
    int n,i,j,res=-1;
    string s;
    cin>>n>>s;
    for(i=0;i<s.size();i++)
    {
        int l=i-1,r=i;
        if(l<0)
        {
            l=n-1;
        }
        res=max(res,solve(l,r,s));
    }
    cout<<res<<endl;

}
