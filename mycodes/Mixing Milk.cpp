/*
ID: msdipu11
PROG: milk
LANG: C++14
*/

#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct product
{
    ll amount,price;
};

bool operator <(product a, product b)
{
    if(a.price!=b.price)
    {
        return a.price<b.price;
    }
    return false;
}

int main()
{
    freopen("milk.in","r",stdin);
    freopen("milk.out","w",stdout);
    ll i,j,total=0,n,m;
    scanf("%lld %lld",&n,&m);
    vector<product>v;
    for(i=1;i<=m;i++)
    {
        product tmp;
        scanf("%lld %lld",&tmp.price,&tmp.amount);
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    for(i=0;i<m;i++)
    {
        total+=v[i].price*(min(n,v[i].amount));
        n-=min(n,v[i].amount);
        if(n==0) break;
    }
    printf("%lld\n",total);
}
