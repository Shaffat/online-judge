#include<bits/stdc++.h>
using namespace std;
struct node
{
    int v,w;
};
bool operator <(node a, node b)
{
    if(a.w!=b.w)
    {
        return a.w>b.w;
    }
    return false;
}
int solve(int n,vector<vector<int> >&connection,vector<vector<int> >&cost)
{
    vector<int>dis(n+1,2e9);
    node source;
    source.v=1;
    source.w=0;
    dis[1]=0;
    priority_queue<node>q;
    q.push(source);
    while(!q.empty())
    {
        node cur=q.top();
        q.pop();
        int i,j,now=cur.v,nxt,w;
        for(i=0;i<connection[now].size();i++)
        {
            nxt=connection[now][i];
            w=cost[now][i];
            if(dis[nxt]>dis[now]+w)
            {
                dis[nxt]=dis[now]+w;
                node nxtnode;
                nxtnode.v=nxt;
                nxtnode.w=dis[nxt];
                q.push(nxtnode);
            }
        }
    }
    return dis[n];
}


int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        int i,j,n,m,u,v,w,res;
        scanf("%d %d",&n,&m);
        vector<vector<int> >connection(n+1);
        vector<vector<int> >cost(n+1);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d %d",&u,&v,&w);
            connection[u].push_back(v);
            cost[u].push_back(w);
            connection[v].push_back(u);
            cost[v].push_back(w);
        }
        res=solve(n,connection,cost);
        if(res==2e9)
        {
            printf("Case %d: Impossible\n",t);
        }
        else
        {
            printf("Case %d: %d\n",t,res);
        }
        t++;
    }
}
