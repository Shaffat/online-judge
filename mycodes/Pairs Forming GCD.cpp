#include<bits/stdc++.h>
using namespace std;

#define ll long long int

vector<bool>numbers(1e7 +1,0);
vector<ll>res(1e7 +1);
vector<ll>ans(1e7 +1,0);
void sieve()
{

    ll i,j;
    //vector<int>numbers(2500,0);
    for(i=1;i<=1e7;i++)
    {
        res[i]=i;
    }
    numbers[1]=1;
    res[2]=1;
    res[1]=1;
    for(i=4;i<=1e7;i+=2)
    {
        numbers[i]=1;
        res[i]=i/2;
    }
    for(i=3;i<=1e7;i+=2)
    {
        //cout<<i<<" why???"<<endl;
        if(numbers[i]==0)
        {
            for(j=i;j<=1e7;j+=i)
            {
                numbers[j]=1;
                res[j]/=i;
                res[j]*=i-1;
            }
        }
    }
}

void preprocess()
{
    ll i;
    for(i=1;i<=1e7;i++)
    {
        ans[i]=res[i]+ans[i-1];
    }
}


ll solve(ll n,ll p)
{
    ll first=1,last=n,mid,x,res=-1;
    while(first<=last)
    {
        mid=(first+last)/2;
        x=n/mid;
        if(ans[x]>=p)
        {
            res=mid;
            first=mid+1;
        }
        else
        {
            last=mid-1;
        }
    }

    return res;
}


int main()
{
    sieve();
    preprocess();
    ll n,p,test,t;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%lld %lld",&n,&p);
        ll res=solve(n,p);
        printf("Case %lld: %lld\n",t,res);
    }
}
