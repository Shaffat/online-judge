#include<bits/stdc++.h>
using namespace std;

int main()
{
    freopen("in.txt", "r", stdin);
    freopen("out.txt", "w", stdout);
    ios_base::sync_with_stdio(0);
    cin.tie(0);

    int i,j,n,q,k;
    string s1,s2;
    cin>>n>>q>>s1;
    map<char,int>ID;
    for(i=0;i<s1.size();i++)
    {
        ID[s1[i]]=i;
    }
    vector<int>col(200,0);
    vector<vector<int> >grid(50,col);
    vector<vector<vector<int> > >allChar(100,grid);
    vector<int>uniqu(1000,0);

    for(i=0;i<n;i++)
    {
        //cout<<"for charcter "<<s1[i]<<endl;
        int cur=i;
        for(j=0;j<17;j++)
        {
            cin>>s2;
            //cout<<s2<<endl;
            for(k=0;k<s2.size();k++)
            {
                if(s2[k]=='*'){

                    grid[j][k]+=1;
                    allChar[cur][j][k]=1;
                    //cout<<"total at ["<<j<<"]["<<k<<"]="<<grid[j][k]<<" "<<cur<<" char is "<<allChar[cur][j][k]<<endl;
                }
            }
        }
    }
    for(i=0;i<17;i++)
    {
        for(j=0;j<=50;j++)
        {
            if(grid[i][j]==1)
            {
                for(k=0;k<n;k++)
                {
                    if(allChar[k][i][j]){
                        uniqu[k]=1;
                    }
                }
            }
        }
    }
    for(i=0;i<17;i++)
    {
        for(j=0;j<40;j++)
        {
            cout<<grid[i][j]<<",";
        }
        cout<<endl;
    }
    for(i=1;i<=q;i++)
    {
        string tmp;
        cin>>tmp;
        cout<<"Query "<<i<<": ";
        for(j=0;j<tmp.size();j++)
        {
            if(uniqu[ID[tmp[j]]])
                cout<<"Y";
            else
                cout<<"N";
        }
        cout<<endl;
    }
}
