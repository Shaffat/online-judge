#include<bits/stdc++.h>
using namespace std;

vector<vector<int> >Storage(1e5+1);
vector<int>coprimeCount(1e5+1);
vector<bool>prime(1e5+1,0);

void countCoprime()
{
    int i,j;
    for(i=1;i<=1e5;i++)
    {
        coprimeCount[i]=i;
    }
    coprimeCount[1]=1;
    prime[1]=1;
    coprimeCount[2]=1;
    for(i=4;i<=1e5;i+=2)
    {
        prime[i]=1;
        coprimeCount[i]/=2;
    }
    for(i=3;i<=1e5;i+=2)
    {
        if(prime[i]==0)
        {
            for(j=i;j<=1e5;j+=i)
            {
                prime[j]=1;
                coprimeCount[j]/=i;
                coprimeCount[j]*=i-1;
            }
        }
    }
    return;
}


void preprocess()
{
    int i,j;
    for(i=1;i<=1e5;i++)
    {
        j=coprimeCount[i];
        Storage[j].push_back(i);
    }
    return;
}

int leftmost(int x,int target)
{
    int first,last,mid,i,j,f=0,res=-1;
    first=0;
    last=Storage[x].size()-1;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;
        mid=(first+last)/2;
        if(Storage[x][mid]>=target)
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}

int rightmost(int x, int target)
{
    //cout<<"right"<<endl;
    int first,last,mid,i,j,f=0,res=-1;
    first=0;
    last=Storage[x].size()-1;
    while(first<=last)
    {
        //cout<<"first="<<first<<" last="<<last<<endl;
        if(f) break;
        if(first==last) f++;
        mid=(first+last)/2;
        if(Storage[x][mid]<=target)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
//    freopen("input6.txt","r",stdin);
//    freopen("output6.txt","w",stdout);
    countCoprime();
    preprocess();
//    for(int i=10;i<=167;i++)
//    {
//        if(coprimeCount[i]==72)
//        cout<<i<<"="<<coprimeCount[i]<<endl;
//    }
    int n,i,j,a,b,x,q,aPos,bPos,res;
    scanf("%d %d",&n,&q);
    for(i=1;i<=q;i++)
    {
        scanf("%d %d %d",&a,&b,&x);
        aPos=leftmost(x,a);
        bPos=rightmost(x,b);
        if(aPos!=-1 && bPos!=-1)
        {
            res=bPos-aPos+1;
        }
        else
            res=0;
        printf("%d\n",res);
    }
}

