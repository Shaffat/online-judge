#include<bits/stdc++.h>

using namespace std;

vector< vector<int> >board;
int k=0,n;



bool nqueen(int c)
{
    int i,j,a,b,d,e,f;

    if(c==n)
    {
        k++;
        return true;
    }
    for(i=0;i<=n;i++)
    {
        if(i==n)
        {
            return false;
        }
        if(board[i][c]==0)
        {
            int cur_row=i,cur_column=c;
            for(j=0;j<n;j++)
            {
                if(j==i)
                {
                    continue;
                }
                board[j][cur_column]+=1;
            }

            for(j=0;j<n;j++)
            {
                if(j==cur_column)
                {
                    continue;
                }
                board[i][j]+=1;
            }

            a=i-min(i,cur_column);
            b=c-min(i,cur_column);

            f=i+c;
            if(f>n-1)
            {
                d=n-1;
                e=f-n+1;
            }
            else
            {
                d=i+c;
                e=0;
            }

            int temp1=a,temp2=b;
            while(temp1!=n&&temp2!=n)
            {

                if(temp1==i&&temp2==c)
                {
                    temp1++;temp2++;
                    continue;
                }
                board[temp1][temp2]+=1;
                temp1++;temp2++;
            }

            temp1=d;
            temp2=e;
            while(temp1!=-1&&temp2!=n)
            {

                if(temp1==i&&temp2==c)
                {
                    temp1--;temp2++;
                    continue;
                }
                board[temp1][temp2]+=1;
                temp1--;
                temp2++;
            }
            board[i][c]+=1;


            bool chk=nqueen(c+1);

                for(j=0;j<n;j++)
                {
                    if(j==i)
                    {
                        continue;
                    }
                    board[j][cur_column]-=1;
                }
                for(j=0;j<n;j++)
                {
                    if(j==cur_column)
                    {
                        continue;
                    }
                    board[i][j]-=1;
                }
                temp1=a,temp2=b;
                while(temp1!=n&&temp2!=n)
                {
                    if(temp1==i&&temp2==c)
                    {
                        temp1++;temp2++;
                        continue;
                    }
                    board[temp1][temp2]-=1;
                    temp1++;temp2++;
                }

                temp1=d;
                temp2=e;
                while(temp1!=-1 && temp2!=n)
                {
                    if(temp1==i&&temp2==c)
                    {
                        temp1--;temp2++;
                        continue;
                    }
                    board[temp1][temp2]-=1;
                    temp1--;
                    temp2++;
                }
                board[i][c]-=1;


        }
    }
}



int main()
{
    int i,j;
    vector<int>v;
    cin>>n;
    for(i=0;i<n;i++)
    {
        board.push_back(v);
        for(j=0;j<n;j++)
        {
            board[i].push_back(0);
        }
    }

    nqueen(0);
    cout<<k<<endl;
    for(i=0;i<n;i++)
    {

        for(j=0;j<n;j++)
        {
            cout<<board[i][j]<<" ";
        }
        cout<<endl;
    }
}
