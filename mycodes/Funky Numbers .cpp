#include<bits/stdc++.h>
using namespace std;

int main()
{
    ios_base::sync_with_stdio(0);
    long long int n,i,j,chk=0,need;
    vector<long long int>v;
    for(i=1;i<=1e6;i++)
    {
        long long int tmp;
        tmp=(i*(i+1))/2;
        if(tmp>1e9)
        {
            break;
        }
        v.push_back(tmp);
    }
    cin>>n;
    for(i=0;i<v.size();i++)
    {
         need=n-v[i];
        if(need<=0)
        {
            break;
        }
        if(binary_search(v.begin(),v.end(),need))
        {
            chk=1;
            break;
        }
    }
    if(chk)
    {
        cout<<"Yes"<<endl;
    }
    else
        cout<<"NO"<<endl;
}
