#include<bits/stdc++.h>

using namespace std;
bool ar[100001];
bool segment[100000000];
void sieve()

{
    int i,N=100000;

    ar[1]=1;
    for(i=4;i<=N;i+=2)
    {
        ar[i]=1;
    }
    int sq=sqrt(N);
    for(i=3;i<=sq;i+=2)
    {
        if (ar[i]==0)
        {
        for(int j=i*i;j<=N;j+=i)
        {
            ar[j]=1;
        }
        }
    }
}
void segmented_sieve(int m,int n,bool *segment,vector<int>&primes)
{
    int low=m,high=n,lowlim,i,j,k;
    for(i=0;i<primes.size();i++)
    {
        if(primes[i]*primes[i]>high)
        {
            break;
        }
        lowlim=floor(low/primes[i])*primes[i];
        if(lowlim<low)
        {
            lowlim+=primes[i];
        }
        for(j=lowlim;j<=high;j+=primes[i])
        {
            segment[j-low]=1;

        }
    }
    for(i=low;i<=high;i++)
    {
        if(segment[i-low]==0)
        {
            printf("%d\n",i);
        }
    }
}

int main()
{
    int i,j,k,l,m,n,test;
    vector<int>prime;
    sieve();
    for(i=1;i<=100000;i++)
    {
        if(ar[i]==0)
        {
           prime.push_back(i);
        }
    }
    while(scanf("%d",&test)!=EOF)
    {
        while(test--)
        {
            scanf("%d %d",&m,&n);
            if(m>100000||n>100000)
            {

                memset(segment,0,sizeof(segment));
                segmented_sieve(m,n,segment,prime);
            }
            else
            {
                for(i=m;i<=n;i++)
                {
                    if(ar[i]==0)
                    {
                        printf("%d\n",i);
                    }
                }
            }
            if(test>0)
            {
                printf("\n");
            }

        }
    }

}
