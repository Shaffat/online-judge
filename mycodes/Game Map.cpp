#include<bits/stdc++.h>

using namespace std;

struct node
{
    int n,c;
};

bool operator <(node a, node b)
{
    if(a.c!=b.c)
    {
        return a.c<b.c;
    }
    return false;
}


int solve(int node,vector<vector<int> >&connection,vector<int>&memory)
{
    int i,j,child=0;
    if(memory[node]!=-1)
    {
        return memory[node];
    }
    for(i=0;i<connection[node].size();i++)
    {
        int nxt_child=connection[node][i],cur;
        if(connection[nxt_child].size()>connection[node].size())
        {
            child=max(child,solve(nxt_child,connection,memory));
        }
    }
    return memory[node]=child+1;
}

int main()
{
    //freopen("in.txt","r",stdin);

    int n,m,i,j,u,v;
    while(scanf("%d %d",&n,&m)!=EOF)
    {
        int res=0;
        vector<vector<int> >connection(n);
        vector<int>memory(n,-1);
        for(i=1;i<=m;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        vector<node>sorted;
        for(i=0;i<n;i++)
        {
            node tmp;
            tmp.n=i;
            tmp.c=connection[i].size();
            sorted.push_back(tmp);
        }
        sort(sorted.begin(),sorted.end());
        for(i=0;i<sorted.size();i++)
        {
            res=max(res,solve(sorted[i].n,connection,memory));
        }
        printf("%d\n",res);
    }
}
