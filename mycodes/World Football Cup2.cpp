#include<bits/stdc++.h>
using namespace std;

struct teams
{
    string s;
    int points;
    int win;
    int dif;
    int goals;
};

bool operator <(teams a, teams b)
{
    if(a.points>b.points)
    {
        return true;
    }
    if(a.win>b.win)
    {
        return true;
    }
    if(a.dif>b.dif)
    {
        return true;
    }
    if(a.goals>b.goals)
    {
        return true;
    }
    return false;
}

int main()
{
    int n,i,j=0;
    map<string,int>teamid;
    scanf("%d",&n);
    vector<teams>selection(n);
    for(i=1;i<=n;i++)
    {
        getchar();
        string s;
        cin>>s;
        teamid[s]=j;
        selection[j].s=s;
        selection[j].dif=0;
        selection[j].goals=0;
        selection[j].points=0;
        selection[j].win=0;
        j++;
    }
    getchar();
    int l;
    for(l=1;l<=(n*(n-1))/2;l++)
    {
        string f;
        getline(cin,f);
        cout<<f<<endl;
        string t1="",t2="",goal1="",goal2="";
        int chkhyphen=0,chkspace=0,chkcolon=0;
        for(i=0;i<f.size();i++)
        {
            if(f[i]=='-')
            {
                chkhyphen=1;
                continue;
            }
            if(f[i]==' ')
            {
                chkspace=1;
                continue;
            }
            if(f[i]==':')
            {
                chkcolon=1;
                continue;
            }
            if(!chkhyphen)
            {
                t1.push_back(f[i]);
            }
            else if(!chkspace)
            {
                t2.push_back(f[i]);
            }
            else if(!chkcolon)
            {
                goal1.push_back(f[i]);
            }
            else
                goal2.push_back(f[i]);
        }
        int g1=0,g2=0;
        for(i=0;i<goal1.size();i++)
        {
            g1*=10;
            g1+=goal1[i]-'0';
        }
         for(i=0;i<goal2.size();i++)
        {
            g2*=10;
            g2+=goal2[i]-'0';
        }
        cout<<"team1= "<<t1<<" "<<g1<<" "<<g1-g2<<" "<<endl;
        cout<<"team2= "<<t2<<" "<<g2<<" "<<g2-g1<<" "<<endl;
        if(g1-g2>0)
        {
            selection[teamid[t1]].points+=3;
            selection[teamid[t1]].win+=1;
        }
        else if(g1==g2)
        {
            selection[teamid[t1]].points+=1;
            selection[teamid[t2]].points+=1;
        }
        else
        {
            selection[teamid[t2]].points+=3;
            selection[teamid[t2]].win+=1;

        }
        selection[teamid[t1]].dif+=g1-g2;
        selection[teamid[t1]].goals+=g1;
        selection[teamid[t2]].dif+=g2-g1;
        selection[teamid[t2]].goals+=g2;
    }
  sort(selection.begin(),selection.end());
  for(i=0;i<n;i++)
  {
      cout<<selection[i].s<<" "<<selection[i].goals<<" "<<selection[i].dif<<endl;
  }

}

