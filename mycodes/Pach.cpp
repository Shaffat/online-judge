#include<bits/stdc++.h>
using namespace std;

int back_cover(int pos,vector<int>&v)
{
    int first,last,mid,res=-1,f=0;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;
        mid=(first+last)/2;
        if(v[mid]>=pos)
        {
            res=mid;
            last=mid-1;
        }
        else
        {
            first=mid+1;
        }

    }
    return res;
}

int min_count(vector<int>&v,int len,int c)
{
    int i,j,counter=1,last,l,res=2e9,cur;
    last=v[0]-len;

    if(last<0){
    last+=c;
    cout<<"lastlen="<<last<<endl;
    l=back_cover(last,v);

    }
    else
        l=-1;
    cout<<"lastpos="<<l<<endl;
    if(l!=-1)
    {
        cur=v[l]+len;
        cur-=c;
    }
    else
    {
        cur=v[0]+len;
        l=v.size()-1;
    }

    for(i=0;i<l;i++)
    {
        cout<<"i="<<i<<" cur="<<cur<<endl;
        if(v[i]>cur)
        {
            cout<<"new patch"<<endl;
            cur=v[i]+len;
            counter++;
        }
    }
    res=counter;
    counter=0;
    cur=-2e9;
    for(i=0;i<v.size();i++)
    {
        if(v[i]>cur)
        {
            cur=v[i]+len;
            counter++;
        }
    }
    res=min(res,counter);
    return res;
}




int main()
{
    int n,c,t1,t2,i,j,f;
    while(scanf("%d %d %d %d",&n,&c,&t1,&t2)!=EOF)
    {
        vector<int>v;
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            v.push_back(j);
        }
        sort(v.begin(),v.end());
        int ans=2e9,counter,len;
        counter=min_count(v,t1,c);
        len=t1*counter;
        ans=len;
        counter=min_count(v,t2,c);
        len=t2*counter;
        ans=min(ans,len);
        printf("%d\n",ans);

    }
}
