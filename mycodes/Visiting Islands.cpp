#include<bits/stdc++.h>
using namespace std;

struct item
{
    int total,pathSize;
};


bool operator<(item a,item b)
{
    if(a.total!=b.total){
        return a.total>b.total;
    }

}


int longestPath(vector<vector<int> >&connection,int node,int n)
{
    int cur,i,j,child,res=0;
    vector<int>vis(n+1,0);
    vector<int>dis(n+1,0);
    queue<int>q;
    q.push(node);
    cur=node;
    vis [cur]=1;
    dis[cur]=0;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            child=connection[cur][i];
            if(!vis[child]){
                vis[child]=1;
                dis[child]=dis[cur]+1;
                res=max(res,dis[child]);
                q.push(child);
            }
        }

    }
    return res+1;
}

int findOne(vector<vector<int> >&connection,int root,int n,int &total,vector<int>&vis)
{
    int cur,i,j,child,res=root,mx=-1;
    total=1;
    vector<int>dis(n+1,0);
    queue<int>q;
    q.push(root);
    cur=root;
    vis [cur]=1;
    dis[cur]=1;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        for(i=0;i<connection[cur].size();i++)
        {
            child=connection[cur][i];
            if(!vis[child]){
                vis[child]=1;
                dis[child]=dis[cur]+1;
                q.push(child);
                total++;
                if(mx<dis[child]){
                    mx=dis[child];
                    res=child;
                }
            }
        }

    }
    return res;
}

void preProcess(vector<item>&v,vector<int>&longest)
{
    int last=0,i,j;
    item tmp;
    tmp.pathSize=0;
    tmp.total=0;
    v.push_back(tmp);
    sort(v.begin(),v.end());
    for(i=0;i<v.size()-1;i++){
        last=max(last,v[i].pathSize);
        //cout<<"from "<<v[i].total<<" to "<<v[i+1].total<<" x="<<v[i].pathSize<<" last="<<last<<endl;
        for(j=v[i].total;j>v[i+1].total;j--){
            longest[j]=last;
        }
    }
    return;
}

void solve(int n,vector<vector<int> >&connection,vector<int>&maxpath)
{
    int i,j;
    vector<int>mx(1e5+10,-1);
    vector<item>v;
    vector<int>vis(n+1,0);
    for(i=1;i<=n;i++){

        if(vis[i]==0){
            int total=0,u,x;
            u=findOne(connection,i,n,total,vis);
            x=longestPath(connection,u,n);
            mx[u]=max(mx[u],x);

            //cout<<"root "<<i<<" longest path = "<<x<<" total="<<total<<" and mx="<<mx[u]<<endl;
        }
    }
    for(i=1;i<=n;i++){
        if(mx[i]>0){
            item tmp;
            tmp.total=i;
            tmp.pathSize=mx[i];
            v.push_back(tmp);
        }
    }
    preProcess(v,maxpath);
}

int query(vector<int>&mxpath,int q){
    if(mxpath[q]==-1){
        return -1;
    }
    if(q<=mxpath[q]) return q-1;
    int cost=(q-mxpath[q])*2+mxpath[q]-1;
    return cost;

}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    int test,t,i,j,u,v,n,m,q,x;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d %d",&n,&m);
        vector<int>col;
        vector<vector<int> >connection(n+1,col);
        for(i=1;i<=m;i++){
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            connection[v].push_back(u);
        }
        vector<int>mxPath(1e5+10,-1);
        solve(n,connection,mxPath);
        scanf("%d",&q);
        printf("Case %d:\n",t);
        while(q--){
            scanf("%d",&x);
            int res=query(mxPath,x);
            if(res==-1){
                printf("impossible\n");
            }
            else{
                printf("%d\n",res);
            }
        }
    }
}
