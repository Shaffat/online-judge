#include<bits/stdc++.h>
using namespace std;

vector<int>memory(1e5 + 1,-1);

int solve(int index,int n,vector<int>&a)
{
    int i;
    if(memory[index]!=-1)
    {
        return memory[index];
    }
    int res=0;
    for(i=index;i<=n;i+=a[index])
    {

        if(a[index]<a[i])
        {
            int cur=solve(i,n,a);
            if(cur==0)
            {
                res=1;
            }
        }
    }


    for(i=index;i>=0;i-=a[index])
    {
        if(a[index]<a[i])
        {
            int cur=solve(i,n,a);
            if(cur==0)
            {
                res=1;
            }
        }
    }

    return memory[index]=res;
}

int main()
{
    int n,i,j;
    scanf("%d",&n);
    vector<int>a(n+1);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        a[i]=j;
    }
    for(i=1;i<=n;i++)
    {
        solve(i,n,a);
    }
    for(i=1;i<=n;i++)
    {
        if(memory[i]==1)
        {
            printf("A");
        }
        else
            printf("B");
    }

}
