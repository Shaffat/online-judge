#include<bits/stdc++.h>
using namespace std;

string strConvert(int n)
{
    string s="";
    int i,j;
    while(n>0)
    {
        s.push_back('0'+n%10);
        n/=10;
    }
    reverse(s.begin(),s.end());
    return s;
}

int main()
{
    int a,b,n,tmp,dig,chk=0,i,j;
    cin>>a>>b>>n;
    for(i=0;i<=9;i++)
    {
        tmp=a;
        tmp*=10;
        tmp+=i;
        if(tmp%b==0)
        {
            chk=1;
            dig=i;
            break;
        }
    }
    if(chk)
    {
        string res=strConvert(a);
        res.push_back('0'+dig);
        for(i=1;i<n;i++)
        {
            res.push_back('0');
        }
        cout<<res<<endl;
    }
    else
        cout<<"-1"<<endl;
}
