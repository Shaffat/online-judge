#include<bits/stdc++.h>
using namespace std;


bool solve(int n,vector<int>&sequence,vector<vector<int> >&connection)
{
    int i,j,cur=0;
    if(sequence[cur]!=1)
    {
        return 0;
    }
    cur++;
    vector<int>vis(n+1,0);
    vis[0]=1;
    queue<int>q;
    q.push(1);
    while(!q.empty())
    {
        int node=q.front();
        q.pop();
        for(i=0;i<connection[node].size();i++)
        {
            int nxt=sequence[cur];
            //cout<<"node="<<node<<" nxt="<<nxt<<" cur="<<cur<<" vis="<<vis[nxt]<<" search="<<binary_search(connection[node].begin(),connection[node].end(),nxt)<<endl;
            if(!vis[nxt] && binary_search(connection[node].begin(),connection[node].end(),nxt))
            {
                //cout<<"pushed"<<endl;
                vis[nxt]=1;
                q.push(nxt);
                cur++;
                if(cur>=n)
                {
                    return 1;
                }
            }
        }
    }
    if(cur<n)
    {
        return 0;
    }

}

int main()
{
    int n,i,j,k;
    scanf("%d",&n);
    vector<vector<int> >connection(n+1);
    for(i=1;i<n;i++)
    {
        scanf("%d %d",&j,&k);
        connection[j].push_back(k);
        connection[k].push_back(j);
    }
    vector<int>sequence;
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        sequence.push_back(j);
    }
    for(i=1;i<=n;i++)
    {
        sort(connection[i].begin(),connection[i].end());
    }
    if(solve(n,sequence,connection))
    {
        cout<<"Yes";
    }
    else
        cout<<"No";
}

