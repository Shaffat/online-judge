#include<bits/stdc++.h>
using namespace std;

void dfs(int node,vector<vector<int> >&connection,vector<int>&vis,vector<int> &level,vector<vector<int> >&parentSparce)
{
    vis[node]=1;
    int i,j,child;
    for(i=0;i<connection[node].size();i++)
    {
         child=connection[node][i];
        if(!vis[child])
        {
            vis[child]=1;
            parentSparce[child][0]=node;
            level[child]=level[node]+1;
            dfs(child,connection,vis,level,parentSparce);
        }
    }
    return;
}


void initLCA(int n,vector<vector<int> >&connection,vector<int>&level,vector<vector<int> >&parentSparce)
{
    vector<int>vis(n+1,0);
    level[1]=0;
    dfs(1,connection,vis,level,parentSparce);

    for(int i=1;i<=12;i++)
    {
        for(int j=1;j<=n;j++)
        {
            int p=parentSparce[j][i-1];
            if(p!=-1)
            {
                parentSparce[j][i]=parentSparce[p][i-1];
            }
        }
    }
    return;
}

int findLCA(int a,int b,vector<vector<int> >&parentSparce,vector<int>&level)
{
    int i,j;
    if(level[a]<level[b])
    {
        swap(a,b);
    }

    for(i=12;i>=0;i--)
    {
        if(level[a]-(1<<i)>=level[b])
        {
            a=parentSparce[a][i];
        }
    }
    if(a==b)
        return a;
    for(i=12;i>=0;i--)
    {
        if(parentSparce[a][i]!=-1 && parentSparce[b][i]!=-1 && parentSparce[a][i]!=parentSparce[b][i])
        {
            a=parentSparce[a][i];
            b=parentSparce[b][i];
        }
    }
    return parentSparce[a][0];
}

int main()
{
    int test,t,i,j,n,u,v,m;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<vector<int> >connection(n+1);
        vector<int>level(n+1,0);
        vector<int>col(13,-1);
        vector<vector<int> >parentSparse(n+1,col);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&m);
            for(j=1;j<=m;j++)
            {
                scanf("%d",&v);
                connection[i].push_back(v);
                connection[v].push_back(i);

            }
        }
        initLCA(n,connection,level,parentSparse);
        int q;
        scanf("%d",&q);
        printf("Case %d:\n",t);
        for(i=1;i<=q;i++)
        {
            scanf("%d %d",&u,&v);
            int res=findLCA(u,v,parentSparse,level);
            printf("%d\n",res);
        }
    }
}
