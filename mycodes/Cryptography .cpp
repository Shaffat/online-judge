#include<bits/stdc++.h>
using namespace std;

#define ll long long int
void floyedWarshal(vector<vector<ll> >&shortest)
{
    int i,j,k;
    for(i=33;i<=126;i++)
    {
        for(j=33;j<=126;j++)
        {
            if(i==j)
            shortest[i][j]=0;
        }
    }
    for(i=33;i<=126;i++)
    {
        for(j=33;j<=126;j++)
        {
            for(k=33;k<=126;k++)
            {
                shortest[j][k]=min(shortest[j][i]+shortest[i][k],shortest[j][k]);
            }
        }
    }

    return;
}

ll solve(string s1,string s2,vector<vector<ll> >&cost)
{
    int i,j,k,total=0;
    floyedWarshal(cost);
    for(i=0;i<s1.size();i++)
    {
        ll tmp=cost[s1[i]][s2[i]];
        if(tmp==1e12)
        {
            //cout<<"couldnt convert "<<s1[i]<<" to "<<s2[i]<<endl;
            return -1;
        }
        total+=tmp;
    }
    return total;
}

int main()
{
    ios_base::sync_with_stdio(0);
    ll i,j,k,w,n,m;
    char u,v;
    string s1,s2;
    cin>>s1>>s2;
    cin>>m;
    vector<ll>col(150,1e12);
    vector<vector<ll> >cost(150,col);
    for(i=1;i<=m;i++)
    {
        cin>>u>>v>>w;
        //cout<<u<<" "<<v<<" "<<w<<endl;
        cost[u][v]=min(cost[u][v],w);
    }
    ll res=solve(s1,s2,cost);
    cout<<res<<endl;
}

