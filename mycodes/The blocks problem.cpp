#include<bits/stdc++.h>

using namespace std;

int n;

int findlocation(int a,vector<int>&where)
{
    if(where[a]==a)
    {
        return a;
    }
    return where[a]=findlocation(where[a],where);
}


void moveonto(int a,int b,vector<stack<int> >&blockworld,vector<int>&whereIsBlock)
{
    int alocation,blocation;
    alocation=findlocation(a,whereIsBlock);
    blocation=findlocation(b,whereIsBlock);
    while(blockworld[alocation].top()!=a)
    {
        int cur=blockworld[alocation].top();
        whereIsBlock[cur]=cur;
        blockworld[cur].push(cur);
        blockworld[alocation].pop();
    }
    blockworld[alocation].pop();
    whereIsBlock[a]=blocation;
    while(blockworld[blocation].top()!=b)
    {
        int cur=blockworld[blocation].top();
        whereIsBlock[cur]=cur;
        blockworld[cur].push(cur);
        blockworld[blocation].pop();
    }
    blockworld[blocation].push(a);


}

void moveover(int a,int b,vector<stack<int> >&blockworld,vector<int>&whereIsBlock)
{

    int alocation,blocation,i,j;
    alocation=findlocation(a,whereIsBlock);
    blocation=findlocation(b,whereIsBlock);
    cout<<"alocation="<<alocation<<" blocation="<<blocation<<endl;
    stack<int>tmp,tmp1;
    tmp=blockworld[alocation];
    cout<<"here "<<tmp.size()<<endl;
    while(!tmp.empty())
    {
        cout<<tmp.top()<<" "<<endl;
        tmp1.push(tmp.top());
        tmp.pop();
    }
    while(!tmp1.empty())
    {
        cout<<tmp1.top()<<" ";
        tmp1.pop();
    }


    while(blockworld[alocation].top()!=a)
    {
        int cur=blockworld[alocation].top();
        blockworld[alocation].pop();
        whereIsBlock[cur]=cur;
        blockworld[cur].push(cur);
    }

    blockworld[alocation].pop();
    whereIsBlock[a]=blocation;
    blockworld[blocation].push(a);

}

void pileonto(int a,int b,vector<stack<int> >&blockworld,vector<int>&whereIsBlock)
{
    int location,i,j,alocation,blocation;
    blocation=findlocation(b,whereIsBlock);
    alocation=findlocation(a,whereIsBlock);
    while(blockworld[blocation].top()!=b)
    {
        int cur=blockworld[blocation].top();
        blockworld[blocation].pop();
        whereIsBlock[cur]=cur;
        blockworld[cur].push(cur);
    }
    stack<int>tmp;

    while(blockworld[alocation].top()!=a)
    {
        int cur=blockworld[alocation].top();
        tmp.push(cur);
        blockworld[alocation].pop();
    }
    blockworld[alocation].pop();
    tmp.push(a);
    whereIsBlock[a]=blocation;
    while(!tmp.empty())
    {
        blockworld[blocation].push(tmp.top());
        tmp.pop();
    }


}
void pileover(int a,int b,vector<stack<int> >&blockworld,vector<int>&whereIsBlock)
{

    int location,i,j,alocation,blocation;
    alocation=findlocation(a,whereIsBlock);
    blocation=findlocation(b,whereIsBlock);
    stack<int>tmp;
    while(blockworld[alocation].top()!=a)
    {
        tmp.push(blockworld[alocation].top());
        blockworld[alocation].pop();
    }
    tmp.push(a);
    blockworld[alocation].pop();
    whereIsBlock[a]=blocation;
    while(!tmp.empty())
    {
        blockworld[blocation].push(tmp.top());
        tmp.pop();
    }

}

int converter(string a)
{
    int i,k=1,res=0;
    for(i=a.size()-1;i>=0;i--)
    {
        res+=k*(a[i]-'0');
        k*=10;
    }
    return  res;
}


int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);

   map<string,int>mp;
   mp["move"]=1;
   mp["pile"]=2;
   mp["onto"]=30;
   mp["over"]=40;

   while(scanf("%d",&n)!=EOF)
   {
       string s;
       vector<stack<int> >blockworld(n+1);
       vector<int>whereisblock(n+1);
       for(int i=0;i<n;i++)
       {
           blockworld[i].push(i);
           whereisblock[i]=i;
       }

       while(getline(cin,s))
       {
           if(s=="quit")
           {
               break;
           }
           int i,j,space=0;
           string as="",bs="",c1="",c2="";

           for(i=0;i<s.size();i++)
           {
               if(s[i]==' ')
               {
                   space++;
                   continue;
               }
               if(space==0)
               {
                   c1+=s[i];
               }
               if(space==1)
               {
                   as+=s[i];
               }
               if(space==2)
               {
                   c2+=s[i];
               }
               if(space==3)
               {
                   bs+=s[i];
               }
           }
            int a,b;
            a=converter(as);
            b=converter(bs);

            int command=mp[c1]+mp[c2];

            cout<<"output "<<c1<<" "<<as<<" "<<c2<<" "<<bs<<endl;
            if(command==31)
            {
                moveonto(a,b,blockworld,whereisblock);
            }
            if(command==41)
            {
                moveover(a,b,blockworld,whereisblock);
            }
            if(command==32)
            {
                pileonto(a,b,blockworld,whereisblock);
            }
            if(command==42)
            {
                pileover(a,b,blockworld,whereisblock);
            }


       }
       for(int i=0;i<n;i++)
       {
           printf("%d:",i);
           stack<int>tmp;
           while(!blockworld[i].empty())
           {
               tmp.push(blockworld[i].top());
               blockworld[i].pop();
           }
           while(!tmp.empty())
           {
               printf(" %d",tmp.top());
               tmp.pop();
           }
           printf("\n");
       }
   }

}
