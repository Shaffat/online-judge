/*
ID: msdipu11
PROG: dualpal
LANG: C++14
*/

#include<bits/stdc++.h>
using namespace std;

string base_convert(int number,int base)
{
    string res="";
    while(number>=base)
    {
        int r,cur;
        r=number%base;
        number/=base;
        //cout<<"number="<<number<<"r="<<r<<endl;
        if(r<10)
        {
            res.insert(res.begin(),'0'+r);
        }
        else
        {
            res.insert(res.begin(),'A'+r-10);
        }
    }
    if(number>0)
    {
        int cur=number;
        if(cur<10)
        {
            res.insert(res.begin(),'0'+cur);
        }
        else
        {
            res.insert(res.begin(),'A'+cur-10);
        }
    }
    //cout<<"conversion is "<<res<<endl;
    return res;
}

bool is_palin(string &s)
{
    int i=0,j=s.size()-1;
    while(i<=j)
    {
        //cout<<"i="<<i<<" j="<<j<<" and "<<s[i]<<" and "<<s[j]<<endl;
        if(s[i]!=s[j]) return false;
        i++;
        j--;
    }
    //cout<<"i am good"<<endl;
    return true;
}

int main()
{
    freopen("dualpal.in","r",stdin);
    freopen("dualpal.out","w",stdout);
    int n,s,i,j;
    cin>>n>>s;
    while(n>0)
    {
        s++;
        j=0;
        for(i=2;i<=10;i++)
        {
            string tmp=base_convert(s,i);
            if(is_palin(tmp))
                j++;
        }
        if(j>1)
        {
            printf("%d\n",s);
            n--;
        }
    }
}
