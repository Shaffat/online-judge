#include<bits/stdc++.h>
#define ll long long int
using namespace std;


ll solve(int bitmask,int m,vector<vector<ll> >&left_total,vector<ll>&memory)
{
    //cout<<"bitmask="<<bitmask<<endl;
    ll i,j,res=2e15,cost;
    if(bitmask==((1<<m)-1))
    {
        return 0;
    }
    if(memory[bitmask]!=-1)
    {
//        cout<<"memory"<<endl;
        return memory[bitmask];
    }
    for(i=0;i<m;i++)
        {
            //cout<<"bitsmask "<<bitmask<<" and "<<(1<<i)<<" and ="<<((bitmask)&(1<<i))<<endl;
            if(((bitmask)&(1<<i))==0)
            {
                //cout<<"taking "<<i<<endl;
                cost=0;
                for(j=0;j<m;j++)
                {
                    if((bitmask&(1<<j))==0 && i!=j)
                    {
                        cost+=left_total[i][j];
                    }
                }
                cost+=solve((bitmask|(1<<i)),m,left_total,memory);
                res=min(cost,res);
            }
        }
    return memory[bitmask]=res;
}

int main()
{
    ll test,t;
    scanf("%lld",&test);
    for(t=1;t<=test;t++)
    {
        ll n,m,i,j,pre,k;
        scanf("%lld %lld",&n,&m);
//        cout<<"n="<<n<<" m="<<m<<endl;
        vector<ll>col(m+1,0);
        vector<vector<ll> >cumulative(n+1,col);
        vector<vector<ll> >left_total(m+1,col);
        for(i=0;i<n;i++)
        {
            scanf("%lld",&k);
            k--;
            if(i>0)
            {
//                cout<<" k ="<<k<<" pre="<<pre<<endl;
                for(j=0;j<m;j++)
                {
                    cumulative[i][j]=cumulative[i-1][j];
//                    cout<<"cumlitive "<<j<<" ="<<cumulative[i-1][j]<<endl;
                }
                cumulative[i][pre]++;
                //cout<<"added pre now "<<pre<<"="<<cumulative[i][pre]<<endl;
                for(j=0;j<m;j++)
                {
                    left_total[k][j]+=cumulative[i][j];
                    //cout<<"left "<<j<<" ="<<left_total[k][j]<<endl;
                }

            }
            pre=k;
        }
        vector<ll>memory((1<<(m+1)),-1);
//        for(i=0;i<m;i++)
//        {
//            cout<<"for color "<<i<<endl;
//            for(j=0;j<m;j++)
//            {
//                cout<<"left "<<j<<" ="<<left_total[i][j]<<endl;
//            }
//        }
        ll res=solve(0,m,left_total,memory);
        printf("Case %lld: %lld\n",t,res);

    }
}
