#include<bits/stdc++.h>
using namespace std;

int luckydigit(int n)
{
    int i,j,total=0;
    while(n>0)
    {
        if(n%10==7 || n%10==4)
        {
            total++;
        }
        n/=10;
    }
    return total;
}

int main()
{
    int n,i,k,j,total=0;
    scanf("%d %d",&n,&k);
    for(i=1;i<=n;i++)
    {
        scanf("%d",&j);
        if(luckydigit(j)<=k)
        {
            total++;
        }
    }
    printf("%d\n",total);
}
