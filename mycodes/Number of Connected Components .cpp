#include<bits/stdc++.h>
using namespace std;

vector<int>primes;
vector<int>p(1e6+1,0);
vector<int>pl(1e6+1,0);

void sieve()
{
    int i,j,n;
    for(i=4;i<=1e6;i+=2)
    {
        p[i]=1;
    }
    p[1]=1;
    for(i=3;i<=sqrt(1e6);i+=2)
    {
        if(p[i]==0)
        {
            for(j=i*i;j<=1e6;j+=i)
            {
                p[j]=1;
            }
        }
    }
    pl[2]=0;
    primes.push_back(2);

    for(i=3;i<1e6;i++)
    {
        if(p[i]==0){
            pl[i]=primes.size();
            primes.push_back(i);
        }
    }
    return;
}


int findparent(int a,vector<int>&parent)
{
    if (parent[a]==a)
        return a;
    return parent[a]=findparent(parent[a],parent);
}

void unionSet(int a,int b,vector<int>&parent){
    int p_a,p_b;
    p_a=findparent(a,parent);
    p_b=findparent(b,parent);
    if(p_a!=p_b)
        parent[p_a]=p_b;
}


void factors(int level,vector<int>&parent,vector<int>&exist){
    //cout<<"for "<<level<<endl;
    int i,j;
    vector<int>v;
    for(i=0;i<primes.size();i++)
    {
        if (primes[i]*primes[i]>level){
            break;
        }
        if(level%primes[i]==0){
            while(level%primes[i]==0){
                level/=primes[i];
            }
            //cout<<"prime is "<<primes[i]<<endl;
            v.push_back(i);
            exist[i]=1;
        }
    }
    if(level>1){
        //cout<<"prime is "<<primes[i]<<endl;
        v.push_back(pl[level]);
        exist[pl[level]]=1;
    }

    for(i=1;i<v.size();i++){
            //cout<<"joint "<<v[i]<<" and "<<v[0]<<endl;
        unionSet(v[i],v[0],parent);
    }

}



int main()
{
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    sieve();
    int test,t,n,m,i,j,start;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);

        int cmp=0;
        vector<int>exist(1e6+1,0);
        vector<int>parent(1e6+1);
        for(i=1;i<=2e5;i++){
            parent[i]=i;
        }
        for(i=0;i<n;i++)
        {
            int node;
            scanf("%d",&node);
            if(node==1){
                cmp++;
            }
            else{
                factors(node,parent,exist);
            }
        }

        for(i=0;i<primes.size();i++){
            if(exist[i]==1){
                //cout<<"checking for "<<i<<" parent is "<<findparent(i,parent)<<endl;
            if(findparent(i,parent)==i){
                cmp++;
            }
            //cout<<"cmp="<<cmp<<endl;
            }
        }

        printf("Case %d: %d\n",t,cmp);
    }
}
