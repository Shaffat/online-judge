/*
ID: msdipu11
PROG: gift1
LANG: C++14
*/
#include<bits/stdc++.h>
using namespace std;

int main()
{
    freopen("gift1.in","r",stdin);
    freopen("gift1.out","w",stdout);
    int np,ng,i,j,m,n;
    map<string,int>money;
    vector<string>people;
    string s;
    cin>>np;
    getchar();
    for(i=1;i<=np;i++)
    {
        getline(cin,s);
        people.push_back(s);
        money[s]=0;
        //cout<<"s="<<s<<endl;
    }
    for(i=1;i<=np;i++)
    {
        getline(cin,s);
        //cout<<"i="<<i<<" s="<<s<<endl;
        cin>>m>>n;
        //cout<<"m="<<m<<" n="<<n<<endl;
        getchar();
        money[s]-=m;
        if(n>0)
            money[s]+=m%n;
        else
            money[s]+=m;
        for(j=1;j<=n;j++)
        {
            getline(cin,s);
            //cout<<"money s="<<s<<endl;
            money[s]+=m/n;
        }
    }
    //cout<<"end"<<endl;
    for(i=0;i<np;i++)
    {
        s=people[i];
        cout<<s<<" "<<money[s]<<endl;
    }
}
