#include<bits/stdc++.h>
using namespace std;

int solve(int cur,int p,vector<vector<int> >&connection,vector<int>&parent,vector<int>&counter)
{
   int i,j,res=0;
   queue<int>q;
   parent[cur]=p;
   q.push(cur);
   while(!q.empty())
   {
       cur=q.front();
       q.pop();
       if (connection[cur].size()==0)
       {
           res++;
           continue;
       }
       for(i=0;i<connection[cur].size();i++)
       {
           int nxt=connection[cur][i];
           if(parent[nxt]==-1)
           {
               parent[nxt]=p;
               q.push(nxt);
           }
           else
           {
               if(counter[parent[nxt] ]!=p)
               {
                   counter[parent[nxt] ] = p;
                   res++;
               }
           }
       }
   }
   return res;
}

int main()
{
    freopen("in.txt","r",stdin);
    freopen("out.txt","w",stdout);
    int test,t,n,i,j,u,v;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        scanf("%d",&n);
        vector<int>indegree(n+1,0);
        vector<int>col;
        vector<vector<int> >connection(n+1,col);
        for(i=1;i<n;i++)
        {
            scanf("%d %d",&u,&v);
            connection[u].push_back(v);
            indegree[v]++;
        }
        int res=0;
        vector<int>roots;
        for(i=0;i<n;i++)
        {
            if(indegree[i]==0)
                roots.push_back(i);
        }
        vector<int>parent(n+1,-1);
        vector<int>counter(n+1,-1);
        for(i=0;i<roots.size();i++)
        {
            res+=solve(roots[i],roots[i],connection,parent,counter);
            //cout<<"after root "<<roots[i]<<" res ="<<res<<endl;
        }

        //res=solve(root,connection,vis);
        printf("Case %d: %d\n",t,res);
    }
}
