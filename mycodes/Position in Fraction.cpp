#include<bits/stdc++.h>

using namespace std;

int stringtoint(string s)
{
    int res=0,j=1,i;
    for(i=s.size()-1;i>=0;i--)
    {
        res+=(s[i]-'0')*j;
        j*=10;
    }
    return res;
}
string intostring(int n)
{
    string res="";
    while(n>0)
    {
        int i=n%10;
        res.insert(res.begin(),'0'+i);
        n/=10;
    }
    return res;
}
string longDivision(string number, int divisor)
{
    string ans;

    int idx = 0;
    int temp = number[idx] - '0';
    while (temp < divisor)
       temp = temp * 10 + (number[++idx] - '0');
    while (number.size() > idx)
    {
        ans += (temp / divisor) + '0';

        temp = (temp % divisor) * 10 + number[++idx] - '0';
    }

    if (ans.length() == 0)
        return "0";

    return ans;
}

int main()
{
    long long int a,b,c,i,j,k=10,pos=-1;
    scanf("%lld %lld %lld",&a,&b,&c);
    while(a>b)
    {
        a/=10;
    }

    string as=intostring(a);

    for(i=1;i<=1e3;i++)
    {
        as.push_back('0');
        string res=longDivision(as,b);
        if(res[res.size()-1]-'0'==c)
        {
            pos=i;
            break;
        }

    }
    cout<<pos<<endl;
}
