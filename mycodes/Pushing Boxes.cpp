#include<bits/stdc++.h>

using namespace std;

struct grid
{
    int r,c;
};
int r,c;
bool ok(grid x)
{
    if(x.r>=0 && x<r && x.c>=0 && x.c<c)
        return true;
    return false;
}
void bfs(vector<string>&maze,grid pos,vector<vector<string> >&dis)
{
    vector<int>col(c+1,0);
    vector<vector<int> >vis(r+1,col);
    string res="";
    queue<grid>q;
    q.push(pos);
    grid cur,nxt;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(ok(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && maze[nxt.r][nxt.c]=='.'){
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+"n";
            }
        }
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(ok(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && maze[nxt.r][nxt.c]=='.'){
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+"s";
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(ok(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && maze[nxt.r][nxt.c]=='.'){
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+"w";
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(ok(nxt))
        {
            if(vis[nxt.r][nxt.c]==0 && maze[nxt.r][nxt.c]=='.'){
                q.push(nxt);
                vis[nxt.r][nxt.c]=1;
                dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+"e";
            }
        }
    }
    return;
}

void preprocess(vector<string>&maze,vector<vector<vector<vector<vector<string> > > > >&memory)
{
    int i,j,k,l,m,n;
    for(i=0;i<r;i++)
    {
        for(j=0;j<c;j++)
        {
            if(maze[i][j]=='#') continue;
            for(k=0;k<r;k++)
            {
                for(l=0;l<c;l++)
                {

                    if((k!=i || l!=j) && maze[k][l]=='.')
                    {
                        maze[k][l]='#';
                        grid box,me,nxt;
                        box.r=k;
                        box.c=l;

                        me.r=i;
                        me.c=j;
                        vector<string>col(c,"");
                        vector<vector<string> >dis(r,col);
                        bfs(maze,me,dis);
                        nxt.r=box.r-1;
                        nxt.c=box.c;
                        if(ok(nxt))
                        {
                            memory[i][j][k][l][0]=dis[nxt.r][nxt.c];
                        }
                        nxt.r=box.r+1;
                        nxt.c=box.c;
                        if(ok(nxt))
                        {
                            memory[i][j][k][l][1]=dis[nxt.r][nxt.c];
                        }
                        nxt.r=box.r;
                        nxt.c=box.c-1;
                        if(ok(nxt))
                        {
                            memory[i][j][k][l][2]=dis[nxt.r][nxt.c];
                        }
                        nxt.r=box.r;
                        nxt.c=box.c+1;
                        if(ok(nxt))
                        {
                            memory[i][j][k][l][3]=dis[nxt.r][nxt.c];
                        }

                    }

                }
            }
        }
    }
}

int main()
{

}
