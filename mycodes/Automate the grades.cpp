#include<bits/stdc++.h>
using namespace std;

int main()
{
    int test,t=1;
    scanf("%d",&test);
    while(t<=test)
    {
        double term1,term2,finalterm,att,ct1,ct2,ct3;
        scanf("%lf %lf %lf %lf %lf %lf %lf",&term1,&term2,&finalterm,&att,&ct1,&ct2,&ct3);
        double i,j,total;
        i=max(max(ct1,ct2),max(ct2,ct3));
        if(i==ct1)
        {
            j=max(ct2,ct3);
        }
        else if(i==ct2)
        {
            j=max(ct1,ct3);
        }
        else
            j=max(ct2,ct1);
        total=term1+term2+finalterm+att+(i+j)/2;
        printf("Case %d: ",t);
        if(total>=90)
        {
            cout<<"A\n";
        }
        else if(total>=80)
        {
            cout<<"B\n";
        }
        else if(total>=70)
        {
            cout<<"C\n";
        }
        else if(total>=60)
        {
            cout<<"D\n";
        }
        else
        {
            cout<<"F\n";
        }
        t++;
    }
}
