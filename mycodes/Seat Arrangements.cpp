#include<bits/stdc++.h>
using namespace std;

int col_ways(int col,int n,int need,vector<vector<char> >&room)
{
    int i,j,ways=0,counter=0;
    for(i=1;i<=n;i++)
    {
        if(room[i][col]=='*')
        {
            //cout<<"bug at row "<<i<<" counter="<<counter<<endl;
            ways+=max(0,counter+1-need);
            counter=0;
        }
        else
            counter++;
    }
    ways+=max(0,counter+1-need);
    return ways;
}
int row_ways(int row,int m,int need,vector<vector<char> >&room)
{
    int i,j,ways=0,counter=0;
    for(i=1;i<=m;i++)
    {
        if(room[row][i]=='*')
        {
            //cout<<"bug at col "<<i<<" counter="<<counter<<endl;
            ways+=max(0,counter+1-need);
            counter=0;
        }
        else
            counter++;
    }
    ways+=max(0,counter+1-need);
    return ways;
}


int main()
{
    int n,m,i,j,need,res=0;
    scanf("%d %d %d",&n,&m,&need);
    vector<char>col(m+1);
    vector<vector<char> >room(n+1,col);
    for(i=1;i<=n;i++)
    {
        char c;
        for(j=1;j<=m;j++)
        {
            scanf(" %c",&c);
            room[i][j]=c;
        }
    }
    for(i=1;i<=n;i++)
    {
        res+=row_ways(i,m,need,room);
    }
    for(i=1;i<=m;i++)
    {
        res+=col_ways(i,n,need,room);
    }
    if(need==1)
    {
        res/=2;
    }
    printf("%d\n",res);
}
