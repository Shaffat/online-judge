#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct place
{
    string s;
    int att;
};
bool operator<(place a,place b)
{
    if(a.att!=b.att)
    {
        return a.att>b.att;
    }
    return false;
}
int main()
{
    freopen("tourist.txt","r",stdin);
    freopen("out.txt","w",stdout);
    ll t,test,k,n,v,i,j;
    string s;
    cin>>test;
    for(t=1;t<=test;t++)
    {
        cin>>n>>k>>v;
        vector<string>vs;
        vector<place>vis;
        for(i=1;i<=n;i++)
        {
            cin>>s;
            vs.push_back(s);
        }
        ll total=(v-1)*k,pos;
        pos=(total%n)+1;

        for(i=1;i<=k;i++)
        {
            place tmp;
            tmp.s=vs[pos-1];
            tmp.att=n-pos;
            pos++;
            if(pos>n)
            {
                pos=1;
            }
            vis.push_back(tmp);
        }
        sort(vis.begin(),vis.end());
        cout<<"Case #"<<t<<":";
        for(i=0;i<vis.size();i++)
        {
            cout<<" "<<vis[i].s;
        }
        cout<<endl;
    }
}
