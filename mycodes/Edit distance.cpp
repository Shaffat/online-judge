#include<bits/stdc++.h>
using namespace std;
int lcs(int i1,int i2,string &s1,string &s2,vector<vector<int> >&memory)
{
    //cout<<"i1="<<i1<<" i2="<<i2<<endl;
    if(i1>=s1.size()||i2>=s2.size())
    {
        if(i1<s1.size())
        {
            //cout<<"returning "<<s1.size()-i1<<endl;
            return s1.size()-i1;
        }
        if(i2<s2.size())
        {
            //cout<<"returning "<<s2.size()-i2<<endl;
            return s2.size()-i2;
        }
        //cout<<"returning 0"<<endl;
        return 0;
    }
    if(memory[i1][i2]!=-1)
    {
        return memory[i1][i2];
    }
    int i,j,mx=0,res;
    if(s1[i1]==s2[i2])
    {
        res=lcs(i1+1,i2+1,s1,s2,memory);
    }
    else
    {
        res=min(lcs(i1+1,i2,s1,s2,memory),lcs(i1,i2+1,s1,s2,memory));
        res=min(lcs(i1+1,i2+1,s1,s2,memory),res);
        res+=1;
    }
    return memory[i1][i2]=res;
}

int main()
{
    int test;
    scanf("%d",&test);
    while(test--)
    {
        string a,b;
        cin>>a>>b;
        vector<int>col(b.size(),-1);
        vector<vector<int> >memory(a.size(),col);
        int res=lcs(0,0,a,b,memory);
        printf("%d\n",res);
    }
}
