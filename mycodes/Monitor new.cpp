#include<bits/stdc++.h>
using namespace std;

#define ll long long int
ll GCD(ll a,ll b)
{
    while(a%b!=0)
    {
        ll temp=a%b;
        a=b;
        b=temp;
    }
    return b;
}
int biggest_multiple(ll a,ll b,ll x,ll y)
{
    ll first=0,last=a/x,mid,res=0,f=0;
    while(first<=last)
    {
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f=1;
        }
        mid=(first+last)/2;
        if(mid*x<=a && mid*y<=b)
        {
            res=mid;
            first=mid+1;
        }
        else
            last=mid-1;
    }
    return res;
}

int main()
{
    ll a,b,x,y,res,i,j,c;
    scanf("%lld %lld %lld %lld",&a,&b,&x,&y);
    while(1)
    {
        ll cur=GCD(x,y);
//        cout<<"x="<<x<<" y="<<y<<endl;
//        cout<<"cur="<<cur<<endl;
        if(cur==1)
        {
            break;
        }
        x/=cur;
        y/=cur;
    }

    res=biggest_multiple(a,b,x,y);
    printf("%lld %lld",x*res,y*res);
}
