#include<bits/stdc++.h>
using namespace std;

 struct pos
 {
     int r,c;
 };
vector<int>a(2,0);
vector<vector<int> >s(201,a);
vector<vector<vector<int> > >stage(201,s);
vector<vector<vector<int> > >vis(201,s);
vector<int>b(201,0);
vector<vector<int> >dis(201,b);
int r,c,t=1;
pos back_in;
 int solve(pos start,pos goal,vector<pos>&teleports)
 {
     int i,j,level=2e9,in=-1;
     vis[start.r][start.c][0]=t;
     dis[start.r][start.c]=0;
     queue<pos>q;
     q.push(start);
     while(!q.empty())
     {

         //cout<<"current="<<q.front().r<<" "<<q.front().c<<" dis="<<dis[q.front().r][q.front().c]<<endl;
         pos cur=q.front();
         q.pop();

         if(in==0)
         {
             if(level==dis[cur.r][cur.c])
             {
                 in=1;
                 if(vis[back_in.r][back_in.c][1]!=t)
                 {
                     vis[back_in.r][back_in.c][1]=t;
                     dis[back_in.r][back_in.c]=level;
                     q.push(back_in);
                 }
             }
         }
         if(cur.r==goal.r && cur.c==goal.c)
         {
             return dis[cur.r][cur.c];
         }
         if(stage[cur.r][cur.c][1]==t && vis[cur.r][cur.c][1]!=t)
         {
             //cout<<"here teleport"<<endl;
             if(teleports.size()<2)
             {
                 //cout<<"i shouldnt"<<endl;
                 continue;
             }
             else
             {
                 in=0;
                 back_in=cur;
                 //cout<<"i should be here"<<endl;
                 //cout<<back_in.r<<" "<<back_in.c<<endl;
                 for(i=0;i<teleports.size();i++)
                 {
                     if(teleports[i].r==cur.r && teleports[i].c==cur.c)
                     {
                         continue;
                     }
                     if( vis[teleports[i].r][teleports[i].c][0]!=t)
                     {
                         //cout<<teleports[i].r<<" "<<teleports[i].c<<" pushed"<<endl;
                         vis[teleports[i].r][teleports[i].c][0]=t;
                         vis[teleports[i].r][teleports[i].c][1]=t;
                         dis[teleports[i].r][teleports[i].c]=1+dis[cur.r][cur.c];
                         level=min(level,dis[cur.r][cur.c]+2);
                         q.push(teleports[i]);
                     }
                     else
                     {
                         vis[teleports[i].r][teleports[i].c][1]=t;
                         dis[teleports[i].r][teleports[i].c]=1+dis[cur.r][cur.c];
                         level=min(level,dis[cur.r][cur.c]+1);
                     }


                 }
             }
         }
         else
         {
             //cout<<"here normal"<<endl;
             pos nxt;
             nxt.r=cur.r+1;
             nxt.c=cur.c;
             if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
             {

                 if(vis[nxt.r][nxt.c][0]!=t && stage[nxt.r][nxt.c][0]==t)
                 {
                     //cout<<nxt.r<<" "<<nxt.c<<" pushed"<<endl;
                     vis[nxt.r][nxt.c][0]=t;
                     dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                     q.push(nxt);
                 }
             }
             nxt.r=cur.r-1;
             nxt.c=cur.c;
             if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
             {

                 if(vis[nxt.r][nxt.c][0]!=t && stage[nxt.r][nxt.c][0]==t)
                 {
                    //cout<<nxt.r<<" "<<nxt.c<<" pushed "<<stage[nxt.r][nxt.c][0]<<endl;
                     vis[nxt.r][nxt.c][0]=t;
                     dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                     q.push(nxt);
                 }
             }
             nxt.r=cur.r;
             nxt.c=cur.c+1;
             if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
             {

                 if(vis[nxt.r][nxt.c][0]!=t && stage[nxt.r][nxt.c][0]==t)
                 {
                     //cout<<nxt.r<<" "<<nxt.c<<" pushed"<<endl;
                     vis[nxt.r][nxt.c][0]=t;
                     dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                     q.push(nxt);
                 }
             }
             nxt.r=cur.r;
             nxt.c=cur.c-1;
             if(nxt.r<=r && nxt.r>0 && nxt.c<=c && nxt.c>0)
             {

                 if(vis[nxt.r][nxt.c][0]!=t && stage[nxt.r][nxt.c][0]==t)
                 {
                     //cout<<nxt.r<<" "<<nxt.c<<" pushed"<<endl;
                     vis[nxt.r][nxt.c][0]=t;
                     dis[nxt.r][nxt.c]=dis[cur.r][cur.c]+1;
                     q.push(nxt);
                 }
             }
         }
         if(q.empty()&& in==0)
         {
                 in=1;

                 if(vis[back_in.r][back_in.c][1]!=t)
                 {
                     vis[back_in.r][back_in.c][1]=t;
                     dis[back_in.r][back_in.c]=level;
                     q.push(back_in);
                 }
         }
     }
     return -1;
 }


 int main()
{
//
//    freopen("in.txt","r",stdin);
//    freopen("out.txt","w",stdout);
    t=1;
    int test,i,j;
    pos start,goal;
    scanf("%d",&test);
    while(t<=test)
    {

        scanf("%d %d",&r,&c);
        vector<pos>teleports;
        for(i=1;i<=r;i++)
        {
           for(j=1;j<=c;j++)
           {
               char k;
               scanf(" %c",&k);
               if(k=='.')
               {
                   stage[i][j][0]=t;
               }
               if(k=='*')
               {
                   stage[i][j][0]=t;
                   stage[i][j][1]=t;
                   pos tmp;
                   tmp.r=i;
                   tmp.c=j;
                   teleports.push_back(tmp);
               }
               if(k=='P')
               {
                   stage[i][j][0]=t;
                   start.r=i;
                   start.c=j;
               }
               if(k=='D')
               {
                   stage[i][j][0]=t;
                   goal.r=i;
                   goal.c=j;
               }
           }
        }


        int res=solve(start,goal,teleports);
        if(res>0)
        {
            printf("Case %d: %d\n",t,res);
        }
        else
        {
            printf("Case %d: impossible\n",t);
        }
        t++;
    }

}
