#include<bits/stdc++.h>
using namespace std;

int tower_building(int index,int h1,int h2,vector<int>&bricks,vector<int>&memory)
{
    cout<<"index="<<index<<" h1="<<h1<<" h2="<<h2<<endl;
    int res=0;
    if(h1==h2)
    {
        cout<<"equal"<<endl;
        res=h1;
    }
    if(index>=bricks.size())
    {
        return res;
    }
    cout<<"res="<<res<<endl;
    if(memory[index]!=-1)
    {
        cout<<"memorization"<<endl;
        return memory[index];
    }
    int case1,case2,case3,block=bricks[index];
    cout<<"index="<<index<<" h1="<<h1<<" h2="<<h2<<" is calling "<<"index="<<index+1<<" h1="<<h1+block<<" h2="<<h2<<endl;
    case1=tower_building(index+1,h1+block,h2,bricks,memory);
    cout<<"index="<<index<<" h1="<<h1<<" h2="<<h2<<" is calling "<<"index="<<index+1<<" h1="<<h1<<" h2="<<h2+block<<endl;
    case2=tower_building(index+1,h1,h2+block,bricks,memory);
    cout<<"index="<<index<<" h1="<<h1<<" h2="<<h2<<" is calling "<<"index="<<index+1<<" h1="<<h1<<" h2="<<h2<<endl;
    case3=tower_building(index+1,h1,h2,bricks,memory);
    cout<<"answrs of index="<<index<<" h1="<<h1<<" h2="<<h2<<endl;
     cout<<"res="<<res<<" case1="<<case1<<" case2="<<case2<<" case3="<<case3<<endl;
    res=max(res,max(case1,max(case2,case3)));
    return memory[index]=res;
}

int main()
{
    int test,t;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        int n,i,j;
        scanf("%d",&n);
        vector<int>bricks(n+1);
        vector<int>memory(n+1,-1);
        for(i=0;i<n;i++)
        {
            scanf("%d",&j);
            bricks[i]=j;
        }
        int res=tower_building(0,0,0,bricks,memory);
        printf("Case %d: %d\n",t,res);
    }
}
