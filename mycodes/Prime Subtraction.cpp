
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/

#include<bits/stdc++.h>

#define ll long long int
using namespace std;

int main()
{
    ll x,y,t;
    cin>>t;
    while(t--){
        cin>>x>>y;
        if(abs(x-y)>1)
        {
            cout<<"YES"<<endl;
        }
        else
            cout<<"NO"<<endl;
    }

}
