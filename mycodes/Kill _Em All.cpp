
/*
ID: msdipu11
PROG: palsquare
LANG: C++14
*/

/*
timus
Your JUDGE_ID: 280608OK
*/
#include<bits/stdc++.h>

#define ll long long int
using namespace std;


struct info
{
    ll p,val;
};
info tree[400000];



void update(ll node,ll val,ll s,ll e,ll q_s,ll q_e)
{
    if(s>q_e||e<q_s)
    {
        return;
    }
    if(s>=q_s && e<=q_e)
    {
        tree[node].p+=val;
        //cout<<"before ="<<tree[node].val<<endl;
        tree[node].val+=val*(e-s+1);
         //cout<<"add propagtion at node "<<node<<" s="<<s<<" e="<<e<<" val="<<tree[node].val<<" p="<<tree[node].p<<endl;
        return;
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    update(left,val,s,mid,q_s,q_e);
    update(right,val,mid+1,e,q_s,q_e);
    tree[node].val=tree[left].val+tree[right].val+(e-s+1)*tree[node].p;
    //cout<<"updated value at node "<<node<<" s="<<s<<" e="<<e<<" total="<<tree[node].val<<endl;
}


ll query(ll node,ll s,ll e,ll q_s,ll q_e,ll carry)
{
   // cout<<"node="<<node<<" s="<<s<<" e="<<e<<" carry="<<carry<<endl;
    if(s>q_e||e<q_s)
    {
        return 0;
    }
    if(s>=q_s && e<=q_e)
    {
        return tree[node].val+carry*(e-s+1);
    }
    ll left,right,mid;
    left=(node<<1);
    right=(node<<1)+1;
    mid=(s+e)/2;
    return query(left,s,mid,q_s,q_e,carry+tree[node].p)+query(right,mid+1,e,q_s,q_e,carry+tree[node].p);
}


void build_tree(ll node,ll start,ll ending,vector<ll>&values)
{

    ll left,right,mid;
    if(start==ending)
    {
        tree[node].p=0;
        tree[node].val=values[start];
        //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
        return;
    }
    left=(node<<1);
    right=(node<<1)+1;
    mid=(start+ending)/2;
    build_tree(left,start,mid,values);
    build_tree(right,mid+1,ending,values);
    tree[node].p=0;
    tree[node].val=tree[left].val+tree[right].val;
     //cout<<"node="<<node<<" s="<<start<<" e="<<ending<<" total="<<tree[node].val<<endl;
    return;
}

int lastLeft(ll pos,ll val,ll r, vector<ll>&v)
{
    ll first,last,mid,target=val-r+1,res=-1,f=0;
    first=1;
    last=pos-1;
    cout<<"first="<<first<<" last="<<last<<" target="<<target<<endl;
    while(first<=last)
    {
        if(f) break;
        if(first==last) f++;

        mid=(first+last)/2;
        if(v[mid]>=target)
        {
            res=mid;
            last=mid-1;
        }
        else
            first=mid+1;
    }
    return res;
}

int main()
{
    ll test,t,i,j,n,m,r,cur,total;
    scanf("%lld",&test);
    while(test--){
        scanf("%lld %lld",&n,&r);
        vector<ll>v;
        v.push_back(-1);
        for(i=1;i<=n;i++)
        {
            scanf("%lld",&j);
            v.push_back(j);
        }
        total=0;
        sort(v.begin(),v.end());
        build_tree(1,1,n,v);
        ll last=1e9;
        for(i=v.size()-1;i>=1;i--)
        {

            cur=query(1,1,n,i,i,0);
            //cout<<"at pos "<<i<<" cur="<<cur<<" last="<<last<<endl;
            if(cur>=last || cur<=0)
            {
                continue;
            }
            //cout<<"launching missile "<<endl;
            total++;
            //ll updaterange=lastLeft(cur,r,v);
            //cout<<"range is "<<updaterange<<endl;
            update(1,-r,1,n,1,i-1);
            last=cur-r;
            //cout<<"missiled and last="<<last<<endl;

        }
        printf("%lld\n",total);    }

}
