#include<bits/stdc++.h>
using namespace std;
struct pos
{
    int r,c;
};
int r,c;
bool ok(pos cur)
{
    //cout<<"cur="<<cur.r<<" "<<cur.c<<endl;
    if(cur.r>=1 && cur.r<=r && cur.c>=1 && cur.c<=c)
        return true;
    //cout<<"not"<<endl;
    return false;
}

void bfs(pos p,vector<pos>&v,vector<vector<int> >&matrix)
{
    //cout<<"here"<<endl;
    int i,j;
    pos cur=p;
    vector<int>col(c+1,0);
    vector<vector<int> >vis(r+1,col);
    queue<pos>q;
    q.push(cur);
    v.push_back(cur);
    vis[p.r][p.c]=1;
    while(!q.empty())
    {
        cur=q.front();
        q.pop();
        pos nxt;
        nxt.r=cur.r+1;
        nxt.c=cur.c;
        if(ok(nxt))
        {
            if(matrix[nxt.r][nxt.c]==0 && vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
                v.push_back(nxt);
            }
        }
        nxt.r=cur.r-1;
        nxt.c=cur.c;
        if(ok(nxt))
        {
            if(matrix[nxt.r][nxt.c]==0 && vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
                v.push_back(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c+1;
        if(ok(nxt))
        {
            if(matrix[nxt.r][nxt.c]==0 && vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
                v.push_back(nxt);
            }
        }
        nxt.r=cur.r;
        nxt.c=cur.c-1;
        if(ok(nxt))
        {
            if(matrix[nxt.r][nxt.c]==0 && vis[nxt.r][nxt.c]==0)
            {
                vis[nxt.r][nxt.c]=1;
                q.push(nxt);
                v.push_back(nxt);
            }
        }

    }
    return;
}

int dis(pos a,pos b)
{
    return (a.r-b.r)*(a.r-b.r)+(a.c-b.c)*(a.c-b.c);
}

int main()
{
    int n,i,j,s_r,s_c,d_r,d_c,res=2e9,k,l;
    pos s,d;
    scanf("%d %d %d %d %d",&n,&s.r,&s.c,&d.r,&d.c);
    vector<int>col(n+1);
    vector<vector<int> >matrix(n+1,col);
    r=n;
    c=n;
    vector<string>v;
    for(i=1;i<=n;i++)
    {
        string s;
        cin>>s;
        v.push_back(s);
    }
    for(i=1;i<=n;i++)
    {
        for(j=1;j<=n;j++)
        {
            k=v[i-1][j-1]-'0';
            if(!k)
                matrix[i][j]=0;
            else
                matrix[i][j]=1;
            //cout<<k;
        }

        //cout<<endl;
    }
//    for(i=1;i<=n;i++)
//    {
//        for(j=1;j<=n;j++)
//            cout<<matrix[i][j];
//        cout<<endl;
//    }

    vector<pos>a;
    vector<pos>b;
    bfs(s,a,matrix);
    //cout<<"..................\n";
    bfs(d,b,matrix);
    for(i=0;i<a.size();i++)
    {

        for(j=0;j<b.size();j++)
        {
            res=min(res,dis(a[i],b[j]));
        }
    }
    cout<<res<<endl;
}
