/*
ID: msdipu11
PROG: milk2
LANG: C++14
*/
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
struct duration
{
    ll start,ending;
};

bool operator <(duration a, duration b)
{
    if(a.start!=b.start)
    {
        return a.start<b.start;
    }
    return false;
}

int main()
{
    freopen("milk2.in","r",stdin);
    freopen("milk2.out","w",stdout);
    ll n,i,j,s,e,active=0,idle=0,mxactive=0,mxidle=0,ending;
    scanf("%lld",&n);
    vector<duration>v;
    for(i=1;i<=n;i++)
    {
        scanf("%lld %lld",&s,&e);
        duration tmp;
        tmp.start=s;
        tmp.ending=e;
        v.push_back(tmp);
    }
    sort(v.begin(),v.end());
    mxactive=v[0].ending-v[0].start;
    active=mxactive;
    ending=v[0].ending;
    for(i=1;i<v.size();i++)
    {
        //cout<<"start="<<v[i].start<<" ending="<<v[i].ending<<" pre ending="<<ending<<endl;
        if(ending<v[i].start)
        {
            mxactive=max(mxactive,active);
            active=v[i].ending-v[i].start;
            mxidle=max(v[i].start-ending,mxidle);
            ending=v[i].ending;
        }
        else
        {
            if(ending>=v[i].ending)
            {
                continue;
            }
            active+=v[i].ending-ending;
            //cout<<"still active, total="<<active<<endl;
            ending=v[i].ending;
        }
    }
    mxactive=max(mxactive,active);
    printf("%lld %lld\n",mxactive,mxidle);

}
