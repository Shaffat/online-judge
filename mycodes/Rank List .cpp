#include<bits/stdc++.h>

using namespace std;

struct ranking
{
    int p,t;
};

bool operator <(ranking a, ranking b)
{
    if(a.p!=b.p)
    {
        return a.p>b.p;
    }
    if(a.t!=b.t)
    {
        return a.t<b.t;
    }
    return false;
}

int main()
{
    int n,k,p,t,i,j;
    scanf("%d %d",&n,&k);
    vector<ranking>teams;
    vector<int>col(59,0);
    vector<vector<int> >total(59,col);
    for(i=1;i<=n;i++)
    {
        scanf("%d %d",&p,&t);
        ranking tmp;
        tmp.p=p;
        tmp.t=t;
        teams.push_back(tmp);
        total[p][t]++;
    }
    sort(teams.begin(),teams.end());
    k--;
    cout<<total[teams[k].p][teams[k].t]<<endl;
}
