#include<bits/stdc++.h>
using namespace std;

int solve(int pos,int limit,int bag,int buyprice,vector<int>&items,vector<vector<vector<int> > >&memory)
{
    if(pos>=items.size()){
        return 0;
    }

    if(memory[pos][limit][bag]!=-1){
        return memory[pos][limit][bag];
    }

    int res1=0,res2=0,res3=0,res4=0,i,j,profit;
    if(bag)
    {
        ///have an item
        profit=items[pos]-buyprice;
        if(profit>=limit)
        {
            res1=solve(pos+1,profit,0,0,items,memory)+profit;
        }
        ///will not sell the item today
        res2=solve(pos+1,limit,bag,buyprice,items,memory);
    }
    else
    {
        ///dont have anything to sell in bag, so i can buy

        res3=solve(pos+1,limit,1,items[pos],items,memory);

        ///will not buy item today
        res4=solve(pos+1,limit,bag,buyprice,items,memory);
    }
    int res=max(res1,max(res2,max(res3,res4)));
    return memory[pos][limit][bag]=res;
}
int main()
{
    int test,t,i,j,n,m;
    scanf("%d",&test);
    for(t=1;t<=test;t++)
    {
        vector<int>items;
        scanf("%d",&n);
        for(i=1;i<=n;i++)
        {
            scanf("%d",&j);
            items.push_back(j);
        }
        //cout<<"item size "<<items.size()<<endl;
        vector<int>col1(2,-1);
        vector<vector<int> >col2(2001,col1);
        vector<vector<vector<int> > >memory(1001,col2);
        int res=solve(0,0,0,0,items,memory);
        printf("%d\n",res);
    }
}
