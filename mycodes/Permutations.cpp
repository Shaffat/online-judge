#include<bits/stdc++.h>

using namespace std;

int converter(string s)
{
    int res=0,j=1,i=1;
    for(i=s.size()-1;i>=0;i--)
    {
        res+=j*(s[i]-'0');
        j*=10;
    }
    return res;
}

string builder(string order,string s)
{
    string tmp;
    int i;
    for(i=0;i<order.size();i++)
    {
        tmp.push_back(s[order[i]-'0']);
    }
    return tmp;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n,k,i,mn=2e9,mx=-1,dif=2e9;
    cin>>n>>k;
    vector<string>v;
    string s;
    for(i=1;i<=n;i++)
    {
        cin>>s;
        v.push_back(s);
    }
    sort(v.begin(),v.end());
     dif=min(dif,converter(v[n-1])-converter(v[0]));
    string order;
    for(i=0;i<k;i++)
    {
        order.push_back(i+'0');
    }
    while(next_permutation(order.begin(),order.end()))
    {
         vector<string>tmp;
         for(i=0;i<v.size();i++)
         {
             tmp.push_back(builder(order,v[i]));
         }
         sort(tmp.begin(),tmp.end());
         dif=min(dif,converter(tmp[n-1])-converter(tmp[0]));
    }
    cout<<dif<<endl;
}
