#include<bits/stdc++.h>
using namespace std;

#define ll long long int

struct point
{
    ll x,y,h;
};

ll calculate_applitute(ll h,ll x,ll y,ll cx,ll cy)
{
    ll h1=h-abs(x-cx)-abs(y-cy);
    //cout<<"h1="<<h1<<endl;
    ll res= max(h1,0ll);
    //cout<<"res="<<res<<endl;
    return res;
}

ll solve(ll cx,ll cy,vector<point>&points)
{
    ll i,j,first,last,mid,f=0;
    first=1;
    last=1e15;
    while(first<=last)
    {
        //cout<<"first="<<first<<" last="<<last;
        if(f)
        {
            break;
        }
        if(first==last)
        {
            f=1;
        }
        mid=(first+last)/2;
        //cout<<" mid="<<mid<<endl;
        int right=1;
        for(i=0;i<points.size();i++)
        {
            ll cur=calculate_applitute(mid,points[i].x,points[i].y,cx,cy);
            //cout<<"cur="<<cur<<endl;
            if(cur!=points[i].h)
            {
                right=0;
                if(cur<points[i].h)
                {
                    //cout<<"for "<<i<<"th point cur h="<<cur<<" correct_h="<<points[i].h<<endl;
                    first=mid+1;
                }
                else
                    last=mid-1;
                break;
            }
        }
        if(right)
        {
            return mid;
        }
    }
    return 0;

}

int main()
{
    ll n,i,j,x,y,h,cx,cy,ch;
    scanf("%lld",&n);
    vector<point>points;
    for(i=1;i<=n;i++)
    {
        point tmp;
        scanf("%lld %lld %lld",&x,&y,&h);
        tmp.x=x;
        tmp.y=y;
        tmp.h=h;
        points.push_back(tmp);
    }
    int done;
    for(i=0;i<=100;i++)
    {
        for(j=0;j<=100;j++)
        {
            done=0;
            ll cur;
            if(cur=solve(i,j,points))
            {
                cx=i;
                cy=j;
                ch=cur;
                done=1;
                break;
            }
        }
        if(done)
        {
            break;
        }
    }
    printf("%lld %lld %lld\n",cx,cy,ch);
}
